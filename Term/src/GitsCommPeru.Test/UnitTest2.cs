﻿using System;
using System.IO;
using gcperu.Term.Agents;
using gcperu.Term.NmeaParser;
using GitsCommPeru.Test.Properties;
using Microsoft.Reactive.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32.SafeHandles;
using ReactiveUI.Testing;

namespace GitsCommPeru.Test
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestMethod1()
        {
            var notifier = new NmeaPositionNotifier();
            var parser = new Nmea();
            parser.Source = new MemoryStream(Resources.NmeaFile_Viaje1);
            notifier.Init(parser);
            parser.Start();
            parser.WaitDone();
            var fakePosObservable = notifier
                .Scheduler
                .CreateHotObservable(notifier.GpsNotificationRecords.ToArray());

            fakePosObservable.Subscribe(d =>
            {
                Console.WriteLine(d.Latitude);
            });
//            notifier.Scheduler.AdvanceToMs(TimeSpan.FromHours(2).Milliseconds);
            var count = 0;
            do
            {
                count += 1000;
                notifier.Scheduler.AdvanceToMs(count);
                Console.WriteLine("Time " + count);
            }
            while (string.IsNullOrWhiteSpace(Console.ReadLine()));
        }
    }
}
