﻿using System;
using System.Collections.Generic;
using System.Reactive;
using Geolocator.Plugin.Abstractions;
using Microsoft.Reactive.Testing;
using ReactiveUI.Testing;

//namespace GITSNavega.CoreDevice.GPS
//{
//    public class GpsPosition 
//    {
//        public static readonly GpsPosition Empty = new GpsPosition();
//
//        //public double speed = -1.0; // km/h
//        //public double course = -1.0; // degrees
//        //public DateTime date = DateTime.MinValue;
//        //public TimeSpan time = TimeSpan.MinValue;
//
//        private double _speedUnit;
//
//        /// <summary>
//        /// The accuracy of the location in meters.
//        /// </summary>
//        public double Accuraty { get; private set; }
//
//        /// <summary>
//        /// The altitude of the location, in meters.
//        /// </summary>
//        public double? Altitude { get; private set; }
//
//        /// <summary>
//        /// The accuracy of the altitude, in meters.
//        /// </summary>
//        public double? AltitudeAccuracy { get; private set; }
//
//        /// <summary>
//        /// The current heading in degrees relative to true north.
//        /// </summary>
//        public double? Heading { get; internal set; }
//
//        /// <summary>
//        /// The latitude in degrees.
//        /// </summary>
//        public double Latitude { get; internal set; }
//
//        /// <summary>
//        /// The longitude in degrees.
//        /// </summary>
//
//        public double Longitude { get; internal set; }
//        
//        /// <summary>
//        /// The speed in meters per second.
//        /// </summary>
//        public double? Speed { get; internal set; }
//
//        /// <summary>
//        /// The time at which the location was determined.
//        /// </summary>
//        public DateTimeOffset Timestamp { get; protected internal set; }
//
//        public GpsPosition()
//        {
//            Init();
//        }
//
//        public GpsPosition(Position geoposition)
//        {
//            this.Altitude = geoposition.Altitude ?? 0;
//            this.AltitudeAccuracy = geoposition.Coordinate.AltitudeAccuracy ?? 0;
//
//            this.Accuraty = geoposition.Coordinate.Accuracy;
//            this.Longitude = geoposition.Coordinate.Longitude;
//            this.Latitude = geoposition.Coordinate.Latitude;
//            this.Heading = geoposition.Coordinate.Heading ?? 0;
//
//            //this.PositionSource = geoposition.Coordinate.PositionSource;
//            this.Timestamp = geoposition.Coordinate.Timestamp;
//            
//            this.Speed = geoposition.Coordinate.Speed ?? 0;
//            _speedUnit=SpeedUnit.MetersPerSecond;
//        }
//
//        #region Properties
//
//        //public Windows.Devices.Geolocation.PositionSource PositionSource { get; private set; }
//
//        public bool Fix { get; set; }
//        public int satelites = 0;
//        public double Hdop { get; internal set; }
//        public double Vdop { get; internal set; }
//        
//
//        #endregion
//
//        private void Init()
//        {
//            _speedUnit = SpeedUnit.KilometersPerHour;
//            Fix = false;
//            Hdop = 0;
//            Vdop = 0;
//
//            Altitude = 0;
//            AltitudeAccuracy = null;
//            Accuraty = -1;
//            Longitude = 0;
//            Latitude = 0;
//            Heading = 0;
//            Speed = 0;
//            Timestamp = DateTimeOffset.Now;
//        }
//
//        public void InitPosition(GpsPosition other)
//        {
//            if (object.ReferenceEquals(this, other))
//                return;
//
//            this.Longitude = other.Longitude;
//            this.Latitude = other.Latitude;
//            this.Speed = other.Speed;
//            this.Heading = other.Heading;
//            this.Timestamp = other.Timestamp;
//            this.Fix = other.Fix;
//            this.satelites = other.satelites;
//            this.Hdop = other.Hdop;
//        }
//
//        public double SpeedKmh
//        {
//            get
//            {
//                if (this.Speed == null)
//                    return 0;
//
//                if (_speedUnit == SpeedUnit.KilometersPerHour)
//                    return Speed.Value;
//                
//                //Si no son metos/seg
//                return GpsFramework.Speed.FromMetersPerSecond(this.Speed.Value).Value;
//            }
//        }
//
//        public bool IsEmpty
//        {
//            get
//            {
//                return (this.Latitude==0 && this.Longitude==0);
//            }
//        }
//
//        // for debug only
//        public override string ToString()
//        {
//            return "Gps: {X=" + Longitude + " Y=" + Latitude 
//                   + "} S:" + Speed + "km/h C:" + Heading
//                   + " H:" + Hdop
//                   + " P:" + Fix
//                   + " ST: " + satelites
//                   + " D:" + Timestamp.DateTime.ToString()
//                   + string.Empty;
//        }
//    }
//}

namespace gcperu.Term.NmeaParser
{
    //EOC

    public class NmeaPositionNotifier
    {
        public event NewGspPositionEventHandler NewGspPosition;
        public delegate void NewGspPositionEventHandler(Position pos);



        //public NmeaMsg.MsgType cycleStartMsgType = NmeaMsg.MsgType.GGA;
        public NmeaMsg.MsgType cycleStartMsgType = NmeaMsg.MsgType.RMC;

        public NmeaPositionNotifier()
        {
            _reportInterval = TimeSpan.FromSeconds(1);
            _initialReportInterval = _reportInterval;
        }

        private readonly TimeSpan _initialReportInterval;
        public TimeSpan ReportInterval
        {
            get { return _reportInterval; }
            set { _reportInterval = value; }
        }

        ~NmeaPositionNotifier()
        {
            Uninit();
        }
        
        private Nmea parser = null;
        private TimeSpan _reportInterval;

        public readonly TestScheduler Scheduler = new TestScheduler();

        public List<Recorded<Notification<Position>>> GpsNotificationRecords { get; private set; }

        private void FireNewPosition(Position pos)
        {
            if (GpsNotificationRecords == null)
                GpsNotificationRecords = new List<Recorded<Notification<Position>>>();
            GpsNotificationRecords.Add(Scheduler.OnNextAt(_reportInterval.TotalMilliseconds, pos));
            _reportInterval = _reportInterval.Add(_initialReportInterval);
            if(NewGspPosition != null)
            {
//                Thread.Sleep((int)_reportInterval.TotalMilliseconds);
//                NewGspPosition(pos);
            }
        }

        public void Init(Nmea parser)
        {
            if (parser != null)
            {
                parser.NewMessage += new Nmea.NewMessageEventHandler(HandleNewMessage);
            }
        }

        public void Uninit()
        {
            if (parser != null)
            {
                parser.NewMessage -= new Nmea.NewMessageEventHandler(HandleNewMessage);
            }
            parser = null;
        }

        private void HandleNewMessage(NmeaMsg msg)
        {
            Field f = null;
            bool newPosition = (msg.id == cycleStartMsgType);
            var pos = new Position();
            switch(msg.id)
            {
                case NmeaMsg.MsgType.GGA:
//                    f = (Field)msg.Fields[GGA.FieldIds.PositionFixIndicator];
//                    if ((f != null) && f.HasValue)
//                    {
//                        position.Fix = f.GetInt(0) >= 1;
//                    }

                    f = (Field)msg.Fields[GGA.FieldIds.X];
                    if((f != null) && f.HasValue)
                    {
                        pos.Longitude = f.GetDouble(pos.Longitude);
                    }
                    f = (Field)msg.Fields[GGA.FieldIds.Y];
                    if ((f != null) && f.HasValue)
                    {
                        pos.Latitude = f.GetDouble(pos.Latitude);
                    }
                    f = (Field)msg.Fields[GGA.FieldIds.Utc];
                    if ((f != null) && f.HasValue)
                    {
                        pos.Timestamp = f.GetDate(pos.Timestamp.DateTime);
                    }
//                    f = (Field)msg.Fields[GGA.FieldIds.Satelites];
//                    if ((f != null) && f.HasValue)
//                    {
//                        position.satelites = f.GetInt(position.satelites);
//                    }
//                    f = (Field)msg.Fields[GGA.FieldIds.Hdop];
//                    if ((f != null) && f.HasValue)
//                    {
//                        position.Hdop = f.GetDouble(position.Hdop);
//                    }
                    break;

                case NmeaMsg.MsgType.RMC:
                    f = (Field)msg.Fields[RMC.FieldIds.Status];
                    if ((f != null) && f.HasValue)
                    {
                        char status = f.GetChar((char)0);
                        if(status == 'A') // read data only if valid
                        {
                            f = (Field)msg.Fields[RMC.FieldIds.X];
                            if ((f != null) && f.HasValue)
                            {
                                pos.Longitude = f.GetDouble(pos.Longitude);
                            }
                            f = (Field)msg.Fields[RMC.FieldIds.Y];
                            if ((f != null) && f.HasValue)
                            {
                                pos.Latitude = f.GetDouble(pos.Latitude);
                            }
                            f = (Field)msg.Fields[RMC.FieldIds.Utc];
                            if ((f != null) && f.HasValue)
                            {
                                var t = f.GetTime(DateTime.Now.TimeOfDay);
                                pos.Timestamp = new DateTimeOffset(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, t.Hours, t.Minutes, t.Seconds, 0, new TimeSpan());
                            }
                            f = (Field)msg.Fields[RMC.FieldIds.Speed];
                            if ((f != null) && f.HasValue)
                            {
                                pos.Speed = f.GetDouble(pos.Speed);
                            }
                            f = (Field)msg.Fields[RMC.FieldIds.Course];
                            if ((f != null) && f.HasValue)
                            {
                                pos.Heading = f.GetDouble(pos.Heading);
                            }
                        }
                    }
                    break;
            }
            if(newPosition)
            {
                this.FireNewPosition(pos);
            }
        }

    }//EOC

}//EON
