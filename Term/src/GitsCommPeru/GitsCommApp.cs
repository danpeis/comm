﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reactive.Linq;
using Amcoex.Communications.Queue;
using gcperu.Term.Agents;
using gcperu.Agents;
using gcperu.Term.Enum;
using gcperu.Term.Utils;
using Geolocator.Plugin.Abstractions;
using ReactiveUI;
using SimpleInjector;
using Splat;

namespace gcperu.Term
{
    [SuppressMessage("ReSharper", "RedundantTypeArgumentsOfMethod")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public sealed class GitsCommApp : ReactiveObject, IEnableLogger
    {
        private readonly ConfigurationManager ConfigurationManager = new ConfigurationManager();
        private readonly Container DependencyContainer = new Container();
        private readonly PackageManager PackageManager;
        private readonly IObservable<Position> GpsPositions = GpsAgent.GpsPositions(1000, 25, true);
        private readonly IObservable<BatteryStatusEnum> BatteryStatus = BatteryAgent.BatteryStatus();
        private readonly IObservable<BroadbandData> BroadbandData = BroadbandAgent.GetCardData();
        private readonly IObservable<IPackageClientQueue> PackageClientSupplier;

        public GitsCommApp()
        {
            Locator.CurrentMutable.RegisterConstant(new LoggerSmartInspectImplementation("GCApp") {Level = LogLevel.Debug}, typeof(ILogger));
            PackageClientSupplier = ConfigurationManager
                .SignalRPackageClientSupplier()
                .PackageClientReconnectAttempts(ConfigurationManager)
                .SplatLogInfo(pc => string.Format("New client pointing to Url: {0}", pc.Url), logger: LogTags.ServerConnectionStateTag.Log())
                .CreatePackageClientOnConnectionFailure();

            PackageManager = new PackageManager(PackageClientSupplier);
        }

        public void Run(string[] args)
        {
            BroadbandData
                .SplatLogInfo(cardData =>
                    string.Format("Broadband:\n\tRegistered: {0}\n\tIMEI: {1}\n\tIMSI: {2}\n\tICCID: {3}",
                        cardData.BroadbandDeviceAvailable, cardData.IMEI, cardData.IMSI, cardData.ICCID), logger: LogTags.BroadbandStatusTag.Log())
                .Subscribe(InterProcessAgent.BroadbandDataBroadcastObserver);

            this.WhenAnyObservable(x => x.PackageManager.PackageClientOpQueue.ConnectionChange)
                .SplatLogDebug(st => string.Format("[Old={0}, New={1}]", st.OldState, st.NewState), logger: LogTags.ServerConnectionStateTag.Log())
                .ConvertStateChangeToServerConnectionEnum()
                .Subscribe(InterProcessAgent.ServerConnectionBroadcastObserver);

            BatteryStatus.Subscribe(InterProcessAgent.BatteryStatusBroadcastObserver);
            
            GpsPositions
                .SplatLogInfo(p => string.Format("GPS      |> Lon:{0}, Lat:{1}", p.Longitude, p.Latitude), logger: LogTags.GpsPositionTag.Log())
                .Odometer()
                .SplatLogInfo(d => string.Format("Odometer |> Dis:{0} Kilometers", d.Kilometers), logger: LogTags.OdometerNewDistanceTag.Log())
                .Subscribe(InterProcessAgent.OdometerBroadcastObserver);
        }
    }

    public class LogTags
    {
        public static readonly ServerConnectionState ServerConnectionStateTag = new ServerConnectionState();
        public static readonly GpsPosition GpsPositionTag = new GpsPosition();
        public static readonly OdometerNewDistance OdometerNewDistanceTag = new OdometerNewDistance();
        public static readonly BroadbandStatus BroadbandStatusTag = new BroadbandStatus();

        public class ServerConnectionState : IEnableLogger { }
        public class GpsPosition : IEnableLogger { }
        public class OdometerNewDistance : IEnableLogger { }
        public class BroadbandStatus : IEnableLogger { }
    }

    public static class LogExtensions
    {
        private static string DefaultErrorLog(Exception ex)
        {
            return ""; 
        }

        private static string DefaultNextLog<T>(T ex)
        {
            return "";
        }

        private static string DefaultCompletedLog()
        {
            return "";
        }

        public static IObservable<T> SplatLogInfo<T>(this IObservable<T> source,
            Func<T, string> formatNext = null, 
            Func<Exception, string> formatError = null,
            Func<string> formatCompleted = null,
            IFullLogger logger = null)
        {
            var inputNext = formatNext ?? DefaultNextLog;
            var inputError = formatError ?? DefaultErrorLog;
            var inputCompleted = formatCompleted ?? DefaultCompletedLog;
            var inputLogger = logger ?? LogHost.Default;
            return source.Do(data => inputLogger.Info(inputNext(data)), ex => inputLogger.InfoException(inputError(ex), ex), () => inputLogger.Info(inputCompleted()));
        }

        public static IObservable<T> SplatLogDebug<T>(this IObservable<T> source,
            Func<T, string> formatNext = null,
            Func<Exception, string> formatError = null,
            Func<string> formatCompleted = null,
            IFullLogger logger = null)
        {
            var inputNext = formatNext ?? DefaultNextLog;
            var inputError = formatError ?? DefaultErrorLog;
            var inputCompleted = formatCompleted ?? DefaultCompletedLog;
            var inputLogger = logger ?? LogHost.Default;
            return source.Do(data => inputLogger.Debug(inputNext(data)), ex => inputLogger.DebugException(inputError(ex), ex), () => inputLogger.Debug(inputCompleted()));
        }

        public static IObservable<T> SplatLogWarn<T>(this IObservable<T> source,
            Func<T, string> formatNext = null,
            Func<Exception, string> formatError = null,
            Func<string> formatCompleted = null,
            IFullLogger logger = null)
        {
            var inputNext = formatNext ?? DefaultNextLog;
            var inputError = formatError ?? DefaultErrorLog;
            var inputCompleted = formatCompleted ?? DefaultCompletedLog;
            var inputLogger = logger ?? LogHost.Default;
            return source.Do(data => inputLogger.Warn(inputNext(data)), ex => inputLogger.WarnException(inputError(ex), ex), () => inputLogger.Warn(inputCompleted()));
        }

        public static IObservable<T> SplatLogError<T>(this IObservable<T> source,
            Func<T, string> formatNext = null,
            Func<Exception, string> formatError = null,
            Func<string> formatCompleted = null,
            IFullLogger logger = null)
        {
            var inputNext = formatNext ?? DefaultNextLog;
            var inputError = formatError ?? DefaultErrorLog;
            var inputCompleted = formatCompleted ?? DefaultCompletedLog;
            var inputLogger = logger ?? LogHost.Default;
            return source.Do(data => inputLogger.Error(inputNext(data)), ex => inputLogger.ErrorException(inputError(ex), ex), () => inputLogger.Error(inputCompleted()));
        }

        public static IObservable<T> SplatLogFatal<T>(this IObservable<T> source,
            Func<T, string> formatNext = null,
            Func<Exception, string> formatError = null,
            Func<string> formatCompleted = null,
            IFullLogger logger = null)
        {
            var inputNext = formatNext ?? DefaultNextLog;
            var inputError = formatError ?? DefaultErrorLog;
            var inputCompleted = formatCompleted ?? DefaultCompletedLog;
            var inputLogger = logger ?? LogHost.Default;
            return source.Do(data => inputLogger.Fatal(inputNext(data)), ex => inputLogger.FatalException(inputError(ex), ex), () => inputLogger.Fatal(inputCompleted()));
        } 
    }
}
