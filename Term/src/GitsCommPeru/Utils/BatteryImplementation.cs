﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Battery.Plugin.Abstractions;
using Microsoft.Win32;

namespace Battery.Plugin.Abstractions
{
    /// <summary>
    /// Current status of battery
    /// </summary>
    public enum BatteryStatus
    {
        /// <summary>
        /// Plugged in and charging
        /// </summary>
        Charging,
        /// <summary>
        /// Battery is being drained currently
        /// </summary>
        Discharging,
        /// <summary>
        /// Battery is full completely
        /// </summary>
        Full,
        /// <summary>
        /// Not charging, but not discharging either
        /// </summary>
        NotCharging,
        /// <summary>
        /// Unknown or other status
        /// </summary>
        Unknown

    }

    /// <summary>
    /// Interface for Battery
    /// </summary>
    public interface IBattery : IDisposable
    {
        /// <summary>
        /// Current battery level 0 - 100
        /// </summary>
        int RemainingChargePercent { get; }

        /// <summary>
        /// Current status of the battery
        /// </summary>
        BatteryStatus Status { get; }

        /// <summary>
        /// Currently how the battery is being charged.
        /// </summary>
        PowerSource PowerSource { get; }

        /// <summary>
        /// Event handler when battery changes
        /// </summary>
        event BatteryChangedEventHandler BatteryChanged;

    }

    /// <summary>
    /// Arguments to pass to event handlers
    /// </summary>
    public class BatteryChangedEventArgs : EventArgs
    {
        /// <summary>
        /// If the battery level is considered low
        /// </summary>
        public bool IsLow { get; set; }
        /// <summary>
        /// Gets if there is an active internet connection
        /// </summary>
        public int RemainingChargePercent { get; set; }

        /// <summary>
        /// Current status of battery
        /// </summary>
        public BatteryStatus Status { get; set; }

        /// <summary>
        /// Get the source of power.
        /// </summary>
        public PowerSource PowerSource { get; set; }
    }

    /// <summary>
    /// Battery Level changed event handlers
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void BatteryChangedEventHandler(object sender, BatteryChangedEventArgs e);


    /// <summary>
    /// Currently how battery is being charged.
    /// </summary>
    public enum PowerSource
    {
        /// <summary>
        /// Not currently charging and on battery
        /// </summary>
        Battery,
        /// <summary>
        /// Wall charging
        /// </summary>
        Ac,
        /// <summary>
        /// USB charging
        /// </summary>
        Usb,
        /// <summary>
        /// Wireless charging
        /// </summary>
        Wireless,
        /// <summary>
        /// Other or unknown charging.
        /// </summary>
        Other
    }

    public abstract class BaseBatteryImplementation : IBattery, IDisposable
    {
        /// <summary>
        /// Current battery level (0 - 100)
        /// </summary>
        public abstract int RemainingChargePercent
        {
            get;
        }

        /// <summary>
        /// Current status of battery
        /// </summary>
        public abstract BatteryStatus Status
        {
            get;
        }

        /// <summary>
        /// Current charge type of battery
        /// </summary>
        public abstract PowerSource PowerSource
        {
            get;
        }



        /// <summary>
        /// When battery level changes
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnBatteryChanged(BatteryChangedEventArgs e)
        {
            if (BatteryChanged == null)
                return;

            BatteryChanged(this, e);
        }

        /// <summary>
        /// Event that fires when Battery status, level, power changes
        /// </summary>
        public event BatteryChangedEventHandler BatteryChanged;

        /// <summary>
        /// Dispose of class and parent classes
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose up
        /// </summary>
        ~BaseBatteryImplementation()
        {
            Dispose(false);
        }
        private bool disposed = false;
        /// <summary>
        /// Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    //dispose only
                }

                disposed = true;
            }
        }

    }
}
namespace Battery.Plugin
{
    /// <summary>
    /// Cross platform Battery implemenations
    /// </summary>
    public class CrossBattery
    {
        static Lazy<IBattery> Implementation = new Lazy<IBattery>(CreateBattery, System.Threading.LazyThreadSafetyMode.PublicationOnly);

        /// <summary>
        /// Current settings to use
        /// </summary>
        public static IBattery Current
        {
            get
            {
                var ret = Implementation.Value;
                if (ret == null)
                {
                    throw NotImplementedInReferenceAssembly();
                }
                return ret;
            }
        }

        static IBattery CreateBattery()
        {
#if PORTABLE
      return null;
#else
            return BatteryImplementation.GetDefault();
#endif
        }

        internal static Exception NotImplementedInReferenceAssembly()
        {
            return new NotImplementedException("This functionality is not implemented in the portable version of this assembly.  You should reference the NuGet package from your main application project in order to reference the platform-specific implementation.");
        }



        /// <summary>
        /// Dispose of everything 
        /// </summary>
        public static void Dispose()
        {
            if (Implementation == null || !Implementation.IsValueCreated) return;
            Implementation.Value.Dispose();
            Implementation = new Lazy<IBattery>(CreateBattery, System.Threading.LazyThreadSafetyMode.PublicationOnly);
        }
    }
}

namespace Battery.Plugin
{
    

    /// <summary>
    /// Provides information about the status of the device's battery.
    /// <para>Not supported for apps deployed via the public Windows Store.</para>
    /// </summary>
    public sealed class BatteryImplementation : BaseBatteryImplementation, IDisposable
    {
        private readonly IDisposable _subscriptionToSystem;
        private readonly IDisposable _timerSubscription;

        private static BatteryImplementation _default;
        /// <summary>
        /// Gets the default Battery object for the phone.
        /// </summary>
        /// <returns></returns>
        public static BatteryImplementation GetDefault()
        {
            return _default ?? (_default = new BatteryImplementation());
        }

        private BatteryImplementation()
        {
            _timerSubscription =
                Observable
                    .Interval(TimeSpan.FromMinutes(1))
                    .Select(_ => CrossBattery.Current.RemainingChargePercent)
                    .DistinctUntilChanged()
                    .Subscribe(_ => OnBatteryChanged(new BatteryChangedEventArgs
                    {
                        IsLow = RemainingChargePercent <= 15,
                        Status =Status,
                        PowerSource = PowerSource,
                        RemainingChargePercent = RemainingChargePercent
                    }));

            PowerModeChangedEventHandler handler = delegate
            {
                OnBatteryChanged(new BatteryChangedEventArgs
                {
                    IsLow = RemainingChargePercent <= 15,
                    Status = Status,
                    PowerSource =  PowerSource,
                    RemainingChargePercent = RemainingChargePercent
                });
            };

            SystemEvents.PowerModeChanged += handler;
            _subscriptionToSystem = Disposable.Create(() => SystemEvents.PowerModeChanged -= handler);
        }

        void IDisposable.Dispose()
        {
            base.Dispose();
            _subscriptionToSystem.Dispose();
            _timerSubscription.Dispose();
        }

        public override BatteryStatus Status
        {
            get
            {
                SYSTEM_POWER_STATUS status;
                GetSystemPowerStatus(out status);
                BatteryStatus batteryStatus;
                switch (status.BatteryFlag)
                {
                    case 1:
                    case 2:
                    case 4:
                        batteryStatus = BatteryStatus.Discharging;
                        break;
                    case 8:
                        batteryStatus = BatteryStatus.Charging;
                        break;
                    case 128:
                        batteryStatus = BatteryStatus.NotCharging;
                        break;
                    default:
                        batteryStatus = BatteryStatus.Unknown;
                        break;
                }
                if (status.BatteryLifePercent == 100)
                    batteryStatus = BatteryStatus.Full;
                if (batteryStatus == BatteryStatus.Unknown)
                    batteryStatus = status.ACLineStatus == 0
                        ? BatteryStatus.Discharging
                        : status.ACLineStatus == 1
                        ? BatteryStatus.Charging
                        : BatteryStatus.Unknown; 
                return batteryStatus;
            }
        }

        public override PowerSource PowerSource 
        {
            get
            {
                SYSTEM_POWER_STATUS status;
                GetSystemPowerStatus(out status);
                return status.ACLineStatus == 0 
                    ? PowerSource.Battery 
                    : status.ACLineStatus == 1
                    ? PowerSource.Ac
                    : PowerSource.Other;
            }
        }

        /// <summary>
        /// Gets a value that indicates the percentage of the charge remaining on the phone's battery.
        /// <para>Not supported for apps deployed via the public Windows Store.</para>
        /// </summary>
        /// <value>A value from 0 to 100 that indicates the percentage of the charge remaining on the phone's battery.</value>
        public override int RemainingChargePercent
        {
            get
            {
                SYSTEM_POWER_STATUS status;
                GetSystemPowerStatus(out status);
                return status.BatteryLifePercent;
            }
        }


        /// <summary>
        /// Gets a value that estimates how long is left until the device's battery is fully discharged.
        /// <para>Not supported for apps deployed via the public Windows Store.</para>
        /// </summary>
        public System.TimeSpan RemainingDischargeTime
        {
            get
            {
                SYSTEM_POWER_STATUS status;
                GetSystemPowerStatus(out status);
                var seconds = status.BatteryLifeTime;
                return seconds == -1 ? TimeSpan.Zero : TimeSpan.FromSeconds(seconds);
            }
        }

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool GetSystemPowerStatus(out SYSTEM_POWER_STATUS lpSystemPowerStatus);

        private struct SYSTEM_POWER_STATUS
        {
            public byte ACLineStatus;
            public byte BatteryFlag;
            public byte BatteryLifePercent;
            public byte SystemStatusFlag;            // set to 0
            public int BatteryLifeTime;
            public int BatteryFullLifeTime;
        }

    }
}