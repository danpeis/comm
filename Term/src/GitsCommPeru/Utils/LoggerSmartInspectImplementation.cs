﻿using System.Collections.Generic;
using gcperu.Contract;
using Gurock.SmartInspect;
using RxTools.IO;
using Splat;

namespace gcperu.Term.Utils
{
    public class LoggerSmartInspectImplementation : ILogger
    {
        private readonly Dictionary<string, Session> Sessions = new Dictionary<string, Session>(); 
        private readonly ConfigurationManager ConfigurationManager;
//        private readonly string Tag;
        private readonly object _gate = new object();

        public LoggerSmartInspectImplementation(string tag, string filename = "gcpl", LogFileRotateMode rotation = LogFileRotateMode.Daily)
        {
//            Tag = tag;
            Level = LogLevel.Info;
            SiAuto.Si.Connections = string.Format("tcp(), file(filename=\"{0}.sil\", rotate=\"{1}\")", filename, rotation.ToString().ToLowerInvariant());
            SiAuto.Si.Enabled = true;
        }

        public void Write(string message, LogLevel logLevel)
        {
//            var tagMatch = Regex.Match(message, "\\w+:\\s");
            if (Level == LogLevel.Debug)
                Locator.Current.GetService<IWriter>().Write(message);
            
            var messageSplit = message.Split(new[] {':'}, 2);
            var session = messageSplit[0];
            message = messageSplit[1].Trim();
            lock (_gate)
            {
                if (!Sessions.ContainsKey(session))
                    Sessions.Add(session, SiAuto.Si.AddSession(session));
                Sessions[session].LogString(logLevel.ToSmartInspectLevel(), session, message);
            }
        }

        public LogLevel Level
        {
            get
            {
                return SiAuto.Si.DefaultLevel.ToSplatLevel();
            }
            set
            {
                SiAuto.Si.DefaultLevel = value.ToSmartInspectLevel();
            }
        }
    }

    static class LevelLogEx
    {
        public static Level ToSmartInspectLevel(this LogLevel level)
        {
            return level.ToContractLevel().ToSmartInspectLevel();
        }

        public static Level ToSmartInspectLevel(this LevelLog level)
        {
            return (Level)level;
        }

        public static LevelLog ToContractLevel(this LogLevel level)
        {
            return (LevelLog) ((int) level);
        }

        public static LevelLog ToContractLevel(this Level level)
        {
            return (LevelLog)(SiAuto.Si.DefaultLevel == Level.Control ? Level.Fatal : SiAuto.Si.DefaultLevel);
        }

        public static LogLevel ToSplatLevel(this LevelLog level)
        {
            var intLevel = (int) level;
            return (LogLevel)(intLevel == 0 ? 1 : intLevel);
        }

        public static LogLevel ToSplatLevel(this Level level)
        {
            return level.ToContractLevel().ToSplatLevel();
        }
    }
}
