﻿using System;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Amcoex.Communications.Packaging;
using Amcoex.Communications.SignalR.Packaging;
using gcperu.Term;


namespace gcperu.Agents
{
    public static partial class SupplierAgent
    {
        public static IObservable<IPackageClient> SignalRPackageClientSupplier (this ConfigurationManager configManager)
        {
            System.Diagnostics.Contracts.Contract.Requires(configManager != null);
            BehaviorSubject<IPackageClient> packageSubject = null;
            var currentId = configManager.TerminalConfig.Behavior.Endpoint.FirstEndpoint;
            return Observable.Create<IPackageClient>(o =>
            {
//                if (state >= configManager.TerminalConfig.EndPoins.Count) // Count elements in preferences
//                    state = 0; // Restart counter
                if (packageSubject == null) // First subscription
                {
                    packageSubject = packageSubject ?? (packageSubject = 
                        new BehaviorSubject<IPackageClient>(new SignalRPackageClient("http://localhost:8080")));
                    return packageSubject.Subscribe(o);
                }

                return new CompositeDisposable(packageSubject.Subscribe(o),
                    Scheduler.CurrentThread.Schedule(currentId, (s, st) =>
                    {
                        var sortedPoints = configManager.TerminalConfig.EndPoins.ToList();
                        sortedPoints.Sort((point, endPoint) => point.Id - endPoint.Id);
                        if (sortedPoints.Count > 0)
                        {
                            try
                            {
                                // El primero que tenga un valor igual que el Id especificado o sea igual
                                packageSubject.OnNext(
                                    new SignalRPackageClient(sortedPoints.First(ep => ep.Id >= currentId).Url));
                                // Cambiamos el id al primer que sea superior
                                try
                                {
                                    currentId = sortedPoints.First(ep => ep.Id > currentId).Id;
                                }
                                catch
                                {
                                    currentId = 0;
                                }
                            }
                            catch
                            {
                                currentId = 0;
                                packageSubject.OnNext(packageSubject.Value);
                            }
                        } else
                        // Use state to generate the new value, this implementation supplies always the same packageclient
                            packageSubject.OnNext(packageSubject.Value);
                        return Disposable.Empty;
                    }));
            });
        }
    }
}
