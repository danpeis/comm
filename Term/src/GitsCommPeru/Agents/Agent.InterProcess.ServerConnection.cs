﻿using System;
using System.Diagnostics.CodeAnalysis;
using gcperu.Term.Enum;
using gcperu.Term.InterComm;

namespace gcperu.Term.Agents
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static partial class InterProcessAgent
    {
        private const string ServerConnectionStatusChannel = "ServerConnectionStateChange";

        public static IObservable<ServerConnectionEnum> ServerConnectionObservable
        {
            get
            {
                return ServerConnectionStatusChannel
                    .RegisterObservableOnChannel<ServerConnectionEnum>();
            }
        }

        public static IObserver<ServerConnectionEnum> ServerConnectionBroadcastObserver
        {
            get
            {
                return ServerConnectionStatusChannel
                    .GetOrRegisterBroadcastObserverOnChannel<ServerConnectionEnum>();
            }
        }
    }
}