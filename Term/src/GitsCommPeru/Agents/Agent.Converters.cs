﻿using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using Amcoex.Communications;
using Amcoex.Communications.Packaging.Package;
using gcperu.Contract.Frames;
using gcperu.Term.Enum;
using Newtonsoft.Json;

namespace gcperu.Term.Agents
{
    public static class ConverterAgent
    {
        public static IObservable<IFrame> ConvertPackageToJsonFrame(this IObservable<DataPackage> packageObservable)
        {
            return packageObservable.SelectMany(p =>
            {
                return Observable.Create<IFrame>(o =>
                {
                    JsonConvert
                        .DeserializeObject<IEnumerable<FrameJson>>(Encoding.UTF8.GetString(p.Data))
                        .ToObservable()
                        .Subscribe(o);
                    return Disposable.Empty;
                });
            });
        }

        public static IObservable<ServerConnectionEnum> ConvertStateChangeToServerConnectionEnum(
            this IObservable<StateChange> stateChange)
        {
            return stateChange.Select(sc =>
            {
                switch (sc.NewState)
                {
                    case ConnectionState.Connected:
                        return ServerConnectionEnum.Connected;
                    case ConnectionState.Connecting:
                    case ConnectionState.Reconnecting:
                        return ServerConnectionEnum.Connecting;
                    default:
                        return ServerConnectionEnum.Disconnected;
                }
            });
        } 
    }
}
