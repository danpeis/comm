﻿using System;
using System.Diagnostics.CodeAnalysis;
using gcperu.Term.InterComm;
using gcperu.Term.Models;

namespace gcperu.Term.Agents
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static partial class InterProcessAgent
    {
        private const string OdometerChannel = "OdometerChange";

        public static IObservable<Distance> OdometerObservable
        {
            get
            {
                return OdometerChannel
                    .RegisterObservableOnChannel<Distance>();
            }
        }

        public static IObserver<Distance> OdometerBroadcastObserver
        {
            get
            {
                return OdometerChannel
                    .GetOrRegisterBroadcastObserverOnChannel<Distance>(defaultWhenBufferOfOneElement:new Distance());
            }
        }
    }
}
