﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using gcperu.Term.Models;
using Geolocator.Plugin.Abstractions;
using RxTools;

namespace gcperu.Term.Agents
{
    public static class OdometerAgent
    {
        public static IObservable<Distance> CalculateDistanceBetweenPositionPairs(this IObservable<Position> source)
        {
            source.EnsureNotNull("source");

            return source
                .Buffer(2, 1)
                .Select(pair => new Distance(pair[0], pair[1]));
        }

        public static IObservable<Distance> ProgressiveSumOfDistances(this IObservable<Distance> source)
        {
            source.EnsureNotNull("source");
            return source.Scan(new Distance(0), (distanceSum, newDistance) => distanceSum.AddDistance(newDistance));
        }

        public static IObservable<Distance> InjectFirstDbDistanceIfAny(this IObservable<Distance> source)
        {
            var subject = new ReplaySubject<Distance>();
            var subjectSubscription = source.Subscribe(subject);

            return Observable.Create<Distance>(o =>
            {
                // Get from db and inject value
                // o.OnNext(firstValue)
                o.OnNext(new Distance());
                var samplingSubscription = subject
                    .Sample(TimeSpan.FromSeconds(10), TaskPoolScheduler.Default)
                    .Subscribe(d =>
                    {
                        //Save distance to db
                    });

                return new CompositeDisposable(
                    subject.Subscribe(o), 
                    subjectSubscription,
                    samplingSubscription);
            });
        }

        public static IObservable<Distance> Odometer(this IObservable<Position> source)
        {
            source.EnsureNotNull("source");
            return source
                .Where(p => p.Speed < 200.0)
                .RemoveDriftPositions()
                .InjectFirstDbPositionIfAny()
                .CalculateDistanceBetweenPositionPairs()
                .InjectFirstDbDistanceIfAny()
                .ProgressiveSumOfDistances();
        }
    }
}