﻿using System;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Amcoex.Communications;
using Amcoex.Communications.Packaging;
using Amcoex.Communications.Queue;
using Connectivity.Plugin;
using Connectivity.Plugin.Abstractions;
using RxTools;

namespace gcperu.Term.Agents
{
    public static partial class ConnectionAgent
    {
        public static Func<ConnectionTuple, bool> DefaultTriggerWatch =
                        i => i.Connectivity.IsConnected && i.ClientConnectionState.NewState == ConnectionState.Disconnected;

        #region CreatePackageClientOnConnectionFailure
        public static IObservable<IPackageClientQueue> CreatePackageClientOnConnectionFailure(
            this IObservable<IPackageClient> supplier,
            Func<IObservable<IPackageClient>, IObservable<ConnectionTuple>> connectionObserverFunc)
        {
            return CreatePackageClientOnConnectionFailure(supplier, connectionObserverFunc, DefaultTriggerWatch);
        }

        public static IObservable<IPackageClientQueue> CreatePackageClientOnConnectionFailure(
            this IObservable<IPackageClient> supplier)
        {
            return CreatePackageClientOnConnectionFailure(supplier, ConnectionTupleChanges, DefaultTriggerWatch);
        }

        public static IObservable<IPackageClientQueue> CreatePackageClientOnConnectionFailure(
            this IObservable<IPackageClient> supplier,
            Func<IObservable<IPackageClient>, IObservable<ConnectionTuple>> connectionObserverFunc,
            Func<ConnectionTuple, bool> triggerWatch)
        {
            connectionObserverFunc.EnsureNotNull("connectionObserverFunc");
            triggerWatch.EnsureNotNull("triggerWatch");

            var refCountedSupplier = supplier.Publish().RefCount();

            return Observable.Create<IPackageClientQueue>(o =>
                {
                    var connObservable = connectionObserverFunc(refCountedSupplier);

                    refCountedSupplier.Select(p => p.ToPackageClientQueue()).Subscribe(o);

                    connObservable
                        .Trigger(triggerWatch,
                            src => src,
                            src => src,Scheduler.Default)
                        .Subscribe(_ => supplier.Subscribe().Dispose());
                    return Disposable.Empty;
                });
        }

        public static IObservable<IPackageClient> PackageClientReconnectAttempts(
            this IObservable<IPackageClient> source, ConfigurationManager configManager)
        {
            // Vars
            ReplaySubject<IPackageClient> packageSubject = null;
            IPackageClient current = null;
            Func<ConnectionState, bool> reconnectingState = st => st == ConnectionState.Connecting || st == ConnectionState.Reconnecting;
            Func<StateChange, bool> connectedState = st => st.NewState == ConnectionState.Connected;
            Func<int> getRetryNumber = () => configManager.TerminalConfig.Behavior.Endpoint.RetryNumberByEndpoint;
            RefCountDisposable mainDisposable = null;
            IDisposable previousSubscriptions = Disposable.Empty;
            
            // Implementation
            return Observable.Create<IPackageClient>(o =>
            {
                if (packageSubject == null)
                {
                    packageSubject = new ReplaySubject<IPackageClient>(1);
                    var d1 = source.Subscribe(packageSubject);
                    var d2 = packageSubject.Subscribe(p =>
                    {
                        previousSubscriptions.Dispose();
                        var attemptsCounter = new BehaviorSubject<int>(-1);
                        var connectionObservableWithBounce = p.ConnectionChange.Throttle(TimeSpan.FromSeconds(1));
                        previousSubscriptions = new CompositeDisposable(
                            connectionObservableWithBounce.Where(st => st.NewState == ConnectionState.Disconnected && reconnectingState(st.OldState)).Subscribe(_ => attemptsCounter.OnNext(attemptsCounter.Value + 1)),
                            connectionObservableWithBounce.Where(connectedState).Subscribe(_ => attemptsCounter.OnNext(-1)),
                            attemptsCounter.Where(attempt => getRetryNumber() <= attempt).Subscribe(_ =>
                            {
                                attemptsCounter.OnNext(-1);
                                source.Subscribe().Dispose();
                            }));
                    });

                    mainDisposable = new RefCountDisposable(Disposable.Create(() =>
                    {
                        packageSubject.Dispose();
                        packageSubject = null;
                        d1.Dispose();
                        d2.Dispose();
                    }));
                }

                var returnDisposable = new CompositeDisposable(packageSubject.Subscribe(o), mainDisposable.GetDisposable());

                source.Subscribe().Dispose();

                return returnDisposable;
            });
        } 
        #endregion

        #region ConnectivityChanges
        public static IObservable<IConnectivity> ConnectivityChanges()
        {
            return Observable.Create<IConnectivity>(o =>
            {
                ConnectivityChangedEventHandler h = delegate
                {
                    o.OnNext(CrossConnectivity.Current);
                };

                CrossConnectivity.Current.ConnectivityChanged += h;

                return () => { CrossConnectivity.Current.ConnectivityChanged -= h; };
            });
        } 
        #endregion

        #region ConnectionTupleChanges
        public class ConnectionTuple
        {
            public ConnectionTuple(IConnectivity connectivity, StateChange clientConnectionState)
            {
                Connectivity = connectivity;
                ClientConnectionState = clientConnectionState;
            }

            public IConnectivity Connectivity { get; private set; }
            public StateChange ClientConnectionState { get; private set; }
        }

        public static IObservable<ConnectionTuple> ConnectionTupleChanges(IObservable<IPackageClient> packageObservable)
        {
            return Observable.Join(
                        ConnectivityChanges(),
                        packageObservable.Select(pO => pO.ConnectionChange).Switch(),
                        c => Observable.Never<Unit>(),
                        st => Observable.Never<Unit>(),
                        (c, st) => new ConnectionTuple(c, st));
        } 
        #endregion
    }
}
