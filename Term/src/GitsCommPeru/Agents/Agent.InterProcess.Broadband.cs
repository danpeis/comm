﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Amcoex.Broadband;
using gcperu.Term.InterComm;
using Splat;

namespace gcperu.Term.Agents
{
    [DataContract]
    public class BroadbandData
    {
        public BroadbandData(string imei, string imsi, string iccid, bool broadbandDeviceAvailable)
        {
            BroadbandDeviceAvailable = broadbandDeviceAvailable;
            IMSI = imsi;
            IMEI = imei;
            ICCID = iccid;
        }

        [DataMember]
        public bool BroadbandDeviceAvailable { get; private set; }
        [DataMember]
        public string IMEI { get; private set; }
        [DataMember]
        public string IMSI { get; private set; }
        [DataMember]
        public string ICCID { get; private set; }
    }

    public static class BroadbandAgent
    {
        private class BroadbandAgentData : IEnableLogger{}
        private static BroadbandAgentData BroadbandAgentDataTag = new BroadbandAgentData();

        public static IObservable<BroadbandData> GetCardData ()
        {
            return Observable.Create<BroadbandData>(o =>
            {
                var handler = new BroadbandMainHandler();
                // TODO: Guardar imei para posible segundo arranque sin imei ????
                if (!handler.HayDispositivoBandaAnchaMovil)
                {
                    BroadbandAgentDataTag.Log().Warn("No se encontro ningun dispositivo de broadband");
                    o.OnNext(new BroadbandData("", "", "", false));
                }
                else
                {
                    o.OnNext(new BroadbandData(
                        handler.Connection.CapabilityInfo.DeviceID,
                        handler.Connection.SubscriberInfo.IMSI,
                        handler.Connection.SubscriberInfo.ICCDID,
                        true
                        ));
                }
                o.OnCompleted();
                handler.Dispose();
                return Disposable.Empty;
            });
        } 
    }

    public static partial class InterProcessAgent
    {
        private const string BroadbandDataChannel = "BroadbandDataChannel";

        public static IObservable<BroadbandData> BroadbandDataObservable
        {
            get
            {
                return BroadbandDataChannel
                    .RegisterObservableOnChannel<BroadbandData>();
            }
        }

        public static IObserver<BroadbandData> BroadbandDataBroadcastObserver
        {
            get
            {
                return BroadbandDataChannel
                    .GetOrRegisterBroadcastObserverOnChannel<BroadbandData>();
            }
        }
    }
}
