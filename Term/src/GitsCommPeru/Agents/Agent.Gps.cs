﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Geolocator.Plugin;
using Geolocator.Plugin.Abstractions;

namespace gcperu.Term.Agents
{
    public static class GpsAgent
    {
        private static ISubject<Position> _subject;
        private static RefCountDisposable _refCountDisposable;
        private static readonly object Gate = new object();
        private static readonly EventHandler<PositionEventArgs> GpsHandler =
            delegate(object sender, PositionEventArgs args)
            {
                _subject.OnNext(args.Position);
            }; 

        public static IObservable<Position> GpsPositions(int minMilliseconds, int minMeters, bool includeHeading = false)
        {
            return Observable.Create<Position>(o =>
            {
                lock (Gate)
                {
                    if (_subject != null)
                        return new CompositeDisposable(_refCountDisposable.GetDisposable(), _subject.Subscribe(o));
                    _refCountDisposable = new RefCountDisposable(
                        Disposable.Create(() =>
                        {
                            lock (Gate)
                            {
                                CrossGeolocator.Current.StopListening();
                                CrossGeolocator.Current.PositionChanged -= GpsHandler;
                                _refCountDisposable.Dispose();
                                _refCountDisposable = null;
                                ((IDisposable)_subject).Dispose();
                                _subject = null;
                            } 
                        })
                    );
                    _subject = new Subject<Position>();
                    CrossGeolocator.Current.PositionChanged += GpsHandler;
                    CrossGeolocator.Current.StartListening(minMilliseconds, minMeters, includeHeading);
                    return new CompositeDisposable(_refCountDisposable.GetDisposable(), _subject.Subscribe(o));
                }
            });
        }

        
        public static IObservable<GeolocationError> GpsError()
        {
            return Observable.Create<GeolocationError>(o =>
            {
                EventHandler<PositionErrorEventArgs> handler = (sender, args) =>
                {
                    o.OnNext(args.Error);
                };

                CrossGeolocator.Current.PositionError += handler;
                return Disposable.Create(() => CrossGeolocator.Current.PositionError -= handler);
            });
        } 
    }
}
