﻿using System;
using System.Diagnostics.CodeAnalysis;
using gcperu.Term.Enum;
using gcperu.Term.InterComm;

namespace gcperu.Term.Agents
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static partial class InterProcessAgent
    {
        private const string BatteryStatusChannel = "BatteryStatusChange";

        public static IObservable<BatteryStatusEnum> BatteryStatusObservable
        {
            get
            {
                return BatteryStatusChannel
                    .RegisterObservableOnChannel<BatteryStatusEnum>();
            }
        }

        public static IObserver<BatteryStatusEnum> BatteryStatusBroadcastObserver
        {
            get
            {
                return BatteryStatusChannel
                    .GetOrRegisterBroadcastObserverOnChannel<BatteryStatusEnum>();
            }
        }
    }
}