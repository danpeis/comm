﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Geolocator.Plugin.Abstractions;

namespace gcperu.Term.Agents
{
    public static class GpsDriftAgent
    {
        public static IObservable<Position> RemoveDriftPositions(this IObservable<Position> source)
        {
            return source;
        }

        public static IObservable<Position> InjectFirstDbPositionIfAny(this IObservable<Position> source)
        {
			var subject = new ReplaySubject<Position>();
            var subscription = source.Subscribe(subject);

            return Observable.Create<Position>(o =>
            {
				// Get from db and inject value
				// o.OnNext(firstValue)
                
                var samplingSubscription = subject
                    .Sample(TimeSpan.FromSeconds(10), TaskPoolScheduler.Default)
                    .Subscribe(d =>
                    {
                        //Save position to db
                    });

                return new CompositeDisposable(
                    subject.Subscribe(o), 
                    subscription,
                    samplingSubscription);
            });
        } 
    }
}