﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Battery.Plugin;
using Battery.Plugin.Abstractions;
using gcperu.Term.Enum;

namespace gcperu.Term.Agents
{
    public static class BatteryAgent
    {
        private static RefCountDisposable _refCountBatteryStatusDisposable = null;
        private static BehaviorSubject<BatteryStatusEnum> _batteryStatusSubject;

        private static readonly BatteryChangedEventHandler BatteryStatusHandler = delegate(object sender, BatteryChangedEventArgs batteryArgs)
        {
            var batteryStatus = default(BatteryStatusEnum); // default: all bits to 0 (Level0)

            // Append battery critical status to enum (overlaps levels)
            if (batteryArgs.IsLow)
                batteryStatus = batteryStatus | BatteryStatusEnum.Critical;

            // Append battery power status to enum
            switch (batteryArgs.PowerSource)
            {
                case PowerSource.Ac:
                case PowerSource.Usb:
                case PowerSource.Wireless:
                    batteryStatus = batteryStatus | BatteryStatusEnum.Charging;
                    break;
                default:
                    batteryStatus = batteryStatus | BatteryStatusEnum.Discharging;
                    break;
            }

            // Append batterylevel data to enum
            switch ((int)Math.Truncate(batteryArgs.RemainingChargePercent / 25.0))
            {
                case 0: // Level: 0 <= x < 1
                    batteryStatus = batteryStatus | BatteryStatusEnum.Level0;
                    break;
                case 1: // Level: 1 <= x < 2
                    batteryStatus = batteryStatus | BatteryStatusEnum.Level1;
                    break;
                case 2: // Level: 2 <= x < 3
                    batteryStatus = batteryStatus | BatteryStatusEnum.Level2;
                    break;
                default:// Level: 3 <= x <= 4
                    batteryStatus = batteryStatus | BatteryStatusEnum.Level3;
                    break;
            }

            // Append battery unkown status to enum (overlaps all the other status)
            if (batteryArgs.Status == Battery.Plugin.Abstractions.BatteryStatus.Unknown)
                batteryStatus = batteryStatus | BatteryStatusEnum.Unknown;

            _batteryStatusSubject.OnNext(batteryStatus);
        };

        public static IObservable<BatteryStatusEnum> BatteryStatus()
        {
            return Observable.Create<BatteryStatusEnum>(o =>
            {
                if (_batteryStatusSubject == null)
                    _batteryStatusSubject = new BehaviorSubject<BatteryStatusEnum>(BatteryStatusEnum.Unknown);

                if (_refCountBatteryStatusDisposable == null || _refCountBatteryStatusDisposable.IsDisposed)
                {
                    BatteryStatusHandler(null, new BatteryChangedEventArgs
                    {
                        IsLow = CrossBattery.Current.RemainingChargePercent <= 15,
                        Status = CrossBattery.Current.Status,
                        PowerSource = CrossBattery.Current.PowerSource,
                        RemainingChargePercent = CrossBattery.Current.RemainingChargePercent
                    });
                    CrossBattery.Current.BatteryChanged += BatteryStatusHandler;

                    _refCountBatteryStatusDisposable = new RefCountDisposable(
                        Disposable.Create(() => CrossBattery.Current.BatteryChanged -= BatteryStatusHandler)
                    );
                }

                return new CompositeDisposable(_batteryStatusSubject.Subscribe(o), _refCountBatteryStatusDisposable.GetDisposable());
            });
        }
    }
}
