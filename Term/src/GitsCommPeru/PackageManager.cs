﻿using System;
using Amcoex.Communications.Queue;
using gcperu.Contract.Config;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using RxTools;
using RxTools.IO;

namespace gcperu.Term
{
    public class ConfigurationManager
    {
        private readonly object _gate = new object();

        private TerminalConfig _terminalConfig = new TerminalConfig();

        public TerminalConfig TerminalConfig
        {
            get {
                lock (_gate)
                {
                    return _terminalConfig;
                } 
            }
        }
    }


    public class PackageManager : ReactiveObject
    {
        [Reactive]
        public IPackageClientQueue PackageClientOpQueue { get; private set; }

        public PackageManager(IObservable<IPackageClientQueue> packageClientObservable)
        {
            packageClientObservable
                .Subscribe(i =>
            {
                PackageClientOpQueue = i;
                PackageClientOpQueue.AutoReconnectEnabled = true;
                PackageClientOpQueue.TryConnect();
            });
            this.WhenAnyObservable(m => m.PackageClientOpQueue.DataPackagesReceived)
                .Subscribe(_ => Console.WriteLine(_.Id));
        }
    }
}
