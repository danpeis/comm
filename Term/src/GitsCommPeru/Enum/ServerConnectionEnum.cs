﻿namespace gcperu.Term.Enum
{
    public enum ServerConnectionEnum
    {
        Disconnected,
        Connecting,
        ConnectedAndWaitingRegistration,
        ConnectedUnregistered,
        Connected,
    }
}