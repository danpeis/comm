﻿using System;

namespace gcperu.Term.Enum
{
    [Flags]
    public enum BatteryStatusEnum : byte
    {
        Level0 = 0,         // Ningun bit activo
        Level1 = 1,         // Primer bit activo:           Level0 + Level1 = Level1
        Level2 = 3,         // Primer dos bits activos:     Level1 + Level2 = Level2
        Level3 = 7,         // Primer tres bits activos:    Level2 + Level3 = Level3
        Critical = 15,      // Primeros cuatro bits activos:LevelX + Critical = Critical
        Charging = 16,      // Quinto bit activo            
        Discharging = 48,   // Quinto y sexto bit activos:  Charging + Discharging = Discharging
        Unknown = 63        // Todos los primeros siete bits activos: Any + Unknown = Unknown 
    }
}