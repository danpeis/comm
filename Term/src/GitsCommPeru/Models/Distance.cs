﻿using System.Runtime.Serialization;
using Geolocator.Plugin.Abstractions;

namespace gcperu.Term.Models
{
    [DataContract]
    public sealed class Distance
    {
        public Distance()
        {
            Meters = 0;
        }

        public Distance(double meters)
        {
            Meters = meters;
        }

        public Distance(Distance d)
        {
            Meters = d.Meters;
        }

        public Distance(Position p1, Position p2)
        {
            var firstPosition = new GeoFramework.Position(new GeoFramework.Latitude(p1.Latitude), new GeoFramework.Longitude(p1.Longitude));
            var secondPosition = new GeoFramework.Position(new GeoFramework.Latitude(p2.Latitude), new GeoFramework.Longitude(p2.Longitude));
            Meters = firstPosition.DistanceTo(secondPosition).ToMeters().Value;
        }

        [DataMember]
        public double Meters { get; private set; }

        public double Kilometers { get { return Meters / 1000.0; } }

        public Distance AddDistance(Distance distance)
        {
            return new Distance(distance.Meters + this.Meters);
        }
    }
}