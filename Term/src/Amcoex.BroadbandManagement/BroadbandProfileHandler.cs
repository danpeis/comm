﻿using System;
using MbnApi;

namespace Amcoex.Broadband
{
    public class BroadbandProfileHandler : IDisposable
    {
        private IMbnConnectionProfileManager _InterfaceCnxManager;
        private IMbnInterface _interface;

        private IMbnConnectionProfile[] _profiles;


        public BroadbandProfileHandler(IMbnInterface interf)
        {
            //ASIGNAMOS EVENTOS DE LA RADIO.
            try
            {
                _interface = interf;
                _InterfaceCnxManager = (IMbnConnectionProfileManager) new MbnConnectionProfileManager();

                _profiles= GetProfiles();
            }
            catch (Exception ex)
            {
//                TraceLog.LogException("BroadbandProfileHandler.Ctor.", ex);
            }
        }

        private IMbnConnectionProfile[] GetProfiles()
        {
            try
            {
                return _InterfaceCnxManager.GetConnectionProfiles(_interface);
            }
            catch (Exception exx)
            {
                //NO HAY PERFILES
                return null;
            }
        }

        public IMbnConnectionProfile[] Profiles
        {
            get { return _profiles ?? (_profiles = GetProfiles()); }
        }

        public void Dispose()
        {
        }
    }
}