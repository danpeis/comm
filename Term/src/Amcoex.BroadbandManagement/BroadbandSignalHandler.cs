﻿using System;
using MbnApi;

namespace Amcoex.Broadband
{
    public class BroadbandSignalHandler:IDisposable
    {
        public const int SIGNAL_LEVEL_ERROR = -1;

        private IMbnInterfaceManager _InterfaceManager;
        private IMbnInterface _interface;
        private IMbnSignal _isignal;
        private string _interfaceid;
        private IConnectionPoint _icp;
        private BroadbandSignalHandler.SignalEventsSink _eventsink;

        private int _signalnivel;

        public event EventHandler<SignalStateChangeEventArgs> SignalStateChange;

        public BroadbandSignalHandler(IMbnInterface interf)
        {
            //ASIGNAMOS EVENTOS DE LA RADIO.
            try
            {
                _InterfaceManager = (IMbnInterfaceManager)new MbnInterfaceManager();

                _interface = interf;
                _isignal = (IMbnSignal)_interface;

                IConnectionPointContainer icpc = (IConnectionPointContainer)_InterfaceManager;

                Guid IID_IMbnEvents = typeof(IMbnSignalEvents).GUID;
                icpc.FindConnectionPoint(ref IID_IMbnEvents, out _icp);

                uint cookie = 0;
                _eventsink = new BroadbandSignalHandler.SignalEventsSink(this);
                _icp.Advise(_eventsink, out cookie);
                _eventsink.Cookie = cookie;

                //Calcular estado;
                RefreshStatus(null);
            }
            catch (Exception ex)
            {
//                TraceLog.LogException("BroadbandSignalHandler.Ctor.", ex);
            }
        }

        public int Level
        {
            get { return GetSignalLevel(); }
        }

        private void RefreshStatus(IMbnSignal newInterface)
        {
            if (newInterface != null)
                _isignal = newInterface;

            try
            {
                int signalcurrent = _signalnivel;
                GetSignalLevel();

                if (signalcurrent!=_signalnivel)
                    OnStateChange(_signalnivel);
            }
            catch (Exception ex)
            {
            }
        }

        public int GetSignalLevel(bool reobtener=true)
        {
            try
            {
                if (reobtener)
                    _signalnivel = SignaValueTo100Scale(_isignal.GetSignalStrength());

                return _signalnivel;
            }
            catch (Exception ex)
            {
                return SIGNAL_LEVEL_ERROR;
            }
        }

        public static int SignaValueTo100Scale(uint signallevel)
        {
            if (signallevel == (int) MBN_SIGNAL_CONSTANTS.MBN_RSSI_UNKNOWN)
                return 0;

            if (signallevel == (int)MBN_SIGNAL_CONSTANTS.MBN_RSSI_DEFAULT)
                return 0;

            return ((int)signallevel*100)/29;
        }

        private void OnStateChange(int e)
        {
            EventHandler<SignalStateChangeEventArgs> handler = SignalStateChange;
            if (handler != null) handler(this, new SignalStateChangeEventArgs(e));
        }

        private void UnregisterEvent()
        {
            if (_eventsink == null || _icp == null)
                return;

            _icp.Unadvise(_eventsink.Cookie);

            _icp = null;
            _eventsink.Dispose();
            _eventsink = null;

        }

        public void Dispose()
        {
            UnregisterEvent();
        }

        private class SignalEventsSink : IMbnSignalEvents, IDisposable
        {
            private BroadbandSignalHandler _classcontrol;

            public SignalEventsSink(BroadbandSignalHandler radiocontrol)
            {
                _classcontrol = radiocontrol;
            }

            public uint Cookie { get; set; }

            public void Dispose()
            {
                _classcontrol = null;
            }

            public void OnSignalStateChange(IMbnSignal newInterface)
            {
                _classcontrol.RefreshStatus(newInterface);                
            }
        }

    }


}