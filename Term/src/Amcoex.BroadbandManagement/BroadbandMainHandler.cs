﻿using System;
using System.Linq;
using System.Threading;
using MbnApi;

namespace Amcoex.Broadband
{
    public class BroadbandMainHandler: IDisposable
    {
        private IMbnInterfaceManager _InterfaceManager;
        private IMbnConnectionManager _ConnectionManager;
        private IMbnInterface _interface;
        private string _interfaceid;

        private BroadbandRadioHandler _radiohld;
        private BroadbandRegistrationHandler _reghld;
        private BroadbandConnectionHandler _connhld;
        private BroadbandProfileHandler _profilehld;
        private BroadbandSignalHandler _signalhld;

        private bool _hayinterfacesBandaAnchaMovil;

        private MobileOperatorStatus _status;

        public BroadbandMainHandler()
        {
            _hayinterfacesBandaAnchaMovil = false;
            Initialize();
        }

        private void Initialize()
        {
            try
            {
                _InterfaceManager = (IMbnInterfaceManager)new MbnInterfaceManager();

                IMbnInterface[] minterfaces = _InterfaceManager.GetInterfaces();
                if (minterfaces.Count() == 0)
                    return;

                _hayinterfacesBandaAnchaMovil = true;

                _interface = minterfaces[0];
                _interfaceid = _interface.InterfaceID;

                //INICIALIZAMOS LOS OBJETOS QUE CONTROLAN CADA ASPECTO DE LA CONEXION.
                _radiohld = new BroadbandRadioHandler();
                _radiohld.StateChange += RadiohldStateChange;
                
                _reghld=new BroadbandRegistrationHandler(_interface);
                _reghld.StateChange += _reghld_StateChange;
                _reghld.StatusChange += Wwan_StatusChange;

                if (_radiohld.RadioOn)
                {
                    CreateConnecionHandler();
                }

                _profilehld = new BroadbandProfileHandler(_interface);

                _status = _reghld.Status;

                if (_reghld.Status!=MobileOperatorStatus.ok)
                    OnStatusChange(new StatusEventArgs(_reghld.Status));
                
            }
            catch (Exception ex)
            {
                
            }
        }

        void Wwan_StatusChange(object sender, StatusEventArgs e)
        {
            OnStatusChange(e);
        }


        #region Metodos

        private void CreateConnecionHandler()
        {
            if (_connhld != null)
            {
                _connhld.ConnectStateChange -= _connhld_ConnectStateChange;
                _connhld.ConnectChange -= _connhld_ConnectChange;
                _connhld.StatusChange -= Wwan_StatusChange;
                _connhld.Dispose();
            }

            _connhld = new BroadbandConnectionHandler(_interface);
            _connhld.ConnectStateChange += _connhld_ConnectStateChange;
            _connhld.ConnectChange += _connhld_ConnectChange;
            _connhld.StatusChange += Wwan_StatusChange;

            if (_signalhld != null)
            {
                _signalhld.SignalStateChange -= _signalhld_SignalStateChange;
                _signalhld.Dispose();
            }

            _signalhld = new BroadbandSignalHandler(_interface);
            _signalhld.SignalStateChange += _signalhld_SignalStateChange;
        }

        public ConexionResult Connectar(ProfileConnectionInfo accessdata,bool forceCreateConnectionhandler=false)
        {
            try
            {
                if (!_hayinterfacesBandaAnchaMovil)
                    return new ConexionResult(false, "No hay dispositivo banda ancha movil");

                if (!_radiohld.RadioOn)
                    return new ConexionResult(false,"LA RADIO ESTÁ APAGADA");

                if (_connhld == null || forceCreateConnectionhandler)
                {
                    CreateConnecionHandler();
                    if (_connhld==null)
                        return new ConexionResult(false,"NO SE HA PODIDO INICIAR EL MANEJADOR DE CONEXION");
                }

                if (_connhld.Conectado)
                    return new ConexionResult(true);

                if (ReadyState!=MBN_READY_STATE.MBN_READY_STATE_INITIALIZED)
                    return new ConexionResult(false, string.Format("EL DISPOSITIVO NO ESTÁ INICIALIZADO. ESTADO '{0}'", ReadyState.ToString().Replace("MBN_READY_STATE_","")), ReadyState);

                _connhld.Connect(accessdata);

                return new ConexionResult(true);
            }
            catch (Exception ex)
            {
                return new ConexionResult(false,ex.ToString());
            }

        }

        public ConexionResult Desconectar()
        {
            try
            {
                if (!_hayinterfacesBandaAnchaMovil)
                    return new ConexionResult(false, "No hay dispositivo banda ancha movil");

                if (!_radiohld.RadioOn)
                    return new ConexionResult(false, "LA RADIO ESTÁ APAGADA");

                if (!_connhld.Conectado)
                    return new ConexionResult(true);

                if (ReadyState != MBN_READY_STATE.MBN_READY_STATE_INITIALIZED)
                    return new ConexionResult(false, string.Format("EL DISPOSITIVO NO ESTÁ INICIALIZADO. ESTADO '{0}'", ReadyState), ReadyState);

                _connhld.Disconnect();

                return new ConexionResult(true);
            }
            catch (Exception ex)
            {
                return new ConexionResult(false, ex.ToString());
            }
        }

        /// <summary>
        /// ReActiva el adaptador de red para conexiones de Banda Ancha Movil.
        /// Desactva y vuelve a Activar el adaptador de red.
        /// </summary>
        /// <returns></returns>
        public static bool ReActivateAdaptadorWwan()
        {
            try
            {
                //obtenemos los adaptadores de Banda Ancha movil que haya.
                string filter = "banda";
                var adapters= NetworkAdapter.GetNetworkAdapters(filter);

                if (!adapters.Any())
                {
//                    TraceLog.LogWarning(string.Format("BroadbandMainHandler.ReActivateAdaptadorWwan. NO HAY ADAPTADOR DE RED QUE CUMPLAN EL CRITERIO. '{0}'", filter));
                    return true;
                }

                //1º DESACTIVAMOS.
                foreach (NetworkAdapter adapter in adapters)
                {
                    if (!string.IsNullOrEmpty(adapter.MACAddress))
                    {
                        adapter.Disable();
//                        TraceLog.LogDebug(string.Format("BroadbandMainHandler.ReActivateAdaptadorWwan. Desactivado adapter '{0}'", adapter.NetConnectionID));
                    }
                }

                Thread.Sleep(5000);

                //2º ACTIVAMOS. volvermos a obtenerlos, despues de desactivarlos.
                adapters = NetworkAdapter.GetNetworkAdapters(filter);
                foreach (NetworkAdapter adapter in adapters)
                {
                    adapter.Enable();
//                    TraceLog.LogDebug(string.Format("BroadbandMainHandler.ReActivateAdaptadorWwan. Activado adapter '{0}'", adapter.NetConnectionID));
                }

//                TraceLog.LogInformation("BroadbandMainHandler.ReActivateAdaptadorWwan. TODOS LOS ADAPTADORES REACTIVADOS CORRECTAMENTE.");

                return true;
            }
            catch (Exception ex)
            {
//                TraceLog.LogException("BroadbandMainHandler.ReActivateAdaptadorWwan. ERROR AL REACTIVAR EL ADAPTADOR.",ex);
                return false;
            }

        }

        #endregion


        #region Propiedades

        public MobileOperatorStatus Status
        {
            get { return _status; }
            private set { _status = value; }
        }

        public MBN_READY_STATE ReadyState
        {
            get
            {
                if (!_hayinterfacesBandaAnchaMovil)
                    return MBN_READY_STATE.MBN_READY_STATE_OFF;

                return _interface.GetReadyState();
            }
        }

        public static bool HayInterfacesBroadBandMobile
        {
            get
            {
                try
                {
                    var i = (IMbnInterfaceManager)new MbnInterfaceManager();
                    
                    IMbnInterface[] minterfaces = i.GetInterfaces();
                    return minterfaces.Any();
                }
                catch (Exception ex)
                {
//                    TraceLog.LogException("BroadbandMainHandler.HayInterfacesBroadBandMobile.",ex);
                    return false;
                }
            }
        }

        public bool HayDispositivoBandaAnchaMovil
        {
            get { return _hayinterfacesBandaAnchaMovil; }
        }

        public BroadbandRegistrationHandler Registration
        {
            get { return _reghld; }
        }

        public BroadbandProfileHandler Profile
        {
            get { return _profilehld; }
        }

        public BroadbandConnectionHandler Connection
        {
            get { return _connhld; }
        }

        public BroadbandRadioHandler Radio
        {
            get { return _radiohld; }
        }

        public BroadbandSignalHandler Signal
        {
            get
            {
                return _signalhld;
            }
        }

        public int SignalLevel(bool reobtener=false)
        {
            if (_signalhld == null)
                return BroadbandSignalHandler.SIGNAL_LEVEL_ERROR;

            return _signalhld.GetSignalLevel(reobtener);
        }

        public HomeProviderInfo HomeProviderInfo
        {
            get
            {
                if (_connhld == null)
                    return HomeProviderInfo.CreateHomeProviderUnknown();

                return _connhld.HomeProviderInfo;
            }
        }

        #endregion

        #region Eventos 

        public event EventHandler<ConnectionStateChangeEventArgs> ConnectStateChange;

        private void OnConnectStateChange(ConnectionStateChangeEventArgs e)
        {
            EventHandler<ConnectionStateChangeEventArgs> handler = ConnectStateChange;
            if (handler != null) handler(this, e);
        }

        void _connhld_ConnectStateChange(object sender, ConnectionStateChangeEventArgs e)
        {
            OnConnectStateChange(e);
        }

        public event EventHandler<ConnectionChangeEventArgs> ConnectChange;

        private void OnConnectChange(ConnectionChangeEventArgs e)
        {
            EventHandler<ConnectionChangeEventArgs> handler = ConnectChange;
            if (handler != null) handler(this, e);
        }

        void _connhld_ConnectChange(object sender, ConnectionChangeEventArgs e)
        {
            OnConnectChange(e);
        }

        public event EventHandler<RegisterStateChangeEventArgs> RegistrationStateChange;

        protected virtual void OnRegistrationStateChange(RegisterStateChangeEventArgs e)
        {
            EventHandler<RegisterStateChangeEventArgs> handler = RegistrationStateChange;
            if (handler != null) handler(this, e);
        }

        void _reghld_StateChange(object sender,RegisterStateChangeEventArgs e)
        {
            OnRegistrationStateChange(e);
        }

        public event EventHandler<SignalStateChangeEventArgs> SignaltStateChange;

        private void OnSignalStateChange(SignalStateChangeEventArgs e)
        {
            EventHandler<SignalStateChangeEventArgs> handler = SignaltStateChange;
            if (handler != null) handler(this, e);
        }

        void _signalhld_SignalStateChange(object sender, SignalStateChangeEventArgs e)
        {
            OnSignalStateChange(e);
        }

        public event EventHandler<StatusEventArgs> StatusChange;

        private void OnStatusChange(StatusEventArgs e)
        {
            EventHandler<StatusEventArgs> handler = StatusChange;
            if (handler != null) handler(this, e);
        }

        #endregion

        #region Refresh Metodos

        public void RefreshRadioState()
        {
            if (_radiohld.RadioOn)
            {
                CreateConnecionHandler();
            }
            else
            {
                if (_connhld != null)
                {
                    _connhld.Dispose();
                    _connhld = null;
                }

                if (_signalhld !=null)
                {
                    _signalhld.Dispose();
                    _signalhld = null;
                }
            }
        }

        #endregion

        #region Event Handler

        private void RadiohldStateChange(object sender, BoolEventArgs e)
        {
            RefreshRadioState();
        }

        #endregion

        public void Dispose()
        {
            if (_radiohld!=null)
            {
                _radiohld.Dispose();
                _radiohld = null;
            }

            if (_reghld!=null)
            {
                _reghld.Dispose();
                _reghld = null;
            }

            if (_connhld!=null)
            {
                _connhld.Dispose();
                _connhld = null;
            }

            if (_profilehld != null)
            {
                _profilehld.Dispose();
                _profilehld = null;
            }

            if (_signalhld != null)
            {
                _signalhld.Dispose();
                _signalhld = null;
            }
        }
    }

    #region Clases EventArgs

    public class StatusEventArgs : EventArgs
    {
        public StatusEventArgs(MobileOperatorStatus status)
        {
            Status = status;
            Message = "";
        }

        public StatusEventArgs(MobileOperatorStatus status, string message)
        {
            Status = status;
            Message = message;
        }

        public MobileOperatorStatus Status { get; set; }

        public string Message { get; set; }

        public StatusEventArgs()
        {
            Status=MobileOperatorStatus.Desconocido;
            Message = "";
        }
    }

    public class ConnectionChangeEventArgs : StatusEventArgs
    {
        public bool Conectado { get; set; }

        public ConnectionChangeEventArgs(bool conectado,MobileOperatorStatus status)
        {
            Conectado = conectado;
            Status = status;
        }
    }

    public class ConnectionStateChangeEventArgs : StatusEventArgs
    {
        public MBN_ACTIVATION_STATE State { get; set; }

        public ConnectionStateChangeEventArgs(MBN_ACTIVATION_STATE state)
        {
            State = state;
        }
    }

    public class RegisterStateChangeEventArgs : EventArgs
    {
        public MBN_REGISTER_STATE State { get; set; }

        public RegisterStateChangeEventArgs(MBN_REGISTER_STATE state)
        {
            State = state;
        }
    }

    public class SignalStateChangeEventArgs : EventArgs
    {
        public int Level { get; set; }

        public SignalStateChangeEventArgs(int level)
        {
            Level = level;
        }
    }

    public class BoolEventArgs : EventArgs
    {
        public bool On { get; set; }

        public BoolEventArgs(bool @on)
        {
            On = @on;
        }
    }

    #endregion
}