﻿using MbnApi;

namespace Amcoex.Broadband
{
    public class SubscriberInformation
    {
        internal SubscriberInformation(IMbnSubscriberInformation info)
        {
            IMSI = info.SubscriberID;
            ICCDID = info.SimIccID;
            TelephoneNumbers = info.TelephoneNumbers;

            if (TelephoneNumbers.GetUpperBound(0)<0)
                TelephoneNumbers = new string[1]{""};
        }

        public SubscriberInformation(string imsi, string iccdid)
        {
            IMSI = imsi;
            ICCDID = iccdid;
            TelephoneNumbers = new string[1] { "" };
        }

        public string IMSI { get; set; }
        public string ICCDID { get; set; }
        public string[] TelephoneNumbers { get; set; }

        public override string ToString()
        {
            return string.Format("IMSI:{0} ICCID:{1} TelephoneNumbers:{2}", IMSI, ICCDID, TelephoneNumbers[0]);
        }
    }

    public class HomeProviderInfo
    {
        public const string PROVIDER_UNKNOWN = "Desconocido";

        public HomeProviderInfo(MBN_PROVIDER provider)
        {
                this.ProviderID = provider.providerID;
                this.ProviderName = provider.providerName;
                this.ProviderState = provider.providerState;
                this.DataClass = provider.dataClass;
        }

        public HomeProviderInfo()
        {
        }

        public string ProviderID { get; set; }
        public uint ProviderState { get; set; }
        public string ProviderName { get; set; }
        public uint DataClass { get; set; }

        public bool IsProviderUnknown
        {
            get { return ProviderID == PROVIDER_UNKNOWN; }
        }

        public override string ToString()
        {
            return string.Format("ProviderID:{0} ProviderName:{1} State:{2} DataClass:{3}", ProviderID, ProviderName, ProviderState, DataClass);
        }

        public static HomeProviderInfo CreateHomeProviderUnknown()
        {
            MBN_PROVIDER homeerror;
            homeerror.dataClass = 0;
            homeerror.providerID = "Desconocido";
            homeerror.providerName = "Desconocido";
            homeerror.providerState = 0;

            return new HomeProviderInfo(homeerror);
        }
    }

    public class CapabilityInfo
    {
        public CapabilityInfo(MBN_INTERFACE_CAPS caps)
        {
            this.CellularClass = caps.cellularClass;
            this.VoiceClass = caps.voiceClass;
            this.DataClass = caps.dataClass;
            this.CustomDataClass = caps.customDataClass;
            this.GsmBandClass = caps.gsmBandClass;
            this.CdmaBandClass = caps.cdmaBandClass;
            this.CustomBandClass = caps.customBandClass;
            this.SmsCaps = caps.smsCaps;
            this.ControlCaps = caps.controlCaps;
            this.DeviceID = caps.deviceID;
            this.Manufacturer = caps.manufacturer;
            this.Model = caps.model;
            this.FirmwareInfo = caps.firmwareInfo;
        }

        public CapabilityInfo()
        {
            CellularClass=MBN_CELLULAR_CLASS.MBN_CELLULAR_CLASS_NONE;
            VoiceClass=MBN_VOICE_CLASS.MBN_VOICE_CLASS_NONE;
            DataClass = 0;
            CustomBandClass = "";
            CdmaBandClass = 0;
            CustomDataClass = "";
            SmsCaps = 0;
            ControlCaps = 00;
            DeviceID = "";
            Manufacturer = "";
            Model = "";
            FirmwareInfo = "";
        }

        public MBN_CELLULAR_CLASS CellularClass { get; set; }
        public MBN_VOICE_CLASS VoiceClass { get; set; }
        public uint DataClass { get; set; }
        public string CustomDataClass { get; set; }
        public uint GsmBandClass { get; set; }
        public uint CdmaBandClass { get; set; }
        public string CustomBandClass { get; set; }
        public uint SmsCaps { get; set; }
        public uint ControlCaps { get; set; }
        public string DeviceID { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string FirmwareInfo { get; set; }

        public override string ToString()
        {
            return string.Format("Manufacturer:{0} Model: {1} DeviceID:{2} CellularClass:{3} VoiceClass:{4} DataClass:{5} GsmBand:{6} CdmaBanda:{7} Sms:{8}", Manufacturer, Model, DeviceID,
                                 CellularClass, VoiceClass, DataClass, GsmBandClass, CdmaBandClass, SmsCaps);
        }
    }

    public class ResultProcess
    {
        public ResultProcess(bool result)
        {
            Result = result;
            Motivo = "";
        }

        public ResultProcess(bool result, string motivo)
        {
            Result = result;
            Motivo = motivo;
        }

        public string Motivo { get; set; }
        public bool Result { get; set; }

    }

    public class ConexionResult: ResultProcess
    {
        public ConexionResult(bool result, string motivo) : base(result, motivo)
        {
        }

        public ConexionResult(bool result, string motivo,MBN_READY_STATE readyState)
            : base(result, motivo)
        {
        }

        public ConexionResult(bool result)
            : base(result)
        {
        }

        public MBN_READY_STATE ReadyState { get; set; }
    }
}