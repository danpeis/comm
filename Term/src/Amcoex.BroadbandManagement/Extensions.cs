﻿using System;

namespace Amcoex.Broadband
{
    static class Extensions
    {
        public static T NumToEnum<T, Tparam>(this Tparam number, T defaultvalue) where Tparam : struct
        {
            try
            {
                return (T)Enum.ToObject(typeof(T), number);
            }
            catch
            {
                return defaultvalue;
            }
        }
    }
}
