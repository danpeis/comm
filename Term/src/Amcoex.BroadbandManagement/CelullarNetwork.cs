﻿using System;

namespace Amcoex.Broadband
{
    public class Imsi
    {
        private string _value;

        public Imsi()
        {
        }

        public Imsi(string imsi)
        {
            if (string.IsNullOrEmpty(imsi))
                return;

            _value = imsi;

            Calculate();
        }

        private void Calculate()
        {
            Mnc = "";
            Mcc = "";
            MSin = "";

            if (string.IsNullOrEmpty(_value))
            {
                Valido = false;
                return;
            }

            Valido = _value.Length >= 15;

            if (Valido)
                DesglosarPartes(_value);
        }

        private void DesglosarPartes(string valor)
        {
            Mcc = valor.Substring(0, 3);

            switch (Mcc)
            {
                case "366": //DOMINICA
                case "310": //Unidates States
                case "405": //indica (Aircel)
                case "338": //Jamaica
                case "334": //Mexico
                case "467": //Corea Norte
                case "356": //Saint Kits and Nevis
                case "365": //Anguilla (United Kingdom)
                case "722": //Argentina
                case "342": //Barbados
                case "348": //British Virgin Islands (United Kingdom)
                case "302": //Canada
                case "346": //Cayman Islands (United Kingdom)
                case "732": //Colombia

                    Mnc = string.Format("{0}", valor.Substring(3, 3));
                    MSin = valor.Substring(6);

//                    TraceLog.LogDebug(string.Format("IMSI. {0}. CALCULO DE PARTES. ESTE MCC '{1}' TIENE 3 DIGITOS", valor, Mcc));

                    break;

                default:
                    //UTILIZAN 3 (MNC)
                    Mnc = string.Format("0{0}", valor.Substring(3, 2));
                    MSin = valor.Substring(5);

//                    TraceLog.LogDebug(string.Format("IMSI. {0}. CALCULO DE PARTES. ESTE MCC '{1}' TIENE 2 DIGITOS", valor, Mcc));

                    break;
            }
        }

        public bool Valido { get; private set; }

        public string Value
        {
            get { return _value; }
            set
            {
                if (_value==value)
                    return;

                _value = value;
                Calculate();
//                TraceLog.Main.Watch("IMSI",ToString());
            }
        }

        public string Mnc { get; private set; }

        public string Mcc { get; private set; }

        public string ProviderId
        {
            get { return Mcc + Mnc; }
        }
        
        public string MSin { get; private set; }

        public override string ToString()
        {
            return Valido ? string.Format("{0}.{1}.{2}, {3}", Mcc, Mnc, MSin, Value) : string.Format("NO VALIDO. Valor={0}", Value);
        }
    }

    public class ProfileConnectionInfo : IComparable<ProfileConnectionInfo>
    {
        public ProfileConnectionInfo(string nombre, string dialupPhoneNumber, string apn, string user, string password,ProfileNetworkType type,string profileID)
        {
            Nombre = nombre;
            DialupPhoneNumber = dialupPhoneNumber;
            APN = apn;
            User = user;
            Password = password;
            Tipo = type;
            ProfileID = profileID;
        }

        public ProfileConnectionInfo(string nombre)
        {
            Nombre = nombre;
        }

        public string Nombre { get; set; }

        public string DialupPhoneNumber { get; set; }
        public string APN { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Comentario { get; set; }

        public ProfileNetworkType Tipo { get; set; }

        /// <summary>
        /// Indica que esta configuración es Forzada a utilizar por el Servidor.
        /// </summary>
        public bool ForceUse { get; set; }

        /// <summary>
        /// Perfil Mobil al que pertenecen estos datos de conexion
        /// </summary>
        public string ProfileID { get; set; }

        public int CompareTo(ProfileConnectionInfo other)
        {
            if (this.DialupPhoneNumber == other.DialupPhoneNumber && this.APN == other.APN && this.User == other.User && this.Password == other.Password)
                return 0;
            
            return 1;
        }

        public override string ToString()
        {
            return string.Format("Nombre:{0} DialupPhoneNumber:{1} APN:{2} User:{3} Password:{4} Tipo:{5} Profile:{6} Coment:{7}", Nombre, DialupPhoneNumber, APN, User, Password, Tipo, ProfileID,Comentario);
        }

        #region Metodos

        /// <summary>
        /// Ejecutar tareas de inicialización del Modem, como asignar el comando de inicialización para asignar la APN a utilizar.
        /// </summary>
        public void ExecuteTaskInitialize()
        {
        }

        #endregion


       
    }
}