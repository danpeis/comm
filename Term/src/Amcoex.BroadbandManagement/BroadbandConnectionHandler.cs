﻿using System;
using System.Linq;
using System.Xml.Linq;
using MbnApi;

namespace Amcoex.Broadband
{
    public class BroadbandConnectionHandler : IDisposable
    {
        #region fields

        private IMbnInterfaceManager _InterfaceManager;
        private IMbnConnectionManager _iconnmanager;
        private IMbnInterface _interface;
        private IMbnConnection _iconn;
        private string _interfaceid;
        private IConnectionPoint _icp;
        private BroadbandConnectionHandler.ConnectionEventsSink _eventsink;

        private SubscriberInformation _subscriberinfo;
        private HomeProviderInfo _homeprovider;
        private CapabilityInfo _capability;

        private MBN_REGISTER_MODE _mode;
        private MBN_ACTIVATION_STATE _state;
        private MBN_DATA_CLASS _dataclass;

        private MobileOperatorStatus _status;

        #endregion

        public BroadbandConnectionHandler(IMbnInterface interf)
        {
            try
            {
                _interface = interf;

                _iconnmanager = (IMbnConnectionManager) new MbnConnectionManager();

                if (GetConnnection(interf))
                {
                    //Calcular estado
                    RefreshStatus(null);
                }

            }
            catch (Exception ex)
            {
//                MessageBox.Show(ex.ToString());
            }
        }

        #region Metodos

        private bool GetConnnection(IMbnInterface interf)
        {
            try
            {
                _subscriberinfo = GetSubscriberInfo(interf);
                _homeprovider = GetMobileProvider(interf);
                _capability = GetCapability(interf);
                _iconn = _interface.GetConnection();
               
                IConnectionPointContainer icpc = (IConnectionPointContainer)_iconnmanager;

                Guid IID_IMbnEvents = typeof (IMbnConnectionEvents).GUID;
                icpc.FindConnectionPoint(ref IID_IMbnEvents, out _icp);

                uint cookie = 0;
                _eventsink = new ConnectionEventsSink(this);
                _icp.Advise(_eventsink, out cookie);
                _eventsink.Cookie = cookie;

                return true;
            }
            catch (Exception ex)
            {
                UnregisterEvent();
                return false;
            }
        }

        private SubscriberInformation GetSubscriberInfo(IMbnInterface interf)
        {
            try
            {
                //MBN_PROVIDER homeprovider = interf.GetHomeProvider();
                return new SubscriberInformation(interf.GetSubscriberInformation());
            }
            catch (Exception ex)
            {
                return new SubscriberInformation("", "");
            }
        }

        public HomeProviderInfo GetMobileProvider(IMbnInterface interf)
        {
            try
            {
                return new HomeProviderInfo((interf ?? _interface).GetHomeProvider());
            }
            catch (Exception ex)
            {
                return HomeProviderInfo.CreateHomeProviderUnknown();
            }
        }

        private CapabilityInfo GetCapability(IMbnInterface interf)
        {
            try
            {
                return new CapabilityInfo(interf.GetInterfaceCapability());
            }
            catch (Exception ex)
            {
                return new CapabilityInfo(interf.GetInterfaceCapability());
            }
        }

        private void UnregisterEvent()
        {
            if (_eventsink==null || _icp==null)
                return;

            _icp.Unadvise(_eventsink.Cookie);
            
            _icp = null;
            _eventsink.Dispose();
            _eventsink = null;

        }

        private void SetConnectStateChange(IMbnConnection connection)
        {
            RefreshStatus(connection);
            OnConnectStateChange(_state);
        }

        public void Connect(ProfileConnectionInfo data)
        {

            string xmlprofile = "";

            try
            {
                if (_iconn==null || _subscriberinfo==null || _homeprovider==null)
                {
                    if (_interface == null)
                        return;

                    if (!GetConnnection(_interface))
                        return;
                }

                //xmlprofile = CreateProfileXML(_homeprovider.ProviderName, _subscriberinfo.IMSI,_subscriberinfo.ICCDID, true, data, ConnectionMode.defecto);
                xmlprofile = ConstructProfileXml(_subscriberinfo.IMSI, data);

                uint requestid = 0;
                _iconn.Connect(MBN_CONNECTION_MODE.MBN_CONNECTION_MODE_TMP_PROFILE, xmlprofile, out requestid);

//                TraceLog.Main.LogColored(Level.Debug, Color.DarkSeaGreen, string.Format("WWan.Connect. ** OK ** .\nProfileUse\n{0}", xmlprofile));

            }
            catch (Exception ex)
            {
//                TraceLog.Main.LogColored(Level.Debug, Color.DarkSeaGreen, string.Format("WWan.Connect. ** ERROR ** .\nProfileUse\n{0}\n\n{1}", xmlprofile,ex));

                var st = new StatusEventArgs(ex.HResult.NumToEnum(MobileOperatorStatus.Desconocido), ex.Message);
                _status = st.Status;

                OnStatusChange(st);
                throw;
            }

        }

        public void Disconnect()
        {
            try
            {
                if (_iconn == null)
                {
                    if (_interface == null)
                        return;

                    if (!GetConnnection(_interface))
                        return;
                }

                uint requestid = 0;
                _iconn.Disconnect(out requestid);
            }
            catch (Exception ex)
            {
//                TraceLog.LogException("WwanConnectionHandler.Disconnect.", ex);

                var st = new StatusEventArgs(ex.HResult.NumToEnum(MobileOperatorStatus.Desconocido), ex.Message);
                _status = st.Status;

                OnStatusChange(st);
            }
        }

        public void RefreshStatus(IMbnConnection newInterface)
        {
            if (newInterface != null)
                _iconn = newInterface;

            if (_iconn == null)
                return;

            try
            {
                if ((_subscriberinfo == null || _homeprovider == null) && _interface != null)
                {
                    _subscriberinfo = GetSubscriberInfo(_interface);
                    _homeprovider = GetMobileProvider(_interface);
                }

                String profilename = "";
                _iconn.GetConnectionState(out _state, out profilename);
//                TraceLog.Main.LogColored(Level.Debug, Color.DarkSalmon, string.Format("WwanConnection.RefreshStatus. STATE:{0}", _state));
            }
            catch (Exception ex)
            {
//                TraceLog.LogException("WwanConnectionHandler.RefreshStatus.", ex);

                var st = new StatusEventArgs(ex.HResult.NumToEnum(MobileOperatorStatus.Desconocido), ex.Message);
                _status = st.Status;

                OnStatusChange(st);
            }
        }

        #endregion

        #region Properties
        public MobileOperatorStatus Status
        {
            get { return _status; }
            private set { _status = value; }
        }

        public bool Conectado
        {
            get { return _state == MBN_ACTIVATION_STATE.MBN_ACTIVATION_STATE_ACTIVATED; }
            private set
            {
                _state = value ? MBN_ACTIVATION_STATE.MBN_ACTIVATION_STATE_ACTIVATED : MBN_ACTIVATION_STATE.MBN_ACTIVATION_STATE_DEACTIVATED;
                OnConnectChange(new ConnectionChangeEventArgs(value,Status));
                OnConnectStateChange(_state);
            }
        }

        public MBN_ACTIVATION_STATE ConnectionState
        {
            get { return _state; }
        }

        public SubscriberInformation SubscriberInfo
        {
            get { return _subscriberinfo; }
        }

        public CapabilityInfo CapabilityInfo
        {
            get { return _capability; }
        }

        public HomeProviderInfo HomeProviderInfo
        {
            get { return _homeprovider ?? HomeProviderInfo.CreateHomeProviderUnknown(); }
        }

        #endregion

        #region Events

        public event EventHandler<ConnectionStateChangeEventArgs> ConnectStateChange;
        public event EventHandler<ConnectionChangeEventArgs> ConnectChange;

        private void OnConnectStateChange(MBN_ACTIVATION_STATE e)
        {
            EventHandler<ConnectionStateChangeEventArgs> handler = ConnectStateChange;
            if (handler != null) handler(this, new ConnectionStateChangeEventArgs(e));
        }

        private void OnConnectChange(ConnectionChangeEventArgs e)
        {
            EventHandler<ConnectionChangeEventArgs> handler = ConnectChange;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<StatusEventArgs> StatusChange;

        private void OnStatusChange(StatusEventArgs e)
        {
            EventHandler<StatusEventArgs> handler = StatusChange;
            if (handler != null) handler(this, e);
        }

        #endregion

        public void Dispose()
        {
            UnregisterEvent();
        }

        private string CreateProfileXML(string providername, string subscriberid, string iccdid, bool autoConnectInternet, ProfileConnectionInfo accessdata, ConnectionMode mode = ConnectionMode.defecto)
        {
            var xdoc = XDocument.Load("Resources/MbnProfile.xml");


            foreach (var xNode in xdoc.Descendants())
            {
                switch (xNode.Name.LocalName)
                {
                    case "Name":
                        xNode.Value = providername;
                        break;
                    case "SubscriberID":
                        xNode.Value = subscriberid;
                        break;
                    case "SimIccID":
                        xNode.Value = iccdid;
                        break;
                    case "HomeProviderName":
                        xNode.Value = providername;
                        break;
                    case "AutoConnectOnInternet":
                        xNode.Value = autoConnectInternet.ToString().ToLower();
                        break;
                    case "ConnectionMode":
                        switch (mode)
                        {
                            case ConnectionMode.defecto:
                            case ConnectionMode.autohome:
                                xNode.Value = "auto-home";
                                break;
                            default:
                                xNode.Value = mode.ToString();
                                break;
                        }
                        break;
                    case "Context":

                        if (xNode.HasElements)
                        {
                            if (accessdata != null)
                            {
                                foreach (var subnode in xNode.Descendants())
                                {
                                    switch (subnode.Name.LocalName)
                                    {
                                        case "AccessString":
                                            subnode.Value = accessdata.APN;
                                            break;
                                        case "UserLogonCred":
                                            var elements = subnode.Descendants();

                                            elements.ElementAt(0).Value = accessdata.User;
                                            elements.ElementAt(1).Value = accessdata.Password;
                                            break;

                                    }

                                }
                            }
                            else
                            {
                                xNode.RemoveAll();
                            }
                        }

                        break;


                }
            }

            return string.Format("<?xml version=\"1.0\"?>\r\n{0}", xdoc);

        }

        private string ConstructProfileXml(string subscriberId, ProfileConnectionInfo accessdata)
        {
            // Connect using any temporary profile name
            string name = "tempProfile";

            // {0} is the profile name
            // {1} is the subscriber id
            // {2} is the APN
            // {3} username if available
            // {4} password if available
            //
            string profileXml = "<?xml version=\"1.0\"?>\r\n" + String.Format(@"<MBNProfile xmlns=""http://www.microsoft.com/networking/WWAN/profile/v1"">
    <Name>{0}</Name>
    <IsDefault>false</IsDefault>
    <SubscriberID>{1}</SubscriberID>
    <Context>
        <AccessString>{2}</AccessString>
        <UserLogonCred>
            <UserName>{3}</UserName>
            <Password>{4}</Password>
        </UserLogonCred>
    </Context>
</MBNProfile>", name, subscriberId, accessdata.APN, accessdata.User, accessdata.Password, "http://www.microsoft.com/networking/WWAN/profile/v1");

            return profileXml;
        }

        private class ConnectionEventsSink : IMbnConnectionEvents, IDisposable
        {
            private BroadbandConnectionHandler _classcontrol;

            public ConnectionEventsSink(BroadbandConnectionHandler radiocontrol)
            {
                _classcontrol = radiocontrol;
            }

            public uint Cookie { get; set; }

            public void OnSetRegisterModeComplete(IMbnConnection newInterface, uint requestID, int status)
            {
                _classcontrol.Status = status.NumToEnum(MobileOperatorStatus.Desconocido);
                _classcontrol.RefreshStatus(newInterface);
            }

            public void Dispose()
            {
                _classcontrol = null;
            }

            public void OnConnectComplete(IMbnConnection newConnection, uint requestID, int status)
            {
                _classcontrol.Status = status.NumToEnum(MobileOperatorStatus.Desconocido);
                _classcontrol.Conectado = (status == 0);

            }

            public void OnDisconnectComplete(IMbnConnection newConnection, uint reqeustID, int status)
            {
                _classcontrol.Status = status.NumToEnum(MobileOperatorStatus.Desconocido);
                _classcontrol.Conectado = false;
            }

            public void OnConnectStateChange(IMbnConnection newConnection)
            {
                _classcontrol.SetConnectStateChange(newConnection);
            }

            public void OnVoiceCallStateChange(IMbnConnection newConnection)
            {
            }
        }
    }

}