﻿using System;
using MbnApi;

namespace Amcoex.Broadband
{
    public class BroadbandRegistrationHandler: IDisposable
    {
        private IMbnInterfaceManager _InterfaceManager;
        private IMbnInterface _interface;
        private IMbnRegistration _iregis;
        private string _interfaceid;
        private IConnectionPoint _icp;
        private BroadbandRegistrationHandler.RegistrationEventsSink _eventsink;

        private MBN_REGISTER_MODE _mode;
        private MBN_REGISTER_STATE _state;
        private MBN_DATA_CLASS _dataclass;

        private MobileOperatorStatus _status;

        public BroadbandRegistrationHandler(IMbnInterface interf)
        {
            //ASIGNAMOS EVENTOS DE LA RADIO.
            try
            {
                _InterfaceManager = (IMbnInterfaceManager)new MbnInterfaceManager();

                _interface = interf;
                _iregis = (IMbnRegistration)_interface;

                IConnectionPointContainer icpc = (IConnectionPointContainer)_InterfaceManager;

                Guid IID_IMbnRadioEvents = typeof(IMbnRegistrationEvents).GUID;
                icpc.FindConnectionPoint(ref IID_IMbnRadioEvents, out _icp);

                uint cookie = 0;
                _eventsink = new BroadbandRegistrationHandler.RegistrationEventsSink(this);
                _icp.Advise(_eventsink, out cookie);
                _eventsink.Cookie = cookie;

                //Calcular estado;
                RefreshStatus(null);
            }
            catch (Exception ex)
            {
//                TraceLog.LogException("BroadbandRegistrationHandler.Ctor.", ex);
                _status = ex.HResult.NumToEnum(MobileOperatorStatus.Desconocido);
            }
        }

        public void Dispose()
        {
            if (_eventsink == null || _icp == null)
                return;

            _icp.Unadvise(_eventsink.Cookie);

            _icp = null;
            _eventsink.Dispose();
            _eventsink = null;
        }

        public MobileOperatorStatus Status
        {
            get { return _status; }
            private set { _status = value; }
        }

        public bool Registered
        {
            get { return _state == MBN_REGISTER_STATE.MBN_REGISTER_STATE_HOME; }
        }

        private void RefreshStatus(IMbnRegistration newInterface)
        {
            if (newInterface!=null)
                _iregis = newInterface;

            try
            {
                _mode= _iregis.GetRegisterMode();
                _state = _iregis.GetRegisterState();

                _status=MobileOperatorStatus.ok;

                OnStateChange(_state);
            }
            catch (Exception ex)
            {
                var st = new StatusEventArgs(ex.HResult.NumToEnum(MobileOperatorStatus.Desconocido), ex.Message);
                _status = st.Status;

                OnStatusChange(st);
            }
        }

        public MBN_REGISTER_STATE RegisterState
        {
            get { return _state; }
        }

        public MBN_REGISTER_MODE RegisterMode
        {
            get { return _mode; }
        }

        public MBN_DATA_CLASS CurrentDataClass
        {
            get { return _dataclass; }
        }

        public event EventHandler<RegisterStateChangeEventArgs> StateChange;

        private void OnStateChange(MBN_REGISTER_STATE e)
        {
            EventHandler<RegisterStateChangeEventArgs> handler = StateChange;
            if (handler != null) handler(this, new RegisterStateChangeEventArgs(e));
        }

        public event EventHandler<StatusEventArgs> StatusChange;

        private void OnStatusChange(StatusEventArgs e)
        {
            EventHandler<StatusEventArgs> handler = StatusChange;
            if (handler != null) handler(this, e);
        }

        private class RegistrationEventsSink : IMbnRegistrationEvents, IDisposable
        {
            private BroadbandRegistrationHandler _classcontrol;

            public RegistrationEventsSink(BroadbandRegistrationHandler classcontrol)
            {
                _classcontrol = classcontrol;
            }

            public uint Cookie { get; set; }

            public void OnRegisterModeAvailable(IMbnRegistration newInterface)
            {
            }

            public void OnRegisterStateChange(IMbnRegistration newInterface)
            {
                _classcontrol.RefreshStatus(newInterface);
            }

            public void OnPacketServiceStateChange(IMbnRegistration newInterface)
            {
            }

            public void OnSetRegisterModeComplete(IMbnRegistration newInterface, uint requestID, int status)
            {
                _classcontrol.Status = status.NumToEnum(MobileOperatorStatus.Desconocido);
                _classcontrol.RefreshStatus(newInterface);
            }

            public void Dispose()
            {
                _classcontrol = null;
            }
        }
        
    }


}