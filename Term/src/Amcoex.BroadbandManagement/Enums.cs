namespace Amcoex.Broadband
{
    public enum GsmMode
    {
        /// <summary>
        /// Indica que el modem tiene una conexi�n gprs v�lida y permite enviar tramas
        /// </summary>
        Connected=1,
        /// <summary>
        /// Indica que el modem no tiene una conexi�n gprs v�lida y no puede enviar tramas
        /// </summary>
        Disconnected= 2
    }

    public enum GsmQuality
    {
        /// <summary>
        /// Indica que la calidad de la cobertura es ideal
        /// </summary>
        QualityIdeal = 80,
        /// <summary>
        /// Indica que la calidad de la cobertura es excelente
        /// </summary>
        QualityExcellent = 40,
        /// <summary>
        /// Indica que la calidad de la cobertura es buena
        /// </summary>
        QualityGood = 30,
        /// <summary>
        /// Indica que la calidad de la cobertura es moderada
        /// </summary>
        QualityModerate = 15,
        /// <summary>
        /// Indica que la calidad de la cobertura es justita
        /// </summary>
        QualityFair = 8,
        /// <summary>
        /// Indica que la calidad de la cobertura es mala, pobre
        /// </summary>
        QualityPoor = 0,

    }

    public enum ProfileNetworkType
    {
        Default=0,
        Generic=1,
        Especific=2,
        Forzada = 3
    }


    public enum MobileOperatorRegistratationMode : byte
    {
        Automatic = 0,
        Manual = 1,
        ManualyAutomatic = 4,
        Unknonw = 99
    }

    public enum MobileOperatorFormatName : byte
    {
        LongName = 0,
        ShortName = 1,
        Numeric = 2,
        Unknonw = 99
    }

    public enum MobileOperatorRegistrationStatus : byte
    {
        /// <summary>
        /// Not registered, ME is not searching for a new operator to register with
        /// </summary>
        NoRegistered = 0,
        /// <summary>
        /// Registered, home network
        /// </summary>
        Registered = 1,
        /// <summary>
        /// Not registered, but ME is searching for a new operator to register with
        /// </summary>
        NoRegistered_SearchingNewOperator = 2,
        /// <summary>
        /// Registration denied
        /// </summary>
        RegistrationDenied = 3,
        Unknown = 4,
        /// <summary>
        /// Registrado pero utilizando Roaming
        /// </summary>
        Registered_Roaming = 5

    }

    public enum MobileOperatorStatus
    {
        ok=0,
        Sim_No_insertada = -2141945334,
        Sim_Invalida= -2141945342,
        PIN_Requerido = -2141945328,
        PIN_Esta_Desactivado=-2141945327,
        PIN_es_no_Soportado=-2141945329,

        /// <summary>
        /// The network service subscription has expired.
        /// </summary>
        Servicio_No_Activado = -2141945335,
        /// <summary>
        /// Provider is not visible.
        /// </summary>
        Proveedor_No_Visible = -2141945337,
        Proveedores_No_Encontrados=-2141945330,
        Proveedor_Cache_Invalida=-2141945332,

        /// <summary>
        /// Access point name (APN) or Access string is incorrect.
        /// </summary>
        APN_No_Valido = -2141945340,
        /// <summary>
        /// The specified network user/password is not correct.
        /// </summary>
        Credenciales_Invalidas = -86,
        /// <summary>
        /// Voice call in progress.
        /// </summary>
        Llamada_Voz_EnProceso = -2141945333,
        /// <summary>
        /// Max activated contexts have reached.
        /// </summary>
        Contextos_Maximo_Nivel_Alcanzado = -2141945339,
        Contexto_No_Activado=-2141945343,
        /// <summary>
        /// Radio is powered off.
        /// </summary>
        ModoAvion_Activo = -2141945336,
        /// <summary>
        /// No active attached packet service is available.
        /// </summary>
        Dispositivo_EnEstado_PacketDetach = -2141945338,
        Dispositivo_No_Esta_Registrado=-2141945331,
        Fallo_Generico = -2141945326,
        Perfil_Conexion_Es_Invalido = -2141945320,
        Perfil_Defecto_Exite = -2141945319,

        Estado_Invalido=5023,
        No_Soportado=50,
        Argumentos_Invalidos=-2147024809,
        
        /// <summary>
        /// No hay disponibles en el Sistema, ninguna interfaz de Red Movil
        /// </summary>
        No_Hay_Interfaz_Conexion=-9998,

        Desconocido=-9999,
    }
}