﻿using System;
using System.Threading;
using System.Linq;
using MbnApi;

namespace Amcoex.Broadband
{
    public class BroadbandRadioHandler: IDisposable
    {
        private IMbnInterfaceManager _InterfaceManager;
        private IMbnInterface _interface;
        private IMbnRadio _iradio;
        private string _interfaceid;
        private IConnectionPoint _icp;
        private RadioEventsSink _eventsink;

        private MBN_RADIO _state;

        public event EventHandler<BoolEventArgs> StateChange;

        private void OnStateChange(bool e)
        {
            EventHandler<BoolEventArgs> handler = StateChange;
            if (handler != null) handler(this, new BoolEventArgs(e));
        }

        public BroadbandRadioHandler()
        {
              //ASIGNAMOS EVENTOS DE LA RADIO.
            try
            {
                _InterfaceManager = (IMbnInterfaceManager)new MbnInterfaceManager();

                IMbnInterface[] minterfaces = _InterfaceManager.GetInterfaces();
                if (minterfaces.Count()==0)
                    return;

                _interface = minterfaces[0];
                _interfaceid = _interface.InterfaceID;
                _iradio = (IMbnRadio) _interface;

                IConnectionPointContainer icpc = (IConnectionPointContainer)_InterfaceManager;

                Guid IID_IMbnRadioEvents = typeof(IMbnRadioEvents).GUID;
                icpc.FindConnectionPoint(ref IID_IMbnRadioEvents, out _icp);

                uint cookie = 0;
                _eventsink = new RadioEventsSink(this);
                _icp.Advise(_eventsink, out cookie);
                _eventsink.Cookie = cookie;

                //Calcular estado;
                _state = _iradio.SoftwareRadioState;
            }
            catch (Exception ex)
            {
//                TraceLog.LogException("BroadbandRadioHandler.Ctor.",ex);
            }
        }

        private void SetRadioInterface(IMbnRadio radiointerface)
        {
            _interface = (IMbnInterface) radiointerface;
            _iradio = radiointerface;

            _state = _iradio.SoftwareRadioState;

            OnStateChange(RadioOn);
        }

        public bool RadioOn
        {
            get { return _state == MBN_RADIO.MBN_RADIO_ON; }
            set
            {
                uint requestid = 0;
                if (value==(RadioOn))
                    return;

//                TraceLog.LogError(string.Format("Radio asignar estado={0}", value));
                _iradio.SetSoftwareRadioState(value ? MBN_RADIO.MBN_RADIO_ON : MBN_RADIO.MBN_RADIO_OFF, out requestid);
            }
        }

        public bool ApagarRadio()
        {
            try
            {
                if (RadioOn)
                    return true;

                RadioOn = false;
                for (int i = 0; i < 3; i++)
                {
                    Thread.Sleep(5000);
                    if (!RadioOn)
                        break;
                }
                if (RadioOn)
                {
//                    TraceLog.LogError("BroadbandRadioHandler.ApagarRadio. NO SE HA PODIDO APAGAR LA RADIO.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
//                TraceLog.LogException("BroadbandRadioHandler. ERROR INTENTAR ACTIVAR LA RADIO DEL MODEM.", ex);
                return false;
            }
        }

        public bool ActivarRadio()
        {
            try
            {
                if (RadioOn)
                    return true;

                RadioOn = true;

                for (int i = 0; i < 3; i++)
                {
                    Thread.Sleep(5000);
                    if (RadioOn)
                        break;
                }
                if (!RadioOn)
                {
//                    TraceLog.LogError("BroadbandRadioHandler.ActivarRadio. NO SE HA PODIDO ACTIVAR LA RADIO.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
//                TraceLog.LogException("BroadbandRadioHandler. ERROR INTENTAR ACTIVAR LA RADIO DEL MODEM.", ex);
                return false;
            }
        }

        public void Dispose()
        {
            if (_eventsink == null || _icp == null)
                return;

            _icp.Unadvise(_eventsink.Cookie);

            _icp = null;
            _eventsink.Dispose();
            _eventsink = null;
        }

        private class RadioEventsSink : IMbnRadioEvents,IDisposable
        {
            private BroadbandRadioHandler _classcontrol;

            public RadioEventsSink(BroadbandRadioHandler radiocontrol)
            {
                _classcontrol = radiocontrol;
            }

            public uint Cookie { get; set; }

            public void OnRadioStateChange(IMbnRadio newInterface)
            {
            }

            public void OnSetSoftwareRadioStateComplete(IMbnRadio newInterface, uint requestID, int status)
            {
                _classcontrol.SetRadioInterface(newInterface);
            }

            public void Dispose()
            {
                _classcontrol = null;
            }
        }
    }
}