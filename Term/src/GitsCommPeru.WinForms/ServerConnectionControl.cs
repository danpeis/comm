﻿using System;
using System.Reactive;
using System.Reactive.Subjects;
using System.Windows.Forms;
using gcperu.Term.Enum;
using gcperu.Term.UI.Properties;

namespace gcperu.Term.UI
{
    public partial class ServerConnectionControl : UserControl, IObserver<ServerConnectionEnum>
    {
        private IDisposable _subscription = null;

        private readonly ISubject<ServerConnectionEnum> _serverConnectionInternalSubject
            = new BehaviorSubject<ServerConnectionEnum>(ServerConnectionEnum.Disconnected);

        public IObserver<ServerConnectionEnum> ServerConnectionObserver { 
            get { return _serverConnectionInternalSubject.AsObserver(); } 
        } 

        public ServerConnectionControl()
        {
            InitializeComponent();

            HandleCreated += (sender, args) =>
            {
                if (_subscription != null)
                    _subscription.Dispose();
                
                _subscription = _serverConnectionInternalSubject
                    .Subscribe(incomingStatus =>
                {
                    switch (incomingStatus)
                    {
                        case ServerConnectionEnum.Disconnected:
                            Picture.Image = Resources.CirculoRojo48;
                            break;
                        case ServerConnectionEnum.Connecting:
                            Picture.Image = Resources.connection;
                            break;
                        case ServerConnectionEnum.Connected:
                            Picture.Image = Resources.CirculoVerde48;
                            break;
                        case ServerConnectionEnum.ConnectedAndWaitingRegistration:
                            Picture.Image = Resources.ConnectionWait;
                            break;
                        default: // Unregistered
                            Picture.Image = Resources.CirculoNaranja48;
                            break;
                    }
                });
            };

            HandleDestroyed += (sender, args) =>
            {
                _subscription.Dispose();
            };
        }

        public void OnNext(ServerConnectionEnum value)
        {
            _serverConnectionInternalSubject.OnNext(value);
        }

        public void OnError(Exception error)
        {
            _serverConnectionInternalSubject.OnError(error);
        }

        public void OnCompleted()
        {
            _serverConnectionInternalSubject.OnCompleted();
        }
    }
}
