using System;
using DevExpress.XtraEditors;

namespace GITSNavega.UI.UserControls.Pantalla
{
    public class PantallaControl : XtraUserControl
    {

//        protected PantallaArgs _args;
//
//        #region EventRaising
//
//        public event EventHandler<PantallaAccionEventArgs> PantallaAccion;
//
//        protected void OnPantallaAccion(PantallaAccionEventArgs e)
//        {
//            EventHandler<PantallaAccionEventArgs> handler = PantallaAccion;
//            if (handler != null) handler(this, e);
//        }
//
//        public event EventHandler<PantallaResultEventArgs> PantallaResult;
//
//        protected void OnPantallaResult(PantallaResultEventArgs e)
//        {
//            EventHandler<PantallaResultEventArgs> handler = PantallaResult;
//            if (handler != null) handler(this, e);
//        }

//        #endregion

//        public virtual void Initialize()
//        {
//            _args = null;
//        }
//
//        public virtual void Initialize(PantallaArgs args)
//        {
//            _args = args;
//        }

        /// <summary>
        /// Ocurre cuando una pantalla obtiene el Foco y se convierte en Activa
        /// </summary>
        public virtual void Activate()
        {

        }

        /// <summary>
        /// Ocurre cuando una pantalla pierde el Foco y se convierte en Inactiva
        /// </summary>
        public virtual void Deactivate()
        {

        }

        public virtual void RePaint()
        {
        }

//        public PantallaArgs Args
//        {
//            get { return _args; }
//        }

        public virtual string Titulo { get; private set; }

        public virtual bool IsPantallDetalle { get { return false; } }

//        public virtual PantallaTipo Tipo { get; private set; }

        /// <summary>
        /// Indica si hay un Refresco del Panel pendiente de Realizar
        /// </summary>
        public bool RefreshPanelPending { get; set; }
    }

    public class PantallaArgs
    {
//        public ServicioInfo Servicio { get; set; }
//        public IncidenciaInfo Incidencia { get; set; }

//        public PantallaArgs(ServicioInfo servicio)
//        {
//            Servicio = servicio;
//        }

//        public PantallaArgs(IncidenciaInfo incidencia)
//        {
//            Incidencia = incidencia;
//        }
    }
}