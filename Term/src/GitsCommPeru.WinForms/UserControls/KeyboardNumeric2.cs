﻿using System;
using System.Reactive.Subjects;
using System.Windows.Forms;

namespace gcperu.Term.UI.UserControls
{
    public partial class KeyboardNumeric2 : UserControl, IObservable<KeyPressEventArgs>
    {
        private readonly Subject<KeyPressEventArgs> _keyPressSubject = new Subject<KeyPressEventArgs>(); 

        public KeyboardNumeric2()
        {
            InitializeComponent();

            EventHandler keyPressDelegate = (sender, args) =>
            {
                var button = (Button)sender;
                _keyPressSubject.OnNext(new KeyPressEventArgs(button.Text[0]));
            };

            this.HandleCreated += (sender, args) =>
            {
                button0.Click += keyPressDelegate;
                button1.Click += keyPressDelegate;
                button2.Click += keyPressDelegate; 
                button3.Click += keyPressDelegate;
                button4.Click += keyPressDelegate;
                button5.Click += keyPressDelegate;
                button6.Click += keyPressDelegate;
                button7.Click += keyPressDelegate;
                button8.Click += keyPressDelegate;
                button9.Click += keyPressDelegate;
            };

            this.HandleDestroyed += (sender, args) =>
            {
                button0.Click -= keyPressDelegate;
                button1.Click -= keyPressDelegate;
                button2.Click -= keyPressDelegate;
                button3.Click -= keyPressDelegate;
                button4.Click -= keyPressDelegate;
                button5.Click -= keyPressDelegate;
                button6.Click -= keyPressDelegate;
                button7.Click -= keyPressDelegate;
                button8.Click -= keyPressDelegate;
                button9.Click -= keyPressDelegate;
            };
        }

        public IDisposable Subscribe(IObserver<KeyPressEventArgs> observer)
        {
            return _keyPressSubject.Subscribe(observer);
        }
    }
}
