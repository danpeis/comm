namespace GITSNavega.UI.UserControls
{
    partial class KeyboardNumeric
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KeyboardNumeric));
            this.fr2 = new DevExpress.XtraEditors.PanelControl();
            this.bt6 = new DevExpress.XtraEditors.SimpleButton();
            this.bt5 = new DevExpress.XtraEditors.SimpleButton();
            this.bt4 = new DevExpress.XtraEditors.SimpleButton();
            this.fr3 = new DevExpress.XtraEditors.PanelControl();
            this.t9 = new DevExpress.XtraEditors.SimpleButton();
            this.b = new DevExpress.XtraEditors.SimpleButton();
            this.bt7 = new DevExpress.XtraEditors.SimpleButton();
            this.fr4 = new DevExpress.XtraEditors.PanelControl();
            this.btBack = new DevExpress.XtraEditors.SimpleButton();
            this.bt0 = new DevExpress.XtraEditors.SimpleButton();
            this.btEnter = new DevExpress.XtraEditors.SimpleButton();
            this.bt1 = new DevExpress.XtraEditors.SimpleButton();
            this.bt2 = new DevExpress.XtraEditors.SimpleButton();
            this.bt3 = new DevExpress.XtraEditors.SimpleButton();
            this.fr1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.fr2)).BeginInit();
            this.fr2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fr3)).BeginInit();
            this.fr3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fr4)).BeginInit();
            this.fr4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fr1)).BeginInit();
            this.fr1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fr2
            // 
            this.fr2.Controls.Add(this.bt6);
            this.fr2.Controls.Add(this.bt5);
            this.fr2.Controls.Add(this.bt4);
            this.fr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.fr2.Location = new System.Drawing.Point(0, 60);
            this.fr2.Name = "fr2";
            this.fr2.Size = new System.Drawing.Size(301, 60);
            this.fr2.TabIndex = 2;
            // 
            // bt6
            // 
            this.bt6.AllowFocus = false;
            this.bt6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bt6.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt6.Appearance.Options.UseFont = true;
            this.bt6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.bt6.Location = new System.Drawing.Point(200, 0);
            this.bt6.Name = "bt6";
            this.bt6.Size = new System.Drawing.Size(100, 60);
            this.bt6.TabIndex = 3;
            this.bt6.Text = "6";
            this.bt6.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // bt5
            // 
            this.bt5.AllowFocus = false;
            this.bt5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bt5.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt5.Appearance.Options.UseFont = true;
            this.bt5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.bt5.Location = new System.Drawing.Point(100, 0);
            this.bt5.Name = "bt5";
            this.bt5.Size = new System.Drawing.Size(100, 60);
            this.bt5.TabIndex = 2;
            this.bt5.Text = "5";
            this.bt5.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // bt4
            // 
            this.bt4.AllowFocus = false;
            this.bt4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bt4.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt4.Appearance.Options.UseFont = true;
            this.bt4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.bt4.Location = new System.Drawing.Point(0, 0);
            this.bt4.Name = "bt4";
            this.bt4.Size = new System.Drawing.Size(100, 60);
            this.bt4.TabIndex = 1;
            this.bt4.Text = "4";
            this.bt4.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // fr3
            // 
            this.fr3.Controls.Add(this.t9);
            this.fr3.Controls.Add(this.b);
            this.fr3.Controls.Add(this.bt7);
            this.fr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.fr3.Location = new System.Drawing.Point(0, 120);
            this.fr3.Name = "fr3";
            this.fr3.Size = new System.Drawing.Size(301, 60);
            this.fr3.TabIndex = 3;
            // 
            // t9
            // 
            this.t9.AllowFocus = false;
            this.t9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.t9.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.t9.Appearance.Options.UseFont = true;
            this.t9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.t9.Location = new System.Drawing.Point(200, 0);
            this.t9.Name = "t9";
            this.t9.Size = new System.Drawing.Size(100, 60);
            this.t9.TabIndex = 3;
            this.t9.Text = "9";
            this.t9.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // b
            // 
            this.b.AllowFocus = false;
            this.b.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.b.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b.Appearance.Options.UseFont = true;
            this.b.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.b.Location = new System.Drawing.Point(100, 0);
            this.b.Name = "b";
            this.b.Size = new System.Drawing.Size(100, 60);
            this.b.TabIndex = 2;
            this.b.Text = "8";
            this.b.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // bt7
            // 
            this.bt7.AllowFocus = false;
            this.bt7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bt7.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt7.Appearance.Options.UseFont = true;
            this.bt7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.bt7.Location = new System.Drawing.Point(0, 0);
            this.bt7.Name = "bt7";
            this.bt7.Size = new System.Drawing.Size(100, 60);
            this.bt7.TabIndex = 1;
            this.bt7.Text = "7";
            this.bt7.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // fr4
            // 
            this.fr4.Controls.Add(this.btBack);
            this.fr4.Controls.Add(this.bt0);
            this.fr4.Controls.Add(this.btEnter);
            this.fr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.fr4.Location = new System.Drawing.Point(0, 180);
            this.fr4.Name = "fr4";
            this.fr4.Size = new System.Drawing.Size(301, 62);
            this.fr4.TabIndex = 4;
            // 
            // btBack
            // 
            this.btBack.AllowFocus = false;
            this.btBack.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btBack.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btBack.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btBack.Appearance.Options.UseBackColor = true;
            this.btBack.Appearance.Options.UseFont = true;
            this.btBack.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btBack.Image = ((System.Drawing.Image)(resources.GetObject("btBack.Image")));
            this.btBack.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btBack.Location = new System.Drawing.Point(200, 0);
            this.btBack.Name = "btBack";
            this.btBack.Size = new System.Drawing.Size(100, 60);
            this.btBack.TabIndex = 3;
            this.btBack.Click += new System.EventHandler(this.ButtonActionClick);
            // 
            // bt0
            // 
            this.bt0.AllowFocus = false;
            this.bt0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bt0.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt0.Appearance.Options.UseFont = true;
            this.bt0.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.bt0.Location = new System.Drawing.Point(100, 0);
            this.bt0.Name = "bt0";
            this.bt0.Size = new System.Drawing.Size(100, 60);
            this.bt0.TabIndex = 2;
            this.bt0.Text = "0";
            this.bt0.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // btEnter
            // 
            this.btEnter.AllowFocus = false;
            this.btEnter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btEnter.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btEnter.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnter.Appearance.Options.UseBackColor = true;
            this.btEnter.Appearance.Options.UseFont = true;
            this.btEnter.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btEnter.Image = global::gcperu.Term.UI.Properties.Resources.Ok2;
            this.btEnter.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btEnter.Location = new System.Drawing.Point(0, 0);
            this.btEnter.Name = "btEnter";
            this.btEnter.Size = new System.Drawing.Size(100, 60);
            this.btEnter.TabIndex = 1;
            this.btEnter.Click += new System.EventHandler(this.ButtonActionClick);
            // 
            // bt1
            // 
            this.bt1.AllowFocus = false;
            this.bt1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.bt1.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt1.Appearance.Options.UseFont = true;
            this.bt1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.bt1.Location = new System.Drawing.Point(0, 0);
            this.bt1.Name = "bt1";
            this.bt1.Size = new System.Drawing.Size(100, 60);
            this.bt1.TabIndex = 1;
            this.bt1.Text = "1";
            this.bt1.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // bt2
            // 
            this.bt2.AllowFocus = false;
            this.bt2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.bt2.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt2.Appearance.Options.UseFont = true;
            this.bt2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.bt2.Location = new System.Drawing.Point(100, 0);
            this.bt2.Name = "bt2";
            this.bt2.Size = new System.Drawing.Size(100, 60);
            this.bt2.TabIndex = 2;
            this.bt2.Text = "2";
            this.bt2.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // bt3
            // 
            this.bt3.AllowFocus = false;
            this.bt3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.bt3.Appearance.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt3.Appearance.Options.UseFont = true;
            this.bt3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.bt3.Location = new System.Drawing.Point(200, 0);
            this.bt3.Name = "bt3";
            this.bt3.Size = new System.Drawing.Size(100, 60);
            this.bt3.TabIndex = 3;
            this.bt3.Text = "3";
            this.bt3.Click += new System.EventHandler(this.ButtonNumericClick);
            // 
            // fr1
            // 
            this.fr1.Controls.Add(this.bt3);
            this.fr1.Controls.Add(this.bt2);
            this.fr1.Controls.Add(this.bt1);
            this.fr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.fr1.Location = new System.Drawing.Point(0, 0);
            this.fr1.Name = "fr1";
            this.fr1.Size = new System.Drawing.Size(301, 60);
            this.fr1.TabIndex = 1;
            // 
            // KeyboardNumeric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fr4);
            this.Controls.Add(this.fr3);
            this.Controls.Add(this.fr2);
            this.Controls.Add(this.fr1);
            this.Name = "KeyboardNumeric";
            this.Size = new System.Drawing.Size(301, 265);
            this.Resize += new System.EventHandler(this.KeyboardNumeric_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.fr2)).EndInit();
            this.fr2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fr3)).EndInit();
            this.fr3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fr4)).EndInit();
            this.fr4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fr1)).EndInit();
            this.fr1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl fr2;
        private DevExpress.XtraEditors.SimpleButton bt6;
        private DevExpress.XtraEditors.SimpleButton bt5;
        private DevExpress.XtraEditors.SimpleButton bt4;
        private DevExpress.XtraEditors.PanelControl fr3;
        private DevExpress.XtraEditors.SimpleButton t9;
        private DevExpress.XtraEditors.SimpleButton b;
        private DevExpress.XtraEditors.SimpleButton bt7;
        private DevExpress.XtraEditors.PanelControl fr4;
        private DevExpress.XtraEditors.SimpleButton btBack;
        private DevExpress.XtraEditors.SimpleButton bt0;
        private DevExpress.XtraEditors.SimpleButton btEnter;
        private DevExpress.XtraEditors.SimpleButton bt1;
        private DevExpress.XtraEditors.SimpleButton bt2;
        private DevExpress.XtraEditors.SimpleButton bt3;
        private DevExpress.XtraEditors.PanelControl fr1;
    }
}
