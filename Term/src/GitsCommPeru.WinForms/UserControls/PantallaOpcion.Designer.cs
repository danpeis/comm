namespace GITSNavega.UI.UserControls.Pantalla
{
    partial class PantallaOpcion
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaOpcion));
            this.ckAdminLogin = new DevExpress.XtraEditors.CheckEdit();
            this.cmdExecHelpOnLine = new DevExpress.XtraEditors.SimpleButton();
            this.btInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btOption = new DevExpress.XtraEditors.SimpleButton();
            this.btTareas = new DevExpress.XtraEditors.SimpleButton();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPagSistema = new DevExpress.XtraTab.XtraTabPage();
            this.gcTrace = new DevExpress.XtraEditors.GroupControl();
            this.cmdUploadLog = new DevExpress.XtraEditors.SimpleButton();
            this.cmdConexRed = new DevExpress.XtraEditors.SimpleButton();
            this.cmdHideTaskBar = new DevExpress.XtraEditors.SimpleButton();
            this.cmdShowTaskBar = new DevExpress.XtraEditors.SimpleButton();
            this.cmdResetDiagnosticos = new DevExpress.XtraEditors.SimpleButton();
            this.cmdServices = new DevExpress.XtraEditors.SimpleButton();
            this.cmdAudio = new DevExpress.XtraEditors.SimpleButton();
            this.cmdSmartInspect = new DevExpress.XtraEditors.SimpleButton();
            this.cmdAdminDevices = new DevExpress.XtraEditors.SimpleButton();
            this.cmdSqlBrowser = new DevExpress.XtraEditors.SimpleButton();
            this.cmdShowDesktop = new DevExpress.XtraEditors.SimpleButton();
            this.cmdShowExplorer = new DevExpress.XtraEditors.SimpleButton();
            this.cmdShowPanelControl = new DevExpress.XtraEditors.SimpleButton();
            this.tabPagTerminal = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cmdEraseTerm = new DevExpress.XtraEditors.SimpleButton();
            this.cmdSleepComputer = new DevExpress.XtraEditors.SimpleButton();
            this.cmdRestart = new DevExpress.XtraEditors.SimpleButton();
            this.cmdRestartSystem = new DevExpress.XtraEditors.SimpleButton();
            this.cmdExecHelpOnLine2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.ckAdminLogin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPagSistema.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).BeginInit();
            this.gcTrace.SuspendLayout();
            this.tabPagTerminal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ckAdminLogin
            // 
            this.ckAdminLogin.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ckAdminLogin.Location = new System.Drawing.Point(358, 44);
            this.ckAdminLogin.Name = "ckAdminLogin";
            this.ckAdminLogin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckAdminLogin.Properties.Appearance.Options.UseFont = true;
            this.ckAdminLogin.Properties.Appearance.Options.UseTextOptions = true;
            this.ckAdminLogin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ckAdminLogin.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ckAdminLogin.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.ckAdminLogin.Properties.Caption = "Desbloquear  Funciones de Administración";
            this.ckAdminLogin.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.ckAdminLogin.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Inactive;
            this.ckAdminLogin.Properties.PictureChecked = ((System.Drawing.Image)(resources.GetObject("ckAdminLogin.Properties.PictureChecked")));
            this.ckAdminLogin.Properties.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("ckAdminLogin.Properties.PictureUnchecked")));
            this.ckAdminLogin.Size = new System.Drawing.Size(270, 52);
            this.ckAdminLogin.TabIndex = 14;
            this.ckAdminLogin.ToolTipTitle = "Permite Bloquear / Desbloquear la funciones especiales de Administracion";
            this.ckAdminLogin.CheckedChanged += new System.EventHandler(this.ckAdminLogin_CheckedChanged);
            // 
            // cmdExecHelpOnLine
            // 
            this.cmdExecHelpOnLine.AllowFocus = false;
            this.cmdExecHelpOnLine.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdExecHelpOnLine.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExecHelpOnLine.Appearance.Options.UseBackColor = true;
            this.cmdExecHelpOnLine.Appearance.Options.UseFont = true;
            this.cmdExecHelpOnLine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdExecHelpOnLine.Image = global::gcperu.Term.UI.Properties.Resources.Sos;
            this.cmdExecHelpOnLine.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.cmdExecHelpOnLine.Location = new System.Drawing.Point(20, 227);
            this.cmdExecHelpOnLine.Margin = new System.Windows.Forms.Padding(0);
            this.cmdExecHelpOnLine.Name = "cmdExecHelpOnLine";
            this.cmdExecHelpOnLine.Size = new System.Drawing.Size(286, 68);
            this.cmdExecHelpOnLine.TabIndex = 12;
            this.cmdExecHelpOnLine.Tag = "";
            this.cmdExecHelpOnLine.Text = "S.O.S";
            this.cmdExecHelpOnLine.ToolTip = "UTILIDAD PARA SOLICITAR AYUDA AL CENTRO DE CONTROL. UTILIZAR SOLO EN CASO INDICAD" +
    "O POR UN ADMINISTRADOR";
            this.cmdExecHelpOnLine.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdExecHelpOnLine.ToolTipTitle = "Solicitud de Ayuda al centro de control";
            this.cmdExecHelpOnLine.Click += new System.EventHandler(this.cmdExecHelpOnLine_Click);
            // 
            // btInfo
            // 
            this.btInfo.AllowFocus = false;
            this.btInfo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btInfo.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btInfo.Appearance.Options.UseBackColor = true;
            this.btInfo.Appearance.Options.UseFont = true;
            this.btInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btInfo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btInfo.Image = ((System.Drawing.Image)(resources.GetObject("btInfo.Image")));
            this.btInfo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btInfo.Location = new System.Drawing.Point(484, 277);
            this.btInfo.Margin = new System.Windows.Forms.Padding(0);
            this.btInfo.Name = "btInfo";
            this.btInfo.Size = new System.Drawing.Size(56, 60);
            this.btInfo.TabIndex = 6;
            this.btInfo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.btInfo.ToolTipTitle = "Enviar Incidencia";
            this.btInfo.Click += new System.EventHandler(this.btInfo_Click);
            // 
            // btOption
            // 
            this.btOption.AllowFocus = false;
            this.btOption.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btOption.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btOption.Appearance.Options.UseBackColor = true;
            this.btOption.Appearance.Options.UseFont = true;
            this.btOption.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btOption.Image = ((System.Drawing.Image)(resources.GetObject("btOption.Image")));
            this.btOption.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btOption.Location = new System.Drawing.Point(20, 38);
            this.btOption.Margin = new System.Windows.Forms.Padding(0);
            this.btOption.Name = "btOption";
            this.btOption.Size = new System.Drawing.Size(286, 68);
            this.btOption.TabIndex = 5;
            this.btOption.Text = "Configuración";
            this.btOption.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.btOption.ToolTipTitle = "Panel de Opciones";
            this.btOption.Click += new System.EventHandler(this.btOption_Click);
            // 
            // btTareas
            // 
            this.btTareas.AllowFocus = false;
            this.btTareas.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btTareas.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTareas.Appearance.Options.UseBackColor = true;
            this.btTareas.Appearance.Options.UseFont = true;
            this.btTareas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btTareas.Image = ((System.Drawing.Image)(resources.GetObject("btTareas.Image")));
            this.btTareas.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btTareas.Location = new System.Drawing.Point(20, 126);
            this.btTareas.Margin = new System.Windows.Forms.Padding(0);
            this.btTareas.Name = "btTareas";
            this.btTareas.Size = new System.Drawing.Size(286, 68);
            this.btTareas.TabIndex = 24;
            this.btTareas.Text = "Panel de Tareas";
            this.btTareas.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.btTareas.ToolTipTitle = "Panel de Opciones";
            this.btTareas.Click += new System.EventHandler(this.btTareas_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.AppearancePage.Header.Options.UseFont = true;
            this.tabControl.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tabControl.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tabControl.AppearancePage.HeaderActive.Options.UseForeColor = true;
            this.tabControl.Location = new System.Drawing.Point(682, 38);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.tabPagSistema;
            this.tabControl.Size = new System.Drawing.Size(319, 352);
            this.tabControl.TabIndex = 25;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPagSistema,
            this.tabPagTerminal});
            // 
            // tabPagSistema
            // 
            this.tabPagSistema.Appearance.HeaderActive.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPagSistema.Appearance.HeaderActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tabPagSistema.Appearance.HeaderActive.Options.UseFont = true;
            this.tabPagSistema.Appearance.HeaderActive.Options.UseForeColor = true;
            this.tabPagSistema.Controls.Add(this.gcTrace);
            this.tabPagSistema.Name = "tabPagSistema";
            this.tabPagSistema.Size = new System.Drawing.Size(313, 314);
            this.tabPagSistema.Text = "Sistema";
            // 
            // gcTrace
            // 
            this.gcTrace.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcTrace.AppearanceCaption.Options.UseFont = true;
            this.gcTrace.Controls.Add(this.cmdUploadLog);
            this.gcTrace.Controls.Add(this.cmdConexRed);
            this.gcTrace.Controls.Add(this.cmdHideTaskBar);
            this.gcTrace.Controls.Add(this.cmdShowTaskBar);
            this.gcTrace.Controls.Add(this.cmdResetDiagnosticos);
            this.gcTrace.Controls.Add(this.cmdServices);
            this.gcTrace.Controls.Add(this.cmdAudio);
            this.gcTrace.Controls.Add(this.cmdSmartInspect);
            this.gcTrace.Controls.Add(this.cmdAdminDevices);
            this.gcTrace.Controls.Add(this.cmdSqlBrowser);
            this.gcTrace.Controls.Add(this.cmdShowDesktop);
            this.gcTrace.Controls.Add(this.cmdShowExplorer);
            this.gcTrace.Controls.Add(this.cmdShowPanelControl);
            this.gcTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTrace.Location = new System.Drawing.Point(0, 0);
            this.gcTrace.Name = "gcTrace";
            this.gcTrace.Size = new System.Drawing.Size(313, 314);
            this.gcTrace.TabIndex = 22;
            this.gcTrace.Text = "Control del Sistema";
            // 
            // cmdUploadLog
            // 
            this.cmdUploadLog.AllowFocus = false;
            this.cmdUploadLog.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdUploadLog.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdUploadLog.Appearance.Options.UseBackColor = true;
            this.cmdUploadLog.Appearance.Options.UseFont = true;
            this.cmdUploadLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdUploadLog.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdUploadLog.Image = ((System.Drawing.Image)(resources.GetObject("cmdUploadLog.Image")));
            this.cmdUploadLog.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdUploadLog.Location = new System.Drawing.Point(14, 195);
            this.cmdUploadLog.Margin = new System.Windows.Forms.Padding(0);
            this.cmdUploadLog.Name = "cmdUploadLog";
            this.cmdUploadLog.Size = new System.Drawing.Size(76, 63);
            this.cmdUploadLog.TabIndex = 38;
            this.cmdUploadLog.ToolTip = "Fuerza la subida de Logs";
            this.cmdUploadLog.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdUploadLog.ToolTipTitle = "Viliv Manager";
            this.cmdUploadLog.Click += new System.EventHandler(this.cmdUploadLog_Click);
            // 
            // cmdConexRed
            // 
            this.cmdConexRed.AllowFocus = false;
            this.cmdConexRed.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdConexRed.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdConexRed.Appearance.Options.UseBackColor = true;
            this.cmdConexRed.Appearance.Options.UseFont = true;
            this.cmdConexRed.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdConexRed.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdConexRed.Image = ((System.Drawing.Image)(resources.GetObject("cmdConexRed.Image")));
            this.cmdConexRed.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdConexRed.Location = new System.Drawing.Point(180, 55);
            this.cmdConexRed.Margin = new System.Windows.Forms.Padding(0);
            this.cmdConexRed.Name = "cmdConexRed";
            this.cmdConexRed.Size = new System.Drawing.Size(63, 67);
            this.cmdConexRed.TabIndex = 36;
            this.cmdConexRed.ToolTip = "MUESTRA EL EXPLORADOR DE CARPETAS";
            this.cmdConexRed.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdConexRed.ToolTipTitle = "Explorador de Carpetas";
            this.cmdConexRed.Click += new System.EventHandler(this.cmdConexRed_Click);
            // 
            // cmdHideTaskBar
            // 
            this.cmdHideTaskBar.AllowFocus = false;
            this.cmdHideTaskBar.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdHideTaskBar.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdHideTaskBar.Appearance.Options.UseBackColor = true;
            this.cmdHideTaskBar.Appearance.Options.UseFont = true;
            this.cmdHideTaskBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdHideTaskBar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdHideTaskBar.Image = ((System.Drawing.Image)(resources.GetObject("cmdHideTaskBar.Image")));
            this.cmdHideTaskBar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.cmdHideTaskBar.Location = new System.Drawing.Point(127, 258);
            this.cmdHideTaskBar.Margin = new System.Windows.Forms.Padding(0);
            this.cmdHideTaskBar.Name = "cmdHideTaskBar";
            this.cmdHideTaskBar.Size = new System.Drawing.Size(100, 33);
            this.cmdHideTaskBar.TabIndex = 35;
            this.cmdHideTaskBar.ToolTip = "MUESTRA EL ESCRITORIO DE WINDOWS";
            this.cmdHideTaskBar.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdHideTaskBar.ToolTipTitle = "Escritorio";
            this.cmdHideTaskBar.Click += new System.EventHandler(this.cmdHideTaskBar_Click);
            // 
            // cmdShowTaskBar
            // 
            this.cmdShowTaskBar.AllowFocus = false;
            this.cmdShowTaskBar.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdShowTaskBar.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdShowTaskBar.Appearance.Options.UseBackColor = true;
            this.cmdShowTaskBar.Appearance.Options.UseFont = true;
            this.cmdShowTaskBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdShowTaskBar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdShowTaskBar.Image = ((System.Drawing.Image)(resources.GetObject("cmdShowTaskBar.Image")));
            this.cmdShowTaskBar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.cmdShowTaskBar.Location = new System.Drawing.Point(10, 258);
            this.cmdShowTaskBar.Margin = new System.Windows.Forms.Padding(0);
            this.cmdShowTaskBar.Name = "cmdShowTaskBar";
            this.cmdShowTaskBar.Size = new System.Drawing.Size(99, 33);
            this.cmdShowTaskBar.TabIndex = 34;
            this.cmdShowTaskBar.ToolTip = "MUESTRA EL ESCRITORIO DE WINDOWS";
            this.cmdShowTaskBar.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdShowTaskBar.ToolTipTitle = "Escritorio";
            this.cmdShowTaskBar.Click += new System.EventHandler(this.cmdShowTaskBar_Click);
            // 
            // cmdResetDiagnosticos
            // 
            this.cmdResetDiagnosticos.AllowFocus = false;
            this.cmdResetDiagnosticos.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdResetDiagnosticos.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdResetDiagnosticos.Appearance.Options.UseBackColor = true;
            this.cmdResetDiagnosticos.Appearance.Options.UseFont = true;
            this.cmdResetDiagnosticos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdResetDiagnosticos.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdResetDiagnosticos.Image = ((System.Drawing.Image)(resources.GetObject("cmdResetDiagnosticos.Image")));
            this.cmdResetDiagnosticos.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdResetDiagnosticos.Location = new System.Drawing.Point(104, 185);
            this.cmdResetDiagnosticos.Margin = new System.Windows.Forms.Padding(0);
            this.cmdResetDiagnosticos.Name = "cmdResetDiagnosticos";
            this.cmdResetDiagnosticos.Size = new System.Drawing.Size(58, 68);
            this.cmdResetDiagnosticos.TabIndex = 33;
            this.cmdResetDiagnosticos.ToolTip = "Reinicia los contadores de los Diagnosticos Activos y Borra los diagnosticos Inac" +
    "tivos.";
            this.cmdResetDiagnosticos.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdResetDiagnosticos.ToolTipTitle = "Reiniciar Diagnosticos";
            this.cmdResetDiagnosticos.Click += new System.EventHandler(this.cmdResetDiagnosticos_Click);
            // 
            // cmdServices
            // 
            this.cmdServices.AllowFocus = false;
            this.cmdServices.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdServices.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdServices.Appearance.Options.UseBackColor = true;
            this.cmdServices.Appearance.Options.UseFont = true;
            this.cmdServices.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdServices.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdServices.Image = ((System.Drawing.Image)(resources.GetObject("cmdServices.Image")));
            this.cmdServices.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdServices.Location = new System.Drawing.Point(246, 131);
            this.cmdServices.Margin = new System.Windows.Forms.Padding(0);
            this.cmdServices.Name = "cmdServices";
            this.cmdServices.Size = new System.Drawing.Size(66, 51);
            this.cmdServices.TabIndex = 32;
            this.cmdServices.ToolTip = "GESTIONAR CONTROL DE SERVICIOS";
            this.cmdServices.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdServices.ToolTipTitle = "Servicios de Windows";
            this.cmdServices.Click += new System.EventHandler(this.cmdServices_Click);
            // 
            // cmdAudio
            // 
            this.cmdAudio.AllowFocus = false;
            this.cmdAudio.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdAudio.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAudio.Appearance.Options.UseBackColor = true;
            this.cmdAudio.Appearance.Options.UseFont = true;
            this.cmdAudio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdAudio.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdAudio.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdAudio.Location = new System.Drawing.Point(242, 258);
            this.cmdAudio.Margin = new System.Windows.Forms.Padding(0);
            this.cmdAudio.Name = "cmdAudio";
            this.cmdAudio.Size = new System.Drawing.Size(66, 51);
            this.cmdAudio.TabIndex = 31;
            this.cmdAudio.ToolTip = "GESTIONAR CONTROL DE AUDIO";
            this.cmdAudio.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdAudio.ToolTipTitle = "AUDIO";
            this.cmdAudio.Click += new System.EventHandler(this.cmdAudio_Click);
            // 
            // cmdSmartInspect
            // 
            this.cmdSmartInspect.AllowFocus = false;
            this.cmdSmartInspect.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdSmartInspect.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSmartInspect.Appearance.Options.UseBackColor = true;
            this.cmdSmartInspect.Appearance.Options.UseFont = true;
            this.cmdSmartInspect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdSmartInspect.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdSmartInspect.Image = ((System.Drawing.Image)(resources.GetObject("cmdSmartInspect.Image")));
            this.cmdSmartInspect.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdSmartInspect.Location = new System.Drawing.Point(104, 122);
            this.cmdSmartInspect.Margin = new System.Windows.Forms.Padding(0);
            this.cmdSmartInspect.Name = "cmdSmartInspect";
            this.cmdSmartInspect.Size = new System.Drawing.Size(76, 68);
            this.cmdSmartInspect.TabIndex = 30;
            this.cmdSmartInspect.ToolTip = "VISUALIZADOR de LOGs";
            this.cmdSmartInspect.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdSmartInspect.ToolTipTitle = "LOG";
            this.cmdSmartInspect.Click += new System.EventHandler(this.cmdSmartInspect_Click);
            // 
            // cmdAdminDevices
            // 
            this.cmdAdminDevices.AllowFocus = false;
            this.cmdAdminDevices.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdAdminDevices.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAdminDevices.Appearance.Options.UseBackColor = true;
            this.cmdAdminDevices.Appearance.Options.UseFont = true;
            this.cmdAdminDevices.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdAdminDevices.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdAdminDevices.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdminDevices.Image")));
            this.cmdAdminDevices.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdAdminDevices.Location = new System.Drawing.Point(180, 122);
            this.cmdAdminDevices.Margin = new System.Windows.Forms.Padding(0);
            this.cmdAdminDevices.Name = "cmdAdminDevices";
            this.cmdAdminDevices.Size = new System.Drawing.Size(76, 68);
            this.cmdAdminDevices.TabIndex = 29;
            this.cmdAdminDevices.ToolTip = "MUESTRA EL ADMINISTRADOR DE DISPOSITIVOS";
            this.cmdAdminDevices.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdAdminDevices.ToolTipTitle = "Dispositivos";
            this.cmdAdminDevices.Click += new System.EventHandler(this.cmdAdminDevices_Click);
            // 
            // cmdSqlBrowser
            // 
            this.cmdSqlBrowser.AllowFocus = false;
            this.cmdSqlBrowser.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdSqlBrowser.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSqlBrowser.Appearance.Options.UseBackColor = true;
            this.cmdSqlBrowser.Appearance.Options.UseFont = true;
            this.cmdSqlBrowser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdSqlBrowser.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdSqlBrowser.Image = ((System.Drawing.Image)(resources.GetObject("cmdSqlBrowser.Image")));
            this.cmdSqlBrowser.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdSqlBrowser.Location = new System.Drawing.Point(246, 54);
            this.cmdSqlBrowser.Margin = new System.Windows.Forms.Padding(0);
            this.cmdSqlBrowser.Name = "cmdSqlBrowser";
            this.cmdSqlBrowser.Size = new System.Drawing.Size(55, 68);
            this.cmdSqlBrowser.TabIndex = 28;
            this.cmdSqlBrowser.ToolTip = "PERMITE CAMBIAR LA CONFIGURACION A NIVEL DE BASE DE DATOS";
            this.cmdSqlBrowser.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdSqlBrowser.ToolTipTitle = "Acceso a BD";
            this.cmdSqlBrowser.Click += new System.EventHandler(this.cmdSqlBrowser_Click);
            // 
            // cmdShowDesktop
            // 
            this.cmdShowDesktop.AllowFocus = false;
            this.cmdShowDesktop.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdShowDesktop.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdShowDesktop.Appearance.Options.UseBackColor = true;
            this.cmdShowDesktop.Appearance.Options.UseFont = true;
            this.cmdShowDesktop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdShowDesktop.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdShowDesktop.Image = ((System.Drawing.Image)(resources.GetObject("cmdShowDesktop.Image")));
            this.cmdShowDesktop.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdShowDesktop.Location = new System.Drawing.Point(14, 122);
            this.cmdShowDesktop.Margin = new System.Windows.Forms.Padding(0);
            this.cmdShowDesktop.Name = "cmdShowDesktop";
            this.cmdShowDesktop.Size = new System.Drawing.Size(76, 68);
            this.cmdShowDesktop.TabIndex = 23;
            this.cmdShowDesktop.ToolTip = "MUESTRA EL ESCRITORIO DE WINDOWS";
            this.cmdShowDesktop.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdShowDesktop.ToolTipTitle = "Escritorio";
            this.cmdShowDesktop.Click += new System.EventHandler(this.cmdShowDesktop_Click);
            // 
            // cmdShowExplorer
            // 
            this.cmdShowExplorer.AllowFocus = false;
            this.cmdShowExplorer.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdShowExplorer.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdShowExplorer.Appearance.Options.UseBackColor = true;
            this.cmdShowExplorer.Appearance.Options.UseFont = true;
            this.cmdShowExplorer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdShowExplorer.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdShowExplorer.Image = ((System.Drawing.Image)(resources.GetObject("cmdShowExplorer.Image")));
            this.cmdShowExplorer.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdShowExplorer.Location = new System.Drawing.Point(104, 55);
            this.cmdShowExplorer.Margin = new System.Windows.Forms.Padding(0);
            this.cmdShowExplorer.Name = "cmdShowExplorer";
            this.cmdShowExplorer.Size = new System.Drawing.Size(76, 67);
            this.cmdShowExplorer.TabIndex = 21;
            this.cmdShowExplorer.ToolTip = "MUESTRA EL EXPLORADOR DE CARPETAS";
            this.cmdShowExplorer.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdShowExplorer.ToolTipTitle = "Explorador de Carpetas";
            this.cmdShowExplorer.Click += new System.EventHandler(this.cmdShowExplorer_Click);
            // 
            // cmdShowPanelControl
            // 
            this.cmdShowPanelControl.AllowFocus = false;
            this.cmdShowPanelControl.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdShowPanelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdShowPanelControl.Appearance.Options.UseBackColor = true;
            this.cmdShowPanelControl.Appearance.Options.UseFont = true;
            this.cmdShowPanelControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdShowPanelControl.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdShowPanelControl.Image = ((System.Drawing.Image)(resources.GetObject("cmdShowPanelControl.Image")));
            this.cmdShowPanelControl.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdShowPanelControl.Location = new System.Drawing.Point(14, 47);
            this.cmdShowPanelControl.Margin = new System.Windows.Forms.Padding(0);
            this.cmdShowPanelControl.Name = "cmdShowPanelControl";
            this.cmdShowPanelControl.Size = new System.Drawing.Size(76, 68);
            this.cmdShowPanelControl.TabIndex = 20;
            this.cmdShowPanelControl.ToolTip = "MUESTRA EL PANEL DE CONTROL";
            this.cmdShowPanelControl.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdShowPanelControl.ToolTipTitle = "Panel de Control";
            this.cmdShowPanelControl.Click += new System.EventHandler(this.cmdShowPanelControl_Click);
            // 
            // tabPagTerminal
            // 
            this.tabPagTerminal.Appearance.HeaderActive.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPagTerminal.Appearance.HeaderActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tabPagTerminal.Appearance.HeaderActive.Options.UseFont = true;
            this.tabPagTerminal.Appearance.HeaderActive.Options.UseForeColor = true;
            this.tabPagTerminal.Controls.Add(this.groupControl1);
            this.tabPagTerminal.Name = "tabPagTerminal";
            this.tabPagTerminal.Size = new System.Drawing.Size(313, 314);
            this.tabPagTerminal.Text = "Terminal";
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.cmdEraseTerm);
            this.groupControl1.Controls.Add(this.cmdSleepComputer);
            this.groupControl1.Controls.Add(this.cmdRestart);
            this.groupControl1.Controls.Add(this.cmdRestartSystem);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(313, 314);
            this.groupControl1.TabIndex = 23;
            this.groupControl1.Text = "Control del Terminal";
            // 
            // cmdEraseTerm
            // 
            this.cmdEraseTerm.AllowFocus = false;
            this.cmdEraseTerm.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdEraseTerm.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEraseTerm.Appearance.Options.UseBackColor = true;
            this.cmdEraseTerm.Appearance.Options.UseFont = true;
            this.cmdEraseTerm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdEraseTerm.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdEraseTerm.Image = ((System.Drawing.Image)(resources.GetObject("cmdEraseTerm.Image")));
            this.cmdEraseTerm.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdEraseTerm.Location = new System.Drawing.Point(119, 102);
            this.cmdEraseTerm.Margin = new System.Windows.Forms.Padding(0);
            this.cmdEraseTerm.Name = "cmdEraseTerm";
            this.cmdEraseTerm.Size = new System.Drawing.Size(89, 68);
            this.cmdEraseTerm.TabIndex = 13;
            this.cmdEraseTerm.ToolTip = "INICIALIZA EL TERMINAL A SU ESTADO INICIAL.";
            this.cmdEraseTerm.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdEraseTerm.Click += new System.EventHandler(this.cmdEraseTerm_Click);
            // 
            // cmdSleepComputer
            // 
            this.cmdSleepComputer.AllowFocus = false;
            this.cmdSleepComputer.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdSleepComputer.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSleepComputer.Appearance.Options.UseBackColor = true;
            this.cmdSleepComputer.Appearance.Options.UseFont = true;
            this.cmdSleepComputer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdSleepComputer.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdSleepComputer.Image = ((System.Drawing.Image)(resources.GetObject("cmdSleepComputer.Image")));
            this.cmdSleepComputer.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdSleepComputer.Location = new System.Drawing.Point(30, 101);
            this.cmdSleepComputer.Margin = new System.Windows.Forms.Padding(0);
            this.cmdSleepComputer.Name = "cmdSleepComputer";
            this.cmdSleepComputer.Size = new System.Drawing.Size(89, 68);
            this.cmdSleepComputer.TabIndex = 12;
            this.cmdSleepComputer.ToolTip = "APAGAR EL SISTEMA";
            this.cmdSleepComputer.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdSleepComputer.ToolTipTitle = "Apagar";
            this.cmdSleepComputer.Click += new System.EventHandler(this.cmdShutdownComputer_Click);
            // 
            // cmdRestart
            // 
            this.cmdRestart.AllowFocus = false;
            this.cmdRestart.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdRestart.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRestart.Appearance.Options.UseBackColor = true;
            this.cmdRestart.Appearance.Options.UseFont = true;
            this.cmdRestart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdRestart.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdRestart.Image = global::gcperu.Term.UI.Properties.Resources.Restart;
            this.cmdRestart.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdRestart.Location = new System.Drawing.Point(30, 34);
            this.cmdRestart.Margin = new System.Windows.Forms.Padding(0);
            this.cmdRestart.Name = "cmdRestart";
            this.cmdRestart.Size = new System.Drawing.Size(89, 68);
            this.cmdRestart.TabIndex = 11;
            this.cmdRestart.ToolTip = "REINICIAR LA APLICACIÓN";
            this.cmdRestart.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdRestart.ToolTipTitle = "Reiniciar Aplicacion";
            this.cmdRestart.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // cmdRestartSystem
            // 
            this.cmdRestartSystem.AllowFocus = false;
            this.cmdRestartSystem.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdRestartSystem.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRestartSystem.Appearance.Options.UseBackColor = true;
            this.cmdRestartSystem.Appearance.Options.UseFont = true;
            this.cmdRestartSystem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdRestartSystem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cmdRestartSystem.Image = ((System.Drawing.Image)(resources.GetObject("cmdRestartSystem.Image")));
            this.cmdRestartSystem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.cmdRestartSystem.Location = new System.Drawing.Point(119, 34);
            this.cmdRestartSystem.Margin = new System.Windows.Forms.Padding(0);
            this.cmdRestartSystem.Name = "cmdRestartSystem";
            this.cmdRestartSystem.Size = new System.Drawing.Size(89, 68);
            this.cmdRestartSystem.TabIndex = 10;
            this.cmdRestartSystem.ToolTip = "REINICIA EL SISTEMA";
            this.cmdRestartSystem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdRestartSystem.ToolTipTitle = "Reiniciar Sistema";
            this.cmdRestartSystem.Click += new System.EventHandler(this.cmdRestart_Click);
            // 
            // cmdExecHelpOnLine2
            // 
            this.cmdExecHelpOnLine2.AllowFocus = false;
            this.cmdExecHelpOnLine2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cmdExecHelpOnLine2.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExecHelpOnLine2.Appearance.Options.UseBackColor = true;
            this.cmdExecHelpOnLine2.Appearance.Options.UseFont = true;
            this.cmdExecHelpOnLine2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdExecHelpOnLine2.Image = global::gcperu.Term.UI.Properties.Resources.Sos;
            this.cmdExecHelpOnLine2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.cmdExecHelpOnLine2.Location = new System.Drawing.Point(20, 302);
            this.cmdExecHelpOnLine2.Margin = new System.Windows.Forms.Padding(0);
            this.cmdExecHelpOnLine2.Name = "cmdExecHelpOnLine2";
            this.cmdExecHelpOnLine2.Size = new System.Drawing.Size(286, 68);
            this.cmdExecHelpOnLine2.TabIndex = 26;
            this.cmdExecHelpOnLine2.Tag = "";
            this.cmdExecHelpOnLine2.Text = "S.O.S(2)";
            this.cmdExecHelpOnLine2.ToolTip = "UTILIDAD PARA SOLICITAR AYUDA AL CENTRO DE CONTROL. UTILIZAR SOLO EN CASO INDICAD" +
    "O POR UN ADMINISTRADOR";
            this.cmdExecHelpOnLine2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.cmdExecHelpOnLine2.ToolTipTitle = "Solicitud de Ayuda al centro de control";
            this.cmdExecHelpOnLine2.Click += new System.EventHandler(this.cmdExecHelpOnLine2_Click);
            // 
            // PantallaOpcion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cmdExecHelpOnLine2);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btTareas);
            this.Controls.Add(this.ckAdminLogin);
            this.Controls.Add(this.cmdExecHelpOnLine);
            this.Controls.Add(this.btInfo);
            this.Controls.Add(this.btOption);
            this.LookAndFeel.SkinName = "iMaginary";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "PantallaOpcion";
            this.Size = new System.Drawing.Size(1024, 459);
            this.Load += new System.EventHandler(this.PantallaOpcion_Load);
            this.Leave += new System.EventHandler(this.PantallaOpcion_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.ckAdminLogin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPagSistema.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).EndInit();
            this.gcTrace.ResumeLayout(false);
            this.tabPagTerminal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btOption;
        private DevExpress.XtraEditors.SimpleButton btInfo;
        private DevExpress.XtraEditors.SimpleButton cmdExecHelpOnLine;
        private DevExpress.XtraEditors.CheckEdit ckAdminLogin;
        private DevExpress.XtraEditors.SimpleButton btTareas;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabPagSistema;
        private DevExpress.XtraEditors.GroupControl gcTrace;
        private DevExpress.XtraEditors.SimpleButton cmdShowDesktop;
        private DevExpress.XtraEditors.SimpleButton cmdShowExplorer;
        private DevExpress.XtraEditors.SimpleButton cmdShowPanelControl;
        private DevExpress.XtraTab.XtraTabPage tabPagTerminal;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton cmdSleepComputer;
        private DevExpress.XtraEditors.SimpleButton cmdRestart;
        private DevExpress.XtraEditors.SimpleButton cmdRestartSystem;
        private DevExpress.XtraEditors.SimpleButton cmdEraseTerm;
        private DevExpress.XtraEditors.SimpleButton cmdSqlBrowser;
        private DevExpress.XtraEditors.SimpleButton cmdAdminDevices;
        private DevExpress.XtraEditors.SimpleButton cmdSmartInspect;
        private DevExpress.XtraEditors.SimpleButton cmdAudio;
        private DevExpress.XtraEditors.SimpleButton cmdServices;
        private DevExpress.XtraEditors.SimpleButton cmdExecHelpOnLine2;
        private DevExpress.XtraEditors.SimpleButton cmdResetDiagnosticos;
        private DevExpress.XtraEditors.SimpleButton cmdShowTaskBar;
        private DevExpress.XtraEditors.SimpleButton cmdHideTaskBar;
        private DevExpress.XtraEditors.SimpleButton cmdConexRed;
        private DevExpress.XtraEditors.SimpleButton cmdUploadLog;
    }
}
