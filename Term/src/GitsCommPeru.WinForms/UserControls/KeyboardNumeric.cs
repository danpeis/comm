using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;

namespace GITSNavega.UI.UserControls
{
    public partial class KeyboardNumeric : DevExpress.XtraEditors.XtraUserControl
    {
        
        public KeyboardNumeric()
        {
            InitializeComponent();
        }

        private void ltS_Click(object sender, EventArgs e)
        {

        }

        private void KeyboardNumeric_Resize(object sender, EventArgs e)
        {
            var panelSize = new Size();
            var controlSize = new Size();

            panelSize.Width = (this.Width/4);
            panelSize.Height = (this.Height/4);

            controlSize.Width = (this.Width / 3); //Calculamos el Ancho de cada Control

            var panels = this.Controls.OfType<PanelControl>();

            foreach (var _panelControl in panels)
            {
                _panelControl.Size = panelSize;

                foreach (var _button in _panelControl.Controls.OfType<SimpleButton>())
                {
                    _button.Width = controlSize.Width;
                    _button.Left = (_button.TabIndex - 1)*controlSize.Width;
                }
            }


            foreach (var _panelControl in panels)
            {
                _panelControl.Size = panelSize;
            }

            
        }


        private void ButtonNumericClick(object sender, EventArgs e)
        {
            var buttonpressed = sender as SimpleButton;

            this.OnKeyPress(new KeyPressEventArgs(buttonpressed.Text[0]));
        }

        private void ButtonActionClick(object sender, EventArgs e)
        {
            var buttonpress = sender as SimpleButton;

            if (buttonpress.Name == "btBack")
                this.OnKeyPress(new KeyPressEventArgs((char) Keys.Back));
            else
                this.OnKeyPress(new KeyPressEventArgs((char)Keys.Enter));

        }
    }
    
}
