using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Windows.Forms;
using gcperu.Term.UI;
using Splat;
using OperatingSystem = gcperu.Term.UI.OperatingSystem;

namespace GITSNavega.UI.UserControls.Pantalla
{
    public partial class PantallaOpcion : PantallaControl, IEnableLogger
    {
        public PantallaOpcion()
        {
            InitializeComponent();
        }

//        public override PantallaTipo Tipo
//        {
//            get { return PantallaTipo.Opciones;}
//        }

//        public override void Initialize()
//        {
////            Global.WorkingArea.Pie.Enabled=false;
////
////            if (Global.Servicios != null) //cuando no se detecta modem no se instancia este objeto y daria un error sin esta comprobaci�n.
////                btSimul.Enabled = (!(Global.Servicios.CountPendientes() > 0 || Global.Servicios.CountEnCurso() > 0));
////            else btSimul.Enabled = false;
//        }

        #region EventHandlers

        private void btOption_Click(object sender, EventArgs e)
        {
            // Open configuration

            //if (!ValidateAdministrador.Validate()) return;

//            var foption = new Forms.NewOptionForm();
//            foption.ShowDialog();
        }

        private void PantallaOpcion_Leave(object sender, EventArgs e)
        {
        }

        private void cmdRestart_Click(object sender, EventArgs e)
        {
//            if (MensajeBox.ShowRsp("�Desea reiniciar el Sistema?", MsgBoxicon.IconQuestion, MsgBoxbuttons.BtYesNo, MsgBoxButtonType.BtNo) == DialogResult.No)
//                return;
//
//            Application.DoEvents();
//            Global.Device.Config.State.WorkingOperateState.State = DeviceOperateState.Restarting;  //Indicamos que queremos reinicar el Sistema.
//            Global.Device.OnOperateStatusChanged(new WorkingStateInfo(DeviceOperateState.Restarting,DeviceOperateStateReason.RebootUser));

            //Operacion ejecutada en metodo anterior.
        }

        private void cmdExit_Click(object sender, EventArgs e)
        {
//            if (MensajeBox.ShowRsp("�Desea reiniciar la Aplicaci�n?", MsgBoxicon.IconQuestion, MsgBoxbuttons.BtYesNo, MsgBoxButtonType.BtNo) == DialogResult.No)
//                return;
//
//            Application.DoEvents();
//            Global.Device.Config.State.WorkingOperateState.State = DeviceOperateState.AppRestarting;  //Indicamos que queremos reinicar el Sistema.
//            Global.Device.OnOperateStatusChanged(new WorkingStateInfo(DeviceOperateState.AppRestarting, DeviceOperateStateReason.AppRestartUser));

            //Operacion ejecutada en metodo anterior.
        }

        private async void cmdAdminDevices_Click(object sender, EventArgs e)
        {
            try
            {
                if (!await Administrator.Validate()) return;
                Process.Start(Global.Default.DeviceManagementProcess);
            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR ABRIR PANEL DE CONTROL.", ex));
            }
        }

        private void cmdExecHelpOnLine_Click(object sender, EventArgs e)
        {
            try
            {
                //ABRIR LA APLICACI�N.
//                if (Procesos.ProcesoIsExecuting(Constantes.APPPROCESS_SOS1))
//                {
//                    Procesos.KillProcess(Constantes.APPPROCESS_SOS1);
//                }

                var psos1 = new Process
                {
                    StartInfo =
                        new ProcessStartInfo()
                        {
                            WindowStyle = ProcessWindowStyle.Minimized,
                            FileName = Path.Combine(Path.Combine(Application.StartupPath, "Bin"), Global.Default.Sos1AppFile),
                            WorkingDirectory = Path.Combine(Application.StartupPath, "Bin")
                        }
                };
                psos1.Start();
            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR ABRIR SOLICITUD AYUDA CENTRO DE CONTROL.", ex));
            }
        }

        #endregion

        private void cmdShutdownComputer_Click(object sender, EventArgs e)
        {
//            if (MensajeBox.ShowRsp("�Desea apagar el Sistema ?", MsgBoxicon.IconQuestion, MsgBoxbuttons.BtYesNo, MsgBoxButtonType.BtNo) == DialogResult.No)
//                return;
//            
//            Application.DoEvents();
//            Global.Device.Config.State.WorkingOperateState.State = DeviceOperateState.Shutdown;  //Indicamos que queremos reinicar el Sistema.
//            Global.Device.OnOperateStatusChanged(new WorkingStateInfo(DeviceOperateState.Shutdown, DeviceOperateStateReason.ShutdownUser));

            //Operacion ejecutada en metodo anterior.
        }

        private void cmdShowPanelControl_Click(object sender, EventArgs e)
        {
            TryLoadProcessWithAdminValidation(Global.Default.ControlPanelProcess, "ERROR ABRIR PANEL DE CONTROL.");
        }

        private async void TryLoadProcessWithAdminValidation(string processName, string errorMessage)
        {
            try
            {
                if (!await Administrator.Validate()) return;
                Process.Start(processName);
            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception(errorMessage, ex));
            }
        }

        private void cmdShowExplorer_Click(object sender, EventArgs e)
        {
            TryLoadProcessWithAdminValidation("explorer.exe", "ERROR ABRIR PANEL DE EXPLORADOR DE WINDOWS.");
        }

        private async void ckAdminLogin_CheckedChanged(object sender, EventArgs e)
        {
            if (ckAdminLogin.Checked)
            {
                if (!await Administrator.Validate(changeAdminLock: true))
                {
                    ckAdminLogin.CheckState=CheckState.Unchecked;
                    return;
                }
                ckAdminLogin.Text = "Bloquear funciones de Administraci�n";
            }
            else
            {
                Administrator.Lock();
                _temporaryTaskBarHookDisable.Dispose();
                ckAdminLogin.Text = "Desbloquear funciones de Administraci�n";
            }
        }

        

        private void cmdShowDesktop_Click(object sender, EventArgs e)
        {
            // Show desktop code
//            try
//            {
//                if (!ValidateAdministrador.Validate())
//                {
//                    return;
//                }
//                OperatingsSystem.TaskBarShow();
//                Global.WorkingArea.Main.WindowState=FormWindowState.Minimized;
//            }
//            catch (Exception ex)
//            {
//                TraceLog.LogException(new GCException("ERROR SHOW DESKTOP",ex));
//            }
        }


        private void btInfo_Click(object sender, EventArgs e)
        {
            // Info click
            var f = new Forms.AboutForm();
            f.ShowDialog();
        }

        private void btSimul_Click(object sender, EventArgs e)
        {
//            if (Global.Servicios.CountPendientes() > 0 || Global.Servicios.CountEnCurso() > 0)
//            {
//                MessageBox.Show("Para comenzar la simulaci�n deben estar todos los Servicios Finalizados.", "Aviso",
//                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
//
//                return;
//            }
//
//            if (!ValidateAdministrador.Validate()) return;
//
//            if (Mensajes.MensajeBox.ShowRsp("� Crear servicios para ejecutar simulaci�n?", MsgBoxicon.IconQuestion, MsgBoxbuttons.BtYesNo, MsgBoxButtonType.BtNo) == DialogResult.No)
//                return;
//
//            //if (!ValidateAdministrador.Validate()) return;
//
//            CrearServiciosSimulados();
//            Global.WorkingArea.Main.SetActionServiciosReceived();
//
//            //btServPendiente.Text = string.Format("{0} - {1}", "Servicios Pendientes", Global.Servicios.CountPendientes());
//            //btServCurso.Text = string.Format("{0} - {1}", "Servicios en Curso", Global.Servicios.CountEnCurso());
//
//            btSimul.Enabled = false;
//            //OnPantallaResult(new PantallaResultEventArgs(PantallaTipo.Servicios, PantallaTipo.Servicios, true));
        }

        private void btTareas_Click(object sender, EventArgs e)
        {
//            OnPantallaResult(new PantallaResultEventArgs(PantallaTipo.Opciones, PantallaTipo.Tareas, true));
        }

        private async void cmdEraseTerm_Click(object sender, EventArgs e)
        {
            if (!await Administrator.Validate()) return;

//            if (MensajeBox.ShowRsp("Esta acci�n eliminar� todos los datos guardados en el Terminal y no podr� deshacerse.\n\n�Desea resetear el Terminal, a valores de f�brica ?.", MsgBoxicon.IconQuestion, MsgBoxbuttons.BtYesNo, MsgBoxButtonType.BtNo) == DialogResult.No)
//                return;

//            var result = Terminal.ResetFactoryTerminal();
//            if (result!=null && !result.Result)
//            {
//                MensajeBox.Show(string.Format("NO SE HA PODIDO APLICAR EL RESET A VALORES DE FABRICA POR:\n\n{0}", result.Tx),MsgBoxicon.IconError,MsgBoxbuttons.BtOk,MsgBoxButtonType.BtOk);
//            }
//            else
//            {
//                MensajeBox.Show("RESET APLICADO CORRECTAMENTE. LA APLICACI�N SE APAGAR� PARA APLICAR LA NUEVA CONFIGURACION.", MsgBoxicon.IconError, MsgBoxbuttons.BtOk, MsgBoxButtonType.BtOk);
//                Application.DoEvents();
//                Global.Device.Config.State.WorkingOperateState.State=DeviceOperateState.Parado;
//                Global.Device.GestionarOperateModeChanged(new WorkingStateInfo(DeviceOperateState.Closed,DeviceOperateStateReason.AppClosedUser));
//            }
        }

        private async void cmdVilivGpsSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!await Administrator.Validate())
                {
                    return;
                }

//                if (!DeviceProviderManager.Default.OpenGpsReset())
//                    MensajeBox.Show("EL FICHERO NO HA PODIDO SER LOCALIZADO.",MsgBoxicon.IconExclamation,MsgBoxbuttons.BtOk,MsgBoxButtonType.BtOk);
                
            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR SHOW VILIV GPS SWITCH", ex));
            }
        }

        private async void cmdSqlBrowser_Click(object sender, EventArgs e)
        {
            try
            {
                if (!await Administrator.Validate())
                {
                    return;
                }

                

                if (File.Exists(Global.Default.SqliteManagerPath))
                {
                    Process.Start(Global.Default.SqliteManagerPath);
                    return;
                }

                //Buscamos en archivos de Programa.Sql CE Browser
                // Anulado por ruta absoluta configurable
//                string pathexe = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),"Sql CE Browser");
//                if (File.Exists(Path.Combine(pathexe, exename)))
//                {
//                    Process.Start(Path.Combine(pathexe, exename));
//                    return;
//                }

//                MensajeBox.Show("EL FICHERO NO HA PODIDO SER LOCALIZADO.", MsgBoxicon.IconExclamation,MsgBoxbuttons.BtOk, MsgBoxButtonType.BtOk);

            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR SHOW SqlCeBrowser", ex));
            }
        }

        private async void cmdAudio_Click(object sender, EventArgs e)
        {
            try
            {
                if (!await Administrator.Validate()) return;
                Process.Start(Global.Default.AudioProcess);
            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR ABRIR CONTROL DE AUDIO.", ex));
            }
        }

        private async void cmdSmartInspect_Click(object sender, EventArgs e)
        {
            try
            {
                if (!await Administrator.Validate())
                    return;



                if (File.Exists(Global.Default.LogManagerPath))
                {
                    Process.Start(Global.Default.LogManagerPath);
                    return;
                }

                //Buscamos en archivos de Programa.Sql CE Browser
//                string pathexe = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),@"Gurock Software\SmartInspect Professional");
//                if (File.Exists(Path.Combine(pathexe, exename)))
//                {
//                    Process.Start(Path.Combine(pathexe, exename));
//                    return;
//                }

                //Buscamos en archivos de Programa.VilivManager
//                pathexe = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vilivManager");
//                if (File.Exists(Path.Combine(pathexe, exename)))
//                {
//                    Process.Start(Path.Combine(pathexe, exename));
//                    return;
//                }

//                MensajeBox.Show("EL FICHERO NO HA PODIDO SER LOCALIZADO.", MsgBoxicon.IconExclamation,MsgBoxbuttons.BtOk, MsgBoxButtonType.BtOk);

            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR SHOW SmartInspectConsole", ex));
            }
        }

        private async void cmdServices_Click(object sender, EventArgs e)
        {
            try
            {
                if (!await Administrator.Validate()) return;
                Process.Start(Global.Default.ServicesProcess);
            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR ABRIR CONTROL DE SERVICIOS.", ex));
            }
        }

        private void PantallaOpcion_Load(object sender, EventArgs e)
        {
            try
            {
                cmdExecHelpOnLine2.Visible =
                    File.Exists(Path.Combine(Path.Combine(Application.StartupPath, "bin"), Global.Default.Sos2AppFile));
            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR PantallaOpcion_Load COMPROBACION FICHERO AYUDA LOCAL.", ex));
                cmdExecHelpOnLine2.Visible = false;
            }
        }

        private void cmdExecHelpOnLine2_Click(object sender, EventArgs e)
        {
            try
            {
                var psos2 = new Process
                {
                    StartInfo =
                        new ProcessStartInfo()
                        {
                            WindowStyle = ProcessWindowStyle.Minimized,
                            FileName = Path.Combine(Path.Combine(Application.StartupPath, "Bin"), Global.Default.Sos2AppFile),
                            WorkingDirectory = Path.Combine(Application.StartupPath, "Bin")
                        }
                };
                psos2.Start();
            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR ABRIR SOLICITUD AYUDA CENTRO DE CONTROL LOCAL.", ex));
            }

        }

        private void cmdResetDiagnosticos_Click(object sender, EventArgs e)
        {
            try
            {
////                if (MensajeBox.ShowRsp("�Borrar los diagnosticos?", MsgBoxicon.IconQuestion, MsgBoxbuttons.BtYesNo, MsgBoxButtonType.BtNo) == DialogResult.No)
////                    return;
//
//                Global.Device.Status.Diagnosticos.Reset();
//                Global.WorkingArea.Cabecera.AlertDiagnostico(false);

            }
            catch (Exception ex)
            {
//                TraceLog.LogException(new GCException("ERROR SHOW VILIV MANAGER", ex));
            }
        }

        IDisposable _temporaryTaskBarHookDisable = Disposable.Empty;

        private async void cmdShowTaskBar_Click(object sender, EventArgs e)
        {
            if (!await Administrator.Validate())
            {
                return;
            }

            _temporaryTaskBarHookDisable = GlobalSystemHooker.TemporaryTaskBarEnable();
//                 = new Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height - 25);
        }

        private void cmdHideTaskBar_Click(object sender, EventArgs e)
        {
            _temporaryTaskBarHookDisable.Dispose();
        }

        private async void cmdConexRed_Click(object sender, EventArgs e)
        {
            try
            {
                if (!await Administrator.Validate()) return;
                Process.Start(Global.Default.NetConnectionsProcess);
            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR ABRIR PANEL DE CONEXIONES DE RED.", ex));
            }
        }

        private void cmdUploadLog_Click(object sender, EventArgs e)
        {
            try
            {
//                if (UI.Componentes.GestorEnvioTraceLog.State == UI.Componentes.GestorEnvioTraceLog.Status.Sending)
//                {
//                    MensajeBox.Show("YA SE ESTAN SUBIENDO FICHEROS... PROCESO CANCELADO", MsgBoxicon.IconQuestion, MsgBoxbuttons.BtOk, MsgBoxButtonType.BtNo);
//                    return;
//                }
//
//                if (MensajeBox.ShowRsp("�Subir los ultimos ficheros log?", MsgBoxicon.IconQuestion, MsgBoxbuttons.BtYesNo, MsgBoxButtonType.BtNo) == DialogResult.No)
//                    return;
//
//                var thsendlog = new Thread(new ThreadStart(UI.Componentes.GestorEnvioTraceLog.EnviarFicherosLog));
//                thsendlog.Start();

            }
            catch (Exception ex)
            {
                this.Log().WarnException("", new Exception("ERROR SHOW DESKTOP", ex));
            }
        }

    }
}
