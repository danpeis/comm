﻿using System;
using System.Reactive.Disposables;
using System.Windows.Forms;
using MouseKeyboardActivityMonitor;
using MouseKeyboardActivityMonitor.WinApi;

namespace gcperu.Term.UI
{
    public static class GlobalSystemHooker
    {
        public static Form MainForm { get; set; }

        public static IDisposable GetGlobalHooker(this Form form)
        {
            OperatingSystem.TaskBarHide();
            var keyboardHookManager = new KeyboardHookListener(new GlobalHooker())
            {
                Enabled = true
            };

            form.TopMost = true;
            form.FormBorderStyle = FormBorderStyle.None;
            form.WindowState = FormWindowState.Maximized;

            return Disposable.Create(() =>
            {
                form.TopMost = false;
                form.FormBorderStyle = FormBorderStyle.Fixed3D;
                form.WindowState = FormWindowState.Normal;
                keyboardHookManager.Dispose();
                OperatingSystem.TaskBarShow();
            });
        }

        public static IDisposable TemporaryHooksDisable(this Form form)
        {
            OperatingSystem.TaskBarShow();

            form.TopMost = false;
            form.FormBorderStyle = FormBorderStyle.Fixed3D;
            form.WindowState = FormWindowState.Normal;

            return Disposable.Create(() =>
            {
                form.TopMost = true;
                form.FormBorderStyle = FormBorderStyle.None;
                form.WindowState = FormWindowState.Maximized;
                OperatingSystem.TaskBarHide();
            });
        }

        public static IDisposable TemporaryTaskBarEnable()
        {
            OperatingSystem.TaskBarShow();
            return Disposable.Create(OperatingSystem.TaskBarHide);
        }
    }
}
