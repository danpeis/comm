﻿using System;
using gcperu.Term.Agents;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace gcperu.Term.UI
{
    class Global : ReactiveObject
    {
        private static Global _global = null;
        public static Global Default { get { return _global ?? (_global = new Global()); } }

        private Global()
        {
            InterProcessAgent.BroadbandDataObservable
                    .Subscribe(data =>
                    {
                        IMEINumber = data.IMEI;
                        IMSINumber = data.IMSI;
                        ICCID = data.ICCID;
                    });

            Sos1AppFile = "";
            Sos2AppFile = "";
            SqliteManagerPath = @"C:\ACCESOS DIRECTOS\SQLite_Manager\sqliteman.exe";
            AudioProcess = "mmsys.cpl";
            LogManagerPath = @"C:\ACCESOS DIRECTOS\SmartInspectConsole.exe";
            NetConnectionsProcess = "ncpa.cpl";
            ServicesProcess = "services.msc";
            DeviceManagementProcess = "devmgmt.msc";
            ControlPanelProcess = "control.exe";
            SentryVersion = "1.0";
        }

        [Reactive]
        public string Sos1AppFile { get; private set; }

        [Reactive]
        public string Sos2AppFile { get; private set; }

        [Reactive]
        public string SqliteManagerPath { get; private set; }

        [Reactive]
        public string AudioProcess { get; private set; }

        [Reactive]
        public string LogManagerPath { get; private set; }

        [Reactive]
        public string ServicesProcess { get; private set; }

        [Reactive]
        public string NetConnectionsProcess { get; private set; }

        [Reactive]
        public string DeviceManagementProcess { get; private set; }

        [Reactive]
        public string ControlPanelProcess { get; private set; }

        [Reactive]
        public string ICCID { get; private set; }

        [Reactive]
        public string IMEINumber { get; private set; }
        
        [Reactive]
        public string IMSINumber { get; private set; }

        [Reactive]
        public string SentryVersion { get; private set; }
    }
}
