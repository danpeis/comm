﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using Splat;

namespace gcperu.Term.UI
{
    public static class OperatingSystem
    {
        private class OperatingSystemUtils : IEnableLogger {}
        private static OperatingSystemUtils OperatingSystemUtilsTag = new OperatingSystemUtils();

        public static void TaskBarShow()
        {
            try
            {
                TaskBarSetVisibility(true);
            }
            catch (Exception e)
            {
                OperatingSystemUtilsTag.Log().WarnException("", new Exception("ERROR TASKBARSHOW", e));
            }
        }

        public static void TaskBarHide()
        {
            try
            {
                TaskBarSetVisibility(false);
            }
            catch (Exception e)
            {
                OperatingSystemUtilsTag.Log().WarnException("", new Exception("ERROR TASKBARHIDE", e));
            }
        }

        public static void TaskBarSetVisibility(bool visible)
        {
            Taskbar.Visible = visible;
        }

//        public static bool IsWindowsXp()
//        {
//            return GetOSVersion() == OSversion.WinXP;
//        }
//
//        public static bool IsWindows7()
//        {
//            return GetOSVersion() == OSversion.Win7oWin2008ServerR2;
//        }
//
//        public static bool IsWindows8()
//        {
//            return GetOSVersion() == OSversion.Win8oWin2012Server;
//        }

//        public static OSversion GetOSVersion()
//        {
//            switch (Environment.OSVersion.Platform)
//            {
//                case PlatformID.Win32S:
//                    return OSversion.Win31;
//                case PlatformID.Win32Windows:
//                    switch (Environment.OSVersion.Version.Minor)
//                    {
//                        case 0:
//                            return OSversion.Win95;
//                            ;
//                        case 10:
//                            return OSversion.Win98;
//                        case 90:
//                            return OSversion.WinME;
//                        default:
//                            return OSversion.Unknown;
//                    }
//                    break;
//                case PlatformID.Win32NT:
//                    switch (Environment.OSVersion.Version.Major)
//                    {
//                        case 3:
//                            return OSversion.NT351;
//                        case 4:
//                            return OSversion.NT40;
//                        case 5:
//                            switch (Environment.OSVersion.Version.Minor)
//                            {
//                                case 0:
//                                    return OSversion.Win2000;
//                                case 1:
//                                    return OSversion.WinXP;
//                                case 2:
//                                    return OSversion.Win2003;
//                            }
//                            break;
//                        case 6:
//                            switch (Environment.OSVersion.Version.Minor)
//                            {
//                                case 0:
//                                    return OSversion.VistaoWin2008Server;
//                                case 1:
//                                    return OSversion.Win7oWin2008ServerR2;
//                                    break;
//                                case 2:
//                                    return OSversion.Win8oWin2012Server;
//                                default:
//                                    return OSversion.Unknown;
//                            }
//                    }
//                    break;
//
//                case PlatformID.WinCE:
//                    return OSversion.WinCE;
//            }
//
//            return OSversion.Unknown;
//        }

        public static class OSSession
        {
            [DllImport("user32.dll", SetLastError = true)]
            private static extern bool LockWorkStation();

            private const int SwHide = 0;
            private const int SwShow = 1;

            public static bool LockSession()
            {
                var result = LockWorkStation();

                if (result == false)
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }

                return true;
            }
        }

        /// <summary>
        /// Helper class for hiding/showing the taskbar and startmenu on
        /// Windows XP and Vista.
        /// </summary>
        private static class Taskbar
        {
            [DllImport("user32.dll")]
            private static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

            [DllImport("user32.dll", CharSet = CharSet.Auto)]
            private static extern bool EnumThreadWindows(int threadId, EnumThreadProc pfnEnum, IntPtr lParam);

            [DllImport("user32.dll", SetLastError = true)]
            private static extern System.IntPtr FindWindow(string lpClassName, string lpWindowName);

            [DllImport("user32.dll", SetLastError = true)]
            private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

            [DllImport("user32.dll")]
            private static extern IntPtr FindWindowEx(IntPtr parentHwnd, IntPtr childAfterHwnd, IntPtr className, string windowText);

            [DllImport("user32.dll")]
            private static extern int ShowWindow(IntPtr hwnd, int nCmdShow);

            [DllImport("user32.dll")]
            private static extern uint GetWindowThreadProcessId(IntPtr hwnd, out int lpdwProcessId);

            private const int SwHide = 0;
            private const int SwShow = 5;

            private const string VistaStartMenuCaption = "Start";
            private static IntPtr _vistaStartMenuWnd = IntPtr.Zero;

            private delegate bool EnumThreadProc(IntPtr hwnd, IntPtr lParam);

            /// <summary>
            /// Show the taskbar.
            /// </summary>
            public static void Show()
            {
                SetVisibility(true);
            }

            /// <summary>
            /// Hide the taskbar.
            /// </summary>
            public static void Hide()
            {
                SetVisibility(false);
            }

            /// <summary>
            /// Sets the visibility of the taskbar.
            /// </summary>
            public static bool Visible
            {
                set { SetVisibility(value); }
            }

            /// <summary>
            /// Hide or show the Windows taskbar and startmenu.
            /// </summary>
            /// <param name="show">true to show, false to hide</param>
            private static void SetVisibility(bool show)
            {
                // get taskbar window
                IntPtr taskBarWnd = FindWindow("Shell_TrayWnd", null);

                // try it the WinXP way first...
                IntPtr startWnd = FindWindowEx(taskBarWnd, IntPtr.Zero, "Button", "Start");

                if (startWnd == IntPtr.Zero)
                {
                    // try an alternate way, as mentioned on CodeProject by Earl Waylon Flinn
                    startWnd = FindWindowEx(IntPtr.Zero, IntPtr.Zero, (IntPtr)0xC017, "Start");
                }

                if (startWnd == IntPtr.Zero)
                {
                    // ok, let's try the Vista easy way...
                    startWnd = FindWindow("Button", null);

                    if (startWnd == IntPtr.Zero)
                    {
                        // no chance, we need to to it the hard way...
                        startWnd = GetVistaStartMenuWnd(taskBarWnd);
                    }
                }

                ShowWindow(taskBarWnd, show ? SwShow : SwHide);
                ShowWindow(startWnd, show ? SwShow : SwHide);
            }

            /// <summary>
            /// Returns the window handle of the Vista start menu orb.
            /// </summary>
            /// <param name="taskBarWnd">windo handle of taskbar</param>
            /// <returns>window handle of start menu</returns>
            private static IntPtr GetVistaStartMenuWnd(IntPtr taskBarWnd)
            {
                // get process that owns the taskbar window
                int procId;
                GetWindowThreadProcessId(taskBarWnd, out procId);

                try
                {
                    var p = Process.GetProcessById(procId);
                    // enumerate all threads of that process...
                    foreach (ProcessThread t in p.Threads)
                    {
                        EnumThreadWindows(t.Id, MyEnumThreadWindowsProc, IntPtr.Zero);
                    }
                }catch{}
                
                return _vistaStartMenuWnd;
            }

            /// <summary>
            /// Callback method that is called from 'EnumThreadWindows' in 'GetVistaStartMenuWnd'.
            /// </summary>
            /// <param name="hWnd">window handle</param>
            /// <param name="lParam">parameter</param>
            /// <returns>true to continue enumeration, false to stop it</returns>
            private static bool MyEnumThreadWindowsProc(IntPtr hWnd, IntPtr lParam)
            {
                var buffer = new StringBuilder(256);
                if (GetWindowText(hWnd, buffer, buffer.Capacity) <= 0) return true;
                if (buffer.ToString() != VistaStartMenuCaption) return true;
                _vistaStartMenuWnd = hWnd;
                return false;
            }
        }
    }
}
