﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Windows.Forms;
using gcperu.Term.Agents;
using GITSNavega.UI.UserControls.Pantalla;

namespace gcperu.Term.UI
{
    public partial class Form1 : Form
    {
        readonly IScheduler FormScheduler;
        private IDisposable _cleanUpHooker = null;

        public Form1()
        {
            FormScheduler = new ControlScheduler(this);
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            CloseButton.Click += (sender, args) => Close();
            AdministrationButton.Click += (sender, args) =>
            {
                foreach (Control control in splitContainer1.Panel1.Controls)
                    splitContainer1.Panel1.Controls.Remove(control);
                splitContainer1.Panel1.Controls.Add(new PantallaOpcion() {Dock = DockStyle.Fill});

            };

            base.OnLoad(e);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _cleanUpHooker = System.Reactive.Disposables.Disposable.Empty;
//            _cleanUpHooker = this.GetGlobalHooker();
            
            InterProcessAgent.ServerConnectionObservable
                .ObserveOn(FormScheduler)
                .Subscribe(ServerConnect);

            InterProcessAgent.BatteryStatusObservable
                .ObserveOn(FormScheduler)
                .Subscribe(BatteryStatus);

            InterProcessAgent.OdometerObservable
                .ObserveOn(FormScheduler)
                .Subscribe( d =>OdometerLabel.Text = string.Format("{0:0.0} Km", d.Kilometers));
        }

        protected override void OnClosed(EventArgs e)
        {
            _cleanUpHooker.Dispose();
            base.OnClosed(e);
        }
    }
}
