﻿namespace gcperu.Term.UI
{
    partial class AdministrationPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AdminUnlockButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AdminUnlockButton
            // 
            this.AdminUnlockButton.Location = new System.Drawing.Point(524, 379);
            this.AdminUnlockButton.Name = "AdminUnlockButton";
            this.AdminUnlockButton.Size = new System.Drawing.Size(75, 23);
            this.AdminUnlockButton.TabIndex = 0;
            this.AdminUnlockButton.Text = "Desbloquear";
            this.AdminUnlockButton.UseVisualStyleBackColor = true;
            // 
            // AdministrationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.AdminUnlockButton);
            this.Name = "AdministrationPanel";
            this.Size = new System.Drawing.Size(602, 405);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button AdminUnlockButton;
    }
}
