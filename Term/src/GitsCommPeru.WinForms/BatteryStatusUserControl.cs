﻿using System;
using System.Drawing;
using System.Reactive;
using System.Reactive.Subjects;
using System.Windows.Forms;
using gcperu.Term.Enum;
using gcperu.Term.UI.Properties;

namespace gcperu.Term.UI
{
    public partial class BatteryStatusUserControl : UserControl, IObserver<BatteryStatusEnum>
    {
        private IDisposable _subscription = null;

        private readonly ISubject<BatteryStatusEnum> _batteryStatusInternalSubject
            = new BehaviorSubject<BatteryStatusEnum>(BatteryStatusEnum.Unknown);

        public IObserver<BatteryStatusEnum> ServerConnectionObserver
        {
            get { return _batteryStatusInternalSubject.AsObserver(); }
        }

        public BatteryStatusUserControl()
        {
            InitializeComponent();

            HandleCreated += (sender, args) =>
            {
                if (_subscription != null)
                    _subscription.Dispose();

                _subscription = _batteryStatusInternalSubject
                    .Subscribe(incomingStatus =>
                    {
                        switch (incomingStatus)
                        {
                            case BatteryStatusEnum.Charging | BatteryStatusEnum.Level0:
                                Picture.Image = Resources.batt_lev0_char;
                                break;
                            case BatteryStatusEnum.Charging | BatteryStatusEnum.Level1:
                                Picture.Image = Resources.batt_lev1_char;
                                break;
                            case BatteryStatusEnum.Charging | BatteryStatusEnum.Level2:
                                Picture.Image = Resources.batt_lev2_char;
                                break;
                            case BatteryStatusEnum.Charging | BatteryStatusEnum.Level3:
                                Picture.Image = Resources.batt_lev3_char;
                                break;
                            case BatteryStatusEnum.Charging | BatteryStatusEnum.Critical:
                                Picture.Image = Resources.batt_crit_char;
                                break;
                            case BatteryStatusEnum.Discharging | BatteryStatusEnum.Level0:
                                Picture.Image = Resources.batt_lev0_disc;
                                break;
                            case BatteryStatusEnum.Discharging | BatteryStatusEnum.Level1:
                                Picture.Image = Resources.batt_lev1_disc;
                                break;
                            case BatteryStatusEnum.Discharging | BatteryStatusEnum.Level2:
                                Picture.Image = Resources.batt_lev2_disc;
                                break;
                            case BatteryStatusEnum.Discharging | BatteryStatusEnum.Level3:
                                Picture.Image = Resources.batt_lev3_disc;
                                break;
                            case BatteryStatusEnum.Discharging | BatteryStatusEnum.Critical:
                                Picture.Image = Resources.batt_crit_disc;
                                break;
                            default: // BatteryStatus unkown
                                Picture.Image = Resources.batt_unknown;
                                break;
                        }
                    });
            };

            HandleDestroyed += (sender, args) =>
            {
                _subscription.Dispose();
            };
        }

        public void OnNext(BatteryStatusEnum value)
        {
            _batteryStatusInternalSubject.OnNext(value);
        }

        public void OnError(Exception error)
        {
            _batteryStatusInternalSubject.OnError(error);
        }

        public void OnCompleted()
        {
            _batteryStatusInternalSubject.OnCompleted();
        }
        
    }
}
