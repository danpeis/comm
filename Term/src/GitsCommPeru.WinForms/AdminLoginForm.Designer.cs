﻿using gcperu.Term.UI.UserControls;
using GITSNavega.UI.UserControls;

namespace GITSNavega.UI
{
    sealed partial class AdminLoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtLogin = new DevExpress.XtraEditors.ButtonEdit();
            this.keyboardNumeric1 = new KeyboardNumeric();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogin.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtLogin
            // 
            this.txtLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLogin.EditValue = "";
            this.txtLogin.Location = new System.Drawing.Point(99, 12);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 36F);
            this.txtLogin.Properties.Appearance.Options.UseFont = true;
            serializableAppearanceObject1.BackColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject1.Options.UseBackColor = true;
            
            this.txtLogin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "1", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::gcperu.Term.UI.Properties.Resources.Escoba.ToBitmap(), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "2", 50, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::gcperu.Term.UI.Properties.Resources.Cancel3.ToBitmap(), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Cerrar Pantalla")});
            this.txtLogin.Properties.PasswordChar = '*';
            this.txtLogin.Size = new System.Drawing.Size(626, 64);
            this.txtLogin.TabIndex = 7;
            this.txtLogin.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtLogin_ButtonClick);
            // 
            // keyboardNumeric1
            // 
            this.keyboardNumeric1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.keyboardNumeric1.Location = new System.Drawing.Point(99, 99);
            this.keyboardNumeric1.Name = "keyboardNumeric1";
            this.keyboardNumeric1.Size = new System.Drawing.Size(626, 255);
            this.keyboardNumeric1.TabIndex = 6;
            this.keyboardNumeric1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.keyboardNumeric1_KeyPress);
            // 
            // AdminLoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(797, 378);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.keyboardNumeric1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AdminLoginForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.AdminLoginForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtLogin.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private KeyboardNumeric keyboardNumeric1;
        private DevExpress.XtraEditors.ButtonEdit txtLogin;

    }
}