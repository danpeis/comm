﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Security;

namespace GITSNavega.UI
{
    public sealed partial class AdminLoginForm : Form
    {

        private static readonly SecureString Pass = new SecureString();
        /// <summary>
        /// Constructor
        /// </summary>
        public AdminLoginForm()
        {
            Pass.AppendChar('4');
            Pass.AppendChar('8');
            Pass.AppendChar('2');
            Pass.AppendChar('5');
            Pass.AppendChar('7');
            Pass.AppendChar('0');
            InitializeComponent();
        }

        private bool ValidatePassword()
        {
            return txtLogin.Text == Pass.ToString();
        }

        private void keyboardNumeric1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (ValidatePassword())
                {
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    txtLogin.Text = "";
                    txtLogin.Focus();
                }
                return;
            }
            if (!txtLogin.Focused) txtLogin.Focus();

            Application.DoEvents();
            SendKeys.Send(e.KeyChar.ToString());
            Application.DoEvents();
        }

        private void AdminLoginForm_Load(object sender, EventArgs e)
        {
            txtLogin.Focus();
        }

        private void txtLogin_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Caption == "1")
                txtLogin.Text = "";
            else
                this.Close();
        }

    }

    public static class Administrator
    {
        private static int _adminUnlocked = 0;

        public static void Lock()
        {
            Interlocked.Exchange(ref _adminUnlocked, 0);
        }

        public static bool IsAdminLocked {get { return _adminUnlocked == 0; }}
        public static bool IsAdminUnlocked { get { return _adminUnlocked == 1; } }

        public static IObservable<bool> Validate(bool changeAdminLock = false)
        {
            return Observable.Create<bool>(o =>
            {
                if (_adminUnlocked == 0)
                {
                    var f = new AdminLoginForm();
                    var value = f.ShowDialog() == DialogResult.OK ? 1 : 0;
                    if (changeAdminLock)
                        Interlocked.Exchange(ref _adminUnlocked, value);
                    else
                    {
                        o.OnNext(value == 1);
                        o.OnCompleted();
                        f.Dispose();
                        return Disposable.Empty;
                    }
                    f.Dispose();
                }
                
                o.OnNext(_adminUnlocked == 1);
                o.OnCompleted();
                return Disposable.Empty;
            });
        }
    }
}
