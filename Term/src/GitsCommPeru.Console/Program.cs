﻿using System;
using RxTools.IO;
using Splat;

namespace gcperu.Term.Con
{
    public class ConsoleWriter : IWriter
    {
        public void Write(string tag, string text)
        {
            Console.WriteLine("{0}", text);
        }
    }

    class Program
    {
        private static void Main(string[] args)
        {
            Locator.CurrentMutable.RegisterConstant(new ConsoleWriter(), typeof(IWriter));

            new GitsCommApp().Run(args);

//            Agents.ConnectionAgent.ConnectivityChanges().Subscribe(_ => Console.WriteLine(_.IsConnected));
            Console.ReadLine();
        }
    }
}
