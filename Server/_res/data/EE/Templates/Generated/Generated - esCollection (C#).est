<% 
/*
=========================================================================
    
    EntitySpaces 2009 
    Persistence Layer and Business Objects for Microsoft .NET 
    Copyright 2005 - 2009 EntitySpaces, LLC 
    EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC 
    http://www.entityspaces.net 

    This template is bound by the EntitySpaces License Agreement which 
    is located here:

    http://www.entityspaces.net/portal/License/tabid/97/Default.aspx
    
=========================================================================
*/
%><%@ TemplateInfo 
    UniqueID="9D9C3562-88A0-4e9b-B706-6AA92BCC8C53" 
    Title="Generated - esCollection (C#)"   
    Description="The Abstract Collection Class" 
    Namespace="EntitySpaces.2009.C#.Generated" 
    Author="EntitySpaces, LLC"
    Version="2009.0.0.0"
    IsSubTemplate="True" %><%
//-------------------------------------------------------
// Extract the UI choices that we need
//-------------------------------------------------------	
bool TargetTheCompactFramework = (bool)esMeta.Input["TargetTheCompactFramework"];
string Namespace = (string)esMeta.Input["Namespace"];
bool GenerateSingleFile = (bool)esMeta.Input["GenerateSingleFile"];
bool UseDnnObjectQualifier = (bool)esMeta.Input["UseDnnObjectQualifier"];
bool UseCustomBaseClass = (bool)esMeta.Input["UseCustomBaseClass"];
bool SerializableQueries = (bool)esMeta.Input["SerializableQueries"];
bool WcfSupport = (bool)esMeta.Input["WcfSupport"];
bool LINQtoSQL = (bool)esMeta.Input["LINQtoSQL"];

//-------------------------------------------------------
// Create Local Variables
//-------------------------------------------------------
string comma;

//-------------------------------------------------------
// Begin Execution
//-------------------------------------------------------
ITable table = (ITable)esMeta.Input["Table"];
IView view = (IView)esMeta.Input["View"];
IColumns cols = (IColumns)esMeta.Input["Columns"];
esPluginSource source = (esPluginSource)esMeta.Input["Source"];

%>/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
						  esCollection.est
===============================================================================
EntitySpaces Version : <%=esMeta.esPlugIn.esVersion%>
EntitySpaces Driver  : <%=esMeta.esPlugIn.esDriver%><%if(!esMeta.esPlugIn.TurnOffDateTimeInClassHeaders){%>
Date Generated       : <%=DateTime.Now.ToString()%><%}%>
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;<%if(LINQtoSQL) {%>
using System.Data.Linq;
using System.Data.Linq.Mapping;<%}%>
using System.ComponentModel;
<%if(!TargetTheCompactFramework){ %>using System.Xml.Serialization;<%}%>
<%if(WcfSupport || SerializableQueries){ %>using System.Runtime.Serialization;<%}%>

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;

<% if(UseDnnObjectQualifier){%>
using DotNetNuke.Framework.Providers;
<%}%>

namespace <%=Namespace%>
{
<%if(!TargetTheCompactFramework){%>
	//esCollection.est
	[Serializable]<%} if (UseCustomBaseClass) {%>
	abstract public class <%=esMeta.esPlugIn.esCollection(source)%> : CollectionBase<%}else{%>
	abstract public class <%=esMeta.esPlugIn.esCollection(source)%> : esEntityCollection<%}%>
	{
		public <%=esMeta.esPlugIn.esCollection(source)%>()
		{

		}

		protected override string GetCollectionName()
		{
			return "<%=esMeta.esPlugIn.Collection(source)%>";
		}

		#region Query Logic
		protected void InitQuery(<%=esMeta.esPlugIn.esQuery(source)%> query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es2.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as <%=esMeta.esPlugIn.esQuery(source)%>);
		}
		#endregion
		
		virtual public <%=esMeta.esPlugIn.Entity(source)%> DetachEntity(<%=esMeta.esPlugIn.Entity(source)%> entity)
		{
			return base.DetachEntity(entity) as <%=esMeta.esPlugIn.Entity(source)%>;
		}
		
		virtual public <%=esMeta.esPlugIn.Entity(source)%> AttachEntity(<%=esMeta.esPlugIn.Entity(source)%> entity)
		{
			return base.AttachEntity(entity) as <%=esMeta.esPlugIn.Entity(source)%>;
		}
		
		virtual public void Combine(<%=esMeta.esPlugIn.Collection(source)%> collection)
		{
			base.Combine(collection);
		}
		
		new public <%=esMeta.esPlugIn.Entity(source)%> this[int index]
		{
			get
			{
				return base[index] as <%=esMeta.esPlugIn.Entity(source)%>;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(<%=esMeta.esPlugIn.Entity(source)%>);
		}
	}
<%if(!GenerateSingleFile){ %>
}<%}%>
<script runat="template">

public string GetFileName()
{
	return esMeta.esPlugIn.esCollection((esPluginSource)esMeta.Input["Source"]) + ".cs";
}

public EntitySpaces.MetadataEngine.Root TheMetaData
{
	get { return esMeta;  }
	set { esMeta = value; }	
}

</script>	