﻿using System.Collections.Generic;
using System.Drawing;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;

namespace gcperu.Server.Core.InterComm
{
    namespace GComunica.Core.IPC
    {

        public enum MessagesIPCTypesEnum : int
        {
            StartListen,
            StopListen,
            RebootListen,
            DisconnectTerminal,
            RefreshConfiguracion,
            /// <summary>
            /// Utilizado para obtener el State del Socket, Listening/Stopped. Valor devuelto lo indica.
            /// </summary>
            SocketStatus
        }

        public enum IpcChannelSenderTypes
        {
            WSWoker,
            WSSentry,
            ConsoleUI
        }
        
        public interface IMessageIpcData
        {
            string FullTypeName { get; }
        }

        public class MessageIPCinfo
        {

            public static readonly Color Logcolor = System.Drawing.Color.DarkSeaGreen;

            //Utilizao para serializacion
            public MessageIPCinfo()
            {
            }

            public MessageIPCinfo(IpcChannelSenderTypes sender, MessagesIPCTypesEnum tipo,IMessageIpcData data)
            {
                Tipo = tipo;
                Sender = sender; 
                Data = data;
            }

            public MessagesIPCTypesEnum Tipo { get; private set; }

            public IpcChannelSenderTypes Sender { get; private set; }

            public IMessageIpcData Data { get; private set; }

        }

        public class DisconnectMessageIpcData : IMessageIpcData
        {
            public DisconnectMessageIpcData()
            {
                FullTypeName = this.GetType().FullName;
            }

            public DisconnectMessageIpcData(List<decimal> listaDisconnect) : this()
            {
                ListaDisconnect = listaDisconnect;
            }

            public List<decimal> ListaDisconnect;
            public string FullTypeName { get; private set; }
        }

        public class SocketstatusMessageIpcData : IMessageIpcData
        {
            public SocketstatusMessageIpcData()
            {
                FullTypeName = this.GetType().FullName;
            }

            public SocketstatusMessageIpcData(bool listening)
                : this()
            {
                Listening = listening;
            }

            public bool Listening { get; private set; }

            public string FullTypeName { get; private set; }
        }

    }
}