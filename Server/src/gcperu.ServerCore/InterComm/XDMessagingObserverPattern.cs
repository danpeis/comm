﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Newtonsoft.Json;
using XDMessaging;
using XDMessaging.Entities;

namespace gcperu.Server.Core.InterComm
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public interface IDisposableObserver<in T> : IObserver<T>, IDisposable
    {

    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class XDMessagingObserverPattern
    {

        private class DisposableObserver<T> : ObserverBase<T>, IDisposableObserver<T>
        {
            public DisposableObserver(string channel, int bufferSize, T firstData)
            {
                _channel = channel;

                if (bufferSize < 1)
                    _subject = new Subject<T>();
                else if (bufferSize == 1)
                    _subject = new BehaviorSubject<T>(firstData);
                else
                    _subject = new ReplaySubject<T>(bufferSize);

                _cleanUp.Add((IDisposable)_subject);

                _cleanUp.Add(_subject.Subscribe(value =>
                {
                    MessageSender.SendToChannel(_channel, JsonConvert.SerializeObject(value));
                }));

                _cleanUp.Add(RegisterObservableOnChannel<Unit>(_channel + ReplayChannelAppend)
                    .Subscribe(_ =>
                    {
                        _subject.Subscribe(value =>
                        {
                            MessageSender.SendToChannel(_channel, JsonConvert.SerializeObject(value));
                        }).Dispose();
                    }));
            }

            void IDisposable.Dispose()
            {
                base.Dispose();
                _cleanUp.Dispose();
            }

            private readonly CompositeDisposable _cleanUp = new CompositeDisposable();
            private readonly ISubject<T> _subject;
            private readonly string _channel;

            protected override void OnNextCore(T data)
            {
                _subject.OnNext(data);
            }

            protected override void OnErrorCore(Exception error)
            {
                throw error;
            }

            protected override void OnCompletedCore()
            {

            }
        }


        private readonly static Dictionary<string, RefCountDisposable> RefCountObservableDisposableDictionary
            = new Dictionary<string, RefCountDisposable>();
        private readonly static Dictionary<string, object> BroadcasterDictionary
            = new Dictionary<string, object>();

        private static readonly IXDListener MessageListener;
        private static readonly IXDBroadcaster MessageSender;
        private const string ReplayChannelAppend = "__Replay__Channel__Do__Not__Use";

        static XDMessagingObserverPattern()
        {
            var messagingClient = new XDMessagingClient();
            MessageListener = messagingClient.Listeners.GetListenerForMode(XDTransportMode.Compatibility);
            MessageSender = messagingClient.Broadcasters.GetBroadcasterForMode(XDTransportMode.Compatibility);
        }

        /// <summary>
        /// Register a broadcast observer on certain channel with the specified bufferSize
        /// throws InvalidOperation if there is a registration on the same channel.
        /// </summary>
        /// <typeparam name="T">The type to cast from</typeparam>
        /// <param name="channel">The channel name</param>
        /// <param name="bufferSize">The cached buffer size defaults to 1. Any number below 1 is treated as a buffer of 0 items.</param>
        /// <param name="firstData">First item of the sequence default(T) if not specified.</param>
        /// <returns>The recently registered Observable</returns>
        public static IDisposableObserver<T> GetOrRegisterBroadcastObserverOnChannel<T>(this string channel, int bufferSize = 1, T firstData = default(T))
        {
            IDisposableObserver<T> observer;
            lock (BroadcasterDictionary)
            {
                if (BroadcasterDictionary.ContainsKey(channel))
                    return (IDisposableObserver<T>)BroadcasterDictionary[channel];
                observer = new DisposableObserver<T>(channel, bufferSize, firstData);
                BroadcasterDictionary.Add(channel, observer);
            }
            return observer;
        }

        /// <summary>
        /// Register an IObservable<T> listening on a channel
        /// </summary>
        /// <typeparam name="T">the param type to cast to</typeparam>
        /// <param name="channel">the working channel</param>
        /// <param name="requestReplay">if false, it does not request a replay of the cached data on the broadcaster</param>
        /// <returns>The observable registered</returns>
        public static IObservable<T> RegisterObservableOnChannel<T>(this string channel, bool requestReplay = true)
        {
            return Observable.Create<T>(o =>
            {
                // handler to register for this to receive
                Listeners.XDMessageHandler handler = delegate(object sender, XDMessageEventArgs args)
                {
                    if (args.DataGram.Channel == channel)
                        o.OnNext(JsonConvert.DeserializeObject<T>(args.DataGram.Message));
                };

                // Handler registration
                MessageListener.MessageReceived += handler;

                // If the channel is not already registered, register the channel and the proper unregistration when not yet under use.
                if (!RefCountObservableDisposableDictionary.ContainsKey(channel) || RefCountObservableDisposableDictionary[channel].IsDisposed)
                {
                    MessageListener.RegisterChannel(channel);
                    if (RefCountObservableDisposableDictionary.ContainsKey(channel))
                        RefCountObservableDisposableDictionary.Remove(channel);
                    RefCountObservableDisposableDictionary.Add(channel, new RefCountDisposable(Disposable.Create(
                        () =>
                        {
                            MessageListener.UnRegisterChannel(channel);
                            RefCountObservableDisposableDictionary.Remove(channel);
                        }
                    )));
                    if (requestReplay)
                        MessageSender.SendToChannel(channel + ReplayChannelAppend, JsonConvert.SerializeObject(Unit.Default));
                    // Send message to broadcasters to replay the cached value.
                }

                // when subscription disposes decrease the count to unregister on the channel and unsubcribe the handler.
                return new CompositeDisposable(
                    RefCountObservableDisposableDictionary[channel].GetDisposable(),
                    Disposable.Create(() => MessageListener.MessageReceived -= handler)
                );
            });
        }
    }
}

