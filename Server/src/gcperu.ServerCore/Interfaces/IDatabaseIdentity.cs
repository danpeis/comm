﻿namespace gcperu.Server.Core.Interfaces
{
    public interface IDatabaseIdentity
    {
        string User { get; } 
        string Password { get; } 
        string ApplicationName { get; } 
    }
}