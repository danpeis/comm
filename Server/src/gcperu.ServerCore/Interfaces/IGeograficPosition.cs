﻿using System;

namespace gcperu.Server.Core.Interfaces
{
    public interface IGeograficPosition
    {
        double GpsLatitud { get; set; }
        double GpsLongitud { get; set; }
        byte? GpsFixGeoTipoVia { get; set; }
        string GpsFixGeoTx { get; set; }
        DateTime? GpsFecha { get; set; }
    }
}