﻿using gitspt.global.Core.Util.Ficheros;

namespace gcperu.Server.Core.Extension
{
    public static class FileVersionExtension
    {
         public static bool Validate(this FileVersion fileversion,string version)
         {
             return new FileVersion(version).MenorIgualQue(new FileVersion("99999.9999.9999.9999"));
         }
    }
}