﻿using System;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using gcperu.Server.Core.Info;
using gcperu.Server.Core.Interfaces;
using gitspt.global.Core.Enums;
using gitspt.global.Core.Exceptions;
using gitspt.global.Core.Extension;
using gitspt.global.Core.Log;
using gitspt.global.Core.Util;

namespace gcperu.Server.Core.Extension
{
    public static class DatabaseInfoExtension
    {
        public static string GetConnectionString(this DatabaseInfo data, IDatabaseIdentity identity, int timeout = 15)
        {
            if (!data.IntegratedSecurity)
                return string.Format("Data Source={3};Initial Catalog={2};User ID={0};Password={1};Application Name={4},Connect Timeout={5}", identity.User, identity.Password, data.DBName, data.Host, identity.ApplicationName, timeout);

            return string.Format("Data Source={0};Initial Catalog={1};Integrated Security=true;Application Name={2};Connect Timeout={3}", data.Host, data.DBName, identity.ApplicationName, timeout);
        }

        public static bool CheckConnection(this DatabaseInfo data, IDatabaseIdentity identity, int timeout, ISSTraceLog log = null)
        {
            try
            {
                using (var cnxsql = new SqlConnection(data.GetConnectionString(identity, timeout)))
                {
                    cnxsql.CheckConnection();
                }

                data.Verificado = true;

                return true;
            }
            catch (Exception ex)
            {
                if (log != null)
                    log.LogException(ex);

                return false;
            }
        }

        public static esConnectionElement CreateEntitySpacesDatabaseConnection(this DatabaseInfo dbinfo, string cnxname,IDatabaseIdentity identity, int timeout, ISSTraceLog log = null)
        {
            string cnxstring = dbinfo.GetConnectionString(identity, timeout);

            if (esConfigSettings.ConnectionInfo.Connections[cnxname] != null)
                esConfigSettings.ConnectionInfo.Connections.Remove(cnxname);

            //Conexion a la BD de campañas
            var escnx = new esConnectionElement
            {
                Name = cnxname,
                Catalog = dbinfo.DBName,
                ConnectionString = cnxstring,
                Provider = "EntitySpaces.SqlClientProvider",
                ProviderClass = "DataProvider",
                SqlAccessType = esSqlAccessType.DynamicSQL,
                ProviderMetadataKey = "esDefault",
                DatabaseVersion = "2005"
            };

            esConfigSettings.ConnectionInfo.Connections.Add(escnx);

            if (log!=null)
                log.LogDebug(string.Format("[CreateEntitySpacesDatabaseConnection]. Cnxstring BD '{0}: {1}", dbinfo.DBName, cnxstring));

            return escnx;
        }
    }
}