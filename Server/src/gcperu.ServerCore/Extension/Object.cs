﻿using System;

namespace gcperu.Server.Core.Extension
{
    public static class ObjectExtension
    {
        public static DateTime? ObjectToDateTime(this object obj)
        {
            try
            {
                DateTime valor;
                if (obj == null || obj.ToString() == "") return null;
                return DateTime.TryParse(obj.ToString(), out valor) ? valor : (DateTime?)null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}