﻿using gcperu.Server.Core.Interfaces;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;

namespace gcperu.Server.Core.Extension
{
    public static class LogExt
    {
         public static ISSTraceLog Log(this IEnableLog obj)
         {
             return Ioc.Container.Get<ISSTraceLog>();
         }
    }
}