﻿using System;
using gcperu.Server.Core.InterComm;
using gitspt.global.Core.Log;
using ServiceStack.Text;

namespace gcperu.Server.Core.Extension
{
    public static class InterCommExt
    {
          public static void Send<T>(this IDisposableObserver<T> sender,T data,Action logaction=null)
          {
              if (logaction!=null)
                  logaction.Invoke();

              sender.OnNext(data);
          }

          public static void Send<T>(this IDisposableObserver<T> sender, T data,ISSTraceLog log) where T:class
          {
              log.LogDebug(string.Format("IPC:Message Send.\n{0}", data.Dump()));
              sender.OnNext(data);
          }

    }
}