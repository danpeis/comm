﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using gcperu.Contract;

namespace gcperu.Server.Core.Info
{
    [DataContract(Name = "TerminalUpdateInfo")]
    public class TerminalUpdateInfo : ICloneable
    {
        public TerminalUpdateInfo()
        {
            SoftwareTipo = TerminalSoftwareTipo.GNavWin8Dsk;
            UpdateMode = SoftwareUpateMode.Automatico;
        }

        [DataMember(Name = "UpdateMode", Order = 1)]
        public SoftwareUpateMode UpdateMode { get; set; }

        [DataMember(Name = "SoftwareTipo", Order = 2)]
        public TerminalSoftwareTipo SoftwareTipo { get; set; }

        [DataMember(Name = "VersionRequerida", Order = 3)]
        public string VersionRequerida { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    [CollectionDataContract(Name = "ListaTerminalUpdateInfo")]
    public class ListaTerminalUpdateInfo : List<TerminalUpdateInfo>, ICloneable
    {
        public object Clone()
        {
            var lsclon = new ListaTerminalUpdateInfo();
            lsclon.AddRange(this.Select(item => (TerminalUpdateInfo)item.Clone()));

            return lsclon;
        }
    }
}