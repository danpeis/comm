﻿using System;
using gcperu.Server.Core.Interfaces;

namespace gcperu.Server.Core.Info
{
    public class DatabaseIdentity : IDatabaseIdentity
    {
        public DatabaseIdentity()
        {
            User = Constantes.BDCNX_USER;
            Password = Constantes.BDCNX_PASSWORD;
            ApplicationName = Constantes.BDCNX_APPNAME;
        }

        public DatabaseIdentity(string user, string password)
        {
            User = user;
            Password = password;
            ApplicationName = "";
        }

        public DatabaseIdentity(string user, string password,string appname)
        {
            User = user;
            Password = password;
            ApplicationName = appname;
        }


        public string User { get; private set; }

        public string Password { get; private set; }

        public string ApplicationName { get; private set; }
    }

    public class DatabaseInfo
    {
        private string _host;

        public string ID { get; set; }

        public string DBName { get; set; }

        public string Host
        {
            get { return _host; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    value = "";

                if (!String.Equals(_host, value, StringComparison.CurrentCultureIgnoreCase))
                    Verificado = false;  //Indicamos que la BD no está verificada

                _host = value;
            }
        }

        public string User { get; set; }
        public string Password { get; set; }

        public bool IntegratedSecurity { get; set; }

        public bool Verificado { get; set; }

        #region Functions

        public DatabaseInfo Clone()
        {
            return (DatabaseInfo)this.MemberwiseClone();
        }

        #endregion
    }
}