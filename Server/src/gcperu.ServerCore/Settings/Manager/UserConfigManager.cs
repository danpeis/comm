﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;
using Newtonsoft.Json;
using ServiceStack.Text;

namespace GCPeru.Server.Core.Settings.Manager
{
    public interface IUserConfigManager:IConfigManager
    {
        LayoutOptions CreateLayoutOptions();
    }

   public class UserConfigManager : ConfigManagerBase, IUserConfigManager, IDisposable
    {
        private static ISSTraceLog _log;

        private const string USER_FILECONFIG = "UserConfig.json";
        private const decimal USER_DEFAULT_ID = 0;

        private decimal _usuarioId;

        public UserConfigManager()
        {
            _log = Ioc.Container.Get<ISSTraceLog>();

            _usuarioId = USER_DEFAULT_ID; //EN ESTA APLICACION NO HAY USUARIOS, POR TANTO, SE UTILIZA ESTE PARA COMPATIBILIZAR CON CLASES QUE LO NECESITAN
            PathUserBase = ConfigurePathLocalUser();
            Fileconfig = Path.Combine(DirectoryBase, USER_FILECONFIG);

            _log.LogDebug(string.Format("UserConfigManager.Ctor: PathAppBase:{0} FileBD:{1}", PathUserBase, Fileconfig));

            if (!File.Exists(Fileconfig))
                _log.LogDebug("UserConfigManager.Ctor: Fichero de configuracion no existe.");

        }

        
        public string Fileconfig { get; protected set; }

        public string PathUserBase { get; private set; }

        public LayoutOptions CreateLayoutOptions()
        {
            return new LayoutOptions(PathUserBase, _usuarioId);
        }

        public T Load<T>() where T : class
        {
            //Obtenemos la configuracion inicial o por defecto, si no existe un usuario aun cargado
            UserSetting setting = null;
            try
            {
                var objserialized = File.ReadAllText(Fileconfig, Encoding.UTF8);
                setting = JsonConvert.DeserializeObject<UserSetting>(objserialized);

                _log.LogDebug(string.Format("UserConfigManager.Load. Cargando información del Usuario: {0} Datos:{1}", _usuarioId, setting.Dump()));
            }
            catch (Exception ex)
            {
                _log.LogException(ex);
                if (setting == null)
                    setting = new UserSetting();
            }

            return setting as T;
        }

        public void Save(object data)
        {
            try
            {
                if (data != null)
                {
                    if (File.Exists(Fileconfig))
                        File.Delete(Fileconfig);

                    var objserialized = JsonConvert.SerializeObject(data, Formatting.Indented);
                    File.WriteAllText(Fileconfig, objserialized);
                }
            }
            catch (Exception ex)
            {
                _log.LogException(ex);
            }
        }

     
        public override void Save()
        {
           
        }

        #region Private Methods

        private string ConfigurePathLocalUser()
        {

            var folderCommondata = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            string strVersion = "1.0";

            string path = Path.Combine(folderCommondata, strVersion);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        #endregion

        public void Dispose()
        {
        }


    }
}