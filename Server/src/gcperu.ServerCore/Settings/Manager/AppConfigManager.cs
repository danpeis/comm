﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;
using Newtonsoft.Json;
using ServiceStack.Text;

namespace GCPeru.Server.Core.Settings.Manager
{
    public interface IAppConfigManager : IConfigManager
    {
    }

    public class AppConfigManager : ConfigManagerBase, IAppConfigManager, IDisposable
    {
        private static ISSTraceLog _log;

        private const string FILECONFIG = "AppConfig.json";

        public AppConfigManager()
        {
            _log = Ioc.Container.Get<ISSTraceLog>();

            DirectoryBase = AppDomain.CurrentDomain.BaseDirectory;
            Fileconfig = Path.Combine(DirectoryBase, FILECONFIG);

            _log.LogDebug(string.Format("AppConfigManager.Ctor: Directorio:{0} FileBD:{1}", DirectoryBase, Fileconfig));

            if (!File.Exists(Fileconfig))
                _log.LogWarning("AppConfigManager.Ctor: Fichero de configuracion no existe.");

        }


        public string Fileconfig { get; protected set; }

        public T Load<T>() where T : class
        {
            //Obtenemos la configuracion inicial o por defecto, si no existe un usuario aun cargado
            AppSetting setting = null;
            try
            {
                if (!File.Exists(Fileconfig))
                {
                    setting = new AppSetting();
                    return setting as T;
                }

                var objserialized=  File.ReadAllText(Fileconfig, Encoding.UTF8);
                setting = JsonConvert.DeserializeObject<AppSetting>(objserialized);

                _log.LogDebug(string.Format("AppConfigManager.Load. {0}", setting.Dump()));
            }
            catch (Exception ex)
            {
                _log.LogException(ex);
                if (setting == null)
                    setting = new AppSetting();
            }

            return setting as T;
        }

        public void Save(object data)
        {
            try
            {
                if (data != null)
                {
                    if (File.Exists(Fileconfig))
                        File.Delete(Fileconfig);

                    var objserialized = JsonConvert.SerializeObject(data, Formatting.Indented);
                    File.WriteAllText(Fileconfig, objserialized);
                }
            }
            catch (Exception ex)
            {
                _log.LogException(ex);
            }
        }


        public override void Save()
        {

        }

        #region Private Methods


        #endregion

        public void Dispose()
        {
        }


    }
}