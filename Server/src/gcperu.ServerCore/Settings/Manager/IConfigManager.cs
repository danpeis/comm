﻿namespace GCPeru.Server.Core.Settings.Manager
{
    public interface IConfigManager
    {
        /// <summary>
        /// Directorio base donde esta guardada la configuración
        /// </summary>
        string DirectoryBase { get; }

        T Load<T>() where T : class;
        void Save(object data);

    }

    public abstract class ConfigManagerBase
    {
        public string DirectoryBase { get; protected set; }

        public abstract void Save();
    }
}