﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using gcperu.Contract;
using gcperu.Server.Core;
using gcperu.Server.Core.Info;
using gitspt.global.Core.IoC;
using GCPeru.Server.Core.Settings.Manager;

namespace GCPeru.Server.Core.Settings
{
    [DataContract]
    public class AppSetting
    {
        private IAppConfigManager _manager;
        private Dictionary<string, DatabaseInfo> _databasesConfig;
        private ConditionalCompileSymbols _compileSymbols;
        private ServerCnfg _serverCnfg;

        public AppSetting()
        {
        }


        #region Properties 


        public Dictionary<string,DatabaseInfo> DatabasesConfig
        {
            get { return _databasesConfig ?? (_databasesConfig = new Dictionary<string, DatabaseInfo>()); }
            private set { _databasesConfig = value; }
        }

        [IgnoreDataMember]
        public string AppVersion { get; set; }

        [IgnoreDataMember]
        public string AppTitle { get; set; }

        [DataMember]
        public ServerCnfg Server
        {
            //Si no esta inyectada, el injectador creara una instancia nueva de esta clase.
            get { return _serverCnfg ?? (_serverCnfg = new ServerCnfg()); }
        }

        #endregion

        [IgnoreDataMember]
        public IAppConfigManager Manager
        {
            get { return _manager; }
            set
            {
                _manager = value;
            }
        }

        [IgnoreDataMember]
        public ConditionalCompileSymbols CompileSymbols
        {
            //Si no esta inyectada, el injectador creara una instancia nueva de esta clase.
            get { return _compileSymbols ?? (_compileSymbols = Ioc.Container.Get<ConditionalCompileSymbols>()); }
        }

     

        #region Nested Classes

        public class ConditionalCompileSymbols
        {
            public ConditionalCompileSymbols()
            {
                EnProduccion = false;
                SaveRestoreLayout = false;
            }

            public bool EnProduccion { get; set; }

            /// <summary>
            /// Get or Set si save/restory layout grid
            /// </summary>
            public bool SaveRestoreLayout { get; set; }
        }

        [DataContract]
        public class ServerCnfg
        {
            private EmailCnfg _emailCnfg;
            private ListaTerminalUpdateInfo _terminalUpdate;

            [DataMember]
            public string UrlEndPoint { get; set; }

            [DataMember]
            public string Empresa { get; set; }

            [DataMember]
            public int ComponentWatchSendInterval { get; set; }

            [DataMember]
            public int ComponentWatchReSendInterval { get; set; }

            [DataMember]
            public int DiarioPosicionesMantenerDias { get; set; }

            [DataMember]
            public SoftwareUpateMode TerminalUpdateMode { get; set; }

            [DataMember]
            public ListaTerminalUpdateInfo TerminalUpdate
            {
                //Si no esta inyectada, el injectador creara una instancia nueva de esta clase.
                get { return _terminalUpdate ?? (_terminalUpdate = new ListaTerminalUpdateInfo()); }
            }

            [DataMember]
            public EmailCnfg EMail
            {
                //Si no esta inyectada, el injectador creara una instancia nueva de esta clase.
                get { return _emailCnfg ?? (_emailCnfg = new EmailCnfg()); }
            }

            [DataMember]
            public LevelLog LevelLog { get; set; }

            /// <summary>
            /// Valor binario indicando las incidencias (enumerado) por la que el sistema debe mandar incidencias
            /// </summary>
            [DataMember]
            public int ThrowIncendeBy { get; set; }
            
            /// <summary>
            /// Nº identificativo del Servidor
            /// </summary>
            [DataMember]
            public short ServerNumber { get; set; }
           
            [DataContract]
            public class EmailCnfg
            {
                 [DataMember]
                 public string EmailServerSmtp { get; set; }

                 [DataMember]
                 public string EmailSecurityUser {get; set; }

                 [DataMember]
                 public string EmailSecurityPwd { get; set; }
                 
                 [DataMember]
                 public string EmailFrom { get; set; }

                 [DataMember]
                 public string EmailListaTo { get; set; }
            }
        }


        #endregion


    }
}