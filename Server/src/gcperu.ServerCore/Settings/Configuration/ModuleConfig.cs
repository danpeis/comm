﻿using System;
using gitspt.global.Core.DevExpress;
using gitspt.global.Core.IoC;
using GCPeru.Server.Core.Settings.Manager;

namespace GCPeru.Server.Core.Settings
{
    public static class ModuleConfig
    {
        private static AppSetting _appConfig;
        private static UserSetting _userConfig;

        private static GlobalStatus _global;
        private static InterfazControls _dxIntefazControls;

        public static UserSetting UserConfig
        {
            get
            {
                if (_userConfig == null)
                {
                     var mng =  Ioc.Container.Get<IUserConfigManager>();
                    _userConfig = mng.Load<UserSetting>();
                    _userConfig.Manager = mng;
                }

                return _userConfig;
            }
        }

        public static AppSetting AppConfig
        {
            get
            {
                if (_appConfig == null)
                {
                    var mng = Ioc.Container.Get<IAppConfigManager>();
                    _appConfig = mng.Load<AppSetting>();
                    _appConfig.Manager = mng;
                }

                return _appConfig;
            }
        }

        public static GlobalStatus Global
        {
            get { return _global ?? (_global = Ioc.Container.Get<GlobalStatus>()); }
        }

        public static InterfazControls DxIntefazControls
        {
            get { return _dxIntefazControls ?? (_dxIntefazControls = new InterfazControls()); }
        }


        public class GlobalStatus
        {
            public object MainForm { get; set; }

            public object ActiveForm { get; set; }
            public int DatabaseVersion { get; set; }

            public bool SessionAdminActive { get; set; }
        }

        public static void Save()
        {

            if (_userConfig!=null)
            {
                _userConfig.Manager.Save(_userConfig);
            }

            if (_appConfig != null)
            {
                _appConfig.Manager.Save(_appConfig);
            }

        }
    }
}