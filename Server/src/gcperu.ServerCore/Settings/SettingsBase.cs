﻿using GCPeru.Server.Core.Settings.Configuration;

namespace GCPeru.Server.Core.Settings
{
    public interface ISettings
    {
        
    }

    public class SettingsBase
    {
        protected IConfig _configmanager;

        public SettingsBase(IConfig configmanager)
        {
            _configmanager = configmanager;
        }
    }

    public class GeneralSettings : SettingsBase
    {
        public GeneralSettings(IConfig configmanager)
            : base(configmanager)
        {
            Load();
        }

        private void Load()
        {
            UrlEndPoint = _configmanager.AppConfig.Default.UrlEndPoint;
        }

        public string UrlEndPoint { get; private set; } 
    }
}