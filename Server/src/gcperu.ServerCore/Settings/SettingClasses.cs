﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using gitspt.global.Core.DevExpress;
using GCPeru.Server.Core.Settings.Manager;
using ServiceStack.Text;

namespace GCPeru.Server.Core.Settings
{

    public class FilterNamedInfo
    {
        public int Id { get; set; }
        public string Filter { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class UserPanelGeometryInfo
    {
        private string _usuarioId;
        private string _panelId;

        public string ID { get; set; }

        public string UsuarioID
        {
            get { return _usuarioId; }
            set
            {
                if (value != _usuarioId)
                    CalculateId(value, _panelId);

                _usuarioId = value;
            }
        }

        public string PanelID
        {
            get { return _panelId; }
            set
            {
                if (value != _panelId)
                    CalculateId(_usuarioId, value);

                _panelId = value;
            }
        }

        public int LocationX { get; set; }
        public int LocationY { get; set; }
        public int SizeWidth { get; set; }
        public int SizeHeight { get; set; }

        private void CalculateId(string userid, string panelid)
        {
            ID = string.Format("{0}#{1}", userid, panelid);
        }
    }

    public class LayoutOptions
    {
        private XLayoutGridOptions _xLayoutgridOptions;

        internal LayoutOptions(string pathbase, decimal userid)
        {
            //Path layout para base
            PathLayoutBase = Path.Combine(pathbase, string.Format("Layout"));

            if (!Directory.Exists(PathLayoutBase))
                Directory.CreateDirectory(PathLayoutBase);

            //Path layout para los grids
            PathLayout = Path.Combine(PathLayoutBase, "Grid");

            if (!Directory.Exists(PathLayout))
                Directory.CreateDirectory(PathLayout);

            PathLayout = Path.Combine(PathLayout, string.Format("User{0}", userid));

            if (!Directory.Exists(PathLayout))
                Directory.CreateDirectory(PathLayout);

            _xLayoutgridOptions = new XLayoutGridOptions { Tarjet = { Type = XLayoutOptionsBase.TargetEnum.ToXml, PathString = PathLayout } };
        }

        internal string PathLayoutBase { get; set; }

        public string PathLayout { get; set; }

        public XLayoutGridOptions OptionsGrid
        {
            get { return _xLayoutgridOptions; }
        }
    }

}