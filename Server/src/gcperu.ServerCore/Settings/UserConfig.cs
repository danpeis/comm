﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;
using gitspt.global.Core.Util.Ficheros;
using GCPeru.Server.Core.Settings.Manager;
using Gurock.SmartInspect;
using Newtonsoft.Json;
using ServiceStack.Text;

namespace GCPeru.Server.Core.Settings
{

    [DataContract]
    public class UserSetting
    {
        private IUserConfigManager _manager;

        private List<UserPanelGeometryInfo> _panelsGeometry;
        private List<FilterNamedInfo> _consoleFiltersNamed;
        private string _interfazFontDefault;

        public UserSetting()
        {
            _interfazFontDefault = "";
        }

        #region Properties

        [IgnoreDataMember]
        public LayoutOptions LayoutOptions { get; private set; }

        [DataMember]
        public string InterfazFontDefault
        {
            get { return _interfazFontDefault; }
            set { _interfazFontDefault = value; }
        }

        [DataMember]
        public int InterfazControlActiveColor { get; set; }

        [DataMember]
        public bool RibbonMinimizedState { get; set; }

        [DataMember]
        public bool BestFitColumnsGrid { get; set; }

        [DataMember]
        public string SkinName { get; set; }


        /// <summary>
        /// Nombre del Diséño de impresion elegido para imprimir los informes de estadisticas
        /// </summary>
        [DataMember]
        public string ReportPrintDesignLayoutName { get; set; }

        [DataMember]
        public List<UserPanelGeometryInfo> PanelsGeometry
        {
            get { return _panelsGeometry ?? (_panelsGeometry = new List<UserPanelGeometryInfo>()); }
            private set { _panelsGeometry = value; }
        }

        /// <summary>
        /// Lista de Filtros con nombre, guardados por el Usuario como Filtros utiles.
        /// </summary>
        [DataMember]
        public List<FilterNamedInfo> ConsoleFiltersNamed
        {
            get { return _consoleFiltersNamed ?? (_consoleFiltersNamed = new List<FilterNamedInfo>()); }
            private set { _consoleFiltersNamed = value; }
        }

        /// <summary>
        /// Indica si gestionar Save and Restore de los diseños de los distintos elementos de la interfaz que lo soporten
        /// </summary>
        [DataMember]
        public bool HandleLayouts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public byte[] ExportOptionsLayout { get; set; }

        #endregion

        [IgnoreDataMember]
        public IUserConfigManager Manager
        {
            get { return _manager; }
            set
            {
                _manager = value;
                LayoutOptions = _manager.CreateLayoutOptions();
            }
        }

        public override string ToString()
        {
            return string.Format("UserSettings:{0}", this.Dump());
        }

        #region Funciones

        public UserPanelGeometryInfo GetPanelGeometrySaved(string panelkey)
        {
            return PanelsGeometry.FirstOrDefault(p => p.PanelID == panelkey);
        }

        public void SavePanelGeometry(string panelkey, UserPanelGeometryInfo data)
        {
            try
            {
                var pnlgeo = PanelsGeometry.FirstOrDefault(p => p.PanelID == panelkey);
                if (pnlgeo != null)
                {
                    pnlgeo.ID = data.ID;
                    pnlgeo.LocationX = data.LocationX;
                    pnlgeo.LocationY = data.LocationY;
                    pnlgeo.PanelID = data.PanelID;
                    pnlgeo.SizeHeight = data.SizeHeight;
                    pnlgeo.SizeWidth = data.SizeWidth;
                    pnlgeo.UsuarioID = "0";
                }
                else
                {
                    PanelsGeometry.Add(data);
                }
            }
            catch (Exception ex)
            {
            }
        }


        #endregion

    }


}