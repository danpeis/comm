﻿using System;
using System.Data;
using System.Data.SqlClient;
using ax.DbMibgrator;
using ax.DbMigrator.Framework;
using EntitySpaces.Interfaces;
using gitspt.global.Core.Extension;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;

namespace gcperu.Server.Core.Migration
{
	public class DatabaseMigration
	{
		public const int MAX_VERSION_SUPPORTED = 200;
		private const int VERSION_DEFAULT_NOTABLA_IDENTIDAD = 0;
		private const string CONEXION_NAME_DATABASE_MAESTRA = Constantes.CNXNAME_GITSGC;
		private const int CONEXION_TIMEOUT = 240;

		private int _versionCurrent, _versionOriginal, _versionUp;
		private int _connecttimeout;
		private SqlMigrator _smgits, _smgenc, _smgtdir;

		private DAL.GC.Identidad _idty;
	    private ISSTraceLog _log;

		public DatabaseMigration()
		{
			_connecttimeout = CONEXION_TIMEOUT;
            _log = Ioc.Container.Get<ISSTraceLog>();

			_log.LogDebug(string.Format("[DatabaseMigration.Ctor. ConnectTimeout:{0}", _connecttimeout));

			//Obtener la version actual de la BD.
			ObtenerVersionActual();
		}

		public int DatabaseCurrentVersion
		{
			get { return _versionCurrent; }
		}

		public bool IsNeedApplyMigration(int versionToMigrate)
		{
			if (versionToMigrate > MAX_VERSION_SUPPORTED)
				throw new ApplicationException(string.Format("[DatabaseMigration.IsNeedApplyMigration] NO SE PERMITE ESTA MIGRACION, LA MAXIMA VERSION SOPORTADA ES {0}", MAX_VERSION_SUPPORTED));

			if (versionToMigrate > _versionCurrent)
				return true;

			return false;
		}

		public MigrationResult ApplyMigration(int versionToMigrate)
		{
			try
			{
				if (versionToMigrate <= _versionCurrent)
					return new MigrationResult();

				//COMENZAMOS LAS TRANSACCIONES
				//_smgits.Provider.BeginTransaction();
				//_smgc.Provider.BeginTransaction();
				//_smgcgps.Provider.BeginTransaction();


				if (versionToMigrate >= 0 || versionToMigrate < 200)
				{
					var mdb = new MigrationUp200(_smgits, _smgenc, _smgtdir);
					var rs = mdb.ApplyMigration(_versionCurrent, versionToMigrate);
					_versionCurrent = rs.MigratedToVersion;
				}

				//ACTUALIZAR EN LA IDENTIDAD CON LA NUEVA VERSION
				_idty.Version = _versionCurrent.ToShort();
				_idty.Fecha = System.DateTime.Now;
				_idty.Save();

				//CONFIRMAMOS LAS TRANSACCIONES
				//_smgits.Provider.Commit();
				//_smgc.Provider.Commit();
				//_smgcgps.Provider.Commit();

				return new MigrationResult(_versionCurrent);
			}
			catch (Exception ex)
			{
				//ROLLBACK LAS TRANSACCIONES
				//_smgits.Provider.Rollback();
				//_smgc.Provider.Rollback();
				//_smgcgps.Provider.Rollback();

				throw new Exception("[DatabaseMigration.ApplyMigration]. ERROR OCURRIDO AL ACTUALIZAR LA BASE DE DATOS.", ex);
			}
		}

		private void ObtenerVersionActual()
		{
			var cnxbld = new SqlConnectionStringBuilder();

			//---GITS_ENCUESTA
			var cnx = esConfigSettings.ConnectionInfo.Connections[CONEXION_NAME_DATABASE_MAESTRA];
			cnxbld.ConnectionString = cnx.ConnectionString;
			_log.LogDebug(string.Format("[DbMigrator.ObtenerVersionActual]. {0}:{1}",CONEXION_NAME_DATABASE_MAESTRA, cnxbld.ConnectionString));

			_smgenc = new SqlMigrator("SqlServer", cnxbld.ConnectionString, _connecttimeout, true);

			//---GITS
			cnx = esConfigSettings.ConnectionInfo.Connections[Core.Constantes.CNXNAME_GITS];
			cnxbld.ConnectionString = cnx.ConnectionString;
            _log.LogDebug(string.Format("[DbMigrator.ObtenerVersionActual]. {0}:{1}", Constantes.CNXNAME_GITS, cnxbld.ConnectionString));

			_smgits = new SqlMigrator("SqlServer", cnxbld.ConnectionString, _connecttimeout, true);

			//---GITS_DIR
			cnx = esConfigSettings.ConnectionInfo.Connections[Constantes.CNXNAME_GITSGCG];
			cnxbld.ConnectionString = cnx.ConnectionString;
            _log.LogDebug(string.Format("[DbMigrator.ObtenerVersionActual]. {0}:{1}", Constantes.CNXNAME_GITSGCG, cnxbld.ConnectionString));

			_smgtdir = new SqlMigrator("SqlServer", cnxbld.ConnectionString, _connecttimeout, true);


			if (!_smgenc.Provider.TableExists("Identidad"))
			{
				CreateTableIdentity();
				ObtenerEEVersionActual();
			}
			else if (!_smgenc.Provider.ColumnExists("Identidad", "ExeName"))
			{
				CreateTableIdentity();
				ObtenerEEVersionActual();
			}
			else
			{
				ObtenerEEVersionActual();
			}

			_log.LogDebug(string.Format("[ObtenerVersionActual]. Version Actual :{0}", _versionCurrent));
		}

		private void ObtenerEEVersionActual()
		{
			_idty = new DAL.GC.Identidad(Constantes.APPID);
			if (_idty.IsNew)
			{
				//Insertamos un registro inicial que indique la versión.
                _idty.Id = Constantes.APPID;
				_idty.Version = VERSION_DEFAULT_NOTABLA_IDENTIDAD;
				_idty.Fecha = System.DateTime.Now;
                _idty.ExeName = Constantes.APPID;

				_versionCurrent = VERSION_DEFAULT_NOTABLA_IDENTIDAD;
			}
			else
			{
				_versionCurrent = (int) _idty.Version;
			}
		}

		private void CreateTableIdentity()
		{
			if (_smgenc.Provider.TableExists("Identidad"))
				_smgenc.Provider.RemoveTable("Identidad");

			_smgenc.Provider.AddTable("Identidad",
										new Column("Id", DbType.String, 20, ColumnProperty.PrimaryKey),
										new Column("Version", DbType.Int16, ColumnProperty.NotNull, 0),
										new Column("Fecha", DbType.DateTime, ColumnProperty.Null),
										new Column("ExeName", DbType.String, 100, ColumnProperty.NotNull, "''"));

			//Creamos el Primer Registro.
            var tid = new DAL.GC.Identidad(Constantes.APPID) { Version = VERSION_DEFAULT_NOTABLA_IDENTIDAD, Fecha = System.DateTime.Now, ExeName = Constantes.APPID };
			tid.Save();

			_versionCurrent = VERSION_DEFAULT_NOTABLA_IDENTIDAD;

		}
	}

	internal abstract class MigrationUp
	{
		internal int MaxVersionUp { get; set; }
		internal int MinVersionUp { get; set; }

		internal int VersionCurrent { get; set; }

		protected int VersionMigrated { get; set; }

		protected SqlMigrator _smgits, _smgenc, _smgtdir;

		internal virtual MigrationResult ApplyMigration(int versionCurrent, int versionToMigration)
		{
			return null;
		}

	}

	public class MigrationResult
	{
		public int MigratedToVersion { get; set; }
		public bool ok { get; set; }

		public MigrationResult()
		{
			ok = false;
			MigratedToVersion = 0;
		}

		public MigrationResult(int migratedToVersion)
		{
			MigratedToVersion = migratedToVersion;
			this.ok = true;
		}
	}
}