﻿using System;
using System.Data;
using ax.DbMibgrator;
using ax.DbMigrator.Framework;
using gitspt.global.Core.Log;

namespace gcperu.Server.Core.Migration
{
	internal class MigrationUp200 : MigrationUp
	{
		private MigrationUp200()
		{
			MaxVersionUp = 200;
			MinVersionUp = 100;
		}

		internal MigrationUp200(SqlMigrator smgits, SqlMigrator smgenc, SqlMigrator smgtdir)
			: this()
		{
			_smgenc = smgenc;
			_smgits = smgits;
			_smgtdir = smgtdir;
		}

		internal override MigrationResult ApplyMigration(int versionCurrent, int versionToMigration)
		{

			if (versionToMigration < MinVersionUp)
				return null;

			base.VersionCurrent = versionCurrent;

			MigrationResult rs = null;

			if (VersionCurrent < 110 && versionToMigration >= 110)
			{
				rs = M100to110();
				VersionMigrated = rs.MigratedToVersion;
				VersionCurrent = rs.MigratedToVersion;
			}

			return new MigrationResult(VersionMigrated);
		}

		#region Metodos Migraciones
        private MigrationResult M100to110()
		{
			try
            {
				TraceLog.LogTraceStart("Migration Database. M100to110. START...");

				string tbl = "";
				//Migration a la version 110.

                //GITS
                _smgits.Provider.AddColumn("Pacientes", new Column("EncFechaUltimaEncuesta", DbType.DateTime, ColumnProperty.Null));
                _smgits.Provider.AddColumn("OrigenDestino", new Column("EncFechaUltimaEncuesta", DbType.DateTime, ColumnProperty.Null));
                _smgits.Provider.AddColumn("Viajes", new Column("EncFechaUltimaEncuesta", DbType.DateTime, ColumnProperty.Null));

                //GITS_ENCUESTAS
				_smgenc.Provider.AddColumn("Configuracion", new Column("PeriodoEncuestasActivo", DbType.String, 50, ColumnProperty.Null));
                _smgenc.Provider.AddColumn("Encuesta", new Column("SujetoTablaRowID", DbType.String, 50, ColumnProperty.Null));

                //GITS_DIR
                _smgtdir.Provider.AddColumn("IdentidadPaciente", new Column("CIP", DbType.String, 50, ColumnProperty.Null));

				return new MigrationResult(110);
			}
			catch (Exception ex)
			{
				throw new Exception("[M100to110]. ERROR ACTUALIZAR VERSION BD A 110.", ex);
			}
			finally
			{
				TraceLog.LogTraceStop("Migration Database. M100to110. END");
			}
		}

		#endregion

	}
}