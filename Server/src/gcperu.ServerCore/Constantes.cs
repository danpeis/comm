﻿namespace gcperu.Server.Core
{
    public static class Constantes
    {
        public const string APPID = "GCPERU-SERVER";

        public const string BUGTRACKERPROJECTKEY = "GCSRVPERU";
        public const string BUGTRACKER_HOSTNAME = "amcoexoficinas1.dynns.com";

        public const string BDCNX_CONNECTIONSTRING = "Data Source={3};Initial Catalog={2};User ID={0};Password={1};Application Name={4},Connect Timeout={5}";
        public const string BDCNX_CONNECTIONSTRING_SSPI = "Data Source={0};Initial Catalog={1};Integrated Security=true;Application Name={2};Connect Timeout={3}";

        public const string BDCNX_USER = "Ambulancia";
        public const string BDCNX_PASSWORD = "28051117";
        public const string BDCNX_APPNAME = "GCPERU.SERVER";

        public const int CNX_TIMEOUT_DEFAULT = 15;

        public const string CNXNAME_GITS = "GT_DB";
        public const string CNXNAME_GITSGC = "GC_DB";
        public const string CNXNAME_GITSGCG = "GCG_DB";

        public const int DATABASE_VERSION_REQUIRED = 110;
    }
}