﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace gcperu.Server.Core.Exceptions
{
    public enum ExceptionErrorLevel
    {
        /// <summary>
        /// Error que no compromete la integridad del sistema. Aviso
        /// </summary>
        Warning = 2,
        /// <summary>
        /// Error que no compromete la integridad del sistema. Informacion
        /// </summary>
        Information = 1,
        /// <summary>
        /// Error, pero es posible continuar con la ejecución tomando las medidas oportunas.
        /// </summary>
        Error = 4,
        /// <summary>
        /// Critico, pero es posible continuar con la ejecución tomando las medidas oportunas.
        /// </summary>
        Critical = 4,
        /// <summary>
        /// No se puede continuar con la ejecucion
        /// </summary>
        Fatal = 5
    }

    public class GCException : ApplicationException
    {
        private string msgCalculado = "";

        public string Msg { get; set; }
        public string MsgEx { get; set; }
        public Type TypeSource { get; set; }

        private ExceptionErrorLevel _errorLevel = ExceptionErrorLevel.Information;
        public ExceptionErrorLevel ErrorLevel
        {
            get { return _errorLevel; }
            set { _errorLevel = value; }
        }

        #region Constructores

        protected GCException()
        {
            CalcularMessage();
        }

        /// <summary>
        /// Crea una nueva excepcion con la informacion suministrada.
        /// </summary>
        /// <param name="msg">Descripcion corta </param>
        public GCException(string msg)
            : base(msg)
        {
            CalcularMessage();
        }


        /// <summary>
        /// Crea una nueva excepcion con la informacion suministrada.
        /// </summary>
        /// <param name="msgEx">Mensaje extendido</param>
        /// <param name="msg">Descripcion corta </param>
        public GCException(string msg, string msgEx)
            : base(msgEx)
        {
            Msg = msg;
            CalcularMessage();
        }

        /// <summary>
        /// Crea una nueva excepcion con la informacion suministrada.
        /// </summary>
        /// <param name="msg">Descripcion corta</param>
        /// <param name="ex">Excepcion Interna</param>
        public GCException(string msg, Exception ex)
            : base(msg, ex)
        {
            Msg = msg;
            MsgEx = "";
            CalcularMessage();
        }

        /// <summary>
        /// Crea una nueva excepcion con la informacion suministrada.
        /// </summary>
        /// <param name="msg">Descripcion corta</param>
        /// <param name="ex">Excepcion Interna</param>
        public GCException(string msg, Exception ex, ExceptionErrorLevel errorLevel)
            : base(msg, ex)
        {
            Msg = msg;
            MsgEx = "";
            _errorLevel = errorLevel;
            CalcularMessage();
        }

        /// <summary>
        /// Crea una nueva excepcion con la informacion suministrada.
        /// </summary>
        /// <param name="ex">Excepcion interna</param>
        /// <param name="msg">mensaje corta</param>
        /// <param name="typeSource">Tipo que genero la excepcion</param>
        public GCException(string msg, Exception ex, Type typeSource)
            : base(msg, ex)
        {
            Msg = msg;
            TypeSource = typeSource;
            CalcularMessage();
        }

        /// <summary>
        /// Crea una nueva excepcion con la informacion suministrada.
        /// </summary>
        /// <param name="msg">Descripcion corta</param>
        /// <param name="msgEx">Descripcion extendida</param>
        /// <param name="typeSource">Tipo que genero la excepcion</param>
        public GCException(string msg, string msgEx, Type typeSource)
            : base(msg)
        {
            Msg = msg;
            MsgEx = msgEx;
            TypeSource = typeSource;
            CalcularMessage();
        }

        /// <summary>
        /// Crea una nueva excepcion con la informacion suministrada.
        /// </summary>
        /// <param name="ex">Excepcion interna</param>
        /// <param name="msg">mensaje corta</param>
        /// <param name="msgEx">mensaje extendido</param>
        /// <param name="typeSource">Tipo que genero la excepcion</param>
        public GCException(string msg, string msgEx, Exception ex, Type typeSource)
            : base(msg, ex)
        {
            Msg = msg;
            MsgEx = msgEx;
            TypeSource = typeSource;
            CalcularMessage();
        }

        protected GCException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

        #endregion

        /// <summary>
        /// Devuelve un mensaje formateado, descriptivo del error.
        /// </summary>
        public override string Message
        {
            get
            {
                return msgCalculado;
            }
        }

        public override string ToString()
        {
            return this.Message;
        }

        protected MethodBase WhoCallMe(byte nivelStack)
        {
            var stackTrace = new StackTrace();
            var stackFrame = stackTrace.GetFrame(nivelStack);

            return stackFrame.GetMethod();
        }

        protected MethodBase WhoCallMe()
        {
            var stackTrace = new StackTrace();
            var stackFrame = stackTrace.GetFrame(1);

            return stackFrame.GetMethod();
        }

        private void CalcularMessage()
        {
            if (!string.IsNullOrEmpty(MsgEx))
            {
                if (string.IsNullOrEmpty(Msg))
                {
                    msgCalculado = string.Format("{0}", MsgEx);
                    return;
                }
                else
                {
                    msgCalculado = string.Format("{0}.\n\n {1}", Msg, MsgEx);
                    return;
                }
            }

            if (InnerException != null)
            {
                MsgEx = InnerException.ToString();
                msgCalculado = string.Format("{0}.\n\n {1}", Msg, MsgEx);
                return;
            }

            msgCalculado = string.Format("{0}.", Msg);
            return;
        }
    }

    public class ArgumentGCException : GCException
    {
        public ArgumentGCException(byte paramPosErr, object paramObject, Type paramTypeExpected)
        {
            string _argNameErr = "", _argNameType = "";
            MethodBase metodo = null;

            try
            {
                metodo = WhoCallMe();  //Obtenemos la informacion de la 2ª posicion de la pila de llamadas
                if (metodo != null)
                {
                    var args = metodo.GetParameters();
                    if (args.GetUpperBound(0) >= (paramPosErr - 1))
                    {
                        _argNameErr = (string)args[paramPosErr - 1].Name;
                        _argNameType = args[paramPosErr - 1].ParameterType.ToString();
                        _argNameType += " Tipo Valido (" + paramObject.ToString() + ", " + paramObject.GetType().ToString() + ")";
                    }
                    else
                    {
                        _argNameErr = "Desconocido.Posicion firma " + paramPosErr;
                        _argNameType = "Desconocido";
                    }
                }
                else
                {
                    _argNameErr = "Desconocido.Posicion firma " + paramPosErr;
                    _argNameType = "Desconocido";
                }
            }
            catch (Exception e)
            {
                _argNameErr = "Desconocido.Posicion firma " + paramPosErr;
                _argNameType = "Desconocido";
            }

            Msg = string.Format("Argumento erroneo. Metodo <{3}>. Parametro <{0}> Tipo <{1}>. Tipo esperado <{2}>",
                                _argNameErr,
                                _argNameType,
                                paramTypeExpected.Name, (metodo != null) ? metodo.ToString() : "desconocido");

        }

    }
}