﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mime;

using EntitySpaces.DebuggerVisualizer;
using EntitySpaces.Interfaces;
using EntitySpaces.LoaderMT;
using EntitySpaces.Profiler;
using gcperu.Server.Core.Properties;
using gitspt.global.Core.Enums;
using gitspt.global.Core.Exceptions;
using gitspt.global.Core.Extension;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;
using gitspt.global.Core.Util;
using GCPeru.Server.Core.Settings;
using gcperu.Server.Core.Extension;
using gcperu.Server.Core.Info;
using gcperu.Server.Core.IoC;

namespace gcperu.Server.Core.Initialization
{
    public interface IRunCase
    {
        void Run();
        void Dispose();
    }

    public abstract class RuncoreCase
    {
        protected NinjectIocContainer _IocContainer;
        protected static ISSTraceLog _log;

        protected RuncoreCase()
        {
            _IocContainer = (NinjectIocContainer)Ioc.Container;
        }

        public virtual void Run()
        {
            LoadIocSettings();
            LoadLogConfig();

            esProviderFactory.Factory = new esDataProviderFactory();
            esVisualizer.Initialize();

            
        }

	    protected abstract void LoadIocSettings();


        protected virtual void LoadLogConfig()
        {

        }

        private void UnloadLog()
        {
            _log.Unload();
        }

        protected  void InitSQLProfiling()
        {
            ProfilerListener.BeginProfiling("EntitySpaces.SqlClientProvider", ProfilerListener.Channels.Channel_1);
            _log.LogInformation("SQLProfiling. Start... Ok");
        }

        protected  void EndSQLProfiling()
        {
            ProfilerListener.EndProfiling("EntitySpaces.SqlClientProvider");
            _log.LogInformation("SQLProfiling. END. Ok");
        }

		protected virtual void DatabaseMigration()
		{
			try
			{
				var dbm = new Core.Migration.DatabaseMigration();

				if (dbm.IsNeedApplyMigration(Core.Constantes.DATABASE_VERSION_REQUIRED))
				{
					var rlt = dbm.ApplyMigration(Core.Constantes.DATABASE_VERSION_REQUIRED);
					if (rlt.ok){
						_log.LogInformation(string.Format("[VerifyDatabaseMigration.ApplyMigration {0}]. SE HA ACTUALIZADO CORRECTAMENTE A AL VERSION DE BASE DE DATOS {0}", rlt.MigratedToVersion));
						ModuleConfig.Global.DatabaseVersion = rlt.MigratedToVersion;
					}
				}
				else
				{
					_log.LogInformation(string.Format("[VerifyDatabaseMigration].NO ES NECESARIO ACTUALIZAR. VERSION ESTA EN LA VERSIÓN REQUERIDA={0}", Core.Constantes.DATABASE_VERSION_REQUIRED));
					ModuleConfig.Global.DatabaseVersion = dbm.DatabaseCurrentVersion;
				}
			}
			catch (Exception EX)
			{
				_log.LogError(string.Format("[VerifyDatabaseMigration.ApplyMigration {0}]. ERROR AL ACTUALIZAR VERSION DE LA BASE DE DATOS {0}\n\n{1}", Core.Constantes.DATABASE_VERSION_REQUIRED, EX));
			}
		}

        protected abstract void ApplyGlobalCompileOptions();

        /// <summary>
        /// Agrega y Verifica las conexiones con los parametros actuales del Sistema
        /// </summary>
        protected bool AddAndVerifyDatabaseConnections()
        {
            //LANZARIA UN ERROR, SI NO HAY CONEXION CON LA BASE DE DATOS...
            try
            {
                _log.LogTraceStar("AddAndVerifyDatabaseConnections...");

                var appconfig = ModuleConfig.AppConfig;
                var dbidentity = Ioc.Container.Get<DatabaseIdentity>();


                string hostserver = "";

                {
                    //GITS
                    var dbinfo = appconfig.DatabasesConfig[Constantes.CNXNAME_GITS];
                    if (dbinfo == null)
                        throw new ArgumentException("Datos de Conexion con GITS database no proporcionados.");

                    if (!dbinfo.CheckConnection(dbidentity, Constantes.CNX_TIMEOUT_DEFAULT, _log))
                        return false;

                    hostserver = dbinfo.Host;

                    //AGREGO LA CONEXION
                    dbinfo.CreateEntitySpacesDatabaseConnection(Constantes.CNXNAME_GITS, dbidentity, Constantes.CNX_TIMEOUT_DEFAULT,_log);
                }

                {
                    //GITSCOMUNICA
                    var dbinfo = appconfig.DatabasesConfig[Constantes.CNXNAME_GITSGC];
                    if (dbinfo == null)
                        throw new ArgumentException("Datos de Conexion con GITSCOMUNICA database no proporcionados.");

                    if (!dbinfo.CheckConnection(dbidentity, Constantes.CNX_TIMEOUT_DEFAULT, _log))
                        return false;

                    hostserver = dbinfo.Host;

                    //AGREGO LA CONEXION
                    dbinfo.CreateEntitySpacesDatabaseConnection(Constantes.CNXNAME_GITSGC, dbidentity, Constantes.CNX_TIMEOUT_DEFAULT, _log);
                }

                {
                    //GITSCOMUNICAGPS
                    var dbinfo = appconfig.DatabasesConfig[Constantes.CNXNAME_GITSGCG];
                    if (dbinfo == null)
                        throw new ArgumentException("Datos de Conexion con GITSCOMUNICAGPS database no proporcionados.");

                    if (!dbinfo.CheckConnection(dbidentity, Constantes.CNX_TIMEOUT_DEFAULT, _log))
                        return false;

                    hostserver = dbinfo.Host;

                    //AGREGO LA CONEXION
                    dbinfo.CreateEntitySpacesDatabaseConnection(Constantes.CNXNAME_GITSGCG, dbidentity, Constantes.CNX_TIMEOUT_DEFAULT, _log);
                }

				DatabaseMigration();

                return true;
            }
            catch (Exception ex)
            {
                ManejarExceptions.ManejarException(ex, ExceptionErrorLevel.Error);
                return false;
            }
            finally
            {
                _log.LogTraceStop("AddAndVerifyDatabaseConnections.");
            }

        }
       
        protected void ConfigurarGestionLayouts()
        {

            gitspt.global.Core.DevExpress.XLayoutGrid.Options.Tarjet.Type = gitspt.global.Core.DevExpress.XLayoutOptionsBase.TargetEnum.ToXml;
            gitspt.global.Core.DevExpress.XLayoutGrid.Options.Tarjet.PathString = ModuleConfig.UserConfig.LayoutOptions.PathLayout;
            gitspt.global.Core.DevExpress.XLayoutGrid.Options.HandleSaveAndRestore = ModuleConfig.AppConfig.CompileSymbols.SaveRestoreLayout;

            if (Directory.Exists(gitspt.global.Core.DevExpress.XLayoutGrid.Options.Tarjet.PathString) == false)
                Directory.CreateDirectory(gitspt.global.Core.DevExpress.XLayoutGrid.Options.Tarjet.PathString);
        }
    
        protected void DisposeBase()
        {
            UnloadLog();
        }

        public void Dispose()
        {
        }
    }
	
}