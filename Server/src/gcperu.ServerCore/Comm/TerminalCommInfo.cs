﻿using gcperu.Server.Core.DAL.GC;
using gitspt.global.Core.Log;

namespace gcperu.Server.Core.Comm
{
    //TODO:TerminalSocketInfo | Clase Temporal que hay que cambiar. Simplemente para compilar
    public class TerminalSocketInfo
    {
        public TerminalState TerminalStateBD { get; set; }

        public ISSTraceLog Log { get; set; }
    }
}