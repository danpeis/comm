/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Reflection;
using EntitySpaces.DynamicQuery;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace gcperu.Server.Core.DAL
{
    [Serializable]
    public class QueryBase : esDynamicQuery
    {
        protected Dictionary<string, esQueryItem> _queryItems;
        public Dictionary<string, esQueryItem> QueryItems
        {
            get
            {
                if (_queryItems == null)
                    CreateQueryItems();

                return _queryItems;
            }
        }

        public virtual QueryBase CreateQuery()
        {
            return null;
        }

        /// <summary>
        /// Devuelve el n� de clausulas de la consulta.
        /// </summary>
        /// <returns></returns>
        public int Count
        {
            get { return 0; }
        }

        public new void PopulateRequest(esDataRequest request)
        {
            base.PopulateRequest(request);
        }

        public virtual void CreateQueryItems()
        {
        }

    }
}