/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
*/

using System;
using System.Collections.Generic;
using System.Data;
using EntitySpaces.Core;
using EntitySpaces.DynamicQuery;
using EntitySpaces.Interfaces;

namespace gcperu.Server.Core.DAL
{
    public interface IEECollection
    {
        bool LoadQueryBase(QueryBase query);
        IEnumerable<EntityBase> Lista();
        DataTable LoadDataTable(QueryBase query);
        DataTable Table { get; }

    }

    [Serializable]
    abstract public class CollectionBase<T> : esEntityCollection<T>, IEECollection
        where T : esEntity, new()
    {

        /// <summary>
        /// Datos obtenidos dede la BD, en formato 'DataTable'
        /// </summary>
        public DataTable _datatable;

        public bool Load(QueryBase query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            if (!query.es2.HasConnection)
            {
                query.es2.Connection = ((IEntityCollection)this).Connection;
            }
            return query.Load();
        }

        #region Implemented Interfaz IEECollection

        public bool LoadQueryBase(QueryBase query)
        {
            return this.Load(query);
        }

        public IEnumerable<EntityBase> Lista()
        {
            return (IEnumerable<EntityBase>)this;
        }

        public DataTable LoadDataTable(QueryBase query)
        {
            esDataRequest request = this.CreateRequest();

            if (query == null)
                query = new QueryBase();

            query.PopulateRequest(request);
            request.Parameters = null;
            request.DynamicQuery = query;
            request.QueryType = esQueryType.DynamicQuery;
            request.DatabaseVersion = this.es.Connection.ProviderSignature.DatabaseVersion;

            esDataResponse response = new esDataProvider().esLoadDataTable(request, this.es.Connection.ProviderSignature);
            _datatable = response.Table;
            this.PopulateCollection(response.Table);

            return response.Table;
        }

        public DataTable Table
        {
            get
            {
                if (_datatable == null)
                    _datatable = CreateDataTable();

                return _datatable;
            }
        }

        public virtual bool ReLoad()
        {
            return false;
        }

        #endregion


        protected new bool OnQueryLoaded(esDynamicQuery query, DataTable table)
        {
            if (_datatable != null)
            {
                _datatable = table;
            }
            return base.OnQueryLoaded(query, table);
        }

        #region  Private

        protected void x()
        {

        }

        protected DataTable CreateDataTable()
        {

            if (this.Count == 0)
                return null;

            DataTable table = new DataTable();
            DataColumnCollection dataColumns = table.Columns;

            List<string> trueColumns = new List<string>();

            // Stnadard Columns
            foreach (esColumnMetadata esCol in this.es.Meta.Columns)
            {
                // If entity is null then the collection is empty we need to create
                // columns however, othewise we create only the columns that were returned
                // by the query or procedure
                dataColumns.Add(new DataColumn(esCol.Name, esCol.Type));

                // We're going to use this later
                trueColumns.Add(esCol.Name);
            }

            // Extra Columns
            Dictionary<string, object> extra = this[0].GetExtraColumns();

            foreach (string columnName in extra.Keys)
            {
                dataColumns.Add(new DataColumn(columnName, extra[columnName].GetType()));
            }

            //Rellenando los datos de la Tabla.
            foreach (T ent in this)
            {
                DataRow row = table.NewRow();
                table.Rows.Add(row);

                foreach (string columnName in trueColumns)
                {
                    if (ent.ContainsColumn(columnName))
                    {
                        object value = ent.GetColumn(columnName);
                        row[columnName] = value ?? DBNull.Value;
                    }
                }

                foreach (string columnName in extra.Keys)
                {
                    object value = extra[columnName];
                    row[columnName] = value ?? DBNull.Value;
                }
            }

            return table;
        }

        #endregion

        #region Static

        /// <summary>
        /// Permite crear un objecto 'DataTable' desde la coleccion y entidad pasada como parametro.
        /// </summary>
        /// <param name="coll">Coleccion desde la que queremos rellenar la tabla</param>
        /// <param name="entity">Entidad tipo de los registros de la tabla</param>
        /// <returns></returns>
        public static DataTable CreateDataTable(esEntityCollectionBase coll, esEntity entity)
        {
            DataTable table = new DataTable();
            DataColumnCollection dataColumns = table.Columns;

            List<string> trueColumns = new List<string>();

            // Stnadard Columns
            foreach (esColumnMetadata esCol in coll.es.Meta.Columns)
            {
                // If entity is null then the collection is empty we need to create
                // columns however, othewise we create only the columns that were returned
                // by the query or procedure
                if (entity == null || entity.ContainsColumn(esCol.Name))
                {
                    dataColumns.Add(new DataColumn(esCol.Name, esCol.Type));

                    // We're going to use this later
                    trueColumns.Add(esCol.Name);
                }
            }

            // Extra Columns
            if (entity != null)
            {
                Dictionary<string, object> extra = entity.GetExtraColumns();

                foreach (string columnName in extra.Keys)
                {
                    dataColumns.Add(new DataColumn(columnName, extra[columnName].GetType()));
                }
            }

            foreach (esEntity ent in coll)
            {
                DataRow row = table.NewRow();
                table.Rows.Add(row);

                foreach (string columnName in trueColumns)
                {
                    if (ent.ContainsColumn(columnName))
                    {
                        object value = ent.GetColumn(columnName);
                        row[columnName] = value == null ? DBNull.Value : value;
                    }
                }

                Dictionary<string, object> extra = ent.GetExtraColumns();

                foreach (string columnName in extra.Keys)
                {
                    object value = extra[columnName];
                    row[columnName] = value == null ? DBNull.Value : value;
                }
            }


            return table;
        }

        #endregion

    }
}