/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Reflection;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace gcperu.Server.Core.DAL
{
    
    [Serializable]
    public class EntityBase : esEntity,IDataErrorInfo
    {
        public EntityBase()
        {
        }

        public virtual bool ValidateEntity()
        {
            return true;
        }

        public bool IsNew
        {
            get { return this.RowState == esDataRowState.Added; }
        }

        public void UndoEntity()
        {
            this.RejectChanges();
        }

        public void Delete()
        {
            this.MarkAsDeleted();
            this.Save();

        }

        public T GetColumn<T>(string columnname) where T : IConvertible
        {
            return Parse<T>(this.GetColumn(columnname));
        }

        public bool ColumnModified(string columnname)
        {
            return this.es.ModifiedColumns.Contains(columnname);
        }


        #region Private

        protected static T Parse<T>(object sourceValue) where T : IConvertible
        {
            try
            {
                if ((sourceValue == null) || (sourceValue is DBNull))
                {
                    return (T)Convert.ChangeType(GetDefaultValueTypeObject(typeof(T)), typeof(T));
                }
                return (T)Convert.ChangeType(sourceValue, typeof(T));
            }
            catch (Exception)
            {
                return (T)Convert.ChangeType(GetDefaultValueTypeObject(typeof(T)), typeof(T));
            }
        }

        protected static object GetDefaultValueTypeObject(Type tipo)
        {
            if (((tipo.Name.Contains("Decimal") || tipo.Name.Contains("Int")) || (tipo.Name.Contains("Double") || tipo.Name.Contains("Byte"))) || tipo.Name.Contains("float"))
            {
                return 0;
            }
            if (tipo.Name.Contains("bool"))
            {
                return false;
            }
            if (tipo.Name.ToUpper().Contains("DATETIME"))
            {
                return DateTime.MinValue;
            }
            return "";
        }

        #endregion


        //#region IDataErrorInfo Members

        ///// <summary>
        ///// Gets an error message indicating what is wrong with this object.
        ///// </summary>
        ///// <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>      
        //public string Error
        //{
        //    get
        //    {
        //        string errorDescription = string.Empty;
        //        if (!this.IsValid)
        //        {
        //            errorDescription = this.ValidationRules.GetBrokenRules().ToString();
        //        }
        //        return errorDescription;
        //    }

        //}


        ///// <summary>
        ///// Gets the <see cref="String"/> error for a specific columnName.
        ///// </summary>
        ///// <value>Returns either the error or an empty string</value>
        //public string this[string columnName]
        //{
        //    get
        //    {
        //        string errorDescription = string.Empty;
        //        if (!this.IsValid)
        //        {
        //            errorDescription = this.ValidationRules.GetBrokenRules().GetPropertyErrorDescriptions(columnName);
        //        }
        //        return errorDescription;

        //    }
        //}

        //#endregion

        //#region Validation

        //private bool _IsValid = true;
        //private ValidationRules _ValidationRules;


        //#region Custom Validation Rules
        ////Add any custom validation rules here
        //#endregion

        ///// <summary>
        ///// Returns the list of <see cref="ValidationRules"/> associated with this object.
        ///// </summary>
        //protected ValidationRules ValidationRules
        //{
        //    get
        //    {
        //        if (_ValidationRules == null)
        //        {
        //            _ValidationRules = new ValidationRules(this);
        //            //lazy init the rules as well.
        //            AddValidationRules();
        //            AddValidationRulesAutomatic();
        //        }

        //        return _ValidationRules;
        //    }
        //}

        ///// <summary>
        ///// Assigns validation rules to this object, pero utilizado de forma automatica por la generacion de Codigo.
        ///// </summary>
        ///// <remarks>
        ///// This method can be overridden in a derived class to add custom validation rules. 
        /////</remarks>
        //protected virtual void AddValidationRulesAutomatic()
        //{

        //}

        ///// <summary>
        ///// Assigns validation rules to this object.
        ///// </summary>
        ///// <remarks>
        ///// This method can be overridden in a derived class to add custom validation rules. 
        /////</remarks>
        //protected virtual void AddValidationRules()
        //{

        //}

        ///// <summary>
        ///// Returns <see langword="true" /> if the object is valid, 
        ///// <see langword="false" /> if the object validation rules that have indicated failure. 
        ///// </summary>
        //public virtual bool IsValid
        //{
        //    get
        //    {
        //        this.Validate();
        //        return ValidationRules.IsValid;
        //    }
        //}

        ///// <summary>
        ///// Returns a list of all the validation rules that failed.
        ///// </summary>
        ///// <returns><see cref="BrokenRulesCol" /></returns>
        //public virtual BrokenRulesCol BrokenRulesCol
        //{
        //    get
        //    {
        //        return ValidationRules.GetBrokenRules();
        //    }
        //}

        ///// <summary>
        ///// Force this object to validate itself using the assigned business rules.
        ///// </summary>
        ///// <remarks>Validates all properties.</remarks>
        //public void Validate()
        //{
        //    ValidationRules.CheckRules();
        //}

        ///// <summary>
        ///// Force the object to validate itself using the assigned business rules.
        ///// </summary>
        ///// <param name="propertyName">Name of the property to validate.</param>
        //public void Validate(string propertyName)
        //{
        //    ValidationRules.CheckRules(propertyName);
        //}

        ///// <summary>
        ///// Force the object to validate itself using the assigned business rules.
        ///// </summary>
        ///// <param name="column">Column enumeration representingt the column to validate.</param>
        //public void Validate(System.Enum column)
        //{
        //    Validate(column.ToString());
        //}
        //#endregion
    }
}
 