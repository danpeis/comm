
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// Encapsulates the 'Parametros' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("Parametros")]
	public partial class Parametros : esParametros
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new Parametros();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal parCodPK)
		{
			var obj = new Parametros();
			obj.ParCodPK = parCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal parCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new Parametros();
			obj.ParCodPK = parCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal PARCODPK)
        {
            try
            {
                var e = new Parametros();
                return (e.LoadByPrimaryKey(PARCODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static Parametros Get(System.Decimal PARCODPK)
        {
            try
            {
                var e = new Parametros();
                return (e.LoadByPrimaryKey(PARCODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public Parametros(System.Decimal PARCODPK)
        {
            this.LoadByPrimaryKey(PARCODPK);
        }
		
		public Parametros()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("ParametrosCol")]
	public partial class ParametrosCol : esParametrosCol, IEnumerable<Parametros>
	{
	
		public ParametrosCol()
		{
		}

	
	
		public Parametros FindByPrimaryKey(System.Decimal parCodPK)
		{
			return this.SingleOrDefault(e => e.ParCodPK == parCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(Parametros))]
		public class ParametrosColWCFPacket : esCollectionWCFPacket<ParametrosCol>
		{
			public static implicit operator ParametrosCol(ParametrosColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator ParametrosColWCFPacket(ParametrosCol collection)
			{
				return new ParametrosColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class ParametrosQ : esParametrosQ
	{
		public ParametrosQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public ParametrosQ()
		{
		}

		override protected string GetQueryName()
		{
			return "ParametrosQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new ParametrosQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(ParametrosQ query)
		{
			return ParametrosQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator ParametrosQ(string query)
		{
			return (ParametrosQ)ParametrosQ.SerializeHelper.FromXml(query, typeof(ParametrosQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esParametros : EntityBase
	{
		public esParametros()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal parCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(parCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(parCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal parCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(parCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(parCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal parCodPK)
		{
			ParametrosQ query = new ParametrosQ();
			query.Where(query.ParCodPK == parCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal parCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("ParCodPK", parCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to Parametros.par_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ParCodPK
		{
			get
			{
				return base.GetSystemDecimal(ParametrosMetadata.ColumnNames.ParCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(ParametrosMetadata.ColumnNames.ParCodPK, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_Ultimo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? ParUltimo
		{
			get
			{
				return base.GetSystemInt32(ParametrosMetadata.ColumnNames.ParUltimo);
			}
			
			set
			{
				if(base.SetSystemInt32(ParametrosMetadata.ColumnNames.ParUltimo, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParUltimo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_Consultas
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParConsultas
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParConsultas);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParConsultas, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParConsultas);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_Dialisis
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParDialisis
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParDialisis);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParDialisis, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParDialisis);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_Terapias
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParTerapias
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParTerapias);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParTerapias, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParTerapias);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_RH
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParRH
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParRH);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParRH, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParRH);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_Curas
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParCuras
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParCuras);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParCuras, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParCuras);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_PlazasSentado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParPlazasSentado
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParPlazasSentado);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParPlazasSentado, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParPlazasSentado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_PlazasTendido
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParPlazasTendido
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParPlazasTendido);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParPlazasTendido, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParPlazasTendido);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_PlazasSilla
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParPlazasSilla
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParPlazasSilla);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParPlazasSilla, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParPlazasSilla);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_Moneda
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Char? ParMoneda
		{
			get
			{
				return base.GetSystemChar(ParametrosMetadata.ColumnNames.ParMoneda);
			}
			
			set
			{
				if(base.SetSystemChar(ParametrosMetadata.ColumnNames.ParMoneda, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParMoneda);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_UmbralLcd
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParUmbralLcd
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParUmbralLcd);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParUmbralLcd, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParUmbralLcd);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_NCmp
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ParNCmp
		{
			get
			{
				return base.GetSystemString(ParametrosMetadata.ColumnNames.ParNCmp);
			}
			
			set
			{
				if(base.SetSystemString(ParametrosMetadata.ColumnNames.ParNCmp, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCmp);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_NCrtRefEmp
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParNCrtRefEmp
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefEmp);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefEmp, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefEmp);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_NCrtRefPrv
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParNCrtRefPrv
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefPrv);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefPrv, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefPrv);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_NCrtRefAreT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParNCrtRefAreT
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefAreT);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefAreT, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefAreT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_NCrtRefAreS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParNCrtRefAreS
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefAreS);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefAreS, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefAreS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_NCrtRefCen
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParNCrtRefCen
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefCen);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefCen, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefCen);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_NCrtRefLcd
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParNCrtRefLcd
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefLcd);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefLcd, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefLcd);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_NCrtRefOD
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParNCrtRefOD
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefOD);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefOD, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefOD);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_NCrtRefDst
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ParNCrtRefDst
		{
			get
			{
				return base.GetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefDst);
			}
			
			set
			{
				if(base.SetSystemInt16(ParametrosMetadata.ColumnNames.ParNCrtRefDst, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefDst);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_CaractActivas
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ParCaractActivas
		{
			get
			{
				return base.GetSystemDecimal(ParametrosMetadata.ColumnNames.ParCaractActivas);
			}
			
			set
			{
				if(base.SetSystemDecimal(ParametrosMetadata.ColumnNames.ParCaractActivas, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParCaractActivas);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_PKTimeSemilla
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? ParPKTimeSemilla
		{
			get
			{
				return base.GetSystemDateTime(ParametrosMetadata.ColumnNames.ParPKTimeSemilla);
			}
			
			set
			{
				if(base.SetSystemDateTime(ParametrosMetadata.ColumnNames.ParPKTimeSemilla, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParPKTimeSemilla);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_Version
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ParVersion
		{
			get
			{
				return base.GetSystemDecimal(ParametrosMetadata.ColumnNames.ParVersion);
			}
			
			set
			{
				if(base.SetSystemDecimal(ParametrosMetadata.ColumnNames.ParVersion, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParVersion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_ActualizacionEnProceso
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? ParActualizacionEnProceso
		{
			get
			{
				return base.GetSystemBoolean(ParametrosMetadata.ColumnNames.ParActualizacionEnProceso);
			}
			
			set
			{
				if(base.SetSystemBoolean(ParametrosMetadata.ColumnNames.ParActualizacionEnProceso, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParActualizacionEnProceso);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_TraspasoEnProceso
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? ParTraspasoEnProceso
		{
			get
			{
				return base.GetSystemBoolean(ParametrosMetadata.ColumnNames.ParTraspasoEnProceso);
			}
			
			set
			{
				if(base.SetSystemBoolean(ParametrosMetadata.ColumnNames.ParTraspasoEnProceso, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParTraspasoEnProceso);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_TerminalTipo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ParTerminalTipo
		{
			get
			{
				return base.GetSystemDecimal(ParametrosMetadata.ColumnNames.ParTerminalTipo);
			}
			
			set
			{
				if(base.SetSystemDecimal(ParametrosMetadata.ColumnNames.ParTerminalTipo, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParTerminalTipo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_MovilTipo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ParMovilTipo
		{
			get
			{
				return base.GetSystemDecimal(ParametrosMetadata.ColumnNames.ParMovilTipo);
			}
			
			set
			{
				if(base.SetSystemDecimal(ParametrosMetadata.ColumnNames.ParMovilTipo, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParMovilTipo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_InitialCatalogZENDAT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ParInitialCatalogZENDAT
		{
			get
			{
				return base.GetSystemString(ParametrosMetadata.ColumnNames.ParInitialCatalogZENDAT);
			}
			
			set
			{
				if(base.SetSystemString(ParametrosMetadata.ColumnNames.ParInitialCatalogZENDAT, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParInitialCatalogZENDAT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_DataSourceZENDAT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ParDataSourceZENDAT
		{
			get
			{
				return base.GetSystemString(ParametrosMetadata.ColumnNames.ParDataSourceZENDAT);
			}
			
			set
			{
				if(base.SetSystemString(ParametrosMetadata.ColumnNames.ParDataSourceZENDAT, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParDataSourceZENDAT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_InitialCatalogZENGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ParInitialCatalogZENGPS
		{
			get
			{
				return base.GetSystemString(ParametrosMetadata.ColumnNames.ParInitialCatalogZENGPS);
			}
			
			set
			{
				if(base.SetSystemString(ParametrosMetadata.ColumnNames.ParInitialCatalogZENGPS, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParInitialCatalogZENGPS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_DataSourceZENGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ParDataSourceZENGPS
		{
			get
			{
				return base.GetSystemString(ParametrosMetadata.ColumnNames.ParDataSourceZENGPS);
			}
			
			set
			{
				if(base.SetSystemString(ParametrosMetadata.ColumnNames.ParDataSourceZENGPS, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParDataSourceZENGPS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_ServidorVinculadoZENDAT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ParServidorVinculadoZENDAT
		{
			get
			{
				return base.GetSystemString(ParametrosMetadata.ColumnNames.ParServidorVinculadoZENDAT);
			}
			
			set
			{
				if(base.SetSystemString(ParametrosMetadata.ColumnNames.ParServidorVinculadoZENDAT, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParServidorVinculadoZENDAT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_ServidorVinculadoZENGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ParServidorVinculadoZENGPS
		{
			get
			{
				return base.GetSystemString(ParametrosMetadata.ColumnNames.ParServidorVinculadoZENGPS);
			}
			
			set
			{
				if(base.SetSystemString(ParametrosMetadata.ColumnNames.ParServidorVinculadoZENGPS, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParServidorVinculadoZENGPS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_Licencia
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte[] ParLicencia
		{
			get
			{
				return base.GetSystemByteArray(ParametrosMetadata.ColumnNames.ParLicencia);
			}
			
			set
			{
				if(base.SetSystemByteArray(ParametrosMetadata.ColumnNames.ParLicencia, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParLicencia);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_DistanciasDobles
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? ParDistanciasDobles
		{
			get
			{
				return base.GetSystemBoolean(ParametrosMetadata.ColumnNames.ParDistanciasDobles);
			}
			
			set
			{
				if(base.SetSystemBoolean(ParametrosMetadata.ColumnNames.ParDistanciasDobles, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParDistanciasDobles);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_Tiempo100Km
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ParTiempo100Km
		{
			get
			{
				return base.GetSystemDecimal(ParametrosMetadata.ColumnNames.ParTiempo100Km);
			}
			
			set
			{
				if(base.SetSystemDecimal(ParametrosMetadata.ColumnNames.ParTiempo100Km, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParTiempo100Km);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_InformesSES
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? ParInformesSES
		{
			get
			{
				return base.GetSystemBoolean(ParametrosMetadata.ColumnNames.ParInformesSES);
			}
			
			set
			{
				if(base.SetSystemBoolean(ParametrosMetadata.ColumnNames.ParInformesSES, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParInformesSES);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Parametros.par_InformesSMS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? ParInformesSMS
		{
			get
			{
				return base.GetSystemBoolean(ParametrosMetadata.ColumnNames.ParInformesSMS);
			}
			
			set
			{
				if(base.SetSystemBoolean(ParametrosMetadata.ColumnNames.ParInformesSMS, value))
				{
					OnPropertyChanged(ParametrosMetadata.PropertyNames.ParInformesSMS);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "ParCodPK": this.str().ParCodPK = (string)value; break;							
						case "ParUltimo": this.str().ParUltimo = (string)value; break;							
						case "ParConsultas": this.str().ParConsultas = (string)value; break;							
						case "ParDialisis": this.str().ParDialisis = (string)value; break;							
						case "ParTerapias": this.str().ParTerapias = (string)value; break;							
						case "ParRH": this.str().ParRH = (string)value; break;							
						case "ParCuras": this.str().ParCuras = (string)value; break;							
						case "ParPlazasSentado": this.str().ParPlazasSentado = (string)value; break;							
						case "ParPlazasTendido": this.str().ParPlazasTendido = (string)value; break;							
						case "ParPlazasSilla": this.str().ParPlazasSilla = (string)value; break;							
						case "ParMoneda": this.str().ParMoneda = (string)value; break;							
						case "ParUmbralLcd": this.str().ParUmbralLcd = (string)value; break;							
						case "ParNCmp": this.str().ParNCmp = (string)value; break;							
						case "ParNCrtRefEmp": this.str().ParNCrtRefEmp = (string)value; break;							
						case "ParNCrtRefPrv": this.str().ParNCrtRefPrv = (string)value; break;							
						case "ParNCrtRefAreT": this.str().ParNCrtRefAreT = (string)value; break;							
						case "ParNCrtRefAreS": this.str().ParNCrtRefAreS = (string)value; break;							
						case "ParNCrtRefCen": this.str().ParNCrtRefCen = (string)value; break;							
						case "ParNCrtRefLcd": this.str().ParNCrtRefLcd = (string)value; break;							
						case "ParNCrtRefOD": this.str().ParNCrtRefOD = (string)value; break;							
						case "ParNCrtRefDst": this.str().ParNCrtRefDst = (string)value; break;							
						case "ParCaractActivas": this.str().ParCaractActivas = (string)value; break;							
						case "ParPKTimeSemilla": this.str().ParPKTimeSemilla = (string)value; break;							
						case "ParVersion": this.str().ParVersion = (string)value; break;							
						case "ParActualizacionEnProceso": this.str().ParActualizacionEnProceso = (string)value; break;							
						case "ParTraspasoEnProceso": this.str().ParTraspasoEnProceso = (string)value; break;							
						case "ParTerminalTipo": this.str().ParTerminalTipo = (string)value; break;							
						case "ParMovilTipo": this.str().ParMovilTipo = (string)value; break;							
						case "ParInitialCatalogZENDAT": this.str().ParInitialCatalogZENDAT = (string)value; break;							
						case "ParDataSourceZENDAT": this.str().ParDataSourceZENDAT = (string)value; break;							
						case "ParInitialCatalogZENGPS": this.str().ParInitialCatalogZENGPS = (string)value; break;							
						case "ParDataSourceZENGPS": this.str().ParDataSourceZENGPS = (string)value; break;							
						case "ParServidorVinculadoZENDAT": this.str().ParServidorVinculadoZENDAT = (string)value; break;							
						case "ParServidorVinculadoZENGPS": this.str().ParServidorVinculadoZENGPS = (string)value; break;							
						case "ParDistanciasDobles": this.str().ParDistanciasDobles = (string)value; break;							
						case "ParTiempo100Km": this.str().ParTiempo100Km = (string)value; break;							
						case "ParInformesSES": this.str().ParInformesSES = (string)value; break;							
						case "ParInformesSMS": this.str().ParInformesSMS = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "ParCodPK":
						
							if (value == null || value is System.Decimal)
								this.ParCodPK = (System.Decimal?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParCodPK);
							break;
						
						case "ParUltimo":
						
							if (value == null || value is System.Int32)
								this.ParUltimo = (System.Int32?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParUltimo);
							break;
						
						case "ParConsultas":
						
							if (value == null || value is System.Int16)
								this.ParConsultas = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParConsultas);
							break;
						
						case "ParDialisis":
						
							if (value == null || value is System.Int16)
								this.ParDialisis = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParDialisis);
							break;
						
						case "ParTerapias":
						
							if (value == null || value is System.Int16)
								this.ParTerapias = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParTerapias);
							break;
						
						case "ParRH":
						
							if (value == null || value is System.Int16)
								this.ParRH = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParRH);
							break;
						
						case "ParCuras":
						
							if (value == null || value is System.Int16)
								this.ParCuras = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParCuras);
							break;
						
						case "ParPlazasSentado":
						
							if (value == null || value is System.Int16)
								this.ParPlazasSentado = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParPlazasSentado);
							break;
						
						case "ParPlazasTendido":
						
							if (value == null || value is System.Int16)
								this.ParPlazasTendido = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParPlazasTendido);
							break;
						
						case "ParPlazasSilla":
						
							if (value == null || value is System.Int16)
								this.ParPlazasSilla = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParPlazasSilla);
							break;
						
						case "ParMoneda":
						
							if (value == null || value is System.Char)
								this.ParMoneda = (System.Char?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParMoneda);
							break;
						
						case "ParUmbralLcd":
						
							if (value == null || value is System.Int16)
								this.ParUmbralLcd = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParUmbralLcd);
							break;
						
						case "ParNCrtRefEmp":
						
							if (value == null || value is System.Int16)
								this.ParNCrtRefEmp = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefEmp);
							break;
						
						case "ParNCrtRefPrv":
						
							if (value == null || value is System.Int16)
								this.ParNCrtRefPrv = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefPrv);
							break;
						
						case "ParNCrtRefAreT":
						
							if (value == null || value is System.Int16)
								this.ParNCrtRefAreT = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefAreT);
							break;
						
						case "ParNCrtRefAreS":
						
							if (value == null || value is System.Int16)
								this.ParNCrtRefAreS = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefAreS);
							break;
						
						case "ParNCrtRefCen":
						
							if (value == null || value is System.Int16)
								this.ParNCrtRefCen = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefCen);
							break;
						
						case "ParNCrtRefLcd":
						
							if (value == null || value is System.Int16)
								this.ParNCrtRefLcd = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefLcd);
							break;
						
						case "ParNCrtRefOD":
						
							if (value == null || value is System.Int16)
								this.ParNCrtRefOD = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefOD);
							break;
						
						case "ParNCrtRefDst":
						
							if (value == null || value is System.Int16)
								this.ParNCrtRefDst = (System.Int16?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParNCrtRefDst);
							break;
						
						case "ParCaractActivas":
						
							if (value == null || value is System.Decimal)
								this.ParCaractActivas = (System.Decimal?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParCaractActivas);
							break;
						
						case "ParPKTimeSemilla":
						
							if (value == null || value is System.DateTime)
								this.ParPKTimeSemilla = (System.DateTime?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParPKTimeSemilla);
							break;
						
						case "ParVersion":
						
							if (value == null || value is System.Decimal)
								this.ParVersion = (System.Decimal?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParVersion);
							break;
						
						case "ParActualizacionEnProceso":
						
							if (value == null || value is System.Boolean)
								this.ParActualizacionEnProceso = (System.Boolean?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParActualizacionEnProceso);
							break;
						
						case "ParTraspasoEnProceso":
						
							if (value == null || value is System.Boolean)
								this.ParTraspasoEnProceso = (System.Boolean?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParTraspasoEnProceso);
							break;
						
						case "ParTerminalTipo":
						
							if (value == null || value is System.Decimal)
								this.ParTerminalTipo = (System.Decimal?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParTerminalTipo);
							break;
						
						case "ParMovilTipo":
						
							if (value == null || value is System.Decimal)
								this.ParMovilTipo = (System.Decimal?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParMovilTipo);
							break;
						
						case "ParLicencia":
						
							if (value == null || value is System.Byte[])
								this.ParLicencia = (System.Byte[])value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParLicencia);
							break;
						
						case "ParDistanciasDobles":
						
							if (value == null || value is System.Boolean)
								this.ParDistanciasDobles = (System.Boolean?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParDistanciasDobles);
							break;
						
						case "ParTiempo100Km":
						
							if (value == null || value is System.Decimal)
								this.ParTiempo100Km = (System.Decimal?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParTiempo100Km);
							break;
						
						case "ParInformesSES":
						
							if (value == null || value is System.Boolean)
								this.ParInformesSES = (System.Boolean?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParInformesSES);
							break;
						
						case "ParInformesSMS":
						
							if (value == null || value is System.Boolean)
								this.ParInformesSMS = (System.Boolean?)value;
								OnPropertyChanged(ParametrosMetadata.PropertyNames.ParInformesSMS);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esParametros entity)
			{
				this.entity = entity;
			}
			
	
			public System.String ParCodPK
			{
				get
				{
					System.Decimal? data = entity.ParCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParCodPK = null;
					else entity.ParCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String ParUltimo
			{
				get
				{
					System.Int32? data = entity.ParUltimo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParUltimo = null;
					else entity.ParUltimo = Convert.ToInt32(value);
				}
			}
				
			public System.String ParConsultas
			{
				get
				{
					System.Int16? data = entity.ParConsultas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParConsultas = null;
					else entity.ParConsultas = Convert.ToInt16(value);
				}
			}
				
			public System.String ParDialisis
			{
				get
				{
					System.Int16? data = entity.ParDialisis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParDialisis = null;
					else entity.ParDialisis = Convert.ToInt16(value);
				}
			}
				
			public System.String ParTerapias
			{
				get
				{
					System.Int16? data = entity.ParTerapias;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParTerapias = null;
					else entity.ParTerapias = Convert.ToInt16(value);
				}
			}
				
			public System.String ParRH
			{
				get
				{
					System.Int16? data = entity.ParRH;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParRH = null;
					else entity.ParRH = Convert.ToInt16(value);
				}
			}
				
			public System.String ParCuras
			{
				get
				{
					System.Int16? data = entity.ParCuras;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParCuras = null;
					else entity.ParCuras = Convert.ToInt16(value);
				}
			}
				
			public System.String ParPlazasSentado
			{
				get
				{
					System.Int16? data = entity.ParPlazasSentado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParPlazasSentado = null;
					else entity.ParPlazasSentado = Convert.ToInt16(value);
				}
			}
				
			public System.String ParPlazasTendido
			{
				get
				{
					System.Int16? data = entity.ParPlazasTendido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParPlazasTendido = null;
					else entity.ParPlazasTendido = Convert.ToInt16(value);
				}
			}
				
			public System.String ParPlazasSilla
			{
				get
				{
					System.Int16? data = entity.ParPlazasSilla;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParPlazasSilla = null;
					else entity.ParPlazasSilla = Convert.ToInt16(value);
				}
			}
				
			public System.String ParMoneda
			{
				get
				{
					System.Char? data = entity.ParMoneda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParMoneda = null;
					else entity.ParMoneda = Convert.ToChar(value);
				}
			}
				
			public System.String ParUmbralLcd
			{
				get
				{
					System.Int16? data = entity.ParUmbralLcd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParUmbralLcd = null;
					else entity.ParUmbralLcd = Convert.ToInt16(value);
				}
			}
				
			public System.String ParNCmp
			{
				get
				{
					System.String data = entity.ParNCmp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParNCmp = null;
					else entity.ParNCmp = Convert.ToString(value);
				}
			}
				
			public System.String ParNCrtRefEmp
			{
				get
				{
					System.Int16? data = entity.ParNCrtRefEmp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParNCrtRefEmp = null;
					else entity.ParNCrtRefEmp = Convert.ToInt16(value);
				}
			}
				
			public System.String ParNCrtRefPrv
			{
				get
				{
					System.Int16? data = entity.ParNCrtRefPrv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParNCrtRefPrv = null;
					else entity.ParNCrtRefPrv = Convert.ToInt16(value);
				}
			}
				
			public System.String ParNCrtRefAreT
			{
				get
				{
					System.Int16? data = entity.ParNCrtRefAreT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParNCrtRefAreT = null;
					else entity.ParNCrtRefAreT = Convert.ToInt16(value);
				}
			}
				
			public System.String ParNCrtRefAreS
			{
				get
				{
					System.Int16? data = entity.ParNCrtRefAreS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParNCrtRefAreS = null;
					else entity.ParNCrtRefAreS = Convert.ToInt16(value);
				}
			}
				
			public System.String ParNCrtRefCen
			{
				get
				{
					System.Int16? data = entity.ParNCrtRefCen;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParNCrtRefCen = null;
					else entity.ParNCrtRefCen = Convert.ToInt16(value);
				}
			}
				
			public System.String ParNCrtRefLcd
			{
				get
				{
					System.Int16? data = entity.ParNCrtRefLcd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParNCrtRefLcd = null;
					else entity.ParNCrtRefLcd = Convert.ToInt16(value);
				}
			}
				
			public System.String ParNCrtRefOD
			{
				get
				{
					System.Int16? data = entity.ParNCrtRefOD;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParNCrtRefOD = null;
					else entity.ParNCrtRefOD = Convert.ToInt16(value);
				}
			}
				
			public System.String ParNCrtRefDst
			{
				get
				{
					System.Int16? data = entity.ParNCrtRefDst;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParNCrtRefDst = null;
					else entity.ParNCrtRefDst = Convert.ToInt16(value);
				}
			}
				
			public System.String ParCaractActivas
			{
				get
				{
					System.Decimal? data = entity.ParCaractActivas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParCaractActivas = null;
					else entity.ParCaractActivas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ParPKTimeSemilla
			{
				get
				{
					System.DateTime? data = entity.ParPKTimeSemilla;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParPKTimeSemilla = null;
					else entity.ParPKTimeSemilla = Convert.ToDateTime(value);
				}
			}
				
			public System.String ParVersion
			{
				get
				{
					System.Decimal? data = entity.ParVersion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParVersion = null;
					else entity.ParVersion = Convert.ToDecimal(value);
				}
			}
				
			public System.String ParActualizacionEnProceso
			{
				get
				{
					System.Boolean? data = entity.ParActualizacionEnProceso;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParActualizacionEnProceso = null;
					else entity.ParActualizacionEnProceso = Convert.ToBoolean(value);
				}
			}
				
			public System.String ParTraspasoEnProceso
			{
				get
				{
					System.Boolean? data = entity.ParTraspasoEnProceso;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParTraspasoEnProceso = null;
					else entity.ParTraspasoEnProceso = Convert.ToBoolean(value);
				}
			}
				
			public System.String ParTerminalTipo
			{
				get
				{
					System.Decimal? data = entity.ParTerminalTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParTerminalTipo = null;
					else entity.ParTerminalTipo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ParMovilTipo
			{
				get
				{
					System.Decimal? data = entity.ParMovilTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParMovilTipo = null;
					else entity.ParMovilTipo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ParInitialCatalogZENDAT
			{
				get
				{
					System.String data = entity.ParInitialCatalogZENDAT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParInitialCatalogZENDAT = null;
					else entity.ParInitialCatalogZENDAT = Convert.ToString(value);
				}
			}
				
			public System.String ParDataSourceZENDAT
			{
				get
				{
					System.String data = entity.ParDataSourceZENDAT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParDataSourceZENDAT = null;
					else entity.ParDataSourceZENDAT = Convert.ToString(value);
				}
			}
				
			public System.String ParInitialCatalogZENGPS
			{
				get
				{
					System.String data = entity.ParInitialCatalogZENGPS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParInitialCatalogZENGPS = null;
					else entity.ParInitialCatalogZENGPS = Convert.ToString(value);
				}
			}
				
			public System.String ParDataSourceZENGPS
			{
				get
				{
					System.String data = entity.ParDataSourceZENGPS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParDataSourceZENGPS = null;
					else entity.ParDataSourceZENGPS = Convert.ToString(value);
				}
			}
				
			public System.String ParServidorVinculadoZENDAT
			{
				get
				{
					System.String data = entity.ParServidorVinculadoZENDAT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParServidorVinculadoZENDAT = null;
					else entity.ParServidorVinculadoZENDAT = Convert.ToString(value);
				}
			}
				
			public System.String ParServidorVinculadoZENGPS
			{
				get
				{
					System.String data = entity.ParServidorVinculadoZENGPS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParServidorVinculadoZENGPS = null;
					else entity.ParServidorVinculadoZENGPS = Convert.ToString(value);
				}
			}
				
			public System.String ParDistanciasDobles
			{
				get
				{
					System.Boolean? data = entity.ParDistanciasDobles;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParDistanciasDobles = null;
					else entity.ParDistanciasDobles = Convert.ToBoolean(value);
				}
			}
				
			public System.String ParTiempo100Km
			{
				get
				{
					System.Decimal? data = entity.ParTiempo100Km;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParTiempo100Km = null;
					else entity.ParTiempo100Km = Convert.ToDecimal(value);
				}
			}
				
			public System.String ParInformesSES
			{
				get
				{
					System.Boolean? data = entity.ParInformesSES;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParInformesSES = null;
					else entity.ParInformesSES = Convert.ToBoolean(value);
				}
			}
				
			public System.String ParInformesSMS
			{
				get
				{
					System.Boolean? data = entity.ParInformesSMS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParInformesSMS = null;
					else entity.ParInformesSMS = Convert.ToBoolean(value);
				}
			}
			

			private esParametros entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return ParametrosMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public ParametrosQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ParametrosQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ParametrosQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(ParametrosQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private ParametrosQ query;		
	}



	[Serializable]
	abstract public partial class esParametrosCol : CollectionBase<Parametros>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ParametrosMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "ParametrosCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public ParametrosQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ParametrosQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ParametrosQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ParametrosQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(ParametrosQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((ParametrosQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private ParametrosQ query;
	}



	[Serializable]
	abstract public partial class esParametrosQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return ParametrosMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParCodPK,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParCodPK, esSystemType.Decimal));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParUltimo,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParUltimo, esSystemType.Int32));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParConsultas,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParConsultas, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParDialisis,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParDialisis, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParTerapias,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParTerapias, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParRH,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParRH, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParCuras,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParCuras, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParPlazasSentado,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParPlazasSentado, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParPlazasTendido,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParPlazasTendido, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParPlazasSilla,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParPlazasSilla, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParMoneda,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParMoneda, esSystemType.Char));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParUmbralLcd,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParUmbralLcd, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParNCmp,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCmp, esSystemType.String));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParNCrtRefEmp,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefEmp, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParNCrtRefPrv,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefPrv, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParNCrtRefAreT,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefAreT, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParNCrtRefAreS,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefAreS, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParNCrtRefCen,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefCen, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParNCrtRefLcd,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefLcd, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParNCrtRefOD,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefOD, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParNCrtRefDst,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefDst, esSystemType.Int16));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParCaractActivas,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParCaractActivas, esSystemType.Decimal));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParPKTimeSemilla,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParPKTimeSemilla, esSystemType.DateTime));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParVersion,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParVersion, esSystemType.Decimal));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParActualizacionEnProceso,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParActualizacionEnProceso, esSystemType.Boolean));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParTraspasoEnProceso,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParTraspasoEnProceso, esSystemType.Boolean));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParTerminalTipo,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParTerminalTipo, esSystemType.Decimal));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParMovilTipo,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParMovilTipo, esSystemType.Decimal));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParInitialCatalogZENDAT,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParInitialCatalogZENDAT, esSystemType.String));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParDataSourceZENDAT,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParDataSourceZENDAT, esSystemType.String));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParInitialCatalogZENGPS,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParInitialCatalogZENGPS, esSystemType.String));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParDataSourceZENGPS,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParDataSourceZENGPS, esSystemType.String));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParServidorVinculadoZENDAT,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParServidorVinculadoZENDAT, esSystemType.String));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParServidorVinculadoZENGPS,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParServidorVinculadoZENGPS, esSystemType.String));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParLicencia,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParLicencia, esSystemType.ByteArray));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParDistanciasDobles,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParDistanciasDobles, esSystemType.Boolean));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParTiempo100Km,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParTiempo100Km, esSystemType.Decimal));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParInformesSES,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParInformesSES, esSystemType.Boolean));
			_queryItems.Add(ParametrosMetadata.ColumnNames.ParInformesSMS,new esQueryItem(this, ParametrosMetadata.ColumnNames.ParInformesSMS, esSystemType.Boolean));
			
	    }
	
	    #region esQueryItems

		public esQueryItem ParCodPK
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem ParUltimo
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParUltimo, esSystemType.Int32); }
		} 
		
		public esQueryItem ParConsultas
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParConsultas, esSystemType.Int16); }
		} 
		
		public esQueryItem ParDialisis
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParDialisis, esSystemType.Int16); }
		} 
		
		public esQueryItem ParTerapias
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParTerapias, esSystemType.Int16); }
		} 
		
		public esQueryItem ParRH
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParRH, esSystemType.Int16); }
		} 
		
		public esQueryItem ParCuras
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParCuras, esSystemType.Int16); }
		} 
		
		public esQueryItem ParPlazasSentado
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParPlazasSentado, esSystemType.Int16); }
		} 
		
		public esQueryItem ParPlazasTendido
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParPlazasTendido, esSystemType.Int16); }
		} 
		
		public esQueryItem ParPlazasSilla
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParPlazasSilla, esSystemType.Int16); }
		} 
		
		public esQueryItem ParMoneda
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParMoneda, esSystemType.Char); }
		} 
		
		public esQueryItem ParUmbralLcd
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParUmbralLcd, esSystemType.Int16); }
		} 
		
		public esQueryItem ParNCmp
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCmp, esSystemType.String); }
		} 
		
		public esQueryItem ParNCrtRefEmp
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefEmp, esSystemType.Int16); }
		} 
		
		public esQueryItem ParNCrtRefPrv
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefPrv, esSystemType.Int16); }
		} 
		
		public esQueryItem ParNCrtRefAreT
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefAreT, esSystemType.Int16); }
		} 
		
		public esQueryItem ParNCrtRefAreS
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefAreS, esSystemType.Int16); }
		} 
		
		public esQueryItem ParNCrtRefCen
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefCen, esSystemType.Int16); }
		} 
		
		public esQueryItem ParNCrtRefLcd
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefLcd, esSystemType.Int16); }
		} 
		
		public esQueryItem ParNCrtRefOD
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefOD, esSystemType.Int16); }
		} 
		
		public esQueryItem ParNCrtRefDst
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParNCrtRefDst, esSystemType.Int16); }
		} 
		
		public esQueryItem ParCaractActivas
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParCaractActivas, esSystemType.Decimal); }
		} 
		
		public esQueryItem ParPKTimeSemilla
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParPKTimeSemilla, esSystemType.DateTime); }
		} 
		
		public esQueryItem ParVersion
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParVersion, esSystemType.Decimal); }
		} 
		
		public esQueryItem ParActualizacionEnProceso
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParActualizacionEnProceso, esSystemType.Boolean); }
		} 
		
		public esQueryItem ParTraspasoEnProceso
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParTraspasoEnProceso, esSystemType.Boolean); }
		} 
		
		public esQueryItem ParTerminalTipo
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParTerminalTipo, esSystemType.Decimal); }
		} 
		
		public esQueryItem ParMovilTipo
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParMovilTipo, esSystemType.Decimal); }
		} 
		
		public esQueryItem ParInitialCatalogZENDAT
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParInitialCatalogZENDAT, esSystemType.String); }
		} 
		
		public esQueryItem ParDataSourceZENDAT
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParDataSourceZENDAT, esSystemType.String); }
		} 
		
		public esQueryItem ParInitialCatalogZENGPS
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParInitialCatalogZENGPS, esSystemType.String); }
		} 
		
		public esQueryItem ParDataSourceZENGPS
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParDataSourceZENGPS, esSystemType.String); }
		} 
		
		public esQueryItem ParServidorVinculadoZENDAT
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParServidorVinculadoZENDAT, esSystemType.String); }
		} 
		
		public esQueryItem ParServidorVinculadoZENGPS
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParServidorVinculadoZENGPS, esSystemType.String); }
		} 
		
		public esQueryItem ParLicencia
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParLicencia, esSystemType.ByteArray); }
		} 
		
		public esQueryItem ParDistanciasDobles
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParDistanciasDobles, esSystemType.Boolean); }
		} 
		
		public esQueryItem ParTiempo100Km
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParTiempo100Km, esSystemType.Decimal); }
		} 
		
		public esQueryItem ParInformesSES
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParInformesSES, esSystemType.Boolean); }
		} 
		
		public esQueryItem ParInformesSMS
		{
			get { return new esQueryItem(this, ParametrosMetadata.ColumnNames.ParInformesSMS, esSystemType.Boolean); }
		} 
		
		#endregion
		
	}


	
	public partial class Parametros : esParametros
	{

		
		
	}
	



	[Serializable]
	public partial class ParametrosMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ParametrosMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParUltimo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParUltimo;
			c.NumericPrecision = 10;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParConsultas, 2, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParConsultas;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParDialisis, 3, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParDialisis;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParTerapias, 4, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParTerapias;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParRH, 5, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParRH;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParCuras, 6, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParCuras;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParPlazasSentado, 7, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParPlazasSentado;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((8))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParPlazasTendido, 8, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParPlazasTendido;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParPlazasSilla, 9, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParPlazasSilla;
			c.NumericPrecision = 5;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParMoneda, 10, typeof(System.Char), esSystemType.Char);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParMoneda;
			c.CharacterMaxLength = 1;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParUmbralLcd, 11, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParUmbralLcd;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParNCmp, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParNCmp;
			c.CharacterMaxLength = 50;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParNCrtRefEmp, 13, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParNCrtRefEmp;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParNCrtRefPrv, 14, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParNCrtRefPrv;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParNCrtRefAreT, 15, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParNCrtRefAreT;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParNCrtRefAreS, 16, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParNCrtRefAreS;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParNCrtRefCen, 17, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParNCrtRefCen;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParNCrtRefLcd, 18, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParNCrtRefLcd;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParNCrtRefOD, 19, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParNCrtRefOD;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParNCrtRefDst, 20, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParNCrtRefDst;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParCaractActivas, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParCaractActivas;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParPKTimeSemilla, 22, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParPKTimeSemilla;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParVersion, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParVersion;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParActualizacionEnProceso, 24, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParActualizacionEnProceso;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParTraspasoEnProceso, 25, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParTraspasoEnProceso;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParTerminalTipo, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParTerminalTipo;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParMovilTipo, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParMovilTipo;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParInitialCatalogZENDAT, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParInitialCatalogZENDAT;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParDataSourceZENDAT, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParDataSourceZENDAT;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParInitialCatalogZENGPS, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParInitialCatalogZENGPS;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParDataSourceZENGPS, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParDataSourceZENGPS;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParServidorVinculadoZENDAT, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParServidorVinculadoZENDAT;
			c.CharacterMaxLength = 150;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParServidorVinculadoZENGPS, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParServidorVinculadoZENGPS;
			c.CharacterMaxLength = 150;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParLicencia, 34, typeof(System.Byte[]), esSystemType.ByteArray);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParLicencia;
			c.CharacterMaxLength = 2147483647;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParDistanciasDobles, 35, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParDistanciasDobles;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParTiempo100Km, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParTiempo100Km;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((60))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParInformesSES, 37, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParInformesSES;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ParametrosMetadata.ColumnNames.ParInformesSMS, 38, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = ParametrosMetadata.PropertyNames.ParInformesSMS;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public ParametrosMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string ParCodPK = "par_CodPK";
			 public const string ParUltimo = "par_Ultimo";
			 public const string ParConsultas = "par_Consultas";
			 public const string ParDialisis = "par_Dialisis";
			 public const string ParTerapias = "par_Terapias";
			 public const string ParRH = "par_RH";
			 public const string ParCuras = "par_Curas";
			 public const string ParPlazasSentado = "par_PlazasSentado";
			 public const string ParPlazasTendido = "par_PlazasTendido";
			 public const string ParPlazasSilla = "par_PlazasSilla";
			 public const string ParMoneda = "par_Moneda";
			 public const string ParUmbralLcd = "par_UmbralLcd";
			 public const string ParNCmp = "par_NCmp";
			 public const string ParNCrtRefEmp = "par_NCrtRefEmp";
			 public const string ParNCrtRefPrv = "par_NCrtRefPrv";
			 public const string ParNCrtRefAreT = "par_NCrtRefAreT";
			 public const string ParNCrtRefAreS = "par_NCrtRefAreS";
			 public const string ParNCrtRefCen = "par_NCrtRefCen";
			 public const string ParNCrtRefLcd = "par_NCrtRefLcd";
			 public const string ParNCrtRefOD = "par_NCrtRefOD";
			 public const string ParNCrtRefDst = "par_NCrtRefDst";
			 public const string ParCaractActivas = "par_CaractActivas";
			 public const string ParPKTimeSemilla = "par_PKTimeSemilla";
			 public const string ParVersion = "par_Version";
			 public const string ParActualizacionEnProceso = "par_ActualizacionEnProceso";
			 public const string ParTraspasoEnProceso = "par_TraspasoEnProceso";
			 public const string ParTerminalTipo = "par_TerminalTipo";
			 public const string ParMovilTipo = "par_MovilTipo";
			 public const string ParInitialCatalogZENDAT = "par_InitialCatalogZENDAT";
			 public const string ParDataSourceZENDAT = "par_DataSourceZENDAT";
			 public const string ParInitialCatalogZENGPS = "par_InitialCatalogZENGPS";
			 public const string ParDataSourceZENGPS = "par_DataSourceZENGPS";
			 public const string ParServidorVinculadoZENDAT = "par_ServidorVinculadoZENDAT";
			 public const string ParServidorVinculadoZENGPS = "par_ServidorVinculadoZENGPS";
			 public const string ParLicencia = "par_Licencia";
			 public const string ParDistanciasDobles = "par_DistanciasDobles";
			 public const string ParTiempo100Km = "par_Tiempo100Km";
			 public const string ParInformesSES = "par_InformesSES";
			 public const string ParInformesSMS = "par_InformesSMS";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string ParCodPK = "ParCodPK";
			 public const string ParUltimo = "ParUltimo";
			 public const string ParConsultas = "ParConsultas";
			 public const string ParDialisis = "ParDialisis";
			 public const string ParTerapias = "ParTerapias";
			 public const string ParRH = "ParRH";
			 public const string ParCuras = "ParCuras";
			 public const string ParPlazasSentado = "ParPlazasSentado";
			 public const string ParPlazasTendido = "ParPlazasTendido";
			 public const string ParPlazasSilla = "ParPlazasSilla";
			 public const string ParMoneda = "ParMoneda";
			 public const string ParUmbralLcd = "ParUmbralLcd";
			 public const string ParNCmp = "ParNCmp";
			 public const string ParNCrtRefEmp = "ParNCrtRefEmp";
			 public const string ParNCrtRefPrv = "ParNCrtRefPrv";
			 public const string ParNCrtRefAreT = "ParNCrtRefAreT";
			 public const string ParNCrtRefAreS = "ParNCrtRefAreS";
			 public const string ParNCrtRefCen = "ParNCrtRefCen";
			 public const string ParNCrtRefLcd = "ParNCrtRefLcd";
			 public const string ParNCrtRefOD = "ParNCrtRefOD";
			 public const string ParNCrtRefDst = "ParNCrtRefDst";
			 public const string ParCaractActivas = "ParCaractActivas";
			 public const string ParPKTimeSemilla = "ParPKTimeSemilla";
			 public const string ParVersion = "ParVersion";
			 public const string ParActualizacionEnProceso = "ParActualizacionEnProceso";
			 public const string ParTraspasoEnProceso = "ParTraspasoEnProceso";
			 public const string ParTerminalTipo = "ParTerminalTipo";
			 public const string ParMovilTipo = "ParMovilTipo";
			 public const string ParInitialCatalogZENDAT = "ParInitialCatalogZENDAT";
			 public const string ParDataSourceZENDAT = "ParDataSourceZENDAT";
			 public const string ParInitialCatalogZENGPS = "ParInitialCatalogZENGPS";
			 public const string ParDataSourceZENGPS = "ParDataSourceZENGPS";
			 public const string ParServidorVinculadoZENDAT = "ParServidorVinculadoZENDAT";
			 public const string ParServidorVinculadoZENGPS = "ParServidorVinculadoZENGPS";
			 public const string ParLicencia = "ParLicencia";
			 public const string ParDistanciasDobles = "ParDistanciasDobles";
			 public const string ParTiempo100Km = "ParTiempo100Km";
			 public const string ParInformesSES = "ParInformesSES";
			 public const string ParInformesSMS = "ParInformesSMS";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ParametrosMetadata))
			{
				if(ParametrosMetadata.mapDelegates == null)
				{
					ParametrosMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ParametrosMetadata.meta == null)
				{
					ParametrosMetadata.meta = new ParametrosMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("ParCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ParUltimo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ParConsultas", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParDialisis", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParTerapias", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParRH", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParCuras", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParPlazasSentado", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParPlazasTendido", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParPlazasSilla", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParMoneda", new esTypeMap("nvarchar", "System.Char"));
				meta.AddTypeMap("ParUmbralLcd", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParNCmp", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ParNCrtRefEmp", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParNCrtRefPrv", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParNCrtRefAreT", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParNCrtRefAreS", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParNCrtRefCen", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParNCrtRefLcd", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParNCrtRefOD", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParNCrtRefDst", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParCaractActivas", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ParPKTimeSemilla", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ParVersion", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ParActualizacionEnProceso", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("ParTraspasoEnProceso", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("ParTerminalTipo", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ParMovilTipo", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ParInitialCatalogZENDAT", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ParDataSourceZENDAT", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ParInitialCatalogZENGPS", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ParDataSourceZENGPS", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ParServidorVinculadoZENDAT", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ParServidorVinculadoZENGPS", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ParLicencia", new esTypeMap("image", "System.Byte[]"));
				meta.AddTypeMap("ParDistanciasDobles", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("ParTiempo100Km", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ParInformesSES", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("ParInformesSMS", new esTypeMap("bit", "System.Boolean"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "Parametros";
				meta.Destination = "Parametros";
				
				meta.spInsert = "proc_ParametrosInsert";				
				meta.spUpdate = "proc_ParametrosUpdate";		
				meta.spDelete = "proc_ParametrosDelete";
				meta.spLoadAll = "proc_ParametrosLoadAll";
				meta.spLoadByPrimaryKey = "proc_ParametrosLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ParametrosMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
