
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// Encapsulates the 'TiposConsulta' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TiposConsulta")]
	public partial class TiposConsulta : esTiposConsulta
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TiposConsulta();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal tcsCodPK)
		{
			var obj = new TiposConsulta();
			obj.TcsCodPK = tcsCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal tcsCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new TiposConsulta();
			obj.TcsCodPK = tcsCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal TCSCODPK)
        {
            try
            {
                var e = new TiposConsulta();
                return (e.LoadByPrimaryKey(TCSCODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TiposConsulta Get(System.Decimal TCSCODPK)
        {
            try
            {
                var e = new TiposConsulta();
                return (e.LoadByPrimaryKey(TCSCODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TiposConsulta(System.Decimal TCSCODPK)
        {
            this.LoadByPrimaryKey(TCSCODPK);
        }
		
		public TiposConsulta()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TiposConsultaCol")]
	public partial class TiposConsultaCol : esTiposConsultaCol, IEnumerable<TiposConsulta>
	{
	
		public TiposConsultaCol()
		{
		}

	
	
		public TiposConsulta FindByPrimaryKey(System.Decimal tcsCodPK)
		{
			return this.SingleOrDefault(e => e.TcsCodPK == tcsCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TiposConsulta))]
		public class TiposConsultaColWCFPacket : esCollectionWCFPacket<TiposConsultaCol>
		{
			public static implicit operator TiposConsultaCol(TiposConsultaColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TiposConsultaColWCFPacket(TiposConsultaCol collection)
			{
				return new TiposConsultaColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TiposConsultaQ : esTiposConsultaQ
	{
		public TiposConsultaQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TiposConsultaQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TiposConsultaQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new TiposConsultaQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TiposConsultaQ query)
		{
			return TiposConsultaQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TiposConsultaQ(string query)
		{
			return (TiposConsultaQ)TiposConsultaQ.SerializeHelper.FromXml(query, typeof(TiposConsultaQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTiposConsulta : EntityBase
	{
		public esTiposConsulta()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal tcsCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(tcsCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(tcsCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal tcsCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(tcsCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(tcsCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal tcsCodPK)
		{
			TiposConsultaQ query = new TiposConsultaQ();
			query.Where(query.TcsCodPK == tcsCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal tcsCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("TcsCodPK", tcsCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to TiposConsulta.tcs_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TcsCodPK
		{
			get
			{
				return base.GetSystemDecimal(TiposConsultaMetadata.ColumnNames.TcsCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(TiposConsultaMetadata.ColumnNames.TcsCodPK, value))
				{
					OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TiposConsulta.tcs_Nombre
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TcsNombre
		{
			get
			{
				return base.GetSystemString(TiposConsultaMetadata.ColumnNames.TcsNombre);
			}
			
			set
			{
				if(base.SetSystemString(TiposConsultaMetadata.ColumnNames.TcsNombre, value))
				{
					OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsNombre);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TiposConsulta.tcs_Alta
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? TcsAlta
		{
			get
			{
				return base.GetSystemBoolean(TiposConsultaMetadata.ColumnNames.TcsAlta);
			}
			
			set
			{
				if(base.SetSystemBoolean(TiposConsultaMetadata.ColumnNames.TcsAlta, value))
				{
					OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsAlta);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TiposConsulta.tcs_Ref
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TcsRef
		{
			get
			{
				return base.GetSystemDecimal(TiposConsultaMetadata.ColumnNames.TcsRef);
			}
			
			set
			{
				if(base.SetSystemDecimal(TiposConsultaMetadata.ColumnNames.TcsRef, value))
				{
					OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsRef);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TiposConsulta.tcs_ConRetorno
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? TcsConRetorno
		{
			get
			{
				return base.GetSystemBoolean(TiposConsultaMetadata.ColumnNames.TcsConRetorno);
			}
			
			set
			{
				if(base.SetSystemBoolean(TiposConsultaMetadata.ColumnNames.TcsConRetorno, value))
				{
					OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsConRetorno);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TiposConsulta.tcs_Eliminada
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? TcsEliminada
		{
			get
			{
				return base.GetSystemBoolean(TiposConsultaMetadata.ColumnNames.TcsEliminada);
			}
			
			set
			{
				if(base.SetSystemBoolean(TiposConsultaMetadata.ColumnNames.TcsEliminada, value))
				{
					OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsEliminada);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TiposConsulta.tcs_Estado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TcsEstado
		{
			get
			{
				return base.GetSystemDecimal(TiposConsultaMetadata.ColumnNames.TcsEstado);
			}
			
			set
			{
				if(base.SetSystemDecimal(TiposConsultaMetadata.ColumnNames.TcsEstado, value))
				{
					OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsEstado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TiposConsulta.tcs_CodigoAdministracion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TcsCodigoAdministracion
		{
			get
			{
				return base.GetSystemString(TiposConsultaMetadata.ColumnNames.TcsCodigoAdministracion);
			}
			
			set
			{
				if(base.SetSystemString(TiposConsultaMetadata.ColumnNames.TcsCodigoAdministracion, value))
				{
					OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsCodigoAdministracion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TiposConsulta.TipoConsultaPadreId
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TipoConsultaPadreId
		{
			get
			{
				return base.GetSystemDecimal(TiposConsultaMetadata.ColumnNames.TipoConsultaPadreId);
			}
			
			set
			{
				if(base.SetSystemDecimal(TiposConsultaMetadata.ColumnNames.TipoConsultaPadreId, value))
				{
					OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TipoConsultaPadreId);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TcsCodPK": this.str().TcsCodPK = (string)value; break;							
						case "TcsNombre": this.str().TcsNombre = (string)value; break;							
						case "TcsAlta": this.str().TcsAlta = (string)value; break;							
						case "TcsRef": this.str().TcsRef = (string)value; break;							
						case "TcsConRetorno": this.str().TcsConRetorno = (string)value; break;							
						case "TcsEliminada": this.str().TcsEliminada = (string)value; break;							
						case "TcsEstado": this.str().TcsEstado = (string)value; break;							
						case "TcsCodigoAdministracion": this.str().TcsCodigoAdministracion = (string)value; break;							
						case "TipoConsultaPadreId": this.str().TipoConsultaPadreId = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TcsCodPK":
						
							if (value == null || value is System.Decimal)
								this.TcsCodPK = (System.Decimal?)value;
								OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsCodPK);
							break;
						
						case "TcsAlta":
						
							if (value == null || value is System.Boolean)
								this.TcsAlta = (System.Boolean?)value;
								OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsAlta);
							break;
						
						case "TcsRef":
						
							if (value == null || value is System.Decimal)
								this.TcsRef = (System.Decimal?)value;
								OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsRef);
							break;
						
						case "TcsConRetorno":
						
							if (value == null || value is System.Boolean)
								this.TcsConRetorno = (System.Boolean?)value;
								OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsConRetorno);
							break;
						
						case "TcsEliminada":
						
							if (value == null || value is System.Boolean)
								this.TcsEliminada = (System.Boolean?)value;
								OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsEliminada);
							break;
						
						case "TcsEstado":
						
							if (value == null || value is System.Decimal)
								this.TcsEstado = (System.Decimal?)value;
								OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TcsEstado);
							break;
						
						case "TipoConsultaPadreId":
						
							if (value == null || value is System.Decimal)
								this.TipoConsultaPadreId = (System.Decimal?)value;
								OnPropertyChanged(TiposConsultaMetadata.PropertyNames.TipoConsultaPadreId);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTiposConsulta entity)
			{
				this.entity = entity;
			}
			
	
			public System.String TcsCodPK
			{
				get
				{
					System.Decimal? data = entity.TcsCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TcsCodPK = null;
					else entity.TcsCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String TcsNombre
			{
				get
				{
					System.String data = entity.TcsNombre;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TcsNombre = null;
					else entity.TcsNombre = Convert.ToString(value);
				}
			}
				
			public System.String TcsAlta
			{
				get
				{
					System.Boolean? data = entity.TcsAlta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TcsAlta = null;
					else entity.TcsAlta = Convert.ToBoolean(value);
				}
			}
				
			public System.String TcsRef
			{
				get
				{
					System.Decimal? data = entity.TcsRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TcsRef = null;
					else entity.TcsRef = Convert.ToDecimal(value);
				}
			}
				
			public System.String TcsConRetorno
			{
				get
				{
					System.Boolean? data = entity.TcsConRetorno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TcsConRetorno = null;
					else entity.TcsConRetorno = Convert.ToBoolean(value);
				}
			}
				
			public System.String TcsEliminada
			{
				get
				{
					System.Boolean? data = entity.TcsEliminada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TcsEliminada = null;
					else entity.TcsEliminada = Convert.ToBoolean(value);
				}
			}
				
			public System.String TcsEstado
			{
				get
				{
					System.Decimal? data = entity.TcsEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TcsEstado = null;
					else entity.TcsEstado = Convert.ToDecimal(value);
				}
			}
				
			public System.String TcsCodigoAdministracion
			{
				get
				{
					System.String data = entity.TcsCodigoAdministracion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TcsCodigoAdministracion = null;
					else entity.TcsCodigoAdministracion = Convert.ToString(value);
				}
			}
				
			public System.String TipoConsultaPadreId
			{
				get
				{
					System.Decimal? data = entity.TipoConsultaPadreId;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoConsultaPadreId = null;
					else entity.TipoConsultaPadreId = Convert.ToDecimal(value);
				}
			}
			

			private esTiposConsulta entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TiposConsultaMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TiposConsultaQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TiposConsultaQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TiposConsultaQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TiposConsultaQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TiposConsultaQ query;		
	}



	[Serializable]
	abstract public partial class esTiposConsultaCol : CollectionBase<TiposConsulta>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TiposConsultaMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TiposConsultaCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TiposConsultaQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TiposConsultaQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TiposConsultaQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TiposConsultaQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TiposConsultaQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TiposConsultaQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TiposConsultaQ query;
	}



	[Serializable]
	abstract public partial class esTiposConsultaQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TiposConsultaMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TiposConsultaMetadata.ColumnNames.TcsCodPK,new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsCodPK, esSystemType.Decimal));
			_queryItems.Add(TiposConsultaMetadata.ColumnNames.TcsNombre,new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsNombre, esSystemType.String));
			_queryItems.Add(TiposConsultaMetadata.ColumnNames.TcsAlta,new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsAlta, esSystemType.Boolean));
			_queryItems.Add(TiposConsultaMetadata.ColumnNames.TcsRef,new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsRef, esSystemType.Decimal));
			_queryItems.Add(TiposConsultaMetadata.ColumnNames.TcsConRetorno,new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsConRetorno, esSystemType.Boolean));
			_queryItems.Add(TiposConsultaMetadata.ColumnNames.TcsEliminada,new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsEliminada, esSystemType.Boolean));
			_queryItems.Add(TiposConsultaMetadata.ColumnNames.TcsEstado,new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsEstado, esSystemType.Decimal));
			_queryItems.Add(TiposConsultaMetadata.ColumnNames.TcsCodigoAdministracion,new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsCodigoAdministracion, esSystemType.String));
			_queryItems.Add(TiposConsultaMetadata.ColumnNames.TipoConsultaPadreId,new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TipoConsultaPadreId, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem TcsCodPK
		{
			get { return new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem TcsNombre
		{
			get { return new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsNombre, esSystemType.String); }
		} 
		
		public esQueryItem TcsAlta
		{
			get { return new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsAlta, esSystemType.Boolean); }
		} 
		
		public esQueryItem TcsRef
		{
			get { return new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsRef, esSystemType.Decimal); }
		} 
		
		public esQueryItem TcsConRetorno
		{
			get { return new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsConRetorno, esSystemType.Boolean); }
		} 
		
		public esQueryItem TcsEliminada
		{
			get { return new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsEliminada, esSystemType.Boolean); }
		} 
		
		public esQueryItem TcsEstado
		{
			get { return new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsEstado, esSystemType.Decimal); }
		} 
		
		public esQueryItem TcsCodigoAdministracion
		{
			get { return new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TcsCodigoAdministracion, esSystemType.String); }
		} 
		
		public esQueryItem TipoConsultaPadreId
		{
			get { return new esQueryItem(this, TiposConsultaMetadata.ColumnNames.TipoConsultaPadreId, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class TiposConsulta : esTiposConsulta
	{

		
		
	}
	



	[Serializable]
	public partial class TiposConsultaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TiposConsultaMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TiposConsultaMetadata.ColumnNames.TcsCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TiposConsultaMetadata.PropertyNames.TcsCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TiposConsultaMetadata.ColumnNames.TcsNombre, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TiposConsultaMetadata.PropertyNames.TcsNombre;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TiposConsultaMetadata.ColumnNames.TcsAlta, 2, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TiposConsultaMetadata.PropertyNames.TcsAlta;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TiposConsultaMetadata.ColumnNames.TcsRef, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TiposConsultaMetadata.PropertyNames.TcsRef;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TiposConsultaMetadata.ColumnNames.TcsConRetorno, 4, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TiposConsultaMetadata.PropertyNames.TcsConRetorno;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TiposConsultaMetadata.ColumnNames.TcsEliminada, 5, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TiposConsultaMetadata.PropertyNames.TcsEliminada;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TiposConsultaMetadata.ColumnNames.TcsEstado, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TiposConsultaMetadata.PropertyNames.TcsEstado;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TiposConsultaMetadata.ColumnNames.TcsCodigoAdministracion, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TiposConsultaMetadata.PropertyNames.TcsCodigoAdministracion;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TiposConsultaMetadata.ColumnNames.TipoConsultaPadreId, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TiposConsultaMetadata.PropertyNames.TipoConsultaPadreId;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TiposConsultaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TcsCodPK = "tcs_CodPK";
			 public const string TcsNombre = "tcs_Nombre";
			 public const string TcsAlta = "tcs_Alta";
			 public const string TcsRef = "tcs_Ref";
			 public const string TcsConRetorno = "tcs_ConRetorno";
			 public const string TcsEliminada = "tcs_Eliminada";
			 public const string TcsEstado = "tcs_Estado";
			 public const string TcsCodigoAdministracion = "tcs_CodigoAdministracion";
			 public const string TipoConsultaPadreId = "TipoConsultaPadreId";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TcsCodPK = "TcsCodPK";
			 public const string TcsNombre = "TcsNombre";
			 public const string TcsAlta = "TcsAlta";
			 public const string TcsRef = "TcsRef";
			 public const string TcsConRetorno = "TcsConRetorno";
			 public const string TcsEliminada = "TcsEliminada";
			 public const string TcsEstado = "TcsEstado";
			 public const string TcsCodigoAdministracion = "TcsCodigoAdministracion";
			 public const string TipoConsultaPadreId = "TipoConsultaPadreId";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TiposConsultaMetadata))
			{
				if(TiposConsultaMetadata.mapDelegates == null)
				{
					TiposConsultaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TiposConsultaMetadata.meta == null)
				{
					TiposConsultaMetadata.meta = new TiposConsultaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("TcsCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TcsNombre", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("TcsAlta", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("TcsRef", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TcsConRetorno", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("TcsEliminada", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("TcsEstado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TcsCodigoAdministracion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("TipoConsultaPadreId", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "TiposConsulta";
				meta.Destination = "TiposConsulta";
				
				meta.spInsert = "proc_TiposConsultaInsert";				
				meta.spUpdate = "proc_TiposConsultaUpdate";		
				meta.spDelete = "proc_TiposConsultaDelete";
				meta.spLoadAll = "proc_TiposConsultaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TiposConsultaLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TiposConsultaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
