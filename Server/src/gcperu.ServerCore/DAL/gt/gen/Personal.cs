
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// Encapsulates the 'Personal' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("Personal")]
	public partial class Personal : esPersonal
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new Personal();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal perCodPK)
		{
			var obj = new Personal();
			obj.PerCodPK = perCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal perCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new Personal();
			obj.PerCodPK = perCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal PERCODPK)
        {
            try
            {
                var e = new Personal();
                return (e.LoadByPrimaryKey(PERCODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static Personal Get(System.Decimal PERCODPK)
        {
            try
            {
                var e = new Personal();
                return (e.LoadByPrimaryKey(PERCODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public Personal(System.Decimal PERCODPK)
        {
            this.LoadByPrimaryKey(PERCODPK);
        }
		
		public Personal()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("PersonalCol")]
	public partial class PersonalCol : esPersonalCol, IEnumerable<Personal>
	{
	
		public PersonalCol()
		{
		}

	
	
		public Personal FindByPrimaryKey(System.Decimal perCodPK)
		{
			return this.SingleOrDefault(e => e.PerCodPK == perCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(Personal))]
		public class PersonalColWCFPacket : esCollectionWCFPacket<PersonalCol>
		{
			public static implicit operator PersonalCol(PersonalColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator PersonalColWCFPacket(PersonalCol collection)
			{
				return new PersonalColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class PersonalQ : esPersonalQ
	{
		public PersonalQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public PersonalQ()
		{
		}

		override protected string GetQueryName()
		{
			return "PersonalQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new PersonalQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(PersonalQ query)
		{
			return PersonalQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator PersonalQ(string query)
		{
			return (PersonalQ)PersonalQ.SerializeHelper.FromXml(query, typeof(PersonalQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esPersonal : EntityBase
	{
		public esPersonal()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal perCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(perCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(perCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal perCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(perCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(perCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal perCodPK)
		{
			PersonalQ query = new PersonalQ();
			query.Where(query.PerCodPK == perCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal perCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("PerCodPK", perCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to Personal.per_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PerCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.PerCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.PerCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Codigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerCodigo
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerCodigo);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerCodigo, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.are_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.AreCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.AreCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.AreCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Nombre
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerNombre
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerNombre);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerNombre, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerNombre);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Apellidos
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerApellidos
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerApellidos);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerApellidos, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerApellidos);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Direccion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerDireccion
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerDireccion);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerDireccion, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerDireccion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_CP
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerCP
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerCP);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerCP, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerCP);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.lcd_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? LcdCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.LcdCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.LcdCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.LcdCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.pvc_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PvcCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.PvcCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.PvcCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PvcCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_NIF
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerNIF
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerNIF);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerNIF, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerNIF);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_NSS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerNSS
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerNSS);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerNSS, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerNSS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_FechaNacimiento
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerFechaNacimiento
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerFechaNacimiento);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerFechaNacimiento, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerFechaNacimiento);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Casado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? PerCasado
		{
			get
			{
				return base.GetSystemByte(PersonalMetadata.ColumnNames.PerCasado);
			}
			
			set
			{
				if(base.SetSystemByte(PersonalMetadata.ColumnNames.PerCasado, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerCasado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Sexo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? PerSexo
		{
			get
			{
				return base.GetSystemByte(PersonalMetadata.ColumnNames.PerSexo);
			}
			
			set
			{
				if(base.SetSystemByte(PersonalMetadata.ColumnNames.PerSexo, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerSexo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_NumHijos
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? PerNumHijos
		{
			get
			{
				return base.GetSystemByte(PersonalMetadata.ColumnNames.PerNumHijos);
			}
			
			set
			{
				if(base.SetSystemByte(PersonalMetadata.ColumnNames.PerNumHijos, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerNumHijos);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Conyuge
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerConyuge
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerConyuge);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerConyuge, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerConyuge);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_CCC
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerCCC
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerCCC);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerCCC, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerCCC);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Socio
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? PerSocio
		{
			get
			{
				return base.GetSystemByte(PersonalMetadata.ColumnNames.PerSocio);
			}
			
			set
			{
				if(base.SetSystemByte(PersonalMetadata.ColumnNames.PerSocio, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerSocio);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.tra_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TraCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.TraCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.TraCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.TraCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.cen_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CenCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.CenCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.CenCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.CenCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.zon_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ZonCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.ZonCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.ZonCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.ZonCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_FechaUltContrato
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerFechaUltContrato
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerFechaUltContrato);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerFechaUltContrato, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerFechaUltContrato);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_TipoCarnet
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerTipoCarnet
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerTipoCarnet);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerTipoCarnet, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerTipoCarnet);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_FechaExpB1
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerFechaExpB1
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerFechaExpB1);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerFechaExpB1, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerFechaExpB1);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_FechaCadB2
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerFechaCadB2
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerFechaCadB2);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerFechaCadB2, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerFechaCadB2);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.veh_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.VehCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.VehCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.VehCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Activo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? PerActivo
		{
			get
			{
				return base.GetSystemByte(PersonalMetadata.ColumnNames.PerActivo);
			}
			
			set
			{
				if(base.SetSystemByte(PersonalMetadata.ColumnNames.PerActivo, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerActivo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_UTE
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? PerUTE
		{
			get
			{
				return base.GetSystemByte(PersonalMetadata.ColumnNames.PerUTE);
			}
			
			set
			{
				if(base.SetSystemByte(PersonalMetadata.ColumnNames.PerUTE, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerUTE);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_NifConyuge
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerNifConyuge
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerNifConyuge);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerNifConyuge, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerNifConyuge);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_AnosHijo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerAnosHijo
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerAnosHijo);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerAnosHijo, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerAnosHijo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_TrabajaConyuge
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? PerTrabajaConyuge
		{
			get
			{
				return base.GetSystemByte(PersonalMetadata.ColumnNames.PerTrabajaConyuge);
			}
			
			set
			{
				if(base.SetSystemByte(PersonalMetadata.ColumnNames.PerTrabajaConyuge, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerTrabajaConyuge);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_DNIValidezH
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerDNIValidezH
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerDNIValidezH);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerDNIValidezH, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerDNIValidezH);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.emp_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.EmpCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.EmpCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.EmpCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Alta
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerAlta
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerAlta);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerAlta, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerAlta);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Baja
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerBaja
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerBaja);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerBaja, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerBaja);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Ref
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PerRef
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.PerRef);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.PerRef, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerRef);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.ttr_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TtrCodPK
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.TtrCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.TtrCodPK, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.TtrCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_EstadoDocumentacion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? PerEstadoDocumentacion
		{
			get
			{
				return base.GetSystemInt16(PersonalMetadata.ColumnNames.PerEstadoDocumentacion);
			}
			
			set
			{
				if(base.SetSystemInt16(PersonalMetadata.ColumnNames.PerEstadoDocumentacion, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerEstadoDocumentacion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_EstadoDocumentacionPrc
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? PerEstadoDocumentacionPrc
		{
			get
			{
				return base.GetSystemInt16(PersonalMetadata.ColumnNames.PerEstadoDocumentacionPrc);
			}
			
			set
			{
				if(base.SetSystemInt16(PersonalMetadata.ColumnNames.PerEstadoDocumentacionPrc, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerEstadoDocumentacionPrc);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_EstadoCaduca
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? PerEstadoCaduca
		{
			get
			{
				return base.GetSystemInt16(PersonalMetadata.ColumnNames.PerEstadoCaduca);
			}
			
			set
			{
				if(base.SetSystemInt16(PersonalMetadata.ColumnNames.PerEstadoCaduca, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerEstadoCaduca);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Documentar
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? PerDocumentar
		{
			get
			{
				return base.GetSystemBoolean(PersonalMetadata.ColumnNames.PerDocumentar);
			}
			
			set
			{
				if(base.SetSystemBoolean(PersonalMetadata.ColumnNames.PerDocumentar, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerDocumentar);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_GestionGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? PerGestionGPS
		{
			get
			{
				return base.GetSystemBoolean(PersonalMetadata.ColumnNames.PerGestionGPS);
			}
			
			set
			{
				if(base.SetSystemBoolean(PersonalMetadata.ColumnNames.PerGestionGPS, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerGestionGPS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_IDConductor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PerIDConductor
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.PerIDConductor);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.PerIDConductor, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerIDConductor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Estado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PerEstado
		{
			get
			{
				return base.GetSystemDecimal(PersonalMetadata.ColumnNames.PerEstado);
			}
			
			set
			{
				if(base.SetSystemDecimal(PersonalMetadata.ColumnNames.PerEstado, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerEstado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_EMail
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerEMail
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerEMail);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerEMail, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerEMail);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_NIFExpedicion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerNIFExpedicion
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerNIFExpedicion);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerNIFExpedicion, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerNIFExpedicion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_NIFVencimiento
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerNIFVencimiento
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerNIFVencimiento);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerNIFVencimiento, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerNIFVencimiento);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_NombreCompleto
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerNombreCompleto
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerNombreCompleto);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerNombreCompleto, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerNombreCompleto);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Antiguedad
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PerAntiguedad
		{
			get
			{
				return base.GetSystemDateTime(PersonalMetadata.ColumnNames.PerAntiguedad);
			}
			
			set
			{
				if(base.SetSystemDateTime(PersonalMetadata.ColumnNames.PerAntiguedad, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerAntiguedad);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_EnviarFax
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? PerEnviarFax
		{
			get
			{
				return base.GetSystemBoolean(PersonalMetadata.ColumnNames.PerEnviarFax);
			}
			
			set
			{
				if(base.SetSystemBoolean(PersonalMetadata.ColumnNames.PerEnviarFax, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerEnviarFax);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Personal.per_Telefono
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PerTelefono
		{
			get
			{
				return base.GetSystemString(PersonalMetadata.ColumnNames.PerTelefono);
			}
			
			set
			{
				if(base.SetSystemString(PersonalMetadata.ColumnNames.PerTelefono, value))
				{
					OnPropertyChanged(PersonalMetadata.PropertyNames.PerTelefono);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "PerCodPK": this.str().PerCodPK = (string)value; break;							
						case "PerCodigo": this.str().PerCodigo = (string)value; break;							
						case "AreCodPK": this.str().AreCodPK = (string)value; break;							
						case "PerNombre": this.str().PerNombre = (string)value; break;							
						case "PerApellidos": this.str().PerApellidos = (string)value; break;							
						case "PerDireccion": this.str().PerDireccion = (string)value; break;							
						case "PerCP": this.str().PerCP = (string)value; break;							
						case "LcdCodPK": this.str().LcdCodPK = (string)value; break;							
						case "PvcCodPK": this.str().PvcCodPK = (string)value; break;							
						case "PerNIF": this.str().PerNIF = (string)value; break;							
						case "PerNSS": this.str().PerNSS = (string)value; break;							
						case "PerFechaNacimiento": this.str().PerFechaNacimiento = (string)value; break;							
						case "PerCasado": this.str().PerCasado = (string)value; break;							
						case "PerSexo": this.str().PerSexo = (string)value; break;							
						case "PerNumHijos": this.str().PerNumHijos = (string)value; break;							
						case "PerConyuge": this.str().PerConyuge = (string)value; break;							
						case "PerCCC": this.str().PerCCC = (string)value; break;							
						case "PerSocio": this.str().PerSocio = (string)value; break;							
						case "TraCodPK": this.str().TraCodPK = (string)value; break;							
						case "CenCodPK": this.str().CenCodPK = (string)value; break;							
						case "ZonCodPK": this.str().ZonCodPK = (string)value; break;							
						case "PerFechaUltContrato": this.str().PerFechaUltContrato = (string)value; break;							
						case "PerTipoCarnet": this.str().PerTipoCarnet = (string)value; break;							
						case "PerFechaExpB1": this.str().PerFechaExpB1 = (string)value; break;							
						case "PerFechaCadB2": this.str().PerFechaCadB2 = (string)value; break;							
						case "VehCodPK": this.str().VehCodPK = (string)value; break;							
						case "PerActivo": this.str().PerActivo = (string)value; break;							
						case "PerUTE": this.str().PerUTE = (string)value; break;							
						case "PerNifConyuge": this.str().PerNifConyuge = (string)value; break;							
						case "PerAnosHijo": this.str().PerAnosHijo = (string)value; break;							
						case "PerTrabajaConyuge": this.str().PerTrabajaConyuge = (string)value; break;							
						case "PerDNIValidezH": this.str().PerDNIValidezH = (string)value; break;							
						case "EmpCodPK": this.str().EmpCodPK = (string)value; break;							
						case "PerAlta": this.str().PerAlta = (string)value; break;							
						case "PerBaja": this.str().PerBaja = (string)value; break;							
						case "PerRef": this.str().PerRef = (string)value; break;							
						case "TtrCodPK": this.str().TtrCodPK = (string)value; break;							
						case "PerEstadoDocumentacion": this.str().PerEstadoDocumentacion = (string)value; break;							
						case "PerEstadoDocumentacionPrc": this.str().PerEstadoDocumentacionPrc = (string)value; break;							
						case "PerEstadoCaduca": this.str().PerEstadoCaduca = (string)value; break;							
						case "PerDocumentar": this.str().PerDocumentar = (string)value; break;							
						case "PerGestionGPS": this.str().PerGestionGPS = (string)value; break;							
						case "PerIDConductor": this.str().PerIDConductor = (string)value; break;							
						case "PerEstado": this.str().PerEstado = (string)value; break;							
						case "PerEMail": this.str().PerEMail = (string)value; break;							
						case "PerNIFExpedicion": this.str().PerNIFExpedicion = (string)value; break;							
						case "PerNIFVencimiento": this.str().PerNIFVencimiento = (string)value; break;							
						case "PerNombreCompleto": this.str().PerNombreCompleto = (string)value; break;							
						case "PerAntiguedad": this.str().PerAntiguedad = (string)value; break;							
						case "PerEnviarFax": this.str().PerEnviarFax = (string)value; break;							
						case "PerTelefono": this.str().PerTelefono = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "PerCodPK":
						
							if (value == null || value is System.Decimal)
								this.PerCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerCodPK);
							break;
						
						case "AreCodPK":
						
							if (value == null || value is System.Decimal)
								this.AreCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.AreCodPK);
							break;
						
						case "LcdCodPK":
						
							if (value == null || value is System.Decimal)
								this.LcdCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.LcdCodPK);
							break;
						
						case "PvcCodPK":
						
							if (value == null || value is System.Decimal)
								this.PvcCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PvcCodPK);
							break;
						
						case "PerFechaNacimiento":
						
							if (value == null || value is System.DateTime)
								this.PerFechaNacimiento = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerFechaNacimiento);
							break;
						
						case "PerCasado":
						
							if (value == null || value is System.Byte)
								this.PerCasado = (System.Byte?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerCasado);
							break;
						
						case "PerSexo":
						
							if (value == null || value is System.Byte)
								this.PerSexo = (System.Byte?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerSexo);
							break;
						
						case "PerNumHijos":
						
							if (value == null || value is System.Byte)
								this.PerNumHijos = (System.Byte?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerNumHijos);
							break;
						
						case "PerSocio":
						
							if (value == null || value is System.Byte)
								this.PerSocio = (System.Byte?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerSocio);
							break;
						
						case "TraCodPK":
						
							if (value == null || value is System.Decimal)
								this.TraCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.TraCodPK);
							break;
						
						case "CenCodPK":
						
							if (value == null || value is System.Decimal)
								this.CenCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.CenCodPK);
							break;
						
						case "ZonCodPK":
						
							if (value == null || value is System.Decimal)
								this.ZonCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.ZonCodPK);
							break;
						
						case "PerFechaUltContrato":
						
							if (value == null || value is System.DateTime)
								this.PerFechaUltContrato = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerFechaUltContrato);
							break;
						
						case "PerFechaExpB1":
						
							if (value == null || value is System.DateTime)
								this.PerFechaExpB1 = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerFechaExpB1);
							break;
						
						case "PerFechaCadB2":
						
							if (value == null || value is System.DateTime)
								this.PerFechaCadB2 = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerFechaCadB2);
							break;
						
						case "VehCodPK":
						
							if (value == null || value is System.Decimal)
								this.VehCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.VehCodPK);
							break;
						
						case "PerActivo":
						
							if (value == null || value is System.Byte)
								this.PerActivo = (System.Byte?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerActivo);
							break;
						
						case "PerUTE":
						
							if (value == null || value is System.Byte)
								this.PerUTE = (System.Byte?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerUTE);
							break;
						
						case "PerTrabajaConyuge":
						
							if (value == null || value is System.Byte)
								this.PerTrabajaConyuge = (System.Byte?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerTrabajaConyuge);
							break;
						
						case "PerDNIValidezH":
						
							if (value == null || value is System.DateTime)
								this.PerDNIValidezH = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerDNIValidezH);
							break;
						
						case "EmpCodPK":
						
							if (value == null || value is System.Decimal)
								this.EmpCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.EmpCodPK);
							break;
						
						case "PerAlta":
						
							if (value == null || value is System.DateTime)
								this.PerAlta = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerAlta);
							break;
						
						case "PerBaja":
						
							if (value == null || value is System.DateTime)
								this.PerBaja = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerBaja);
							break;
						
						case "PerRef":
						
							if (value == null || value is System.Decimal)
								this.PerRef = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerRef);
							break;
						
						case "TtrCodPK":
						
							if (value == null || value is System.Decimal)
								this.TtrCodPK = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.TtrCodPK);
							break;
						
						case "PerEstadoDocumentacion":
						
							if (value == null || value is System.Int16)
								this.PerEstadoDocumentacion = (System.Int16?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerEstadoDocumentacion);
							break;
						
						case "PerEstadoDocumentacionPrc":
						
							if (value == null || value is System.Int16)
								this.PerEstadoDocumentacionPrc = (System.Int16?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerEstadoDocumentacionPrc);
							break;
						
						case "PerEstadoCaduca":
						
							if (value == null || value is System.Int16)
								this.PerEstadoCaduca = (System.Int16?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerEstadoCaduca);
							break;
						
						case "PerDocumentar":
						
							if (value == null || value is System.Boolean)
								this.PerDocumentar = (System.Boolean?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerDocumentar);
							break;
						
						case "PerGestionGPS":
						
							if (value == null || value is System.Boolean)
								this.PerGestionGPS = (System.Boolean?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerGestionGPS);
							break;
						
						case "PerIDConductor":
						
							if (value == null || value is System.Decimal)
								this.PerIDConductor = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerIDConductor);
							break;
						
						case "PerEstado":
						
							if (value == null || value is System.Decimal)
								this.PerEstado = (System.Decimal?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerEstado);
							break;
						
						case "PerNIFExpedicion":
						
							if (value == null || value is System.DateTime)
								this.PerNIFExpedicion = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerNIFExpedicion);
							break;
						
						case "PerNIFVencimiento":
						
							if (value == null || value is System.DateTime)
								this.PerNIFVencimiento = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerNIFVencimiento);
							break;
						
						case "PerAntiguedad":
						
							if (value == null || value is System.DateTime)
								this.PerAntiguedad = (System.DateTime?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerAntiguedad);
							break;
						
						case "PerEnviarFax":
						
							if (value == null || value is System.Boolean)
								this.PerEnviarFax = (System.Boolean?)value;
								OnPropertyChanged(PersonalMetadata.PropertyNames.PerEnviarFax);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esPersonal entity)
			{
				this.entity = entity;
			}
			
	
			public System.String PerCodPK
			{
				get
				{
					System.Decimal? data = entity.PerCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerCodPK = null;
					else entity.PerCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PerCodigo
			{
				get
				{
					System.String data = entity.PerCodigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerCodigo = null;
					else entity.PerCodigo = Convert.ToString(value);
				}
			}
				
			public System.String AreCodPK
			{
				get
				{
					System.Decimal? data = entity.AreCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreCodPK = null;
					else entity.AreCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PerNombre
			{
				get
				{
					System.String data = entity.PerNombre;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerNombre = null;
					else entity.PerNombre = Convert.ToString(value);
				}
			}
				
			public System.String PerApellidos
			{
				get
				{
					System.String data = entity.PerApellidos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerApellidos = null;
					else entity.PerApellidos = Convert.ToString(value);
				}
			}
				
			public System.String PerDireccion
			{
				get
				{
					System.String data = entity.PerDireccion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerDireccion = null;
					else entity.PerDireccion = Convert.ToString(value);
				}
			}
				
			public System.String PerCP
			{
				get
				{
					System.String data = entity.PerCP;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerCP = null;
					else entity.PerCP = Convert.ToString(value);
				}
			}
				
			public System.String LcdCodPK
			{
				get
				{
					System.Decimal? data = entity.LcdCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LcdCodPK = null;
					else entity.LcdCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PvcCodPK
			{
				get
				{
					System.Decimal? data = entity.PvcCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PvcCodPK = null;
					else entity.PvcCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PerNIF
			{
				get
				{
					System.String data = entity.PerNIF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerNIF = null;
					else entity.PerNIF = Convert.ToString(value);
				}
			}
				
			public System.String PerNSS
			{
				get
				{
					System.String data = entity.PerNSS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerNSS = null;
					else entity.PerNSS = Convert.ToString(value);
				}
			}
				
			public System.String PerFechaNacimiento
			{
				get
				{
					System.DateTime? data = entity.PerFechaNacimiento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerFechaNacimiento = null;
					else entity.PerFechaNacimiento = Convert.ToDateTime(value);
				}
			}
				
			public System.String PerCasado
			{
				get
				{
					System.Byte? data = entity.PerCasado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerCasado = null;
					else entity.PerCasado = Convert.ToByte(value);
				}
			}
				
			public System.String PerSexo
			{
				get
				{
					System.Byte? data = entity.PerSexo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerSexo = null;
					else entity.PerSexo = Convert.ToByte(value);
				}
			}
				
			public System.String PerNumHijos
			{
				get
				{
					System.Byte? data = entity.PerNumHijos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerNumHijos = null;
					else entity.PerNumHijos = Convert.ToByte(value);
				}
			}
				
			public System.String PerConyuge
			{
				get
				{
					System.String data = entity.PerConyuge;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerConyuge = null;
					else entity.PerConyuge = Convert.ToString(value);
				}
			}
				
			public System.String PerCCC
			{
				get
				{
					System.String data = entity.PerCCC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerCCC = null;
					else entity.PerCCC = Convert.ToString(value);
				}
			}
				
			public System.String PerSocio
			{
				get
				{
					System.Byte? data = entity.PerSocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerSocio = null;
					else entity.PerSocio = Convert.ToByte(value);
				}
			}
				
			public System.String TraCodPK
			{
				get
				{
					System.Decimal? data = entity.TraCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TraCodPK = null;
					else entity.TraCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String CenCodPK
			{
				get
				{
					System.Decimal? data = entity.CenCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CenCodPK = null;
					else entity.CenCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String ZonCodPK
			{
				get
				{
					System.Decimal? data = entity.ZonCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ZonCodPK = null;
					else entity.ZonCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PerFechaUltContrato
			{
				get
				{
					System.DateTime? data = entity.PerFechaUltContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerFechaUltContrato = null;
					else entity.PerFechaUltContrato = Convert.ToDateTime(value);
				}
			}
				
			public System.String PerTipoCarnet
			{
				get
				{
					System.String data = entity.PerTipoCarnet;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerTipoCarnet = null;
					else entity.PerTipoCarnet = Convert.ToString(value);
				}
			}
				
			public System.String PerFechaExpB1
			{
				get
				{
					System.DateTime? data = entity.PerFechaExpB1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerFechaExpB1 = null;
					else entity.PerFechaExpB1 = Convert.ToDateTime(value);
				}
			}
				
			public System.String PerFechaCadB2
			{
				get
				{
					System.DateTime? data = entity.PerFechaCadB2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerFechaCadB2 = null;
					else entity.PerFechaCadB2 = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehCodPK
			{
				get
				{
					System.Decimal? data = entity.VehCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehCodPK = null;
					else entity.VehCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PerActivo
			{
				get
				{
					System.Byte? data = entity.PerActivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerActivo = null;
					else entity.PerActivo = Convert.ToByte(value);
				}
			}
				
			public System.String PerUTE
			{
				get
				{
					System.Byte? data = entity.PerUTE;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerUTE = null;
					else entity.PerUTE = Convert.ToByte(value);
				}
			}
				
			public System.String PerNifConyuge
			{
				get
				{
					System.String data = entity.PerNifConyuge;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerNifConyuge = null;
					else entity.PerNifConyuge = Convert.ToString(value);
				}
			}
				
			public System.String PerAnosHijo
			{
				get
				{
					System.String data = entity.PerAnosHijo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerAnosHijo = null;
					else entity.PerAnosHijo = Convert.ToString(value);
				}
			}
				
			public System.String PerTrabajaConyuge
			{
				get
				{
					System.Byte? data = entity.PerTrabajaConyuge;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerTrabajaConyuge = null;
					else entity.PerTrabajaConyuge = Convert.ToByte(value);
				}
			}
				
			public System.String PerDNIValidezH
			{
				get
				{
					System.DateTime? data = entity.PerDNIValidezH;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerDNIValidezH = null;
					else entity.PerDNIValidezH = Convert.ToDateTime(value);
				}
			}
				
			public System.String EmpCodPK
			{
				get
				{
					System.Decimal? data = entity.EmpCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCodPK = null;
					else entity.EmpCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PerAlta
			{
				get
				{
					System.DateTime? data = entity.PerAlta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerAlta = null;
					else entity.PerAlta = Convert.ToDateTime(value);
				}
			}
				
			public System.String PerBaja
			{
				get
				{
					System.DateTime? data = entity.PerBaja;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerBaja = null;
					else entity.PerBaja = Convert.ToDateTime(value);
				}
			}
				
			public System.String PerRef
			{
				get
				{
					System.Decimal? data = entity.PerRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerRef = null;
					else entity.PerRef = Convert.ToDecimal(value);
				}
			}
				
			public System.String TtrCodPK
			{
				get
				{
					System.Decimal? data = entity.TtrCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TtrCodPK = null;
					else entity.TtrCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PerEstadoDocumentacion
			{
				get
				{
					System.Int16? data = entity.PerEstadoDocumentacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerEstadoDocumentacion = null;
					else entity.PerEstadoDocumentacion = Convert.ToInt16(value);
				}
			}
				
			public System.String PerEstadoDocumentacionPrc
			{
				get
				{
					System.Int16? data = entity.PerEstadoDocumentacionPrc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerEstadoDocumentacionPrc = null;
					else entity.PerEstadoDocumentacionPrc = Convert.ToInt16(value);
				}
			}
				
			public System.String PerEstadoCaduca
			{
				get
				{
					System.Int16? data = entity.PerEstadoCaduca;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerEstadoCaduca = null;
					else entity.PerEstadoCaduca = Convert.ToInt16(value);
				}
			}
				
			public System.String PerDocumentar
			{
				get
				{
					System.Boolean? data = entity.PerDocumentar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerDocumentar = null;
					else entity.PerDocumentar = Convert.ToBoolean(value);
				}
			}
				
			public System.String PerGestionGPS
			{
				get
				{
					System.Boolean? data = entity.PerGestionGPS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerGestionGPS = null;
					else entity.PerGestionGPS = Convert.ToBoolean(value);
				}
			}
				
			public System.String PerIDConductor
			{
				get
				{
					System.Decimal? data = entity.PerIDConductor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerIDConductor = null;
					else entity.PerIDConductor = Convert.ToDecimal(value);
				}
			}
				
			public System.String PerEstado
			{
				get
				{
					System.Decimal? data = entity.PerEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerEstado = null;
					else entity.PerEstado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PerEMail
			{
				get
				{
					System.String data = entity.PerEMail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerEMail = null;
					else entity.PerEMail = Convert.ToString(value);
				}
			}
				
			public System.String PerNIFExpedicion
			{
				get
				{
					System.DateTime? data = entity.PerNIFExpedicion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerNIFExpedicion = null;
					else entity.PerNIFExpedicion = Convert.ToDateTime(value);
				}
			}
				
			public System.String PerNIFVencimiento
			{
				get
				{
					System.DateTime? data = entity.PerNIFVencimiento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerNIFVencimiento = null;
					else entity.PerNIFVencimiento = Convert.ToDateTime(value);
				}
			}
				
			public System.String PerNombreCompleto
			{
				get
				{
					System.String data = entity.PerNombreCompleto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerNombreCompleto = null;
					else entity.PerNombreCompleto = Convert.ToString(value);
				}
			}
				
			public System.String PerAntiguedad
			{
				get
				{
					System.DateTime? data = entity.PerAntiguedad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerAntiguedad = null;
					else entity.PerAntiguedad = Convert.ToDateTime(value);
				}
			}
				
			public System.String PerEnviarFax
			{
				get
				{
					System.Boolean? data = entity.PerEnviarFax;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerEnviarFax = null;
					else entity.PerEnviarFax = Convert.ToBoolean(value);
				}
			}
				
			public System.String PerTelefono
			{
				get
				{
					System.String data = entity.PerTelefono;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerTelefono = null;
					else entity.PerTelefono = Convert.ToString(value);
				}
			}
			

			private esPersonal entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return PersonalMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public PersonalQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PersonalQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(PersonalQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(PersonalQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private PersonalQ query;		
	}



	[Serializable]
	abstract public partial class esPersonalCol : CollectionBase<Personal>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PersonalMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "PersonalCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public PersonalQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PersonalQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(PersonalQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PersonalQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(PersonalQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((PersonalQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private PersonalQ query;
	}



	[Serializable]
	abstract public partial class esPersonalQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return PersonalMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(PersonalMetadata.ColumnNames.PerCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.PerCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerCodigo,new esQueryItem(this, PersonalMetadata.ColumnNames.PerCodigo, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.AreCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.AreCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerNombre,new esQueryItem(this, PersonalMetadata.ColumnNames.PerNombre, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerApellidos,new esQueryItem(this, PersonalMetadata.ColumnNames.PerApellidos, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerDireccion,new esQueryItem(this, PersonalMetadata.ColumnNames.PerDireccion, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerCP,new esQueryItem(this, PersonalMetadata.ColumnNames.PerCP, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.LcdCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.LcdCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PvcCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.PvcCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerNIF,new esQueryItem(this, PersonalMetadata.ColumnNames.PerNIF, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerNSS,new esQueryItem(this, PersonalMetadata.ColumnNames.PerNSS, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerFechaNacimiento,new esQueryItem(this, PersonalMetadata.ColumnNames.PerFechaNacimiento, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerCasado,new esQueryItem(this, PersonalMetadata.ColumnNames.PerCasado, esSystemType.Byte));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerSexo,new esQueryItem(this, PersonalMetadata.ColumnNames.PerSexo, esSystemType.Byte));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerNumHijos,new esQueryItem(this, PersonalMetadata.ColumnNames.PerNumHijos, esSystemType.Byte));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerConyuge,new esQueryItem(this, PersonalMetadata.ColumnNames.PerConyuge, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerCCC,new esQueryItem(this, PersonalMetadata.ColumnNames.PerCCC, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerSocio,new esQueryItem(this, PersonalMetadata.ColumnNames.PerSocio, esSystemType.Byte));
			_queryItems.Add(PersonalMetadata.ColumnNames.TraCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.TraCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.CenCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.CenCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.ZonCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.ZonCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerFechaUltContrato,new esQueryItem(this, PersonalMetadata.ColumnNames.PerFechaUltContrato, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerTipoCarnet,new esQueryItem(this, PersonalMetadata.ColumnNames.PerTipoCarnet, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerFechaExpB1,new esQueryItem(this, PersonalMetadata.ColumnNames.PerFechaExpB1, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerFechaCadB2,new esQueryItem(this, PersonalMetadata.ColumnNames.PerFechaCadB2, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.VehCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.VehCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerActivo,new esQueryItem(this, PersonalMetadata.ColumnNames.PerActivo, esSystemType.Byte));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerUTE,new esQueryItem(this, PersonalMetadata.ColumnNames.PerUTE, esSystemType.Byte));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerNifConyuge,new esQueryItem(this, PersonalMetadata.ColumnNames.PerNifConyuge, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerAnosHijo,new esQueryItem(this, PersonalMetadata.ColumnNames.PerAnosHijo, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerTrabajaConyuge,new esQueryItem(this, PersonalMetadata.ColumnNames.PerTrabajaConyuge, esSystemType.Byte));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerDNIValidezH,new esQueryItem(this, PersonalMetadata.ColumnNames.PerDNIValidezH, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.EmpCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerAlta,new esQueryItem(this, PersonalMetadata.ColumnNames.PerAlta, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerBaja,new esQueryItem(this, PersonalMetadata.ColumnNames.PerBaja, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerRef,new esQueryItem(this, PersonalMetadata.ColumnNames.PerRef, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.TtrCodPK,new esQueryItem(this, PersonalMetadata.ColumnNames.TtrCodPK, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerEstadoDocumentacion,new esQueryItem(this, PersonalMetadata.ColumnNames.PerEstadoDocumentacion, esSystemType.Int16));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerEstadoDocumentacionPrc,new esQueryItem(this, PersonalMetadata.ColumnNames.PerEstadoDocumentacionPrc, esSystemType.Int16));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerEstadoCaduca,new esQueryItem(this, PersonalMetadata.ColumnNames.PerEstadoCaduca, esSystemType.Int16));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerDocumentar,new esQueryItem(this, PersonalMetadata.ColumnNames.PerDocumentar, esSystemType.Boolean));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerGestionGPS,new esQueryItem(this, PersonalMetadata.ColumnNames.PerGestionGPS, esSystemType.Boolean));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerIDConductor,new esQueryItem(this, PersonalMetadata.ColumnNames.PerIDConductor, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerEstado,new esQueryItem(this, PersonalMetadata.ColumnNames.PerEstado, esSystemType.Decimal));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerEMail,new esQueryItem(this, PersonalMetadata.ColumnNames.PerEMail, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerNIFExpedicion,new esQueryItem(this, PersonalMetadata.ColumnNames.PerNIFExpedicion, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerNIFVencimiento,new esQueryItem(this, PersonalMetadata.ColumnNames.PerNIFVencimiento, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerNombreCompleto,new esQueryItem(this, PersonalMetadata.ColumnNames.PerNombreCompleto, esSystemType.String));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerAntiguedad,new esQueryItem(this, PersonalMetadata.ColumnNames.PerAntiguedad, esSystemType.DateTime));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerEnviarFax,new esQueryItem(this, PersonalMetadata.ColumnNames.PerEnviarFax, esSystemType.Boolean));
			_queryItems.Add(PersonalMetadata.ColumnNames.PerTelefono,new esQueryItem(this, PersonalMetadata.ColumnNames.PerTelefono, esSystemType.String));
			
	    }
	
	    #region esQueryItems

		public esQueryItem PerCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PerCodigo
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerCodigo, esSystemType.String); }
		} 
		
		public esQueryItem AreCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.AreCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PerNombre
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerNombre, esSystemType.String); }
		} 
		
		public esQueryItem PerApellidos
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerApellidos, esSystemType.String); }
		} 
		
		public esQueryItem PerDireccion
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerDireccion, esSystemType.String); }
		} 
		
		public esQueryItem PerCP
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerCP, esSystemType.String); }
		} 
		
		public esQueryItem LcdCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.LcdCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PvcCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PvcCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PerNIF
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerNIF, esSystemType.String); }
		} 
		
		public esQueryItem PerNSS
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerNSS, esSystemType.String); }
		} 
		
		public esQueryItem PerFechaNacimiento
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerFechaNacimiento, esSystemType.DateTime); }
		} 
		
		public esQueryItem PerCasado
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerCasado, esSystemType.Byte); }
		} 
		
		public esQueryItem PerSexo
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerSexo, esSystemType.Byte); }
		} 
		
		public esQueryItem PerNumHijos
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerNumHijos, esSystemType.Byte); }
		} 
		
		public esQueryItem PerConyuge
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerConyuge, esSystemType.String); }
		} 
		
		public esQueryItem PerCCC
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerCCC, esSystemType.String); }
		} 
		
		public esQueryItem PerSocio
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerSocio, esSystemType.Byte); }
		} 
		
		public esQueryItem TraCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.TraCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem CenCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.CenCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem ZonCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.ZonCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PerFechaUltContrato
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerFechaUltContrato, esSystemType.DateTime); }
		} 
		
		public esQueryItem PerTipoCarnet
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerTipoCarnet, esSystemType.String); }
		} 
		
		public esQueryItem PerFechaExpB1
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerFechaExpB1, esSystemType.DateTime); }
		} 
		
		public esQueryItem PerFechaCadB2
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerFechaCadB2, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.VehCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PerActivo
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerActivo, esSystemType.Byte); }
		} 
		
		public esQueryItem PerUTE
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerUTE, esSystemType.Byte); }
		} 
		
		public esQueryItem PerNifConyuge
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerNifConyuge, esSystemType.String); }
		} 
		
		public esQueryItem PerAnosHijo
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerAnosHijo, esSystemType.String); }
		} 
		
		public esQueryItem PerTrabajaConyuge
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerTrabajaConyuge, esSystemType.Byte); }
		} 
		
		public esQueryItem PerDNIValidezH
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerDNIValidezH, esSystemType.DateTime); }
		} 
		
		public esQueryItem EmpCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PerAlta
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerAlta, esSystemType.DateTime); }
		} 
		
		public esQueryItem PerBaja
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerBaja, esSystemType.DateTime); }
		} 
		
		public esQueryItem PerRef
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerRef, esSystemType.Decimal); }
		} 
		
		public esQueryItem TtrCodPK
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.TtrCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PerEstadoDocumentacion
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerEstadoDocumentacion, esSystemType.Int16); }
		} 
		
		public esQueryItem PerEstadoDocumentacionPrc
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerEstadoDocumentacionPrc, esSystemType.Int16); }
		} 
		
		public esQueryItem PerEstadoCaduca
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerEstadoCaduca, esSystemType.Int16); }
		} 
		
		public esQueryItem PerDocumentar
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerDocumentar, esSystemType.Boolean); }
		} 
		
		public esQueryItem PerGestionGPS
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerGestionGPS, esSystemType.Boolean); }
		} 
		
		public esQueryItem PerIDConductor
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerIDConductor, esSystemType.Decimal); }
		} 
		
		public esQueryItem PerEstado
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerEstado, esSystemType.Decimal); }
		} 
		
		public esQueryItem PerEMail
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerEMail, esSystemType.String); }
		} 
		
		public esQueryItem PerNIFExpedicion
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerNIFExpedicion, esSystemType.DateTime); }
		} 
		
		public esQueryItem PerNIFVencimiento
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerNIFVencimiento, esSystemType.DateTime); }
		} 
		
		public esQueryItem PerNombreCompleto
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerNombreCompleto, esSystemType.String); }
		} 
		
		public esQueryItem PerAntiguedad
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerAntiguedad, esSystemType.DateTime); }
		} 
		
		public esQueryItem PerEnviarFax
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerEnviarFax, esSystemType.Boolean); }
		} 
		
		public esQueryItem PerTelefono
		{
			get { return new esQueryItem(this, PersonalMetadata.ColumnNames.PerTelefono, esSystemType.String); }
		} 
		
		#endregion
		
	}


	
	public partial class Personal : esPersonal
	{

		
		
	}
	



	[Serializable]
	public partial class PersonalMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PersonalMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.PerCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerCodigo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerCodigo;
			c.CharacterMaxLength = 10;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.AreCodPK, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.AreCodPK;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerNombre, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerNombre;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerApellidos, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerApellidos;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerDireccion, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerDireccion;
			c.CharacterMaxLength = 40;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerCP, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerCP;
			c.CharacterMaxLength = 7;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.LcdCodPK, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.LcdCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PvcCodPK, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.PvcCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerNIF, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerNIF;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerNSS, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerNSS;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerFechaNacimiento, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerFechaNacimiento;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerCasado, 12, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PersonalMetadata.PropertyNames.PerCasado;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerSexo, 13, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PersonalMetadata.PropertyNames.PerSexo;
			c.NumericPrecision = 3;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerNumHijos, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PersonalMetadata.PropertyNames.PerNumHijos;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerConyuge, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerConyuge;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerCCC, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerCCC;
			c.CharacterMaxLength = 25;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerSocio, 17, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PersonalMetadata.PropertyNames.PerSocio;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.TraCodPK, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.TraCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.CenCodPK, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.CenCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.ZonCodPK, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.ZonCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerFechaUltContrato, 21, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerFechaUltContrato;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerTipoCarnet, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerTipoCarnet;
			c.CharacterMaxLength = 5;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerFechaExpB1, 23, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerFechaExpB1;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerFechaCadB2, 24, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerFechaCadB2;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.VehCodPK, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.VehCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerActivo, 26, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PersonalMetadata.PropertyNames.PerActivo;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerUTE, 27, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PersonalMetadata.PropertyNames.PerUTE;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerNifConyuge, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerNifConyuge;
			c.CharacterMaxLength = 10;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerAnosHijo, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerAnosHijo;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerTrabajaConyuge, 30, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PersonalMetadata.PropertyNames.PerTrabajaConyuge;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerDNIValidezH, 31, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerDNIValidezH;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.EmpCodPK, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.EmpCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerAlta, 33, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerAlta;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerBaja, 34, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerBaja;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerRef, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.PerRef;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.TtrCodPK, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.TtrCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerEstadoDocumentacion, 37, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PersonalMetadata.PropertyNames.PerEstadoDocumentacion;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((80))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerEstadoDocumentacionPrc, 38, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PersonalMetadata.PropertyNames.PerEstadoDocumentacionPrc;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerEstadoCaduca, 39, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PersonalMetadata.PropertyNames.PerEstadoCaduca;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((77))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerDocumentar, 40, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = PersonalMetadata.PropertyNames.PerDocumentar;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerGestionGPS, 41, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = PersonalMetadata.PropertyNames.PerGestionGPS;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerIDConductor, 42, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.PerIDConductor;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerEstado, 43, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PersonalMetadata.PropertyNames.PerEstado;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerEMail, 44, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerEMail;
			c.CharacterMaxLength = 150;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerNIFExpedicion, 45, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerNIFExpedicion;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerNIFVencimiento, 46, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerNIFVencimiento;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerNombreCompleto, 47, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerNombreCompleto;
			c.CharacterMaxLength = 200;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerAntiguedad, 48, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PersonalMetadata.PropertyNames.PerAntiguedad;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerEnviarFax, 49, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = PersonalMetadata.PropertyNames.PerEnviarFax;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(PersonalMetadata.ColumnNames.PerTelefono, 50, typeof(System.String), esSystemType.String);
			c.PropertyName = PersonalMetadata.PropertyNames.PerTelefono;
			c.CharacterMaxLength = 50;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public PersonalMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string PerCodPK = "per_CodPK";
			 public const string PerCodigo = "per_Codigo";
			 public const string AreCodPK = "are_CodPK";
			 public const string PerNombre = "per_Nombre";
			 public const string PerApellidos = "per_Apellidos";
			 public const string PerDireccion = "per_Direccion";
			 public const string PerCP = "per_CP";
			 public const string LcdCodPK = "lcd_CodPK";
			 public const string PvcCodPK = "pvc_CodPK";
			 public const string PerNIF = "per_NIF";
			 public const string PerNSS = "per_NSS";
			 public const string PerFechaNacimiento = "per_FechaNacimiento";
			 public const string PerCasado = "per_Casado";
			 public const string PerSexo = "per_Sexo";
			 public const string PerNumHijos = "per_NumHijos";
			 public const string PerConyuge = "per_Conyuge";
			 public const string PerCCC = "per_CCC";
			 public const string PerSocio = "per_Socio";
			 public const string TraCodPK = "tra_CodPK";
			 public const string CenCodPK = "cen_CodPK";
			 public const string ZonCodPK = "zon_CodPK";
			 public const string PerFechaUltContrato = "per_FechaUltContrato";
			 public const string PerTipoCarnet = "per_TipoCarnet";
			 public const string PerFechaExpB1 = "per_FechaExpB1";
			 public const string PerFechaCadB2 = "per_FechaCadB2";
			 public const string VehCodPK = "veh_CodPK";
			 public const string PerActivo = "per_Activo";
			 public const string PerUTE = "per_UTE";
			 public const string PerNifConyuge = "per_NifConyuge";
			 public const string PerAnosHijo = "per_AnosHijo";
			 public const string PerTrabajaConyuge = "per_TrabajaConyuge";
			 public const string PerDNIValidezH = "per_DNIValidezH";
			 public const string EmpCodPK = "emp_CodPK";
			 public const string PerAlta = "per_Alta";
			 public const string PerBaja = "per_Baja";
			 public const string PerRef = "per_Ref";
			 public const string TtrCodPK = "ttr_CodPK";
			 public const string PerEstadoDocumentacion = "per_EstadoDocumentacion";
			 public const string PerEstadoDocumentacionPrc = "per_EstadoDocumentacionPrc";
			 public const string PerEstadoCaduca = "per_EstadoCaduca";
			 public const string PerDocumentar = "per_Documentar";
			 public const string PerGestionGPS = "per_GestionGPS";
			 public const string PerIDConductor = "per_IDConductor";
			 public const string PerEstado = "per_Estado";
			 public const string PerEMail = "per_EMail";
			 public const string PerNIFExpedicion = "per_NIFExpedicion";
			 public const string PerNIFVencimiento = "per_NIFVencimiento";
			 public const string PerNombreCompleto = "per_NombreCompleto";
			 public const string PerAntiguedad = "per_Antiguedad";
			 public const string PerEnviarFax = "per_EnviarFax";
			 public const string PerTelefono = "per_Telefono";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string PerCodPK = "PerCodPK";
			 public const string PerCodigo = "PerCodigo";
			 public const string AreCodPK = "AreCodPK";
			 public const string PerNombre = "PerNombre";
			 public const string PerApellidos = "PerApellidos";
			 public const string PerDireccion = "PerDireccion";
			 public const string PerCP = "PerCP";
			 public const string LcdCodPK = "LcdCodPK";
			 public const string PvcCodPK = "PvcCodPK";
			 public const string PerNIF = "PerNIF";
			 public const string PerNSS = "PerNSS";
			 public const string PerFechaNacimiento = "PerFechaNacimiento";
			 public const string PerCasado = "PerCasado";
			 public const string PerSexo = "PerSexo";
			 public const string PerNumHijos = "PerNumHijos";
			 public const string PerConyuge = "PerConyuge";
			 public const string PerCCC = "PerCCC";
			 public const string PerSocio = "PerSocio";
			 public const string TraCodPK = "TraCodPK";
			 public const string CenCodPK = "CenCodPK";
			 public const string ZonCodPK = "ZonCodPK";
			 public const string PerFechaUltContrato = "PerFechaUltContrato";
			 public const string PerTipoCarnet = "PerTipoCarnet";
			 public const string PerFechaExpB1 = "PerFechaExpB1";
			 public const string PerFechaCadB2 = "PerFechaCadB2";
			 public const string VehCodPK = "VehCodPK";
			 public const string PerActivo = "PerActivo";
			 public const string PerUTE = "PerUTE";
			 public const string PerNifConyuge = "PerNifConyuge";
			 public const string PerAnosHijo = "PerAnosHijo";
			 public const string PerTrabajaConyuge = "PerTrabajaConyuge";
			 public const string PerDNIValidezH = "PerDNIValidezH";
			 public const string EmpCodPK = "EmpCodPK";
			 public const string PerAlta = "PerAlta";
			 public const string PerBaja = "PerBaja";
			 public const string PerRef = "PerRef";
			 public const string TtrCodPK = "TtrCodPK";
			 public const string PerEstadoDocumentacion = "PerEstadoDocumentacion";
			 public const string PerEstadoDocumentacionPrc = "PerEstadoDocumentacionPrc";
			 public const string PerEstadoCaduca = "PerEstadoCaduca";
			 public const string PerDocumentar = "PerDocumentar";
			 public const string PerGestionGPS = "PerGestionGPS";
			 public const string PerIDConductor = "PerIDConductor";
			 public const string PerEstado = "PerEstado";
			 public const string PerEMail = "PerEMail";
			 public const string PerNIFExpedicion = "PerNIFExpedicion";
			 public const string PerNIFVencimiento = "PerNIFVencimiento";
			 public const string PerNombreCompleto = "PerNombreCompleto";
			 public const string PerAntiguedad = "PerAntiguedad";
			 public const string PerEnviarFax = "PerEnviarFax";
			 public const string PerTelefono = "PerTelefono";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PersonalMetadata))
			{
				if(PersonalMetadata.mapDelegates == null)
				{
					PersonalMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PersonalMetadata.meta == null)
				{
					PersonalMetadata.meta = new PersonalMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("PerCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PerCodigo", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("AreCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PerNombre", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerApellidos", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerDireccion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerCP", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("LcdCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PvcCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PerNIF", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerNSS", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerFechaNacimiento", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("PerCasado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PerSexo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PerNumHijos", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PerConyuge", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerCCC", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerSocio", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TraCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("CenCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ZonCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PerFechaUltContrato", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("PerTipoCarnet", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerFechaExpB1", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("PerFechaCadB2", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PerActivo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PerUTE", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PerNifConyuge", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerAnosHijo", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerTrabajaConyuge", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PerDNIValidezH", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("EmpCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PerAlta", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("PerBaja", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("PerRef", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TtrCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PerEstadoDocumentacion", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("PerEstadoDocumentacionPrc", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("PerEstadoCaduca", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("PerDocumentar", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("PerGestionGPS", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("PerIDConductor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PerEstado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PerEMail", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerNIFExpedicion", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PerNIFVencimiento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PerNombreCompleto", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PerAntiguedad", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PerEnviarFax", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("PerTelefono", new esTypeMap("nvarchar", "System.String"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "Personal";
				meta.Destination = "Personal";
				
				meta.spInsert = "proc_PersonalInsert";				
				meta.spUpdate = "proc_PersonalUpdate";		
				meta.spDelete = "proc_PersonalDelete";
				meta.spLoadAll = "proc_PersonalLoadAll";
				meta.spLoadByPrimaryKey = "proc_PersonalLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PersonalMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
