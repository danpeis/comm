
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// MANTIENE LA LISTA DE TODOS LOS TERMINALES REGISTRADOS POR EL GITS. SE VINCULA CON [Vehiculos] para saber en que vehiculo está un terminal embarcado.
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TerminalGPS")]
	public partial class TerminalGPS : esTerminalGPS
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TerminalGPS();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal mdtCodPK)
		{
			var obj = new TerminalGPS();
			obj.MdtCodPK = mdtCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal mdtCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new TerminalGPS();
			obj.MdtCodPK = mdtCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal MDTCODPK)
        {
            try
            {
                var e = new TerminalGPS();
                return (e.LoadByPrimaryKey(MDTCODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TerminalGPS Get(System.Decimal MDTCODPK)
        {
            try
            {
                var e = new TerminalGPS();
                return (e.LoadByPrimaryKey(MDTCODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TerminalGPS(System.Decimal MDTCODPK)
        {
            this.LoadByPrimaryKey(MDTCODPK);
        }
		
		public TerminalGPS()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TerminalGPSCol")]
	public partial class TerminalGPSCol : esTerminalGPSCol, IEnumerable<TerminalGPS>
	{
	
		public TerminalGPSCol()
		{
		}

	
	
		public TerminalGPS FindByPrimaryKey(System.Decimal mdtCodPK)
		{
			return this.SingleOrDefault(e => e.MdtCodPK == mdtCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TerminalGPS))]
		public class TerminalGPSColWCFPacket : esCollectionWCFPacket<TerminalGPSCol>
		{
			public static implicit operator TerminalGPSCol(TerminalGPSColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TerminalGPSColWCFPacket(TerminalGPSCol collection)
			{
				return new TerminalGPSColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TerminalGPSQ : esTerminalGPSQ
	{
		public TerminalGPSQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TerminalGPSQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TerminalGPSQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new TerminalGPSQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TerminalGPSQ query)
		{
			return TerminalGPSQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TerminalGPSQ(string query)
		{
			return (TerminalGPSQ)TerminalGPSQ.SerializeHelper.FromXml(query, typeof(TerminalGPSQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTerminalGPS : EntityBase
	{
		public esTerminalGPS()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal mdtCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(mdtCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(mdtCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal mdtCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(mdtCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(mdtCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal mdtCodPK)
		{
			TerminalGPSQ query = new TerminalGPSQ();
			query.Where(query.MdtCodPK == mdtCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal mdtCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("MdtCodPK", mdtCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? MdtCodPK
		{
			get
			{
				return base.GetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtCodPK, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Estado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? MdtEstado
		{
			get
			{
				return base.GetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtEstado);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtEstado, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtEstado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Ref
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? MdtRef
		{
			get
			{
				return base.GetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtRef);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtRef, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtRef);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_NS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MdtNS
		{
			get
			{
				return base.GetSystemString(TerminalGPSMetadata.ColumnNames.MdtNS);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGPSMetadata.ColumnNames.MdtNS, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtNS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_FechaAlta
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? MdtFechaAlta
		{
			get
			{
				return base.GetSystemDateTime(TerminalGPSMetadata.ColumnNames.MdtFechaAlta);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalGPSMetadata.ColumnNames.MdtFechaAlta, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtFechaAlta);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Marca
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MdtMarca
		{
			get
			{
				return base.GetSystemString(TerminalGPSMetadata.ColumnNames.MdtMarca);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGPSMetadata.ColumnNames.MdtMarca, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtMarca);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Proveedor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MdtProveedor
		{
			get
			{
				return base.GetSystemString(TerminalGPSMetadata.ColumnNames.MdtProveedor);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGPSMetadata.ColumnNames.MdtProveedor, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtProveedor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Telefono
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MdtTelefono
		{
			get
			{
				return base.GetSystemString(TerminalGPSMetadata.ColumnNames.MdtTelefono);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGPSMetadata.ColumnNames.MdtTelefono, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtTelefono);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Observacion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MdtObservacion
		{
			get
			{
				return base.GetSystemString(TerminalGPSMetadata.ColumnNames.MdtObservacion);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGPSMetadata.ColumnNames.MdtObservacion, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtObservacion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Averiado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? MdtAveriado
		{
			get
			{
				return base.GetSystemBoolean(TerminalGPSMetadata.ColumnNames.MdtAveriado);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalGPSMetadata.ColumnNames.MdtAveriado, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtAveriado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Averia
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MdtAveria
		{
			get
			{
				return base.GetSystemString(TerminalGPSMetadata.ColumnNames.MdtAveria);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGPSMetadata.ColumnNames.MdtAveria, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtAveria);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Actualizado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? MdtActualizado
		{
			get
			{
				return base.GetSystemBoolean(TerminalGPSMetadata.ColumnNames.MdtActualizado);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalGPSMetadata.ColumnNames.MdtActualizado, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtActualizado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Servidor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MdtServidor
		{
			get
			{
				return base.GetSystemString(TerminalGPSMetadata.ColumnNames.MdtServidor);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGPSMetadata.ColumnNames.MdtServidor, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtServidor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_ServidorIP
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MdtServidorIP
		{
			get
			{
				return base.GetSystemString(TerminalGPSMetadata.ColumnNames.MdtServidorIP);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGPSMetadata.ColumnNames.MdtServidorIP, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtServidorIP);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Caja
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MdtCaja
		{
			get
			{
				return base.GetSystemString(TerminalGPSMetadata.ColumnNames.MdtCaja);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGPSMetadata.ColumnNames.MdtCaja, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtCaja);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_Perdido
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? MdtPerdido
		{
			get
			{
				return base.GetSystemBoolean(TerminalGPSMetadata.ColumnNames.MdtPerdido);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalGPSMetadata.ColumnNames.MdtPerdido, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtPerdido);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.tjs_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TjsCodPK
		{
			get
			{
				return base.GetSystemDecimal(TerminalGPSMetadata.ColumnNames.TjsCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGPSMetadata.ColumnNames.TjsCodPK, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.TjsCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.vrsm_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VrsmCodPK
		{
			get
			{
				return base.GetSystemDecimal(TerminalGPSMetadata.ColumnNames.VrsmCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGPSMetadata.ColumnNames.VrsmCodPK, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.VrsmCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_EstadoCMN
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? MdtEstadoCMN
		{
			get
			{
				return base.GetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtEstadoCMN);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtEstadoCMN, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtEstadoCMN);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.emp_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCodPK
		{
			get
			{
				return base.GetSystemDecimal(TerminalGPSMetadata.ColumnNames.EmpCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGPSMetadata.ColumnNames.EmpCodPK, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.EmpCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalesGPS.mdt_PaqueteLimite
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? MdtPaqueteLimite
		{
			get
			{
				return base.GetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtPaqueteLimite);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGPSMetadata.ColumnNames.MdtPaqueteLimite, value))
				{
					OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtPaqueteLimite);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "MdtCodPK": this.str().MdtCodPK = (string)value; break;							
						case "MdtEstado": this.str().MdtEstado = (string)value; break;							
						case "MdtRef": this.str().MdtRef = (string)value; break;							
						case "MdtNS": this.str().MdtNS = (string)value; break;							
						case "MdtFechaAlta": this.str().MdtFechaAlta = (string)value; break;							
						case "MdtMarca": this.str().MdtMarca = (string)value; break;							
						case "MdtProveedor": this.str().MdtProveedor = (string)value; break;							
						case "MdtTelefono": this.str().MdtTelefono = (string)value; break;							
						case "MdtObservacion": this.str().MdtObservacion = (string)value; break;							
						case "MdtAveriado": this.str().MdtAveriado = (string)value; break;							
						case "MdtAveria": this.str().MdtAveria = (string)value; break;							
						case "MdtActualizado": this.str().MdtActualizado = (string)value; break;							
						case "MdtServidor": this.str().MdtServidor = (string)value; break;							
						case "MdtServidorIP": this.str().MdtServidorIP = (string)value; break;							
						case "MdtCaja": this.str().MdtCaja = (string)value; break;							
						case "MdtPerdido": this.str().MdtPerdido = (string)value; break;							
						case "TjsCodPK": this.str().TjsCodPK = (string)value; break;							
						case "VrsmCodPK": this.str().VrsmCodPK = (string)value; break;							
						case "MdtEstadoCMN": this.str().MdtEstadoCMN = (string)value; break;							
						case "EmpCodPK": this.str().EmpCodPK = (string)value; break;							
						case "MdtPaqueteLimite": this.str().MdtPaqueteLimite = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "MdtCodPK":
						
							if (value == null || value is System.Decimal)
								this.MdtCodPK = (System.Decimal?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtCodPK);
							break;
						
						case "MdtEstado":
						
							if (value == null || value is System.Decimal)
								this.MdtEstado = (System.Decimal?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtEstado);
							break;
						
						case "MdtRef":
						
							if (value == null || value is System.Decimal)
								this.MdtRef = (System.Decimal?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtRef);
							break;
						
						case "MdtFechaAlta":
						
							if (value == null || value is System.DateTime)
								this.MdtFechaAlta = (System.DateTime?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtFechaAlta);
							break;
						
						case "MdtAveriado":
						
							if (value == null || value is System.Boolean)
								this.MdtAveriado = (System.Boolean?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtAveriado);
							break;
						
						case "MdtActualizado":
						
							if (value == null || value is System.Boolean)
								this.MdtActualizado = (System.Boolean?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtActualizado);
							break;
						
						case "MdtPerdido":
						
							if (value == null || value is System.Boolean)
								this.MdtPerdido = (System.Boolean?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtPerdido);
							break;
						
						case "TjsCodPK":
						
							if (value == null || value is System.Decimal)
								this.TjsCodPK = (System.Decimal?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.TjsCodPK);
							break;
						
						case "VrsmCodPK":
						
							if (value == null || value is System.Decimal)
								this.VrsmCodPK = (System.Decimal?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.VrsmCodPK);
							break;
						
						case "MdtEstadoCMN":
						
							if (value == null || value is System.Decimal)
								this.MdtEstadoCMN = (System.Decimal?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtEstadoCMN);
							break;
						
						case "EmpCodPK":
						
							if (value == null || value is System.Decimal)
								this.EmpCodPK = (System.Decimal?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.EmpCodPK);
							break;
						
						case "MdtPaqueteLimite":
						
							if (value == null || value is System.Decimal)
								this.MdtPaqueteLimite = (System.Decimal?)value;
								OnPropertyChanged(TerminalGPSMetadata.PropertyNames.MdtPaqueteLimite);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTerminalGPS entity)
			{
				this.entity = entity;
			}
			
	
			public System.String MdtCodPK
			{
				get
				{
					System.Decimal? data = entity.MdtCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtCodPK = null;
					else entity.MdtCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String MdtEstado
			{
				get
				{
					System.Decimal? data = entity.MdtEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtEstado = null;
					else entity.MdtEstado = Convert.ToDecimal(value);
				}
			}
				
			public System.String MdtRef
			{
				get
				{
					System.Decimal? data = entity.MdtRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtRef = null;
					else entity.MdtRef = Convert.ToDecimal(value);
				}
			}
				
			public System.String MdtNS
			{
				get
				{
					System.String data = entity.MdtNS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtNS = null;
					else entity.MdtNS = Convert.ToString(value);
				}
			}
				
			public System.String MdtFechaAlta
			{
				get
				{
					System.DateTime? data = entity.MdtFechaAlta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtFechaAlta = null;
					else entity.MdtFechaAlta = Convert.ToDateTime(value);
				}
			}
				
			public System.String MdtMarca
			{
				get
				{
					System.String data = entity.MdtMarca;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtMarca = null;
					else entity.MdtMarca = Convert.ToString(value);
				}
			}
				
			public System.String MdtProveedor
			{
				get
				{
					System.String data = entity.MdtProveedor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtProveedor = null;
					else entity.MdtProveedor = Convert.ToString(value);
				}
			}
				
			public System.String MdtTelefono
			{
				get
				{
					System.String data = entity.MdtTelefono;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtTelefono = null;
					else entity.MdtTelefono = Convert.ToString(value);
				}
			}
				
			public System.String MdtObservacion
			{
				get
				{
					System.String data = entity.MdtObservacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtObservacion = null;
					else entity.MdtObservacion = Convert.ToString(value);
				}
			}
				
			public System.String MdtAveriado
			{
				get
				{
					System.Boolean? data = entity.MdtAveriado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtAveriado = null;
					else entity.MdtAveriado = Convert.ToBoolean(value);
				}
			}
				
			public System.String MdtAveria
			{
				get
				{
					System.String data = entity.MdtAveria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtAveria = null;
					else entity.MdtAveria = Convert.ToString(value);
				}
			}
				
			public System.String MdtActualizado
			{
				get
				{
					System.Boolean? data = entity.MdtActualizado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtActualizado = null;
					else entity.MdtActualizado = Convert.ToBoolean(value);
				}
			}
				
			public System.String MdtServidor
			{
				get
				{
					System.String data = entity.MdtServidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtServidor = null;
					else entity.MdtServidor = Convert.ToString(value);
				}
			}
				
			public System.String MdtServidorIP
			{
				get
				{
					System.String data = entity.MdtServidorIP;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtServidorIP = null;
					else entity.MdtServidorIP = Convert.ToString(value);
				}
			}
				
			public System.String MdtCaja
			{
				get
				{
					System.String data = entity.MdtCaja;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtCaja = null;
					else entity.MdtCaja = Convert.ToString(value);
				}
			}
				
			public System.String MdtPerdido
			{
				get
				{
					System.Boolean? data = entity.MdtPerdido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtPerdido = null;
					else entity.MdtPerdido = Convert.ToBoolean(value);
				}
			}
				
			public System.String TjsCodPK
			{
				get
				{
					System.Decimal? data = entity.TjsCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsCodPK = null;
					else entity.TjsCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VrsmCodPK
			{
				get
				{
					System.Decimal? data = entity.VrsmCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VrsmCodPK = null;
					else entity.VrsmCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String MdtEstadoCMN
			{
				get
				{
					System.Decimal? data = entity.MdtEstadoCMN;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtEstadoCMN = null;
					else entity.MdtEstadoCMN = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpCodPK
			{
				get
				{
					System.Decimal? data = entity.EmpCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCodPK = null;
					else entity.EmpCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String MdtPaqueteLimite
			{
				get
				{
					System.Decimal? data = entity.MdtPaqueteLimite;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtPaqueteLimite = null;
					else entity.MdtPaqueteLimite = Convert.ToDecimal(value);
				}
			}
			

			private esTerminalGPS entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TerminalGPSMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TerminalGPSQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalGPSQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalGPSQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TerminalGPSQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TerminalGPSQ query;		
	}



	[Serializable]
	abstract public partial class esTerminalGPSCol : CollectionBase<TerminalGPS>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TerminalGPSMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TerminalGPSCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TerminalGPSQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalGPSQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalGPSQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TerminalGPSQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TerminalGPSQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TerminalGPSQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TerminalGPSQ query;
	}



	[Serializable]
	abstract public partial class esTerminalGPSQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TerminalGPSMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtCodPK,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtCodPK, esSystemType.Decimal));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtEstado,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtEstado, esSystemType.Decimal));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtRef,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtRef, esSystemType.Decimal));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtNS,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtNS, esSystemType.String));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtFechaAlta,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtFechaAlta, esSystemType.DateTime));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtMarca,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtMarca, esSystemType.String));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtProveedor,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtProveedor, esSystemType.String));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtTelefono,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtTelefono, esSystemType.String));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtObservacion,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtObservacion, esSystemType.String));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtAveriado,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtAveriado, esSystemType.Boolean));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtAveria,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtAveria, esSystemType.String));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtActualizado,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtActualizado, esSystemType.Boolean));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtServidor,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtServidor, esSystemType.String));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtServidorIP,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtServidorIP, esSystemType.String));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtCaja,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtCaja, esSystemType.String));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtPerdido,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtPerdido, esSystemType.Boolean));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.TjsCodPK,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.TjsCodPK, esSystemType.Decimal));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.VrsmCodPK,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.VrsmCodPK, esSystemType.Decimal));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtEstadoCMN,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtEstadoCMN, esSystemType.Decimal));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.EmpCodPK,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal));
			_queryItems.Add(TerminalGPSMetadata.ColumnNames.MdtPaqueteLimite,new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtPaqueteLimite, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem MdtCodPK
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem MdtEstado
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtEstado, esSystemType.Decimal); }
		} 
		
		public esQueryItem MdtRef
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtRef, esSystemType.Decimal); }
		} 
		
		public esQueryItem MdtNS
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtNS, esSystemType.String); }
		} 
		
		public esQueryItem MdtFechaAlta
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtFechaAlta, esSystemType.DateTime); }
		} 
		
		public esQueryItem MdtMarca
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtMarca, esSystemType.String); }
		} 
		
		public esQueryItem MdtProveedor
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtProveedor, esSystemType.String); }
		} 
		
		public esQueryItem MdtTelefono
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtTelefono, esSystemType.String); }
		} 
		
		public esQueryItem MdtObservacion
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtObservacion, esSystemType.String); }
		} 
		
		public esQueryItem MdtAveriado
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtAveriado, esSystemType.Boolean); }
		} 
		
		public esQueryItem MdtAveria
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtAveria, esSystemType.String); }
		} 
		
		public esQueryItem MdtActualizado
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtActualizado, esSystemType.Boolean); }
		} 
		
		public esQueryItem MdtServidor
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtServidor, esSystemType.String); }
		} 
		
		public esQueryItem MdtServidorIP
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtServidorIP, esSystemType.String); }
		} 
		
		public esQueryItem MdtCaja
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtCaja, esSystemType.String); }
		} 
		
		public esQueryItem MdtPerdido
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtPerdido, esSystemType.Boolean); }
		} 
		
		public esQueryItem TjsCodPK
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.TjsCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VrsmCodPK
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.VrsmCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem MdtEstadoCMN
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtEstadoCMN, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpCodPK
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem MdtPaqueteLimite
		{
			get { return new esQueryItem(this, TerminalGPSMetadata.ColumnNames.MdtPaqueteLimite, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class TerminalGPS : esTerminalGPS
	{

		
		
	}
	



	[Serializable]
	public partial class TerminalGPSMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TerminalGPSMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtEstado, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtEstado;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtRef, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtRef;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtNS, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtNS;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtFechaAlta, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtFechaAlta;
			c.HasDefault = true;
			c.Default = @"('03/07/2008')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtMarca, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtMarca;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('Net-960 EX')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtProveedor, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtProveedor;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtTelefono, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtTelefono;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtObservacion, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtObservacion;
			c.CharacterMaxLength = 200;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtAveriado, 9, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtAveriado;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtAveria, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtAveria;
			c.CharacterMaxLength = 200;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtActualizado, 11, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtActualizado;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtServidor, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtServidor;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtServidorIP, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtServidorIP;
			c.CharacterMaxLength = 15;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtCaja, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtCaja;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtPerdido, 15, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtPerdido;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.TjsCodPK, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.TjsCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.VrsmCodPK, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.VrsmCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtEstadoCMN, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtEstadoCMN;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.EmpCodPK, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.EmpCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGPSMetadata.ColumnNames.MdtPaqueteLimite, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGPSMetadata.PropertyNames.MdtPaqueteLimite;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TerminalGPSMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string MdtCodPK = "mdt_CodPK";
			 public const string MdtEstado = "mdt_Estado";
			 public const string MdtRef = "mdt_Ref";
			 public const string MdtNS = "mdt_NS";
			 public const string MdtFechaAlta = "mdt_FechaAlta";
			 public const string MdtMarca = "mdt_Marca";
			 public const string MdtProveedor = "mdt_Proveedor";
			 public const string MdtTelefono = "mdt_Telefono";
			 public const string MdtObservacion = "mdt_Observacion";
			 public const string MdtAveriado = "mdt_Averiado";
			 public const string MdtAveria = "mdt_Averia";
			 public const string MdtActualizado = "mdt_Actualizado";
			 public const string MdtServidor = "mdt_Servidor";
			 public const string MdtServidorIP = "mdt_ServidorIP";
			 public const string MdtCaja = "mdt_Caja";
			 public const string MdtPerdido = "mdt_Perdido";
			 public const string TjsCodPK = "tjs_CodPK";
			 public const string VrsmCodPK = "vrsm_CodPK";
			 public const string MdtEstadoCMN = "mdt_EstadoCMN";
			 public const string EmpCodPK = "emp_CodPK";
			 public const string MdtPaqueteLimite = "mdt_PaqueteLimite";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string MdtCodPK = "MdtCodPK";
			 public const string MdtEstado = "MdtEstado";
			 public const string MdtRef = "MdtRef";
			 public const string MdtNS = "MdtNS";
			 public const string MdtFechaAlta = "MdtFechaAlta";
			 public const string MdtMarca = "MdtMarca";
			 public const string MdtProveedor = "MdtProveedor";
			 public const string MdtTelefono = "MdtTelefono";
			 public const string MdtObservacion = "MdtObservacion";
			 public const string MdtAveriado = "MdtAveriado";
			 public const string MdtAveria = "MdtAveria";
			 public const string MdtActualizado = "MdtActualizado";
			 public const string MdtServidor = "MdtServidor";
			 public const string MdtServidorIP = "MdtServidorIP";
			 public const string MdtCaja = "MdtCaja";
			 public const string MdtPerdido = "MdtPerdido";
			 public const string TjsCodPK = "TjsCodPK";
			 public const string VrsmCodPK = "VrsmCodPK";
			 public const string MdtEstadoCMN = "MdtEstadoCMN";
			 public const string EmpCodPK = "EmpCodPK";
			 public const string MdtPaqueteLimite = "MdtPaqueteLimite";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TerminalGPSMetadata))
			{
				if(TerminalGPSMetadata.mapDelegates == null)
				{
					TerminalGPSMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TerminalGPSMetadata.meta == null)
				{
					TerminalGPSMetadata.meta = new TerminalGPSMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("MdtCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("MdtEstado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("MdtRef", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("MdtNS", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MdtFechaAlta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("MdtMarca", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MdtProveedor", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MdtTelefono", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MdtObservacion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MdtAveriado", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("MdtAveria", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MdtActualizado", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("MdtServidor", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MdtServidorIP", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MdtCaja", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MdtPerdido", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("TjsCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VrsmCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("MdtEstadoCMN", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("MdtPaqueteLimite", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "TerminalesGPS";
				meta.Destination = "TerminalesGPS";
				
				meta.spInsert = "proc_TerminalesGPSInsert";				
				meta.spUpdate = "proc_TerminalesGPSUpdate";		
				meta.spDelete = "proc_TerminalesGPSDelete";
				meta.spLoadAll = "proc_TerminalesGPSLoadAll";
				meta.spLoadByPrimaryKey = "proc_TerminalesGPSLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TerminalGPSMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
