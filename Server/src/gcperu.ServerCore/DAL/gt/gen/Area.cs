
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:19
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// Encapsulates the 'Areas' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("Area")]
	public partial class Area : esArea
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new Area();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal areCodPK)
		{
			var obj = new Area();
			obj.AreCodPK = areCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal areCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new Area();
			obj.AreCodPK = areCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ARECODPK)
        {
            try
            {
                var e = new Area();
                return (e.LoadByPrimaryKey(ARECODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static Area Get(System.Decimal ARECODPK)
        {
            try
            {
                var e = new Area();
                return (e.LoadByPrimaryKey(ARECODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public Area(System.Decimal ARECODPK)
        {
            this.LoadByPrimaryKey(ARECODPK);
        }
		
		public Area()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("AreaCol")]
	public partial class AreaCol : esAreaCol, IEnumerable<Area>
	{
	
		public AreaCol()
		{
		}

	
	
		public Area FindByPrimaryKey(System.Decimal areCodPK)
		{
			return this.SingleOrDefault(e => e.AreCodPK == areCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(Area))]
		public class AreaColWCFPacket : esCollectionWCFPacket<AreaCol>
		{
			public static implicit operator AreaCol(AreaColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator AreaColWCFPacket(AreaCol collection)
			{
				return new AreaColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class AreaQ : esAreaQ
	{
		public AreaQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public AreaQ()
		{
		}

		override protected string GetQueryName()
		{
			return "AreaQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new AreaQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(AreaQ query)
		{
			return AreaQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator AreaQ(string query)
		{
			return (AreaQ)AreaQ.SerializeHelper.FromXml(query, typeof(AreaQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esArea : EntityBase
	{
		public esArea()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal areCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(areCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(areCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal areCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(areCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(areCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal areCodPK)
		{
			AreaQ query = new AreaQ();
			query.Where(query.AreCodPK == areCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal areCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("AreCodPK", areCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to Areas.are_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreCodPK
		{
			get
			{
				return base.GetSystemDecimal(AreaMetadata.ColumnNames.AreCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(AreaMetadata.ColumnNames.AreCodPK, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AreCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.are_Descripcion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String AreDescripcion
		{
			get
			{
				return base.GetSystemString(AreaMetadata.ColumnNames.AreDescripcion);
			}
			
			set
			{
				if(base.SetSystemString(AreaMetadata.ColumnNames.AreDescripcion, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AreDescripcion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.are_Codigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String AreCodigo
		{
			get
			{
				return base.GetSystemString(AreaMetadata.ColumnNames.AreCodigo);
			}
			
			set
			{
				if(base.SetSystemString(AreaMetadata.ColumnNames.AreCodigo, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AreCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.are_Color
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String AreColor
		{
			get
			{
				return base.GetSystemString(AreaMetadata.ColumnNames.AreColor);
			}
			
			set
			{
				if(base.SetSystemString(AreaMetadata.ColumnNames.AreColor, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AreColor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.asa_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AsaCodPK
		{
			get
			{
				return base.GetSystemDecimal(AreaMetadata.ColumnNames.AsaCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(AreaMetadata.ColumnNames.AsaCodPK, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AsaCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.are_Eliminada
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? AreEliminada
		{
			get
			{
				return base.GetSystemBoolean(AreaMetadata.ColumnNames.AreEliminada);
			}
			
			set
			{
				if(base.SetSystemBoolean(AreaMetadata.ColumnNames.AreEliminada, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AreEliminada);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.emp_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCodPK
		{
			get
			{
				return base.GetSystemDecimal(AreaMetadata.ColumnNames.EmpCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(AreaMetadata.ColumnNames.EmpCodPK, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.EmpCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.pvc_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PvcCodPK
		{
			get
			{
				return base.GetSystemDecimal(AreaMetadata.ColumnNames.PvcCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(AreaMetadata.ColumnNames.PvcCodPK, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.PvcCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.are_Ref
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreRef
		{
			get
			{
				return base.GetSystemDecimal(AreaMetadata.ColumnNames.AreRef);
			}
			
			set
			{
				if(base.SetSystemDecimal(AreaMetadata.ColumnNames.AreRef, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AreRef);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.are_FlotaGlobal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? AreFlotaGlobal
		{
			get
			{
				return base.GetSystemBoolean(AreaMetadata.ColumnNames.AreFlotaGlobal);
			}
			
			set
			{
				if(base.SetSystemBoolean(AreaMetadata.ColumnNames.AreFlotaGlobal, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AreFlotaGlobal);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.are_Estado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreEstado
		{
			get
			{
				return base.GetSystemDecimal(AreaMetadata.ColumnNames.AreEstado);
			}
			
			set
			{
				if(base.SetSystemDecimal(AreaMetadata.ColumnNames.AreEstado, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AreEstado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Areas.are_CodigoAdministracion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String AreCodigoAdministracion
		{
			get
			{
				return base.GetSystemString(AreaMetadata.ColumnNames.AreCodigoAdministracion);
			}
			
			set
			{
				if(base.SetSystemString(AreaMetadata.ColumnNames.AreCodigoAdministracion, value))
				{
					OnPropertyChanged(AreaMetadata.PropertyNames.AreCodigoAdministracion);
				}
			}
		}		
		
		[CLSCompliant(false)]
		internal protected Area _UpToAreaByAreCodPK;
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "AreCodPK": this.str().AreCodPK = (string)value; break;							
						case "AreDescripcion": this.str().AreDescripcion = (string)value; break;							
						case "AreCodigo": this.str().AreCodigo = (string)value; break;							
						case "AreColor": this.str().AreColor = (string)value; break;							
						case "AsaCodPK": this.str().AsaCodPK = (string)value; break;							
						case "AreEliminada": this.str().AreEliminada = (string)value; break;							
						case "EmpCodPK": this.str().EmpCodPK = (string)value; break;							
						case "PvcCodPK": this.str().PvcCodPK = (string)value; break;							
						case "AreRef": this.str().AreRef = (string)value; break;							
						case "AreFlotaGlobal": this.str().AreFlotaGlobal = (string)value; break;							
						case "AreEstado": this.str().AreEstado = (string)value; break;							
						case "AreCodigoAdministracion": this.str().AreCodigoAdministracion = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "AreCodPK":
						
							if (value == null || value is System.Decimal)
								this.AreCodPK = (System.Decimal?)value;
								OnPropertyChanged(AreaMetadata.PropertyNames.AreCodPK);
							break;
						
						case "AsaCodPK":
						
							if (value == null || value is System.Decimal)
								this.AsaCodPK = (System.Decimal?)value;
								OnPropertyChanged(AreaMetadata.PropertyNames.AsaCodPK);
							break;
						
						case "AreEliminada":
						
							if (value == null || value is System.Boolean)
								this.AreEliminada = (System.Boolean?)value;
								OnPropertyChanged(AreaMetadata.PropertyNames.AreEliminada);
							break;
						
						case "EmpCodPK":
						
							if (value == null || value is System.Decimal)
								this.EmpCodPK = (System.Decimal?)value;
								OnPropertyChanged(AreaMetadata.PropertyNames.EmpCodPK);
							break;
						
						case "PvcCodPK":
						
							if (value == null || value is System.Decimal)
								this.PvcCodPK = (System.Decimal?)value;
								OnPropertyChanged(AreaMetadata.PropertyNames.PvcCodPK);
							break;
						
						case "AreRef":
						
							if (value == null || value is System.Decimal)
								this.AreRef = (System.Decimal?)value;
								OnPropertyChanged(AreaMetadata.PropertyNames.AreRef);
							break;
						
						case "AreFlotaGlobal":
						
							if (value == null || value is System.Boolean)
								this.AreFlotaGlobal = (System.Boolean?)value;
								OnPropertyChanged(AreaMetadata.PropertyNames.AreFlotaGlobal);
							break;
						
						case "AreEstado":
						
							if (value == null || value is System.Decimal)
								this.AreEstado = (System.Decimal?)value;
								OnPropertyChanged(AreaMetadata.PropertyNames.AreEstado);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esArea entity)
			{
				this.entity = entity;
			}
			
	
			public System.String AreCodPK
			{
				get
				{
					System.Decimal? data = entity.AreCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreCodPK = null;
					else entity.AreCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreDescripcion
			{
				get
				{
					System.String data = entity.AreDescripcion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreDescripcion = null;
					else entity.AreDescripcion = Convert.ToString(value);
				}
			}
				
			public System.String AreCodigo
			{
				get
				{
					System.String data = entity.AreCodigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreCodigo = null;
					else entity.AreCodigo = Convert.ToString(value);
				}
			}
				
			public System.String AreColor
			{
				get
				{
					System.String data = entity.AreColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreColor = null;
					else entity.AreColor = Convert.ToString(value);
				}
			}
				
			public System.String AsaCodPK
			{
				get
				{
					System.Decimal? data = entity.AsaCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AsaCodPK = null;
					else entity.AsaCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreEliminada
			{
				get
				{
					System.Boolean? data = entity.AreEliminada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreEliminada = null;
					else entity.AreEliminada = Convert.ToBoolean(value);
				}
			}
				
			public System.String EmpCodPK
			{
				get
				{
					System.Decimal? data = entity.EmpCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCodPK = null;
					else entity.EmpCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PvcCodPK
			{
				get
				{
					System.Decimal? data = entity.PvcCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PvcCodPK = null;
					else entity.PvcCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreRef
			{
				get
				{
					System.Decimal? data = entity.AreRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreRef = null;
					else entity.AreRef = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreFlotaGlobal
			{
				get
				{
					System.Boolean? data = entity.AreFlotaGlobal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreFlotaGlobal = null;
					else entity.AreFlotaGlobal = Convert.ToBoolean(value);
				}
			}
				
			public System.String AreEstado
			{
				get
				{
					System.Decimal? data = entity.AreEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreEstado = null;
					else entity.AreEstado = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreCodigoAdministracion
			{
				get
				{
					System.String data = entity.AreCodigoAdministracion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreCodigoAdministracion = null;
					else entity.AreCodigoAdministracion = Convert.ToString(value);
				}
			}
			

			private esArea entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return AreaMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public AreaQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AreaQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(AreaQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(AreaQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private AreaQ query;		
	}



	[Serializable]
	abstract public partial class esAreaCol : CollectionBase<Area>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AreaMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "AreaCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public AreaQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AreaQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(AreaQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AreaQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(AreaQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((AreaQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private AreaQ query;
	}



	[Serializable]
	abstract public partial class esAreaQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return AreaMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(AreaMetadata.ColumnNames.AreCodPK,new esQueryItem(this, AreaMetadata.ColumnNames.AreCodPK, esSystemType.Decimal));
			_queryItems.Add(AreaMetadata.ColumnNames.AreDescripcion,new esQueryItem(this, AreaMetadata.ColumnNames.AreDescripcion, esSystemType.String));
			_queryItems.Add(AreaMetadata.ColumnNames.AreCodigo,new esQueryItem(this, AreaMetadata.ColumnNames.AreCodigo, esSystemType.String));
			_queryItems.Add(AreaMetadata.ColumnNames.AreColor,new esQueryItem(this, AreaMetadata.ColumnNames.AreColor, esSystemType.String));
			_queryItems.Add(AreaMetadata.ColumnNames.AsaCodPK,new esQueryItem(this, AreaMetadata.ColumnNames.AsaCodPK, esSystemType.Decimal));
			_queryItems.Add(AreaMetadata.ColumnNames.AreEliminada,new esQueryItem(this, AreaMetadata.ColumnNames.AreEliminada, esSystemType.Boolean));
			_queryItems.Add(AreaMetadata.ColumnNames.EmpCodPK,new esQueryItem(this, AreaMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal));
			_queryItems.Add(AreaMetadata.ColumnNames.PvcCodPK,new esQueryItem(this, AreaMetadata.ColumnNames.PvcCodPK, esSystemType.Decimal));
			_queryItems.Add(AreaMetadata.ColumnNames.AreRef,new esQueryItem(this, AreaMetadata.ColumnNames.AreRef, esSystemType.Decimal));
			_queryItems.Add(AreaMetadata.ColumnNames.AreFlotaGlobal,new esQueryItem(this, AreaMetadata.ColumnNames.AreFlotaGlobal, esSystemType.Boolean));
			_queryItems.Add(AreaMetadata.ColumnNames.AreEstado,new esQueryItem(this, AreaMetadata.ColumnNames.AreEstado, esSystemType.Decimal));
			_queryItems.Add(AreaMetadata.ColumnNames.AreCodigoAdministracion,new esQueryItem(this, AreaMetadata.ColumnNames.AreCodigoAdministracion, esSystemType.String));
			
	    }
	
	    #region esQueryItems

		public esQueryItem AreCodPK
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AreCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreDescripcion
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AreDescripcion, esSystemType.String); }
		} 
		
		public esQueryItem AreCodigo
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AreCodigo, esSystemType.String); }
		} 
		
		public esQueryItem AreColor
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AreColor, esSystemType.String); }
		} 
		
		public esQueryItem AsaCodPK
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AsaCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreEliminada
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AreEliminada, esSystemType.Boolean); }
		} 
		
		public esQueryItem EmpCodPK
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PvcCodPK
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.PvcCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreRef
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AreRef, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreFlotaGlobal
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AreFlotaGlobal, esSystemType.Boolean); }
		} 
		
		public esQueryItem AreEstado
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AreEstado, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreCodigoAdministracion
		{
			get { return new esQueryItem(this, AreaMetadata.ColumnNames.AreCodigoAdministracion, esSystemType.String); }
		} 
		
		#endregion
		
	}


	
	public partial class Area : esArea
	{

		#region AreaByAreCodPK - Zero To Many
		
		static public esPrefetchMap Prefetch_AreaByAreCodPK
		{
			get
			{
				esPrefetchMap map = new esPrefetchMap();
				map.PrefetchDelegate = gcperu.Server.Core.DAL.GT.Area.AreaByAreCodPK_Delegate;
				map.PropertyName = "AreaByAreCodPK";
				map.MyColumnName = "are_CodPK";
				map.ParentColumnName = "are_CodPK";
				map.IsMultiPartKey = false;
				return map;
			}
		}		
		
		static private void AreaByAreCodPK_Delegate(esPrefetchParameters data)
		{
			AreaQ parent = new AreaQ(data.NextAlias());

			AreaQ me = data.You != null ? data.You as AreaQ : new AreaQ(data.NextAlias());

			if (data.Root == null)
			{
				data.Root = me;
			}
			
			data.Root.InnerJoin(parent).On(parent.AreCodPK == me.AreCodPK);

			data.You = parent;
		}			
		
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_Areas_Areas
		/// </summary>

		[XmlIgnore]
		public AreaCol AreaByAreCodPK
		{
			get
			{
				if(this._AreaByAreCodPK == null)
				{
					this._AreaByAreCodPK = new AreaCol();
					this._AreaByAreCodPK.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AreaByAreCodPK", this._AreaByAreCodPK);
				
					if (this.AreCodPK != null)
					{
						if (!this.es.IsLazyLoadDisabled)
						{
							this._AreaByAreCodPK.Query.Where(this._AreaByAreCodPK.Query.AreCodPK == this.AreCodPK);
							this._AreaByAreCodPK.Query.Load();
						}

						// Auto-hookup Foreign Keys
						this._AreaByAreCodPK.fks.Add(AreaMetadata.ColumnNames.AreCodPK, this.AreCodPK);
					}
				}

				return this._AreaByAreCodPK;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AreaByAreCodPK != null) 
				{ 
					this.RemovePostSave("AreaByAreCodPK"); 
					this._AreaByAreCodPK = null;
					this.OnPropertyChanged("AreaByAreCodPK");
				} 
			} 			
		}
			
		
		private AreaCol _AreaByAreCodPK;
		#endregion

				
		#region UpToAreaByAreCodPK - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_Areas_Areas
		/// </summary>

		[XmlIgnore]
					
		public Area UpToAreaByAreCodPK
		{
			get
			{
				if (this.es.IsLazyLoadDisabled) return null;
				
				if(this._UpToAreaByAreCodPK == null && AreCodPK != null)
				{
					this._UpToAreaByAreCodPK = new Area();
					this._UpToAreaByAreCodPK.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAreaByAreCodPK", this._UpToAreaByAreCodPK);
					this._UpToAreaByAreCodPK.Query.Where(this._UpToAreaByAreCodPK.Query.AreCodPK == this.AreCodPK);
					this._UpToAreaByAreCodPK.Query.Load();
				}	
				return this._UpToAreaByAreCodPK;
			}
			
			set
			{
				this.RemovePreSave("UpToAreaByAreCodPK");
				
				bool changed = this._UpToAreaByAreCodPK != value;

				if(value == null)
				{
					this.AreCodPK = null;
					this._UpToAreaByAreCodPK = null;
				}
				else
				{
					this.AreCodPK = value.AreCodPK;
					this._UpToAreaByAreCodPK = value;
					this.SetPreSave("UpToAreaByAreCodPK", this._UpToAreaByAreCodPK);
				}
				
				if( changed )
				{
					this.OnPropertyChanged("UpToAreaByAreCodPK");
				}
			}
		}
		#endregion
		

		#region CentroByAreCodPK - Zero To Many
		
		static public esPrefetchMap Prefetch_CentroByAreCodPK
		{
			get
			{
				esPrefetchMap map = new esPrefetchMap();
				map.PrefetchDelegate = gcperu.Server.Core.DAL.GT.Area.CentroByAreCodPK_Delegate;
				map.PropertyName = "CentroByAreCodPK";
				map.MyColumnName = "are_CodPK";
				map.ParentColumnName = "are_CodPK";
				map.IsMultiPartKey = false;
				return map;
			}
		}		
		
		static private void CentroByAreCodPK_Delegate(esPrefetchParameters data)
		{
			AreaQ parent = new AreaQ(data.NextAlias());

			CentroQ me = data.You != null ? data.You as CentroQ : new CentroQ(data.NextAlias());

			if (data.Root == null)
			{
				data.Root = me;
			}
			
			data.Root.InnerJoin(parent).On(parent.AreCodPK == me.AreCodPK);

			data.You = parent;
		}			
		
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_Centros_Areas
		/// </summary>

		[XmlIgnore]
		public CentroCol CentroByAreCodPK
		{
			get
			{
				if(this._CentroByAreCodPK == null)
				{
					this._CentroByAreCodPK = new CentroCol();
					this._CentroByAreCodPK.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CentroByAreCodPK", this._CentroByAreCodPK);
				
					if (this.AreCodPK != null)
					{
						if (!this.es.IsLazyLoadDisabled)
						{
							this._CentroByAreCodPK.Query.Where(this._CentroByAreCodPK.Query.AreCodPK == this.AreCodPK);
							this._CentroByAreCodPK.Query.Load();
						}

						// Auto-hookup Foreign Keys
						this._CentroByAreCodPK.fks.Add(CentroMetadata.ColumnNames.AreCodPK, this.AreCodPK);
					}
				}

				return this._CentroByAreCodPK;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CentroByAreCodPK != null) 
				{ 
					this.RemovePostSave("CentroByAreCodPK"); 
					this._CentroByAreCodPK = null;
					this.OnPropertyChanged("CentroByAreCodPK");
				} 
			} 			
		}
			
		
		private CentroCol _CentroByAreCodPK;
		#endregion

		#region VehiculoByAreCodPK - Zero To Many
		
		static public esPrefetchMap Prefetch_VehiculoByAreCodPK
		{
			get
			{
				esPrefetchMap map = new esPrefetchMap();
				map.PrefetchDelegate = gcperu.Server.Core.DAL.GT.Area.VehiculoByAreCodPK_Delegate;
				map.PropertyName = "VehiculoByAreCodPK";
				map.MyColumnName = "are_CodPK";
				map.ParentColumnName = "are_CodPK";
				map.IsMultiPartKey = false;
				return map;
			}
		}		
		
		static private void VehiculoByAreCodPK_Delegate(esPrefetchParameters data)
		{
			AreaQ parent = new AreaQ(data.NextAlias());

			VehiculoQ me = data.You != null ? data.You as VehiculoQ : new VehiculoQ(data.NextAlias());

			if (data.Root == null)
			{
				data.Root = me;
			}
			
			data.Root.InnerJoin(parent).On(parent.AreCodPK == me.AreCodPK);

			data.You = parent;
		}			
		
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_Vehiculos_Areas
		/// </summary>

		[XmlIgnore]
		public VehiculoCol VehiculoByAreCodPK
		{
			get
			{
				if(this._VehiculoByAreCodPK == null)
				{
					this._VehiculoByAreCodPK = new VehiculoCol();
					this._VehiculoByAreCodPK.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("VehiculoByAreCodPK", this._VehiculoByAreCodPK);
				
					if (this.AreCodPK != null)
					{
						if (!this.es.IsLazyLoadDisabled)
						{
							this._VehiculoByAreCodPK.Query.Where(this._VehiculoByAreCodPK.Query.AreCodPK == this.AreCodPK);
							this._VehiculoByAreCodPK.Query.Load();
						}

						// Auto-hookup Foreign Keys
						this._VehiculoByAreCodPK.fks.Add(VehiculoMetadata.ColumnNames.AreCodPK, this.AreCodPK);
					}
				}

				return this._VehiculoByAreCodPK;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._VehiculoByAreCodPK != null) 
				{ 
					this.RemovePostSave("VehiculoByAreCodPK"); 
					this._VehiculoByAreCodPK = null;
					this.OnPropertyChanged("VehiculoByAreCodPK");
				} 
			} 			
		}
			
		
		private VehiculoCol _VehiculoByAreCodPK;
		#endregion

		
		protected override esEntityCollectionBase CreateCollectionForPrefetch(string name)
		{
			esEntityCollectionBase coll = null;

			switch (name)
			{
				case "AreaByAreCodPK":
					coll = this.AreaByAreCodPK;
					break;
				case "CentroByAreCodPK":
					coll = this.CentroByAreCodPK;
					break;
				case "VehiculoByAreCodPK":
					coll = this.VehiculoByAreCodPK;
					break;	
			}

			return coll;
		}		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AreaByAreCodPK", typeof(AreaCol), new Area()));
			props.Add(new esPropertyDescriptor(this, "CentroByAreCodPK", typeof(CentroCol), new Centro()));
			props.Add(new esPropertyDescriptor(this, "VehiculoByAreCodPK", typeof(VehiculoCol), new Vehiculo()));
		
			return props;
		}
		
	}
	



	[Serializable]
	public partial class AreaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AreaMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AreaMetadata.ColumnNames.AreCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AreaMetadata.PropertyNames.AreCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.AreDescripcion, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = AreaMetadata.PropertyNames.AreDescripcion;
			c.CharacterMaxLength = 50;
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.AreCodigo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = AreaMetadata.PropertyNames.AreCodigo;
			c.CharacterMaxLength = 2;
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.AreColor, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = AreaMetadata.PropertyNames.AreColor;
			c.CharacterMaxLength = 8;
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.AsaCodPK, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AreaMetadata.PropertyNames.AsaCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.AreEliminada, 5, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = AreaMetadata.PropertyNames.AreEliminada;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.EmpCodPK, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AreaMetadata.PropertyNames.EmpCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.PvcCodPK, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AreaMetadata.PropertyNames.PvcCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.AreRef, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AreaMetadata.PropertyNames.AreRef;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.AreFlotaGlobal, 9, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = AreaMetadata.PropertyNames.AreFlotaGlobal;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.AreEstado, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AreaMetadata.PropertyNames.AreEstado;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(AreaMetadata.ColumnNames.AreCodigoAdministracion, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = AreaMetadata.PropertyNames.AreCodigoAdministracion;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public AreaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string AreCodPK = "are_CodPK";
			 public const string AreDescripcion = "are_Descripcion";
			 public const string AreCodigo = "are_Codigo";
			 public const string AreColor = "are_Color";
			 public const string AsaCodPK = "asa_CodPK";
			 public const string AreEliminada = "are_Eliminada";
			 public const string EmpCodPK = "emp_CodPK";
			 public const string PvcCodPK = "pvc_CodPK";
			 public const string AreRef = "are_Ref";
			 public const string AreFlotaGlobal = "are_FlotaGlobal";
			 public const string AreEstado = "are_Estado";
			 public const string AreCodigoAdministracion = "are_CodigoAdministracion";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string AreCodPK = "AreCodPK";
			 public const string AreDescripcion = "AreDescripcion";
			 public const string AreCodigo = "AreCodigo";
			 public const string AreColor = "AreColor";
			 public const string AsaCodPK = "AsaCodPK";
			 public const string AreEliminada = "AreEliminada";
			 public const string EmpCodPK = "EmpCodPK";
			 public const string PvcCodPK = "PvcCodPK";
			 public const string AreRef = "AreRef";
			 public const string AreFlotaGlobal = "AreFlotaGlobal";
			 public const string AreEstado = "AreEstado";
			 public const string AreCodigoAdministracion = "AreCodigoAdministracion";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AreaMetadata))
			{
				if(AreaMetadata.mapDelegates == null)
				{
					AreaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AreaMetadata.meta == null)
				{
					AreaMetadata.meta = new AreaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("AreCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreDescripcion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("AreCodigo", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("AreColor", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("AsaCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreEliminada", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("EmpCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PvcCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreRef", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreFlotaGlobal", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("AreEstado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreCodigoAdministracion", new esTypeMap("nvarchar", "System.String"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "Areas";
				meta.Destination = "Areas";
				
				meta.spInsert = "proc_AreasInsert";				
				meta.spUpdate = "proc_AreasUpdate";		
				meta.spDelete = "proc_AreasDelete";
				meta.spLoadAll = "proc_AreasLoadAll";
				meta.spLoadByPrimaryKey = "proc_AreasLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AreaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
