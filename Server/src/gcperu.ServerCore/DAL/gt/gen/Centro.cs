
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:19
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// Encapsulates the 'Centros' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("Centro")]
	public partial class Centro : esCentro
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new Centro();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal cenCodPK)
		{
			var obj = new Centro();
			obj.CenCodPK = cenCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal cenCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new Centro();
			obj.CenCodPK = cenCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal CENCODPK)
        {
            try
            {
                var e = new Centro();
                return (e.LoadByPrimaryKey(CENCODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static Centro Get(System.Decimal CENCODPK)
        {
            try
            {
                var e = new Centro();
                return (e.LoadByPrimaryKey(CENCODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public Centro(System.Decimal CENCODPK)
        {
            this.LoadByPrimaryKey(CENCODPK);
        }
		
		public Centro()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("CentroCol")]
	public partial class CentroCol : esCentroCol, IEnumerable<Centro>
	{
	
		public CentroCol()
		{
		}

	
	
		public Centro FindByPrimaryKey(System.Decimal cenCodPK)
		{
			return this.SingleOrDefault(e => e.CenCodPK == cenCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(Centro))]
		public class CentroColWCFPacket : esCollectionWCFPacket<CentroCol>
		{
			public static implicit operator CentroCol(CentroColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator CentroColWCFPacket(CentroCol collection)
			{
				return new CentroColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class CentroQ : esCentroQ
	{
		public CentroQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public CentroQ()
		{
		}

		override protected string GetQueryName()
		{
			return "CentroQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new CentroQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(CentroQ query)
		{
			return CentroQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator CentroQ(string query)
		{
			return (CentroQ)CentroQ.SerializeHelper.FromXml(query, typeof(CentroQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esCentro : EntityBase
	{
		public esCentro()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal cenCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cenCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(cenCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal cenCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cenCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(cenCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal cenCodPK)
		{
			CentroQ query = new CentroQ();
			query.Where(query.CenCodPK == cenCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal cenCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("CenCodPK", cenCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to Centros.cen_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CenCodPK
		{
			get
			{
				return base.GetSystemDecimal(CentroMetadata.ColumnNames.CenCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(CentroMetadata.ColumnNames.CenCodPK, value))
				{
					OnPropertyChanged(CentroMetadata.PropertyNames.CenCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Centros.cen_Descripcion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String CenDescripcion
		{
			get
			{
				return base.GetSystemString(CentroMetadata.ColumnNames.CenDescripcion);
			}
			
			set
			{
				if(base.SetSystemString(CentroMetadata.ColumnNames.CenDescripcion, value))
				{
					OnPropertyChanged(CentroMetadata.PropertyNames.CenDescripcion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Centros.are_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreCodPK
		{
			get
			{
				return base.GetSystemDecimal(CentroMetadata.ColumnNames.AreCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(CentroMetadata.ColumnNames.AreCodPK, value))
				{
					this._UpToAreaByAreCodPK = null;
					this.OnPropertyChanged("UpToAreaByAreCodPK");
					OnPropertyChanged(CentroMetadata.PropertyNames.AreCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Centros.cen_Eliminado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? CenEliminado
		{
			get
			{
				return base.GetSystemBoolean(CentroMetadata.ColumnNames.CenEliminado);
			}
			
			set
			{
				if(base.SetSystemBoolean(CentroMetadata.ColumnNames.CenEliminado, value))
				{
					OnPropertyChanged(CentroMetadata.PropertyNames.CenEliminado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Centros.cen_Ref
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CenRef
		{
			get
			{
				return base.GetSystemDecimal(CentroMetadata.ColumnNames.CenRef);
			}
			
			set
			{
				if(base.SetSystemDecimal(CentroMetadata.ColumnNames.CenRef, value))
				{
					OnPropertyChanged(CentroMetadata.PropertyNames.CenRef);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Centros.cen_Estado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CenEstado
		{
			get
			{
				return base.GetSystemDecimal(CentroMetadata.ColumnNames.CenEstado);
			}
			
			set
			{
				if(base.SetSystemDecimal(CentroMetadata.ColumnNames.CenEstado, value))
				{
					OnPropertyChanged(CentroMetadata.PropertyNames.CenEstado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Centros.ZonaTrabajoId
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ZonaTrabajoId
		{
			get
			{
				return base.GetSystemDecimal(CentroMetadata.ColumnNames.ZonaTrabajoId);
			}
			
			set
			{
				if(base.SetSystemDecimal(CentroMetadata.ColumnNames.ZonaTrabajoId, value))
				{
					OnPropertyChanged(CentroMetadata.PropertyNames.ZonaTrabajoId);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Centros.CentroIdProgramado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CentroIdProgramado
		{
			get
			{
				return base.GetSystemDecimal(CentroMetadata.ColumnNames.CentroIdProgramado);
			}
			
			set
			{
				if(base.SetSystemDecimal(CentroMetadata.ColumnNames.CentroIdProgramado, value))
				{
					OnPropertyChanged(CentroMetadata.PropertyNames.CentroIdProgramado);
				}
			}
		}		
		
		[CLSCompliant(false)]
		internal protected Area _UpToAreaByAreCodPK;
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CenCodPK": this.str().CenCodPK = (string)value; break;							
						case "CenDescripcion": this.str().CenDescripcion = (string)value; break;							
						case "AreCodPK": this.str().AreCodPK = (string)value; break;							
						case "CenEliminado": this.str().CenEliminado = (string)value; break;							
						case "CenRef": this.str().CenRef = (string)value; break;							
						case "CenEstado": this.str().CenEstado = (string)value; break;							
						case "ZonaTrabajoId": this.str().ZonaTrabajoId = (string)value; break;							
						case "CentroIdProgramado": this.str().CentroIdProgramado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "CenCodPK":
						
							if (value == null || value is System.Decimal)
								this.CenCodPK = (System.Decimal?)value;
								OnPropertyChanged(CentroMetadata.PropertyNames.CenCodPK);
							break;
						
						case "AreCodPK":
						
							if (value == null || value is System.Decimal)
								this.AreCodPK = (System.Decimal?)value;
								OnPropertyChanged(CentroMetadata.PropertyNames.AreCodPK);
							break;
						
						case "CenEliminado":
						
							if (value == null || value is System.Boolean)
								this.CenEliminado = (System.Boolean?)value;
								OnPropertyChanged(CentroMetadata.PropertyNames.CenEliminado);
							break;
						
						case "CenRef":
						
							if (value == null || value is System.Decimal)
								this.CenRef = (System.Decimal?)value;
								OnPropertyChanged(CentroMetadata.PropertyNames.CenRef);
							break;
						
						case "CenEstado":
						
							if (value == null || value is System.Decimal)
								this.CenEstado = (System.Decimal?)value;
								OnPropertyChanged(CentroMetadata.PropertyNames.CenEstado);
							break;
						
						case "ZonaTrabajoId":
						
							if (value == null || value is System.Decimal)
								this.ZonaTrabajoId = (System.Decimal?)value;
								OnPropertyChanged(CentroMetadata.PropertyNames.ZonaTrabajoId);
							break;
						
						case "CentroIdProgramado":
						
							if (value == null || value is System.Decimal)
								this.CentroIdProgramado = (System.Decimal?)value;
								OnPropertyChanged(CentroMetadata.PropertyNames.CentroIdProgramado);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esCentro entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CenCodPK
			{
				get
				{
					System.Decimal? data = entity.CenCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CenCodPK = null;
					else entity.CenCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String CenDescripcion
			{
				get
				{
					System.String data = entity.CenDescripcion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CenDescripcion = null;
					else entity.CenDescripcion = Convert.ToString(value);
				}
			}
				
			public System.String AreCodPK
			{
				get
				{
					System.Decimal? data = entity.AreCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreCodPK = null;
					else entity.AreCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String CenEliminado
			{
				get
				{
					System.Boolean? data = entity.CenEliminado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CenEliminado = null;
					else entity.CenEliminado = Convert.ToBoolean(value);
				}
			}
				
			public System.String CenRef
			{
				get
				{
					System.Decimal? data = entity.CenRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CenRef = null;
					else entity.CenRef = Convert.ToDecimal(value);
				}
			}
				
			public System.String CenEstado
			{
				get
				{
					System.Decimal? data = entity.CenEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CenEstado = null;
					else entity.CenEstado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ZonaTrabajoId
			{
				get
				{
					System.Decimal? data = entity.ZonaTrabajoId;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ZonaTrabajoId = null;
					else entity.ZonaTrabajoId = Convert.ToDecimal(value);
				}
			}
				
			public System.String CentroIdProgramado
			{
				get
				{
					System.Decimal? data = entity.CentroIdProgramado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CentroIdProgramado = null;
					else entity.CentroIdProgramado = Convert.ToDecimal(value);
				}
			}
			

			private esCentro entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return CentroMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public CentroQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CentroQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(CentroQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(CentroQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private CentroQ query;		
	}



	[Serializable]
	abstract public partial class esCentroCol : CollectionBase<Centro>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CentroMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "CentroCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public CentroQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CentroQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(CentroQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CentroQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(CentroQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((CentroQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private CentroQ query;
	}



	[Serializable]
	abstract public partial class esCentroQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return CentroMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(CentroMetadata.ColumnNames.CenCodPK,new esQueryItem(this, CentroMetadata.ColumnNames.CenCodPK, esSystemType.Decimal));
			_queryItems.Add(CentroMetadata.ColumnNames.CenDescripcion,new esQueryItem(this, CentroMetadata.ColumnNames.CenDescripcion, esSystemType.String));
			_queryItems.Add(CentroMetadata.ColumnNames.AreCodPK,new esQueryItem(this, CentroMetadata.ColumnNames.AreCodPK, esSystemType.Decimal));
			_queryItems.Add(CentroMetadata.ColumnNames.CenEliminado,new esQueryItem(this, CentroMetadata.ColumnNames.CenEliminado, esSystemType.Boolean));
			_queryItems.Add(CentroMetadata.ColumnNames.CenRef,new esQueryItem(this, CentroMetadata.ColumnNames.CenRef, esSystemType.Decimal));
			_queryItems.Add(CentroMetadata.ColumnNames.CenEstado,new esQueryItem(this, CentroMetadata.ColumnNames.CenEstado, esSystemType.Decimal));
			_queryItems.Add(CentroMetadata.ColumnNames.ZonaTrabajoId,new esQueryItem(this, CentroMetadata.ColumnNames.ZonaTrabajoId, esSystemType.Decimal));
			_queryItems.Add(CentroMetadata.ColumnNames.CentroIdProgramado,new esQueryItem(this, CentroMetadata.ColumnNames.CentroIdProgramado, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem CenCodPK
		{
			get { return new esQueryItem(this, CentroMetadata.ColumnNames.CenCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem CenDescripcion
		{
			get { return new esQueryItem(this, CentroMetadata.ColumnNames.CenDescripcion, esSystemType.String); }
		} 
		
		public esQueryItem AreCodPK
		{
			get { return new esQueryItem(this, CentroMetadata.ColumnNames.AreCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem CenEliminado
		{
			get { return new esQueryItem(this, CentroMetadata.ColumnNames.CenEliminado, esSystemType.Boolean); }
		} 
		
		public esQueryItem CenRef
		{
			get { return new esQueryItem(this, CentroMetadata.ColumnNames.CenRef, esSystemType.Decimal); }
		} 
		
		public esQueryItem CenEstado
		{
			get { return new esQueryItem(this, CentroMetadata.ColumnNames.CenEstado, esSystemType.Decimal); }
		} 
		
		public esQueryItem ZonaTrabajoId
		{
			get { return new esQueryItem(this, CentroMetadata.ColumnNames.ZonaTrabajoId, esSystemType.Decimal); }
		} 
		
		public esQueryItem CentroIdProgramado
		{
			get { return new esQueryItem(this, CentroMetadata.ColumnNames.CentroIdProgramado, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class Centro : esCentro
	{

		#region VehiculoByCenCodPK - Zero To Many
		
		static public esPrefetchMap Prefetch_VehiculoByCenCodPK
		{
			get
			{
				esPrefetchMap map = new esPrefetchMap();
				map.PrefetchDelegate = gcperu.Server.Core.DAL.GT.Centro.VehiculoByCenCodPK_Delegate;
				map.PropertyName = "VehiculoByCenCodPK";
				map.MyColumnName = "cen_CodPK";
				map.ParentColumnName = "cen_CodPK";
				map.IsMultiPartKey = false;
				return map;
			}
		}		
		
		static private void VehiculoByCenCodPK_Delegate(esPrefetchParameters data)
		{
			CentroQ parent = new CentroQ(data.NextAlias());

			VehiculoQ me = data.You != null ? data.You as VehiculoQ : new VehiculoQ(data.NextAlias());

			if (data.Root == null)
			{
				data.Root = me;
			}
			
			data.Root.InnerJoin(parent).On(parent.CenCodPK == me.CenCodPK);

			data.You = parent;
		}			
		
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_Vehiculos_Centros
		/// </summary>

		[XmlIgnore]
		public VehiculoCol VehiculoByCenCodPK
		{
			get
			{
				if(this._VehiculoByCenCodPK == null)
				{
					this._VehiculoByCenCodPK = new VehiculoCol();
					this._VehiculoByCenCodPK.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("VehiculoByCenCodPK", this._VehiculoByCenCodPK);
				
					if (this.CenCodPK != null)
					{
						if (!this.es.IsLazyLoadDisabled)
						{
							this._VehiculoByCenCodPK.Query.Where(this._VehiculoByCenCodPK.Query.CenCodPK == this.CenCodPK);
							this._VehiculoByCenCodPK.Query.Load();
						}

						// Auto-hookup Foreign Keys
						this._VehiculoByCenCodPK.fks.Add(VehiculoMetadata.ColumnNames.CenCodPK, this.CenCodPK);
					}
				}

				return this._VehiculoByCenCodPK;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._VehiculoByCenCodPK != null) 
				{ 
					this.RemovePostSave("VehiculoByCenCodPK"); 
					this._VehiculoByCenCodPK = null;
					this.OnPropertyChanged("VehiculoByCenCodPK");
				} 
			} 			
		}
			
		
		private VehiculoCol _VehiculoByCenCodPK;
		#endregion

				
		#region UpToAreaByAreCodPK - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_Centros_Areas
		/// </summary>

		[XmlIgnore]
					
		public Area UpToAreaByAreCodPK
		{
			get
			{
				if (this.es.IsLazyLoadDisabled) return null;
				
				if(this._UpToAreaByAreCodPK == null && AreCodPK != null)
				{
					this._UpToAreaByAreCodPK = new Area();
					this._UpToAreaByAreCodPK.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAreaByAreCodPK", this._UpToAreaByAreCodPK);
					this._UpToAreaByAreCodPK.Query.Where(this._UpToAreaByAreCodPK.Query.AreCodPK == this.AreCodPK);
					this._UpToAreaByAreCodPK.Query.Load();
				}	
				return this._UpToAreaByAreCodPK;
			}
			
			set
			{
				this.RemovePreSave("UpToAreaByAreCodPK");
				
				bool changed = this._UpToAreaByAreCodPK != value;

				if(value == null)
				{
					this.AreCodPK = null;
					this._UpToAreaByAreCodPK = null;
				}
				else
				{
					this.AreCodPK = value.AreCodPK;
					this._UpToAreaByAreCodPK = value;
					this.SetPreSave("UpToAreaByAreCodPK", this._UpToAreaByAreCodPK);
				}
				
				if( changed )
				{
					this.OnPropertyChanged("UpToAreaByAreCodPK");
				}
			}
		}
		#endregion
		

		
		protected override esEntityCollectionBase CreateCollectionForPrefetch(string name)
		{
			esEntityCollectionBase coll = null;

			switch (name)
			{
				case "VehiculoByCenCodPK":
					coll = this.VehiculoByCenCodPK;
					break;	
			}

			return coll;
		}		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "VehiculoByCenCodPK", typeof(VehiculoCol), new Vehiculo()));
		
			return props;
		}
		
	}
	



	[Serializable]
	public partial class CentroMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CentroMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CentroMetadata.ColumnNames.CenCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CentroMetadata.PropertyNames.CenCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(CentroMetadata.ColumnNames.CenDescripcion, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CentroMetadata.PropertyNames.CenDescripcion;
			c.CharacterMaxLength = 80;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(CentroMetadata.ColumnNames.AreCodPK, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CentroMetadata.PropertyNames.AreCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(CentroMetadata.ColumnNames.CenEliminado, 3, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = CentroMetadata.PropertyNames.CenEliminado;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(CentroMetadata.ColumnNames.CenRef, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CentroMetadata.PropertyNames.CenRef;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(CentroMetadata.ColumnNames.CenEstado, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CentroMetadata.PropertyNames.CenEstado;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(CentroMetadata.ColumnNames.ZonaTrabajoId, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CentroMetadata.PropertyNames.ZonaTrabajoId;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(CentroMetadata.ColumnNames.CentroIdProgramado, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CentroMetadata.PropertyNames.CentroIdProgramado;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public CentroMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CenCodPK = "cen_CodPK";
			 public const string CenDescripcion = "cen_Descripcion";
			 public const string AreCodPK = "are_CodPK";
			 public const string CenEliminado = "cen_Eliminado";
			 public const string CenRef = "cen_Ref";
			 public const string CenEstado = "cen_Estado";
			 public const string ZonaTrabajoId = "ZonaTrabajoId";
			 public const string CentroIdProgramado = "CentroIdProgramado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CenCodPK = "CenCodPK";
			 public const string CenDescripcion = "CenDescripcion";
			 public const string AreCodPK = "AreCodPK";
			 public const string CenEliminado = "CenEliminado";
			 public const string CenRef = "CenRef";
			 public const string CenEstado = "CenEstado";
			 public const string ZonaTrabajoId = "ZonaTrabajoId";
			 public const string CentroIdProgramado = "CentroIdProgramado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CentroMetadata))
			{
				if(CentroMetadata.mapDelegates == null)
				{
					CentroMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CentroMetadata.meta == null)
				{
					CentroMetadata.meta = new CentroMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("CenCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("CenDescripcion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("AreCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("CenEliminado", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("CenRef", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("CenEstado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ZonaTrabajoId", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("CentroIdProgramado", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "Centros";
				meta.Destination = "Centros";
				
				meta.spInsert = "proc_CentrosInsert";				
				meta.spUpdate = "proc_CentrosUpdate";		
				meta.spDelete = "proc_CentrosDelete";
				meta.spLoadAll = "proc_CentrosLoadAll";
				meta.spLoadByPrimaryKey = "proc_CentrosLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CentroMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
