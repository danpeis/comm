
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// Encapsulates the 'Empresas' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("Empresas")]
	public partial class Empresas : esEmpresas
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new Empresas();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal empCodPK)
		{
			var obj = new Empresas();
			obj.EmpCodPK = empCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal empCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new Empresas();
			obj.EmpCodPK = empCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal EMPCODPK)
        {
            try
            {
                var e = new Empresas();
                return (e.LoadByPrimaryKey(EMPCODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static Empresas Get(System.Decimal EMPCODPK)
        {
            try
            {
                var e = new Empresas();
                return (e.LoadByPrimaryKey(EMPCODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public Empresas(System.Decimal EMPCODPK)
        {
            this.LoadByPrimaryKey(EMPCODPK);
        }
		
		public Empresas()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("EmpresasCol")]
	public partial class EmpresasCol : esEmpresasCol, IEnumerable<Empresas>
	{
	
		public EmpresasCol()
		{
		}

	
	
		public Empresas FindByPrimaryKey(System.Decimal empCodPK)
		{
			return this.SingleOrDefault(e => e.EmpCodPK == empCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(Empresas))]
		public class EmpresasColWCFPacket : esCollectionWCFPacket<EmpresasCol>
		{
			public static implicit operator EmpresasCol(EmpresasColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator EmpresasColWCFPacket(EmpresasCol collection)
			{
				return new EmpresasColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class EmpresasQ : esEmpresasQ
	{
		public EmpresasQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public EmpresasQ()
		{
		}

		override protected string GetQueryName()
		{
			return "EmpresasQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new EmpresasQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(EmpresasQ query)
		{
			return EmpresasQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator EmpresasQ(string query)
		{
			return (EmpresasQ)EmpresasQ.SerializeHelper.FromXml(query, typeof(EmpresasQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esEmpresas : EntityBase
	{
		public esEmpresas()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal empCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(empCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(empCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal empCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(empCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(empCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal empCodPK)
		{
			EmpresasQ query = new EmpresasQ();
			query.Where(query.EmpCodPK == empCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal empCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("EmpCodPK", empCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to Empresas.emp_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCodPK
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCodPK, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Nombre
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpNombre
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpNombre);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpNombre, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpNombre);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_CifNif
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpCifNif
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpCifNif);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpCifNif, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCifNif);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Administrador
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpAdministrador
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpAdministrador);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpAdministrador, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpAdministrador);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Domicilio
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpDomicilio
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpDomicilio);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpDomicilio, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpDomicilio);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_CP
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpCP
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpCP);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpCP, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCP);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Telefono
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpTelefono
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpTelefono);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpTelefono, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTelefono);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Movil
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpMovil
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpMovil);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpMovil, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpMovil);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Fax
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpFax
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpFax);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpFax, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFax);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Email
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpEmail
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpEmail);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpEmail, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpEmail);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_WWW
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpWWW
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpWWW);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpWWW, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpWWW);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.lcd_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? LcdCodPK
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.LcdCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.LcdCodPK, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.LcdCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.pvc_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PvcCodPK
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.PvcCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.PvcCodPK, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.PvcCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ref
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpRef
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpRef);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpRef, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpRef);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Eliminada
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? EmpEliminada
		{
			get
			{
				return base.GetSystemBoolean(EmpresasMetadata.ColumnNames.EmpEliminada);
			}
			
			set
			{
				if(base.SetSystemBoolean(EmpresasMetadata.ColumnNames.EmpEliminada, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpEliminada);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_HeD
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpHeD
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeD);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeD, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeD);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_HeC
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpHeC
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeC);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeC, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeC);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_HeRH
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpHeRH
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeRH);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeRH, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeRH);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_HeT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpHeT
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeT);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeT, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_HeO
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpHeO
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeO);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpHeO, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeO);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_VehPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpVehPS
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpVehPS);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpVehPS, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpVehPS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_VehPT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpVehPT
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpVehPT);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpVehPT, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpVehPT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ModoTrabajo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpModoTrabajo
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpModoTrabajo);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpModoTrabajo, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpModoTrabajo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_TpFactSES
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpTpFactSES
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTpFactSES);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTpFactSES, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTpFactSES);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_KmPoblacion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpKmPoblacion
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpKmPoblacion);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpKmPoblacion, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpKmPoblacion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_PacienteCnt
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpPacienteCnt
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpPacienteCnt);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpPacienteCnt, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpPacienteCnt);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_TurnoTrabConductor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpTurnoTrabConductor
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTurnoTrabConductor);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTurnoTrabConductor, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTurnoTrabConductor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_FacturaSerie1
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpFacturaSerie1
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpFacturaSerie1);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpFacturaSerie1, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaSerie1);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_FacturaSerie2
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpFacturaSerie2
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpFacturaSerie2);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpFacturaSerie2, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaSerie2);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_FacturaNumero1
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpFacturaNumero1
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpFacturaNumero1);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpFacturaNumero1, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaNumero1);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_FacturaNumero2
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpFacturaNumero2
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpFacturaNumero2);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpFacturaNumero2, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaNumero2);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_FacturaFecha1
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? EmpFacturaFecha1
		{
			get
			{
				return base.GetSystemDateTime(EmpresasMetadata.ColumnNames.EmpFacturaFecha1);
			}
			
			set
			{
				if(base.SetSystemDateTime(EmpresasMetadata.ColumnNames.EmpFacturaFecha1, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaFecha1);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_FacturaFecha2
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? EmpFacturaFecha2
		{
			get
			{
				return base.GetSystemDateTime(EmpresasMetadata.ColumnNames.EmpFacturaFecha2);
			}
			
			set
			{
				if(base.SetSystemDateTime(EmpresasMetadata.ColumnNames.EmpFacturaFecha2, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaFecha2);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_PrecisionU
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpPrecisionU
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpPrecisionU);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpPrecisionU, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpPrecisionU);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_PrecisionI
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpPrecisionI
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpPrecisionI);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpPrecisionI, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpPrecisionI);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.ttr_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TtrCodPK
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.TtrCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.TtrCodPK, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.TtrCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_HorasJornadaNorma
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpHorasJornadaNorma
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpHorasJornadaNorma);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpHorasJornadaNorma, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHorasJornadaNorma);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_HorasJornadaNormal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpHorasJornadaNormal
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpHorasJornadaNormal);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpHorasJornadaNormal, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHorasJornadaNormal);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_VehPC
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpVehPC
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpVehPC);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpVehPC, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpVehPC);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ParametrosxDefecto
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? EmpParametrosxDefecto
		{
			get
			{
				return base.GetSystemBoolean(EmpresasMetadata.ColumnNames.EmpParametrosxDefecto);
			}
			
			set
			{
				if(base.SetSystemBoolean(EmpresasMetadata.ColumnNames.EmpParametrosxDefecto, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpParametrosxDefecto);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ModoTrabajoFacturacion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpModoTrabajoFacturacion
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpModoTrabajoFacturacion);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpModoTrabajoFacturacion, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpModoTrabajoFacturacion);
				}
			}
		}		
		
		/// <summary>
		/// P=80 M=67 C=77
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? EmpEstadoDocumentacion
		{
			get
			{
				return base.GetSystemInt16(EmpresasMetadata.ColumnNames.EmpEstadoDocumentacion);
			}
			
			set
			{
				if(base.SetSystemInt16(EmpresasMetadata.ColumnNames.EmpEstadoDocumentacion, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpEstadoDocumentacion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_NombreCMPSP
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpNombreCMPSP
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpNombreCMPSP);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpNombreCMPSP, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpNombreCMPSP);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Color
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpColor
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpColor);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpColor, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpColor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Estado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpEstado
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpEstado);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpEstado, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpEstado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_EstadoCaduca
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? EmpEstadoCaduca
		{
			get
			{
				return base.GetSystemInt16(EmpresasMetadata.ColumnNames.EmpEstadoCaduca);
			}
			
			set
			{
				if(base.SetSystemInt16(EmpresasMetadata.ColumnNames.EmpEstadoCaduca, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpEstadoCaduca);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Documentar
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? EmpDocumentar
		{
			get
			{
				return base.GetSystemBoolean(EmpresasMetadata.ColumnNames.EmpDocumentar);
			}
			
			set
			{
				if(base.SetSystemBoolean(EmpresasMetadata.ColumnNames.EmpDocumentar, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpDocumentar);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Gestion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? EmpGestion
		{
			get
			{
				return base.GetSystemBoolean(EmpresasMetadata.ColumnNames.EmpGestion);
			}
			
			set
			{
				if(base.SetSystemBoolean(EmpresasMetadata.ColumnNames.EmpGestion, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpGestion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_CaracteristicasFSRV
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCaracteristicasFSRV
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasFSRV);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasFSRV, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasFSRV);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_CaracteristicasFPCT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCaracteristicasFPCT
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasFPCT);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasFPCT, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasFPCT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_CaracteristicasGMDT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCaracteristicasGMDT
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasGMDT);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasGMDT, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasGMDT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_CaracteristicasDSRV
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCaracteristicasDSRV
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasDSRV);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasDSRV, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasDSRV);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_HoraEnvio
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpHoraEnvio
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpHoraEnvio);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpHoraEnvio, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHoraEnvio);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_CaracteristicasINC
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCaracteristicasINC
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasINC);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasINC, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasINC);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_CaracteristicasVSD
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCaracteristicasVSD
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasVSD);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpCaracteristicasVSD, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasVSD);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_Inscripcion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EmpInscripcion
		{
			get
			{
				return base.GetSystemString(EmpresasMetadata.ColumnNames.EmpInscripcion);
			}
			
			set
			{
				if(base.SetSystemString(EmpresasMetadata.ColumnNames.EmpInscripcion, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpInscripcion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ControlNumeroSesiones
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpControlNumeroSesiones
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlNumeroSesiones);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlNumeroSesiones, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlNumeroSesiones);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ControlDiasFinTratamiento
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpControlDiasFinTratamiento
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlDiasFinTratamiento);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlDiasFinTratamiento, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlDiasFinTratamiento);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_FacturacionAutomatizada
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? EmpFacturacionAutomatizada
		{
			get
			{
				return base.GetSystemBoolean(EmpresasMetadata.ColumnNames.EmpFacturacionAutomatizada);
			}
			
			set
			{
				if(base.SetSystemBoolean(EmpresasMetadata.ColumnNames.EmpFacturacionAutomatizada, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturacionAutomatizada);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ControlCalidadTiempoMaximo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpControlCalidadTiempoMaximo
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMaximo);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMaximo, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlCalidadTiempoMaximo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ControlCalidadTiempoMargen
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpControlCalidadTiempoMargen
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMargen);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMargen, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlCalidadTiempoMargen);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ControlCalidadServicios
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpControlCalidadServicios
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlCalidadServicios);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlCalidadServicios, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlCalidadServicios);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_ControlCalidadHora
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpControlCalidadHora
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlCalidadHora);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpControlCalidadHora, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlCalidadHora);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_TurnoTrabajoVehiculoM
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpTurnoTrabajoVehiculoM
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoM);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoM, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTurnoTrabajoVehiculoM);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_TurnoTrabajoVehiculoT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpTurnoTrabajoVehiculoT
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoT);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoT, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTurnoTrabajoVehiculoT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Empresas.emp_TurnoTrabajoVehiculoN
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpTurnoTrabajoVehiculoN
		{
			get
			{
				return base.GetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoN);
			}
			
			set
			{
				if(base.SetSystemDecimal(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoN, value))
				{
					OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTurnoTrabajoVehiculoN);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "EmpCodPK": this.str().EmpCodPK = (string)value; break;							
						case "EmpNombre": this.str().EmpNombre = (string)value; break;							
						case "EmpCifNif": this.str().EmpCifNif = (string)value; break;							
						case "EmpAdministrador": this.str().EmpAdministrador = (string)value; break;							
						case "EmpDomicilio": this.str().EmpDomicilio = (string)value; break;							
						case "EmpCP": this.str().EmpCP = (string)value; break;							
						case "EmpTelefono": this.str().EmpTelefono = (string)value; break;							
						case "EmpMovil": this.str().EmpMovil = (string)value; break;							
						case "EmpFax": this.str().EmpFax = (string)value; break;							
						case "EmpEmail": this.str().EmpEmail = (string)value; break;							
						case "EmpWWW": this.str().EmpWWW = (string)value; break;							
						case "LcdCodPK": this.str().LcdCodPK = (string)value; break;							
						case "PvcCodPK": this.str().PvcCodPK = (string)value; break;							
						case "EmpRef": this.str().EmpRef = (string)value; break;							
						case "EmpEliminada": this.str().EmpEliminada = (string)value; break;							
						case "EmpHeD": this.str().EmpHeD = (string)value; break;							
						case "EmpHeC": this.str().EmpHeC = (string)value; break;							
						case "EmpHeRH": this.str().EmpHeRH = (string)value; break;							
						case "EmpHeT": this.str().EmpHeT = (string)value; break;							
						case "EmpHeO": this.str().EmpHeO = (string)value; break;							
						case "EmpVehPS": this.str().EmpVehPS = (string)value; break;							
						case "EmpVehPT": this.str().EmpVehPT = (string)value; break;							
						case "EmpModoTrabajo": this.str().EmpModoTrabajo = (string)value; break;							
						case "EmpTpFactSES": this.str().EmpTpFactSES = (string)value; break;							
						case "EmpKmPoblacion": this.str().EmpKmPoblacion = (string)value; break;							
						case "EmpPacienteCnt": this.str().EmpPacienteCnt = (string)value; break;							
						case "EmpTurnoTrabConductor": this.str().EmpTurnoTrabConductor = (string)value; break;							
						case "EmpFacturaSerie1": this.str().EmpFacturaSerie1 = (string)value; break;							
						case "EmpFacturaSerie2": this.str().EmpFacturaSerie2 = (string)value; break;							
						case "EmpFacturaNumero1": this.str().EmpFacturaNumero1 = (string)value; break;							
						case "EmpFacturaNumero2": this.str().EmpFacturaNumero2 = (string)value; break;							
						case "EmpFacturaFecha1": this.str().EmpFacturaFecha1 = (string)value; break;							
						case "EmpFacturaFecha2": this.str().EmpFacturaFecha2 = (string)value; break;							
						case "EmpPrecisionU": this.str().EmpPrecisionU = (string)value; break;							
						case "EmpPrecisionI": this.str().EmpPrecisionI = (string)value; break;							
						case "TtrCodPK": this.str().TtrCodPK = (string)value; break;							
						case "EmpHorasJornadaNorma": this.str().EmpHorasJornadaNorma = (string)value; break;							
						case "EmpHorasJornadaNormal": this.str().EmpHorasJornadaNormal = (string)value; break;							
						case "EmpVehPC": this.str().EmpVehPC = (string)value; break;							
						case "EmpParametrosxDefecto": this.str().EmpParametrosxDefecto = (string)value; break;							
						case "EmpModoTrabajoFacturacion": this.str().EmpModoTrabajoFacturacion = (string)value; break;							
						case "EmpEstadoDocumentacion": this.str().EmpEstadoDocumentacion = (string)value; break;							
						case "EmpNombreCMPSP": this.str().EmpNombreCMPSP = (string)value; break;							
						case "EmpColor": this.str().EmpColor = (string)value; break;							
						case "EmpEstado": this.str().EmpEstado = (string)value; break;							
						case "EmpEstadoCaduca": this.str().EmpEstadoCaduca = (string)value; break;							
						case "EmpDocumentar": this.str().EmpDocumentar = (string)value; break;							
						case "EmpGestion": this.str().EmpGestion = (string)value; break;							
						case "EmpCaracteristicasFSRV": this.str().EmpCaracteristicasFSRV = (string)value; break;							
						case "EmpCaracteristicasFPCT": this.str().EmpCaracteristicasFPCT = (string)value; break;							
						case "EmpCaracteristicasGMDT": this.str().EmpCaracteristicasGMDT = (string)value; break;							
						case "EmpCaracteristicasDSRV": this.str().EmpCaracteristicasDSRV = (string)value; break;							
						case "EmpHoraEnvio": this.str().EmpHoraEnvio = (string)value; break;							
						case "EmpCaracteristicasINC": this.str().EmpCaracteristicasINC = (string)value; break;							
						case "EmpCaracteristicasVSD": this.str().EmpCaracteristicasVSD = (string)value; break;							
						case "EmpInscripcion": this.str().EmpInscripcion = (string)value; break;							
						case "EmpControlNumeroSesiones": this.str().EmpControlNumeroSesiones = (string)value; break;							
						case "EmpControlDiasFinTratamiento": this.str().EmpControlDiasFinTratamiento = (string)value; break;							
						case "EmpFacturacionAutomatizada": this.str().EmpFacturacionAutomatizada = (string)value; break;							
						case "EmpControlCalidadTiempoMaximo": this.str().EmpControlCalidadTiempoMaximo = (string)value; break;							
						case "EmpControlCalidadTiempoMargen": this.str().EmpControlCalidadTiempoMargen = (string)value; break;							
						case "EmpControlCalidadServicios": this.str().EmpControlCalidadServicios = (string)value; break;							
						case "EmpControlCalidadHora": this.str().EmpControlCalidadHora = (string)value; break;							
						case "EmpTurnoTrabajoVehiculoM": this.str().EmpTurnoTrabajoVehiculoM = (string)value; break;							
						case "EmpTurnoTrabajoVehiculoT": this.str().EmpTurnoTrabajoVehiculoT = (string)value; break;							
						case "EmpTurnoTrabajoVehiculoN": this.str().EmpTurnoTrabajoVehiculoN = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "EmpCodPK":
						
							if (value == null || value is System.Decimal)
								this.EmpCodPK = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCodPK);
							break;
						
						case "LcdCodPK":
						
							if (value == null || value is System.Decimal)
								this.LcdCodPK = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.LcdCodPK);
							break;
						
						case "PvcCodPK":
						
							if (value == null || value is System.Decimal)
								this.PvcCodPK = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.PvcCodPK);
							break;
						
						case "EmpRef":
						
							if (value == null || value is System.Decimal)
								this.EmpRef = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpRef);
							break;
						
						case "EmpEliminada":
						
							if (value == null || value is System.Boolean)
								this.EmpEliminada = (System.Boolean?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpEliminada);
							break;
						
						case "EmpHeD":
						
							if (value == null || value is System.Decimal)
								this.EmpHeD = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeD);
							break;
						
						case "EmpHeC":
						
							if (value == null || value is System.Decimal)
								this.EmpHeC = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeC);
							break;
						
						case "EmpHeRH":
						
							if (value == null || value is System.Decimal)
								this.EmpHeRH = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeRH);
							break;
						
						case "EmpHeT":
						
							if (value == null || value is System.Decimal)
								this.EmpHeT = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeT);
							break;
						
						case "EmpHeO":
						
							if (value == null || value is System.Decimal)
								this.EmpHeO = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpHeO);
							break;
						
						case "EmpVehPS":
						
							if (value == null || value is System.Decimal)
								this.EmpVehPS = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpVehPS);
							break;
						
						case "EmpVehPT":
						
							if (value == null || value is System.Decimal)
								this.EmpVehPT = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpVehPT);
							break;
						
						case "EmpModoTrabajo":
						
							if (value == null || value is System.Decimal)
								this.EmpModoTrabajo = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpModoTrabajo);
							break;
						
						case "EmpTpFactSES":
						
							if (value == null || value is System.Decimal)
								this.EmpTpFactSES = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTpFactSES);
							break;
						
						case "EmpKmPoblacion":
						
							if (value == null || value is System.Decimal)
								this.EmpKmPoblacion = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpKmPoblacion);
							break;
						
						case "EmpPacienteCnt":
						
							if (value == null || value is System.Decimal)
								this.EmpPacienteCnt = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpPacienteCnt);
							break;
						
						case "EmpTurnoTrabConductor":
						
							if (value == null || value is System.Decimal)
								this.EmpTurnoTrabConductor = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTurnoTrabConductor);
							break;
						
						case "EmpFacturaNumero1":
						
							if (value == null || value is System.Decimal)
								this.EmpFacturaNumero1 = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaNumero1);
							break;
						
						case "EmpFacturaNumero2":
						
							if (value == null || value is System.Decimal)
								this.EmpFacturaNumero2 = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaNumero2);
							break;
						
						case "EmpFacturaFecha1":
						
							if (value == null || value is System.DateTime)
								this.EmpFacturaFecha1 = (System.DateTime?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaFecha1);
							break;
						
						case "EmpFacturaFecha2":
						
							if (value == null || value is System.DateTime)
								this.EmpFacturaFecha2 = (System.DateTime?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturaFecha2);
							break;
						
						case "EmpPrecisionU":
						
							if (value == null || value is System.Decimal)
								this.EmpPrecisionU = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpPrecisionU);
							break;
						
						case "EmpPrecisionI":
						
							if (value == null || value is System.Decimal)
								this.EmpPrecisionI = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpPrecisionI);
							break;
						
						case "TtrCodPK":
						
							if (value == null || value is System.Decimal)
								this.TtrCodPK = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.TtrCodPK);
							break;
						
						case "EmpVehPC":
						
							if (value == null || value is System.Decimal)
								this.EmpVehPC = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpVehPC);
							break;
						
						case "EmpParametrosxDefecto":
						
							if (value == null || value is System.Boolean)
								this.EmpParametrosxDefecto = (System.Boolean?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpParametrosxDefecto);
							break;
						
						case "EmpModoTrabajoFacturacion":
						
							if (value == null || value is System.Decimal)
								this.EmpModoTrabajoFacturacion = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpModoTrabajoFacturacion);
							break;
						
						case "EmpEstadoDocumentacion":
						
							if (value == null || value is System.Int16)
								this.EmpEstadoDocumentacion = (System.Int16?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpEstadoDocumentacion);
							break;
						
						case "EmpColor":
						
							if (value == null || value is System.Decimal)
								this.EmpColor = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpColor);
							break;
						
						case "EmpEstado":
						
							if (value == null || value is System.Decimal)
								this.EmpEstado = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpEstado);
							break;
						
						case "EmpEstadoCaduca":
						
							if (value == null || value is System.Int16)
								this.EmpEstadoCaduca = (System.Int16?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpEstadoCaduca);
							break;
						
						case "EmpDocumentar":
						
							if (value == null || value is System.Boolean)
								this.EmpDocumentar = (System.Boolean?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpDocumentar);
							break;
						
						case "EmpGestion":
						
							if (value == null || value is System.Boolean)
								this.EmpGestion = (System.Boolean?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpGestion);
							break;
						
						case "EmpCaracteristicasFSRV":
						
							if (value == null || value is System.Decimal)
								this.EmpCaracteristicasFSRV = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasFSRV);
							break;
						
						case "EmpCaracteristicasFPCT":
						
							if (value == null || value is System.Decimal)
								this.EmpCaracteristicasFPCT = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasFPCT);
							break;
						
						case "EmpCaracteristicasGMDT":
						
							if (value == null || value is System.Decimal)
								this.EmpCaracteristicasGMDT = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasGMDT);
							break;
						
						case "EmpCaracteristicasDSRV":
						
							if (value == null || value is System.Decimal)
								this.EmpCaracteristicasDSRV = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasDSRV);
							break;
						
						case "EmpCaracteristicasINC":
						
							if (value == null || value is System.Decimal)
								this.EmpCaracteristicasINC = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasINC);
							break;
						
						case "EmpCaracteristicasVSD":
						
							if (value == null || value is System.Decimal)
								this.EmpCaracteristicasVSD = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpCaracteristicasVSD);
							break;
						
						case "EmpControlNumeroSesiones":
						
							if (value == null || value is System.Decimal)
								this.EmpControlNumeroSesiones = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlNumeroSesiones);
							break;
						
						case "EmpControlDiasFinTratamiento":
						
							if (value == null || value is System.Decimal)
								this.EmpControlDiasFinTratamiento = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlDiasFinTratamiento);
							break;
						
						case "EmpFacturacionAutomatizada":
						
							if (value == null || value is System.Boolean)
								this.EmpFacturacionAutomatizada = (System.Boolean?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpFacturacionAutomatizada);
							break;
						
						case "EmpControlCalidadTiempoMaximo":
						
							if (value == null || value is System.Decimal)
								this.EmpControlCalidadTiempoMaximo = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlCalidadTiempoMaximo);
							break;
						
						case "EmpControlCalidadTiempoMargen":
						
							if (value == null || value is System.Decimal)
								this.EmpControlCalidadTiempoMargen = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlCalidadTiempoMargen);
							break;
						
						case "EmpControlCalidadServicios":
						
							if (value == null || value is System.Decimal)
								this.EmpControlCalidadServicios = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlCalidadServicios);
							break;
						
						case "EmpControlCalidadHora":
						
							if (value == null || value is System.Decimal)
								this.EmpControlCalidadHora = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpControlCalidadHora);
							break;
						
						case "EmpTurnoTrabajoVehiculoM":
						
							if (value == null || value is System.Decimal)
								this.EmpTurnoTrabajoVehiculoM = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTurnoTrabajoVehiculoM);
							break;
						
						case "EmpTurnoTrabajoVehiculoT":
						
							if (value == null || value is System.Decimal)
								this.EmpTurnoTrabajoVehiculoT = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTurnoTrabajoVehiculoT);
							break;
						
						case "EmpTurnoTrabajoVehiculoN":
						
							if (value == null || value is System.Decimal)
								this.EmpTurnoTrabajoVehiculoN = (System.Decimal?)value;
								OnPropertyChanged(EmpresasMetadata.PropertyNames.EmpTurnoTrabajoVehiculoN);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esEmpresas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String EmpCodPK
			{
				get
				{
					System.Decimal? data = entity.EmpCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCodPK = null;
					else entity.EmpCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpNombre
			{
				get
				{
					System.String data = entity.EmpNombre;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpNombre = null;
					else entity.EmpNombre = Convert.ToString(value);
				}
			}
				
			public System.String EmpCifNif
			{
				get
				{
					System.String data = entity.EmpCifNif;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCifNif = null;
					else entity.EmpCifNif = Convert.ToString(value);
				}
			}
				
			public System.String EmpAdministrador
			{
				get
				{
					System.String data = entity.EmpAdministrador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpAdministrador = null;
					else entity.EmpAdministrador = Convert.ToString(value);
				}
			}
				
			public System.String EmpDomicilio
			{
				get
				{
					System.String data = entity.EmpDomicilio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpDomicilio = null;
					else entity.EmpDomicilio = Convert.ToString(value);
				}
			}
				
			public System.String EmpCP
			{
				get
				{
					System.String data = entity.EmpCP;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCP = null;
					else entity.EmpCP = Convert.ToString(value);
				}
			}
				
			public System.String EmpTelefono
			{
				get
				{
					System.String data = entity.EmpTelefono;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpTelefono = null;
					else entity.EmpTelefono = Convert.ToString(value);
				}
			}
				
			public System.String EmpMovil
			{
				get
				{
					System.String data = entity.EmpMovil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpMovil = null;
					else entity.EmpMovil = Convert.ToString(value);
				}
			}
				
			public System.String EmpFax
			{
				get
				{
					System.String data = entity.EmpFax;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpFax = null;
					else entity.EmpFax = Convert.ToString(value);
				}
			}
				
			public System.String EmpEmail
			{
				get
				{
					System.String data = entity.EmpEmail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpEmail = null;
					else entity.EmpEmail = Convert.ToString(value);
				}
			}
				
			public System.String EmpWWW
			{
				get
				{
					System.String data = entity.EmpWWW;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpWWW = null;
					else entity.EmpWWW = Convert.ToString(value);
				}
			}
				
			public System.String LcdCodPK
			{
				get
				{
					System.Decimal? data = entity.LcdCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LcdCodPK = null;
					else entity.LcdCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PvcCodPK
			{
				get
				{
					System.Decimal? data = entity.PvcCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PvcCodPK = null;
					else entity.PvcCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpRef
			{
				get
				{
					System.Decimal? data = entity.EmpRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpRef = null;
					else entity.EmpRef = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpEliminada
			{
				get
				{
					System.Boolean? data = entity.EmpEliminada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpEliminada = null;
					else entity.EmpEliminada = Convert.ToBoolean(value);
				}
			}
				
			public System.String EmpHeD
			{
				get
				{
					System.Decimal? data = entity.EmpHeD;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpHeD = null;
					else entity.EmpHeD = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpHeC
			{
				get
				{
					System.Decimal? data = entity.EmpHeC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpHeC = null;
					else entity.EmpHeC = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpHeRH
			{
				get
				{
					System.Decimal? data = entity.EmpHeRH;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpHeRH = null;
					else entity.EmpHeRH = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpHeT
			{
				get
				{
					System.Decimal? data = entity.EmpHeT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpHeT = null;
					else entity.EmpHeT = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpHeO
			{
				get
				{
					System.Decimal? data = entity.EmpHeO;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpHeO = null;
					else entity.EmpHeO = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpVehPS
			{
				get
				{
					System.Decimal? data = entity.EmpVehPS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpVehPS = null;
					else entity.EmpVehPS = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpVehPT
			{
				get
				{
					System.Decimal? data = entity.EmpVehPT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpVehPT = null;
					else entity.EmpVehPT = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpModoTrabajo
			{
				get
				{
					System.Decimal? data = entity.EmpModoTrabajo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpModoTrabajo = null;
					else entity.EmpModoTrabajo = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpTpFactSES
			{
				get
				{
					System.Decimal? data = entity.EmpTpFactSES;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpTpFactSES = null;
					else entity.EmpTpFactSES = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpKmPoblacion
			{
				get
				{
					System.Decimal? data = entity.EmpKmPoblacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpKmPoblacion = null;
					else entity.EmpKmPoblacion = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpPacienteCnt
			{
				get
				{
					System.Decimal? data = entity.EmpPacienteCnt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpPacienteCnt = null;
					else entity.EmpPacienteCnt = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpTurnoTrabConductor
			{
				get
				{
					System.Decimal? data = entity.EmpTurnoTrabConductor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpTurnoTrabConductor = null;
					else entity.EmpTurnoTrabConductor = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpFacturaSerie1
			{
				get
				{
					System.String data = entity.EmpFacturaSerie1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpFacturaSerie1 = null;
					else entity.EmpFacturaSerie1 = Convert.ToString(value);
				}
			}
				
			public System.String EmpFacturaSerie2
			{
				get
				{
					System.String data = entity.EmpFacturaSerie2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpFacturaSerie2 = null;
					else entity.EmpFacturaSerie2 = Convert.ToString(value);
				}
			}
				
			public System.String EmpFacturaNumero1
			{
				get
				{
					System.Decimal? data = entity.EmpFacturaNumero1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpFacturaNumero1 = null;
					else entity.EmpFacturaNumero1 = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpFacturaNumero2
			{
				get
				{
					System.Decimal? data = entity.EmpFacturaNumero2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpFacturaNumero2 = null;
					else entity.EmpFacturaNumero2 = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpFacturaFecha1
			{
				get
				{
					System.DateTime? data = entity.EmpFacturaFecha1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpFacturaFecha1 = null;
					else entity.EmpFacturaFecha1 = Convert.ToDateTime(value);
				}
			}
				
			public System.String EmpFacturaFecha2
			{
				get
				{
					System.DateTime? data = entity.EmpFacturaFecha2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpFacturaFecha2 = null;
					else entity.EmpFacturaFecha2 = Convert.ToDateTime(value);
				}
			}
				
			public System.String EmpPrecisionU
			{
				get
				{
					System.Decimal? data = entity.EmpPrecisionU;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpPrecisionU = null;
					else entity.EmpPrecisionU = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpPrecisionI
			{
				get
				{
					System.Decimal? data = entity.EmpPrecisionI;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpPrecisionI = null;
					else entity.EmpPrecisionI = Convert.ToDecimal(value);
				}
			}
				
			public System.String TtrCodPK
			{
				get
				{
					System.Decimal? data = entity.TtrCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TtrCodPK = null;
					else entity.TtrCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpHorasJornadaNorma
			{
				get
				{
					System.String data = entity.EmpHorasJornadaNorma;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpHorasJornadaNorma = null;
					else entity.EmpHorasJornadaNorma = Convert.ToString(value);
				}
			}
				
			public System.String EmpHorasJornadaNormal
			{
				get
				{
					System.String data = entity.EmpHorasJornadaNormal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpHorasJornadaNormal = null;
					else entity.EmpHorasJornadaNormal = Convert.ToString(value);
				}
			}
				
			public System.String EmpVehPC
			{
				get
				{
					System.Decimal? data = entity.EmpVehPC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpVehPC = null;
					else entity.EmpVehPC = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpParametrosxDefecto
			{
				get
				{
					System.Boolean? data = entity.EmpParametrosxDefecto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpParametrosxDefecto = null;
					else entity.EmpParametrosxDefecto = Convert.ToBoolean(value);
				}
			}
				
			public System.String EmpModoTrabajoFacturacion
			{
				get
				{
					System.Decimal? data = entity.EmpModoTrabajoFacturacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpModoTrabajoFacturacion = null;
					else entity.EmpModoTrabajoFacturacion = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpEstadoDocumentacion
			{
				get
				{
					System.Int16? data = entity.EmpEstadoDocumentacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpEstadoDocumentacion = null;
					else entity.EmpEstadoDocumentacion = Convert.ToInt16(value);
				}
			}
				
			public System.String EmpNombreCMPSP
			{
				get
				{
					System.String data = entity.EmpNombreCMPSP;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpNombreCMPSP = null;
					else entity.EmpNombreCMPSP = Convert.ToString(value);
				}
			}
				
			public System.String EmpColor
			{
				get
				{
					System.Decimal? data = entity.EmpColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpColor = null;
					else entity.EmpColor = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpEstado
			{
				get
				{
					System.Decimal? data = entity.EmpEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpEstado = null;
					else entity.EmpEstado = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpEstadoCaduca
			{
				get
				{
					System.Int16? data = entity.EmpEstadoCaduca;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpEstadoCaduca = null;
					else entity.EmpEstadoCaduca = Convert.ToInt16(value);
				}
			}
				
			public System.String EmpDocumentar
			{
				get
				{
					System.Boolean? data = entity.EmpDocumentar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpDocumentar = null;
					else entity.EmpDocumentar = Convert.ToBoolean(value);
				}
			}
				
			public System.String EmpGestion
			{
				get
				{
					System.Boolean? data = entity.EmpGestion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpGestion = null;
					else entity.EmpGestion = Convert.ToBoolean(value);
				}
			}
				
			public System.String EmpCaracteristicasFSRV
			{
				get
				{
					System.Decimal? data = entity.EmpCaracteristicasFSRV;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCaracteristicasFSRV = null;
					else entity.EmpCaracteristicasFSRV = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpCaracteristicasFPCT
			{
				get
				{
					System.Decimal? data = entity.EmpCaracteristicasFPCT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCaracteristicasFPCT = null;
					else entity.EmpCaracteristicasFPCT = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpCaracteristicasGMDT
			{
				get
				{
					System.Decimal? data = entity.EmpCaracteristicasGMDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCaracteristicasGMDT = null;
					else entity.EmpCaracteristicasGMDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpCaracteristicasDSRV
			{
				get
				{
					System.Decimal? data = entity.EmpCaracteristicasDSRV;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCaracteristicasDSRV = null;
					else entity.EmpCaracteristicasDSRV = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpHoraEnvio
			{
				get
				{
					System.String data = entity.EmpHoraEnvio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpHoraEnvio = null;
					else entity.EmpHoraEnvio = Convert.ToString(value);
				}
			}
				
			public System.String EmpCaracteristicasINC
			{
				get
				{
					System.Decimal? data = entity.EmpCaracteristicasINC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCaracteristicasINC = null;
					else entity.EmpCaracteristicasINC = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpCaracteristicasVSD
			{
				get
				{
					System.Decimal? data = entity.EmpCaracteristicasVSD;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCaracteristicasVSD = null;
					else entity.EmpCaracteristicasVSD = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpInscripcion
			{
				get
				{
					System.String data = entity.EmpInscripcion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpInscripcion = null;
					else entity.EmpInscripcion = Convert.ToString(value);
				}
			}
				
			public System.String EmpControlNumeroSesiones
			{
				get
				{
					System.Decimal? data = entity.EmpControlNumeroSesiones;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpControlNumeroSesiones = null;
					else entity.EmpControlNumeroSesiones = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpControlDiasFinTratamiento
			{
				get
				{
					System.Decimal? data = entity.EmpControlDiasFinTratamiento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpControlDiasFinTratamiento = null;
					else entity.EmpControlDiasFinTratamiento = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpFacturacionAutomatizada
			{
				get
				{
					System.Boolean? data = entity.EmpFacturacionAutomatizada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpFacturacionAutomatizada = null;
					else entity.EmpFacturacionAutomatizada = Convert.ToBoolean(value);
				}
			}
				
			public System.String EmpControlCalidadTiempoMaximo
			{
				get
				{
					System.Decimal? data = entity.EmpControlCalidadTiempoMaximo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpControlCalidadTiempoMaximo = null;
					else entity.EmpControlCalidadTiempoMaximo = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpControlCalidadTiempoMargen
			{
				get
				{
					System.Decimal? data = entity.EmpControlCalidadTiempoMargen;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpControlCalidadTiempoMargen = null;
					else entity.EmpControlCalidadTiempoMargen = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpControlCalidadServicios
			{
				get
				{
					System.Decimal? data = entity.EmpControlCalidadServicios;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpControlCalidadServicios = null;
					else entity.EmpControlCalidadServicios = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpControlCalidadHora
			{
				get
				{
					System.Decimal? data = entity.EmpControlCalidadHora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpControlCalidadHora = null;
					else entity.EmpControlCalidadHora = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpTurnoTrabajoVehiculoM
			{
				get
				{
					System.Decimal? data = entity.EmpTurnoTrabajoVehiculoM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpTurnoTrabajoVehiculoM = null;
					else entity.EmpTurnoTrabajoVehiculoM = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpTurnoTrabajoVehiculoT
			{
				get
				{
					System.Decimal? data = entity.EmpTurnoTrabajoVehiculoT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpTurnoTrabajoVehiculoT = null;
					else entity.EmpTurnoTrabajoVehiculoT = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpTurnoTrabajoVehiculoN
			{
				get
				{
					System.Decimal? data = entity.EmpTurnoTrabajoVehiculoN;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpTurnoTrabajoVehiculoN = null;
					else entity.EmpTurnoTrabajoVehiculoN = Convert.ToDecimal(value);
				}
			}
			

			private esEmpresas entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return EmpresasMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public EmpresasQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EmpresasQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(EmpresasQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(EmpresasQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private EmpresasQ query;		
	}



	[Serializable]
	abstract public partial class esEmpresasCol : CollectionBase<Empresas>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EmpresasMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "EmpresasCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public EmpresasQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EmpresasQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(EmpresasQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EmpresasQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(EmpresasQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((EmpresasQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private EmpresasQ query;
	}



	[Serializable]
	abstract public partial class esEmpresasQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return EmpresasMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpCodPK,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpNombre,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpNombre, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpCifNif,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCifNif, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpAdministrador,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpAdministrador, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpDomicilio,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpDomicilio, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpCP,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCP, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpTelefono,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTelefono, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpMovil,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpMovil, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpFax,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFax, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpEmail,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEmail, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpWWW,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpWWW, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.LcdCodPK,new esQueryItem(this, EmpresasMetadata.ColumnNames.LcdCodPK, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.PvcCodPK,new esQueryItem(this, EmpresasMetadata.ColumnNames.PvcCodPK, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpRef,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpRef, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpEliminada,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEliminada, esSystemType.Boolean));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpHeD,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeD, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpHeC,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeC, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpHeRH,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeRH, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpHeT,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeT, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpHeO,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeO, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpVehPS,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpVehPS, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpVehPT,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpVehPT, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpModoTrabajo,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpModoTrabajo, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpTpFactSES,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTpFactSES, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpKmPoblacion,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpKmPoblacion, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpPacienteCnt,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpPacienteCnt, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpTurnoTrabConductor,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTurnoTrabConductor, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpFacturaSerie1,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaSerie1, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpFacturaSerie2,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaSerie2, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpFacturaNumero1,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaNumero1, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpFacturaNumero2,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaNumero2, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpFacturaFecha1,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaFecha1, esSystemType.DateTime));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpFacturaFecha2,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaFecha2, esSystemType.DateTime));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpPrecisionU,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpPrecisionU, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpPrecisionI,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpPrecisionI, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.TtrCodPK,new esQueryItem(this, EmpresasMetadata.ColumnNames.TtrCodPK, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpHorasJornadaNorma,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHorasJornadaNorma, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpHorasJornadaNormal,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHorasJornadaNormal, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpVehPC,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpVehPC, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpParametrosxDefecto,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpParametrosxDefecto, esSystemType.Boolean));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpModoTrabajoFacturacion,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpModoTrabajoFacturacion, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpEstadoDocumentacion,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEstadoDocumentacion, esSystemType.Int16));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpNombreCMPSP,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpNombreCMPSP, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpColor,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpColor, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpEstado,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEstado, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpEstadoCaduca,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEstadoCaduca, esSystemType.Int16));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpDocumentar,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpDocumentar, esSystemType.Boolean));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpGestion,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpGestion, esSystemType.Boolean));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpCaracteristicasFSRV,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasFSRV, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpCaracteristicasFPCT,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasFPCT, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpCaracteristicasGMDT,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasGMDT, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpCaracteristicasDSRV,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasDSRV, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpHoraEnvio,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHoraEnvio, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpCaracteristicasINC,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasINC, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpCaracteristicasVSD,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasVSD, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpInscripcion,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpInscripcion, esSystemType.String));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpControlNumeroSesiones,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlNumeroSesiones, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpControlDiasFinTratamiento,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlDiasFinTratamiento, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpFacturacionAutomatizada,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturacionAutomatizada, esSystemType.Boolean));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMaximo,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMaximo, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMargen,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMargen, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpControlCalidadServicios,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlCalidadServicios, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpControlCalidadHora,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlCalidadHora, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoM,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoM, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoT,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoT, esSystemType.Decimal));
			_queryItems.Add(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoN,new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoN, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem EmpCodPK
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpNombre
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpNombre, esSystemType.String); }
		} 
		
		public esQueryItem EmpCifNif
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCifNif, esSystemType.String); }
		} 
		
		public esQueryItem EmpAdministrador
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpAdministrador, esSystemType.String); }
		} 
		
		public esQueryItem EmpDomicilio
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpDomicilio, esSystemType.String); }
		} 
		
		public esQueryItem EmpCP
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCP, esSystemType.String); }
		} 
		
		public esQueryItem EmpTelefono
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTelefono, esSystemType.String); }
		} 
		
		public esQueryItem EmpMovil
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpMovil, esSystemType.String); }
		} 
		
		public esQueryItem EmpFax
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFax, esSystemType.String); }
		} 
		
		public esQueryItem EmpEmail
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEmail, esSystemType.String); }
		} 
		
		public esQueryItem EmpWWW
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpWWW, esSystemType.String); }
		} 
		
		public esQueryItem LcdCodPK
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.LcdCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PvcCodPK
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.PvcCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpRef
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpRef, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpEliminada
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEliminada, esSystemType.Boolean); }
		} 
		
		public esQueryItem EmpHeD
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeD, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpHeC
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeC, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpHeRH
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeRH, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpHeT
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeT, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpHeO
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHeO, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpVehPS
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpVehPS, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpVehPT
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpVehPT, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpModoTrabajo
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpModoTrabajo, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpTpFactSES
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTpFactSES, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpKmPoblacion
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpKmPoblacion, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpPacienteCnt
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpPacienteCnt, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpTurnoTrabConductor
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTurnoTrabConductor, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpFacturaSerie1
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaSerie1, esSystemType.String); }
		} 
		
		public esQueryItem EmpFacturaSerie2
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaSerie2, esSystemType.String); }
		} 
		
		public esQueryItem EmpFacturaNumero1
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaNumero1, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpFacturaNumero2
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaNumero2, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpFacturaFecha1
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaFecha1, esSystemType.DateTime); }
		} 
		
		public esQueryItem EmpFacturaFecha2
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturaFecha2, esSystemType.DateTime); }
		} 
		
		public esQueryItem EmpPrecisionU
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpPrecisionU, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpPrecisionI
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpPrecisionI, esSystemType.Decimal); }
		} 
		
		public esQueryItem TtrCodPK
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.TtrCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpHorasJornadaNorma
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHorasJornadaNorma, esSystemType.String); }
		} 
		
		public esQueryItem EmpHorasJornadaNormal
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHorasJornadaNormal, esSystemType.String); }
		} 
		
		public esQueryItem EmpVehPC
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpVehPC, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpParametrosxDefecto
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpParametrosxDefecto, esSystemType.Boolean); }
		} 
		
		public esQueryItem EmpModoTrabajoFacturacion
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpModoTrabajoFacturacion, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpEstadoDocumentacion
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEstadoDocumentacion, esSystemType.Int16); }
		} 
		
		public esQueryItem EmpNombreCMPSP
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpNombreCMPSP, esSystemType.String); }
		} 
		
		public esQueryItem EmpColor
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpColor, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpEstado
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEstado, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpEstadoCaduca
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpEstadoCaduca, esSystemType.Int16); }
		} 
		
		public esQueryItem EmpDocumentar
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpDocumentar, esSystemType.Boolean); }
		} 
		
		public esQueryItem EmpGestion
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpGestion, esSystemType.Boolean); }
		} 
		
		public esQueryItem EmpCaracteristicasFSRV
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasFSRV, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpCaracteristicasFPCT
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasFPCT, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpCaracteristicasGMDT
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasGMDT, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpCaracteristicasDSRV
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasDSRV, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpHoraEnvio
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpHoraEnvio, esSystemType.String); }
		} 
		
		public esQueryItem EmpCaracteristicasINC
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasINC, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpCaracteristicasVSD
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpCaracteristicasVSD, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpInscripcion
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpInscripcion, esSystemType.String); }
		} 
		
		public esQueryItem EmpControlNumeroSesiones
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlNumeroSesiones, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpControlDiasFinTratamiento
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlDiasFinTratamiento, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpFacturacionAutomatizada
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpFacturacionAutomatizada, esSystemType.Boolean); }
		} 
		
		public esQueryItem EmpControlCalidadTiempoMaximo
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMaximo, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpControlCalidadTiempoMargen
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMargen, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpControlCalidadServicios
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlCalidadServicios, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpControlCalidadHora
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpControlCalidadHora, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpTurnoTrabajoVehiculoM
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoM, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpTurnoTrabajoVehiculoT
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoT, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpTurnoTrabajoVehiculoN
		{
			get { return new esQueryItem(this, EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoN, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class Empresas : esEmpresas
	{

		
		
	}
	



	[Serializable]
	public partial class EmpresasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EmpresasMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpNombre, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpNombre;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpCifNif, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpCifNif;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpAdministrador, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpAdministrador;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpDomicilio, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpDomicilio;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpCP, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpCP;
			c.CharacterMaxLength = 5;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpTelefono, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpTelefono;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpMovil, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpMovil;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpFax, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpFax;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpEmail, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpEmail;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpWWW, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpWWW;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.LcdCodPK, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.LcdCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.PvcCodPK, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.PvcCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpRef, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpRef;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpEliminada, 14, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpEliminada;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpHeD, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpHeD;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpHeC, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpHeC;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpHeRH, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpHeRH;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpHeT, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpHeT;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpHeO, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpHeO;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpVehPS, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpVehPS;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((7))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpVehPT, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpVehPT;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpModoTrabajo, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpModoTrabajo;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpTpFactSES, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpTpFactSES;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpKmPoblacion, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpKmPoblacion;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpPacienteCnt, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpPacienteCnt;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpTurnoTrabConductor, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpTurnoTrabConductor;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpFacturaSerie1, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpFacturaSerie1;
			c.CharacterMaxLength = 5;
			c.HasDefault = true;
			c.Default = @"(N'F')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpFacturaSerie2, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpFacturaSerie2;
			c.CharacterMaxLength = 5;
			c.HasDefault = true;
			c.Default = @"(N'Z')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpFacturaNumero1, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpFacturaNumero1;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpFacturaNumero2, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpFacturaNumero2;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpFacturaFecha1, 31, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpFacturaFecha1;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpFacturaFecha2, 32, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpFacturaFecha2;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpPrecisionU, 33, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpPrecisionU;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpPrecisionI, 34, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpPrecisionI;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.TtrCodPK, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.TtrCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpHorasJornadaNorma, 36, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpHorasJornadaNorma;
			c.CharacterMaxLength = 5;
			c.HasDefault = true;
			c.Default = @"(N'12:00')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpHorasJornadaNormal, 37, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpHorasJornadaNormal;
			c.CharacterMaxLength = 5;
			c.HasDefault = true;
			c.Default = @"(N'12:00')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpVehPC, 38, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpVehPC;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpParametrosxDefecto, 39, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpParametrosxDefecto;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpModoTrabajoFacturacion, 40, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpModoTrabajoFacturacion;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpEstadoDocumentacion, 41, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpEstadoDocumentacion;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((80))";
			c.Description = "P=80 M=67 C=77";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpNombreCMPSP, 42, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpNombreCMPSP;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpColor, 43, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpColor;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpEstado, 44, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpEstado;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpEstadoCaduca, 45, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpEstadoCaduca;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((77))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpDocumentar, 46, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpDocumentar;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpGestion, 47, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpGestion;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpCaracteristicasFSRV, 48, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpCaracteristicasFSRV;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpCaracteristicasFPCT, 49, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpCaracteristicasFPCT;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpCaracteristicasGMDT, 50, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpCaracteristicasGMDT;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpCaracteristicasDSRV, 51, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpCaracteristicasDSRV;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpHoraEnvio, 52, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpHoraEnvio;
			c.CharacterMaxLength = 5;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpCaracteristicasINC, 53, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpCaracteristicasINC;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((3))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpCaracteristicasVSD, 54, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpCaracteristicasVSD;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpInscripcion, 55, typeof(System.String), esSystemType.String);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpInscripcion;
			c.CharacterMaxLength = 254;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpControlNumeroSesiones, 56, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpControlNumeroSesiones;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpControlDiasFinTratamiento, 57, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpControlDiasFinTratamiento;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpFacturacionAutomatizada, 58, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpFacturacionAutomatizada;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMaximo, 59, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpControlCalidadTiempoMaximo;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpControlCalidadTiempoMargen, 60, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpControlCalidadTiempoMargen;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpControlCalidadServicios, 61, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpControlCalidadServicios;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpControlCalidadHora, 62, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpControlCalidadHora;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoM, 63, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpTurnoTrabajoVehiculoM;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoT, 64, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpTurnoTrabajoVehiculoT;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(EmpresasMetadata.ColumnNames.EmpTurnoTrabajoVehiculoN, 65, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EmpresasMetadata.PropertyNames.EmpTurnoTrabajoVehiculoN;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public EmpresasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string EmpCodPK = "emp_CodPK";
			 public const string EmpNombre = "emp_Nombre";
			 public const string EmpCifNif = "emp_CifNif";
			 public const string EmpAdministrador = "emp_Administrador";
			 public const string EmpDomicilio = "emp_Domicilio";
			 public const string EmpCP = "emp_CP";
			 public const string EmpTelefono = "emp_Telefono";
			 public const string EmpMovil = "emp_Movil";
			 public const string EmpFax = "emp_Fax";
			 public const string EmpEmail = "emp_Email";
			 public const string EmpWWW = "emp_WWW";
			 public const string LcdCodPK = "lcd_CodPK";
			 public const string PvcCodPK = "pvc_CodPK";
			 public const string EmpRef = "emp_ref";
			 public const string EmpEliminada = "emp_Eliminada";
			 public const string EmpHeD = "emp_HeD";
			 public const string EmpHeC = "emp_HeC";
			 public const string EmpHeRH = "emp_HeRH";
			 public const string EmpHeT = "emp_HeT";
			 public const string EmpHeO = "emp_HeO";
			 public const string EmpVehPS = "emp_VehPS";
			 public const string EmpVehPT = "emp_VehPT";
			 public const string EmpModoTrabajo = "emp_ModoTrabajo";
			 public const string EmpTpFactSES = "emp_TpFactSES";
			 public const string EmpKmPoblacion = "emp_KmPoblacion";
			 public const string EmpPacienteCnt = "emp_PacienteCnt";
			 public const string EmpTurnoTrabConductor = "emp_TurnoTrabConductor";
			 public const string EmpFacturaSerie1 = "emp_FacturaSerie1";
			 public const string EmpFacturaSerie2 = "emp_FacturaSerie2";
			 public const string EmpFacturaNumero1 = "emp_FacturaNumero1";
			 public const string EmpFacturaNumero2 = "emp_FacturaNumero2";
			 public const string EmpFacturaFecha1 = "emp_FacturaFecha1";
			 public const string EmpFacturaFecha2 = "emp_FacturaFecha2";
			 public const string EmpPrecisionU = "emp_PrecisionU";
			 public const string EmpPrecisionI = "emp_PrecisionI";
			 public const string TtrCodPK = "ttr_CodPK";
			 public const string EmpHorasJornadaNorma = "emp_HorasJornadaNorma";
			 public const string EmpHorasJornadaNormal = "emp_HorasJornadaNormal";
			 public const string EmpVehPC = "emp_VehPC";
			 public const string EmpParametrosxDefecto = "emp_ParametrosxDefecto";
			 public const string EmpModoTrabajoFacturacion = "emp_ModoTrabajoFacturacion";
			 public const string EmpEstadoDocumentacion = "emp_EstadoDocumentacion";
			 public const string EmpNombreCMPSP = "emp_NombreCMPSP";
			 public const string EmpColor = "emp_Color";
			 public const string EmpEstado = "emp_Estado";
			 public const string EmpEstadoCaduca = "emp_EstadoCaduca";
			 public const string EmpDocumentar = "emp_Documentar";
			 public const string EmpGestion = "emp_Gestion";
			 public const string EmpCaracteristicasFSRV = "emp_CaracteristicasFSRV";
			 public const string EmpCaracteristicasFPCT = "emp_CaracteristicasFPCT";
			 public const string EmpCaracteristicasGMDT = "emp_CaracteristicasGMDT";
			 public const string EmpCaracteristicasDSRV = "emp_CaracteristicasDSRV";
			 public const string EmpHoraEnvio = "emp_HoraEnvio";
			 public const string EmpCaracteristicasINC = "emp_CaracteristicasINC";
			 public const string EmpCaracteristicasVSD = "emp_CaracteristicasVSD";
			 public const string EmpInscripcion = "emp_Inscripcion";
			 public const string EmpControlNumeroSesiones = "emp_ControlNumeroSesiones";
			 public const string EmpControlDiasFinTratamiento = "emp_ControlDiasFinTratamiento";
			 public const string EmpFacturacionAutomatizada = "emp_FacturacionAutomatizada";
			 public const string EmpControlCalidadTiempoMaximo = "emp_ControlCalidadTiempoMaximo";
			 public const string EmpControlCalidadTiempoMargen = "emp_ControlCalidadTiempoMargen";
			 public const string EmpControlCalidadServicios = "emp_ControlCalidadServicios";
			 public const string EmpControlCalidadHora = "emp_ControlCalidadHora";
			 public const string EmpTurnoTrabajoVehiculoM = "emp_TurnoTrabajoVehiculoM";
			 public const string EmpTurnoTrabajoVehiculoT = "emp_TurnoTrabajoVehiculoT";
			 public const string EmpTurnoTrabajoVehiculoN = "emp_TurnoTrabajoVehiculoN";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string EmpCodPK = "EmpCodPK";
			 public const string EmpNombre = "EmpNombre";
			 public const string EmpCifNif = "EmpCifNif";
			 public const string EmpAdministrador = "EmpAdministrador";
			 public const string EmpDomicilio = "EmpDomicilio";
			 public const string EmpCP = "EmpCP";
			 public const string EmpTelefono = "EmpTelefono";
			 public const string EmpMovil = "EmpMovil";
			 public const string EmpFax = "EmpFax";
			 public const string EmpEmail = "EmpEmail";
			 public const string EmpWWW = "EmpWWW";
			 public const string LcdCodPK = "LcdCodPK";
			 public const string PvcCodPK = "PvcCodPK";
			 public const string EmpRef = "EmpRef";
			 public const string EmpEliminada = "EmpEliminada";
			 public const string EmpHeD = "EmpHeD";
			 public const string EmpHeC = "EmpHeC";
			 public const string EmpHeRH = "EmpHeRH";
			 public const string EmpHeT = "EmpHeT";
			 public const string EmpHeO = "EmpHeO";
			 public const string EmpVehPS = "EmpVehPS";
			 public const string EmpVehPT = "EmpVehPT";
			 public const string EmpModoTrabajo = "EmpModoTrabajo";
			 public const string EmpTpFactSES = "EmpTpFactSES";
			 public const string EmpKmPoblacion = "EmpKmPoblacion";
			 public const string EmpPacienteCnt = "EmpPacienteCnt";
			 public const string EmpTurnoTrabConductor = "EmpTurnoTrabConductor";
			 public const string EmpFacturaSerie1 = "EmpFacturaSerie1";
			 public const string EmpFacturaSerie2 = "EmpFacturaSerie2";
			 public const string EmpFacturaNumero1 = "EmpFacturaNumero1";
			 public const string EmpFacturaNumero2 = "EmpFacturaNumero2";
			 public const string EmpFacturaFecha1 = "EmpFacturaFecha1";
			 public const string EmpFacturaFecha2 = "EmpFacturaFecha2";
			 public const string EmpPrecisionU = "EmpPrecisionU";
			 public const string EmpPrecisionI = "EmpPrecisionI";
			 public const string TtrCodPK = "TtrCodPK";
			 public const string EmpHorasJornadaNorma = "EmpHorasJornadaNorma";
			 public const string EmpHorasJornadaNormal = "EmpHorasJornadaNormal";
			 public const string EmpVehPC = "EmpVehPC";
			 public const string EmpParametrosxDefecto = "EmpParametrosxDefecto";
			 public const string EmpModoTrabajoFacturacion = "EmpModoTrabajoFacturacion";
			 public const string EmpEstadoDocumentacion = "EmpEstadoDocumentacion";
			 public const string EmpNombreCMPSP = "EmpNombreCMPSP";
			 public const string EmpColor = "EmpColor";
			 public const string EmpEstado = "EmpEstado";
			 public const string EmpEstadoCaduca = "EmpEstadoCaduca";
			 public const string EmpDocumentar = "EmpDocumentar";
			 public const string EmpGestion = "EmpGestion";
			 public const string EmpCaracteristicasFSRV = "EmpCaracteristicasFSRV";
			 public const string EmpCaracteristicasFPCT = "EmpCaracteristicasFPCT";
			 public const string EmpCaracteristicasGMDT = "EmpCaracteristicasGMDT";
			 public const string EmpCaracteristicasDSRV = "EmpCaracteristicasDSRV";
			 public const string EmpHoraEnvio = "EmpHoraEnvio";
			 public const string EmpCaracteristicasINC = "EmpCaracteristicasINC";
			 public const string EmpCaracteristicasVSD = "EmpCaracteristicasVSD";
			 public const string EmpInscripcion = "EmpInscripcion";
			 public const string EmpControlNumeroSesiones = "EmpControlNumeroSesiones";
			 public const string EmpControlDiasFinTratamiento = "EmpControlDiasFinTratamiento";
			 public const string EmpFacturacionAutomatizada = "EmpFacturacionAutomatizada";
			 public const string EmpControlCalidadTiempoMaximo = "EmpControlCalidadTiempoMaximo";
			 public const string EmpControlCalidadTiempoMargen = "EmpControlCalidadTiempoMargen";
			 public const string EmpControlCalidadServicios = "EmpControlCalidadServicios";
			 public const string EmpControlCalidadHora = "EmpControlCalidadHora";
			 public const string EmpTurnoTrabajoVehiculoM = "EmpTurnoTrabajoVehiculoM";
			 public const string EmpTurnoTrabajoVehiculoT = "EmpTurnoTrabajoVehiculoT";
			 public const string EmpTurnoTrabajoVehiculoN = "EmpTurnoTrabajoVehiculoN";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EmpresasMetadata))
			{
				if(EmpresasMetadata.mapDelegates == null)
				{
					EmpresasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EmpresasMetadata.meta == null)
				{
					EmpresasMetadata.meta = new EmpresasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("EmpCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpNombre", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpCifNif", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpAdministrador", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpDomicilio", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpCP", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpTelefono", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpMovil", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpFax", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpEmail", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpWWW", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("LcdCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PvcCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpRef", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpEliminada", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("EmpHeD", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpHeC", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpHeRH", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpHeT", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpHeO", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpVehPS", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpVehPT", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpModoTrabajo", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpTpFactSES", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpKmPoblacion", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpPacienteCnt", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpTurnoTrabConductor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpFacturaSerie1", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpFacturaSerie2", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpFacturaNumero1", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpFacturaNumero2", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpFacturaFecha1", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("EmpFacturaFecha2", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("EmpPrecisionU", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpPrecisionI", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TtrCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpHorasJornadaNorma", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpHorasJornadaNormal", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpVehPC", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpParametrosxDefecto", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("EmpModoTrabajoFacturacion", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpEstadoDocumentacion", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("EmpNombreCMPSP", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpColor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpEstado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpEstadoCaduca", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("EmpDocumentar", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("EmpGestion", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("EmpCaracteristicasFSRV", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpCaracteristicasFPCT", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpCaracteristicasGMDT", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpCaracteristicasDSRV", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpHoraEnvio", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpCaracteristicasINC", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpCaracteristicasVSD", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpInscripcion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EmpControlNumeroSesiones", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpControlDiasFinTratamiento", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpFacturacionAutomatizada", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("EmpControlCalidadTiempoMaximo", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpControlCalidadTiempoMargen", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpControlCalidadServicios", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpControlCalidadHora", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpTurnoTrabajoVehiculoM", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpTurnoTrabajoVehiculoT", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpTurnoTrabajoVehiculoN", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "Empresas";
				meta.Destination = "Empresas";
				
				meta.spInsert = "proc_EmpresasInsert";				
				meta.spUpdate = "proc_EmpresasUpdate";		
				meta.spDelete = "proc_EmpresasDelete";
				meta.spLoadAll = "proc_EmpresasLoadAll";
				meta.spLoadByPrimaryKey = "proc_EmpresasLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EmpresasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
