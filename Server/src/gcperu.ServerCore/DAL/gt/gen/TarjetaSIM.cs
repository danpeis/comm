
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// GUARDA LA INFORMACION DEL TELEFONO DEL TERMINAL. SE VINCULA A TRAVES DEL ID, CON LA TABLA [TEMINALES]
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TarjetaSIM")]
	public partial class TarjetaSIM : esTarjetaSIM
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TarjetaSIM();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal tjsCodPK)
		{
			var obj = new TarjetaSIM();
			obj.TjsCodPK = tjsCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal tjsCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new TarjetaSIM();
			obj.TjsCodPK = tjsCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal TJSCODPK)
        {
            try
            {
                var e = new TarjetaSIM();
                return (e.LoadByPrimaryKey(TJSCODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TarjetaSIM Get(System.Decimal TJSCODPK)
        {
            try
            {
                var e = new TarjetaSIM();
                return (e.LoadByPrimaryKey(TJSCODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TarjetaSIM(System.Decimal TJSCODPK)
        {
            this.LoadByPrimaryKey(TJSCODPK);
        }
		
		public TarjetaSIM()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TarjetaSIMCol")]
	public partial class TarjetaSIMCol : esTarjetaSIMCol, IEnumerable<TarjetaSIM>
	{
	
		public TarjetaSIMCol()
		{
		}

	
	
		public TarjetaSIM FindByPrimaryKey(System.Decimal tjsCodPK)
		{
			return this.SingleOrDefault(e => e.TjsCodPK == tjsCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TarjetaSIM))]
		public class TarjetaSIMColWCFPacket : esCollectionWCFPacket<TarjetaSIMCol>
		{
			public static implicit operator TarjetaSIMCol(TarjetaSIMColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TarjetaSIMColWCFPacket(TarjetaSIMCol collection)
			{
				return new TarjetaSIMColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TarjetaSIMQ : esTarjetaSIMQ
	{
		public TarjetaSIMQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TarjetaSIMQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TarjetaSIMQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new TarjetaSIMQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TarjetaSIMQ query)
		{
			return TarjetaSIMQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TarjetaSIMQ(string query)
		{
			return (TarjetaSIMQ)TarjetaSIMQ.SerializeHelper.FromXml(query, typeof(TarjetaSIMQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTarjetaSIM : EntityBase
	{
		public esTarjetaSIM()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal tjsCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(tjsCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(tjsCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal tjsCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(tjsCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(tjsCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal tjsCodPK)
		{
			TarjetaSIMQ query = new TarjetaSIMQ();
			query.Where(query.TjsCodPK == tjsCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal tjsCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("TjsCodPK", tjsCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to TarjetasSIM.tjs_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TjsCodPK
		{
			get
			{
				return base.GetSystemDecimal(TarjetaSIMMetadata.ColumnNames.TjsCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(TarjetaSIMMetadata.ColumnNames.TjsCodPK, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TarjetasSIM.tjs_Estado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TjsEstado
		{
			get
			{
				return base.GetSystemDecimal(TarjetaSIMMetadata.ColumnNames.TjsEstado);
			}
			
			set
			{
				if(base.SetSystemDecimal(TarjetaSIMMetadata.ColumnNames.TjsEstado, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsEstado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TarjetasSIM.tjs_Ref
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TjsRef
		{
			get
			{
				return base.GetSystemDecimal(TarjetaSIMMetadata.ColumnNames.TjsRef);
			}
			
			set
			{
				if(base.SetSystemDecimal(TarjetaSIMMetadata.ColumnNames.TjsRef, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsRef);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TarjetasSIM.tjs_NumSerie
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TjsNumSerie
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.TjsNumSerie);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.TjsNumSerie, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsNumSerie);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TarjetasSIM.tjs_NumPin
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TjsNumPin
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.TjsNumPin);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.TjsNumPin, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsNumPin);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TarjetasSIM.tjs_NumPuk
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TjsNumPuk
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.TjsNumPuk);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.TjsNumPuk, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsNumPuk);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TarjetasSIM.tjs_NumTelefono
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TjsNumTelefono
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.TjsNumTelefono);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.TjsNumTelefono, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsNumTelefono);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TarjetasSIM.tjs_Proveedor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TjsProveedor
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.TjsProveedor);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.TjsProveedor, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsProveedor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TarjetasSIM.tjs_Observacion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TjsObservacion
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.TjsObservacion);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.TjsObservacion, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsObservacion);
				}
			}
		}		
		
		/// <summary>
		/// Mobile Country Code: 214 para españa
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Mcc
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.Mcc);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.Mcc, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.Mcc);
				}
			}
		}		
		
		/// <summary>
		/// Mobile Network Code: Identificacion de la operadora
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Mnc
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.Mnc);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.Mnc, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.Mnc);
				}
			}
		}		
		
		/// <summary>
		/// Fecha del Ultimo Uso de la Tarjeta
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaLastUse
		{
			get
			{
				return base.GetSystemDateTime(TarjetaSIMMetadata.ColumnNames.FechaLastUse);
			}
			
			set
			{
				if(base.SetSystemDateTime(TarjetaSIMMetadata.ColumnNames.FechaLastUse, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.FechaLastUse);
				}
			}
		}		
		
		/// <summary>
		/// Perfil movil que esta utilizando el Terminal para conectase a la red movil
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MobileNetworkProfileID
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileID);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileID, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.MobileNetworkProfileID);
				}
			}
		}		
		
		/// <summary>
		/// Perfil movil que se fuerza a traves del GITS para que lo utilize el Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MobileNetworkProfileForceuseID
		{
			get
			{
				return base.GetSystemString(TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileForceuseID);
			}
			
			set
			{
				if(base.SetSystemString(TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileForceuseID, value))
				{
					OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.MobileNetworkProfileForceuseID);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TjsCodPK": this.str().TjsCodPK = (string)value; break;							
						case "TjsEstado": this.str().TjsEstado = (string)value; break;							
						case "TjsRef": this.str().TjsRef = (string)value; break;							
						case "TjsNumSerie": this.str().TjsNumSerie = (string)value; break;							
						case "TjsNumPin": this.str().TjsNumPin = (string)value; break;							
						case "TjsNumPuk": this.str().TjsNumPuk = (string)value; break;							
						case "TjsNumTelefono": this.str().TjsNumTelefono = (string)value; break;							
						case "TjsProveedor": this.str().TjsProveedor = (string)value; break;							
						case "TjsObservacion": this.str().TjsObservacion = (string)value; break;							
						case "Mcc": this.str().Mcc = (string)value; break;							
						case "Mnc": this.str().Mnc = (string)value; break;							
						case "FechaLastUse": this.str().FechaLastUse = (string)value; break;							
						case "MobileNetworkProfileID": this.str().MobileNetworkProfileID = (string)value; break;							
						case "MobileNetworkProfileForceuseID": this.str().MobileNetworkProfileForceuseID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TjsCodPK":
						
							if (value == null || value is System.Decimal)
								this.TjsCodPK = (System.Decimal?)value;
								OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsCodPK);
							break;
						
						case "TjsEstado":
						
							if (value == null || value is System.Decimal)
								this.TjsEstado = (System.Decimal?)value;
								OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsEstado);
							break;
						
						case "TjsRef":
						
							if (value == null || value is System.Decimal)
								this.TjsRef = (System.Decimal?)value;
								OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.TjsRef);
							break;
						
						case "FechaLastUse":
						
							if (value == null || value is System.DateTime)
								this.FechaLastUse = (System.DateTime?)value;
								OnPropertyChanged(TarjetaSIMMetadata.PropertyNames.FechaLastUse);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTarjetaSIM entity)
			{
				this.entity = entity;
			}
			
	
			public System.String TjsCodPK
			{
				get
				{
					System.Decimal? data = entity.TjsCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsCodPK = null;
					else entity.TjsCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String TjsEstado
			{
				get
				{
					System.Decimal? data = entity.TjsEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsEstado = null;
					else entity.TjsEstado = Convert.ToDecimal(value);
				}
			}
				
			public System.String TjsRef
			{
				get
				{
					System.Decimal? data = entity.TjsRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsRef = null;
					else entity.TjsRef = Convert.ToDecimal(value);
				}
			}
				
			public System.String TjsNumSerie
			{
				get
				{
					System.String data = entity.TjsNumSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsNumSerie = null;
					else entity.TjsNumSerie = Convert.ToString(value);
				}
			}
				
			public System.String TjsNumPin
			{
				get
				{
					System.String data = entity.TjsNumPin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsNumPin = null;
					else entity.TjsNumPin = Convert.ToString(value);
				}
			}
				
			public System.String TjsNumPuk
			{
				get
				{
					System.String data = entity.TjsNumPuk;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsNumPuk = null;
					else entity.TjsNumPuk = Convert.ToString(value);
				}
			}
				
			public System.String TjsNumTelefono
			{
				get
				{
					System.String data = entity.TjsNumTelefono;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsNumTelefono = null;
					else entity.TjsNumTelefono = Convert.ToString(value);
				}
			}
				
			public System.String TjsProveedor
			{
				get
				{
					System.String data = entity.TjsProveedor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsProveedor = null;
					else entity.TjsProveedor = Convert.ToString(value);
				}
			}
				
			public System.String TjsObservacion
			{
				get
				{
					System.String data = entity.TjsObservacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TjsObservacion = null;
					else entity.TjsObservacion = Convert.ToString(value);
				}
			}
				
			public System.String Mcc
			{
				get
				{
					System.String data = entity.Mcc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mcc = null;
					else entity.Mcc = Convert.ToString(value);
				}
			}
				
			public System.String Mnc
			{
				get
				{
					System.String data = entity.Mnc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mnc = null;
					else entity.Mnc = Convert.ToString(value);
				}
			}
				
			public System.String FechaLastUse
			{
				get
				{
					System.DateTime? data = entity.FechaLastUse;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaLastUse = null;
					else entity.FechaLastUse = Convert.ToDateTime(value);
				}
			}
				
			public System.String MobileNetworkProfileID
			{
				get
				{
					System.String data = entity.MobileNetworkProfileID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MobileNetworkProfileID = null;
					else entity.MobileNetworkProfileID = Convert.ToString(value);
				}
			}
				
			public System.String MobileNetworkProfileForceuseID
			{
				get
				{
					System.String data = entity.MobileNetworkProfileForceuseID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MobileNetworkProfileForceuseID = null;
					else entity.MobileNetworkProfileForceuseID = Convert.ToString(value);
				}
			}
			

			private esTarjetaSIM entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TarjetaSIMMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TarjetaSIMQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TarjetaSIMQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TarjetaSIMQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TarjetaSIMQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TarjetaSIMQ query;		
	}



	[Serializable]
	abstract public partial class esTarjetaSIMCol : CollectionBase<TarjetaSIM>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TarjetaSIMMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TarjetaSIMCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TarjetaSIMQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TarjetaSIMQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TarjetaSIMQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TarjetaSIMQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TarjetaSIMQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TarjetaSIMQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TarjetaSIMQ query;
	}



	[Serializable]
	abstract public partial class esTarjetaSIMQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TarjetaSIMMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.TjsCodPK,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsCodPK, esSystemType.Decimal));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.TjsEstado,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsEstado, esSystemType.Decimal));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.TjsRef,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsRef, esSystemType.Decimal));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.TjsNumSerie,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsNumSerie, esSystemType.String));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.TjsNumPin,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsNumPin, esSystemType.String));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.TjsNumPuk,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsNumPuk, esSystemType.String));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.TjsNumTelefono,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsNumTelefono, esSystemType.String));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.TjsProveedor,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsProveedor, esSystemType.String));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.TjsObservacion,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsObservacion, esSystemType.String));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.Mcc,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.Mcc, esSystemType.String));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.Mnc,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.Mnc, esSystemType.String));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.FechaLastUse,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.FechaLastUse, esSystemType.DateTime));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileID,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileID, esSystemType.String));
			_queryItems.Add(TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileForceuseID,new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileForceuseID, esSystemType.String));
			
	    }
	
	    #region esQueryItems

		public esQueryItem TjsCodPK
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem TjsEstado
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsEstado, esSystemType.Decimal); }
		} 
		
		public esQueryItem TjsRef
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsRef, esSystemType.Decimal); }
		} 
		
		public esQueryItem TjsNumSerie
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsNumSerie, esSystemType.String); }
		} 
		
		public esQueryItem TjsNumPin
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsNumPin, esSystemType.String); }
		} 
		
		public esQueryItem TjsNumPuk
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsNumPuk, esSystemType.String); }
		} 
		
		public esQueryItem TjsNumTelefono
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsNumTelefono, esSystemType.String); }
		} 
		
		public esQueryItem TjsProveedor
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsProveedor, esSystemType.String); }
		} 
		
		public esQueryItem TjsObservacion
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.TjsObservacion, esSystemType.String); }
		} 
		
		public esQueryItem Mcc
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.Mcc, esSystemType.String); }
		} 
		
		public esQueryItem Mnc
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.Mnc, esSystemType.String); }
		} 
		
		public esQueryItem FechaLastUse
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.FechaLastUse, esSystemType.DateTime); }
		} 
		
		public esQueryItem MobileNetworkProfileID
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileID, esSystemType.String); }
		} 
		
		public esQueryItem MobileNetworkProfileForceuseID
		{
			get { return new esQueryItem(this, TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileForceuseID, esSystemType.String); }
		} 
		
		#endregion
		
	}


	
	public partial class TarjetaSIM : esTarjetaSIM
	{

		
		
	}
	



	[Serializable]
	public partial class TarjetaSIMMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TarjetaSIMMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.TjsCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.TjsCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.TjsEstado, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.TjsEstado;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.TjsRef, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.TjsRef;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.TjsNumSerie, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.TjsNumSerie;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.TjsNumPin, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.TjsNumPin;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.TjsNumPuk, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.TjsNumPuk;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.TjsNumTelefono, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.TjsNumTelefono;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.TjsProveedor, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.TjsProveedor;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.TjsObservacion, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.TjsObservacion;
			c.CharacterMaxLength = 200;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.Mcc, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.Mcc;
			c.CharacterMaxLength = 3;
			c.Description = "Mobile Country Code: 214 para españa";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.Mnc, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.Mnc;
			c.CharacterMaxLength = 3;
			c.Description = "Mobile Network Code: Identificacion de la operadora";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.FechaLastUse, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.FechaLastUse;
			c.Description = "Fecha del Ultimo Uso de la Tarjeta";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileID, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.MobileNetworkProfileID;
			c.CharacterMaxLength = 10;
			c.Description = "Perfil movil que esta utilizando el Terminal para conectase a la red movil";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TarjetaSIMMetadata.ColumnNames.MobileNetworkProfileForceuseID, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = TarjetaSIMMetadata.PropertyNames.MobileNetworkProfileForceuseID;
			c.CharacterMaxLength = 10;
			c.Description = "Perfil movil que se fuerza a traves del GITS para que lo utilize el Terminal";
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TarjetaSIMMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TjsCodPK = "tjs_CodPK";
			 public const string TjsEstado = "tjs_Estado";
			 public const string TjsRef = "tjs_Ref";
			 public const string TjsNumSerie = "tjs_NumSerie";
			 public const string TjsNumPin = "tjs_NumPin";
			 public const string TjsNumPuk = "tjs_NumPuk";
			 public const string TjsNumTelefono = "tjs_NumTelefono";
			 public const string TjsProveedor = "tjs_Proveedor";
			 public const string TjsObservacion = "tjs_Observacion";
			 public const string Mcc = "MCC";
			 public const string Mnc = "MNC";
			 public const string FechaLastUse = "FechaLastUse";
			 public const string MobileNetworkProfileID = "MobileNetworkProfileID";
			 public const string MobileNetworkProfileForceuseID = "MobileNetworkProfileForceuseID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TjsCodPK = "TjsCodPK";
			 public const string TjsEstado = "TjsEstado";
			 public const string TjsRef = "TjsRef";
			 public const string TjsNumSerie = "TjsNumSerie";
			 public const string TjsNumPin = "TjsNumPin";
			 public const string TjsNumPuk = "TjsNumPuk";
			 public const string TjsNumTelefono = "TjsNumTelefono";
			 public const string TjsProveedor = "TjsProveedor";
			 public const string TjsObservacion = "TjsObservacion";
			 public const string Mcc = "Mcc";
			 public const string Mnc = "Mnc";
			 public const string FechaLastUse = "FechaLastUse";
			 public const string MobileNetworkProfileID = "MobileNetworkProfileID";
			 public const string MobileNetworkProfileForceuseID = "MobileNetworkProfileForceuseID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TarjetaSIMMetadata))
			{
				if(TarjetaSIMMetadata.mapDelegates == null)
				{
					TarjetaSIMMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TarjetaSIMMetadata.meta == null)
				{
					TarjetaSIMMetadata.meta = new TarjetaSIMMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("TjsCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TjsEstado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TjsRef", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TjsNumSerie", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("TjsNumPin", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("TjsNumPuk", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("TjsNumTelefono", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("TjsProveedor", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("TjsObservacion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("Mcc", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Mnc", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FechaLastUse", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("MobileNetworkProfileID", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MobileNetworkProfileForceuseID", new esTypeMap("varchar", "System.String"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "TarjetasSIM";
				meta.Destination = "TarjetasSIM";
				
				meta.spInsert = "proc_TarjetasSIMInsert";				
				meta.spUpdate = "proc_TarjetasSIMUpdate";		
				meta.spDelete = "proc_TarjetasSIMDelete";
				meta.spLoadAll = "proc_TarjetasSIMLoadAll";
				meta.spLoadByPrimaryKey = "proc_TarjetasSIMLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TarjetaSIMMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
