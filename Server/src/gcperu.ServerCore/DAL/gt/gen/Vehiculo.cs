
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// Encapsulates the 'Vehiculos' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("Vehiculo")]
	public partial class Vehiculo : esVehiculo
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new Vehiculo();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal vehCodPK)
		{
			var obj = new Vehiculo();
			obj.VehCodPK = vehCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal vehCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new Vehiculo();
			obj.VehCodPK = vehCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal VEHCODPK)
        {
            try
            {
                var e = new Vehiculo();
                return (e.LoadByPrimaryKey(VEHCODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static Vehiculo Get(System.Decimal VEHCODPK)
        {
            try
            {
                var e = new Vehiculo();
                return (e.LoadByPrimaryKey(VEHCODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public Vehiculo(System.Decimal VEHCODPK)
        {
            this.LoadByPrimaryKey(VEHCODPK);
        }
		
		public Vehiculo()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("VehiculoCol")]
	public partial class VehiculoCol : esVehiculoCol, IEnumerable<Vehiculo>
	{
	
		public VehiculoCol()
		{
		}

	
	
		public Vehiculo FindByPrimaryKey(System.Decimal vehCodPK)
		{
			return this.SingleOrDefault(e => e.VehCodPK == vehCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(Vehiculo))]
		public class VehiculoColWCFPacket : esCollectionWCFPacket<VehiculoCol>
		{
			public static implicit operator VehiculoCol(VehiculoColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator VehiculoColWCFPacket(VehiculoCol collection)
			{
				return new VehiculoColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class VehiculoQ : esVehiculoQ
	{
		public VehiculoQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public VehiculoQ()
		{
		}

		override protected string GetQueryName()
		{
			return "VehiculoQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new VehiculoQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(VehiculoQ query)
		{
			return VehiculoQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator VehiculoQ(string query)
		{
			return (VehiculoQ)VehiculoQ.SerializeHelper.FromXml(query, typeof(VehiculoQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esVehiculo : EntityBase
	{
		public esVehiculo()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal vehCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(vehCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(vehCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal vehCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(vehCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(vehCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal vehCodPK)
		{
			VehiculoQ query = new VehiculoQ();
			query.Where(query.VehCodPK == vehCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal vehCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("VehCodPK", vehCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to Vehiculos.veh_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.VehCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.VehCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.are_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.AreCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.AreCodPK, value))
				{
					this._UpToAreaByAreCodPK = null;
					this.OnPropertyChanged("UpToAreaByAreCodPK");
					OnPropertyChanged(VehiculoMetadata.PropertyNames.AreCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_Codigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehCodigo
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehCodigo);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehCodigo, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_Matricula
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehMatricula
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehMatricula);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehMatricula, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehMatricula);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.mrc_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? MrcCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.MrcCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.MrcCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.MrcCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.mdl_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? MdlCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.MdlCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.MdlCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.MdlCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_NumChasis
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehNumChasis
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehNumChasis);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehNumChasis, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehNumChasis);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_Plazas
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? VehPlazas
		{
			get
			{
				return base.GetSystemByte(VehiculoMetadata.ColumnNames.VehPlazas);
			}
			
			set
			{
				if(base.SetSystemByte(VehiculoMetadata.ColumnNames.VehPlazas, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPlazas);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.tvh_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TvhCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.TvhCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.TvhCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.TvhCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.tam_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TamCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.TamCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.TamCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.TamCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.cmp_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CmpCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.CmpCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.CmpCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.CmpCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_NumPoliza
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehNumPoliza
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehNumPoliza);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehNumPoliza, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehNumPoliza);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaCadSeguro
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaCadSeguro
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaCadSeguro);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaCadSeguro, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaCadSeguro);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_ImporteSeguro
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? VehImporteSeguro
		{
			get
			{
				return base.GetSystemDouble(VehiculoMetadata.ColumnNames.VehImporteSeguro);
			}
			
			set
			{
				if(base.SetSystemDouble(VehiculoMetadata.ColumnNames.VehImporteSeguro, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehImporteSeguro);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.lcd_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? LcdCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.LcdCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.LcdCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.LcdCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaSolLicencia
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaSolLicencia
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaSolLicencia);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaSolLicencia, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaSolLicencia);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_AnoActualPagado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? VehAnoActualPagado
		{
			get
			{
				return base.GetSystemInt16(VehiculoMetadata.ColumnNames.VehAnoActualPagado);
			}
			
			set
			{
				if(base.SetSystemInt16(VehiculoMetadata.ColumnNames.VehAnoActualPagado, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehAnoActualPagado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaSolTarjetaSant
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaSolTarjetaSant
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaSolTarjetaSant);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaSolTarjetaSant, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaSolTarjetaSant);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaCadTarjetaSant
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaCadTarjetaSant
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaCadTarjetaSant);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaCadTarjetaSant, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaCadTarjetaSant);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaSolTarjetaTransporte
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaSolTarjetaTransporte
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaSolTarjetaTransporte);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaSolTarjetaTransporte, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaSolTarjetaTransporte);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaCadTarjetaTransporte
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaCadTarjetaTransporte
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaCadTarjetaTransporte);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaCadTarjetaTransporte, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaCadTarjetaTransporte);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_NumSerieTarjetaTransporte
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehNumSerieTarjetaTransporte
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehNumSerieTarjetaTransporte);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehNumSerieTarjetaTransporte, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehNumSerieTarjetaTransporte);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaProximaITV
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaProximaITV
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaProximaITV);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaProximaITV, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaProximaITV);
				}
			}
		}		
		
		/// <summary>
		/// [OBSOLETO]. No se utiliza, se mantiene para compatibilidad.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? VehActivo
		{
			get
			{
				return base.GetSystemByte(VehiculoMetadata.ColumnNames.VehActivo);
			}
			
			set
			{
				if(base.SetSystemByte(VehiculoMetadata.ColumnNames.VehActivo, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehActivo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_ImpuestoRodaje
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? VehImpuestoRodaje
		{
			get
			{
				return base.GetSystemDouble(VehiculoMetadata.ColumnNames.VehImpuestoRodaje);
			}
			
			set
			{
				if(base.SetSystemDouble(VehiculoMetadata.ColumnNames.VehImpuestoRodaje, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehImpuestoRodaje);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_NumEmisora
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehNumEmisora
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehNumEmisora);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehNumEmisora, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehNumEmisora);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.cen_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CenCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.CenCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.CenCodPK, value))
				{
					this._UpToCentroByCenCodPK = null;
					this.OnPropertyChanged("UpToCentroByCenCodPK");
					OnPropertyChanged(VehiculoMetadata.PropertyNames.CenCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaAutorizacionTT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaAutorizacionTT
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAutorizacionTT);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAutorizacionTT, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAutorizacionTT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaAutorizacionPC
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaAutorizacionPC
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAutorizacionPC);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAutorizacionPC, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAutorizacionPC);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaVencimientoPC
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaVencimientoPC
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaVencimientoPC);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaVencimientoPC, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaVencimientoPC);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaAutorizacionCS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaAutorizacionCS
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAutorizacionCS);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAutorizacionCS, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAutorizacionCS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaAlta
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaAlta
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAlta);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAlta, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAlta);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaBaja
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaBaja
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaBaja);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaBaja, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaBaja);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_LRNum
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehLRNum
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehLRNum);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehLRNum, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehLRNum);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_LRFechaAut
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehLRFechaAut
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehLRFechaAut);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehLRFechaAut, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehLRFechaAut);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.emp_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.EmpCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.EmpCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.EmpCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_Ref
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehRef
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.VehRef);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.VehRef, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehRef);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_Titular
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehTitular
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehTitular);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehTitular, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehTitular);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_CSES
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehCSES
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehCSES);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehCSES, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehCSES);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaAplicacionBaja
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaAplicacionBaja
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAplicacionBaja);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaAplicacionBaja, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAplicacionBaja);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaUltDSNF
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaUltDSNF
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaUltDSNF);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaUltDSNF, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaUltDSNF);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_FechaPrxDSNF
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehFechaPrxDSNF
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaPrxDSNF);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehFechaPrxDSNF, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaPrxDSNF);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_PerioricidadDSNF
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehPerioricidadDSNF
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.VehPerioricidadDSNF);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.VehPerioricidadDSNF, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPerioricidadDSNF);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.emp_TitularPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpTitularPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.EmpTitularPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.EmpTitularPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.EmpTitularPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_TerminalGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? VehTerminalGPS
		{
			get
			{
				return base.GetSystemBoolean(VehiculoMetadata.ColumnNames.VehTerminalGPS);
			}
			
			set
			{
				if(base.SetSystemBoolean(VehiculoMetadata.ColumnNames.VehTerminalGPS, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehTerminalGPS);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_TerminalNS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehTerminalNS
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehTerminalNS);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehTerminalNS, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehTerminalNS);
				}
			}
		}		
		
		/// <summary>
		/// P=80 M=67 C=77
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? VehEstadoDocumentacion
		{
			get
			{
				return base.GetSystemInt16(VehiculoMetadata.ColumnNames.VehEstadoDocumentacion);
			}
			
			set
			{
				if(base.SetSystemInt16(VehiculoMetadata.ColumnNames.VehEstadoDocumentacion, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstadoDocumentacion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_EstadoCaduca
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? VehEstadoCaduca
		{
			get
			{
				return base.GetSystemInt16(VehiculoMetadata.ColumnNames.VehEstadoCaduca);
			}
			
			set
			{
				if(base.SetSystemInt16(VehiculoMetadata.ColumnNames.VehEstadoCaduca, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstadoCaduca);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_Documentar
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? VehDocumentar
		{
			get
			{
				return base.GetSystemBoolean(VehiculoMetadata.ColumnNames.VehDocumentar);
			}
			
			set
			{
				if(base.SetSystemBoolean(VehiculoMetadata.ColumnNames.VehDocumentar, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehDocumentar);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_EstadoDocumentacionPrc
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? VehEstadoDocumentacionPrc
		{
			get
			{
				return base.GetSystemInt16(VehiculoMetadata.ColumnNames.VehEstadoDocumentacionPrc);
			}
			
			set
			{
				if(base.SetSystemInt16(VehiculoMetadata.ColumnNames.VehEstadoDocumentacionPrc, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstadoDocumentacionPrc);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_EstadoInstalacionMDT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehEstadoInstalacionMDT
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.VehEstadoInstalacionMDT);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.VehEstadoInstalacionMDT, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstadoInstalacionMDT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_TelefonoExt
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehTelefonoExt
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehTelefonoExt);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehTelefonoExt, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehTelefonoExt);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_TelefonoExtension
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehTelefonoExtension
		{
			get
			{
				return base.GetSystemString(VehiculoMetadata.ColumnNames.VehTelefonoExtension);
			}
			
			set
			{
				if(base.SetSystemString(VehiculoMetadata.ColumnNames.VehTelefonoExtension, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehTelefonoExtension);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_DesinfeccionFechaProxima
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? VehDesinfeccionFechaProxima
		{
			get
			{
				return base.GetSystemDateTime(VehiculoMetadata.ColumnNames.VehDesinfeccionFechaProxima);
			}
			
			set
			{
				if(base.SetSystemDateTime(VehiculoMetadata.ColumnNames.VehDesinfeccionFechaProxima, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehDesinfeccionFechaProxima);
				}
			}
		}		
		
		/// <summary>
		/// Terminal asociado a este Vehiculo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? MdtCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.MdtCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.MdtCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.MdtCodPK);
				}
			}
		}		
		
		/// <summary>
		/// 0-Activo  1-Eliminado (Posible recuperacion)  2-Eliminado sin posibilidad de recuperacion a traves de Interfaz. Este metodo se utiliza en todos los campos estados, de las tablas que lo llevan.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehEstado
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.VehEstado);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.VehEstado, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.sim_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? SimCodPK
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.SimCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.SimCodPK, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.SimCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_PlazasSentado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehPlazasSentado
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.VehPlazasSentado);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.VehPlazasSentado, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPlazasSentado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_PlazasTendido
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehPlazasTendido
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.VehPlazasTendido);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.VehPlazasTendido, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPlazasTendido);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Vehiculos.veh_PlazasSillaRuedas
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehPlazasSillaRuedas
		{
			get
			{
				return base.GetSystemDecimal(VehiculoMetadata.ColumnNames.VehPlazasSillaRuedas);
			}
			
			set
			{
				if(base.SetSystemDecimal(VehiculoMetadata.ColumnNames.VehPlazasSillaRuedas, value))
				{
					OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPlazasSillaRuedas);
				}
			}
		}		
		
		[CLSCompliant(false)]
		internal protected Area _UpToAreaByAreCodPK;
		[CLSCompliant(false)]
		internal protected Centro _UpToCentroByCenCodPK;
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "VehCodPK": this.str().VehCodPK = (string)value; break;							
						case "AreCodPK": this.str().AreCodPK = (string)value; break;							
						case "VehCodigo": this.str().VehCodigo = (string)value; break;							
						case "VehMatricula": this.str().VehMatricula = (string)value; break;							
						case "MrcCodPK": this.str().MrcCodPK = (string)value; break;							
						case "MdlCodPK": this.str().MdlCodPK = (string)value; break;							
						case "VehNumChasis": this.str().VehNumChasis = (string)value; break;							
						case "VehPlazas": this.str().VehPlazas = (string)value; break;							
						case "TvhCodPK": this.str().TvhCodPK = (string)value; break;							
						case "TamCodPK": this.str().TamCodPK = (string)value; break;							
						case "CmpCodPK": this.str().CmpCodPK = (string)value; break;							
						case "VehNumPoliza": this.str().VehNumPoliza = (string)value; break;							
						case "VehFechaCadSeguro": this.str().VehFechaCadSeguro = (string)value; break;							
						case "VehImporteSeguro": this.str().VehImporteSeguro = (string)value; break;							
						case "LcdCodPK": this.str().LcdCodPK = (string)value; break;							
						case "VehFechaSolLicencia": this.str().VehFechaSolLicencia = (string)value; break;							
						case "VehAnoActualPagado": this.str().VehAnoActualPagado = (string)value; break;							
						case "VehFechaSolTarjetaSant": this.str().VehFechaSolTarjetaSant = (string)value; break;							
						case "VehFechaCadTarjetaSant": this.str().VehFechaCadTarjetaSant = (string)value; break;							
						case "VehFechaSolTarjetaTransporte": this.str().VehFechaSolTarjetaTransporte = (string)value; break;							
						case "VehFechaCadTarjetaTransporte": this.str().VehFechaCadTarjetaTransporte = (string)value; break;							
						case "VehNumSerieTarjetaTransporte": this.str().VehNumSerieTarjetaTransporte = (string)value; break;							
						case "VehFechaProximaITV": this.str().VehFechaProximaITV = (string)value; break;							
						case "VehActivo": this.str().VehActivo = (string)value; break;							
						case "VehImpuestoRodaje": this.str().VehImpuestoRodaje = (string)value; break;							
						case "VehNumEmisora": this.str().VehNumEmisora = (string)value; break;							
						case "CenCodPK": this.str().CenCodPK = (string)value; break;							
						case "VehFechaAutorizacionTT": this.str().VehFechaAutorizacionTT = (string)value; break;							
						case "VehFechaAutorizacionPC": this.str().VehFechaAutorizacionPC = (string)value; break;							
						case "VehFechaVencimientoPC": this.str().VehFechaVencimientoPC = (string)value; break;							
						case "VehFechaAutorizacionCS": this.str().VehFechaAutorizacionCS = (string)value; break;							
						case "VehFechaAlta": this.str().VehFechaAlta = (string)value; break;							
						case "VehFechaBaja": this.str().VehFechaBaja = (string)value; break;							
						case "VehLRNum": this.str().VehLRNum = (string)value; break;							
						case "VehLRFechaAut": this.str().VehLRFechaAut = (string)value; break;							
						case "EmpCodPK": this.str().EmpCodPK = (string)value; break;							
						case "VehRef": this.str().VehRef = (string)value; break;							
						case "VehTitular": this.str().VehTitular = (string)value; break;							
						case "VehCSES": this.str().VehCSES = (string)value; break;							
						case "VehFechaAplicacionBaja": this.str().VehFechaAplicacionBaja = (string)value; break;							
						case "VehFechaUltDSNF": this.str().VehFechaUltDSNF = (string)value; break;							
						case "VehFechaPrxDSNF": this.str().VehFechaPrxDSNF = (string)value; break;							
						case "VehPerioricidadDSNF": this.str().VehPerioricidadDSNF = (string)value; break;							
						case "EmpTitularPK": this.str().EmpTitularPK = (string)value; break;							
						case "VehTerminalGPS": this.str().VehTerminalGPS = (string)value; break;							
						case "VehTerminalNS": this.str().VehTerminalNS = (string)value; break;							
						case "VehEstadoDocumentacion": this.str().VehEstadoDocumentacion = (string)value; break;							
						case "VehEstadoCaduca": this.str().VehEstadoCaduca = (string)value; break;							
						case "VehDocumentar": this.str().VehDocumentar = (string)value; break;							
						case "VehEstadoDocumentacionPrc": this.str().VehEstadoDocumentacionPrc = (string)value; break;							
						case "VehEstadoInstalacionMDT": this.str().VehEstadoInstalacionMDT = (string)value; break;							
						case "VehTelefonoExt": this.str().VehTelefonoExt = (string)value; break;							
						case "VehTelefonoExtension": this.str().VehTelefonoExtension = (string)value; break;							
						case "VehDesinfeccionFechaProxima": this.str().VehDesinfeccionFechaProxima = (string)value; break;							
						case "MdtCodPK": this.str().MdtCodPK = (string)value; break;							
						case "VehEstado": this.str().VehEstado = (string)value; break;							
						case "SimCodPK": this.str().SimCodPK = (string)value; break;							
						case "VehPlazasSentado": this.str().VehPlazasSentado = (string)value; break;							
						case "VehPlazasTendido": this.str().VehPlazasTendido = (string)value; break;							
						case "VehPlazasSillaRuedas": this.str().VehPlazasSillaRuedas = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "VehCodPK":
						
							if (value == null || value is System.Decimal)
								this.VehCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehCodPK);
							break;
						
						case "AreCodPK":
						
							if (value == null || value is System.Decimal)
								this.AreCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.AreCodPK);
							break;
						
						case "MrcCodPK":
						
							if (value == null || value is System.Decimal)
								this.MrcCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.MrcCodPK);
							break;
						
						case "MdlCodPK":
						
							if (value == null || value is System.Decimal)
								this.MdlCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.MdlCodPK);
							break;
						
						case "VehPlazas":
						
							if (value == null || value is System.Byte)
								this.VehPlazas = (System.Byte?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPlazas);
							break;
						
						case "TvhCodPK":
						
							if (value == null || value is System.Decimal)
								this.TvhCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.TvhCodPK);
							break;
						
						case "TamCodPK":
						
							if (value == null || value is System.Decimal)
								this.TamCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.TamCodPK);
							break;
						
						case "CmpCodPK":
						
							if (value == null || value is System.Decimal)
								this.CmpCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.CmpCodPK);
							break;
						
						case "VehFechaCadSeguro":
						
							if (value == null || value is System.DateTime)
								this.VehFechaCadSeguro = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaCadSeguro);
							break;
						
						case "VehImporteSeguro":
						
							if (value == null || value is System.Double)
								this.VehImporteSeguro = (System.Double?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehImporteSeguro);
							break;
						
						case "LcdCodPK":
						
							if (value == null || value is System.Decimal)
								this.LcdCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.LcdCodPK);
							break;
						
						case "VehFechaSolLicencia":
						
							if (value == null || value is System.DateTime)
								this.VehFechaSolLicencia = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaSolLicencia);
							break;
						
						case "VehAnoActualPagado":
						
							if (value == null || value is System.Int16)
								this.VehAnoActualPagado = (System.Int16?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehAnoActualPagado);
							break;
						
						case "VehFechaSolTarjetaSant":
						
							if (value == null || value is System.DateTime)
								this.VehFechaSolTarjetaSant = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaSolTarjetaSant);
							break;
						
						case "VehFechaCadTarjetaSant":
						
							if (value == null || value is System.DateTime)
								this.VehFechaCadTarjetaSant = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaCadTarjetaSant);
							break;
						
						case "VehFechaSolTarjetaTransporte":
						
							if (value == null || value is System.DateTime)
								this.VehFechaSolTarjetaTransporte = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaSolTarjetaTransporte);
							break;
						
						case "VehFechaCadTarjetaTransporte":
						
							if (value == null || value is System.DateTime)
								this.VehFechaCadTarjetaTransporte = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaCadTarjetaTransporte);
							break;
						
						case "VehFechaProximaITV":
						
							if (value == null || value is System.DateTime)
								this.VehFechaProximaITV = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaProximaITV);
							break;
						
						case "VehActivo":
						
							if (value == null || value is System.Byte)
								this.VehActivo = (System.Byte?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehActivo);
							break;
						
						case "VehImpuestoRodaje":
						
							if (value == null || value is System.Double)
								this.VehImpuestoRodaje = (System.Double?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehImpuestoRodaje);
							break;
						
						case "CenCodPK":
						
							if (value == null || value is System.Decimal)
								this.CenCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.CenCodPK);
							break;
						
						case "VehFechaAutorizacionTT":
						
							if (value == null || value is System.DateTime)
								this.VehFechaAutorizacionTT = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAutorizacionTT);
							break;
						
						case "VehFechaAutorizacionPC":
						
							if (value == null || value is System.DateTime)
								this.VehFechaAutorizacionPC = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAutorizacionPC);
							break;
						
						case "VehFechaVencimientoPC":
						
							if (value == null || value is System.DateTime)
								this.VehFechaVencimientoPC = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaVencimientoPC);
							break;
						
						case "VehFechaAutorizacionCS":
						
							if (value == null || value is System.DateTime)
								this.VehFechaAutorizacionCS = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAutorizacionCS);
							break;
						
						case "VehFechaAlta":
						
							if (value == null || value is System.DateTime)
								this.VehFechaAlta = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAlta);
							break;
						
						case "VehFechaBaja":
						
							if (value == null || value is System.DateTime)
								this.VehFechaBaja = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaBaja);
							break;
						
						case "VehLRFechaAut":
						
							if (value == null || value is System.DateTime)
								this.VehLRFechaAut = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehLRFechaAut);
							break;
						
						case "EmpCodPK":
						
							if (value == null || value is System.Decimal)
								this.EmpCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.EmpCodPK);
							break;
						
						case "VehRef":
						
							if (value == null || value is System.Decimal)
								this.VehRef = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehRef);
							break;
						
						case "VehFechaAplicacionBaja":
						
							if (value == null || value is System.DateTime)
								this.VehFechaAplicacionBaja = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaAplicacionBaja);
							break;
						
						case "VehFechaUltDSNF":
						
							if (value == null || value is System.DateTime)
								this.VehFechaUltDSNF = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaUltDSNF);
							break;
						
						case "VehFechaPrxDSNF":
						
							if (value == null || value is System.DateTime)
								this.VehFechaPrxDSNF = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehFechaPrxDSNF);
							break;
						
						case "VehPerioricidadDSNF":
						
							if (value == null || value is System.Decimal)
								this.VehPerioricidadDSNF = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPerioricidadDSNF);
							break;
						
						case "EmpTitularPK":
						
							if (value == null || value is System.Decimal)
								this.EmpTitularPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.EmpTitularPK);
							break;
						
						case "VehTerminalGPS":
						
							if (value == null || value is System.Boolean)
								this.VehTerminalGPS = (System.Boolean?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehTerminalGPS);
							break;
						
						case "VehEstadoDocumentacion":
						
							if (value == null || value is System.Int16)
								this.VehEstadoDocumentacion = (System.Int16?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstadoDocumentacion);
							break;
						
						case "VehEstadoCaduca":
						
							if (value == null || value is System.Int16)
								this.VehEstadoCaduca = (System.Int16?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstadoCaduca);
							break;
						
						case "VehDocumentar":
						
							if (value == null || value is System.Boolean)
								this.VehDocumentar = (System.Boolean?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehDocumentar);
							break;
						
						case "VehEstadoDocumentacionPrc":
						
							if (value == null || value is System.Int16)
								this.VehEstadoDocumentacionPrc = (System.Int16?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstadoDocumentacionPrc);
							break;
						
						case "VehEstadoInstalacionMDT":
						
							if (value == null || value is System.Decimal)
								this.VehEstadoInstalacionMDT = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstadoInstalacionMDT);
							break;
						
						case "VehDesinfeccionFechaProxima":
						
							if (value == null || value is System.DateTime)
								this.VehDesinfeccionFechaProxima = (System.DateTime?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehDesinfeccionFechaProxima);
							break;
						
						case "MdtCodPK":
						
							if (value == null || value is System.Decimal)
								this.MdtCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.MdtCodPK);
							break;
						
						case "VehEstado":
						
							if (value == null || value is System.Decimal)
								this.VehEstado = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehEstado);
							break;
						
						case "SimCodPK":
						
							if (value == null || value is System.Decimal)
								this.SimCodPK = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.SimCodPK);
							break;
						
						case "VehPlazasSentado":
						
							if (value == null || value is System.Decimal)
								this.VehPlazasSentado = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPlazasSentado);
							break;
						
						case "VehPlazasTendido":
						
							if (value == null || value is System.Decimal)
								this.VehPlazasTendido = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPlazasTendido);
							break;
						
						case "VehPlazasSillaRuedas":
						
							if (value == null || value is System.Decimal)
								this.VehPlazasSillaRuedas = (System.Decimal?)value;
								OnPropertyChanged(VehiculoMetadata.PropertyNames.VehPlazasSillaRuedas);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esVehiculo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String VehCodPK
			{
				get
				{
					System.Decimal? data = entity.VehCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehCodPK = null;
					else entity.VehCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreCodPK
			{
				get
				{
					System.Decimal? data = entity.AreCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreCodPK = null;
					else entity.AreCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehCodigo
			{
				get
				{
					System.String data = entity.VehCodigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehCodigo = null;
					else entity.VehCodigo = Convert.ToString(value);
				}
			}
				
			public System.String VehMatricula
			{
				get
				{
					System.String data = entity.VehMatricula;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehMatricula = null;
					else entity.VehMatricula = Convert.ToString(value);
				}
			}
				
			public System.String MrcCodPK
			{
				get
				{
					System.Decimal? data = entity.MrcCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MrcCodPK = null;
					else entity.MrcCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String MdlCodPK
			{
				get
				{
					System.Decimal? data = entity.MdlCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdlCodPK = null;
					else entity.MdlCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehNumChasis
			{
				get
				{
					System.String data = entity.VehNumChasis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehNumChasis = null;
					else entity.VehNumChasis = Convert.ToString(value);
				}
			}
				
			public System.String VehPlazas
			{
				get
				{
					System.Byte? data = entity.VehPlazas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehPlazas = null;
					else entity.VehPlazas = Convert.ToByte(value);
				}
			}
				
			public System.String TvhCodPK
			{
				get
				{
					System.Decimal? data = entity.TvhCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TvhCodPK = null;
					else entity.TvhCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String TamCodPK
			{
				get
				{
					System.Decimal? data = entity.TamCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TamCodPK = null;
					else entity.TamCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String CmpCodPK
			{
				get
				{
					System.Decimal? data = entity.CmpCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CmpCodPK = null;
					else entity.CmpCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehNumPoliza
			{
				get
				{
					System.String data = entity.VehNumPoliza;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehNumPoliza = null;
					else entity.VehNumPoliza = Convert.ToString(value);
				}
			}
				
			public System.String VehFechaCadSeguro
			{
				get
				{
					System.DateTime? data = entity.VehFechaCadSeguro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaCadSeguro = null;
					else entity.VehFechaCadSeguro = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehImporteSeguro
			{
				get
				{
					System.Double? data = entity.VehImporteSeguro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehImporteSeguro = null;
					else entity.VehImporteSeguro = Convert.ToDouble(value);
				}
			}
				
			public System.String LcdCodPK
			{
				get
				{
					System.Decimal? data = entity.LcdCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LcdCodPK = null;
					else entity.LcdCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehFechaSolLicencia
			{
				get
				{
					System.DateTime? data = entity.VehFechaSolLicencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaSolLicencia = null;
					else entity.VehFechaSolLicencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehAnoActualPagado
			{
				get
				{
					System.Int16? data = entity.VehAnoActualPagado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehAnoActualPagado = null;
					else entity.VehAnoActualPagado = Convert.ToInt16(value);
				}
			}
				
			public System.String VehFechaSolTarjetaSant
			{
				get
				{
					System.DateTime? data = entity.VehFechaSolTarjetaSant;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaSolTarjetaSant = null;
					else entity.VehFechaSolTarjetaSant = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaCadTarjetaSant
			{
				get
				{
					System.DateTime? data = entity.VehFechaCadTarjetaSant;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaCadTarjetaSant = null;
					else entity.VehFechaCadTarjetaSant = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaSolTarjetaTransporte
			{
				get
				{
					System.DateTime? data = entity.VehFechaSolTarjetaTransporte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaSolTarjetaTransporte = null;
					else entity.VehFechaSolTarjetaTransporte = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaCadTarjetaTransporte
			{
				get
				{
					System.DateTime? data = entity.VehFechaCadTarjetaTransporte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaCadTarjetaTransporte = null;
					else entity.VehFechaCadTarjetaTransporte = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehNumSerieTarjetaTransporte
			{
				get
				{
					System.String data = entity.VehNumSerieTarjetaTransporte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehNumSerieTarjetaTransporte = null;
					else entity.VehNumSerieTarjetaTransporte = Convert.ToString(value);
				}
			}
				
			public System.String VehFechaProximaITV
			{
				get
				{
					System.DateTime? data = entity.VehFechaProximaITV;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaProximaITV = null;
					else entity.VehFechaProximaITV = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehActivo
			{
				get
				{
					System.Byte? data = entity.VehActivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehActivo = null;
					else entity.VehActivo = Convert.ToByte(value);
				}
			}
				
			public System.String VehImpuestoRodaje
			{
				get
				{
					System.Double? data = entity.VehImpuestoRodaje;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehImpuestoRodaje = null;
					else entity.VehImpuestoRodaje = Convert.ToDouble(value);
				}
			}
				
			public System.String VehNumEmisora
			{
				get
				{
					System.String data = entity.VehNumEmisora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehNumEmisora = null;
					else entity.VehNumEmisora = Convert.ToString(value);
				}
			}
				
			public System.String CenCodPK
			{
				get
				{
					System.Decimal? data = entity.CenCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CenCodPK = null;
					else entity.CenCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehFechaAutorizacionTT
			{
				get
				{
					System.DateTime? data = entity.VehFechaAutorizacionTT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaAutorizacionTT = null;
					else entity.VehFechaAutorizacionTT = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaAutorizacionPC
			{
				get
				{
					System.DateTime? data = entity.VehFechaAutorizacionPC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaAutorizacionPC = null;
					else entity.VehFechaAutorizacionPC = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaVencimientoPC
			{
				get
				{
					System.DateTime? data = entity.VehFechaVencimientoPC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaVencimientoPC = null;
					else entity.VehFechaVencimientoPC = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaAutorizacionCS
			{
				get
				{
					System.DateTime? data = entity.VehFechaAutorizacionCS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaAutorizacionCS = null;
					else entity.VehFechaAutorizacionCS = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaAlta
			{
				get
				{
					System.DateTime? data = entity.VehFechaAlta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaAlta = null;
					else entity.VehFechaAlta = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaBaja
			{
				get
				{
					System.DateTime? data = entity.VehFechaBaja;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaBaja = null;
					else entity.VehFechaBaja = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehLRNum
			{
				get
				{
					System.String data = entity.VehLRNum;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehLRNum = null;
					else entity.VehLRNum = Convert.ToString(value);
				}
			}
				
			public System.String VehLRFechaAut
			{
				get
				{
					System.DateTime? data = entity.VehLRFechaAut;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehLRFechaAut = null;
					else entity.VehLRFechaAut = Convert.ToDateTime(value);
				}
			}
				
			public System.String EmpCodPK
			{
				get
				{
					System.Decimal? data = entity.EmpCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpCodPK = null;
					else entity.EmpCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehRef
			{
				get
				{
					System.Decimal? data = entity.VehRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehRef = null;
					else entity.VehRef = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehTitular
			{
				get
				{
					System.String data = entity.VehTitular;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehTitular = null;
					else entity.VehTitular = Convert.ToString(value);
				}
			}
				
			public System.String VehCSES
			{
				get
				{
					System.String data = entity.VehCSES;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehCSES = null;
					else entity.VehCSES = Convert.ToString(value);
				}
			}
				
			public System.String VehFechaAplicacionBaja
			{
				get
				{
					System.DateTime? data = entity.VehFechaAplicacionBaja;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaAplicacionBaja = null;
					else entity.VehFechaAplicacionBaja = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaUltDSNF
			{
				get
				{
					System.DateTime? data = entity.VehFechaUltDSNF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaUltDSNF = null;
					else entity.VehFechaUltDSNF = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehFechaPrxDSNF
			{
				get
				{
					System.DateTime? data = entity.VehFechaPrxDSNF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehFechaPrxDSNF = null;
					else entity.VehFechaPrxDSNF = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehPerioricidadDSNF
			{
				get
				{
					System.Decimal? data = entity.VehPerioricidadDSNF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehPerioricidadDSNF = null;
					else entity.VehPerioricidadDSNF = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpTitularPK
			{
				get
				{
					System.Decimal? data = entity.EmpTitularPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpTitularPK = null;
					else entity.EmpTitularPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehTerminalGPS
			{
				get
				{
					System.Boolean? data = entity.VehTerminalGPS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehTerminalGPS = null;
					else entity.VehTerminalGPS = Convert.ToBoolean(value);
				}
			}
				
			public System.String VehTerminalNS
			{
				get
				{
					System.String data = entity.VehTerminalNS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehTerminalNS = null;
					else entity.VehTerminalNS = Convert.ToString(value);
				}
			}
				
			public System.String VehEstadoDocumentacion
			{
				get
				{
					System.Int16? data = entity.VehEstadoDocumentacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehEstadoDocumentacion = null;
					else entity.VehEstadoDocumentacion = Convert.ToInt16(value);
				}
			}
				
			public System.String VehEstadoCaduca
			{
				get
				{
					System.Int16? data = entity.VehEstadoCaduca;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehEstadoCaduca = null;
					else entity.VehEstadoCaduca = Convert.ToInt16(value);
				}
			}
				
			public System.String VehDocumentar
			{
				get
				{
					System.Boolean? data = entity.VehDocumentar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehDocumentar = null;
					else entity.VehDocumentar = Convert.ToBoolean(value);
				}
			}
				
			public System.String VehEstadoDocumentacionPrc
			{
				get
				{
					System.Int16? data = entity.VehEstadoDocumentacionPrc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehEstadoDocumentacionPrc = null;
					else entity.VehEstadoDocumentacionPrc = Convert.ToInt16(value);
				}
			}
				
			public System.String VehEstadoInstalacionMDT
			{
				get
				{
					System.Decimal? data = entity.VehEstadoInstalacionMDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehEstadoInstalacionMDT = null;
					else entity.VehEstadoInstalacionMDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehTelefonoExt
			{
				get
				{
					System.String data = entity.VehTelefonoExt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehTelefonoExt = null;
					else entity.VehTelefonoExt = Convert.ToString(value);
				}
			}
				
			public System.String VehTelefonoExtension
			{
				get
				{
					System.String data = entity.VehTelefonoExtension;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehTelefonoExtension = null;
					else entity.VehTelefonoExtension = Convert.ToString(value);
				}
			}
				
			public System.String VehDesinfeccionFechaProxima
			{
				get
				{
					System.DateTime? data = entity.VehDesinfeccionFechaProxima;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehDesinfeccionFechaProxima = null;
					else entity.VehDesinfeccionFechaProxima = Convert.ToDateTime(value);
				}
			}
				
			public System.String MdtCodPK
			{
				get
				{
					System.Decimal? data = entity.MdtCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MdtCodPK = null;
					else entity.MdtCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehEstado
			{
				get
				{
					System.Decimal? data = entity.VehEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehEstado = null;
					else entity.VehEstado = Convert.ToDecimal(value);
				}
			}
				
			public System.String SimCodPK
			{
				get
				{
					System.Decimal? data = entity.SimCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SimCodPK = null;
					else entity.SimCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehPlazasSentado
			{
				get
				{
					System.Decimal? data = entity.VehPlazasSentado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehPlazasSentado = null;
					else entity.VehPlazasSentado = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehPlazasTendido
			{
				get
				{
					System.Decimal? data = entity.VehPlazasTendido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehPlazasTendido = null;
					else entity.VehPlazasTendido = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehPlazasSillaRuedas
			{
				get
				{
					System.Decimal? data = entity.VehPlazasSillaRuedas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehPlazasSillaRuedas = null;
					else entity.VehPlazasSillaRuedas = Convert.ToDecimal(value);
				}
			}
			

			private esVehiculo entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return VehiculoMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public VehiculoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VehiculoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(VehiculoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(VehiculoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private VehiculoQ query;		
	}



	[Serializable]
	abstract public partial class esVehiculoCol : CollectionBase<Vehiculo>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return VehiculoMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "VehiculoCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public VehiculoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VehiculoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(VehiculoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VehiculoQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(VehiculoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((VehiculoQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private VehiculoQ query;
	}



	[Serializable]
	abstract public partial class esVehiculoQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return VehiculoMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.AreCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.AreCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehCodigo,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehCodigo, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehMatricula,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehMatricula, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.MrcCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.MrcCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.MdlCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.MdlCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehNumChasis,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehNumChasis, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehPlazas,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPlazas, esSystemType.Byte));
			_queryItems.Add(VehiculoMetadata.ColumnNames.TvhCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.TvhCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.TamCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.TamCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.CmpCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.CmpCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehNumPoliza,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehNumPoliza, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaCadSeguro,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaCadSeguro, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehImporteSeguro,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehImporteSeguro, esSystemType.Double));
			_queryItems.Add(VehiculoMetadata.ColumnNames.LcdCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.LcdCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaSolLicencia,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaSolLicencia, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehAnoActualPagado,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehAnoActualPagado, esSystemType.Int16));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaSolTarjetaSant,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaSolTarjetaSant, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaCadTarjetaSant,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaCadTarjetaSant, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaSolTarjetaTransporte,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaSolTarjetaTransporte, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaCadTarjetaTransporte,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaCadTarjetaTransporte, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehNumSerieTarjetaTransporte,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehNumSerieTarjetaTransporte, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaProximaITV,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaProximaITV, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehActivo,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehActivo, esSystemType.Byte));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehImpuestoRodaje,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehImpuestoRodaje, esSystemType.Double));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehNumEmisora,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehNumEmisora, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.CenCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.CenCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaAutorizacionTT,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAutorizacionTT, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaAutorizacionPC,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAutorizacionPC, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaVencimientoPC,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaVencimientoPC, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaAutorizacionCS,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAutorizacionCS, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaAlta,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAlta, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaBaja,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaBaja, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehLRNum,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehLRNum, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehLRFechaAut,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehLRFechaAut, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.EmpCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehRef,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehRef, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehTitular,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTitular, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehCSES,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehCSES, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaAplicacionBaja,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAplicacionBaja, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaUltDSNF,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaUltDSNF, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehFechaPrxDSNF,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaPrxDSNF, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehPerioricidadDSNF,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPerioricidadDSNF, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.EmpTitularPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.EmpTitularPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehTerminalGPS,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTerminalGPS, esSystemType.Boolean));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehTerminalNS,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTerminalNS, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehEstadoDocumentacion,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstadoDocumentacion, esSystemType.Int16));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehEstadoCaduca,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstadoCaduca, esSystemType.Int16));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehDocumentar,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehDocumentar, esSystemType.Boolean));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehEstadoDocumentacionPrc,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstadoDocumentacionPrc, esSystemType.Int16));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehEstadoInstalacionMDT,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstadoInstalacionMDT, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehTelefonoExt,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTelefonoExt, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehTelefonoExtension,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTelefonoExtension, esSystemType.String));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehDesinfeccionFechaProxima,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehDesinfeccionFechaProxima, esSystemType.DateTime));
			_queryItems.Add(VehiculoMetadata.ColumnNames.MdtCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.MdtCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehEstado,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstado, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.SimCodPK,new esQueryItem(this, VehiculoMetadata.ColumnNames.SimCodPK, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehPlazasSentado,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPlazasSentado, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehPlazasTendido,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPlazasTendido, esSystemType.Decimal));
			_queryItems.Add(VehiculoMetadata.ColumnNames.VehPlazasSillaRuedas,new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPlazasSillaRuedas, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem VehCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.AreCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehCodigo
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehCodigo, esSystemType.String); }
		} 
		
		public esQueryItem VehMatricula
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehMatricula, esSystemType.String); }
		} 
		
		public esQueryItem MrcCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.MrcCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem MdlCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.MdlCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehNumChasis
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehNumChasis, esSystemType.String); }
		} 
		
		public esQueryItem VehPlazas
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPlazas, esSystemType.Byte); }
		} 
		
		public esQueryItem TvhCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.TvhCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem TamCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.TamCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem CmpCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.CmpCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehNumPoliza
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehNumPoliza, esSystemType.String); }
		} 
		
		public esQueryItem VehFechaCadSeguro
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaCadSeguro, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehImporteSeguro
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehImporteSeguro, esSystemType.Double); }
		} 
		
		public esQueryItem LcdCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.LcdCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehFechaSolLicencia
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaSolLicencia, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehAnoActualPagado
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehAnoActualPagado, esSystemType.Int16); }
		} 
		
		public esQueryItem VehFechaSolTarjetaSant
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaSolTarjetaSant, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaCadTarjetaSant
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaCadTarjetaSant, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaSolTarjetaTransporte
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaSolTarjetaTransporte, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaCadTarjetaTransporte
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaCadTarjetaTransporte, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehNumSerieTarjetaTransporte
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehNumSerieTarjetaTransporte, esSystemType.String); }
		} 
		
		public esQueryItem VehFechaProximaITV
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaProximaITV, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehActivo
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehActivo, esSystemType.Byte); }
		} 
		
		public esQueryItem VehImpuestoRodaje
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehImpuestoRodaje, esSystemType.Double); }
		} 
		
		public esQueryItem VehNumEmisora
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehNumEmisora, esSystemType.String); }
		} 
		
		public esQueryItem CenCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.CenCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehFechaAutorizacionTT
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAutorizacionTT, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaAutorizacionPC
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAutorizacionPC, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaVencimientoPC
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaVencimientoPC, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaAutorizacionCS
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAutorizacionCS, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaAlta
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAlta, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaBaja
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaBaja, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehLRNum
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehLRNum, esSystemType.String); }
		} 
		
		public esQueryItem VehLRFechaAut
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehLRFechaAut, esSystemType.DateTime); }
		} 
		
		public esQueryItem EmpCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.EmpCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehRef
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehRef, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehTitular
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTitular, esSystemType.String); }
		} 
		
		public esQueryItem VehCSES
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehCSES, esSystemType.String); }
		} 
		
		public esQueryItem VehFechaAplicacionBaja
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaAplicacionBaja, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaUltDSNF
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaUltDSNF, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehFechaPrxDSNF
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehFechaPrxDSNF, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehPerioricidadDSNF
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPerioricidadDSNF, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpTitularPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.EmpTitularPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehTerminalGPS
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTerminalGPS, esSystemType.Boolean); }
		} 
		
		public esQueryItem VehTerminalNS
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTerminalNS, esSystemType.String); }
		} 
		
		public esQueryItem VehEstadoDocumentacion
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstadoDocumentacion, esSystemType.Int16); }
		} 
		
		public esQueryItem VehEstadoCaduca
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstadoCaduca, esSystemType.Int16); }
		} 
		
		public esQueryItem VehDocumentar
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehDocumentar, esSystemType.Boolean); }
		} 
		
		public esQueryItem VehEstadoDocumentacionPrc
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstadoDocumentacionPrc, esSystemType.Int16); }
		} 
		
		public esQueryItem VehEstadoInstalacionMDT
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstadoInstalacionMDT, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehTelefonoExt
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTelefonoExt, esSystemType.String); }
		} 
		
		public esQueryItem VehTelefonoExtension
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehTelefonoExtension, esSystemType.String); }
		} 
		
		public esQueryItem VehDesinfeccionFechaProxima
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehDesinfeccionFechaProxima, esSystemType.DateTime); }
		} 
		
		public esQueryItem MdtCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.MdtCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehEstado
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehEstado, esSystemType.Decimal); }
		} 
		
		public esQueryItem SimCodPK
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.SimCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehPlazasSentado
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPlazasSentado, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehPlazasTendido
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPlazasTendido, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehPlazasSillaRuedas
		{
			get { return new esQueryItem(this, VehiculoMetadata.ColumnNames.VehPlazasSillaRuedas, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class Vehiculo : esVehiculo
	{

				
		#region UpToAreaByAreCodPK - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_Vehiculos_Areas
		/// </summary>

		[XmlIgnore]
					
		public Area UpToAreaByAreCodPK
		{
			get
			{
				if (this.es.IsLazyLoadDisabled) return null;
				
				if(this._UpToAreaByAreCodPK == null && AreCodPK != null)
				{
					this._UpToAreaByAreCodPK = new Area();
					this._UpToAreaByAreCodPK.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAreaByAreCodPK", this._UpToAreaByAreCodPK);
					this._UpToAreaByAreCodPK.Query.Where(this._UpToAreaByAreCodPK.Query.AreCodPK == this.AreCodPK);
					this._UpToAreaByAreCodPK.Query.Load();
				}	
				return this._UpToAreaByAreCodPK;
			}
			
			set
			{
				this.RemovePreSave("UpToAreaByAreCodPK");
				
				bool changed = this._UpToAreaByAreCodPK != value;

				if(value == null)
				{
					this.AreCodPK = null;
					this._UpToAreaByAreCodPK = null;
				}
				else
				{
					this.AreCodPK = value.AreCodPK;
					this._UpToAreaByAreCodPK = value;
					this.SetPreSave("UpToAreaByAreCodPK", this._UpToAreaByAreCodPK);
				}
				
				if( changed )
				{
					this.OnPropertyChanged("UpToAreaByAreCodPK");
				}
			}
		}
		#endregion
		

				
		#region UpToCentroByCenCodPK - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_Vehiculos_Centros
		/// </summary>

		[XmlIgnore]
					
		public Centro UpToCentroByCenCodPK
		{
			get
			{
				if (this.es.IsLazyLoadDisabled) return null;
				
				if(this._UpToCentroByCenCodPK == null && CenCodPK != null)
				{
					this._UpToCentroByCenCodPK = new Centro();
					this._UpToCentroByCenCodPK.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCentroByCenCodPK", this._UpToCentroByCenCodPK);
					this._UpToCentroByCenCodPK.Query.Where(this._UpToCentroByCenCodPK.Query.CenCodPK == this.CenCodPK);
					this._UpToCentroByCenCodPK.Query.Load();
				}	
				return this._UpToCentroByCenCodPK;
			}
			
			set
			{
				this.RemovePreSave("UpToCentroByCenCodPK");
				
				bool changed = this._UpToCentroByCenCodPK != value;

				if(value == null)
				{
					this.CenCodPK = null;
					this._UpToCentroByCenCodPK = null;
				}
				else
				{
					this.CenCodPK = value.CenCodPK;
					this._UpToCentroByCenCodPK = value;
					this.SetPreSave("UpToCentroByCenCodPK", this._UpToCentroByCenCodPK);
				}
				
				if( changed )
				{
					this.OnPropertyChanged("UpToCentroByCenCodPK");
				}
			}
		}
		#endregion
		

		
		
	}
	



	[Serializable]
	public partial class VehiculoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected VehiculoMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.AreCodPK, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.AreCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehCodigo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehCodigo;
			c.CharacterMaxLength = 10;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehMatricula, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehMatricula;
			c.CharacterMaxLength = 10;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.MrcCodPK, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.MrcCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.MdlCodPK, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.MdlCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehNumChasis, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehNumChasis;
			c.CharacterMaxLength = 25;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehPlazas, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehPlazas;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.TvhCodPK, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.TvhCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.TamCodPK, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.TamCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.CmpCodPK, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.CmpCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehNumPoliza, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehNumPoliza;
			c.CharacterMaxLength = 25;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaCadSeguro, 12, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaCadSeguro;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehImporteSeguro, 13, typeof(System.Double), esSystemType.Double);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehImporteSeguro;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.LcdCodPK, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.LcdCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaSolLicencia, 15, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaSolLicencia;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehAnoActualPagado, 16, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehAnoActualPagado;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaSolTarjetaSant, 17, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaSolTarjetaSant;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaCadTarjetaSant, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaCadTarjetaSant;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaSolTarjetaTransporte, 19, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaSolTarjetaTransporte;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaCadTarjetaTransporte, 20, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaCadTarjetaTransporte;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehNumSerieTarjetaTransporte, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehNumSerieTarjetaTransporte;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaProximaITV, 22, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaProximaITV;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehActivo, 23, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehActivo;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "[OBSOLETO]. No se utiliza, se mantiene para compatibilidad.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehImpuestoRodaje, 24, typeof(System.Double), esSystemType.Double);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehImpuestoRodaje;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehNumEmisora, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehNumEmisora;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.CenCodPK, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.CenCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaAutorizacionTT, 27, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaAutorizacionTT;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaAutorizacionPC, 28, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaAutorizacionPC;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaVencimientoPC, 29, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaVencimientoPC;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaAutorizacionCS, 30, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaAutorizacionCS;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaAlta, 31, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaAlta;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaBaja, 32, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaBaja;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehLRNum, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehLRNum;
			c.CharacterMaxLength = 20;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehLRFechaAut, 34, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehLRFechaAut;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.EmpCodPK, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.EmpCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehRef, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehRef;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehTitular, 37, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehTitular;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehCSES, 38, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehCSES;
			c.CharacterMaxLength = 20;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaAplicacionBaja, 39, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaAplicacionBaja;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaUltDSNF, 40, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaUltDSNF;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehFechaPrxDSNF, 41, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehFechaPrxDSNF;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehPerioricidadDSNF, 42, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehPerioricidadDSNF;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.EmpTitularPK, 43, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.EmpTitularPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehTerminalGPS, 44, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehTerminalGPS;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehTerminalNS, 45, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehTerminalNS;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehEstadoDocumentacion, 46, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehEstadoDocumentacion;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((80))";
			c.Description = "P=80 M=67 C=77";
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehEstadoCaduca, 47, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehEstadoCaduca;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((77))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehDocumentar, 48, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehDocumentar;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehEstadoDocumentacionPrc, 49, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehEstadoDocumentacionPrc;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehEstadoInstalacionMDT, 50, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehEstadoInstalacionMDT;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehTelefonoExt, 51, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehTelefonoExt;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehTelefonoExtension, 52, typeof(System.String), esSystemType.String);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehTelefonoExtension;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehDesinfeccionFechaProxima, 53, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehDesinfeccionFechaProxima;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.MdtCodPK, 54, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.MdtCodPK;
			c.NumericPrecision = 18;
			c.Description = "Terminal asociado a este Vehiculo";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehEstado, 55, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehEstado;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "0-Activo  1-Eliminado (Posible recuperacion)  2-Eliminado sin posibilidad de recuperacion a traves de Interfaz. Este metodo se utiliza en todos los campos estados, de las tablas que lo llevan.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.SimCodPK, 56, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.SimCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehPlazasSentado, 57, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehPlazasSentado;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehPlazasTendido, 58, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehPlazasTendido;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(VehiculoMetadata.ColumnNames.VehPlazasSillaRuedas, 59, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VehiculoMetadata.PropertyNames.VehPlazasSillaRuedas;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public VehiculoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string VehCodPK = "veh_CodPK";
			 public const string AreCodPK = "are_CodPK";
			 public const string VehCodigo = "veh_Codigo";
			 public const string VehMatricula = "veh_Matricula";
			 public const string MrcCodPK = "mrc_CodPK";
			 public const string MdlCodPK = "mdl_CodPK";
			 public const string VehNumChasis = "veh_NumChasis";
			 public const string VehPlazas = "veh_Plazas";
			 public const string TvhCodPK = "tvh_CodPK";
			 public const string TamCodPK = "tam_CodPK";
			 public const string CmpCodPK = "cmp_CodPK";
			 public const string VehNumPoliza = "veh_NumPoliza";
			 public const string VehFechaCadSeguro = "veh_FechaCadSeguro";
			 public const string VehImporteSeguro = "veh_ImporteSeguro";
			 public const string LcdCodPK = "lcd_CodPK";
			 public const string VehFechaSolLicencia = "veh_FechaSolLicencia";
			 public const string VehAnoActualPagado = "veh_AnoActualPagado";
			 public const string VehFechaSolTarjetaSant = "veh_FechaSolTarjetaSant";
			 public const string VehFechaCadTarjetaSant = "veh_FechaCadTarjetaSant";
			 public const string VehFechaSolTarjetaTransporte = "veh_FechaSolTarjetaTransporte";
			 public const string VehFechaCadTarjetaTransporte = "veh_FechaCadTarjetaTransporte";
			 public const string VehNumSerieTarjetaTransporte = "veh_NumSerieTarjetaTransporte";
			 public const string VehFechaProximaITV = "veh_FechaProximaITV";
			 public const string VehActivo = "veh_Activo";
			 public const string VehImpuestoRodaje = "veh_ImpuestoRodaje";
			 public const string VehNumEmisora = "veh_NumEmisora";
			 public const string CenCodPK = "cen_CodPK";
			 public const string VehFechaAutorizacionTT = "veh_FechaAutorizacionTT";
			 public const string VehFechaAutorizacionPC = "veh_FechaAutorizacionPC";
			 public const string VehFechaVencimientoPC = "veh_FechaVencimientoPC";
			 public const string VehFechaAutorizacionCS = "veh_FechaAutorizacionCS";
			 public const string VehFechaAlta = "veh_FechaAlta";
			 public const string VehFechaBaja = "veh_FechaBaja";
			 public const string VehLRNum = "veh_LRNum";
			 public const string VehLRFechaAut = "veh_LRFechaAut";
			 public const string EmpCodPK = "emp_CodPK";
			 public const string VehRef = "veh_Ref";
			 public const string VehTitular = "veh_Titular";
			 public const string VehCSES = "veh_CSES";
			 public const string VehFechaAplicacionBaja = "veh_FechaAplicacionBaja";
			 public const string VehFechaUltDSNF = "veh_FechaUltDSNF";
			 public const string VehFechaPrxDSNF = "veh_FechaPrxDSNF";
			 public const string VehPerioricidadDSNF = "veh_PerioricidadDSNF";
			 public const string EmpTitularPK = "emp_TitularPK";
			 public const string VehTerminalGPS = "veh_TerminalGPS";
			 public const string VehTerminalNS = "veh_TerminalNS";
			 public const string VehEstadoDocumentacion = "veh_EstadoDocumentacion";
			 public const string VehEstadoCaduca = "veh_EstadoCaduca";
			 public const string VehDocumentar = "veh_Documentar";
			 public const string VehEstadoDocumentacionPrc = "veh_EstadoDocumentacionPrc";
			 public const string VehEstadoInstalacionMDT = "veh_EstadoInstalacionMDT";
			 public const string VehTelefonoExt = "veh_TelefonoExt";
			 public const string VehTelefonoExtension = "veh_TelefonoExtension";
			 public const string VehDesinfeccionFechaProxima = "veh_DesinfeccionFechaProxima";
			 public const string MdtCodPK = "mdt_CodPK";
			 public const string VehEstado = "veh_Estado";
			 public const string SimCodPK = "sim_CodPK";
			 public const string VehPlazasSentado = "veh_PlazasSentado";
			 public const string VehPlazasTendido = "veh_PlazasTendido";
			 public const string VehPlazasSillaRuedas = "veh_PlazasSillaRuedas";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string VehCodPK = "VehCodPK";
			 public const string AreCodPK = "AreCodPK";
			 public const string VehCodigo = "VehCodigo";
			 public const string VehMatricula = "VehMatricula";
			 public const string MrcCodPK = "MrcCodPK";
			 public const string MdlCodPK = "MdlCodPK";
			 public const string VehNumChasis = "VehNumChasis";
			 public const string VehPlazas = "VehPlazas";
			 public const string TvhCodPK = "TvhCodPK";
			 public const string TamCodPK = "TamCodPK";
			 public const string CmpCodPK = "CmpCodPK";
			 public const string VehNumPoliza = "VehNumPoliza";
			 public const string VehFechaCadSeguro = "VehFechaCadSeguro";
			 public const string VehImporteSeguro = "VehImporteSeguro";
			 public const string LcdCodPK = "LcdCodPK";
			 public const string VehFechaSolLicencia = "VehFechaSolLicencia";
			 public const string VehAnoActualPagado = "VehAnoActualPagado";
			 public const string VehFechaSolTarjetaSant = "VehFechaSolTarjetaSant";
			 public const string VehFechaCadTarjetaSant = "VehFechaCadTarjetaSant";
			 public const string VehFechaSolTarjetaTransporte = "VehFechaSolTarjetaTransporte";
			 public const string VehFechaCadTarjetaTransporte = "VehFechaCadTarjetaTransporte";
			 public const string VehNumSerieTarjetaTransporte = "VehNumSerieTarjetaTransporte";
			 public const string VehFechaProximaITV = "VehFechaProximaITV";
			 public const string VehActivo = "VehActivo";
			 public const string VehImpuestoRodaje = "VehImpuestoRodaje";
			 public const string VehNumEmisora = "VehNumEmisora";
			 public const string CenCodPK = "CenCodPK";
			 public const string VehFechaAutorizacionTT = "VehFechaAutorizacionTT";
			 public const string VehFechaAutorizacionPC = "VehFechaAutorizacionPC";
			 public const string VehFechaVencimientoPC = "VehFechaVencimientoPC";
			 public const string VehFechaAutorizacionCS = "VehFechaAutorizacionCS";
			 public const string VehFechaAlta = "VehFechaAlta";
			 public const string VehFechaBaja = "VehFechaBaja";
			 public const string VehLRNum = "VehLRNum";
			 public const string VehLRFechaAut = "VehLRFechaAut";
			 public const string EmpCodPK = "EmpCodPK";
			 public const string VehRef = "VehRef";
			 public const string VehTitular = "VehTitular";
			 public const string VehCSES = "VehCSES";
			 public const string VehFechaAplicacionBaja = "VehFechaAplicacionBaja";
			 public const string VehFechaUltDSNF = "VehFechaUltDSNF";
			 public const string VehFechaPrxDSNF = "VehFechaPrxDSNF";
			 public const string VehPerioricidadDSNF = "VehPerioricidadDSNF";
			 public const string EmpTitularPK = "EmpTitularPK";
			 public const string VehTerminalGPS = "VehTerminalGPS";
			 public const string VehTerminalNS = "VehTerminalNS";
			 public const string VehEstadoDocumentacion = "VehEstadoDocumentacion";
			 public const string VehEstadoCaduca = "VehEstadoCaduca";
			 public const string VehDocumentar = "VehDocumentar";
			 public const string VehEstadoDocumentacionPrc = "VehEstadoDocumentacionPrc";
			 public const string VehEstadoInstalacionMDT = "VehEstadoInstalacionMDT";
			 public const string VehTelefonoExt = "VehTelefonoExt";
			 public const string VehTelefonoExtension = "VehTelefonoExtension";
			 public const string VehDesinfeccionFechaProxima = "VehDesinfeccionFechaProxima";
			 public const string MdtCodPK = "MdtCodPK";
			 public const string VehEstado = "VehEstado";
			 public const string SimCodPK = "SimCodPK";
			 public const string VehPlazasSentado = "VehPlazasSentado";
			 public const string VehPlazasTendido = "VehPlazasTendido";
			 public const string VehPlazasSillaRuedas = "VehPlazasSillaRuedas";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(VehiculoMetadata))
			{
				if(VehiculoMetadata.mapDelegates == null)
				{
					VehiculoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (VehiculoMetadata.meta == null)
				{
					VehiculoMetadata.meta = new VehiculoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("VehCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehCodigo", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehMatricula", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("MrcCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("MdlCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehNumChasis", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehPlazas", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TvhCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TamCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("CmpCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehNumPoliza", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehFechaCadSeguro", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehImporteSeguro", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("LcdCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehFechaSolLicencia", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehAnoActualPagado", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("VehFechaSolTarjetaSant", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaCadTarjetaSant", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaSolTarjetaTransporte", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaCadTarjetaTransporte", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehNumSerieTarjetaTransporte", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehFechaProximaITV", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehActivo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("VehImpuestoRodaje", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("VehNumEmisora", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("CenCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehFechaAutorizacionTT", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaAutorizacionPC", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaVencimientoPC", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaAutorizacionCS", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaAlta", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaBaja", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehLRNum", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehLRFechaAut", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("EmpCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehRef", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehTitular", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehCSES", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehFechaAplicacionBaja", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaUltDSNF", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehFechaPrxDSNF", new esTypeMap("smalldatetime", "System.DateTime"));
				meta.AddTypeMap("VehPerioricidadDSNF", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpTitularPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehTerminalGPS", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("VehTerminalNS", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehEstadoDocumentacion", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("VehEstadoCaduca", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("VehDocumentar", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("VehEstadoDocumentacionPrc", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("VehEstadoInstalacionMDT", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehTelefonoExt", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehTelefonoExtension", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehDesinfeccionFechaProxima", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("MdtCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehEstado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("SimCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehPlazasSentado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehPlazasTendido", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehPlazasSillaRuedas", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "Vehiculos";
				meta.Destination = "Vehiculos";
				
				meta.spInsert = "proc_VehiculosInsert";				
				meta.spUpdate = "proc_VehiculosUpdate";		
				meta.spDelete = "proc_VehiculosDelete";
				meta.spLoadAll = "proc_VehiculosLoadAll";
				meta.spLoadByPrimaryKey = "proc_VehiculosLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private VehiculoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
