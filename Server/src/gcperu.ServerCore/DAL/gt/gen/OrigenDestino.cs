
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 22/06/2015 11:59:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GT
{
	/// <summary>
	/// Encapsulates the 'OrigenDestino' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("OrigenDestino")]
	public partial class OrigenDestino : esOrigenDestino
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new OrigenDestino();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal odoCodPK)
		{
			var obj = new OrigenDestino();
			obj.OdoCodPK = odoCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal odoCodPK, esSqlAccessType sqlAccessType)
		{
			var obj = new OrigenDestino();
			obj.OdoCodPK = odoCodPK;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ODOCODPK)
        {
            try
            {
                var e = new OrigenDestino();
                return (e.LoadByPrimaryKey(ODOCODPK));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static OrigenDestino Get(System.Decimal ODOCODPK)
        {
            try
            {
                var e = new OrigenDestino();
                return (e.LoadByPrimaryKey(ODOCODPK) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public OrigenDestino(System.Decimal ODOCODPK)
        {
            this.LoadByPrimaryKey(ODOCODPK);
        }
		
		public OrigenDestino()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("OrigenDestinoCol")]
	public partial class OrigenDestinoCol : esOrigenDestinoCol, IEnumerable<OrigenDestino>
	{
	
		public OrigenDestinoCol()
		{
		}

	
	
		public OrigenDestino FindByPrimaryKey(System.Decimal odoCodPK)
		{
			return this.SingleOrDefault(e => e.OdoCodPK == odoCodPK);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(OrigenDestino))]
		public class OrigenDestinoColWCFPacket : esCollectionWCFPacket<OrigenDestinoCol>
		{
			public static implicit operator OrigenDestinoCol(OrigenDestinoColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator OrigenDestinoColWCFPacket(OrigenDestinoCol collection)
			{
				return new OrigenDestinoColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class OrigenDestinoQ : esOrigenDestinoQ
	{
		public OrigenDestinoQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public OrigenDestinoQ()
		{
		}

		override protected string GetQueryName()
		{
			return "OrigenDestinoQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GT";
		}

		public override QueryBase CreateQuery()
        {
            return new OrigenDestinoQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(OrigenDestinoQ query)
		{
			return OrigenDestinoQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator OrigenDestinoQ(string query)
		{
			return (OrigenDestinoQ)OrigenDestinoQ.SerializeHelper.FromXml(query, typeof(OrigenDestinoQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esOrigenDestino : EntityBase
	{
		public esOrigenDestino()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal odoCodPK)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(odoCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(odoCodPK);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal odoCodPK)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(odoCodPK);
			else
				return LoadByPrimaryKeyStoredProcedure(odoCodPK);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal odoCodPK)
		{
			OrigenDestinoQ query = new OrigenDestinoQ();
			query.Where(query.OdoCodPK == odoCodPK);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal odoCodPK)
		{
			esParameters parms = new esParameters();
			parms.Add("OdoCodPK", odoCodPK);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? OdoCodPK
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdoCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdoCodPK, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_Nombre
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String OdoNombre
		{
			get
			{
				return base.GetSystemString(OrigenDestinoMetadata.ColumnNames.OdoNombre);
			}
			
			set
			{
				if(base.SetSystemString(OrigenDestinoMetadata.ColumnNames.OdoNombre, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoNombre);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.are_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreCodPK
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.AreCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.AreCodPK, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.AreCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.pvc_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PvcCodPK
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.PvcCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.PvcCodPK, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.PvcCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.lcd_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? LcdCodPK
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.LcdCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.LcdCodPK, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.LcdCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.cen_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CenCodPK
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.CenCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.CenCodPK, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.CenCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_Localidad
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? OdoLocalidad
		{
			get
			{
				return base.GetSystemByte(OrigenDestinoMetadata.ColumnNames.OdoLocalidad);
			}
			
			set
			{
				if(base.SetSystemByte(OrigenDestinoMetadata.ColumnNames.OdoLocalidad, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoLocalidad);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_Eliminado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? OdoEliminado
		{
			get
			{
				return base.GetSystemBoolean(OrigenDestinoMetadata.ColumnNames.OdoEliminado);
			}
			
			set
			{
				if(base.SetSystemBoolean(OrigenDestinoMetadata.ColumnNames.OdoEliminado, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoEliminado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.lcd_BaseUrg
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? LcdBaseUrg
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.LcdBaseUrg);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.LcdBaseUrg, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.LcdBaseUrg);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_Ref
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? OdoRef
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdoRef);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdoRef, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoRef);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_Estado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? OdoEstado
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdoEstado);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdoEstado, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoEstado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_CentroMedico
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? OdoCentroMedico
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdoCentroMedico);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdoCentroMedico, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoCentroMedico);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odot_CodPK
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? OdotCodPK
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdotCodPK);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.OdotCodPK, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdotCodPK);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_Latitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? OdoLatitud
		{
			get
			{
				return base.GetSystemDouble(OrigenDestinoMetadata.ColumnNames.OdoLatitud);
			}
			
			set
			{
				if(base.SetSystemDouble(OrigenDestinoMetadata.ColumnNames.OdoLatitud, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoLatitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_Longitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? OdoLongitud
		{
			get
			{
				return base.GetSystemDouble(OrigenDestinoMetadata.ColumnNames.OdoLongitud);
			}
			
			set
			{
				if(base.SetSystemDouble(OrigenDestinoMetadata.ColumnNames.OdoLongitud, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoLongitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_Radio
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? OdoRadio
		{
			get
			{
				return base.GetSystemDouble(OrigenDestinoMetadata.ColumnNames.OdoRadio);
			}
			
			set
			{
				if(base.SetSystemDouble(OrigenDestinoMetadata.ColumnNames.OdoRadio, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoRadio);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.odo_Direccion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String OdoDireccion
		{
			get
			{
				return base.GetSystemString(OrigenDestinoMetadata.ColumnNames.OdoDireccion);
			}
			
			set
			{
				if(base.SetSystemString(OrigenDestinoMetadata.ColumnNames.OdoDireccion, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoDireccion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.TipoCentroSanitarioID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TipoCentroSanitarioID
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.TipoCentroSanitarioID);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.TipoCentroSanitarioID, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.TipoCentroSanitarioID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.CentroSanitarioId
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CentroSanitarioId
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.CentroSanitarioId);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.CentroSanitarioId, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.CentroSanitarioId);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.AreaSanitariaId
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreaSanitariaId
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.AreaSanitariaId);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.AreaSanitariaId, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.AreaSanitariaId);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.ComarcaSanitariaId
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ComarcaSanitariaId
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.ComarcaSanitariaId);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.ComarcaSanitariaId, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.ComarcaSanitariaId);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.DistritoId
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? DistritoId
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.DistritoId);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.DistritoId, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.DistritoId);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.CodigoAdministracion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String CodigoAdministracion
		{
			get
			{
				return base.GetSystemString(OrigenDestinoMetadata.ColumnNames.CodigoAdministracion);
			}
			
			set
			{
				if(base.SetSystemString(OrigenDestinoMetadata.ColumnNames.CodigoAdministracion, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.CodigoAdministracion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.LocalidadBaseUP
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? LocalidadBaseUP
		{
			get
			{
				return base.GetSystemDecimal(OrigenDestinoMetadata.ColumnNames.LocalidadBaseUP);
			}
			
			set
			{
				if(base.SetSystemDecimal(OrigenDestinoMetadata.ColumnNames.LocalidadBaseUP, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.LocalidadBaseUP);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion Unica del esta entidad en el Sistema GITS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EntityIdentidadID
		{
			get
			{
				return base.GetSystemString(OrigenDestinoMetadata.ColumnNames.EntityIdentidadID);
			}
			
			set
			{
				if(base.SetSystemString(OrigenDestinoMetadata.ColumnNames.EntityIdentidadID, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.EntityIdentidadID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to OrigenDestino.EncFechaUltimaEncuesta
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? EncFechaUltimaEncuesta
		{
			get
			{
				return base.GetSystemDateTime(OrigenDestinoMetadata.ColumnNames.EncFechaUltimaEncuesta);
			}
			
			set
			{
				if(base.SetSystemDateTime(OrigenDestinoMetadata.ColumnNames.EncFechaUltimaEncuesta, value))
				{
					OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.EncFechaUltimaEncuesta);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "OdoCodPK": this.str().OdoCodPK = (string)value; break;							
						case "OdoNombre": this.str().OdoNombre = (string)value; break;							
						case "AreCodPK": this.str().AreCodPK = (string)value; break;							
						case "PvcCodPK": this.str().PvcCodPK = (string)value; break;							
						case "LcdCodPK": this.str().LcdCodPK = (string)value; break;							
						case "CenCodPK": this.str().CenCodPK = (string)value; break;							
						case "OdoLocalidad": this.str().OdoLocalidad = (string)value; break;							
						case "OdoEliminado": this.str().OdoEliminado = (string)value; break;							
						case "LcdBaseUrg": this.str().LcdBaseUrg = (string)value; break;							
						case "OdoRef": this.str().OdoRef = (string)value; break;							
						case "OdoEstado": this.str().OdoEstado = (string)value; break;							
						case "OdoCentroMedico": this.str().OdoCentroMedico = (string)value; break;							
						case "OdotCodPK": this.str().OdotCodPK = (string)value; break;							
						case "OdoLatitud": this.str().OdoLatitud = (string)value; break;							
						case "OdoLongitud": this.str().OdoLongitud = (string)value; break;							
						case "OdoRadio": this.str().OdoRadio = (string)value; break;							
						case "OdoDireccion": this.str().OdoDireccion = (string)value; break;							
						case "TipoCentroSanitarioID": this.str().TipoCentroSanitarioID = (string)value; break;							
						case "CentroSanitarioId": this.str().CentroSanitarioId = (string)value; break;							
						case "AreaSanitariaId": this.str().AreaSanitariaId = (string)value; break;							
						case "ComarcaSanitariaId": this.str().ComarcaSanitariaId = (string)value; break;							
						case "DistritoId": this.str().DistritoId = (string)value; break;							
						case "CodigoAdministracion": this.str().CodigoAdministracion = (string)value; break;							
						case "LocalidadBaseUP": this.str().LocalidadBaseUP = (string)value; break;							
						case "EntityIdentidadID": this.str().EntityIdentidadID = (string)value; break;							
						case "EncFechaUltimaEncuesta": this.str().EncFechaUltimaEncuesta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "OdoCodPK":
						
							if (value == null || value is System.Decimal)
								this.OdoCodPK = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoCodPK);
							break;
						
						case "AreCodPK":
						
							if (value == null || value is System.Decimal)
								this.AreCodPK = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.AreCodPK);
							break;
						
						case "PvcCodPK":
						
							if (value == null || value is System.Decimal)
								this.PvcCodPK = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.PvcCodPK);
							break;
						
						case "LcdCodPK":
						
							if (value == null || value is System.Decimal)
								this.LcdCodPK = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.LcdCodPK);
							break;
						
						case "CenCodPK":
						
							if (value == null || value is System.Decimal)
								this.CenCodPK = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.CenCodPK);
							break;
						
						case "OdoLocalidad":
						
							if (value == null || value is System.Byte)
								this.OdoLocalidad = (System.Byte?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoLocalidad);
							break;
						
						case "OdoEliminado":
						
							if (value == null || value is System.Boolean)
								this.OdoEliminado = (System.Boolean?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoEliminado);
							break;
						
						case "LcdBaseUrg":
						
							if (value == null || value is System.Decimal)
								this.LcdBaseUrg = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.LcdBaseUrg);
							break;
						
						case "OdoRef":
						
							if (value == null || value is System.Decimal)
								this.OdoRef = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoRef);
							break;
						
						case "OdoEstado":
						
							if (value == null || value is System.Decimal)
								this.OdoEstado = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoEstado);
							break;
						
						case "OdoCentroMedico":
						
							if (value == null || value is System.Decimal)
								this.OdoCentroMedico = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoCentroMedico);
							break;
						
						case "OdotCodPK":
						
							if (value == null || value is System.Decimal)
								this.OdotCodPK = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdotCodPK);
							break;
						
						case "OdoLatitud":
						
							if (value == null || value is System.Double)
								this.OdoLatitud = (System.Double?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoLatitud);
							break;
						
						case "OdoLongitud":
						
							if (value == null || value is System.Double)
								this.OdoLongitud = (System.Double?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoLongitud);
							break;
						
						case "OdoRadio":
						
							if (value == null || value is System.Double)
								this.OdoRadio = (System.Double?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.OdoRadio);
							break;
						
						case "TipoCentroSanitarioID":
						
							if (value == null || value is System.Decimal)
								this.TipoCentroSanitarioID = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.TipoCentroSanitarioID);
							break;
						
						case "CentroSanitarioId":
						
							if (value == null || value is System.Decimal)
								this.CentroSanitarioId = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.CentroSanitarioId);
							break;
						
						case "AreaSanitariaId":
						
							if (value == null || value is System.Decimal)
								this.AreaSanitariaId = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.AreaSanitariaId);
							break;
						
						case "ComarcaSanitariaId":
						
							if (value == null || value is System.Decimal)
								this.ComarcaSanitariaId = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.ComarcaSanitariaId);
							break;
						
						case "DistritoId":
						
							if (value == null || value is System.Decimal)
								this.DistritoId = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.DistritoId);
							break;
						
						case "LocalidadBaseUP":
						
							if (value == null || value is System.Decimal)
								this.LocalidadBaseUP = (System.Decimal?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.LocalidadBaseUP);
							break;
						
						case "EncFechaUltimaEncuesta":
						
							if (value == null || value is System.DateTime)
								this.EncFechaUltimaEncuesta = (System.DateTime?)value;
								OnPropertyChanged(OrigenDestinoMetadata.PropertyNames.EncFechaUltimaEncuesta);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esOrigenDestino entity)
			{
				this.entity = entity;
			}
			
	
			public System.String OdoCodPK
			{
				get
				{
					System.Decimal? data = entity.OdoCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoCodPK = null;
					else entity.OdoCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String OdoNombre
			{
				get
				{
					System.String data = entity.OdoNombre;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoNombre = null;
					else entity.OdoNombre = Convert.ToString(value);
				}
			}
				
			public System.String AreCodPK
			{
				get
				{
					System.Decimal? data = entity.AreCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreCodPK = null;
					else entity.AreCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String PvcCodPK
			{
				get
				{
					System.Decimal? data = entity.PvcCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PvcCodPK = null;
					else entity.PvcCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String LcdCodPK
			{
				get
				{
					System.Decimal? data = entity.LcdCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LcdCodPK = null;
					else entity.LcdCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String CenCodPK
			{
				get
				{
					System.Decimal? data = entity.CenCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CenCodPK = null;
					else entity.CenCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String OdoLocalidad
			{
				get
				{
					System.Byte? data = entity.OdoLocalidad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoLocalidad = null;
					else entity.OdoLocalidad = Convert.ToByte(value);
				}
			}
				
			public System.String OdoEliminado
			{
				get
				{
					System.Boolean? data = entity.OdoEliminado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoEliminado = null;
					else entity.OdoEliminado = Convert.ToBoolean(value);
				}
			}
				
			public System.String LcdBaseUrg
			{
				get
				{
					System.Decimal? data = entity.LcdBaseUrg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LcdBaseUrg = null;
					else entity.LcdBaseUrg = Convert.ToDecimal(value);
				}
			}
				
			public System.String OdoRef
			{
				get
				{
					System.Decimal? data = entity.OdoRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoRef = null;
					else entity.OdoRef = Convert.ToDecimal(value);
				}
			}
				
			public System.String OdoEstado
			{
				get
				{
					System.Decimal? data = entity.OdoEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoEstado = null;
					else entity.OdoEstado = Convert.ToDecimal(value);
				}
			}
				
			public System.String OdoCentroMedico
			{
				get
				{
					System.Decimal? data = entity.OdoCentroMedico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoCentroMedico = null;
					else entity.OdoCentroMedico = Convert.ToDecimal(value);
				}
			}
				
			public System.String OdotCodPK
			{
				get
				{
					System.Decimal? data = entity.OdotCodPK;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdotCodPK = null;
					else entity.OdotCodPK = Convert.ToDecimal(value);
				}
			}
				
			public System.String OdoLatitud
			{
				get
				{
					System.Double? data = entity.OdoLatitud;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoLatitud = null;
					else entity.OdoLatitud = Convert.ToDouble(value);
				}
			}
				
			public System.String OdoLongitud
			{
				get
				{
					System.Double? data = entity.OdoLongitud;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoLongitud = null;
					else entity.OdoLongitud = Convert.ToDouble(value);
				}
			}
				
			public System.String OdoRadio
			{
				get
				{
					System.Double? data = entity.OdoRadio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoRadio = null;
					else entity.OdoRadio = Convert.ToDouble(value);
				}
			}
				
			public System.String OdoDireccion
			{
				get
				{
					System.String data = entity.OdoDireccion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdoDireccion = null;
					else entity.OdoDireccion = Convert.ToString(value);
				}
			}
				
			public System.String TipoCentroSanitarioID
			{
				get
				{
					System.Decimal? data = entity.TipoCentroSanitarioID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCentroSanitarioID = null;
					else entity.TipoCentroSanitarioID = Convert.ToDecimal(value);
				}
			}
				
			public System.String CentroSanitarioId
			{
				get
				{
					System.Decimal? data = entity.CentroSanitarioId;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CentroSanitarioId = null;
					else entity.CentroSanitarioId = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreaSanitariaId
			{
				get
				{
					System.Decimal? data = entity.AreaSanitariaId;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaSanitariaId = null;
					else entity.AreaSanitariaId = Convert.ToDecimal(value);
				}
			}
				
			public System.String ComarcaSanitariaId
			{
				get
				{
					System.Decimal? data = entity.ComarcaSanitariaId;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ComarcaSanitariaId = null;
					else entity.ComarcaSanitariaId = Convert.ToDecimal(value);
				}
			}
				
			public System.String DistritoId
			{
				get
				{
					System.Decimal? data = entity.DistritoId;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DistritoId = null;
					else entity.DistritoId = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodigoAdministracion
			{
				get
				{
					System.String data = entity.CodigoAdministracion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAdministracion = null;
					else entity.CodigoAdministracion = Convert.ToString(value);
				}
			}
				
			public System.String LocalidadBaseUP
			{
				get
				{
					System.Decimal? data = entity.LocalidadBaseUP;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LocalidadBaseUP = null;
					else entity.LocalidadBaseUP = Convert.ToDecimal(value);
				}
			}
				
			public System.String EntityIdentidadID
			{
				get
				{
					System.String data = entity.EntityIdentidadID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EntityIdentidadID = null;
					else entity.EntityIdentidadID = Convert.ToString(value);
				}
			}
				
			public System.String EncFechaUltimaEncuesta
			{
				get
				{
					System.DateTime? data = entity.EncFechaUltimaEncuesta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EncFechaUltimaEncuesta = null;
					else entity.EncFechaUltimaEncuesta = Convert.ToDateTime(value);
				}
			}
			

			private esOrigenDestino entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return OrigenDestinoMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public OrigenDestinoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrigenDestinoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(OrigenDestinoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(OrigenDestinoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private OrigenDestinoQ query;		
	}



	[Serializable]
	abstract public partial class esOrigenDestinoCol : CollectionBase<OrigenDestino>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OrigenDestinoMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "OrigenDestinoCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public OrigenDestinoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrigenDestinoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(OrigenDestinoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OrigenDestinoQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(OrigenDestinoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((OrigenDestinoQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private OrigenDestinoQ query;
	}



	[Serializable]
	abstract public partial class esOrigenDestinoQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return OrigenDestinoMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoCodPK,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoCodPK, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoNombre,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoNombre, esSystemType.String));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.AreCodPK,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.AreCodPK, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.PvcCodPK,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.PvcCodPK, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.LcdCodPK,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.LcdCodPK, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.CenCodPK,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.CenCodPK, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoLocalidad,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoLocalidad, esSystemType.Byte));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoEliminado,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoEliminado, esSystemType.Boolean));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.LcdBaseUrg,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.LcdBaseUrg, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoRef,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoRef, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoEstado,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoEstado, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoCentroMedico,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoCentroMedico, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdotCodPK,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdotCodPK, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoLatitud,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoLatitud, esSystemType.Double));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoLongitud,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoLongitud, esSystemType.Double));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoRadio,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoRadio, esSystemType.Double));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.OdoDireccion,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoDireccion, esSystemType.String));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.TipoCentroSanitarioID,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.TipoCentroSanitarioID, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.CentroSanitarioId,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.CentroSanitarioId, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.AreaSanitariaId,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.AreaSanitariaId, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.ComarcaSanitariaId,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.ComarcaSanitariaId, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.DistritoId,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.DistritoId, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.CodigoAdministracion,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.CodigoAdministracion, esSystemType.String));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.LocalidadBaseUP,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.LocalidadBaseUP, esSystemType.Decimal));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.EntityIdentidadID,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.EntityIdentidadID, esSystemType.String));
			_queryItems.Add(OrigenDestinoMetadata.ColumnNames.EncFechaUltimaEncuesta,new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.EncFechaUltimaEncuesta, esSystemType.DateTime));
			
	    }
	
	    #region esQueryItems

		public esQueryItem OdoCodPK
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem OdoNombre
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoNombre, esSystemType.String); }
		} 
		
		public esQueryItem AreCodPK
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.AreCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem PvcCodPK
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.PvcCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem LcdCodPK
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.LcdCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem CenCodPK
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.CenCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem OdoLocalidad
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoLocalidad, esSystemType.Byte); }
		} 
		
		public esQueryItem OdoEliminado
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoEliminado, esSystemType.Boolean); }
		} 
		
		public esQueryItem LcdBaseUrg
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.LcdBaseUrg, esSystemType.Decimal); }
		} 
		
		public esQueryItem OdoRef
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoRef, esSystemType.Decimal); }
		} 
		
		public esQueryItem OdoEstado
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoEstado, esSystemType.Decimal); }
		} 
		
		public esQueryItem OdoCentroMedico
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoCentroMedico, esSystemType.Decimal); }
		} 
		
		public esQueryItem OdotCodPK
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdotCodPK, esSystemType.Decimal); }
		} 
		
		public esQueryItem OdoLatitud
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoLatitud, esSystemType.Double); }
		} 
		
		public esQueryItem OdoLongitud
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoLongitud, esSystemType.Double); }
		} 
		
		public esQueryItem OdoRadio
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoRadio, esSystemType.Double); }
		} 
		
		public esQueryItem OdoDireccion
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.OdoDireccion, esSystemType.String); }
		} 
		
		public esQueryItem TipoCentroSanitarioID
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.TipoCentroSanitarioID, esSystemType.Decimal); }
		} 
		
		public esQueryItem CentroSanitarioId
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.CentroSanitarioId, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreaSanitariaId
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.AreaSanitariaId, esSystemType.Decimal); }
		} 
		
		public esQueryItem ComarcaSanitariaId
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.ComarcaSanitariaId, esSystemType.Decimal); }
		} 
		
		public esQueryItem DistritoId
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.DistritoId, esSystemType.Decimal); }
		} 
		
		public esQueryItem CodigoAdministracion
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.CodigoAdministracion, esSystemType.String); }
		} 
		
		public esQueryItem LocalidadBaseUP
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.LocalidadBaseUP, esSystemType.Decimal); }
		} 
		
		public esQueryItem EntityIdentidadID
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.EntityIdentidadID, esSystemType.String); }
		} 
		
		public esQueryItem EncFechaUltimaEncuesta
		{
			get { return new esQueryItem(this, OrigenDestinoMetadata.ColumnNames.EncFechaUltimaEncuesta, esSystemType.DateTime); }
		} 
		
		#endregion
		
	}


	
	public partial class OrigenDestino : esOrigenDestino
	{

		
		
	}
	



	[Serializable]
	public partial class OrigenDestinoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OrigenDestinoMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoCodPK, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoCodPK;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoNombre, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoNombre;
			c.CharacterMaxLength = 80;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.AreCodPK, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.AreCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.PvcCodPK, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.PvcCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.LcdCodPK, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.LcdCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.CenCodPK, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.CenCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoLocalidad, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoLocalidad;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoEliminado, 7, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoEliminado;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.LcdBaseUrg, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.LcdBaseUrg;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoRef, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoRef;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoEstado, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoEstado;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoCentroMedico, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoCentroMedico;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdotCodPK, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdotCodPK;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoLatitud, 13, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoLatitud;
			c.NumericPrecision = 15;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoLongitud, 14, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoLongitud;
			c.NumericPrecision = 15;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoRadio, 15, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoRadio;
			c.NumericPrecision = 15;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.OdoDireccion, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.OdoDireccion;
			c.CharacterMaxLength = 80;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.TipoCentroSanitarioID, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.TipoCentroSanitarioID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.CentroSanitarioId, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.CentroSanitarioId;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.AreaSanitariaId, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.AreaSanitariaId;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.ComarcaSanitariaId, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.ComarcaSanitariaId;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.DistritoId, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.DistritoId;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.CodigoAdministracion, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.CodigoAdministracion;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.LocalidadBaseUP, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.LocalidadBaseUP;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.EntityIdentidadID, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.EntityIdentidadID;
			c.CharacterMaxLength = 50;
			c.Description = "Identificacion Unica del esta entidad en el Sistema GITS";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(OrigenDestinoMetadata.ColumnNames.EncFechaUltimaEncuesta, 25, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrigenDestinoMetadata.PropertyNames.EncFechaUltimaEncuesta;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public OrigenDestinoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string OdoCodPK = "odo_CodPK";
			 public const string OdoNombre = "odo_Nombre";
			 public const string AreCodPK = "are_CodPK";
			 public const string PvcCodPK = "pvc_CodPK";
			 public const string LcdCodPK = "lcd_CodPK";
			 public const string CenCodPK = "cen_CodPK";
			 public const string OdoLocalidad = "odo_Localidad";
			 public const string OdoEliminado = "odo_Eliminado";
			 public const string LcdBaseUrg = "lcd_BaseUrg";
			 public const string OdoRef = "odo_Ref";
			 public const string OdoEstado = "odo_Estado";
			 public const string OdoCentroMedico = "odo_CentroMedico";
			 public const string OdotCodPK = "odot_CodPK";
			 public const string OdoLatitud = "odo_Latitud";
			 public const string OdoLongitud = "odo_Longitud";
			 public const string OdoRadio = "odo_Radio";
			 public const string OdoDireccion = "odo_Direccion";
			 public const string TipoCentroSanitarioID = "TipoCentroSanitarioID";
			 public const string CentroSanitarioId = "CentroSanitarioId";
			 public const string AreaSanitariaId = "AreaSanitariaId";
			 public const string ComarcaSanitariaId = "ComarcaSanitariaId";
			 public const string DistritoId = "DistritoId";
			 public const string CodigoAdministracion = "CodigoAdministracion";
			 public const string LocalidadBaseUP = "LocalidadBaseUP";
			 public const string EntityIdentidadID = "EntityIdentidadID";
			 public const string EncFechaUltimaEncuesta = "EncFechaUltimaEncuesta";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string OdoCodPK = "OdoCodPK";
			 public const string OdoNombre = "OdoNombre";
			 public const string AreCodPK = "AreCodPK";
			 public const string PvcCodPK = "PvcCodPK";
			 public const string LcdCodPK = "LcdCodPK";
			 public const string CenCodPK = "CenCodPK";
			 public const string OdoLocalidad = "OdoLocalidad";
			 public const string OdoEliminado = "OdoEliminado";
			 public const string LcdBaseUrg = "LcdBaseUrg";
			 public const string OdoRef = "OdoRef";
			 public const string OdoEstado = "OdoEstado";
			 public const string OdoCentroMedico = "OdoCentroMedico";
			 public const string OdotCodPK = "OdotCodPK";
			 public const string OdoLatitud = "OdoLatitud";
			 public const string OdoLongitud = "OdoLongitud";
			 public const string OdoRadio = "OdoRadio";
			 public const string OdoDireccion = "OdoDireccion";
			 public const string TipoCentroSanitarioID = "TipoCentroSanitarioID";
			 public const string CentroSanitarioId = "CentroSanitarioId";
			 public const string AreaSanitariaId = "AreaSanitariaId";
			 public const string ComarcaSanitariaId = "ComarcaSanitariaId";
			 public const string DistritoId = "DistritoId";
			 public const string CodigoAdministracion = "CodigoAdministracion";
			 public const string LocalidadBaseUP = "LocalidadBaseUP";
			 public const string EntityIdentidadID = "EntityIdentidadID";
			 public const string EncFechaUltimaEncuesta = "EncFechaUltimaEncuesta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OrigenDestinoMetadata))
			{
				if(OrigenDestinoMetadata.mapDelegates == null)
				{
					OrigenDestinoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OrigenDestinoMetadata.meta == null)
				{
					OrigenDestinoMetadata.meta = new OrigenDestinoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("OdoCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("OdoNombre", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("AreCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PvcCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("LcdCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("CenCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("OdoLocalidad", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("OdoEliminado", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("LcdBaseUrg", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("OdoRef", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("OdoEstado", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("OdoCentroMedico", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("OdotCodPK", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("OdoLatitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("OdoLongitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("OdoRadio", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("OdoDireccion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("TipoCentroSanitarioID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("CentroSanitarioId", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreaSanitariaId", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ComarcaSanitariaId", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("DistritoId", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("CodigoAdministracion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("LocalidadBaseUP", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EntityIdentidadID", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EncFechaUltimaEncuesta", new esTypeMap("datetime", "System.DateTime"));			
				meta.Catalog = "GITS";
				meta.Schema = "dbo";
				
				meta.Source = "OrigenDestino";
				meta.Destination = "OrigenDestino";
				
				meta.spInsert = "proc_OrigenDestinoInsert";				
				meta.spUpdate = "proc_OrigenDestinoUpdate";		
				meta.spDelete = "proc_OrigenDestinoDelete";
				meta.spLoadAll = "proc_OrigenDestinoLoadAll";
				meta.spLoadByPrimaryKey = "proc_OrigenDestinoLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OrigenDestinoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
