
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// MANTIENE EL ESTADO DE LOS TERMINALES ACTIVOS. RELLENA A TRAVES DE INTEGRACION CON GITS, AL ASIGNAR A UN VEHICULO UN TERMINAL, Y SE ELIMINA AL BORRAR ESTA VINCULACION. TAMBIEN AUTOMATICAMENTE SE COMPRUEBA AL INICIAR EL SERVIDOR,  EL ESTADO DEL TERMINAL Y VEHICULO. CUANDO EL TERMINAL ES DESVINCULADO O PUESTO INACTIVO, ESTE DATO SE PASA A LA TABLA [HistoricoTerminalState].
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("HistoricoTerminalState")]
	public partial class HistoricoTerminalState : esHistoricoTerminalState
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new HistoricoTerminalState();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal terminalID)
		{
			var obj = new HistoricoTerminalState();
			obj.TerminalID = terminalID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal terminalID, esSqlAccessType sqlAccessType)
		{
			var obj = new HistoricoTerminalState();
			obj.TerminalID = terminalID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal TERMINALID)
        {
            try
            {
                var e = new HistoricoTerminalState();
                return (e.LoadByPrimaryKey(TERMINALID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static HistoricoTerminalState Get(System.Decimal TERMINALID)
        {
            try
            {
                var e = new HistoricoTerminalState();
                return (e.LoadByPrimaryKey(TERMINALID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public HistoricoTerminalState(System.Decimal TERMINALID)
        {
            this.LoadByPrimaryKey(TERMINALID);
        }
		
		public HistoricoTerminalState()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("HistoricoTerminalStateCol")]
	public partial class HistoricoTerminalStateCol : esHistoricoTerminalStateCol, IEnumerable<HistoricoTerminalState>
	{
	
		public HistoricoTerminalStateCol()
		{
		}

	
	
		public HistoricoTerminalState FindByPrimaryKey(System.Decimal terminalID)
		{
			return this.SingleOrDefault(e => e.TerminalID == terminalID);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(HistoricoTerminalState))]
		public class HistoricoTerminalStateColWCFPacket : esCollectionWCFPacket<HistoricoTerminalStateCol>
		{
			public static implicit operator HistoricoTerminalStateCol(HistoricoTerminalStateColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator HistoricoTerminalStateColWCFPacket(HistoricoTerminalStateCol collection)
			{
				return new HistoricoTerminalStateColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class HistoricoTerminalStateQ : esHistoricoTerminalStateQ
	{
		public HistoricoTerminalStateQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public HistoricoTerminalStateQ()
		{
		}

		override protected string GetQueryName()
		{
			return "HistoricoTerminalStateQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new HistoricoTerminalStateQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(HistoricoTerminalStateQ query)
		{
			return HistoricoTerminalStateQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator HistoricoTerminalStateQ(string query)
		{
			return (HistoricoTerminalStateQ)HistoricoTerminalStateQ.SerializeHelper.FromXml(query, typeof(HistoricoTerminalStateQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esHistoricoTerminalState : EntityBase
	{
		public esHistoricoTerminalState()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal terminalID)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(terminalID);
			else
				return LoadByPrimaryKeyStoredProcedure(terminalID);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal terminalID)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(terminalID);
			else
				return LoadByPrimaryKeyStoredProcedure(terminalID);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal terminalID)
		{
			HistoricoTerminalStateQ query = new HistoricoTerminalStateQ();
			query.Where(query.TerminalID == terminalID);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal terminalID)
		{
			esParameters parms = new esParameters();
			parms.Add("TerminalID", terminalID);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal TerminalID
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoTerminalStateMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.VehiculoMatricula
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoMatricula
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.VehiculoMatricula);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.VehiculoMatricula, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.VehiculoMatricula);
				}
			}
		}		
		
		/// <summary>
		/// Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CentroTrabajoID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.CentroTrabajoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.CentroTrabajoID, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.CentroTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.AreaTrabajoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreaTrabajoID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.AreaTrabajoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.AreaTrabajoID, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.AreaTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.EmpresaID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpresaID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.EmpresaID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.EmpresaID, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.EmpresaID);
				}
			}
		}		
		
		/// <summary>
		/// Conductor Logado actualmente en el Vehiculo.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PersonalID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.PersonalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.PersonalID, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.PersonalID);
				}
			}
		}		
		
		/// <summary>
		/// Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte TerminalEstado
		{
			get
			{
				return base.GetSystemByteRequired(HistoricoTerminalStateMetadata.ColumnNames.TerminalEstado);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoTerminalStateMetadata.ColumnNames.TerminalEstado, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalEstado);
				}
			}
		}		
		
		/// <summary>
		/// Identifica el Tipo de Protocolo de comunicacion con el Terminal. 1- MDT. 2-MDV.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte TerminalProtocoloTipo
		{
			get
			{
				return base.GetSystemByteRequired(HistoricoTerminalStateMetadata.ColumnNames.TerminalProtocoloTipo);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoTerminalStateMetadata.ColumnNames.TerminalProtocoloTipo, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalProtocoloTipo);
				}
			}
		}		
		
		/// <summary>
		/// Nº de Serie del Terminal o Identificacion del Terminal Unico guardado en GITS. El Proceso debe comparar los datos mandados por el Terminal (NSerie, IMEI,NSerieSim) y revisar si coincide alguno de ellos con la Identificacion del Dispositivo Terminal en GITS. Se obtiene de la Tabla GITS.dbo.TerminalesGPS campo mdt_NS.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalNS
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.TerminalNS);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.TerminalNS, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalNS);
				}
			}
		}		
		
		/// <summary>
		/// Nº de IMEI del model del Terminal. Se rellena cuando lo envia el Terminal a realizar la conexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalIMEI
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.TerminalIMEI);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.TerminalIMEI, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalIMEI);
				}
			}
		}		
		
		/// <summary>
		/// Numero de Serie de la tarjeta sim del terminal. Simplemente se utiliza de control.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalNSSim
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.TerminalNSSim);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.TerminalNSSim, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalNSSim);
				}
			}
		}		
		
		/// <summary>
		/// Version del software del dispoisitvo, fundamental a la hora de update el software del Dispositvo.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalVersionSW
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.TerminalVersionSW);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.TerminalVersionSW, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalVersionSW);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la Ultima posicion GPS válida recibida. Las tramas de Estado y EstadoServicio, que mandan informacion GPS, siempre envia una posicion válida si el Terminal ha podido obtener una, independientemente que en el momento del envio de la trama no esté obteniendo posiciones validas. Para calcular este fecha, hay que mirar el campo GpsFixMode sea >1, que indica que el Fix es valido. Si no este fecha, no se actualiza.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? GpsFecha
		{
			get
			{
				return base.GetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.GpsFecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.GpsFecha, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsFecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.GpsLatitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLatitud
		{
			get
			{
				return base.GetSystemDoubleRequired(HistoricoTerminalStateMetadata.ColumnNames.GpsLatitud);
			}
			
			set
			{
				if(base.SetSystemDouble(HistoricoTerminalStateMetadata.ColumnNames.GpsLatitud, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsLatitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.GpsLongitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLongitud
		{
			get
			{
				return base.GetSystemDoubleRequired(HistoricoTerminalStateMetadata.ColumnNames.GpsLongitud);
			}
			
			set
			{
				if(base.SetSystemDouble(HistoricoTerminalStateMetadata.ColumnNames.GpsLongitud, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsLongitud);
				}
			}
		}		
		
		/// <summary>
		/// Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte GpsFixMode
		{
			get
			{
				return base.GetSystemByteRequired(HistoricoTerminalStateMetadata.ColumnNames.GpsFixMode);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoTerminalStateMetadata.ColumnNames.GpsFixMode, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsFixMode);
				}
			}
		}		
		
		/// <summary>
		/// Georefencia textual de la posicion obtenida en GPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GpsFixGeoTx
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.GpsFixGeoTx);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.GpsFixGeoTx, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsFixGeoTx);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.GpsNSAT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte GpsNSAT
		{
			get
			{
				return base.GetSystemByteRequired(HistoricoTerminalStateMetadata.ColumnNames.GpsNSAT);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoTerminalStateMetadata.ColumnNames.GpsNSAT, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsNSAT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.GpsAltitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16 GpsAltitud
		{
			get
			{
				return base.GetSystemInt16Required(HistoricoTerminalStateMetadata.ColumnNames.GpsAltitud);
			}
			
			set
			{
				if(base.SetSystemInt16(HistoricoTerminalStateMetadata.ColumnNames.GpsAltitud, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsAltitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.GpsRumbo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal GpsRumbo
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoTerminalStateMetadata.ColumnNames.GpsRumbo);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalStateMetadata.ColumnNames.GpsRumbo, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsRumbo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.GpsVelocidad
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsVelocidad
		{
			get
			{
				return base.GetSystemDoubleRequired(HistoricoTerminalStateMetadata.ColumnNames.GpsVelocidad);
			}
			
			set
			{
				if(base.SetSystemDouble(HistoricoTerminalStateMetadata.ColumnNames.GpsVelocidad, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsVelocidad);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.GpsDistancia
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsDistancia
		{
			get
			{
				return base.GetSystemDoubleRequired(HistoricoTerminalStateMetadata.ColumnNames.GpsDistancia);
			}
			
			set
			{
				if(base.SetSystemDouble(HistoricoTerminalStateMetadata.ColumnNames.GpsDistancia, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsDistancia);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la Ultima trama recibida desde el Terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? GsmFecha
		{
			get
			{
				return base.GetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.GsmFecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.GsmFecha, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GsmFecha);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Total
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double OdometroTt
		{
			get
			{
				return base.GetSystemDoubleRequired(HistoricoTerminalStateMetadata.ColumnNames.OdometroTt);
			}
			
			set
			{
				if(base.SetSystemDouble(HistoricoTerminalStateMetadata.ColumnNames.OdometroTt, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.OdometroTt);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Parcial
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double OdometroPc
		{
			get
			{
				return base.GetSystemDoubleRequired(HistoricoTerminalStateMetadata.ColumnNames.OdometroPc);
			}
			
			set
			{
				if(base.SetSystemDouble(HistoricoTerminalStateMetadata.ColumnNames.OdometroPc, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.OdometroPc);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de entrada del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String InputState
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.InputState);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.InputState, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.InputState);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de salida del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String OutputState
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.OutputState);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.OutputState, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.OutputState);
				}
			}
		}		
		
		/// <summary>
		/// Indica si el Terminal está conectado. Si es <> null => conectado sino Desconectado. Guarda la fecha del comienzo de conexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalConnectedFrom
		{
			get
			{
				return base.GetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.TerminalConnectedFrom);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.TerminalConnectedFrom, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalConnectedFrom);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la ultima recepcion obtenida desde este Terminal. 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalLastReception
		{
			get
			{
				return base.GetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastReception);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastReception, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalLastReception);
				}
			}
		}		
		
		/// <summary>
		/// Si posee valor indica que el Terminal está desconectado, y marca la fecha de desconexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalDisconnect
		{
			get
			{
				return base.GetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.TerminalDisconnect);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.TerminalDisconnect, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalDisconnect);
				}
			}
		}		
		
		/// <summary>
		/// Duración en Horas de la ultima sesion ininterrumpida que este terminal estuvo conectado.  Es null si nunca se conecto este Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 TerminalLastSessionDuration
		{
			get
			{
				return base.GetSystemInt32Required(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastSessionDuration);
			}
			
			set
			{
				if(base.SetSystemInt32(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastSessionDuration, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalLastSessionDuration);
				}
			}
		}		
		
		/// <summary>
		/// Fecha del ultimo reinicio forzado. Bien por reparación o fallo electrico, o por fallo del Terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalLastRestart
		{
			get
			{
				return base.GetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastRestart);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastRestart, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalLastRestart);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.VehiculoCodigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigo
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.VehiculoCodigo);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.VehiculoCodigo, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.VehiculoCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.VehiculoTelefono
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoTelefono
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.VehiculoTelefono);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.VehiculoTelefono, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.VehiculoTelefono);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalState.Comentario
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Comentario
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalStateMetadata.ColumnNames.Comentario);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalStateMetadata.ColumnNames.Comentario, value))
				{
					OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.Comentario);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "VehiculoMatricula": this.str().VehiculoMatricula = (string)value; break;							
						case "CentroTrabajoID": this.str().CentroTrabajoID = (string)value; break;							
						case "AreaTrabajoID": this.str().AreaTrabajoID = (string)value; break;							
						case "EmpresaID": this.str().EmpresaID = (string)value; break;							
						case "PersonalID": this.str().PersonalID = (string)value; break;							
						case "TerminalEstado": this.str().TerminalEstado = (string)value; break;							
						case "TerminalProtocoloTipo": this.str().TerminalProtocoloTipo = (string)value; break;							
						case "TerminalNS": this.str().TerminalNS = (string)value; break;							
						case "TerminalIMEI": this.str().TerminalIMEI = (string)value; break;							
						case "TerminalNSSim": this.str().TerminalNSSim = (string)value; break;							
						case "TerminalVersionSW": this.str().TerminalVersionSW = (string)value; break;							
						case "GpsFecha": this.str().GpsFecha = (string)value; break;							
						case "GpsLatitud": this.str().GpsLatitud = (string)value; break;							
						case "GpsLongitud": this.str().GpsLongitud = (string)value; break;							
						case "GpsFixMode": this.str().GpsFixMode = (string)value; break;							
						case "GpsFixGeoTx": this.str().GpsFixGeoTx = (string)value; break;							
						case "GpsNSAT": this.str().GpsNSAT = (string)value; break;							
						case "GpsAltitud": this.str().GpsAltitud = (string)value; break;							
						case "GpsRumbo": this.str().GpsRumbo = (string)value; break;							
						case "GpsVelocidad": this.str().GpsVelocidad = (string)value; break;							
						case "GpsDistancia": this.str().GpsDistancia = (string)value; break;							
						case "GsmFecha": this.str().GsmFecha = (string)value; break;							
						case "OdometroTt": this.str().OdometroTt = (string)value; break;							
						case "OdometroPc": this.str().OdometroPc = (string)value; break;							
						case "InputState": this.str().InputState = (string)value; break;							
						case "OutputState": this.str().OutputState = (string)value; break;							
						case "TerminalConnectedFrom": this.str().TerminalConnectedFrom = (string)value; break;							
						case "TerminalLastReception": this.str().TerminalLastReception = (string)value; break;							
						case "TerminalDisconnect": this.str().TerminalDisconnect = (string)value; break;							
						case "TerminalLastSessionDuration": this.str().TerminalLastSessionDuration = (string)value; break;							
						case "TerminalLastRestart": this.str().TerminalLastRestart = (string)value; break;							
						case "VehiculoCodigo": this.str().VehiculoCodigo = (string)value; break;							
						case "VehiculoTelefono": this.str().VehiculoTelefono = (string)value; break;							
						case "Comentario": this.str().Comentario = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.VehiculoID);
							break;
						
						case "CentroTrabajoID":
						
							if (value == null || value is System.Decimal)
								this.CentroTrabajoID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.CentroTrabajoID);
							break;
						
						case "AreaTrabajoID":
						
							if (value == null || value is System.Decimal)
								this.AreaTrabajoID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.AreaTrabajoID);
							break;
						
						case "EmpresaID":
						
							if (value == null || value is System.Decimal)
								this.EmpresaID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.EmpresaID);
							break;
						
						case "PersonalID":
						
							if (value == null || value is System.Decimal)
								this.PersonalID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.PersonalID);
							break;
						
						case "TerminalEstado":
						
							if (value == null || value is System.Byte)
								this.TerminalEstado = (System.Byte)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalEstado);
							break;
						
						case "TerminalProtocoloTipo":
						
							if (value == null || value is System.Byte)
								this.TerminalProtocoloTipo = (System.Byte)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalProtocoloTipo);
							break;
						
						case "GpsFecha":
						
							if (value == null || value is System.DateTime)
								this.GpsFecha = (System.DateTime?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsFecha);
							break;
						
						case "GpsLatitud":
						
							if (value == null || value is System.Double)
								this.GpsLatitud = (System.Double)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsLatitud);
							break;
						
						case "GpsLongitud":
						
							if (value == null || value is System.Double)
								this.GpsLongitud = (System.Double)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsLongitud);
							break;
						
						case "GpsFixMode":
						
							if (value == null || value is System.Byte)
								this.GpsFixMode = (System.Byte)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsFixMode);
							break;
						
						case "GpsNSAT":
						
							if (value == null || value is System.Byte)
								this.GpsNSAT = (System.Byte)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsNSAT);
							break;
						
						case "GpsAltitud":
						
							if (value == null || value is System.Int16)
								this.GpsAltitud = (System.Int16)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsAltitud);
							break;
						
						case "GpsRumbo":
						
							if (value == null || value is System.Decimal)
								this.GpsRumbo = (System.Decimal)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsRumbo);
							break;
						
						case "GpsVelocidad":
						
							if (value == null || value is System.Double)
								this.GpsVelocidad = (System.Double)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsVelocidad);
							break;
						
						case "GpsDistancia":
						
							if (value == null || value is System.Double)
								this.GpsDistancia = (System.Double)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GpsDistancia);
							break;
						
						case "GsmFecha":
						
							if (value == null || value is System.DateTime)
								this.GsmFecha = (System.DateTime?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.GsmFecha);
							break;
						
						case "OdometroTt":
						
							if (value == null || value is System.Double)
								this.OdometroTt = (System.Double)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.OdometroTt);
							break;
						
						case "OdometroPc":
						
							if (value == null || value is System.Double)
								this.OdometroPc = (System.Double)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.OdometroPc);
							break;
						
						case "TerminalConnectedFrom":
						
							if (value == null || value is System.DateTime)
								this.TerminalConnectedFrom = (System.DateTime?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalConnectedFrom);
							break;
						
						case "TerminalLastReception":
						
							if (value == null || value is System.DateTime)
								this.TerminalLastReception = (System.DateTime?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalLastReception);
							break;
						
						case "TerminalDisconnect":
						
							if (value == null || value is System.DateTime)
								this.TerminalDisconnect = (System.DateTime?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalDisconnect);
							break;
						
						case "TerminalLastSessionDuration":
						
							if (value == null || value is System.Int32)
								this.TerminalLastSessionDuration = (System.Int32)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalLastSessionDuration);
							break;
						
						case "TerminalLastRestart":
						
							if (value == null || value is System.DateTime)
								this.TerminalLastRestart = (System.DateTime?)value;
								OnPropertyChanged(HistoricoTerminalStateMetadata.PropertyNames.TerminalLastRestart);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esHistoricoTerminalState entity)
			{
				this.entity = entity;
			}
			

			public System.String TerminalID
			{
				get
				{
					return Convert.ToString(entity.TerminalID);
				}

				set
				{
					entity.TerminalID = Convert.ToDecimal(value);
				}
			}
	
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
			
			public System.String VehiculoMatricula
			{
				get
				{
					return Convert.ToString(entity.VehiculoMatricula);
				}

				set
				{
					entity.VehiculoMatricula = Convert.ToString(value);
				}
			}
	
			public System.String CentroTrabajoID
			{
				get
				{
					System.Decimal? data = entity.CentroTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CentroTrabajoID = null;
					else entity.CentroTrabajoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreaTrabajoID
			{
				get
				{
					System.Decimal? data = entity.AreaTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaTrabajoID = null;
					else entity.AreaTrabajoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpresaID
			{
				get
				{
					System.Decimal? data = entity.EmpresaID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpresaID = null;
					else entity.EmpresaID = Convert.ToDecimal(value);
				}
			}
				
			public System.String PersonalID
			{
				get
				{
					System.Decimal? data = entity.PersonalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalID = null;
					else entity.PersonalID = Convert.ToDecimal(value);
				}
			}
			
			public System.String TerminalEstado
			{
				get
				{
					return Convert.ToString(entity.TerminalEstado);
				}

				set
				{
					entity.TerminalEstado = Convert.ToByte(value);
				}
			}

			public System.String TerminalProtocoloTipo
			{
				get
				{
					return Convert.ToString(entity.TerminalProtocoloTipo);
				}

				set
				{
					entity.TerminalProtocoloTipo = Convert.ToByte(value);
				}
			}

			public System.String TerminalNS
			{
				get
				{
					return Convert.ToString(entity.TerminalNS);
				}

				set
				{
					entity.TerminalNS = Convert.ToString(value);
				}
			}

			public System.String TerminalIMEI
			{
				get
				{
					return Convert.ToString(entity.TerminalIMEI);
				}

				set
				{
					entity.TerminalIMEI = Convert.ToString(value);
				}
			}

			public System.String TerminalNSSim
			{
				get
				{
					return Convert.ToString(entity.TerminalNSSim);
				}

				set
				{
					entity.TerminalNSSim = Convert.ToString(value);
				}
			}

			public System.String TerminalVersionSW
			{
				get
				{
					return Convert.ToString(entity.TerminalVersionSW);
				}

				set
				{
					entity.TerminalVersionSW = Convert.ToString(value);
				}
			}
	
			public System.String GpsFecha
			{
				get
				{
					System.DateTime? data = entity.GpsFecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFecha = null;
					else entity.GpsFecha = Convert.ToDateTime(value);
				}
			}
			
			public System.String GpsLatitud
			{
				get
				{
					return Convert.ToString(entity.GpsLatitud);
				}

				set
				{
					entity.GpsLatitud = Convert.ToDouble(value);
				}
			}

			public System.String GpsLongitud
			{
				get
				{
					return Convert.ToString(entity.GpsLongitud);
				}

				set
				{
					entity.GpsLongitud = Convert.ToDouble(value);
				}
			}

			public System.String GpsFixMode
			{
				get
				{
					return Convert.ToString(entity.GpsFixMode);
				}

				set
				{
					entity.GpsFixMode = Convert.ToByte(value);
				}
			}

			public System.String GpsFixGeoTx
			{
				get
				{
					return Convert.ToString(entity.GpsFixGeoTx);
				}

				set
				{
					entity.GpsFixGeoTx = Convert.ToString(value);
				}
			}

			public System.String GpsNSAT
			{
				get
				{
					return Convert.ToString(entity.GpsNSAT);
				}

				set
				{
					entity.GpsNSAT = Convert.ToByte(value);
				}
			}

			public System.String GpsAltitud
			{
				get
				{
					return Convert.ToString(entity.GpsAltitud);
				}

				set
				{
					entity.GpsAltitud = Convert.ToInt16(value);
				}
			}

			public System.String GpsRumbo
			{
				get
				{
					return Convert.ToString(entity.GpsRumbo);
				}

				set
				{
					entity.GpsRumbo = Convert.ToDecimal(value);
				}
			}

			public System.String GpsVelocidad
			{
				get
				{
					return Convert.ToString(entity.GpsVelocidad);
				}

				set
				{
					entity.GpsVelocidad = Convert.ToDouble(value);
				}
			}

			public System.String GpsDistancia
			{
				get
				{
					return Convert.ToString(entity.GpsDistancia);
				}

				set
				{
					entity.GpsDistancia = Convert.ToDouble(value);
				}
			}
	
			public System.String GsmFecha
			{
				get
				{
					System.DateTime? data = entity.GsmFecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GsmFecha = null;
					else entity.GsmFecha = Convert.ToDateTime(value);
				}
			}
			
			public System.String OdometroTt
			{
				get
				{
					return Convert.ToString(entity.OdometroTt);
				}

				set
				{
					entity.OdometroTt = Convert.ToDouble(value);
				}
			}

			public System.String OdometroPc
			{
				get
				{
					return Convert.ToString(entity.OdometroPc);
				}

				set
				{
					entity.OdometroPc = Convert.ToDouble(value);
				}
			}

			public System.String InputState
			{
				get
				{
					return Convert.ToString(entity.InputState);
				}

				set
				{
					entity.InputState = Convert.ToString(value);
				}
			}

			public System.String OutputState
			{
				get
				{
					return Convert.ToString(entity.OutputState);
				}

				set
				{
					entity.OutputState = Convert.ToString(value);
				}
			}
	
			public System.String TerminalConnectedFrom
			{
				get
				{
					System.DateTime? data = entity.TerminalConnectedFrom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalConnectedFrom = null;
					else entity.TerminalConnectedFrom = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalLastReception
			{
				get
				{
					System.DateTime? data = entity.TerminalLastReception;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalLastReception = null;
					else entity.TerminalLastReception = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalDisconnect
			{
				get
				{
					System.DateTime? data = entity.TerminalDisconnect;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalDisconnect = null;
					else entity.TerminalDisconnect = Convert.ToDateTime(value);
				}
			}
			
			public System.String TerminalLastSessionDuration
			{
				get
				{
					return Convert.ToString(entity.TerminalLastSessionDuration);
				}

				set
				{
					entity.TerminalLastSessionDuration = Convert.ToInt32(value);
				}
			}
	
			public System.String TerminalLastRestart
			{
				get
				{
					System.DateTime? data = entity.TerminalLastRestart;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalLastRestart = null;
					else entity.TerminalLastRestart = Convert.ToDateTime(value);
				}
			}
			
			public System.String VehiculoCodigo
			{
				get
				{
					return Convert.ToString(entity.VehiculoCodigo);
				}

				set
				{
					entity.VehiculoCodigo = Convert.ToString(value);
				}
			}

			public System.String VehiculoTelefono
			{
				get
				{
					return Convert.ToString(entity.VehiculoTelefono);
				}

				set
				{
					entity.VehiculoTelefono = Convert.ToString(value);
				}
			}

			public System.String Comentario
			{
				get
				{
					return Convert.ToString(entity.Comentario);
				}

				set
				{
					entity.Comentario = Convert.ToString(value);
				}
			}


			private esHistoricoTerminalState entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return HistoricoTerminalStateMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public HistoricoTerminalStateQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoTerminalStateQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoTerminalStateQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(HistoricoTerminalStateQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private HistoricoTerminalStateQ query;		
	}



	[Serializable]
	abstract public partial class esHistoricoTerminalStateCol : CollectionBase<HistoricoTerminalState>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoTerminalStateMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "HistoricoTerminalStateCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public HistoricoTerminalStateQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoTerminalStateQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoTerminalStateQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoTerminalStateQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(HistoricoTerminalStateQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((HistoricoTerminalStateQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private HistoricoTerminalStateQ query;
	}



	[Serializable]
	abstract public partial class esHistoricoTerminalStateQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoTerminalStateMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalID,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.VehiculoID,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.VehiculoMatricula,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.VehiculoMatricula, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.CentroTrabajoID,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.CentroTrabajoID, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.AreaTrabajoID,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.AreaTrabajoID, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.EmpresaID,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.EmpresaID, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.PersonalID,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.PersonalID, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalEstado,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalEstado, esSystemType.Byte));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalProtocoloTipo,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalProtocoloTipo, esSystemType.Byte));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalNS,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalNS, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalIMEI,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalIMEI, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalNSSim,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalNSSim, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalVersionSW,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalVersionSW, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsFecha,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsFecha, esSystemType.DateTime));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsLatitud,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsLatitud, esSystemType.Double));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsLongitud,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsLongitud, esSystemType.Double));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsFixMode,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsFixMode, esSystemType.Byte));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsFixGeoTx,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsNSAT,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsNSAT, esSystemType.Byte));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsAltitud,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsAltitud, esSystemType.Int16));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsRumbo,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsVelocidad,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsVelocidad, esSystemType.Double));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GpsDistancia,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsDistancia, esSystemType.Double));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.GsmFecha,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GsmFecha, esSystemType.DateTime));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.OdometroTt,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.OdometroTt, esSystemType.Double));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.OdometroPc,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.OdometroPc, esSystemType.Double));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.InputState,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.InputState, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.OutputState,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.OutputState, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalConnectedFrom,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalConnectedFrom, esSystemType.DateTime));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastReception,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalLastReception, esSystemType.DateTime));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalDisconnect,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalDisconnect, esSystemType.DateTime));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastSessionDuration,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalLastSessionDuration, esSystemType.Int32));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastRestart,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalLastRestart, esSystemType.DateTime));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.VehiculoCodigo,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.VehiculoCodigo, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.VehiculoTelefono,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.VehiculoTelefono, esSystemType.String));
			_queryItems.Add(HistoricoTerminalStateMetadata.ColumnNames.Comentario,new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.Comentario, esSystemType.String));
			
	    }
	
	    #region esQueryItems

		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoMatricula
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.VehiculoMatricula, esSystemType.String); }
		} 
		
		public esQueryItem CentroTrabajoID
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.CentroTrabajoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreaTrabajoID
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.AreaTrabajoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpresaID
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.EmpresaID, esSystemType.Decimal); }
		} 
		
		public esQueryItem PersonalID
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.PersonalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalEstado
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalEstado, esSystemType.Byte); }
		} 
		
		public esQueryItem TerminalProtocoloTipo
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalProtocoloTipo, esSystemType.Byte); }
		} 
		
		public esQueryItem TerminalNS
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalNS, esSystemType.String); }
		} 
		
		public esQueryItem TerminalIMEI
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalIMEI, esSystemType.String); }
		} 
		
		public esQueryItem TerminalNSSim
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalNSSim, esSystemType.String); }
		} 
		
		public esQueryItem TerminalVersionSW
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalVersionSW, esSystemType.String); }
		} 
		
		public esQueryItem GpsFecha
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsFecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem GpsLatitud
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsLatitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsLongitud
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsLongitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsFixMode
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsFixMode, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsFixGeoTx
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String); }
		} 
		
		public esQueryItem GpsNSAT
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsNSAT, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsAltitud
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsAltitud, esSystemType.Int16); }
		} 
		
		public esQueryItem GpsRumbo
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal); }
		} 
		
		public esQueryItem GpsVelocidad
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsVelocidad, esSystemType.Double); }
		} 
		
		public esQueryItem GpsDistancia
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GpsDistancia, esSystemType.Double); }
		} 
		
		public esQueryItem GsmFecha
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.GsmFecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem OdometroTt
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.OdometroTt, esSystemType.Double); }
		} 
		
		public esQueryItem OdometroPc
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.OdometroPc, esSystemType.Double); }
		} 
		
		public esQueryItem InputState
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.InputState, esSystemType.String); }
		} 
		
		public esQueryItem OutputState
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.OutputState, esSystemType.String); }
		} 
		
		public esQueryItem TerminalConnectedFrom
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalConnectedFrom, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalLastReception
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalLastReception, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalDisconnect
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalDisconnect, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalLastSessionDuration
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalLastSessionDuration, esSystemType.Int32); }
		} 
		
		public esQueryItem TerminalLastRestart
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.TerminalLastRestart, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehiculoCodigo
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.VehiculoCodigo, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoTelefono
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.VehiculoTelefono, esSystemType.String); }
		} 
		
		public esQueryItem Comentario
		{
			get { return new esQueryItem(this, HistoricoTerminalStateMetadata.ColumnNames.Comentario, esSystemType.String); }
		} 
		
		#endregion
		
	}


	
	public partial class HistoricoTerminalState : esHistoricoTerminalState
	{

		
		
	}
	



	[Serializable]
	public partial class HistoricoTerminalStateMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HistoricoTerminalStateMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalID, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalID;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.VehiculoID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.VehiculoMatricula, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.VehiculoMatricula;
			c.CharacterMaxLength = 12;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.CentroTrabajoID, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.CentroTrabajoID;
			c.NumericPrecision = 18;
			c.Description = "Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.AreaTrabajoID, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.AreaTrabajoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.EmpresaID, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.EmpresaID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.PersonalID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.PersonalID;
			c.NumericPrecision = 18;
			c.Description = "Conductor Logado actualmente en el Vehiculo.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalEstado, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalEstado;
			c.NumericPrecision = 3;
			c.Description = "Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul ";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalProtocoloTipo, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalProtocoloTipo;
			c.NumericPrecision = 3;
			c.Description = "Identifica el Tipo de Protocolo de comunicacion con el Terminal. 1- MDT. 2-MDV.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalNS, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalNS;
			c.CharacterMaxLength = 30;
			c.Description = "Nº de Serie del Terminal o Identificacion del Terminal Unico guardado en GITS. El Proceso debe comparar los datos mandados por el Terminal (NSerie, IMEI,NSerieSim) y revisar si coincide alguno de ellos con la Identificacion del Dispositivo Terminal en GITS. Se obtiene de la Tabla GITS.dbo.TerminalesGPS campo mdt_NS.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalIMEI, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalIMEI;
			c.CharacterMaxLength = 30;
			c.Description = "Nº de IMEI del model del Terminal. Se rellena cuando lo envia el Terminal a realizar la conexion.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalNSSim, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalNSSim;
			c.CharacterMaxLength = 30;
			c.Description = "Numero de Serie de la tarjeta sim del terminal. Simplemente se utiliza de control.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalVersionSW, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalVersionSW;
			c.CharacterMaxLength = 15;
			c.Description = "Version del software del dispoisitvo, fundamental a la hora de update el software del Dispositvo.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsFecha, 13, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsFecha;
			c.Description = "Fecha de la Ultima posicion GPS válida recibida. Las tramas de Estado y EstadoServicio, que mandan informacion GPS, siempre envia una posicion válida si el Terminal ha podido obtener una, independientemente que en el momento del envio de la trama no esté obteniendo posiciones validas. Para calcular este fecha, hay que mirar el campo GpsFixMode sea >1, que indica que el Fix es valido. Si no este fecha, no se actualiza.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsLatitud, 14, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsLatitud;
			c.NumericPrecision = 15;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsLongitud, 15, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsLongitud;
			c.NumericPrecision = 15;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsFixMode, 16, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsFixMode;
			c.NumericPrecision = 3;
			c.Description = "Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsFixGeoTx, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsFixGeoTx;
			c.CharacterMaxLength = 500;
			c.Description = "Georefencia textual de la posicion obtenida en GPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsNSAT, 18, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsNSAT;
			c.NumericPrecision = 3;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsAltitud, 19, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsAltitud;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsRumbo, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsRumbo;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsVelocidad, 21, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsVelocidad;
			c.NumericPrecision = 15;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GpsDistancia, 22, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GpsDistancia;
			c.NumericPrecision = 15;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.GsmFecha, 23, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.GsmFecha;
			c.Description = "Fecha de la Ultima trama recibida desde el Terminal.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.OdometroTt, 24, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.OdometroTt;
			c.NumericPrecision = 15;
			c.Description = "Valor del Odometro Total";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.OdometroPc, 25, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.OdometroPc;
			c.NumericPrecision = 15;
			c.Description = "Valor del Odometro Parcial";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.InputState, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.InputState;
			c.CharacterMaxLength = 20;
			c.Description = "Valor hexadecimal de las Señales de entrada del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.OutputState, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.OutputState;
			c.CharacterMaxLength = 20;
			c.Description = "Valor hexadecimal de las Señales de salida del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalConnectedFrom, 28, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalConnectedFrom;
			c.Description = "Indica si el Terminal está conectado. Si es <> null => conectado sino Desconectado. Guarda la fecha del comienzo de conexion.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastReception, 29, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalLastReception;
			c.Description = "Fecha de la ultima recepcion obtenida desde este Terminal. ";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalDisconnect, 30, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalDisconnect;
			c.Description = "Si posee valor indica que el Terminal está desconectado, y marca la fecha de desconexion.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastSessionDuration, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalLastSessionDuration;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Duración en Horas de la ultima sesion ininterrumpida que este terminal estuvo conectado.  Es null si nunca se conecto este Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.TerminalLastRestart, 32, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.TerminalLastRestart;
			c.Description = "Fecha del ultimo reinicio forzado. Bien por reparación o fallo electrico, o por fallo del Terminal.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.VehiculoCodigo, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.VehiculoCodigo;
			c.CharacterMaxLength = 10;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.VehiculoTelefono, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.VehiculoTelefono;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalStateMetadata.ColumnNames.Comentario, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalStateMetadata.PropertyNames.Comentario;
			c.CharacterMaxLength = 500;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public HistoricoTerminalStateMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string EmpresaID = "EmpresaID";
			 public const string PersonalID = "PersonalID";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string TerminalProtocoloTipo = "TerminalProtocoloTipo";
			 public const string TerminalNS = "TerminalNS";
			 public const string TerminalIMEI = "TerminalIMEI";
			 public const string TerminalNSSim = "TerminalNSSim";
			 public const string TerminalVersionSW = "TerminalVersionSW";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsNSAT = "GpsNSAT";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string GsmFecha = "GsmFecha";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string TerminalConnectedFrom = "TerminalConnectedFrom";
			 public const string TerminalLastReception = "TerminalLastReception";
			 public const string TerminalDisconnect = "TerminalDisconnect";
			 public const string TerminalLastSessionDuration = "TerminalLastSessionDuration";
			 public const string TerminalLastRestart = "TerminalLastRestart";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoTelefono = "VehiculoTelefono";
			 public const string Comentario = "Comentario";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string EmpresaID = "EmpresaID";
			 public const string PersonalID = "PersonalID";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string TerminalProtocoloTipo = "TerminalProtocoloTipo";
			 public const string TerminalNS = "TerminalNS";
			 public const string TerminalIMEI = "TerminalIMEI";
			 public const string TerminalNSSim = "TerminalNSSim";
			 public const string TerminalVersionSW = "TerminalVersionSW";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsNSAT = "GpsNSAT";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string GsmFecha = "GsmFecha";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string TerminalConnectedFrom = "TerminalConnectedFrom";
			 public const string TerminalLastReception = "TerminalLastReception";
			 public const string TerminalDisconnect = "TerminalDisconnect";
			 public const string TerminalLastSessionDuration = "TerminalLastSessionDuration";
			 public const string TerminalLastRestart = "TerminalLastRestart";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoTelefono = "VehiculoTelefono";
			 public const string Comentario = "Comentario";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HistoricoTerminalStateMetadata))
			{
				if(HistoricoTerminalStateMetadata.mapDelegates == null)
				{
					HistoricoTerminalStateMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HistoricoTerminalStateMetadata.meta == null)
				{
					HistoricoTerminalStateMetadata.meta = new HistoricoTerminalStateMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoMatricula", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CentroTrabajoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreaTrabajoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpresaID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PersonalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalEstado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TerminalProtocoloTipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TerminalNS", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalIMEI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalNSSim", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalVersionSW", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("GpsFecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GpsLatitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsLongitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsFixMode", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsFixGeoTx", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("GpsNSAT", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsAltitud", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("GpsRumbo", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("GpsVelocidad", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsDistancia", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GsmFecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("OdometroTt", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("OdometroPc", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("InputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("OutputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalConnectedFrom", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalLastReception", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalDisconnect", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalLastSessionDuration", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TerminalLastRestart", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("VehiculoCodigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoTelefono", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("Comentario", new esTypeMap("nvarchar", "System.String"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "HistoricoTerminalState";
				meta.Destination = "HistoricoTerminalState";
				
				meta.spInsert = "proc_HistoricoTerminalStateInsert";				
				meta.spUpdate = "proc_HistoricoTerminalStateUpdate";		
				meta.spDelete = "proc_HistoricoTerminalStateDelete";
				meta.spLoadAll = "proc_HistoricoTerminalStateLoadAll";
				meta.spLoadByPrimaryKey = "proc_HistoricoTerminalStateLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HistoricoTerminalStateMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
