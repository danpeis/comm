
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// MANTIENE EL ESTADO DE LOS TERMINALES ACTIVOS. RELLENA A TRAVES DE INTEGRACION CON GITS, AL ASIGNAR A UN VEHICULO UN TERMINAL, Y SE ELIMINA AL BORRAR ESTA VINCULACION. TAMBIEN AUTOMATICAMENTE SE COMPRUEBA AL INICIAR EL SERVIDOR,  EL ESTADO DEL TERMINAL Y VEHICULO. CUANDO EL TERMINAL ES DESVINCULADO O PUESTO INACTIVO, ESTE DATO SE PASA A LA TABLA [HistoricoTerminalState].
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TerminalState")]
	public partial class TerminalState : esTerminalState
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TerminalState();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal terminalID)
		{
			var obj = new TerminalState();
			obj.TerminalID = terminalID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal terminalID, esSqlAccessType sqlAccessType)
		{
			var obj = new TerminalState();
			obj.TerminalID = terminalID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal TERMINALID)
        {
            try
            {
                var e = new TerminalState();
                return (e.LoadByPrimaryKey(TERMINALID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TerminalState Get(System.Decimal TERMINALID)
        {
            try
            {
                var e = new TerminalState();
                return (e.LoadByPrimaryKey(TERMINALID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TerminalState(System.Decimal TERMINALID)
        {
            this.LoadByPrimaryKey(TERMINALID);
        }
		
		public TerminalState()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TerminalStateCol")]
	public partial class TerminalStateCol : esTerminalStateCol, IEnumerable<TerminalState>
	{
	
		public TerminalStateCol()
		{
		}

	
	
		public TerminalState FindByPrimaryKey(System.Decimal terminalID)
		{
			return this.SingleOrDefault(e => e.TerminalID == terminalID);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TerminalState))]
		public class TerminalStateColWCFPacket : esCollectionWCFPacket<TerminalStateCol>
		{
			public static implicit operator TerminalStateCol(TerminalStateColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TerminalStateColWCFPacket(TerminalStateCol collection)
			{
				return new TerminalStateColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TerminalStateQ : esTerminalStateQ
	{
		public TerminalStateQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TerminalStateQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TerminalStateQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new TerminalStateQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TerminalStateQ query)
		{
			return TerminalStateQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TerminalStateQ(string query)
		{
			return (TerminalStateQ)TerminalStateQ.SerializeHelper.FromXml(query, typeof(TerminalStateQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTerminalState : EntityBase
	{
		public esTerminalState()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal terminalID)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(terminalID);
			else
				return LoadByPrimaryKeyStoredProcedure(terminalID);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal terminalID)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(terminalID);
			else
				return LoadByPrimaryKeyStoredProcedure(terminalID);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal terminalID)
		{
			TerminalStateQ query = new TerminalStateQ();
			query.Where(query.TerminalID == terminalID);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal terminalID)
		{
			esParameters parms = new esParameters();
			parms.Add("TerminalID", terminalID);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal TerminalID
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalStateMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal VehiculoID
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalStateMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.VehiculoCodigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigo
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.VehiculoCodigo);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.VehiculoCodigo, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.VehiculoCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.VehiculoCodigoADM
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigoADM
		{
			get
			{
				return base.GetSystemString(TerminalStateMetadata.ColumnNames.VehiculoCodigoADM);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.VehiculoCodigoADM, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.VehiculoCodigoADM);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.VehiculoMatricula
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoMatricula
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.VehiculoMatricula);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.VehiculoMatricula, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.VehiculoMatricula);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.VehiculoTelefono
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoTelefono
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.VehiculoTelefono);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.VehiculoTelefono, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.VehiculoTelefono);
				}
			}
		}		
		
		/// <summary>
		/// Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CentroTrabajoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateMetadata.ColumnNames.CentroTrabajoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.CentroTrabajoID, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.CentroTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.AreaTrabajoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreaTrabajoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateMetadata.ColumnNames.AreaTrabajoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.AreaTrabajoID, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.AreaTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.AreaTrabajoColor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreaTrabajoColor
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateMetadata.ColumnNames.AreaTrabajoColor);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.AreaTrabajoColor, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.AreaTrabajoColor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.EmpresaID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpresaID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateMetadata.ColumnNames.EmpresaID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.EmpresaID, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.EmpresaID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.EmpresaColor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpresaColor
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateMetadata.ColumnNames.EmpresaColor);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.EmpresaColor, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.EmpresaColor);
				}
			}
		}		
		
		/// <summary>
		/// Conductor Logado actualmente en el Vehiculo.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PersonalID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateMetadata.ColumnNames.PersonalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.PersonalID, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.PersonalID);
				}
			}
		}		
		
		/// <summary>
		/// Fecha del Login mas actual. Si esta fecha tiene valor, la fecha del logout es Nula y a la inversa.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? LoginFrom
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.LoginFrom);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.LoginFrom, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.LoginFrom);
				}
			}
		}		
		
		/// <summary>
		/// Fecha del Logout mas actual. Si esta fecha tiene valor, la fecha del login es Nula y a la inversa.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? LogoutFrom
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.LogoutFrom);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.LogoutFrom, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.LogoutFrom);
				}
			}
		}		
		
		/// <summary>
		/// Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 TerminalEstado
		{
			get
			{
				return base.GetSystemInt32Required(TerminalStateMetadata.ColumnNames.TerminalEstado);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateMetadata.ColumnNames.TerminalEstado, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalEstado);
				}
			}
		}		
		
		/// <summary>
		/// Identifica el Tipo de Protocolo de comunicacion con el Terminal. 1- MDT. 2-MDV.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte TerminalProtocoloTipo
		{
			get
			{
				return base.GetSystemByteRequired(TerminalStateMetadata.ColumnNames.TerminalProtocoloTipo);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalStateMetadata.ColumnNames.TerminalProtocoloTipo, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalProtocoloTipo);
				}
			}
		}		
		
		/// <summary>
		/// Nº de Serie del Terminal o Identificacion del Terminal Unico guardado en GITS. El Proceso debe comparar los datos mandados por el Terminal (NSerie, IMEI,NSerieSim) y revisar si coincide alguno de ellos con la Identificacion del Dispositivo Terminal en GITS. Se obtiene de la Tabla GITS.dbo.TerminalesGPS campo mdt_NS.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalNS
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.TerminalNS);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.TerminalNS, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalNS);
				}
			}
		}		
		
		/// <summary>
		/// Nº de IMEI del model del Terminal. Se rellena cuando lo envia el Terminal a realizar la conexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalIMEI
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.TerminalIMEI);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.TerminalIMEI, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalIMEI);
				}
			}
		}		
		
		/// <summary>
		/// Numero de Serie de la tarjeta sim del terminal. Simplemente se utiliza de control.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalNSSim
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.TerminalNSSim);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.TerminalNSSim, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalNSSim);
				}
			}
		}		
		
		/// <summary>
		/// Version del software del dispoisitvo, fundamental a la hora de update el software del Dispositvo.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalVersionSW
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.TerminalVersionSW);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.TerminalVersionSW, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalVersionSW);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la Ultima posicion GPS válida recibida. Las tramas de Estado y EstadoServicio, que mandan informacion GPS, siempre envia una posicion válida si el Terminal ha podido obtener una, independientemente que en el momento del envio de la trama no esté obteniendo posiciones validas. Para calcular este fecha, hay que mirar el campo GpsFixMode sea >1, que indica que el Fix es valido. Si no este fecha, no se actualiza.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? GpsFecha
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.GpsFecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.GpsFecha, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsFecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GpsLatitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLatitud
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalStateMetadata.ColumnNames.GpsLatitud);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateMetadata.ColumnNames.GpsLatitud, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsLatitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GpsLongitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLongitud
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalStateMetadata.ColumnNames.GpsLongitud);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateMetadata.ColumnNames.GpsLongitud, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsLongitud);
				}
			}
		}		
		
		/// <summary>
		/// Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte GpsFixMode
		{
			get
			{
				return base.GetSystemByteRequired(TerminalStateMetadata.ColumnNames.GpsFixMode);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalStateMetadata.ColumnNames.GpsFixMode, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsFixMode);
				}
			}
		}		
		
		/// <summary>
		/// Georefencia textual de la posicion obtenida en GPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GpsFixGeoTx
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.GpsFixGeoTx);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.GpsFixGeoTx, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsFixGeoTx);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GpsAltitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16 GpsAltitud
		{
			get
			{
				return base.GetSystemInt16Required(TerminalStateMetadata.ColumnNames.GpsAltitud);
			}
			
			set
			{
				if(base.SetSystemInt16(TerminalStateMetadata.ColumnNames.GpsAltitud, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsAltitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GpsRumbo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal GpsRumbo
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalStateMetadata.ColumnNames.GpsRumbo);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.GpsRumbo, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsRumbo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GpsVelocidad
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 GpsVelocidad
		{
			get
			{
				return base.GetSystemInt32Required(TerminalStateMetadata.ColumnNames.GpsVelocidad);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateMetadata.ColumnNames.GpsVelocidad, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsVelocidad);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GpsDistancia
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsDistancia
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalStateMetadata.ColumnNames.GpsDistancia);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateMetadata.ColumnNames.GpsDistancia, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsDistancia);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GpsFechaDetenido
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? GpsFechaDetenido
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.GpsFechaDetenido);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.GpsFechaDetenido, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsFechaDetenido);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Total
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double OdometroTt
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalStateMetadata.ColumnNames.OdometroTt);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateMetadata.ColumnNames.OdometroTt, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.OdometroTt);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Parcial
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double OdometroPc
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalStateMetadata.ColumnNames.OdometroPc);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateMetadata.ColumnNames.OdometroPc, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.OdometroPc);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de entrada del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String InputState
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.InputState);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.InputState, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.InputState);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de salida del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String OutputState
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.OutputState);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.OutputState, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.OutputState);
				}
			}
		}		
		
		/// <summary>
		/// Servidor que gestiona la conexión de este Terminal. Esta vinculado a la conexion. Si estuviera desconectado,  este valor seria null.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? ServerID
		{
			get
			{
				return base.GetSystemByte(TerminalStateMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalStateMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la ultima recepcion obtenida desde este Terminal. 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalLastReception
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.TerminalLastReception);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.TerminalLastReception, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalLastReception);
				}
			}
		}		
		
		/// <summary>
		/// Indica si el Terminal está conectado. Si es <> null => conectado sino Desconectado. Guarda la fecha del comienzo de conexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalConnectedFrom
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.TerminalConnectedFrom);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.TerminalConnectedFrom, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalConnectedFrom);
				}
			}
		}		
		
		/// <summary>
		/// Duración en Horas de la ultima sesion ininterrumpida que este terminal estuvo conectado.  Es null si nunca se conecto este Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal TerminalLastSessionDuration
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalStateMetadata.ColumnNames.TerminalLastSessionDuration);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.TerminalLastSessionDuration, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalLastSessionDuration);
				}
			}
		}		
		
		/// <summary>
		/// Si posee valor indica que el Terminal está desconectado, y marca la fecha de desconexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalDisconnect
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.TerminalDisconnect);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.TerminalDisconnect, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalDisconnect);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.TerminalDisconnectCount
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16 TerminalDisconnectCount
		{
			get
			{
				return base.GetSystemInt16Required(TerminalStateMetadata.ColumnNames.TerminalDisconnectCount);
			}
			
			set
			{
				if(base.SetSystemInt16(TerminalStateMetadata.ColumnNames.TerminalDisconnectCount, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalDisconnectCount);
				}
			}
		}		
		
		/// <summary>
		/// Fecha del ultimo reinicio forzado. Bien por reparación o fallo electrico, o por fallo del Terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalLastRestart
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.TerminalLastRestart);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.TerminalLastRestart, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalLastRestart);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la Ultima trama recibida desde el Terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaTerminalInfoMasActual
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.FechaTerminalInfoMasActual);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.FechaTerminalInfoMasActual, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.FechaTerminalInfoMasActual);
				}
			}
		}		
		
		/// <summary>
		/// Indica la fecha de la última vez que este terminal se actualizo los ficheros maestros de mantenimientos que el Servidor envie a los Terminales.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaLastSyncroMtnos
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.FechaLastSyncroMtnos);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.FechaLastSyncroMtnos, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.FechaLastSyncroMtnos);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.FlotaID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? FlotaID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateMetadata.ColumnNames.FlotaID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.FlotaID, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.FlotaID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.ModoHistorico
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean ModoHistorico
		{
			get
			{
				return base.GetSystemBooleanRequired(TerminalStateMetadata.ColumnNames.ModoHistorico);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalStateMetadata.ColumnNames.ModoHistorico, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.ModoHistorico);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.ModoHistoricoDesde
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? ModoHistoricoDesde
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.ModoHistoricoDesde);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.ModoHistoricoDesde, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.ModoHistoricoDesde);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.PowerState
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16 PowerState
		{
			get
			{
				return base.GetSystemInt16Required(TerminalStateMetadata.ColumnNames.PowerState);
			}
			
			set
			{
				if(base.SetSystemInt16(TerminalStateMetadata.ColumnNames.PowerState, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.PowerState);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.PowerStateTx
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PowerStateTx
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.PowerStateTx);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.PowerStateTx, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.PowerStateTx);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.PowerStateDesde
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PowerStateDesde
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.PowerStateDesde);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.PowerStateDesde, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.PowerStateDesde);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GrupoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? GrupoID
		{
			get
			{
				return base.GetSystemInt32(TerminalStateMetadata.ColumnNames.GrupoID);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateMetadata.ColumnNames.GrupoID, value))
				{
					this._UpToGrupoTerminalByGrupoID = null;
					this.OnPropertyChanged("UpToGrupoTerminalByGrupoID");
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GrupoID);
				}
			}
		}		
		
		/// <summary>
		/// Es el ID de la Plantilla de Configuración asociada a este Terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? TemplateConfigID
		{
			get
			{
				return base.GetSystemInt32(TerminalStateMetadata.ColumnNames.TemplateConfigID);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateMetadata.ColumnNames.TemplateConfigID, value))
				{
					this._UpToTemplateConfigurationByTemplateConfigID = null;
					this.OnPropertyChanged("UpToTemplateConfigurationByTemplateConfigID");
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TemplateConfigID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.AltaFecha
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? AltaFecha
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.AltaFecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.AltaFecha, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.AltaFecha);
				}
			}
		}		
		
		/// <summary>
		/// Indica si el Terminal esta en estado Especial: 1:Comida  2:Mantenimiento
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal EstadoEspecial
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalStateMetadata.ColumnNames.EstadoEspecial);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.EstadoEspecial, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.EstadoEspecial);
				}
			}
		}		
		
		/// <summary>
		/// Indica la fecha de la última vez que este terminal se actualizo los ficheros maestros de mantenimientos que el Servidor envie a los Terminales.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? EstadoEspecialDesde
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.EstadoEspecialDesde);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.EstadoEspecialDesde, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.EstadoEspecialDesde);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.TipoActividadID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TipoActividadID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateMetadata.ColumnNames.TipoActividadID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateMetadata.ColumnNames.TipoActividadID, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.TipoActividadID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.ProximoServicioEn
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? ProximoServicioEn
		{
			get
			{
				return base.GetSystemInt32(TerminalStateMetadata.ColumnNames.ProximoServicioEn);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateMetadata.ColumnNames.ProximoServicioEn, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.ProximoServicioEn);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.ICCID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Iccid
		{
			get
			{
				return base.GetSystemString(TerminalStateMetadata.ColumnNames.Iccid);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.Iccid, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.Iccid);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de Activacion del Autologout
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? AutologoutDesde
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateMetadata.ColumnNames.AutologoutDesde);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateMetadata.ColumnNames.AutologoutDesde, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.AutologoutDesde);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.SoftwareTipo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? SoftwareTipo
		{
			get
			{
				return base.GetSystemByte(TerminalStateMetadata.ColumnNames.SoftwareTipo);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalStateMetadata.ColumnNames.SoftwareTipo, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.SoftwareTipo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.EliminacionActiva
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 EliminacionActiva
		{
			get
			{
				return base.GetSystemInt32Required(TerminalStateMetadata.ColumnNames.EliminacionActiva);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateMetadata.ColumnNames.EliminacionActiva, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.EliminacionActiva);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.WorkingNow
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean WorkingNow
		{
			get
			{
				return base.GetSystemBooleanRequired(TerminalStateMetadata.ColumnNames.WorkingNow);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalStateMetadata.ColumnNames.WorkingNow, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.WorkingNow);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.OnLine
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean OnLine
		{
			get
			{
				return base.GetSystemBooleanRequired(TerminalStateMetadata.ColumnNames.OnLine);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalStateMetadata.ColumnNames.OnLine, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.OnLine);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.OffLineCausa
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String OffLineCausa
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.OffLineCausa);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.OffLineCausa, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.OffLineCausa);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GpsLatitudAnterior
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? GpsLatitudAnterior
		{
			get
			{
				return base.GetSystemDouble(TerminalStateMetadata.ColumnNames.GpsLatitudAnterior);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateMetadata.ColumnNames.GpsLatitudAnterior, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsLatitudAnterior);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.GpsLongitudAnterior
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? GpsLongitudAnterior
		{
			get
			{
				return base.GetSystemDouble(TerminalStateMetadata.ColumnNames.GpsLongitudAnterior);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateMetadata.ColumnNames.GpsLongitudAnterior, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsLongitudAnterior);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalState.Comentario
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Comentario
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateMetadata.ColumnNames.Comentario);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateMetadata.ColumnNames.Comentario, value))
				{
					OnPropertyChanged(TerminalStateMetadata.PropertyNames.Comentario);
				}
			}
		}		
		
		[CLSCompliant(false)]
		internal protected GrupoTerminal _UpToGrupoTerminalByGrupoID;
		[CLSCompliant(false)]
		internal protected TemplateConfiguration _UpToTemplateConfigurationByTemplateConfigID;
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "VehiculoCodigo": this.str().VehiculoCodigo = (string)value; break;							
						case "VehiculoCodigoADM": this.str().VehiculoCodigoADM = (string)value; break;							
						case "VehiculoMatricula": this.str().VehiculoMatricula = (string)value; break;							
						case "VehiculoTelefono": this.str().VehiculoTelefono = (string)value; break;							
						case "CentroTrabajoID": this.str().CentroTrabajoID = (string)value; break;							
						case "AreaTrabajoID": this.str().AreaTrabajoID = (string)value; break;							
						case "AreaTrabajoColor": this.str().AreaTrabajoColor = (string)value; break;							
						case "EmpresaID": this.str().EmpresaID = (string)value; break;							
						case "EmpresaColor": this.str().EmpresaColor = (string)value; break;							
						case "PersonalID": this.str().PersonalID = (string)value; break;							
						case "LoginFrom": this.str().LoginFrom = (string)value; break;							
						case "LogoutFrom": this.str().LogoutFrom = (string)value; break;							
						case "TerminalEstado": this.str().TerminalEstado = (string)value; break;							
						case "TerminalProtocoloTipo": this.str().TerminalProtocoloTipo = (string)value; break;							
						case "TerminalNS": this.str().TerminalNS = (string)value; break;							
						case "TerminalIMEI": this.str().TerminalIMEI = (string)value; break;							
						case "TerminalNSSim": this.str().TerminalNSSim = (string)value; break;							
						case "TerminalVersionSW": this.str().TerminalVersionSW = (string)value; break;							
						case "GpsFecha": this.str().GpsFecha = (string)value; break;							
						case "GpsLatitud": this.str().GpsLatitud = (string)value; break;							
						case "GpsLongitud": this.str().GpsLongitud = (string)value; break;							
						case "GpsFixMode": this.str().GpsFixMode = (string)value; break;							
						case "GpsFixGeoTx": this.str().GpsFixGeoTx = (string)value; break;							
						case "GpsAltitud": this.str().GpsAltitud = (string)value; break;							
						case "GpsRumbo": this.str().GpsRumbo = (string)value; break;							
						case "GpsVelocidad": this.str().GpsVelocidad = (string)value; break;							
						case "GpsDistancia": this.str().GpsDistancia = (string)value; break;							
						case "GpsFechaDetenido": this.str().GpsFechaDetenido = (string)value; break;							
						case "OdometroTt": this.str().OdometroTt = (string)value; break;							
						case "OdometroPc": this.str().OdometroPc = (string)value; break;							
						case "InputState": this.str().InputState = (string)value; break;							
						case "OutputState": this.str().OutputState = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;							
						case "TerminalLastReception": this.str().TerminalLastReception = (string)value; break;							
						case "TerminalConnectedFrom": this.str().TerminalConnectedFrom = (string)value; break;							
						case "TerminalLastSessionDuration": this.str().TerminalLastSessionDuration = (string)value; break;							
						case "TerminalDisconnect": this.str().TerminalDisconnect = (string)value; break;							
						case "TerminalDisconnectCount": this.str().TerminalDisconnectCount = (string)value; break;							
						case "TerminalLastRestart": this.str().TerminalLastRestart = (string)value; break;							
						case "FechaTerminalInfoMasActual": this.str().FechaTerminalInfoMasActual = (string)value; break;							
						case "FechaLastSyncroMtnos": this.str().FechaLastSyncroMtnos = (string)value; break;							
						case "FlotaID": this.str().FlotaID = (string)value; break;							
						case "ModoHistorico": this.str().ModoHistorico = (string)value; break;							
						case "ModoHistoricoDesde": this.str().ModoHistoricoDesde = (string)value; break;							
						case "PowerState": this.str().PowerState = (string)value; break;							
						case "PowerStateTx": this.str().PowerStateTx = (string)value; break;							
						case "PowerStateDesde": this.str().PowerStateDesde = (string)value; break;							
						case "GrupoID": this.str().GrupoID = (string)value; break;							
						case "TemplateConfigID": this.str().TemplateConfigID = (string)value; break;							
						case "AltaFecha": this.str().AltaFecha = (string)value; break;							
						case "EstadoEspecial": this.str().EstadoEspecial = (string)value; break;							
						case "EstadoEspecialDesde": this.str().EstadoEspecialDesde = (string)value; break;							
						case "TipoActividadID": this.str().TipoActividadID = (string)value; break;							
						case "ProximoServicioEn": this.str().ProximoServicioEn = (string)value; break;							
						case "Iccid": this.str().Iccid = (string)value; break;							
						case "AutologoutDesde": this.str().AutologoutDesde = (string)value; break;							
						case "SoftwareTipo": this.str().SoftwareTipo = (string)value; break;							
						case "EliminacionActiva": this.str().EliminacionActiva = (string)value; break;							
						case "WorkingNow": this.str().WorkingNow = (string)value; break;							
						case "OnLine": this.str().OnLine = (string)value; break;							
						case "OffLineCausa": this.str().OffLineCausa = (string)value; break;							
						case "GpsLatitudAnterior": this.str().GpsLatitudAnterior = (string)value; break;							
						case "GpsLongitudAnterior": this.str().GpsLongitudAnterior = (string)value; break;							
						case "Comentario": this.str().Comentario = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.VehiculoID);
							break;
						
						case "CentroTrabajoID":
						
							if (value == null || value is System.Decimal)
								this.CentroTrabajoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.CentroTrabajoID);
							break;
						
						case "AreaTrabajoID":
						
							if (value == null || value is System.Decimal)
								this.AreaTrabajoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.AreaTrabajoID);
							break;
						
						case "AreaTrabajoColor":
						
							if (value == null || value is System.Decimal)
								this.AreaTrabajoColor = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.AreaTrabajoColor);
							break;
						
						case "EmpresaID":
						
							if (value == null || value is System.Decimal)
								this.EmpresaID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.EmpresaID);
							break;
						
						case "EmpresaColor":
						
							if (value == null || value is System.Decimal)
								this.EmpresaColor = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.EmpresaColor);
							break;
						
						case "PersonalID":
						
							if (value == null || value is System.Decimal)
								this.PersonalID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.PersonalID);
							break;
						
						case "LoginFrom":
						
							if (value == null || value is System.DateTime)
								this.LoginFrom = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.LoginFrom);
							break;
						
						case "LogoutFrom":
						
							if (value == null || value is System.DateTime)
								this.LogoutFrom = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.LogoutFrom);
							break;
						
						case "TerminalEstado":
						
							if (value == null || value is System.Int32)
								this.TerminalEstado = (System.Int32)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalEstado);
							break;
						
						case "TerminalProtocoloTipo":
						
							if (value == null || value is System.Byte)
								this.TerminalProtocoloTipo = (System.Byte)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalProtocoloTipo);
							break;
						
						case "GpsFecha":
						
							if (value == null || value is System.DateTime)
								this.GpsFecha = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsFecha);
							break;
						
						case "GpsLatitud":
						
							if (value == null || value is System.Double)
								this.GpsLatitud = (System.Double)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsLatitud);
							break;
						
						case "GpsLongitud":
						
							if (value == null || value is System.Double)
								this.GpsLongitud = (System.Double)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsLongitud);
							break;
						
						case "GpsFixMode":
						
							if (value == null || value is System.Byte)
								this.GpsFixMode = (System.Byte)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsFixMode);
							break;
						
						case "GpsAltitud":
						
							if (value == null || value is System.Int16)
								this.GpsAltitud = (System.Int16)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsAltitud);
							break;
						
						case "GpsRumbo":
						
							if (value == null || value is System.Decimal)
								this.GpsRumbo = (System.Decimal)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsRumbo);
							break;
						
						case "GpsVelocidad":
						
							if (value == null || value is System.Int32)
								this.GpsVelocidad = (System.Int32)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsVelocidad);
							break;
						
						case "GpsDistancia":
						
							if (value == null || value is System.Double)
								this.GpsDistancia = (System.Double)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsDistancia);
							break;
						
						case "GpsFechaDetenido":
						
							if (value == null || value is System.DateTime)
								this.GpsFechaDetenido = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsFechaDetenido);
							break;
						
						case "OdometroTt":
						
							if (value == null || value is System.Double)
								this.OdometroTt = (System.Double)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.OdometroTt);
							break;
						
						case "OdometroPc":
						
							if (value == null || value is System.Double)
								this.OdometroPc = (System.Double)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.OdometroPc);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Byte)
								this.ServerID = (System.Byte?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.ServerID);
							break;
						
						case "TerminalLastReception":
						
							if (value == null || value is System.DateTime)
								this.TerminalLastReception = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalLastReception);
							break;
						
						case "TerminalConnectedFrom":
						
							if (value == null || value is System.DateTime)
								this.TerminalConnectedFrom = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalConnectedFrom);
							break;
						
						case "TerminalLastSessionDuration":
						
							if (value == null || value is System.Decimal)
								this.TerminalLastSessionDuration = (System.Decimal)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalLastSessionDuration);
							break;
						
						case "TerminalDisconnect":
						
							if (value == null || value is System.DateTime)
								this.TerminalDisconnect = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalDisconnect);
							break;
						
						case "TerminalDisconnectCount":
						
							if (value == null || value is System.Int16)
								this.TerminalDisconnectCount = (System.Int16)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalDisconnectCount);
							break;
						
						case "TerminalLastRestart":
						
							if (value == null || value is System.DateTime)
								this.TerminalLastRestart = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TerminalLastRestart);
							break;
						
						case "FechaTerminalInfoMasActual":
						
							if (value == null || value is System.DateTime)
								this.FechaTerminalInfoMasActual = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.FechaTerminalInfoMasActual);
							break;
						
						case "FechaLastSyncroMtnos":
						
							if (value == null || value is System.DateTime)
								this.FechaLastSyncroMtnos = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.FechaLastSyncroMtnos);
							break;
						
						case "FlotaID":
						
							if (value == null || value is System.Decimal)
								this.FlotaID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.FlotaID);
							break;
						
						case "ModoHistorico":
						
							if (value == null || value is System.Boolean)
								this.ModoHistorico = (System.Boolean)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.ModoHistorico);
							break;
						
						case "ModoHistoricoDesde":
						
							if (value == null || value is System.DateTime)
								this.ModoHistoricoDesde = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.ModoHistoricoDesde);
							break;
						
						case "PowerState":
						
							if (value == null || value is System.Int16)
								this.PowerState = (System.Int16)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.PowerState);
							break;
						
						case "PowerStateDesde":
						
							if (value == null || value is System.DateTime)
								this.PowerStateDesde = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.PowerStateDesde);
							break;
						
						case "GrupoID":
						
							if (value == null || value is System.Int32)
								this.GrupoID = (System.Int32?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GrupoID);
							break;
						
						case "TemplateConfigID":
						
							if (value == null || value is System.Int32)
								this.TemplateConfigID = (System.Int32?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TemplateConfigID);
							break;
						
						case "AltaFecha":
						
							if (value == null || value is System.DateTime)
								this.AltaFecha = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.AltaFecha);
							break;
						
						case "EstadoEspecial":
						
							if (value == null || value is System.Decimal)
								this.EstadoEspecial = (System.Decimal)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.EstadoEspecial);
							break;
						
						case "EstadoEspecialDesde":
						
							if (value == null || value is System.DateTime)
								this.EstadoEspecialDesde = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.EstadoEspecialDesde);
							break;
						
						case "TipoActividadID":
						
							if (value == null || value is System.Decimal)
								this.TipoActividadID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.TipoActividadID);
							break;
						
						case "ProximoServicioEn":
						
							if (value == null || value is System.Int32)
								this.ProximoServicioEn = (System.Int32?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.ProximoServicioEn);
							break;
						
						case "AutologoutDesde":
						
							if (value == null || value is System.DateTime)
								this.AutologoutDesde = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.AutologoutDesde);
							break;
						
						case "SoftwareTipo":
						
							if (value == null || value is System.Byte)
								this.SoftwareTipo = (System.Byte?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.SoftwareTipo);
							break;
						
						case "EliminacionActiva":
						
							if (value == null || value is System.Int32)
								this.EliminacionActiva = (System.Int32)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.EliminacionActiva);
							break;
						
						case "WorkingNow":
						
							if (value == null || value is System.Boolean)
								this.WorkingNow = (System.Boolean)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.WorkingNow);
							break;
						
						case "OnLine":
						
							if (value == null || value is System.Boolean)
								this.OnLine = (System.Boolean)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.OnLine);
							break;
						
						case "GpsLatitudAnterior":
						
							if (value == null || value is System.Double)
								this.GpsLatitudAnterior = (System.Double?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsLatitudAnterior);
							break;
						
						case "GpsLongitudAnterior":
						
							if (value == null || value is System.Double)
								this.GpsLongitudAnterior = (System.Double?)value;
								OnPropertyChanged(TerminalStateMetadata.PropertyNames.GpsLongitudAnterior);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTerminalState entity)
			{
				this.entity = entity;
			}
			

			public System.String TerminalID
			{
				get
				{
					return Convert.ToString(entity.TerminalID);
				}

				set
				{
					entity.TerminalID = Convert.ToDecimal(value);
				}
			}

			public System.String VehiculoID
			{
				get
				{
					return Convert.ToString(entity.VehiculoID);
				}

				set
				{
					entity.VehiculoID = Convert.ToDecimal(value);
				}
			}

			public System.String VehiculoCodigo
			{
				get
				{
					return Convert.ToString(entity.VehiculoCodigo);
				}

				set
				{
					entity.VehiculoCodigo = Convert.ToString(value);
				}
			}
	
			public System.String VehiculoCodigoADM
			{
				get
				{
					System.String data = entity.VehiculoCodigoADM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoCodigoADM = null;
					else entity.VehiculoCodigoADM = Convert.ToString(value);
				}
			}
			
			public System.String VehiculoMatricula
			{
				get
				{
					return Convert.ToString(entity.VehiculoMatricula);
				}

				set
				{
					entity.VehiculoMatricula = Convert.ToString(value);
				}
			}

			public System.String VehiculoTelefono
			{
				get
				{
					return Convert.ToString(entity.VehiculoTelefono);
				}

				set
				{
					entity.VehiculoTelefono = Convert.ToString(value);
				}
			}
	
			public System.String CentroTrabajoID
			{
				get
				{
					System.Decimal? data = entity.CentroTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CentroTrabajoID = null;
					else entity.CentroTrabajoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreaTrabajoID
			{
				get
				{
					System.Decimal? data = entity.AreaTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaTrabajoID = null;
					else entity.AreaTrabajoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreaTrabajoColor
			{
				get
				{
					System.Decimal? data = entity.AreaTrabajoColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaTrabajoColor = null;
					else entity.AreaTrabajoColor = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpresaID
			{
				get
				{
					System.Decimal? data = entity.EmpresaID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpresaID = null;
					else entity.EmpresaID = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpresaColor
			{
				get
				{
					System.Decimal? data = entity.EmpresaColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpresaColor = null;
					else entity.EmpresaColor = Convert.ToDecimal(value);
				}
			}
				
			public System.String PersonalID
			{
				get
				{
					System.Decimal? data = entity.PersonalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalID = null;
					else entity.PersonalID = Convert.ToDecimal(value);
				}
			}
				
			public System.String LoginFrom
			{
				get
				{
					System.DateTime? data = entity.LoginFrom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LoginFrom = null;
					else entity.LoginFrom = Convert.ToDateTime(value);
				}
			}
				
			public System.String LogoutFrom
			{
				get
				{
					System.DateTime? data = entity.LogoutFrom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LogoutFrom = null;
					else entity.LogoutFrom = Convert.ToDateTime(value);
				}
			}
			
			public System.String TerminalEstado
			{
				get
				{
					return Convert.ToString(entity.TerminalEstado);
				}

				set
				{
					entity.TerminalEstado = Convert.ToInt32(value);
				}
			}

			public System.String TerminalProtocoloTipo
			{
				get
				{
					return Convert.ToString(entity.TerminalProtocoloTipo);
				}

				set
				{
					entity.TerminalProtocoloTipo = Convert.ToByte(value);
				}
			}

			public System.String TerminalNS
			{
				get
				{
					return Convert.ToString(entity.TerminalNS);
				}

				set
				{
					entity.TerminalNS = Convert.ToString(value);
				}
			}

			public System.String TerminalIMEI
			{
				get
				{
					return Convert.ToString(entity.TerminalIMEI);
				}

				set
				{
					entity.TerminalIMEI = Convert.ToString(value);
				}
			}

			public System.String TerminalNSSim
			{
				get
				{
					return Convert.ToString(entity.TerminalNSSim);
				}

				set
				{
					entity.TerminalNSSim = Convert.ToString(value);
				}
			}

			public System.String TerminalVersionSW
			{
				get
				{
					return Convert.ToString(entity.TerminalVersionSW);
				}

				set
				{
					entity.TerminalVersionSW = Convert.ToString(value);
				}
			}
	
			public System.String GpsFecha
			{
				get
				{
					System.DateTime? data = entity.GpsFecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFecha = null;
					else entity.GpsFecha = Convert.ToDateTime(value);
				}
			}
			
			public System.String GpsLatitud
			{
				get
				{
					return Convert.ToString(entity.GpsLatitud);
				}

				set
				{
					entity.GpsLatitud = Convert.ToDouble(value);
				}
			}

			public System.String GpsLongitud
			{
				get
				{
					return Convert.ToString(entity.GpsLongitud);
				}

				set
				{
					entity.GpsLongitud = Convert.ToDouble(value);
				}
			}

			public System.String GpsFixMode
			{
				get
				{
					return Convert.ToString(entity.GpsFixMode);
				}

				set
				{
					entity.GpsFixMode = Convert.ToByte(value);
				}
			}

			public System.String GpsFixGeoTx
			{
				get
				{
					return Convert.ToString(entity.GpsFixGeoTx);
				}

				set
				{
					entity.GpsFixGeoTx = Convert.ToString(value);
				}
			}

			public System.String GpsAltitud
			{
				get
				{
					return Convert.ToString(entity.GpsAltitud);
				}

				set
				{
					entity.GpsAltitud = Convert.ToInt16(value);
				}
			}

			public System.String GpsRumbo
			{
				get
				{
					return Convert.ToString(entity.GpsRumbo);
				}

				set
				{
					entity.GpsRumbo = Convert.ToDecimal(value);
				}
			}

			public System.String GpsVelocidad
			{
				get
				{
					return Convert.ToString(entity.GpsVelocidad);
				}

				set
				{
					entity.GpsVelocidad = Convert.ToInt32(value);
				}
			}

			public System.String GpsDistancia
			{
				get
				{
					return Convert.ToString(entity.GpsDistancia);
				}

				set
				{
					entity.GpsDistancia = Convert.ToDouble(value);
				}
			}
	
			public System.String GpsFechaDetenido
			{
				get
				{
					System.DateTime? data = entity.GpsFechaDetenido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFechaDetenido = null;
					else entity.GpsFechaDetenido = Convert.ToDateTime(value);
				}
			}
			
			public System.String OdometroTt
			{
				get
				{
					return Convert.ToString(entity.OdometroTt);
				}

				set
				{
					entity.OdometroTt = Convert.ToDouble(value);
				}
			}

			public System.String OdometroPc
			{
				get
				{
					return Convert.ToString(entity.OdometroPc);
				}

				set
				{
					entity.OdometroPc = Convert.ToDouble(value);
				}
			}

			public System.String InputState
			{
				get
				{
					return Convert.ToString(entity.InputState);
				}

				set
				{
					entity.InputState = Convert.ToString(value);
				}
			}

			public System.String OutputState
			{
				get
				{
					return Convert.ToString(entity.OutputState);
				}

				set
				{
					entity.OutputState = Convert.ToString(value);
				}
			}
	
			public System.String ServerID
			{
				get
				{
					System.Byte? data = entity.ServerID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServerID = null;
					else entity.ServerID = Convert.ToByte(value);
				}
			}
				
			public System.String TerminalLastReception
			{
				get
				{
					System.DateTime? data = entity.TerminalLastReception;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalLastReception = null;
					else entity.TerminalLastReception = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalConnectedFrom
			{
				get
				{
					System.DateTime? data = entity.TerminalConnectedFrom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalConnectedFrom = null;
					else entity.TerminalConnectedFrom = Convert.ToDateTime(value);
				}
			}
			
			public System.String TerminalLastSessionDuration
			{
				get
				{
					return Convert.ToString(entity.TerminalLastSessionDuration);
				}

				set
				{
					entity.TerminalLastSessionDuration = Convert.ToDecimal(value);
				}
			}
	
			public System.String TerminalDisconnect
			{
				get
				{
					System.DateTime? data = entity.TerminalDisconnect;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalDisconnect = null;
					else entity.TerminalDisconnect = Convert.ToDateTime(value);
				}
			}
			
			public System.String TerminalDisconnectCount
			{
				get
				{
					return Convert.ToString(entity.TerminalDisconnectCount);
				}

				set
				{
					entity.TerminalDisconnectCount = Convert.ToInt16(value);
				}
			}
	
			public System.String TerminalLastRestart
			{
				get
				{
					System.DateTime? data = entity.TerminalLastRestart;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalLastRestart = null;
					else entity.TerminalLastRestart = Convert.ToDateTime(value);
				}
			}
				
			public System.String FechaTerminalInfoMasActual
			{
				get
				{
					System.DateTime? data = entity.FechaTerminalInfoMasActual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaTerminalInfoMasActual = null;
					else entity.FechaTerminalInfoMasActual = Convert.ToDateTime(value);
				}
			}
				
			public System.String FechaLastSyncroMtnos
			{
				get
				{
					System.DateTime? data = entity.FechaLastSyncroMtnos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaLastSyncroMtnos = null;
					else entity.FechaLastSyncroMtnos = Convert.ToDateTime(value);
				}
			}
				
			public System.String FlotaID
			{
				get
				{
					System.Decimal? data = entity.FlotaID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlotaID = null;
					else entity.FlotaID = Convert.ToDecimal(value);
				}
			}
			
			public System.String ModoHistorico
			{
				get
				{
					return Convert.ToString(entity.ModoHistorico);
				}

				set
				{
					entity.ModoHistorico = Convert.ToBoolean(value);
				}
			}
	
			public System.String ModoHistoricoDesde
			{
				get
				{
					System.DateTime? data = entity.ModoHistoricoDesde;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ModoHistoricoDesde = null;
					else entity.ModoHistoricoDesde = Convert.ToDateTime(value);
				}
			}
			
			public System.String PowerState
			{
				get
				{
					return Convert.ToString(entity.PowerState);
				}

				set
				{
					entity.PowerState = Convert.ToInt16(value);
				}
			}

			public System.String PowerStateTx
			{
				get
				{
					return Convert.ToString(entity.PowerStateTx);
				}

				set
				{
					entity.PowerStateTx = Convert.ToString(value);
				}
			}
	
			public System.String PowerStateDesde
			{
				get
				{
					System.DateTime? data = entity.PowerStateDesde;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PowerStateDesde = null;
					else entity.PowerStateDesde = Convert.ToDateTime(value);
				}
			}
				
			public System.String GrupoID
			{
				get
				{
					System.Int32? data = entity.GrupoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GrupoID = null;
					else entity.GrupoID = Convert.ToInt32(value);
				}
			}
				
			public System.String TemplateConfigID
			{
				get
				{
					System.Int32? data = entity.TemplateConfigID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TemplateConfigID = null;
					else entity.TemplateConfigID = Convert.ToInt32(value);
				}
			}
				
			public System.String AltaFecha
			{
				get
				{
					System.DateTime? data = entity.AltaFecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AltaFecha = null;
					else entity.AltaFecha = Convert.ToDateTime(value);
				}
			}
			
			public System.String EstadoEspecial
			{
				get
				{
					return Convert.ToString(entity.EstadoEspecial);
				}

				set
				{
					entity.EstadoEspecial = Convert.ToDecimal(value);
				}
			}
	
			public System.String EstadoEspecialDesde
			{
				get
				{
					System.DateTime? data = entity.EstadoEspecialDesde;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EstadoEspecialDesde = null;
					else entity.EstadoEspecialDesde = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoActividadID
			{
				get
				{
					System.Decimal? data = entity.TipoActividadID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoActividadID = null;
					else entity.TipoActividadID = Convert.ToDecimal(value);
				}
			}
				
			public System.String ProximoServicioEn
			{
				get
				{
					System.Int32? data = entity.ProximoServicioEn;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ProximoServicioEn = null;
					else entity.ProximoServicioEn = Convert.ToInt32(value);
				}
			}
				
			public System.String Iccid
			{
				get
				{
					System.String data = entity.Iccid;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Iccid = null;
					else entity.Iccid = Convert.ToString(value);
				}
			}
				
			public System.String AutologoutDesde
			{
				get
				{
					System.DateTime? data = entity.AutologoutDesde;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AutologoutDesde = null;
					else entity.AutologoutDesde = Convert.ToDateTime(value);
				}
			}
				
			public System.String SoftwareTipo
			{
				get
				{
					System.Byte? data = entity.SoftwareTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SoftwareTipo = null;
					else entity.SoftwareTipo = Convert.ToByte(value);
				}
			}
			
			public System.String EliminacionActiva
			{
				get
				{
					return Convert.ToString(entity.EliminacionActiva);
				}

				set
				{
					entity.EliminacionActiva = Convert.ToInt32(value);
				}
			}

			public System.String WorkingNow
			{
				get
				{
					return Convert.ToString(entity.WorkingNow);
				}

				set
				{
					entity.WorkingNow = Convert.ToBoolean(value);
				}
			}

			public System.String OnLine
			{
				get
				{
					return Convert.ToString(entity.OnLine);
				}

				set
				{
					entity.OnLine = Convert.ToBoolean(value);
				}
			}

			public System.String OffLineCausa
			{
				get
				{
					return Convert.ToString(entity.OffLineCausa);
				}

				set
				{
					entity.OffLineCausa = Convert.ToString(value);
				}
			}
	
			public System.String GpsLatitudAnterior
			{
				get
				{
					System.Double? data = entity.GpsLatitudAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsLatitudAnterior = null;
					else entity.GpsLatitudAnterior = Convert.ToDouble(value);
				}
			}
				
			public System.String GpsLongitudAnterior
			{
				get
				{
					System.Double? data = entity.GpsLongitudAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsLongitudAnterior = null;
					else entity.GpsLongitudAnterior = Convert.ToDouble(value);
				}
			}
			
			public System.String Comentario
			{
				get
				{
					return Convert.ToString(entity.Comentario);
				}

				set
				{
					entity.Comentario = Convert.ToString(value);
				}
			}


			private esTerminalState entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TerminalStateMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TerminalStateQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalStateQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalStateQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TerminalStateQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TerminalStateQ query;		
	}



	[Serializable]
	abstract public partial class esTerminalStateCol : CollectionBase<TerminalState>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TerminalStateMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TerminalStateCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TerminalStateQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalStateQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalStateQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TerminalStateQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TerminalStateQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TerminalStateQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TerminalStateQ query;
	}



	[Serializable]
	abstract public partial class esTerminalStateQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TerminalStateMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.VehiculoID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.VehiculoCodigo,new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoCodigo, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.VehiculoCodigoADM,new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoCodigoADM, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.VehiculoMatricula,new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoMatricula, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.VehiculoTelefono,new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoTelefono, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.CentroTrabajoID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.CentroTrabajoID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.AreaTrabajoID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.AreaTrabajoID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.AreaTrabajoColor,new esQueryItem(this, TerminalStateMetadata.ColumnNames.AreaTrabajoColor, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.EmpresaID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.EmpresaID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.EmpresaColor,new esQueryItem(this, TerminalStateMetadata.ColumnNames.EmpresaColor, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.PersonalID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.PersonalID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.LoginFrom,new esQueryItem(this, TerminalStateMetadata.ColumnNames.LoginFrom, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.LogoutFrom,new esQueryItem(this, TerminalStateMetadata.ColumnNames.LogoutFrom, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalEstado,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalEstado, esSystemType.Int32));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalProtocoloTipo,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalProtocoloTipo, esSystemType.Byte));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalNS,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalNS, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalIMEI,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalIMEI, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalNSSim,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalNSSim, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalVersionSW,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalVersionSW, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsFecha,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsFecha, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsLatitud,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsLatitud, esSystemType.Double));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsLongitud,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsLongitud, esSystemType.Double));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsFixMode,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsFixMode, esSystemType.Byte));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsFixGeoTx,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsAltitud,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsAltitud, esSystemType.Int16));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsRumbo,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsVelocidad,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsVelocidad, esSystemType.Int32));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsDistancia,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsDistancia, esSystemType.Double));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsFechaDetenido,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsFechaDetenido, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.OdometroTt,new esQueryItem(this, TerminalStateMetadata.ColumnNames.OdometroTt, esSystemType.Double));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.OdometroPc,new esQueryItem(this, TerminalStateMetadata.ColumnNames.OdometroPc, esSystemType.Double));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.InputState,new esQueryItem(this, TerminalStateMetadata.ColumnNames.InputState, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.OutputState,new esQueryItem(this, TerminalStateMetadata.ColumnNames.OutputState, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.ServerID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.ServerID, esSystemType.Byte));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalLastReception,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalLastReception, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalConnectedFrom,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalConnectedFrom, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalLastSessionDuration,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalLastSessionDuration, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalDisconnect,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalDisconnect, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalDisconnectCount,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalDisconnectCount, esSystemType.Int16));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TerminalLastRestart,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalLastRestart, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.FechaTerminalInfoMasActual,new esQueryItem(this, TerminalStateMetadata.ColumnNames.FechaTerminalInfoMasActual, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.FechaLastSyncroMtnos,new esQueryItem(this, TerminalStateMetadata.ColumnNames.FechaLastSyncroMtnos, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.FlotaID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.FlotaID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.ModoHistorico,new esQueryItem(this, TerminalStateMetadata.ColumnNames.ModoHistorico, esSystemType.Boolean));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.ModoHistoricoDesde,new esQueryItem(this, TerminalStateMetadata.ColumnNames.ModoHistoricoDesde, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.PowerState,new esQueryItem(this, TerminalStateMetadata.ColumnNames.PowerState, esSystemType.Int16));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.PowerStateTx,new esQueryItem(this, TerminalStateMetadata.ColumnNames.PowerStateTx, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.PowerStateDesde,new esQueryItem(this, TerminalStateMetadata.ColumnNames.PowerStateDesde, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GrupoID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GrupoID, esSystemType.Int32));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TemplateConfigID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TemplateConfigID, esSystemType.Int32));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.AltaFecha,new esQueryItem(this, TerminalStateMetadata.ColumnNames.AltaFecha, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.EstadoEspecial,new esQueryItem(this, TerminalStateMetadata.ColumnNames.EstadoEspecial, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.EstadoEspecialDesde,new esQueryItem(this, TerminalStateMetadata.ColumnNames.EstadoEspecialDesde, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.TipoActividadID,new esQueryItem(this, TerminalStateMetadata.ColumnNames.TipoActividadID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.ProximoServicioEn,new esQueryItem(this, TerminalStateMetadata.ColumnNames.ProximoServicioEn, esSystemType.Int32));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.Iccid,new esQueryItem(this, TerminalStateMetadata.ColumnNames.Iccid, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.AutologoutDesde,new esQueryItem(this, TerminalStateMetadata.ColumnNames.AutologoutDesde, esSystemType.DateTime));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.SoftwareTipo,new esQueryItem(this, TerminalStateMetadata.ColumnNames.SoftwareTipo, esSystemType.Byte));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.EliminacionActiva,new esQueryItem(this, TerminalStateMetadata.ColumnNames.EliminacionActiva, esSystemType.Int32));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.WorkingNow,new esQueryItem(this, TerminalStateMetadata.ColumnNames.WorkingNow, esSystemType.Boolean));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.OnLine,new esQueryItem(this, TerminalStateMetadata.ColumnNames.OnLine, esSystemType.Boolean));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.OffLineCausa,new esQueryItem(this, TerminalStateMetadata.ColumnNames.OffLineCausa, esSystemType.String));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsLatitudAnterior,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsLatitudAnterior, esSystemType.Double));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.GpsLongitudAnterior,new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsLongitudAnterior, esSystemType.Double));
			_queryItems.Add(TerminalStateMetadata.ColumnNames.Comentario,new esQueryItem(this, TerminalStateMetadata.ColumnNames.Comentario, esSystemType.String));
			
	    }
	
	    #region esQueryItems

		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoCodigo
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoCodigo, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoCodigoADM
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoCodigoADM, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoMatricula
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoMatricula, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoTelefono
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.VehiculoTelefono, esSystemType.String); }
		} 
		
		public esQueryItem CentroTrabajoID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.CentroTrabajoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreaTrabajoID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.AreaTrabajoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreaTrabajoColor
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.AreaTrabajoColor, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpresaID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.EmpresaID, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpresaColor
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.EmpresaColor, esSystemType.Decimal); }
		} 
		
		public esQueryItem PersonalID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.PersonalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem LoginFrom
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.LoginFrom, esSystemType.DateTime); }
		} 
		
		public esQueryItem LogoutFrom
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.LogoutFrom, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalEstado
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalEstado, esSystemType.Int32); }
		} 
		
		public esQueryItem TerminalProtocoloTipo
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalProtocoloTipo, esSystemType.Byte); }
		} 
		
		public esQueryItem TerminalNS
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalNS, esSystemType.String); }
		} 
		
		public esQueryItem TerminalIMEI
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalIMEI, esSystemType.String); }
		} 
		
		public esQueryItem TerminalNSSim
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalNSSim, esSystemType.String); }
		} 
		
		public esQueryItem TerminalVersionSW
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalVersionSW, esSystemType.String); }
		} 
		
		public esQueryItem GpsFecha
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsFecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem GpsLatitud
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsLatitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsLongitud
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsLongitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsFixMode
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsFixMode, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsFixGeoTx
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String); }
		} 
		
		public esQueryItem GpsAltitud
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsAltitud, esSystemType.Int16); }
		} 
		
		public esQueryItem GpsRumbo
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal); }
		} 
		
		public esQueryItem GpsVelocidad
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsVelocidad, esSystemType.Int32); }
		} 
		
		public esQueryItem GpsDistancia
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsDistancia, esSystemType.Double); }
		} 
		
		public esQueryItem GpsFechaDetenido
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsFechaDetenido, esSystemType.DateTime); }
		} 
		
		public esQueryItem OdometroTt
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.OdometroTt, esSystemType.Double); }
		} 
		
		public esQueryItem OdometroPc
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.OdometroPc, esSystemType.Double); }
		} 
		
		public esQueryItem InputState
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.InputState, esSystemType.String); }
		} 
		
		public esQueryItem OutputState
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.OutputState, esSystemType.String); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.ServerID, esSystemType.Byte); }
		} 
		
		public esQueryItem TerminalLastReception
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalLastReception, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalConnectedFrom
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalConnectedFrom, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalLastSessionDuration
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalLastSessionDuration, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalDisconnect
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalDisconnect, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalDisconnectCount
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalDisconnectCount, esSystemType.Int16); }
		} 
		
		public esQueryItem TerminalLastRestart
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TerminalLastRestart, esSystemType.DateTime); }
		} 
		
		public esQueryItem FechaTerminalInfoMasActual
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.FechaTerminalInfoMasActual, esSystemType.DateTime); }
		} 
		
		public esQueryItem FechaLastSyncroMtnos
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.FechaLastSyncroMtnos, esSystemType.DateTime); }
		} 
		
		public esQueryItem FlotaID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.FlotaID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ModoHistorico
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.ModoHistorico, esSystemType.Boolean); }
		} 
		
		public esQueryItem ModoHistoricoDesde
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.ModoHistoricoDesde, esSystemType.DateTime); }
		} 
		
		public esQueryItem PowerState
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.PowerState, esSystemType.Int16); }
		} 
		
		public esQueryItem PowerStateTx
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.PowerStateTx, esSystemType.String); }
		} 
		
		public esQueryItem PowerStateDesde
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.PowerStateDesde, esSystemType.DateTime); }
		} 
		
		public esQueryItem GrupoID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GrupoID, esSystemType.Int32); }
		} 
		
		public esQueryItem TemplateConfigID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TemplateConfigID, esSystemType.Int32); }
		} 
		
		public esQueryItem AltaFecha
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.AltaFecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem EstadoEspecial
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.EstadoEspecial, esSystemType.Decimal); }
		} 
		
		public esQueryItem EstadoEspecialDesde
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.EstadoEspecialDesde, esSystemType.DateTime); }
		} 
		
		public esQueryItem TipoActividadID
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.TipoActividadID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ProximoServicioEn
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.ProximoServicioEn, esSystemType.Int32); }
		} 
		
		public esQueryItem Iccid
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.Iccid, esSystemType.String); }
		} 
		
		public esQueryItem AutologoutDesde
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.AutologoutDesde, esSystemType.DateTime); }
		} 
		
		public esQueryItem SoftwareTipo
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.SoftwareTipo, esSystemType.Byte); }
		} 
		
		public esQueryItem EliminacionActiva
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.EliminacionActiva, esSystemType.Int32); }
		} 
		
		public esQueryItem WorkingNow
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.WorkingNow, esSystemType.Boolean); }
		} 
		
		public esQueryItem OnLine
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.OnLine, esSystemType.Boolean); }
		} 
		
		public esQueryItem OffLineCausa
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.OffLineCausa, esSystemType.String); }
		} 
		
		public esQueryItem GpsLatitudAnterior
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsLatitudAnterior, esSystemType.Double); }
		} 
		
		public esQueryItem GpsLongitudAnterior
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.GpsLongitudAnterior, esSystemType.Double); }
		} 
		
		public esQueryItem Comentario
		{
			get { return new esQueryItem(this, TerminalStateMetadata.ColumnNames.Comentario, esSystemType.String); }
		} 
		
		#endregion
		
	}


	
	public partial class TerminalState : esTerminalState
	{

				
		#region TerminalSelectiveSoftwareUpdate - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - FK_TerminalSelectiveSoftwareUpdate_TerminalState
		/// </summary>

		[XmlIgnore]
		public TerminalSelectiveSoftwareUpdate TerminalSelectiveSoftwareUpdate
		{
			get
			{
				if (this.es.IsLazyLoadDisabled) return null;
				
				if(this._TerminalSelectiveSoftwareUpdate == null)
				{
					this._TerminalSelectiveSoftwareUpdate = new TerminalSelectiveSoftwareUpdate();
					this._TerminalSelectiveSoftwareUpdate.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("TerminalSelectiveSoftwareUpdate", this._TerminalSelectiveSoftwareUpdate);
				
					if(this.TerminalID != null)
					{
						this._TerminalSelectiveSoftwareUpdate.Query.Where(this._TerminalSelectiveSoftwareUpdate.Query.TerminalID == this.TerminalID);
						this._TerminalSelectiveSoftwareUpdate.Query.Load();
					}
				}

				return this._TerminalSelectiveSoftwareUpdate;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TerminalSelectiveSoftwareUpdate != null) 
				{ 
					this.RemovePostOneSave("TerminalSelectiveSoftwareUpdate"); 
					this._TerminalSelectiveSoftwareUpdate = null;
					this.OnPropertyChanged("TerminalSelectiveSoftwareUpdate");
				} 
			}          			
		}
		
		
		private TerminalSelectiveSoftwareUpdate _TerminalSelectiveSoftwareUpdate;
		#endregion

				
		#region UpToGrupoTerminalByGrupoID - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_TerminalState_GrupoTerminal
		/// </summary>

		[XmlIgnore]
					
		public GrupoTerminal UpToGrupoTerminalByGrupoID
		{
			get
			{
				if (this.es.IsLazyLoadDisabled) return null;
				
				if(this._UpToGrupoTerminalByGrupoID == null && GrupoID != null)
				{
					this._UpToGrupoTerminalByGrupoID = new GrupoTerminal();
					this._UpToGrupoTerminalByGrupoID.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrupoTerminalByGrupoID", this._UpToGrupoTerminalByGrupoID);
					this._UpToGrupoTerminalByGrupoID.Query.Where(this._UpToGrupoTerminalByGrupoID.Query.Id == this.GrupoID);
					this._UpToGrupoTerminalByGrupoID.Query.Load();
				}	
				return this._UpToGrupoTerminalByGrupoID;
			}
			
			set
			{
				this.RemovePreSave("UpToGrupoTerminalByGrupoID");
				
				bool changed = this._UpToGrupoTerminalByGrupoID != value;

				if(value == null)
				{
					this.GrupoID = null;
					this._UpToGrupoTerminalByGrupoID = null;
				}
				else
				{
					this.GrupoID = value.Id;
					this._UpToGrupoTerminalByGrupoID = value;
					this.SetPreSave("UpToGrupoTerminalByGrupoID", this._UpToGrupoTerminalByGrupoID);
				}
				
				if( changed )
				{
					this.OnPropertyChanged("UpToGrupoTerminalByGrupoID");
				}
			}
		}
		#endregion
		

				
		#region UpToTemplateConfigurationByTemplateConfigID - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_TerminalState_TemplateConfiguration
		/// </summary>

		[XmlIgnore]
					
		public TemplateConfiguration UpToTemplateConfigurationByTemplateConfigID
		{
			get
			{
				if (this.es.IsLazyLoadDisabled) return null;
				
				if(this._UpToTemplateConfigurationByTemplateConfigID == null && TemplateConfigID != null)
				{
					this._UpToTemplateConfigurationByTemplateConfigID = new TemplateConfiguration();
					this._UpToTemplateConfigurationByTemplateConfigID.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTemplateConfigurationByTemplateConfigID", this._UpToTemplateConfigurationByTemplateConfigID);
					this._UpToTemplateConfigurationByTemplateConfigID.Query.Where(this._UpToTemplateConfigurationByTemplateConfigID.Query.Id == this.TemplateConfigID);
					this._UpToTemplateConfigurationByTemplateConfigID.Query.Load();
				}	
				return this._UpToTemplateConfigurationByTemplateConfigID;
			}
			
			set
			{
				this.RemovePreSave("UpToTemplateConfigurationByTemplateConfigID");
				
				bool changed = this._UpToTemplateConfigurationByTemplateConfigID != value;

				if(value == null)
				{
					this.TemplateConfigID = null;
					this._UpToTemplateConfigurationByTemplateConfigID = null;
				}
				else
				{
					this.TemplateConfigID = value.Id;
					this._UpToTemplateConfigurationByTemplateConfigID = value;
					this.SetPreSave("UpToTemplateConfigurationByTemplateConfigID", this._UpToTemplateConfigurationByTemplateConfigID);
				}
				
				if( changed )
				{
					this.OnPropertyChanged("UpToTemplateConfigurationByTemplateConfigID");
				}
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToGrupoTerminalByGrupoID != null)
			{
				this.GrupoID = this._UpToGrupoTerminalByGrupoID.Id;
			}
		}
		
	}
	



	[Serializable]
	public partial class TerminalStateMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TerminalStateMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalID, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalID;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.VehiculoID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.VehiculoCodigo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.VehiculoCodigo;
			c.CharacterMaxLength = 10;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.VehiculoCodigoADM, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.VehiculoCodigoADM;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.VehiculoMatricula, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.VehiculoMatricula;
			c.CharacterMaxLength = 12;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.VehiculoTelefono, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.VehiculoTelefono;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.CentroTrabajoID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.CentroTrabajoID;
			c.NumericPrecision = 18;
			c.Description = "Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.AreaTrabajoID, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.AreaTrabajoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.AreaTrabajoColor, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.AreaTrabajoColor;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.EmpresaID, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.EmpresaID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.EmpresaColor, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.EmpresaColor;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.PersonalID, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.PersonalID;
			c.NumericPrecision = 18;
			c.Description = "Conductor Logado actualmente en el Vehiculo.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.LoginFrom, 12, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.LoginFrom;
			c.Description = "Fecha del Login mas actual. Si esta fecha tiene valor, la fecha del logout es Nula y a la inversa.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.LogoutFrom, 13, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.LogoutFrom;
			c.Description = "Fecha del Logout mas actual. Si esta fecha tiene valor, la fecha del login es Nula y a la inversa.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalEstado, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalEstado;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.Description = "Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul ";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalProtocoloTipo, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalProtocoloTipo;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((2))";
			c.Description = "Identifica el Tipo de Protocolo de comunicacion con el Terminal. 1- MDT. 2-MDV.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalNS, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalNS;
			c.CharacterMaxLength = 30;
			c.Description = "Nº de Serie del Terminal o Identificacion del Terminal Unico guardado en GITS. El Proceso debe comparar los datos mandados por el Terminal (NSerie, IMEI,NSerieSim) y revisar si coincide alguno de ellos con la Identificacion del Dispositivo Terminal en GITS. Se obtiene de la Tabla GITS.dbo.TerminalesGPS campo mdt_NS.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalIMEI, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalIMEI;
			c.CharacterMaxLength = 30;
			c.Description = "Nº de IMEI del model del Terminal. Se rellena cuando lo envia el Terminal a realizar la conexion.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalNSSim, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalNSSim;
			c.CharacterMaxLength = 30;
			c.Description = "Numero de Serie de la tarjeta sim del terminal. Simplemente se utiliza de control.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalVersionSW, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalVersionSW;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"/****** Object:  Default [dbo].[EmptyString]    Script Date: 25/03/2015 13:04:57 ******/
CREATE DEFAULT [dbo].[EmptyString] 
AS
'';
";
			c.Description = "Version del software del dispoisitvo, fundamental a la hora de update el software del Dispositvo.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsFecha, 20, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsFecha;
			c.Description = "Fecha de la Ultima posicion GPS válida recibida. Las tramas de Estado y EstadoServicio, que mandan informacion GPS, siempre envia una posicion válida si el Terminal ha podido obtener una, independientemente que en el momento del envio de la trama no esté obteniendo posiciones validas. Para calcular este fecha, hay que mirar el campo GpsFixMode sea >1, que indica que el Fix es valido. Si no este fecha, no se actualiza.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsLatitud, 21, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsLatitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsLongitud, 22, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsLongitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsFixMode, 23, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsFixMode;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.Description = "Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsFixGeoTx, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsFixGeoTx;
			c.CharacterMaxLength = 500;
			c.HasDefault = true;
			c.Default = @"/****** Object:  Default [dbo].[EmptyString]    Script Date: 25/03/2015 13:04:57 ******/
CREATE DEFAULT [dbo].[EmptyString] 
AS
'';
";
			c.Description = "Georefencia textual de la posicion obtenida en GPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsAltitud, 25, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsAltitud;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsRumbo, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsRumbo;
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsVelocidad, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsVelocidad;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsDistancia, 28, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsDistancia;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsFechaDetenido, 29, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsFechaDetenido;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.OdometroTt, 30, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateMetadata.PropertyNames.OdometroTt;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Valor del Odometro Total";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.OdometroPc, 31, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateMetadata.PropertyNames.OdometroPc;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Valor del Odometro Parcial";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.InputState, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.InputState;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"/****** Object:  Default [dbo].[EmptyString]    Script Date: 25/03/2015 13:04:57 ******/
CREATE DEFAULT [dbo].[EmptyString] 
AS
'';
";
			c.Description = "Valor hexadecimal de las Señales de entrada del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.OutputState, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.OutputState;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"/****** Object:  Default [dbo].[EmptyString]    Script Date: 25/03/2015 13:04:57 ******/
CREATE DEFAULT [dbo].[EmptyString] 
AS
'';
";
			c.Description = "Valor hexadecimal de las Señales de salida del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.ServerID, 34, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalStateMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 3;
			c.Description = "Servidor que gestiona la conexión de este Terminal. Esta vinculado a la conexion. Si estuviera desconectado,  este valor seria null.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalLastReception, 35, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalLastReception;
			c.Description = "Fecha de la ultima recepcion obtenida desde este Terminal. ";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalConnectedFrom, 36, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalConnectedFrom;
			c.Description = "Indica si el Terminal está conectado. Si es <> null => conectado sino Desconectado. Guarda la fecha del comienzo de conexion.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalLastSessionDuration, 37, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalLastSessionDuration;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Duración en Horas de la ultima sesion ininterrumpida que este terminal estuvo conectado.  Es null si nunca se conecto este Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalDisconnect, 38, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalDisconnect;
			c.Description = "Si posee valor indica que el Terminal está desconectado, y marca la fecha de desconexion.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalDisconnectCount, 39, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalDisconnectCount;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TerminalLastRestart, 40, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TerminalLastRestart;
			c.Description = "Fecha del ultimo reinicio forzado. Bien por reparación o fallo electrico, o por fallo del Terminal.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.FechaTerminalInfoMasActual, 41, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.FechaTerminalInfoMasActual;
			c.Description = "Fecha de la Ultima trama recibida desde el Terminal.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.FechaLastSyncroMtnos, 42, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.FechaLastSyncroMtnos;
			c.Description = "Indica la fecha de la última vez que este terminal se actualizo los ficheros maestros de mantenimientos que el Servidor envie a los Terminales.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.FlotaID, 43, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.FlotaID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.ModoHistorico, 44, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalStateMetadata.PropertyNames.ModoHistorico;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.ModoHistoricoDesde, 45, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.ModoHistoricoDesde;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.PowerState, 46, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TerminalStateMetadata.PropertyNames.PowerState;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.PowerStateTx, 47, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.PowerStateTx;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.PowerStateDesde, 48, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.PowerStateDesde;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GrupoID, 49, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GrupoID;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TemplateConfigID, 50, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TemplateConfigID;
			c.NumericPrecision = 10;
			c.Description = "Es el ID de la Plantilla de Configuración asociada a este Terminal.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.AltaFecha, 51, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.AltaFecha;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.EstadoEspecial, 52, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.EstadoEspecial;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Indica si el Terminal esta en estado Especial: 1:Comida  2:Mantenimiento";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.EstadoEspecialDesde, 53, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.EstadoEspecialDesde;
			c.Description = "Indica la fecha de la última vez que este terminal se actualizo los ficheros maestros de mantenimientos que el Servidor envie a los Terminales.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.TipoActividadID, 54, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateMetadata.PropertyNames.TipoActividadID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.ProximoServicioEn, 55, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateMetadata.PropertyNames.ProximoServicioEn;
			c.NumericPrecision = 10;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.Iccid, 56, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.Iccid;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.AutologoutDesde, 57, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateMetadata.PropertyNames.AutologoutDesde;
			c.Description = "Fecha de Activacion del Autologout";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.SoftwareTipo, 58, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalStateMetadata.PropertyNames.SoftwareTipo;
			c.NumericPrecision = 3;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.EliminacionActiva, 59, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateMetadata.PropertyNames.EliminacionActiva;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.WorkingNow, 60, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalStateMetadata.PropertyNames.WorkingNow;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.OnLine, 61, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalStateMetadata.PropertyNames.OnLine;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.OffLineCausa, 62, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.OffLineCausa;
			c.CharacterMaxLength = 150;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsLatitudAnterior, 63, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsLatitudAnterior;
			c.NumericPrecision = 15;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.GpsLongitudAnterior, 64, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateMetadata.PropertyNames.GpsLongitudAnterior;
			c.NumericPrecision = 15;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateMetadata.ColumnNames.Comentario, 65, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateMetadata.PropertyNames.Comentario;
			c.CharacterMaxLength = 500;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TerminalStateMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoCodigoADM = "VehiculoCodigoADM";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string VehiculoTelefono = "VehiculoTelefono";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string AreaTrabajoColor = "AreaTrabajoColor";
			 public const string EmpresaID = "EmpresaID";
			 public const string EmpresaColor = "EmpresaColor";
			 public const string PersonalID = "PersonalID";
			 public const string LoginFrom = "LoginFrom";
			 public const string LogoutFrom = "LogoutFrom";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string TerminalProtocoloTipo = "TerminalProtocoloTipo";
			 public const string TerminalNS = "TerminalNS";
			 public const string TerminalIMEI = "TerminalIMEI";
			 public const string TerminalNSSim = "TerminalNSSim";
			 public const string TerminalVersionSW = "TerminalVersionSW";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string GpsFechaDetenido = "GpsFechaDetenido";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string ServerID = "ServerID";
			 public const string TerminalLastReception = "TerminalLastReception";
			 public const string TerminalConnectedFrom = "TerminalConnectedFrom";
			 public const string TerminalLastSessionDuration = "TerminalLastSessionDuration";
			 public const string TerminalDisconnect = "TerminalDisconnect";
			 public const string TerminalDisconnectCount = "TerminalDisconnectCount";
			 public const string TerminalLastRestart = "TerminalLastRestart";
			 public const string FechaTerminalInfoMasActual = "FechaTerminalInfoMasActual";
			 public const string FechaLastSyncroMtnos = "FechaLastSyncroMtnos";
			 public const string FlotaID = "FlotaID";
			 public const string ModoHistorico = "ModoHistorico";
			 public const string ModoHistoricoDesde = "ModoHistoricoDesde";
			 public const string PowerState = "PowerState";
			 public const string PowerStateTx = "PowerStateTx";
			 public const string PowerStateDesde = "PowerStateDesde";
			 public const string GrupoID = "GrupoID";
			 public const string TemplateConfigID = "TemplateConfigID";
			 public const string AltaFecha = "AltaFecha";
			 public const string EstadoEspecial = "EstadoEspecial";
			 public const string EstadoEspecialDesde = "EstadoEspecialDesde";
			 public const string TipoActividadID = "TipoActividadID";
			 public const string ProximoServicioEn = "ProximoServicioEn";
			 public const string Iccid = "ICCID";
			 public const string AutologoutDesde = "AutologoutDesde";
			 public const string SoftwareTipo = "SoftwareTipo";
			 public const string EliminacionActiva = "EliminacionActiva";
			 public const string WorkingNow = "WorkingNow";
			 public const string OnLine = "OnLine";
			 public const string OffLineCausa = "OffLineCausa";
			 public const string GpsLatitudAnterior = "GpsLatitudAnterior";
			 public const string GpsLongitudAnterior = "GpsLongitudAnterior";
			 public const string Comentario = "Comentario";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoCodigoADM = "VehiculoCodigoADM";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string VehiculoTelefono = "VehiculoTelefono";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string AreaTrabajoColor = "AreaTrabajoColor";
			 public const string EmpresaID = "EmpresaID";
			 public const string EmpresaColor = "EmpresaColor";
			 public const string PersonalID = "PersonalID";
			 public const string LoginFrom = "LoginFrom";
			 public const string LogoutFrom = "LogoutFrom";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string TerminalProtocoloTipo = "TerminalProtocoloTipo";
			 public const string TerminalNS = "TerminalNS";
			 public const string TerminalIMEI = "TerminalIMEI";
			 public const string TerminalNSSim = "TerminalNSSim";
			 public const string TerminalVersionSW = "TerminalVersionSW";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string GpsFechaDetenido = "GpsFechaDetenido";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string ServerID = "ServerID";
			 public const string TerminalLastReception = "TerminalLastReception";
			 public const string TerminalConnectedFrom = "TerminalConnectedFrom";
			 public const string TerminalLastSessionDuration = "TerminalLastSessionDuration";
			 public const string TerminalDisconnect = "TerminalDisconnect";
			 public const string TerminalDisconnectCount = "TerminalDisconnectCount";
			 public const string TerminalLastRestart = "TerminalLastRestart";
			 public const string FechaTerminalInfoMasActual = "FechaTerminalInfoMasActual";
			 public const string FechaLastSyncroMtnos = "FechaLastSyncroMtnos";
			 public const string FlotaID = "FlotaID";
			 public const string ModoHistorico = "ModoHistorico";
			 public const string ModoHistoricoDesde = "ModoHistoricoDesde";
			 public const string PowerState = "PowerState";
			 public const string PowerStateTx = "PowerStateTx";
			 public const string PowerStateDesde = "PowerStateDesde";
			 public const string GrupoID = "GrupoID";
			 public const string TemplateConfigID = "TemplateConfigID";
			 public const string AltaFecha = "AltaFecha";
			 public const string EstadoEspecial = "EstadoEspecial";
			 public const string EstadoEspecialDesde = "EstadoEspecialDesde";
			 public const string TipoActividadID = "TipoActividadID";
			 public const string ProximoServicioEn = "ProximoServicioEn";
			 public const string Iccid = "Iccid";
			 public const string AutologoutDesde = "AutologoutDesde";
			 public const string SoftwareTipo = "SoftwareTipo";
			 public const string EliminacionActiva = "EliminacionActiva";
			 public const string WorkingNow = "WorkingNow";
			 public const string OnLine = "OnLine";
			 public const string OffLineCausa = "OffLineCausa";
			 public const string GpsLatitudAnterior = "GpsLatitudAnterior";
			 public const string GpsLongitudAnterior = "GpsLongitudAnterior";
			 public const string Comentario = "Comentario";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TerminalStateMetadata))
			{
				if(TerminalStateMetadata.mapDelegates == null)
				{
					TerminalStateMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TerminalStateMetadata.meta == null)
				{
					TerminalStateMetadata.meta = new TerminalStateMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoCodigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoCodigoADM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoMatricula", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoTelefono", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("CentroTrabajoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreaTrabajoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreaTrabajoColor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpresaID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpresaColor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PersonalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("LoginFrom", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("LogoutFrom", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalEstado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TerminalProtocoloTipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TerminalNS", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalIMEI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalNSSim", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalVersionSW", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("GpsFecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GpsLatitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsLongitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsFixMode", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsFixGeoTx", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("GpsAltitud", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("GpsRumbo", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("GpsVelocidad", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("GpsDistancia", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsFechaDetenido", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("OdometroTt", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("OdometroPc", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("InputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("OutputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ServerID", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TerminalLastReception", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalConnectedFrom", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalLastSessionDuration", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalDisconnect", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalDisconnectCount", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TerminalLastRestart", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FechaTerminalInfoMasActual", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FechaLastSyncroMtnos", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FlotaID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ModoHistorico", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("ModoHistoricoDesde", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PowerState", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("PowerStateTx", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PowerStateDesde", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GrupoID", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TemplateConfigID", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AltaFecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("EstadoEspecial", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EstadoEspecialDesde", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoActividadID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ProximoServicioEn", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Iccid", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("AutologoutDesde", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("SoftwareTipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("EliminacionActiva", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("WorkingNow", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("OnLine", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("OffLineCausa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("GpsLatitudAnterior", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsLongitudAnterior", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("Comentario", new esTypeMap("nvarchar", "System.String"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "TerminalState";
				meta.Destination = "TerminalState";
				
				meta.spInsert = "proc_TerminalStateInsert";				
				meta.spUpdate = "proc_TerminalStateUpdate";		
				meta.spDelete = "proc_TerminalStateDelete";
				meta.spLoadAll = "proc_TerminalStateLoadAll";
				meta.spLoadByPrimaryKey = "proc_TerminalStateLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TerminalStateMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
