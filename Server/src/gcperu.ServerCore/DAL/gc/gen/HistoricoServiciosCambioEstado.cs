
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// ALMACENA LOS CAMBIOS DE ESTADO DE SERVICIOS QUE ESTAN FINALIZADOS. SI UN SERVICIO ESTÁ EN PAPELERA EN GITS, ESTA TABLA SIGUE MANTENIENDO ESTOS ESTADOS, NO SE BORRAN. 
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("HistoricoServiciosCambioEstado")]
	public partial class HistoricoServiciosCambioEstado : esHistoricoServiciosCambioEstado
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new HistoricoServiciosCambioEstado();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new HistoricoServiciosCambioEstado();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new HistoricoServiciosCambioEstado();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new HistoricoServiciosCambioEstado();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static HistoricoServiciosCambioEstado Get(System.Decimal ID)
        {
            try
            {
                var e = new HistoricoServiciosCambioEstado();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public HistoricoServiciosCambioEstado(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public HistoricoServiciosCambioEstado()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("HistoricoServiciosCambioEstadoCol")]
	public partial class HistoricoServiciosCambioEstadoCol : esHistoricoServiciosCambioEstadoCol, IEnumerable<HistoricoServiciosCambioEstado>
	{
	
		public HistoricoServiciosCambioEstadoCol()
		{
		}

	
	
		public HistoricoServiciosCambioEstado FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(HistoricoServiciosCambioEstado))]
		public class HistoricoServiciosCambioEstadoColWCFPacket : esCollectionWCFPacket<HistoricoServiciosCambioEstadoCol>
		{
			public static implicit operator HistoricoServiciosCambioEstadoCol(HistoricoServiciosCambioEstadoColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator HistoricoServiciosCambioEstadoColWCFPacket(HistoricoServiciosCambioEstadoCol collection)
			{
				return new HistoricoServiciosCambioEstadoColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class HistoricoServiciosCambioEstadoQ : esHistoricoServiciosCambioEstadoQ
	{
		public HistoricoServiciosCambioEstadoQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public HistoricoServiciosCambioEstadoQ()
		{
		}

		override protected string GetQueryName()
		{
			return "HistoricoServiciosCambioEstadoQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new HistoricoServiciosCambioEstadoQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(HistoricoServiciosCambioEstadoQ query)
		{
			return HistoricoServiciosCambioEstadoQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator HistoricoServiciosCambioEstadoQ(string query)
		{
			return (HistoricoServiciosCambioEstadoQ)HistoricoServiciosCambioEstadoQ.SerializeHelper.FromXml(query, typeof(HistoricoServiciosCambioEstadoQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esHistoricoServiciosCambioEstado : EntityBase
	{
		public esHistoricoServiciosCambioEstado()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			HistoricoServiciosCambioEstadoQ query = new HistoricoServiciosCambioEstadoQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// PK. Autonumerica
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal Id
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// ID del Servicio respecto al SERVIDOR, FK de HistoricoServiciosEnCurso
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal ServicioCursoID
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.ServicioCursoID);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Terminal. Obtenido tabla GITS.TerminalesGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoServiciosCambioEstadoMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosCambioEstadoMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Identificador de la Trama, que envio este Registro.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String NumSec
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.NumSec);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoServiciosCambioEstadoMetadata.ColumnNames.NumSec, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.NumSec);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Vehiculo. Obtenido de GITS.Vehiculos
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoServiciosCambioEstadoMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosCambioEstadoMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Servicio. Obtenido de GITS.dbo.Viajes.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ServicioID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioID, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.ServicioID);
				}
			}
		}		
		
		/// <summary>
		/// Indica el estado del Servicio. Se realizan por pesos, siendo mayor miestras mas avanzado en su progreso este el Servicio.  99 - Especial. Indica que está pendiente de Anulacion.  1 - Asignado. Estado incial.  2 - Confirmado Terminal. (C para GITS)  3 - Confirmado Conductor (CC para GITS)  4 - En Curso. (E para GITS)  5 - Finalizado (R para GITS)
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte Estado
		{
			get
			{
				return base.GetSystemByteRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Estado);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Estado, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.Estado);
				}
			}
		}		
		
		/// <summary>
		/// Fecha en que se produjo el Cambio de Estado. Cuando se registra en el Historico, esta fecha no cambia.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime Fecha
		{
			get
			{
				return base.GetSystemDateTimeRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Fecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Fecha, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.Fecha);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Servidor, que proceso está operacion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte ServerID
		{
			get
			{
				return base.GetSystemByteRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		/// <summary>
		/// Se utiliza para marcar el registro con un información sobre su estado de Proceso. Sus valores seran definidos en codigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 FlagState
		{
			get
			{
				return base.GetSystemInt32Required(HistoricoServiciosCambioEstadoMetadata.ColumnNames.FlagState);
			}
			
			set
			{
				if(base.SetSystemInt32(HistoricoServiciosCambioEstadoMetadata.ColumnNames.FlagState, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.FlagState);
				}
			}
		}		
		
		/// <summary>
		/// Fecha en la entra en el Historico
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime FechaRegEntry
		{
			get
			{
				return base.GetSystemDateTimeRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.FechaRegEntry);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoServiciosCambioEstadoMetadata.ColumnNames.FechaRegEntry, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.FechaRegEntry);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoServiciosCambioEstado.GpsLatitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLatitud
		{
			get
			{
				return base.GetSystemDoubleRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud);
			}
			
			set
			{
				if(base.SetSystemDouble(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.GpsLatitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoServiciosCambioEstado.GpsLongitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLongitud
		{
			get
			{
				return base.GetSystemDoubleRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud);
			}
			
			set
			{
				if(base.SetSystemDouble(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.GpsLongitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoServiciosCambioEstado.Odometro
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double Odometro
		{
			get
			{
				return base.GetSystemDoubleRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Odometro);
			}
			
			set
			{
				if(base.SetSystemDouble(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Odometro, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.Odometro);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoServiciosCambioEstado.GrupoCursadoRuta
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GrupoCursadoRuta
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta, value))
				{
					OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.GrupoCursadoRuta);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "ServicioCursoID": this.str().ServicioCursoID = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "NumSec": this.str().NumSec = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "ServicioID": this.str().ServicioID = (string)value; break;							
						case "Estado": this.str().Estado = (string)value; break;							
						case "Fecha": this.str().Fecha = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;							
						case "FlagState": this.str().FlagState = (string)value; break;							
						case "FechaRegEntry": this.str().FechaRegEntry = (string)value; break;							
						case "GpsLatitud": this.str().GpsLatitud = (string)value; break;							
						case "GpsLongitud": this.str().GpsLongitud = (string)value; break;							
						case "Odometro": this.str().Odometro = (string)value; break;							
						case "GrupoCursadoRuta": this.str().GrupoCursadoRuta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.Id);
							break;
						
						case "ServicioCursoID":
						
							if (value == null || value is System.Decimal)
								this.ServicioCursoID = (System.Decimal)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.ServicioCursoID);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.VehiculoID);
							break;
						
						case "ServicioID":
						
							if (value == null || value is System.Decimal)
								this.ServicioID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.ServicioID);
							break;
						
						case "Estado":
						
							if (value == null || value is System.Byte)
								this.Estado = (System.Byte)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.Estado);
							break;
						
						case "Fecha":
						
							if (value == null || value is System.DateTime)
								this.Fecha = (System.DateTime)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.Fecha);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Byte)
								this.ServerID = (System.Byte)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.ServerID);
							break;
						
						case "FlagState":
						
							if (value == null || value is System.Int32)
								this.FlagState = (System.Int32)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.FlagState);
							break;
						
						case "FechaRegEntry":
						
							if (value == null || value is System.DateTime)
								this.FechaRegEntry = (System.DateTime)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.FechaRegEntry);
							break;
						
						case "GpsLatitud":
						
							if (value == null || value is System.Double)
								this.GpsLatitud = (System.Double)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.GpsLatitud);
							break;
						
						case "GpsLongitud":
						
							if (value == null || value is System.Double)
								this.GpsLongitud = (System.Double)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.GpsLongitud);
							break;
						
						case "Odometro":
						
							if (value == null || value is System.Double)
								this.Odometro = (System.Double)value;
								OnPropertyChanged(HistoricoServiciosCambioEstadoMetadata.PropertyNames.Odometro);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esHistoricoServiciosCambioEstado entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToDecimal(value);
				}
			}

			public System.String ServicioCursoID
			{
				get
				{
					return Convert.ToString(entity.ServicioCursoID);
				}

				set
				{
					entity.ServicioCursoID = Convert.ToDecimal(value);
				}
			}
	
			public System.String TerminalID
			{
				get
				{
					System.Decimal? data = entity.TerminalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalID = null;
					else entity.TerminalID = Convert.ToDecimal(value);
				}
			}
			
			public System.String NumSec
			{
				get
				{
					return Convert.ToString(entity.NumSec);
				}

				set
				{
					entity.NumSec = Convert.ToString(value);
				}
			}
	
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String ServicioID
			{
				get
				{
					System.Decimal? data = entity.ServicioID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioID = null;
					else entity.ServicioID = Convert.ToDecimal(value);
				}
			}
			
			public System.String Estado
			{
				get
				{
					return Convert.ToString(entity.Estado);
				}

				set
				{
					entity.Estado = Convert.ToByte(value);
				}
			}

			public System.String Fecha
			{
				get
				{
					return Convert.ToString(entity.Fecha);
				}

				set
				{
					entity.Fecha = Convert.ToDateTime(value);
				}
			}

			public System.String ServerID
			{
				get
				{
					return Convert.ToString(entity.ServerID);
				}

				set
				{
					entity.ServerID = Convert.ToByte(value);
				}
			}

			public System.String FlagState
			{
				get
				{
					return Convert.ToString(entity.FlagState);
				}

				set
				{
					entity.FlagState = Convert.ToInt32(value);
				}
			}

			public System.String FechaRegEntry
			{
				get
				{
					return Convert.ToString(entity.FechaRegEntry);
				}

				set
				{
					entity.FechaRegEntry = Convert.ToDateTime(value);
				}
			}

			public System.String GpsLatitud
			{
				get
				{
					return Convert.ToString(entity.GpsLatitud);
				}

				set
				{
					entity.GpsLatitud = Convert.ToDouble(value);
				}
			}

			public System.String GpsLongitud
			{
				get
				{
					return Convert.ToString(entity.GpsLongitud);
				}

				set
				{
					entity.GpsLongitud = Convert.ToDouble(value);
				}
			}

			public System.String Odometro
			{
				get
				{
					return Convert.ToString(entity.Odometro);
				}

				set
				{
					entity.Odometro = Convert.ToDouble(value);
				}
			}

			public System.String GrupoCursadoRuta
			{
				get
				{
					return Convert.ToString(entity.GrupoCursadoRuta);
				}

				set
				{
					entity.GrupoCursadoRuta = Convert.ToString(value);
				}
			}


			private esHistoricoServiciosCambioEstado entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return HistoricoServiciosCambioEstadoMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public HistoricoServiciosCambioEstadoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoServiciosCambioEstadoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoServiciosCambioEstadoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(HistoricoServiciosCambioEstadoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private HistoricoServiciosCambioEstadoQ query;		
	}



	[Serializable]
	abstract public partial class esHistoricoServiciosCambioEstadoCol : CollectionBase<HistoricoServiciosCambioEstado>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoServiciosCambioEstadoMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "HistoricoServiciosCambioEstadoCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public HistoricoServiciosCambioEstadoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoServiciosCambioEstadoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoServiciosCambioEstadoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoServiciosCambioEstadoQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(HistoricoServiciosCambioEstadoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((HistoricoServiciosCambioEstadoQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private HistoricoServiciosCambioEstadoQ query;
	}



	[Serializable]
	abstract public partial class esHistoricoServiciosCambioEstadoQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoServiciosCambioEstadoMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Id,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.TerminalID,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.NumSec,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.NumSec, esSystemType.String));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.VehiculoID,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioID,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioID, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Estado,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.Estado, esSystemType.Byte));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Fecha,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.Fecha, esSystemType.DateTime));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServerID,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServerID, esSystemType.Byte));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.FlagState,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.FlagState, esSystemType.Int32));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.FechaRegEntry,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.FechaRegEntry, esSystemType.DateTime));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud, esSystemType.Double));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud, esSystemType.Double));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Odometro,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.Odometro, esSystemType.Double));
			_queryItems.Add(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta,new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta, esSystemType.String));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServicioCursoID
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem NumSec
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.NumSec, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServicioID
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioID, esSystemType.Decimal); }
		} 
		
		public esQueryItem Estado
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.Estado, esSystemType.Byte); }
		} 
		
		public esQueryItem Fecha
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.Fecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServerID, esSystemType.Byte); }
		} 
		
		public esQueryItem FlagState
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.FlagState, esSystemType.Int32); }
		} 
		
		public esQueryItem FechaRegEntry
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.FechaRegEntry, esSystemType.DateTime); }
		} 
		
		public esQueryItem GpsLatitud
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsLongitud
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud, esSystemType.Double); }
		} 
		
		public esQueryItem Odometro
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.Odometro, esSystemType.Double); }
		} 
		
		public esQueryItem GrupoCursadoRuta
		{
			get { return new esQueryItem(this, HistoricoServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta, esSystemType.String); }
		} 
		
		#endregion
		
	}


	
	public partial class HistoricoServiciosCambioEstado : esHistoricoServiciosCambioEstado
	{

		
		
	}
	



	[Serializable]
	public partial class HistoricoServiciosCambioEstadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HistoricoServiciosCambioEstadoMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 18;
			c.Description = "PK. Autonumerica";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.ServicioCursoID;
			c.NumericPrecision = 18;
			c.Description = "ID del Servicio respecto al SERVIDOR, FK de HistoricoServiciosEnCurso";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.TerminalID, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Terminal. Obtenido tabla GITS.TerminalesGPS";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.NumSec, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.NumSec;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.Description = "Identificador de la Trama, que envio este Registro.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.VehiculoID, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Vehiculo. Obtenido de GITS.Vehiculos";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServicioID, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.ServicioID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Servicio. Obtenido de GITS.dbo.Viajes.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Estado, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.Estado;
			c.NumericPrecision = 3;
			c.Description = "Indica el estado del Servicio. Se realizan por pesos, siendo mayor miestras mas avanzado en su progreso este el Servicio.  99 - Especial. Indica que está pendiente de Anulacion.  1 - Asignado. Estado incial.  2 - Confirmado Terminal. (C para GITS)  3 - Confirmado Conductor (CC para GITS)  4 - En Curso. (E para GITS)  5 - Finalizado (R para GITS)";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Fecha, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.Fecha;
			c.Description = "Fecha en que se produjo el Cambio de Estado. Cuando se registra en el Historico, esta fecha no cambia.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.ServerID, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 3;
			c.Description = "Identificador del Servidor, que proceso está operacion";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.FlagState, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.FlagState;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Se utiliza para marcar el registro con un información sobre su estado de Proceso. Sus valores seran definidos en codigo";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.FechaRegEntry, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.FechaRegEntry;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.Description = "Fecha en la entra en el Historico";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud, 11, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.GpsLatitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud, 12, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.GpsLongitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.Odometro, 13, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.Odometro;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoServiciosCambioEstadoMetadata.PropertyNames.GrupoCursadoRuta;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public HistoricoServiciosCambioEstadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string ServicioCursoID = "ServicioCursoID";
			 public const string TerminalID = "TerminalID";
			 public const string NumSec = "NumSec";
			 public const string VehiculoID = "VehiculoID";
			 public const string ServicioID = "ServicioID";
			 public const string Estado = "Estado";
			 public const string Fecha = "Fecha";
			 public const string ServerID = "ServerID";
			 public const string FlagState = "FlagState";
			 public const string FechaRegEntry = "FechaRegEntry";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string Odometro = "Odometro";
			 public const string GrupoCursadoRuta = "GrupoCursadoRuta";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string ServicioCursoID = "ServicioCursoID";
			 public const string TerminalID = "TerminalID";
			 public const string NumSec = "NumSec";
			 public const string VehiculoID = "VehiculoID";
			 public const string ServicioID = "ServicioID";
			 public const string Estado = "Estado";
			 public const string Fecha = "Fecha";
			 public const string ServerID = "ServerID";
			 public const string FlagState = "FlagState";
			 public const string FechaRegEntry = "FechaRegEntry";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string Odometro = "Odometro";
			 public const string GrupoCursadoRuta = "GrupoCursadoRuta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HistoricoServiciosCambioEstadoMetadata))
			{
				if(HistoricoServiciosCambioEstadoMetadata.mapDelegates == null)
				{
					HistoricoServiciosCambioEstadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HistoricoServiciosCambioEstadoMetadata.meta == null)
				{
					HistoricoServiciosCambioEstadoMetadata.meta = new HistoricoServiciosCambioEstadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServicioCursoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("NumSec", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServicioID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Estado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Fecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ServerID", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("FlagState", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FechaRegEntry", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GpsLatitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsLongitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("Odometro", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GrupoCursadoRuta", new esTypeMap("varchar", "System.String"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "HistoricoServiciosCambioEstado";
				meta.Destination = "HistoricoServiciosCambioEstado";
				
				meta.spInsert = "proc_HistoricoServiciosCambioEstadoInsert";				
				meta.spUpdate = "proc_HistoricoServiciosCambioEstadoUpdate";		
				meta.spDelete = "proc_HistoricoServiciosCambioEstadoDelete";
				meta.spLoadAll = "proc_HistoricoServiciosCambioEstadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_HistoricoServiciosCambioEstadoLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HistoricoServiciosCambioEstadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
