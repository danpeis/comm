
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Encapsulates the 'TemplateConfiguration' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TemplateConfiguration")]
	public partial class TemplateConfiguration : esTemplateConfiguration
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TemplateConfiguration();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Int32 id)
		{
			var obj = new TemplateConfiguration();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Int32 id, esSqlAccessType sqlAccessType)
		{
			var obj = new TemplateConfiguration();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Int32 ID)
        {
            try
            {
                var e = new TemplateConfiguration();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TemplateConfiguration Get(System.Int32 ID)
        {
            try
            {
                var e = new TemplateConfiguration();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TemplateConfiguration(System.Int32 ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public TemplateConfiguration()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TemplateConfigurationCol")]
	public partial class TemplateConfigurationCol : esTemplateConfigurationCol, IEnumerable<TemplateConfiguration>
	{
	
		public TemplateConfigurationCol()
		{
		}

	
	
		public TemplateConfiguration FindByPrimaryKey(System.Int32 id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TemplateConfiguration))]
		public class TemplateConfigurationColWCFPacket : esCollectionWCFPacket<TemplateConfigurationCol>
		{
			public static implicit operator TemplateConfigurationCol(TemplateConfigurationColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TemplateConfigurationColWCFPacket(TemplateConfigurationCol collection)
			{
				return new TemplateConfigurationColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TemplateConfigurationQ : esTemplateConfigurationQ
	{
		public TemplateConfigurationQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TemplateConfigurationQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TemplateConfigurationQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new TemplateConfigurationQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TemplateConfigurationQ query)
		{
			return TemplateConfigurationQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TemplateConfigurationQ(string query)
		{
			return (TemplateConfigurationQ)TemplateConfigurationQ.SerializeHelper.FromXml(query, typeof(TemplateConfigurationQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTemplateConfiguration : EntityBase
	{
		public esTemplateConfiguration()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 id)
		{
			TemplateConfigurationQ query = new TemplateConfigurationQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to TemplateConfiguration.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 Id
		{
			get
			{
				return base.GetSystemInt32Required(TemplateConfigurationMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemInt32(TemplateConfigurationMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TemplateConfiguration.Codigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 Codigo
		{
			get
			{
				return base.GetSystemInt32Required(TemplateConfigurationMetadata.ColumnNames.Codigo);
			}
			
			set
			{
				if(base.SetSystemInt32(TemplateConfigurationMetadata.ColumnNames.Codigo, value))
				{
					OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.Codigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TemplateConfiguration.Nombre
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Nombre
		{
			get
			{
				return base.GetSystemStringRequired(TemplateConfigurationMetadata.ColumnNames.Nombre);
			}
			
			set
			{
				if(base.SetSystemString(TemplateConfigurationMetadata.ColumnNames.Nombre, value))
				{
					OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.Nombre);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TemplateConfiguration.ConfigData
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ConfigData
		{
			get
			{
				return base.GetSystemStringRequired(TemplateConfigurationMetadata.ColumnNames.ConfigData);
			}
			
			set
			{
				if(base.SetSystemString(TemplateConfigurationMetadata.ColumnNames.ConfigData, value))
				{
					OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.ConfigData);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TemplateConfiguration.Comentario
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Comentario
		{
			get
			{
				return base.GetSystemStringRequired(TemplateConfigurationMetadata.ColumnNames.Comentario);
			}
			
			set
			{
				if(base.SetSystemString(TemplateConfigurationMetadata.ColumnNames.Comentario, value))
				{
					OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.Comentario);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TemplateConfiguration.SoftwareTipo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte SoftwareTipo
		{
			get
			{
				return base.GetSystemByteRequired(TemplateConfigurationMetadata.ColumnNames.SoftwareTipo);
			}
			
			set
			{
				if(base.SetSystemByte(TemplateConfigurationMetadata.ColumnNames.SoftwareTipo, value))
				{
					OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.SoftwareTipo);
				}
			}
		}		
		
		/// <summary>
		/// Indica que es la plantilla predeterminada
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean Predeterminada
		{
			get
			{
				return base.GetSystemBooleanRequired(TemplateConfigurationMetadata.ColumnNames.Predeterminada);
			}
			
			set
			{
				if(base.SetSystemBoolean(TemplateConfigurationMetadata.ColumnNames.Predeterminada, value))
				{
					OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.Predeterminada);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "Codigo": this.str().Codigo = (string)value; break;							
						case "Nombre": this.str().Nombre = (string)value; break;							
						case "ConfigData": this.str().ConfigData = (string)value; break;							
						case "Comentario": this.str().Comentario = (string)value; break;							
						case "SoftwareTipo": this.str().SoftwareTipo = (string)value; break;							
						case "Predeterminada": this.str().Predeterminada = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Int32)
								this.Id = (System.Int32)value;
								OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.Id);
							break;
						
						case "Codigo":
						
							if (value == null || value is System.Int32)
								this.Codigo = (System.Int32)value;
								OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.Codigo);
							break;
						
						case "SoftwareTipo":
						
							if (value == null || value is System.Byte)
								this.SoftwareTipo = (System.Byte)value;
								OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.SoftwareTipo);
							break;
						
						case "Predeterminada":
						
							if (value == null || value is System.Boolean)
								this.Predeterminada = (System.Boolean)value;
								OnPropertyChanged(TemplateConfigurationMetadata.PropertyNames.Predeterminada);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTemplateConfiguration entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToInt32(value);
				}
			}

			public System.String Codigo
			{
				get
				{
					return Convert.ToString(entity.Codigo);
				}

				set
				{
					entity.Codigo = Convert.ToInt32(value);
				}
			}

			public System.String Nombre
			{
				get
				{
					return Convert.ToString(entity.Nombre);
				}

				set
				{
					entity.Nombre = Convert.ToString(value);
				}
			}

			public System.String ConfigData
			{
				get
				{
					return Convert.ToString(entity.ConfigData);
				}

				set
				{
					entity.ConfigData = Convert.ToString(value);
				}
			}

			public System.String Comentario
			{
				get
				{
					return Convert.ToString(entity.Comentario);
				}

				set
				{
					entity.Comentario = Convert.ToString(value);
				}
			}

			public System.String SoftwareTipo
			{
				get
				{
					return Convert.ToString(entity.SoftwareTipo);
				}

				set
				{
					entity.SoftwareTipo = Convert.ToByte(value);
				}
			}

			public System.String Predeterminada
			{
				get
				{
					return Convert.ToString(entity.Predeterminada);
				}

				set
				{
					entity.Predeterminada = Convert.ToBoolean(value);
				}
			}


			private esTemplateConfiguration entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TemplateConfigurationMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TemplateConfigurationQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TemplateConfigurationQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TemplateConfigurationQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TemplateConfigurationQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TemplateConfigurationQ query;		
	}



	[Serializable]
	abstract public partial class esTemplateConfigurationCol : CollectionBase<TemplateConfiguration>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TemplateConfigurationMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TemplateConfigurationCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TemplateConfigurationQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TemplateConfigurationQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TemplateConfigurationQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TemplateConfigurationQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TemplateConfigurationQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TemplateConfigurationQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TemplateConfigurationQ query;
	}



	[Serializable]
	abstract public partial class esTemplateConfigurationQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TemplateConfigurationMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TemplateConfigurationMetadata.ColumnNames.Id,new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Id, esSystemType.Int32));
			_queryItems.Add(TemplateConfigurationMetadata.ColumnNames.Codigo,new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Codigo, esSystemType.Int32));
			_queryItems.Add(TemplateConfigurationMetadata.ColumnNames.Nombre,new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Nombre, esSystemType.String));
			_queryItems.Add(TemplateConfigurationMetadata.ColumnNames.ConfigData,new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.ConfigData, esSystemType.String));
			_queryItems.Add(TemplateConfigurationMetadata.ColumnNames.Comentario,new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Comentario, esSystemType.String));
			_queryItems.Add(TemplateConfigurationMetadata.ColumnNames.SoftwareTipo,new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.SoftwareTipo, esSystemType.Byte));
			_queryItems.Add(TemplateConfigurationMetadata.ColumnNames.Predeterminada,new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Predeterminada, esSystemType.Boolean));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Id, esSystemType.Int32); }
		} 
		
		public esQueryItem Codigo
		{
			get { return new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Codigo, esSystemType.Int32); }
		} 
		
		public esQueryItem Nombre
		{
			get { return new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Nombre, esSystemType.String); }
		} 
		
		public esQueryItem ConfigData
		{
			get { return new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.ConfigData, esSystemType.String); }
		} 
		
		public esQueryItem Comentario
		{
			get { return new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Comentario, esSystemType.String); }
		} 
		
		public esQueryItem SoftwareTipo
		{
			get { return new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.SoftwareTipo, esSystemType.Byte); }
		} 
		
		public esQueryItem Predeterminada
		{
			get { return new esQueryItem(this, TemplateConfigurationMetadata.ColumnNames.Predeterminada, esSystemType.Boolean); }
		} 
		
		#endregion
		
	}


	
	public partial class TemplateConfiguration : esTemplateConfiguration
	{

		#region TerminalStateByTemplateConfigID - Zero To Many
		
		static public esPrefetchMap Prefetch_TerminalStateByTemplateConfigID
		{
			get
			{
				esPrefetchMap map = new esPrefetchMap();
				map.PrefetchDelegate = gcperu.Server.Core.DAL.GC.TemplateConfiguration.TerminalStateByTemplateConfigID_Delegate;
				map.PropertyName = "TerminalStateByTemplateConfigID";
				map.MyColumnName = "TemplateConfigID";
				map.ParentColumnName = "ID";
				map.IsMultiPartKey = false;
				return map;
			}
		}		
		
		static private void TerminalStateByTemplateConfigID_Delegate(esPrefetchParameters data)
		{
			TemplateConfigurationQ parent = new TemplateConfigurationQ(data.NextAlias());

			TerminalStateQ me = data.You != null ? data.You as TerminalStateQ : new TerminalStateQ(data.NextAlias());

			if (data.Root == null)
			{
				data.Root = me;
			}
			
			data.Root.InnerJoin(parent).On(parent.Id == me.TemplateConfigID);

			data.You = parent;
		}			
		
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_TerminalState_TemplateConfiguration
		/// </summary>

		[XmlIgnore]
		public TerminalStateCol TerminalStateByTemplateConfigID
		{
			get
			{
				if(this._TerminalStateByTemplateConfigID == null)
				{
					this._TerminalStateByTemplateConfigID = new TerminalStateCol();
					this._TerminalStateByTemplateConfigID.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TerminalStateByTemplateConfigID", this._TerminalStateByTemplateConfigID);
				
					if (this.Id != null)
					{
						if (!this.es.IsLazyLoadDisabled)
						{
							this._TerminalStateByTemplateConfigID.Query.Where(this._TerminalStateByTemplateConfigID.Query.TemplateConfigID == this.Id);
							this._TerminalStateByTemplateConfigID.Query.Load();
						}

						// Auto-hookup Foreign Keys
						this._TerminalStateByTemplateConfigID.fks.Add(TerminalStateMetadata.ColumnNames.TemplateConfigID, this.Id);
					}
				}

				return this._TerminalStateByTemplateConfigID;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TerminalStateByTemplateConfigID != null) 
				{ 
					this.RemovePostSave("TerminalStateByTemplateConfigID"); 
					this._TerminalStateByTemplateConfigID = null;
					this.OnPropertyChanged("TerminalStateByTemplateConfigID");
				} 
			} 			
		}
			
		
		private TerminalStateCol _TerminalStateByTemplateConfigID;
		#endregion

		
		protected override esEntityCollectionBase CreateCollectionForPrefetch(string name)
		{
			esEntityCollectionBase coll = null;

			switch (name)
			{
				case "TerminalStateByTemplateConfigID":
					coll = this.TerminalStateByTemplateConfigID;
					break;	
			}

			return coll;
		}		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "TerminalStateByTemplateConfigID", typeof(TerminalStateCol), new TerminalState()));
		
			return props;
		}
		
	}
	



	[Serializable]
	public partial class TemplateConfigurationMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TemplateConfigurationMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TemplateConfigurationMetadata.ColumnNames.Id, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TemplateConfigurationMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 10;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TemplateConfigurationMetadata.ColumnNames.Codigo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TemplateConfigurationMetadata.PropertyNames.Codigo;
			c.NumericPrecision = 10;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TemplateConfigurationMetadata.ColumnNames.Nombre, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TemplateConfigurationMetadata.PropertyNames.Nombre;
			c.CharacterMaxLength = 100;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TemplateConfigurationMetadata.ColumnNames.ConfigData, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TemplateConfigurationMetadata.PropertyNames.ConfigData;
			c.CharacterMaxLength = 5000;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TemplateConfigurationMetadata.ColumnNames.Comentario, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TemplateConfigurationMetadata.PropertyNames.Comentario;
			c.CharacterMaxLength = 500;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TemplateConfigurationMetadata.ColumnNames.SoftwareTipo, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TemplateConfigurationMetadata.PropertyNames.SoftwareTipo;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TemplateConfigurationMetadata.ColumnNames.Predeterminada, 6, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TemplateConfigurationMetadata.PropertyNames.Predeterminada;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Indica que es la plantilla predeterminada";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TemplateConfigurationMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string Codigo = "Codigo";
			 public const string Nombre = "Nombre";
			 public const string ConfigData = "ConfigData";
			 public const string Comentario = "Comentario";
			 public const string SoftwareTipo = "SoftwareTipo";
			 public const string Predeterminada = "Predeterminada";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Codigo = "Codigo";
			 public const string Nombre = "Nombre";
			 public const string ConfigData = "ConfigData";
			 public const string Comentario = "Comentario";
			 public const string SoftwareTipo = "SoftwareTipo";
			 public const string Predeterminada = "Predeterminada";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TemplateConfigurationMetadata))
			{
				if(TemplateConfigurationMetadata.mapDelegates == null)
				{
					TemplateConfigurationMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TemplateConfigurationMetadata.meta == null)
				{
					TemplateConfigurationMetadata.meta = new TemplateConfigurationMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Codigo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nombre", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ConfigData", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Comentario", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("SoftwareTipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Predeterminada", new esTypeMap("bit", "System.Boolean"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "TemplateConfiguration";
				meta.Destination = "TemplateConfiguration";
				
				meta.spInsert = "proc_TemplateConfigurationInsert";				
				meta.spUpdate = "proc_TemplateConfigurationUpdate";		
				meta.spDelete = "proc_TemplateConfigurationDelete";
				meta.spLoadAll = "proc_TemplateConfigurationLoadAll";
				meta.spLoadByPrimaryKey = "proc_TemplateConfigurationLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TemplateConfigurationMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
