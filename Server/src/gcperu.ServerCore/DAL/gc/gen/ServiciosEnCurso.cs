
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Mantiene los Servicios que estan actualmente activos. Son creados por orden del GITS, al realizar una peticion de Envio del Servicio al Terminal. El Servidor modificará los estados segun los reciba, y tambien modificará los estados en el Servicio en el GITS. El GITS puede tambien enviar una solucitud de cambio de estado forzado por el controlador. Un servicio es borrado, cuando se recibe el estado de Finalizado, o bien el GITS mande una peticion de anulación. El PK lo componen ServicioID y TerminalID, porque puede haber un mismo servicio en 2 estados distintos. Ej. Uno pendiente de anulacion y otro que se ha reasignado a otro Vehiculo/Terminal. Si se anulara un Servivio que aun no se ha enviado, simplemente se borra de la Tabla.
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("ServiciosEnCurso")]
	public partial class ServiciosEnCurso : esServiciosEnCurso
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new ServiciosEnCurso();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new ServiciosEnCurso();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new ServiciosEnCurso();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new ServiciosEnCurso();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static ServiciosEnCurso Get(System.Decimal ID)
        {
            try
            {
                var e = new ServiciosEnCurso();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public ServiciosEnCurso(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public ServiciosEnCurso()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("ServiciosEnCursoCol")]
	public partial class ServiciosEnCursoCol : esServiciosEnCursoCol, IEnumerable<ServiciosEnCurso>
	{
	
		public ServiciosEnCursoCol()
		{
		}

	
	
		public ServiciosEnCurso FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(ServiciosEnCurso))]
		public class ServiciosEnCursoColWCFPacket : esCollectionWCFPacket<ServiciosEnCursoCol>
		{
			public static implicit operator ServiciosEnCursoCol(ServiciosEnCursoColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator ServiciosEnCursoColWCFPacket(ServiciosEnCursoCol collection)
			{
				return new ServiciosEnCursoColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class ServiciosEnCursoQ : esServiciosEnCursoQ
	{
		public ServiciosEnCursoQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public ServiciosEnCursoQ()
		{
		}

		override protected string GetQueryName()
		{
			return "ServiciosEnCursoQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new ServiciosEnCursoQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(ServiciosEnCursoQ query)
		{
			return ServiciosEnCursoQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator ServiciosEnCursoQ(string query)
		{
			return (ServiciosEnCursoQ)ServiciosEnCursoQ.SerializeHelper.FromXml(query, typeof(ServiciosEnCursoQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esServiciosEnCurso : EntityBase
	{
		public esServiciosEnCurso()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			ServiciosEnCursoQ query = new ServiciosEnCursoQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Identificador del Servicio en GITS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ServicioID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.ServicioID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.ServicioID, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.ServicioID);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Terminal, para el cual va destinado este Servicio, vinculado al Vehiculo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Identificador de la Trama, que creo este Servicio.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String NumSec
		{
			get
			{
				return base.GetSystemStringRequired(ServiciosEnCursoMetadata.ColumnNames.NumSec);
			}
			
			set
			{
				if(base.SetSystemString(ServiciosEnCursoMetadata.ColumnNames.NumSec, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.NumSec);
				}
			}
		}		
		
		/// <summary>
		/// Indica el estado del Servicio. Se realizan por pesos, siendo mayor miestras mas avanzado en su progreso este el Servicio.  99 - Especial. Indica que está pendiente de Anulacion.  1 - Asignado. Estado incial.  2 - Confirmado Terminal. (C para GITS)  3 - Confirmado Conductor (CC para GITS)  4 - En Curso. (E para GITS)  5 - Finalizado (R para GITS)
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte Estado
		{
			get
			{
				return base.GetSystemByteRequired(ServiciosEnCursoMetadata.ColumnNames.Estado);
			}
			
			set
			{
				if(base.SetSystemByte(ServiciosEnCursoMetadata.ColumnNames.Estado, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.Estado);
				}
			}
		}		
		
		/// <summary>
		/// Fecha servicio entra en esta tabla, enviada por el GITS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaAlta
		{
			get
			{
				return base.GetSystemDateTime(ServiciosEnCursoMetadata.ColumnNames.FechaAlta);
			}
			
			set
			{
				if(base.SetSystemDateTime(ServiciosEnCursoMetadata.ColumnNames.FechaAlta, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.FechaAlta);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Vehiculo, asociado al Servicio
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Personal vinculado al Servicio (NO CONDUCTOR LOGADO)
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PersonalID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.PersonalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.PersonalID, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.PersonalID);
				}
			}
		}		
		
		/// <summary>
		/// Si tiene valor incida, el ID del Servidor que está gestionando este Servicio, si está null, el Servicio aun no se ha enviado, esta en la tabla [TerminalDataSend]
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? ServerID
		{
			get
			{
				return base.GetSystemByte(ServiciosEnCursoMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemByte(ServiciosEnCursoMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		/// <summary>
		/// ID del Servicio respecto al SERVIDOR, mientras que ServicioID es respecto a GITS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal Id
		{
			get
			{
				return base.GetSystemDecimalRequired(ServiciosEnCursoMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Indica si este servicio es controlado por los Servicios de Integracion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? DeIntegracion
		{
			get
			{
				return base.GetSystemBoolean(ServiciosEnCursoMetadata.ColumnNames.DeIntegracion);
			}
			
			set
			{
				if(base.SetSystemBoolean(ServiciosEnCursoMetadata.ColumnNames.DeIntegracion, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.DeIntegracion);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Servicio Termporal. Este se utiliza para los Servicios Especiales que crean los terminales. Cuando un Terminal crea un SE, lo envia y si por cualquier motivo llega antes el cambio de estado que la inserción del Servicio, se crea un Registro de Cambio de Estado con un identificador temporal del Servicio (negativo) y se activa con la marca de no procesado.  Cuando se vuelva a intentar procesar, se buscará en esta tabla un Cambio de Estado (0) que contenga este Identificador Temporal y que en ServicioID tiene que contener el Identifcador del Servicio en GITS.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ServicioTempID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.ServicioTempID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosEnCursoMetadata.ColumnNames.ServicioTempID, value))
				{
					OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.ServicioTempID);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "ServicioID": this.str().ServicioID = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "NumSec": this.str().NumSec = (string)value; break;							
						case "Estado": this.str().Estado = (string)value; break;							
						case "FechaAlta": this.str().FechaAlta = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "PersonalID": this.str().PersonalID = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;							
						case "Id": this.str().Id = (string)value; break;							
						case "DeIntegracion": this.str().DeIntegracion = (string)value; break;							
						case "ServicioTempID": this.str().ServicioTempID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "ServicioID":
						
							if (value == null || value is System.Decimal)
								this.ServicioID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.ServicioID);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.TerminalID);
							break;
						
						case "Estado":
						
							if (value == null || value is System.Byte)
								this.Estado = (System.Byte)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.Estado);
							break;
						
						case "FechaAlta":
						
							if (value == null || value is System.DateTime)
								this.FechaAlta = (System.DateTime?)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.FechaAlta);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.VehiculoID);
							break;
						
						case "PersonalID":
						
							if (value == null || value is System.Decimal)
								this.PersonalID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.PersonalID);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Byte)
								this.ServerID = (System.Byte?)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.ServerID);
							break;
						
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.Id);
							break;
						
						case "DeIntegracion":
						
							if (value == null || value is System.Boolean)
								this.DeIntegracion = (System.Boolean?)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.DeIntegracion);
							break;
						
						case "ServicioTempID":
						
							if (value == null || value is System.Decimal)
								this.ServicioTempID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosEnCursoMetadata.PropertyNames.ServicioTempID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esServiciosEnCurso entity)
			{
				this.entity = entity;
			}
			
	
			public System.String ServicioID
			{
				get
				{
					System.Decimal? data = entity.ServicioID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioID = null;
					else entity.ServicioID = Convert.ToDecimal(value);
				}
			}
				
			public System.String TerminalID
			{
				get
				{
					System.Decimal? data = entity.TerminalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalID = null;
					else entity.TerminalID = Convert.ToDecimal(value);
				}
			}
			
			public System.String NumSec
			{
				get
				{
					return Convert.ToString(entity.NumSec);
				}

				set
				{
					entity.NumSec = Convert.ToString(value);
				}
			}

			public System.String Estado
			{
				get
				{
					return Convert.ToString(entity.Estado);
				}

				set
				{
					entity.Estado = Convert.ToByte(value);
				}
			}
	
			public System.String FechaAlta
			{
				get
				{
					System.DateTime? data = entity.FechaAlta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaAlta = null;
					else entity.FechaAlta = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String PersonalID
			{
				get
				{
					System.Decimal? data = entity.PersonalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalID = null;
					else entity.PersonalID = Convert.ToDecimal(value);
				}
			}
				
			public System.String ServerID
			{
				get
				{
					System.Byte? data = entity.ServerID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServerID = null;
					else entity.ServerID = Convert.ToByte(value);
				}
			}
			
			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToDecimal(value);
				}
			}
	
			public System.String DeIntegracion
			{
				get
				{
					System.Boolean? data = entity.DeIntegracion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DeIntegracion = null;
					else entity.DeIntegracion = Convert.ToBoolean(value);
				}
			}
				
			public System.String ServicioTempID
			{
				get
				{
					System.Decimal? data = entity.ServicioTempID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioTempID = null;
					else entity.ServicioTempID = Convert.ToDecimal(value);
				}
			}
			

			private esServiciosEnCurso entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return ServiciosEnCursoMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public ServiciosEnCursoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ServiciosEnCursoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ServiciosEnCursoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(ServiciosEnCursoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private ServiciosEnCursoQ query;		
	}



	[Serializable]
	abstract public partial class esServiciosEnCursoCol : CollectionBase<ServiciosEnCurso>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ServiciosEnCursoMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "ServiciosEnCursoCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public ServiciosEnCursoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ServiciosEnCursoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ServiciosEnCursoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ServiciosEnCursoQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(ServiciosEnCursoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((ServiciosEnCursoQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private ServiciosEnCursoQ query;
	}



	[Serializable]
	abstract public partial class esServiciosEnCursoQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return ServiciosEnCursoMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.ServicioID,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.ServicioID, esSystemType.Decimal));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.TerminalID,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.NumSec,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.NumSec, esSystemType.String));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.Estado,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.Estado, esSystemType.Byte));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.FechaAlta,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.FechaAlta, esSystemType.DateTime));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.VehiculoID,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.PersonalID,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.PersonalID, esSystemType.Decimal));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.ServerID,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.ServerID, esSystemType.Byte));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.Id,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.DeIntegracion,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.DeIntegracion, esSystemType.Boolean));
			_queryItems.Add(ServiciosEnCursoMetadata.ColumnNames.ServicioTempID,new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.ServicioTempID, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem ServicioID
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.ServicioID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem NumSec
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.NumSec, esSystemType.String); }
		} 
		
		public esQueryItem Estado
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.Estado, esSystemType.Byte); }
		} 
		
		public esQueryItem FechaAlta
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.FechaAlta, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem PersonalID
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.PersonalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.ServerID, esSystemType.Byte); }
		} 
		
		public esQueryItem Id
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem DeIntegracion
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.DeIntegracion, esSystemType.Boolean); }
		} 
		
		public esQueryItem ServicioTempID
		{
			get { return new esQueryItem(this, ServiciosEnCursoMetadata.ColumnNames.ServicioTempID, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class ServiciosEnCurso : esServiciosEnCurso
	{

		#region ServiciosCambioEstadoByServicioCursoID - Zero To Many
		
		static public esPrefetchMap Prefetch_ServiciosCambioEstadoByServicioCursoID
		{
			get
			{
				esPrefetchMap map = new esPrefetchMap();
				map.PrefetchDelegate = gcperu.Server.Core.DAL.GC.ServiciosEnCurso.ServiciosCambioEstadoByServicioCursoID_Delegate;
				map.PropertyName = "ServiciosCambioEstadoByServicioCursoID";
				map.MyColumnName = "ServicioCursoID";
				map.ParentColumnName = "ID";
				map.IsMultiPartKey = false;
				return map;
			}
		}		
		
		static private void ServiciosCambioEstadoByServicioCursoID_Delegate(esPrefetchParameters data)
		{
			ServiciosEnCursoQ parent = new ServiciosEnCursoQ(data.NextAlias());

			ServiciosCambioEstadoQ me = data.You != null ? data.You as ServiciosCambioEstadoQ : new ServiciosCambioEstadoQ(data.NextAlias());

			if (data.Root == null)
			{
				data.Root = me;
			}
			
			data.Root.InnerJoin(parent).On(parent.Id == me.ServicioCursoID);

			data.You = parent;
		}			
		
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_ServiciosCambioEstado_ServiciosEnCurso
		/// </summary>

		[XmlIgnore]
		public ServiciosCambioEstadoCol ServiciosCambioEstadoByServicioCursoID
		{
			get
			{
				if(this._ServiciosCambioEstadoByServicioCursoID == null)
				{
					this._ServiciosCambioEstadoByServicioCursoID = new ServiciosCambioEstadoCol();
					this._ServiciosCambioEstadoByServicioCursoID.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ServiciosCambioEstadoByServicioCursoID", this._ServiciosCambioEstadoByServicioCursoID);
				
					if (this.Id != null)
					{
						if (!this.es.IsLazyLoadDisabled)
						{
							this._ServiciosCambioEstadoByServicioCursoID.Query.Where(this._ServiciosCambioEstadoByServicioCursoID.Query.ServicioCursoID == this.Id);
							this._ServiciosCambioEstadoByServicioCursoID.Query.Load();
						}

						// Auto-hookup Foreign Keys
						this._ServiciosCambioEstadoByServicioCursoID.fks.Add(ServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID, this.Id);
					}
				}

				return this._ServiciosCambioEstadoByServicioCursoID;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ServiciosCambioEstadoByServicioCursoID != null) 
				{ 
					this.RemovePostSave("ServiciosCambioEstadoByServicioCursoID"); 
					this._ServiciosCambioEstadoByServicioCursoID = null;
					this.OnPropertyChanged("ServiciosCambioEstadoByServicioCursoID");
				} 
			} 			
		}
			
		
		private ServiciosCambioEstadoCol _ServiciosCambioEstadoByServicioCursoID;
		#endregion

		
		protected override esEntityCollectionBase CreateCollectionForPrefetch(string name)
		{
			esEntityCollectionBase coll = null;

			switch (name)
			{
				case "ServiciosCambioEstadoByServicioCursoID":
					coll = this.ServiciosCambioEstadoByServicioCursoID;
					break;	
			}

			return coll;
		}		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ServiciosCambioEstadoByServicioCursoID", typeof(ServiciosCambioEstadoCol), new ServiciosCambioEstado()));
		
			return props;
		}
		
		/// <summary>
		/// Called by ApplyPostSaveKeys 
		/// </summary>
		/// <param name="coll">The collection to enumerate over</param>
		/// <param name="key">"The column name</param>
		/// <param name="value">The column value</param>
		private void Apply(esEntityCollectionBase coll, string key, object value)
		{
			foreach (esEntity obj in coll)
			{
				if (obj.es.IsAdded)
				{
					obj.SetColumn(key, value, false);
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._ServiciosCambioEstadoByServicioCursoID != null)
			{
				Apply(this._ServiciosCambioEstadoByServicioCursoID, "ServicioCursoID", this.Id);
			}
		}
		
	}
	



	[Serializable]
	public partial class ServiciosEnCursoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ServiciosEnCursoMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.ServicioID, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.ServicioID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Servicio en GITS";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.TerminalID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Terminal, para el cual va destinado este Servicio, vinculado al Vehiculo";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.NumSec, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.NumSec;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.Description = "Identificador de la Trama, que creo este Servicio.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.Estado, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.Estado;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Indica el estado del Servicio. Se realizan por pesos, siendo mayor miestras mas avanzado en su progreso este el Servicio.  99 - Especial. Indica que está pendiente de Anulacion.  1 - Asignado. Estado incial.  2 - Confirmado Terminal. (C para GITS)  3 - Confirmado Conductor (CC para GITS)  4 - En Curso. (E para GITS)  5 - Finalizado (R para GITS)";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.FechaAlta, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.FechaAlta;
			c.Description = "Fecha servicio entra en esta tabla, enviada por el GITS";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.VehiculoID, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Vehiculo, asociado al Servicio";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.PersonalID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.PersonalID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Personal vinculado al Servicio (NO CONDUCTOR LOGADO)";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.ServerID, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 3;
			c.Description = "Si tiene valor incida, el ID del Servidor que está gestionando este Servicio, si está null, el Servicio aun no se ha enviado, esta en la tabla [TerminalDataSend]";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.Id, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 18;
			c.Description = "ID del Servicio respecto al SERVIDOR, mientras que ServicioID es respecto a GITS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.DeIntegracion, 9, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.DeIntegracion;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Indica si este servicio es controlado por los Servicios de Integracion";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosEnCursoMetadata.ColumnNames.ServicioTempID, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosEnCursoMetadata.PropertyNames.ServicioTempID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Servicio Termporal. Este se utiliza para los Servicios Especiales que crean los terminales. Cuando un Terminal crea un SE, lo envia y si por cualquier motivo llega antes el cambio de estado que la inserción del Servicio, se crea un Registro de Cambio de Estado con un identificador temporal del Servicio (negativo) y se activa con la marca de no procesado.  Cuando se vuelva a intentar procesar, se buscará en esta tabla un Cambio de Estado (0) que contenga este Identificador Temporal y que en ServicioID tiene que contener el Identifcador del Servicio en GITS.";
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public ServiciosEnCursoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string ServicioID = "ServicioID";
			 public const string TerminalID = "TerminalID";
			 public const string NumSec = "NumSec";
			 public const string Estado = "Estado";
			 public const string FechaAlta = "FechaAlta";
			 public const string VehiculoID = "VehiculoID";
			 public const string PersonalID = "PersonalID";
			 public const string ServerID = "ServerID";
			 public const string Id = "ID";
			 public const string DeIntegracion = "DeIntegracion";
			 public const string ServicioTempID = "ServicioTempID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string ServicioID = "ServicioID";
			 public const string TerminalID = "TerminalID";
			 public const string NumSec = "NumSec";
			 public const string Estado = "Estado";
			 public const string FechaAlta = "FechaAlta";
			 public const string VehiculoID = "VehiculoID";
			 public const string PersonalID = "PersonalID";
			 public const string ServerID = "ServerID";
			 public const string Id = "Id";
			 public const string DeIntegracion = "DeIntegracion";
			 public const string ServicioTempID = "ServicioTempID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ServiciosEnCursoMetadata))
			{
				if(ServiciosEnCursoMetadata.mapDelegates == null)
				{
					ServiciosEnCursoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ServiciosEnCursoMetadata.meta == null)
				{
					ServiciosEnCursoMetadata.meta = new ServiciosEnCursoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("ServicioID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("NumSec", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Estado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("FechaAlta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PersonalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServerID", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("DeIntegracion", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("ServicioTempID", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "ServiciosEnCurso";
				meta.Destination = "ServiciosEnCurso";
				
				meta.spInsert = "proc_ServiciosEnCursoInsert";				
				meta.spUpdate = "proc_ServiciosEnCursoUpdate";		
				meta.spDelete = "proc_ServiciosEnCursoDelete";
				meta.spLoadAll = "proc_ServiciosEnCursoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ServiciosEnCursoLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ServiciosEnCursoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
