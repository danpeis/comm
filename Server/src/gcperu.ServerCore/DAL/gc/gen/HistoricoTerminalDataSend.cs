
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// ALMACENA LAS PETICIONES O MENSAJES QUE HAY QUE ENVIAR A LOS TERMINALES. LA PRIORIDAD ES UN DATO, QUE SE UTILZA PARA ORDENARACION DE LOS ENVIOS. Orden: Prioridad DESC, y por Fecha ASC
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("HistoricoTerminalDataSend")]
	public partial class HistoricoTerminalDataSend : esHistoricoTerminalDataSend
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new HistoricoTerminalDataSend();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new HistoricoTerminalDataSend();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new HistoricoTerminalDataSend();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new HistoricoTerminalDataSend();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static HistoricoTerminalDataSend Get(System.Decimal ID)
        {
            try
            {
                var e = new HistoricoTerminalDataSend();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public HistoricoTerminalDataSend(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public HistoricoTerminalDataSend()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("HistoricoTerminalDataSendCol")]
	public partial class HistoricoTerminalDataSendCol : esHistoricoTerminalDataSendCol, IEnumerable<HistoricoTerminalDataSend>
	{
	
		public HistoricoTerminalDataSendCol()
		{
		}

	
	
		public HistoricoTerminalDataSend FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(HistoricoTerminalDataSend))]
		public class HistoricoTerminalDataSendColWCFPacket : esCollectionWCFPacket<HistoricoTerminalDataSendCol>
		{
			public static implicit operator HistoricoTerminalDataSendCol(HistoricoTerminalDataSendColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator HistoricoTerminalDataSendColWCFPacket(HistoricoTerminalDataSendCol collection)
			{
				return new HistoricoTerminalDataSendColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class HistoricoTerminalDataSendQ : esHistoricoTerminalDataSendQ
	{
		public HistoricoTerminalDataSendQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public HistoricoTerminalDataSendQ()
		{
		}

		override protected string GetQueryName()
		{
			return "HistoricoTerminalDataSendQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new HistoricoTerminalDataSendQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(HistoricoTerminalDataSendQ query)
		{
			return HistoricoTerminalDataSendQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator HistoricoTerminalDataSendQ(string query)
		{
			return (HistoricoTerminalDataSendQ)HistoricoTerminalDataSendQ.SerializeHelper.FromXml(query, typeof(HistoricoTerminalDataSendQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esHistoricoTerminalDataSend : EntityBase
	{
		public esHistoricoTerminalDataSend()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			HistoricoTerminalDataSendQ query = new HistoricoTerminalDataSendQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to HistoricoTerminalDataSend.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal Id
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoTerminalDataSendMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalDataSendMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de creación del Registro en la Tabla
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime Fecha
		{
			get
			{
				return base.GetSystemDateTimeRequired(HistoricoTerminalDataSendMetadata.ColumnNames.Fecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoTerminalDataSendMetadata.ColumnNames.Fecha, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.Fecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalDataSend.ServicioID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ServicioID
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalDataSendMetadata.ColumnNames.ServicioID);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalDataSendMetadata.ColumnNames.ServicioID, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.ServicioID);
				}
			}
		}		
		
		/// <summary>
		/// Datos que componen el servicio o la peticion a envia al Terminal . Está formateado como XML.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Data
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalDataSendMetadata.ColumnNames.Data);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalDataSendMetadata.ColumnNames.Data, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.Data);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalDataSend.Data1
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Data1
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalDataSendMetadata.ColumnNames.Data1);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalDataSendMetadata.ColumnNames.Data1, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.Data1);
				}
			}
		}		
		
		/// <summary>
		/// Codigo de la Peticion. Codificado en el Software. Ej. Alta servicio,Modificacion Servicio, Anulacion o Borrado.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 PeticionCode
		{
			get
			{
				return base.GetSystemInt32Required(HistoricoTerminalDataSendMetadata.ColumnNames.PeticionCode);
			}
			
			set
			{
				if(base.SetSystemInt32(HistoricoTerminalDataSendMetadata.ColumnNames.PeticionCode, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.PeticionCode);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalDataSend.TerminalID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoTerminalDataSendMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalDataSendMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalDataSend.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoTerminalDataSendMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoTerminalDataSendMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Indica la prioridad de la Peticion. Influye en como el Servidor la procesa.  1-Normal 2-High 3-Highest 4-Critical
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte Priority
		{
			get
			{
				return base.GetSystemByteRequired(HistoricoTerminalDataSendMetadata.ColumnNames.Priority);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoTerminalDataSendMetadata.ColumnNames.Priority, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.Priority);
				}
			}
		}		
		
		/// <summary>
		/// Se utiliza para marcar el registro con un información sobre su estado de Proceso. Sus valores seran definidos en codigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? FlagState
		{
			get
			{
				return base.GetSystemInt32(HistoricoTerminalDataSendMetadata.ColumnNames.FlagState);
			}
			
			set
			{
				if(base.SetSystemInt32(HistoricoTerminalDataSendMetadata.ColumnNames.FlagState, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.FlagState);
				}
			}
		}		
		
		/// <summary>
		/// Ultima vez que el Servidor proceso este envio.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? LastTimeProcessed
		{
			get
			{
				return base.GetSystemDateTime(HistoricoTerminalDataSendMetadata.ColumnNames.LastTimeProcessed);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoTerminalDataSendMetadata.ColumnNames.LastTimeProcessed, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.LastTimeProcessed);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalDataSend.FechaEntroHistorico
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaEntroHistorico
		{
			get
			{
				return base.GetSystemDateTime(HistoricoTerminalDataSendMetadata.ColumnNames.FechaEntroHistorico);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoTerminalDataSendMetadata.ColumnNames.FechaEntroHistorico, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.FechaEntroHistorico);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalDataSend.IntentosProcessed
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 IntentosProcessed
		{
			get
			{
				return base.GetSystemInt32Required(HistoricoTerminalDataSendMetadata.ColumnNames.IntentosProcessed);
			}
			
			set
			{
				if(base.SetSystemInt32(HistoricoTerminalDataSendMetadata.ColumnNames.IntentosProcessed, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.IntentosProcessed);
				}
			}
		}		
		
		/// <summary>
		/// Indica el nº de veces que esta trama ha sido recibida por el Terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 RecibidaPorTerminal
		{
			get
			{
				return base.GetSystemInt32Required(HistoricoTerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal);
			}
			
			set
			{
				if(base.SetSystemInt32(HistoricoTerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.RecibidaPorTerminal);
				}
			}
		}		
		
		/// <summary>
		/// Motivo por el cual se introdujo en el Historico.  1-Terminal confirmo la peticion(Llego ok)  2-Server la borro, por antiguedad no habiendola enviado a ningun terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte MotivoEntroHistorico
		{
			get
			{
				return base.GetSystemByteRequired(HistoricoTerminalDataSendMetadata.ColumnNames.MotivoEntroHistorico);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoTerminalDataSendMetadata.ColumnNames.MotivoEntroHistorico, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.MotivoEntroHistorico);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalDataSend.NoEnviados
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String NoEnviados
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoTerminalDataSendMetadata.ColumnNames.NoEnviados);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoTerminalDataSendMetadata.ColumnNames.NoEnviados, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.NoEnviados);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoTerminalDataSend.ServerID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ServerID
		{
			get
			{
				return base.GetSystemInt16(HistoricoTerminalDataSendMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemInt16(HistoricoTerminalDataSendMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "Fecha": this.str().Fecha = (string)value; break;							
						case "ServicioID": this.str().ServicioID = (string)value; break;							
						case "Data": this.str().Data = (string)value; break;							
						case "Data1": this.str().Data1 = (string)value; break;							
						case "PeticionCode": this.str().PeticionCode = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "Priority": this.str().Priority = (string)value; break;							
						case "FlagState": this.str().FlagState = (string)value; break;							
						case "LastTimeProcessed": this.str().LastTimeProcessed = (string)value; break;							
						case "FechaEntroHistorico": this.str().FechaEntroHistorico = (string)value; break;							
						case "IntentosProcessed": this.str().IntentosProcessed = (string)value; break;							
						case "RecibidaPorTerminal": this.str().RecibidaPorTerminal = (string)value; break;							
						case "MotivoEntroHistorico": this.str().MotivoEntroHistorico = (string)value; break;							
						case "NoEnviados": this.str().NoEnviados = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.Id);
							break;
						
						case "Fecha":
						
							if (value == null || value is System.DateTime)
								this.Fecha = (System.DateTime)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.Fecha);
							break;
						
						case "PeticionCode":
						
							if (value == null || value is System.Int32)
								this.PeticionCode = (System.Int32)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.PeticionCode);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.VehiculoID);
							break;
						
						case "Priority":
						
							if (value == null || value is System.Byte)
								this.Priority = (System.Byte)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.Priority);
							break;
						
						case "FlagState":
						
							if (value == null || value is System.Int32)
								this.FlagState = (System.Int32?)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.FlagState);
							break;
						
						case "LastTimeProcessed":
						
							if (value == null || value is System.DateTime)
								this.LastTimeProcessed = (System.DateTime?)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.LastTimeProcessed);
							break;
						
						case "FechaEntroHistorico":
						
							if (value == null || value is System.DateTime)
								this.FechaEntroHistorico = (System.DateTime?)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.FechaEntroHistorico);
							break;
						
						case "IntentosProcessed":
						
							if (value == null || value is System.Int32)
								this.IntentosProcessed = (System.Int32)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.IntentosProcessed);
							break;
						
						case "RecibidaPorTerminal":
						
							if (value == null || value is System.Int32)
								this.RecibidaPorTerminal = (System.Int32)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.RecibidaPorTerminal);
							break;
						
						case "MotivoEntroHistorico":
						
							if (value == null || value is System.Byte)
								this.MotivoEntroHistorico = (System.Byte)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.MotivoEntroHistorico);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Int16)
								this.ServerID = (System.Int16?)value;
								OnPropertyChanged(HistoricoTerminalDataSendMetadata.PropertyNames.ServerID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esHistoricoTerminalDataSend entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToDecimal(value);
				}
			}

			public System.String Fecha
			{
				get
				{
					return Convert.ToString(entity.Fecha);
				}

				set
				{
					entity.Fecha = Convert.ToDateTime(value);
				}
			}

			public System.String ServicioID
			{
				get
				{
					return Convert.ToString(entity.ServicioID);
				}

				set
				{
					entity.ServicioID = Convert.ToString(value);
				}
			}

			public System.String Data
			{
				get
				{
					return Convert.ToString(entity.Data);
				}

				set
				{
					entity.Data = Convert.ToString(value);
				}
			}

			public System.String Data1
			{
				get
				{
					return Convert.ToString(entity.Data1);
				}

				set
				{
					entity.Data1 = Convert.ToString(value);
				}
			}

			public System.String PeticionCode
			{
				get
				{
					return Convert.ToString(entity.PeticionCode);
				}

				set
				{
					entity.PeticionCode = Convert.ToInt32(value);
				}
			}
	
			public System.String TerminalID
			{
				get
				{
					System.Decimal? data = entity.TerminalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalID = null;
					else entity.TerminalID = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
			
			public System.String Priority
			{
				get
				{
					return Convert.ToString(entity.Priority);
				}

				set
				{
					entity.Priority = Convert.ToByte(value);
				}
			}
	
			public System.String FlagState
			{
				get
				{
					System.Int32? data = entity.FlagState;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlagState = null;
					else entity.FlagState = Convert.ToInt32(value);
				}
			}
				
			public System.String LastTimeProcessed
			{
				get
				{
					System.DateTime? data = entity.LastTimeProcessed;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LastTimeProcessed = null;
					else entity.LastTimeProcessed = Convert.ToDateTime(value);
				}
			}
				
			public System.String FechaEntroHistorico
			{
				get
				{
					System.DateTime? data = entity.FechaEntroHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaEntroHistorico = null;
					else entity.FechaEntroHistorico = Convert.ToDateTime(value);
				}
			}
			
			public System.String IntentosProcessed
			{
				get
				{
					return Convert.ToString(entity.IntentosProcessed);
				}

				set
				{
					entity.IntentosProcessed = Convert.ToInt32(value);
				}
			}

			public System.String RecibidaPorTerminal
			{
				get
				{
					return Convert.ToString(entity.RecibidaPorTerminal);
				}

				set
				{
					entity.RecibidaPorTerminal = Convert.ToInt32(value);
				}
			}

			public System.String MotivoEntroHistorico
			{
				get
				{
					return Convert.ToString(entity.MotivoEntroHistorico);
				}

				set
				{
					entity.MotivoEntroHistorico = Convert.ToByte(value);
				}
			}

			public System.String NoEnviados
			{
				get
				{
					return Convert.ToString(entity.NoEnviados);
				}

				set
				{
					entity.NoEnviados = Convert.ToString(value);
				}
			}
	
			public System.String ServerID
			{
				get
				{
					System.Int16? data = entity.ServerID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServerID = null;
					else entity.ServerID = Convert.ToInt16(value);
				}
			}
			

			private esHistoricoTerminalDataSend entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return HistoricoTerminalDataSendMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public HistoricoTerminalDataSendQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoTerminalDataSendQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoTerminalDataSendQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(HistoricoTerminalDataSendQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private HistoricoTerminalDataSendQ query;		
	}



	[Serializable]
	abstract public partial class esHistoricoTerminalDataSendCol : CollectionBase<HistoricoTerminalDataSend>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoTerminalDataSendMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "HistoricoTerminalDataSendCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public HistoricoTerminalDataSendQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoTerminalDataSendQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoTerminalDataSendQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoTerminalDataSendQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(HistoricoTerminalDataSendQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((HistoricoTerminalDataSendQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private HistoricoTerminalDataSendQ query;
	}



	[Serializable]
	abstract public partial class esHistoricoTerminalDataSendQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoTerminalDataSendMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.Id,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.Fecha,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Fecha, esSystemType.DateTime));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.ServicioID,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.ServicioID, esSystemType.String));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.Data,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Data, esSystemType.String));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.Data1,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Data1, esSystemType.String));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.PeticionCode,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.PeticionCode, esSystemType.Int32));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.TerminalID,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.VehiculoID,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.Priority,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Priority, esSystemType.Byte));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.FlagState,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.FlagState, esSystemType.Int32));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.LastTimeProcessed,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.LastTimeProcessed, esSystemType.DateTime));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.FechaEntroHistorico,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.FechaEntroHistorico, esSystemType.DateTime));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.IntentosProcessed,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.IntentosProcessed, esSystemType.Int32));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal, esSystemType.Int32));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.MotivoEntroHistorico,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.MotivoEntroHistorico, esSystemType.Byte));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.NoEnviados,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.NoEnviados, esSystemType.String));
			_queryItems.Add(HistoricoTerminalDataSendMetadata.ColumnNames.ServerID,new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.ServerID, esSystemType.Int16));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem Fecha
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Fecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem ServicioID
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.ServicioID, esSystemType.String); }
		} 
		
		public esQueryItem Data
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Data, esSystemType.String); }
		} 
		
		public esQueryItem Data1
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Data1, esSystemType.String); }
		} 
		
		public esQueryItem PeticionCode
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.PeticionCode, esSystemType.Int32); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem Priority
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.Priority, esSystemType.Byte); }
		} 
		
		public esQueryItem FlagState
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.FlagState, esSystemType.Int32); }
		} 
		
		public esQueryItem LastTimeProcessed
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.LastTimeProcessed, esSystemType.DateTime); }
		} 
		
		public esQueryItem FechaEntroHistorico
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.FechaEntroHistorico, esSystemType.DateTime); }
		} 
		
		public esQueryItem IntentosProcessed
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.IntentosProcessed, esSystemType.Int32); }
		} 
		
		public esQueryItem RecibidaPorTerminal
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal, esSystemType.Int32); }
		} 
		
		public esQueryItem MotivoEntroHistorico
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.MotivoEntroHistorico, esSystemType.Byte); }
		} 
		
		public esQueryItem NoEnviados
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.NoEnviados, esSystemType.String); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, HistoricoTerminalDataSendMetadata.ColumnNames.ServerID, esSystemType.Int16); }
		} 
		
		#endregion
		
	}


	
	public partial class HistoricoTerminalDataSend : esHistoricoTerminalDataSend
	{

		
		
	}
	



	[Serializable]
	public partial class HistoricoTerminalDataSendMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HistoricoTerminalDataSendMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.Fecha, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.Fecha;
			c.Description = "Fecha de creación del Registro en la Tabla";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.ServicioID, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.ServicioID;
			c.CharacterMaxLength = 2000;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.Data, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.Data;
			c.CharacterMaxLength = 3000;
			c.HasDefault = true;
			c.Default = @"('')";
			c.Description = "Datos que componen el servicio o la peticion a envia al Terminal . Está formateado como XML.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.Data1, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.Data1;
			c.CharacterMaxLength = 1000;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.PeticionCode, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.PeticionCode;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Codigo de la Peticion. Codificado en el Software. Ej. Alta servicio,Modificacion Servicio, Anulacion o Borrado.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.TerminalID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.VehiculoID, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.Priority, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.Priority;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Indica la prioridad de la Peticion. Influye en como el Servidor la procesa.  1-Normal 2-High 3-Highest 4-Critical";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.FlagState, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.FlagState;
			c.NumericPrecision = 10;
			c.Description = "Se utiliza para marcar el registro con un información sobre su estado de Proceso. Sus valores seran definidos en codigo";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.LastTimeProcessed, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.LastTimeProcessed;
			c.Description = "Ultima vez que el Servidor proceso este envio.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.FechaEntroHistorico, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.FechaEntroHistorico;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.IntentosProcessed, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.IntentosProcessed;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.RecibidaPorTerminal;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Indica el nº de veces que esta trama ha sido recibida por el Terminal.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.MotivoEntroHistorico, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.MotivoEntroHistorico;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.Description = "Motivo por el cual se introdujo en el Historico.  1-Terminal confirmo la peticion(Llego ok)  2-Server la borro, por antiguedad no habiendola enviado a ningun terminal.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.NoEnviados, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.NoEnviados;
			c.CharacterMaxLength = 2000;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoTerminalDataSendMetadata.ColumnNames.ServerID, 16, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = HistoricoTerminalDataSendMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 5;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public HistoricoTerminalDataSendMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string Fecha = "Fecha";
			 public const string ServicioID = "ServicioID";
			 public const string Data = "Data";
			 public const string Data1 = "Data1";
			 public const string PeticionCode = "PeticionCode";
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string Priority = "Priority";
			 public const string FlagState = "FlagState";
			 public const string LastTimeProcessed = "LastTimeProcessed";
			 public const string FechaEntroHistorico = "FechaEntroHistorico";
			 public const string IntentosProcessed = "IntentosProcessed";
			 public const string RecibidaPorTerminal = "RecibidaPorTerminal";
			 public const string MotivoEntroHistorico = "MotivoEntroHistorico";
			 public const string NoEnviados = "NoEnviados";
			 public const string ServerID = "ServerID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Fecha = "Fecha";
			 public const string ServicioID = "ServicioID";
			 public const string Data = "Data";
			 public const string Data1 = "Data1";
			 public const string PeticionCode = "PeticionCode";
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string Priority = "Priority";
			 public const string FlagState = "FlagState";
			 public const string LastTimeProcessed = "LastTimeProcessed";
			 public const string FechaEntroHistorico = "FechaEntroHistorico";
			 public const string IntentosProcessed = "IntentosProcessed";
			 public const string RecibidaPorTerminal = "RecibidaPorTerminal";
			 public const string MotivoEntroHistorico = "MotivoEntroHistorico";
			 public const string NoEnviados = "NoEnviados";
			 public const string ServerID = "ServerID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HistoricoTerminalDataSendMetadata))
			{
				if(HistoricoTerminalDataSendMetadata.mapDelegates == null)
				{
					HistoricoTerminalDataSendMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HistoricoTerminalDataSendMetadata.meta == null)
				{
					HistoricoTerminalDataSendMetadata.meta = new HistoricoTerminalDataSendMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Fecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ServicioID", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data1", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PeticionCode", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Priority", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("FlagState", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("LastTimeProcessed", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FechaEntroHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IntentosProcessed", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RecibidaPorTerminal", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("MotivoEntroHistorico", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NoEnviados", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ServerID", new esTypeMap("smallint", "System.Int16"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "HistoricoTerminalDataSend";
				meta.Destination = "HistoricoTerminalDataSend";
				
				meta.spInsert = "proc_HistoricoTerminalDataSendInsert";				
				meta.spUpdate = "proc_HistoricoTerminalDataSendUpdate";		
				meta.spDelete = "proc_HistoricoTerminalDataSendDelete";
				meta.spLoadAll = "proc_HistoricoTerminalDataSendLoadAll";
				meta.spLoadByPrimaryKey = "proc_HistoricoTerminalDataSendLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HistoricoTerminalDataSendMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
