
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// LISTA DE TODAS LAS INCIDENCIAS QUE SE PUEDEN MOSTRAR EN EL SISTEMA. CADA REGISTRO ES DE TIPO PESO BINARIOS. Se relaciona con la tabla opciones en el campo incidencias.
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("IncidenciasDescripcion")]
	public partial class IncidenciasDescripcion : esIncidenciasDescripcion
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new IncidenciasDescripcion();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Int32 id)
		{
			var obj = new IncidenciasDescripcion();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Int32 id, esSqlAccessType sqlAccessType)
		{
			var obj = new IncidenciasDescripcion();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Int32 ID)
        {
            try
            {
                var e = new IncidenciasDescripcion();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static IncidenciasDescripcion Get(System.Int32 ID)
        {
            try
            {
                var e = new IncidenciasDescripcion();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public IncidenciasDescripcion(System.Int32 ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public IncidenciasDescripcion()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("IncidenciasDescripcionCol")]
	public partial class IncidenciasDescripcionCol : esIncidenciasDescripcionCol, IEnumerable<IncidenciasDescripcion>
	{
	
		public IncidenciasDescripcionCol()
		{
		}

	
	
		public IncidenciasDescripcion FindByPrimaryKey(System.Int32 id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(IncidenciasDescripcion))]
		public class IncidenciasDescripcionColWCFPacket : esCollectionWCFPacket<IncidenciasDescripcionCol>
		{
			public static implicit operator IncidenciasDescripcionCol(IncidenciasDescripcionColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator IncidenciasDescripcionColWCFPacket(IncidenciasDescripcionCol collection)
			{
				return new IncidenciasDescripcionColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class IncidenciasDescripcionQ : esIncidenciasDescripcionQ
	{
		public IncidenciasDescripcionQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public IncidenciasDescripcionQ()
		{
		}

		override protected string GetQueryName()
		{
			return "IncidenciasDescripcionQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new IncidenciasDescripcionQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(IncidenciasDescripcionQ query)
		{
			return IncidenciasDescripcionQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator IncidenciasDescripcionQ(string query)
		{
			return (IncidenciasDescripcionQ)IncidenciasDescripcionQ.SerializeHelper.FromXml(query, typeof(IncidenciasDescripcionQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esIncidenciasDescripcion : EntityBase
	{
		public esIncidenciasDescripcion()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 id)
		{
			IncidenciasDescripcionQ query = new IncidenciasDescripcionQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// ID de la incidencia. 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 Id
		{
			get
			{
				return base.GetSystemInt32Required(IncidenciasDescripcionMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemInt32(IncidenciasDescripcionMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(IncidenciasDescripcionMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Maps to IncidenciasDescripcion.Nombre
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Nombre
		{
			get
			{
				return base.GetSystemStringRequired(IncidenciasDescripcionMetadata.ColumnNames.Nombre);
			}
			
			set
			{
				if(base.SetSystemString(IncidenciasDescripcionMetadata.ColumnNames.Nombre, value))
				{
					OnPropertyChanged(IncidenciasDescripcionMetadata.PropertyNames.Nombre);
				}
			}
		}		
		
		/// <summary>
		/// Comentario descriptivo de la incidencia. Lo que se enviará por correo.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Comentario
		{
			get
			{
				return base.GetSystemStringRequired(IncidenciasDescripcionMetadata.ColumnNames.Comentario);
			}
			
			set
			{
				if(base.SetSystemString(IncidenciasDescripcionMetadata.ColumnNames.Comentario, value))
				{
					OnPropertyChanged(IncidenciasDescripcionMetadata.PropertyNames.Comentario);
				}
			}
		}		
		
		/// <summary>
		/// Permite especificar un requeisitor que tiene que cumpliar para que se cumpla la incidencia
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int64? Requisito
		{
			get
			{
				return base.GetSystemInt64(IncidenciasDescripcionMetadata.ColumnNames.Requisito);
			}
			
			set
			{
				if(base.SetSystemInt64(IncidenciasDescripcionMetadata.ColumnNames.Requisito, value))
				{
					OnPropertyChanged(IncidenciasDescripcionMetadata.PropertyNames.Requisito);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "Nombre": this.str().Nombre = (string)value; break;							
						case "Comentario": this.str().Comentario = (string)value; break;							
						case "Requisito": this.str().Requisito = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Int32)
								this.Id = (System.Int32)value;
								OnPropertyChanged(IncidenciasDescripcionMetadata.PropertyNames.Id);
							break;
						
						case "Requisito":
						
							if (value == null || value is System.Int64)
								this.Requisito = (System.Int64?)value;
								OnPropertyChanged(IncidenciasDescripcionMetadata.PropertyNames.Requisito);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esIncidenciasDescripcion entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToInt32(value);
				}
			}

			public System.String Nombre
			{
				get
				{
					return Convert.ToString(entity.Nombre);
				}

				set
				{
					entity.Nombre = Convert.ToString(value);
				}
			}

			public System.String Comentario
			{
				get
				{
					return Convert.ToString(entity.Comentario);
				}

				set
				{
					entity.Comentario = Convert.ToString(value);
				}
			}
	
			public System.String Requisito
			{
				get
				{
					System.Int64? data = entity.Requisito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Requisito = null;
					else entity.Requisito = Convert.ToInt64(value);
				}
			}
			

			private esIncidenciasDescripcion entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return IncidenciasDescripcionMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public IncidenciasDescripcionQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncidenciasDescripcionQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(IncidenciasDescripcionQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(IncidenciasDescripcionQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private IncidenciasDescripcionQ query;		
	}



	[Serializable]
	abstract public partial class esIncidenciasDescripcionCol : CollectionBase<IncidenciasDescripcion>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IncidenciasDescripcionMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "IncidenciasDescripcionCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public IncidenciasDescripcionQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncidenciasDescripcionQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(IncidenciasDescripcionQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IncidenciasDescripcionQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(IncidenciasDescripcionQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((IncidenciasDescripcionQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private IncidenciasDescripcionQ query;
	}



	[Serializable]
	abstract public partial class esIncidenciasDescripcionQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return IncidenciasDescripcionMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(IncidenciasDescripcionMetadata.ColumnNames.Id,new esQueryItem(this, IncidenciasDescripcionMetadata.ColumnNames.Id, esSystemType.Int32));
			_queryItems.Add(IncidenciasDescripcionMetadata.ColumnNames.Nombre,new esQueryItem(this, IncidenciasDescripcionMetadata.ColumnNames.Nombre, esSystemType.String));
			_queryItems.Add(IncidenciasDescripcionMetadata.ColumnNames.Comentario,new esQueryItem(this, IncidenciasDescripcionMetadata.ColumnNames.Comentario, esSystemType.String));
			_queryItems.Add(IncidenciasDescripcionMetadata.ColumnNames.Requisito,new esQueryItem(this, IncidenciasDescripcionMetadata.ColumnNames.Requisito, esSystemType.Int64));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, IncidenciasDescripcionMetadata.ColumnNames.Id, esSystemType.Int32); }
		} 
		
		public esQueryItem Nombre
		{
			get { return new esQueryItem(this, IncidenciasDescripcionMetadata.ColumnNames.Nombre, esSystemType.String); }
		} 
		
		public esQueryItem Comentario
		{
			get { return new esQueryItem(this, IncidenciasDescripcionMetadata.ColumnNames.Comentario, esSystemType.String); }
		} 
		
		public esQueryItem Requisito
		{
			get { return new esQueryItem(this, IncidenciasDescripcionMetadata.ColumnNames.Requisito, esSystemType.Int64); }
		} 
		
		#endregion
		
	}


	
	public partial class IncidenciasDescripcion : esIncidenciasDescripcion
	{

		
		
	}
	



	[Serializable]
	public partial class IncidenciasDescripcionMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IncidenciasDescripcionMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IncidenciasDescripcionMetadata.ColumnNames.Id, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IncidenciasDescripcionMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 10;
			c.Description = "ID de la incidencia. ";
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciasDescripcionMetadata.ColumnNames.Nombre, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = IncidenciasDescripcionMetadata.PropertyNames.Nombre;
			c.CharacterMaxLength = 200;
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciasDescripcionMetadata.ColumnNames.Comentario, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = IncidenciasDescripcionMetadata.PropertyNames.Comentario;
			c.CharacterMaxLength = 1500;
			c.Description = "Comentario descriptivo de la incidencia. Lo que se enviará por correo.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciasDescripcionMetadata.ColumnNames.Requisito, 3, typeof(System.Int64), esSystemType.Int64);
			c.PropertyName = IncidenciasDescripcionMetadata.PropertyNames.Requisito;
			c.NumericPrecision = 19;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Permite especificar un requeisitor que tiene que cumpliar para que se cumpla la incidencia";
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public IncidenciasDescripcionMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string Nombre = "Nombre";
			 public const string Comentario = "Comentario";
			 public const string Requisito = "Requisito";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Nombre = "Nombre";
			 public const string Comentario = "Comentario";
			 public const string Requisito = "Requisito";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IncidenciasDescripcionMetadata))
			{
				if(IncidenciasDescripcionMetadata.mapDelegates == null)
				{
					IncidenciasDescripcionMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IncidenciasDescripcionMetadata.meta == null)
				{
					IncidenciasDescripcionMetadata.meta = new IncidenciasDescripcionMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nombre", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Comentario", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Requisito", new esTypeMap("bigint", "System.Int64"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "IncidenciasDescripcion";
				meta.Destination = "IncidenciasDescripcion";
				
				meta.spInsert = "proc_IncidenciasDescripcionInsert";				
				meta.spUpdate = "proc_IncidenciasDescripcionUpdate";		
				meta.spDelete = "proc_IncidenciasDescripcionDelete";
				meta.spLoadAll = "proc_IncidenciasDescripcionLoadAll";
				meta.spLoadByPrimaryKey = "proc_IncidenciasDescripcionLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IncidenciasDescripcionMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
