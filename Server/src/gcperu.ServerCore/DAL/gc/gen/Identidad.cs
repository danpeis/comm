
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Encapsulates the 'Identidad' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("Identidad")]
	public partial class Identidad : esIdentidad
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new Identidad();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.String id)
		{
			var obj = new Identidad();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.String id, esSqlAccessType sqlAccessType)
		{
			var obj = new Identidad();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.String ID)
        {
            try
            {
                var e = new Identidad();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static Identidad Get(System.String ID)
        {
            try
            {
                var e = new Identidad();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public Identidad(System.String ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public Identidad()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("IdentidadCol")]
	public partial class IdentidadCol : esIdentidadCol, IEnumerable<Identidad>
	{
	
		public IdentidadCol()
		{
		}

	
	
		public Identidad FindByPrimaryKey(System.String id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(Identidad))]
		public class IdentidadColWCFPacket : esCollectionWCFPacket<IdentidadCol>
		{
			public static implicit operator IdentidadCol(IdentidadColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator IdentidadColWCFPacket(IdentidadCol collection)
			{
				return new IdentidadColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class IdentidadQ : esIdentidadQ
	{
		public IdentidadQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public IdentidadQ()
		{
		}

		override protected string GetQueryName()
		{
			return "IdentidadQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new IdentidadQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(IdentidadQ query)
		{
			return IdentidadQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator IdentidadQ(string query)
		{
			return (IdentidadQ)IdentidadQ.SerializeHelper.FromXml(query, typeof(IdentidadQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esIdentidad : EntityBase
	{
		public esIdentidad()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.String id)
		{
			IdentidadQ query = new IdentidadQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to Identidad.Id
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Id
		{
			get
			{
				return base.GetSystemStringRequired(IdentidadMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemString(IdentidadMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(IdentidadMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Identidad.Version
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16 Version
		{
			get
			{
				return base.GetSystemInt16Required(IdentidadMetadata.ColumnNames.Version);
			}
			
			set
			{
				if(base.SetSystemInt16(IdentidadMetadata.ColumnNames.Version, value))
				{
					OnPropertyChanged(IdentidadMetadata.PropertyNames.Version);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Identidad.Fecha
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? Fecha
		{
			get
			{
				return base.GetSystemDateTime(IdentidadMetadata.ColumnNames.Fecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(IdentidadMetadata.ColumnNames.Fecha, value))
				{
					OnPropertyChanged(IdentidadMetadata.PropertyNames.Fecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Identidad.ExeName
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ExeName
		{
			get
			{
				return base.GetSystemStringRequired(IdentidadMetadata.ColumnNames.ExeName);
			}
			
			set
			{
				if(base.SetSystemString(IdentidadMetadata.ColumnNames.ExeName, value))
				{
					OnPropertyChanged(IdentidadMetadata.PropertyNames.ExeName);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "Version": this.str().Version = (string)value; break;							
						case "Fecha": this.str().Fecha = (string)value; break;							
						case "ExeName": this.str().ExeName = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Version":
						
							if (value == null || value is System.Int16)
								this.Version = (System.Int16)value;
								OnPropertyChanged(IdentidadMetadata.PropertyNames.Version);
							break;
						
						case "Fecha":
						
							if (value == null || value is System.DateTime)
								this.Fecha = (System.DateTime?)value;
								OnPropertyChanged(IdentidadMetadata.PropertyNames.Fecha);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esIdentidad entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToString(value);
				}
			}

			public System.String Version
			{
				get
				{
					return Convert.ToString(entity.Version);
				}

				set
				{
					entity.Version = Convert.ToInt16(value);
				}
			}
	
			public System.String Fecha
			{
				get
				{
					System.DateTime? data = entity.Fecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fecha = null;
					else entity.Fecha = Convert.ToDateTime(value);
				}
			}
			
			public System.String ExeName
			{
				get
				{
					return Convert.ToString(entity.ExeName);
				}

				set
				{
					entity.ExeName = Convert.ToString(value);
				}
			}


			private esIdentidad entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return IdentidadMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public IdentidadQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IdentidadQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(IdentidadQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(IdentidadQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private IdentidadQ query;		
	}



	[Serializable]
	abstract public partial class esIdentidadCol : CollectionBase<Identidad>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IdentidadMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "IdentidadCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public IdentidadQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IdentidadQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(IdentidadQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IdentidadQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(IdentidadQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((IdentidadQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private IdentidadQ query;
	}



	[Serializable]
	abstract public partial class esIdentidadQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return IdentidadMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(IdentidadMetadata.ColumnNames.Id,new esQueryItem(this, IdentidadMetadata.ColumnNames.Id, esSystemType.String));
			_queryItems.Add(IdentidadMetadata.ColumnNames.Version,new esQueryItem(this, IdentidadMetadata.ColumnNames.Version, esSystemType.Int16));
			_queryItems.Add(IdentidadMetadata.ColumnNames.Fecha,new esQueryItem(this, IdentidadMetadata.ColumnNames.Fecha, esSystemType.DateTime));
			_queryItems.Add(IdentidadMetadata.ColumnNames.ExeName,new esQueryItem(this, IdentidadMetadata.ColumnNames.ExeName, esSystemType.String));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, IdentidadMetadata.ColumnNames.Id, esSystemType.String); }
		} 
		
		public esQueryItem Version
		{
			get { return new esQueryItem(this, IdentidadMetadata.ColumnNames.Version, esSystemType.Int16); }
		} 
		
		public esQueryItem Fecha
		{
			get { return new esQueryItem(this, IdentidadMetadata.ColumnNames.Fecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem ExeName
		{
			get { return new esQueryItem(this, IdentidadMetadata.ColumnNames.ExeName, esSystemType.String); }
		} 
		
		#endregion
		
	}


	
	public partial class Identidad : esIdentidad
	{

		
		
	}
	



	[Serializable]
	public partial class IdentidadMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IdentidadMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IdentidadMetadata.ColumnNames.Id, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = IdentidadMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			m_columns.Add(c);
				
			c = new esColumnMetadata(IdentidadMetadata.ColumnNames.Version, 1, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = IdentidadMetadata.PropertyNames.Version;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(IdentidadMetadata.ColumnNames.Fecha, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IdentidadMetadata.PropertyNames.Fecha;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(IdentidadMetadata.ColumnNames.ExeName, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = IdentidadMetadata.PropertyNames.ExeName;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public IdentidadMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "Id";
			 public const string Version = "Version";
			 public const string Fecha = "Fecha";
			 public const string ExeName = "ExeName";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Version = "Version";
			 public const string Fecha = "Fecha";
			 public const string ExeName = "ExeName";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IdentidadMetadata))
			{
				if(IdentidadMetadata.mapDelegates == null)
				{
					IdentidadMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IdentidadMetadata.meta == null)
				{
					IdentidadMetadata.meta = new IdentidadMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("Version", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Fecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ExeName", new esTypeMap("nvarchar", "System.String"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "Identidad";
				meta.Destination = "Identidad";
				
				meta.spInsert = "proc_IdentidadInsert";				
				meta.spUpdate = "proc_IdentidadUpdate";		
				meta.spDelete = "proc_IdentidadDelete";
				meta.spLoadAll = "proc_IdentidadLoadAll";
				meta.spLoadByPrimaryKey = "proc_IdentidadLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IdentidadMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
