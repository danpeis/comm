
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:22
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Encapsulates the 'HistoricoConexiones' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("HistoricoConexiones")]
	public partial class HistoricoConexiones : esHistoricoConexiones
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new HistoricoConexiones();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new HistoricoConexiones();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new HistoricoConexiones();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new HistoricoConexiones();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static HistoricoConexiones Get(System.Decimal ID)
        {
            try
            {
                var e = new HistoricoConexiones();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public HistoricoConexiones(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public HistoricoConexiones()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("HistoricoConexionesCol")]
	public partial class HistoricoConexionesCol : esHistoricoConexionesCol, IEnumerable<HistoricoConexiones>
	{
	
		public HistoricoConexionesCol()
		{
		}

	
	
		public HistoricoConexiones FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(HistoricoConexiones))]
		public class HistoricoConexionesColWCFPacket : esCollectionWCFPacket<HistoricoConexionesCol>
		{
			public static implicit operator HistoricoConexionesCol(HistoricoConexionesColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator HistoricoConexionesColWCFPacket(HistoricoConexionesCol collection)
			{
				return new HistoricoConexionesColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class HistoricoConexionesQ : esHistoricoConexionesQ
	{
		public HistoricoConexionesQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public HistoricoConexionesQ()
		{
		}

		override protected string GetQueryName()
		{
			return "HistoricoConexionesQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new HistoricoConexionesQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(HistoricoConexionesQ query)
		{
			return HistoricoConexionesQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator HistoricoConexionesQ(string query)
		{
			return (HistoricoConexionesQ)HistoricoConexionesQ.SerializeHelper.FromXml(query, typeof(HistoricoConexionesQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esHistoricoConexiones : EntityBase
	{
		public esHistoricoConexiones()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			HistoricoConexionesQ query = new HistoricoConexionesQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to HistoricoConexiones.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal Id
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoConexionesMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoConexionesMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal TerminalID
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoConexionesMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoConexionesMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Indica si el Terminal está conectado. Si es <> null => conectado sino Desconectado. Guarda la fecha del comienzo de conexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalConnectedFrom
		{
			get
			{
				return base.GetSystemDateTime(HistoricoConexionesMetadata.ColumnNames.TerminalConnectedFrom);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoConexionesMetadata.ColumnNames.TerminalConnectedFrom, value))
				{
					OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.TerminalConnectedFrom);
				}
			}
		}		
		
		/// <summary>
		/// Si posee valor indica que el Terminal está desconectado, y marca la fecha de desconexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalDisconnect
		{
			get
			{
				return base.GetSystemDateTime(HistoricoConexionesMetadata.ColumnNames.TerminalDisconnect);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoConexionesMetadata.ColumnNames.TerminalDisconnect, value))
				{
					OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.TerminalDisconnect);
				}
			}
		}		
		
		/// <summary>
		/// Duración en Horas de la ultima sesion ininterrumpida que este terminal estuvo conectado.  Es null si nunca se conecto este Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalLastSessionDuration
		{
			get
			{
				return base.GetSystemDecimal(HistoricoConexionesMetadata.ColumnNames.TerminalLastSessionDuration);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoConexionesMetadata.ColumnNames.TerminalLastSessionDuration, value))
				{
					OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.TerminalLastSessionDuration);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoConexiones.VehiculoCodigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigo
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoConexionesMetadata.ColumnNames.VehiculoCodigo);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoConexionesMetadata.ColumnNames.VehiculoCodigo, value))
				{
					OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.VehiculoCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoConexiones.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoConexionesMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoConexionesMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "TerminalConnectedFrom": this.str().TerminalConnectedFrom = (string)value; break;							
						case "TerminalDisconnect": this.str().TerminalDisconnect = (string)value; break;							
						case "TerminalLastSessionDuration": this.str().TerminalLastSessionDuration = (string)value; break;							
						case "VehiculoCodigo": this.str().VehiculoCodigo = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal)value;
								OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.Id);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal)value;
								OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.TerminalID);
							break;
						
						case "TerminalConnectedFrom":
						
							if (value == null || value is System.DateTime)
								this.TerminalConnectedFrom = (System.DateTime?)value;
								OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.TerminalConnectedFrom);
							break;
						
						case "TerminalDisconnect":
						
							if (value == null || value is System.DateTime)
								this.TerminalDisconnect = (System.DateTime?)value;
								OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.TerminalDisconnect);
							break;
						
						case "TerminalLastSessionDuration":
						
							if (value == null || value is System.Decimal)
								this.TerminalLastSessionDuration = (System.Decimal?)value;
								OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.TerminalLastSessionDuration);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoConexionesMetadata.PropertyNames.VehiculoID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esHistoricoConexiones entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToDecimal(value);
				}
			}

			public System.String TerminalID
			{
				get
				{
					return Convert.ToString(entity.TerminalID);
				}

				set
				{
					entity.TerminalID = Convert.ToDecimal(value);
				}
			}
	
			public System.String TerminalConnectedFrom
			{
				get
				{
					System.DateTime? data = entity.TerminalConnectedFrom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalConnectedFrom = null;
					else entity.TerminalConnectedFrom = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalDisconnect
			{
				get
				{
					System.DateTime? data = entity.TerminalDisconnect;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalDisconnect = null;
					else entity.TerminalDisconnect = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalLastSessionDuration
			{
				get
				{
					System.Decimal? data = entity.TerminalLastSessionDuration;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalLastSessionDuration = null;
					else entity.TerminalLastSessionDuration = Convert.ToDecimal(value);
				}
			}
			
			public System.String VehiculoCodigo
			{
				get
				{
					return Convert.ToString(entity.VehiculoCodigo);
				}

				set
				{
					entity.VehiculoCodigo = Convert.ToString(value);
				}
			}
	
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
			

			private esHistoricoConexiones entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return HistoricoConexionesMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public HistoricoConexionesQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoConexionesQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoConexionesQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(HistoricoConexionesQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private HistoricoConexionesQ query;		
	}



	[Serializable]
	abstract public partial class esHistoricoConexionesCol : CollectionBase<HistoricoConexiones>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoConexionesMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "HistoricoConexionesCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public HistoricoConexionesQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoConexionesQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoConexionesQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoConexionesQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(HistoricoConexionesQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((HistoricoConexionesQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private HistoricoConexionesQ query;
	}



	[Serializable]
	abstract public partial class esHistoricoConexionesQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoConexionesMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(HistoricoConexionesMetadata.ColumnNames.Id,new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(HistoricoConexionesMetadata.ColumnNames.TerminalID,new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(HistoricoConexionesMetadata.ColumnNames.TerminalConnectedFrom,new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.TerminalConnectedFrom, esSystemType.DateTime));
			_queryItems.Add(HistoricoConexionesMetadata.ColumnNames.TerminalDisconnect,new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.TerminalDisconnect, esSystemType.DateTime));
			_queryItems.Add(HistoricoConexionesMetadata.ColumnNames.TerminalLastSessionDuration,new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.TerminalLastSessionDuration, esSystemType.Decimal));
			_queryItems.Add(HistoricoConexionesMetadata.ColumnNames.VehiculoCodigo,new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.VehiculoCodigo, esSystemType.String));
			_queryItems.Add(HistoricoConexionesMetadata.ColumnNames.VehiculoID,new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalConnectedFrom
		{
			get { return new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.TerminalConnectedFrom, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalDisconnect
		{
			get { return new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.TerminalDisconnect, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalLastSessionDuration
		{
			get { return new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.TerminalLastSessionDuration, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoCodigo
		{
			get { return new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.VehiculoCodigo, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, HistoricoConexionesMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class HistoricoConexiones : esHistoricoConexiones
	{

		
		
	}
	



	[Serializable]
	public partial class HistoricoConexionesMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HistoricoConexionesMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HistoricoConexionesMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoConexionesMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoConexionesMetadata.ColumnNames.TerminalID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoConexionesMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoConexionesMetadata.ColumnNames.TerminalConnectedFrom, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoConexionesMetadata.PropertyNames.TerminalConnectedFrom;
			c.Description = "Indica si el Terminal está conectado. Si es <> null => conectado sino Desconectado. Guarda la fecha del comienzo de conexion.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoConexionesMetadata.ColumnNames.TerminalDisconnect, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoConexionesMetadata.PropertyNames.TerminalDisconnect;
			c.Description = "Si posee valor indica que el Terminal está desconectado, y marca la fecha de desconexion.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoConexionesMetadata.ColumnNames.TerminalLastSessionDuration, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoConexionesMetadata.PropertyNames.TerminalLastSessionDuration;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Duración en Horas de la ultima sesion ininterrumpida que este terminal estuvo conectado.  Es null si nunca se conecto este Terminal";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoConexionesMetadata.ColumnNames.VehiculoCodigo, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoConexionesMetadata.PropertyNames.VehiculoCodigo;
			c.CharacterMaxLength = 10;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoConexionesMetadata.ColumnNames.VehiculoID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoConexionesMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public HistoricoConexionesMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string TerminalID = "TerminalID";
			 public const string TerminalConnectedFrom = "TerminalConnectedFrom";
			 public const string TerminalDisconnect = "TerminalDisconnect";
			 public const string TerminalLastSessionDuration = "TerminalLastSessionDuration";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoID = "VehiculoID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string TerminalID = "TerminalID";
			 public const string TerminalConnectedFrom = "TerminalConnectedFrom";
			 public const string TerminalDisconnect = "TerminalDisconnect";
			 public const string TerminalLastSessionDuration = "TerminalLastSessionDuration";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoID = "VehiculoID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HistoricoConexionesMetadata))
			{
				if(HistoricoConexionesMetadata.mapDelegates == null)
				{
					HistoricoConexionesMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HistoricoConexionesMetadata.meta == null)
				{
					HistoricoConexionesMetadata.meta = new HistoricoConexionesMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalConnectedFrom", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalDisconnect", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalLastSessionDuration", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoCodigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "HistoricoConexiones";
				meta.Destination = "HistoricoConexiones";
				
				meta.spInsert = "proc_HistoricoConexionesInsert";				
				meta.spUpdate = "proc_HistoricoConexionesUpdate";		
				meta.spDelete = "proc_HistoricoConexionesDelete";
				meta.spLoadAll = "proc_HistoricoConexionesLoadAll";
				meta.spLoadByPrimaryKey = "proc_HistoricoConexionesLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HistoricoConexionesMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
