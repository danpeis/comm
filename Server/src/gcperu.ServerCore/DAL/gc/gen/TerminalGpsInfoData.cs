
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// GUARDA CADA POSICION GPS EMITIDA POR LOS TERMINALES. ESTA POSICION PUEDE SER ENVIADA PERIODICAMENTE, O CUANDO OCURRE UN HECHO DESTACABLE. (Cursar un Servicio, Finalizar, ... Motor Parado, Motor en Marcha, ....)
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TerminalGpsInfoData")]
	public partial class TerminalGpsInfoData : esTerminalGpsInfoData
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TerminalGpsInfoData();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new TerminalGpsInfoData();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new TerminalGpsInfoData();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new TerminalGpsInfoData();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TerminalGpsInfoData Get(System.Decimal ID)
        {
            try
            {
                var e = new TerminalGpsInfoData();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TerminalGpsInfoData(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public TerminalGpsInfoData()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TerminalGpsInfoDataCol")]
	public partial class TerminalGpsInfoDataCol : esTerminalGpsInfoDataCol, IEnumerable<TerminalGpsInfoData>
	{
	
		public TerminalGpsInfoDataCol()
		{
		}

	
	
		public TerminalGpsInfoData FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TerminalGpsInfoData))]
		public class TerminalGpsInfoDataColWCFPacket : esCollectionWCFPacket<TerminalGpsInfoDataCol>
		{
			public static implicit operator TerminalGpsInfoDataCol(TerminalGpsInfoDataColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TerminalGpsInfoDataColWCFPacket(TerminalGpsInfoDataCol collection)
			{
				return new TerminalGpsInfoDataColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TerminalGpsInfoDataQ : esTerminalGpsInfoDataQ
	{
		public TerminalGpsInfoDataQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TerminalGpsInfoDataQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TerminalGpsInfoDataQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new TerminalGpsInfoDataQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TerminalGpsInfoDataQ query)
		{
			return TerminalGpsInfoDataQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TerminalGpsInfoDataQ(string query)
		{
			return (TerminalGpsInfoDataQ)TerminalGpsInfoDataQ.SerializeHelper.FromXml(query, typeof(TerminalGpsInfoDataQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTerminalGpsInfoData : EntityBase
	{
		public esTerminalGpsInfoData()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			TerminalGpsInfoDataQ query = new TerminalGpsInfoDataQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal Id
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalGpsInfoDataMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal TerminalID
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalGpsInfoDataMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.VehiculoMatricula
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoMatricula
		{
			get
			{
				return base.GetSystemStringRequired(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.VehiculoMatricula);
				}
			}
		}		
		
		/// <summary>
		/// Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CentroTrabajoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.CentroTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.AreaTrabajoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreaTrabajoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.EmpresaID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpresaID
		{
			get
			{
				return base.GetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.EmpresaID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.EmpresaID, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.EmpresaID);
				}
			}
		}		
		
		/// <summary>
		/// Conductor Logado actualmente en el Vehiculo.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PersonalID
		{
			get
			{
				return base.GetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.PersonalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.PersonalID, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.PersonalID);
				}
			}
		}		
		
		/// <summary>
		/// Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte TerminalEstado
		{
			get
			{
				return base.GetSystemByteRequired(TerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.TerminalEstado);
				}
			}
		}		
		
		/// <summary>
		/// Nº de Secuencia de la Trama, que envio este registro de Posicionamiento.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String NumSec
		{
			get
			{
				return base.GetSystemStringRequired(TerminalGpsInfoDataMetadata.ColumnNames.NumSec);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.NumSec, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.NumSec);
				}
			}
		}		
		
		/// <summary>
		/// Tipo de Origen que genero el Registro. 0- Trama de Localizacion. 1- Servicio Cambio de Estado. 2- Vehiculo/Terminal Cambio Estado.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte OrigenTipo
		{
			get
			{
				return base.GetSystemByteRequired(TerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.OrigenTipo);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Servicio. Obtenido de GITS.dbo.Viajes.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ServicioID
		{
			get
			{
				return base.GetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.ServicioID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.ServicioID, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.ServicioID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.ServicioEstado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16 ServicioEstado
		{
			get
			{
				return base.GetSystemInt16Required(TerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado);
			}
			
			set
			{
				if(base.SetSystemInt16(TerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.ServicioEstado);
				}
			}
		}		
		
		/// <summary>
		/// Fecha en la cual el Servidor obtuvo la informacion y la Inserto en la BD. La fecha de la Posicion obtendia es GpsFecha.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaServerGet
		{
			get
			{
				return base.GetSystemDateTime(TerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.FechaServerGet);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la posicion GPS tomada por el Terminal. Si la posicion es Invalida, está fecha sera la del Terminal. El Terminal se sincroniza con el Reloj de Terminal, cada intervalo, definido en el Terminal. 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? GpsFecha
		{
			get
			{
				return base.GetSystemDateTime(TerminalGpsInfoDataMetadata.ColumnNames.GpsFecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalGpsInfoDataMetadata.ColumnNames.GpsFecha, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsFecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.GpsLatitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLatitud
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsLatitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.GpsLongitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLongitud
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsLongitud);
				}
			}
		}		
		
		/// <summary>
		/// Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte GpsFixMode
		{
			get
			{
				return base.GetSystemByteRequired(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsFixMode);
				}
			}
		}		
		
		/// <summary>
		/// Georefencia textual de la posicion obtenida en GPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GpsFixGeoTx
		{
			get
			{
				return base.GetSystemStringRequired(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTx);
				}
			}
		}		
		
		/// <summary>
		/// 0:No concocido 1: Autovia 2:Nacional 3:Comarcal 4:Calle 5:Plaza
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte GpsFixGeoTipoVia
		{
			get
			{
				return base.GetSystemByteRequired(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTipoVia);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.GpsNSAT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte GpsNSAT
		{
			get
			{
				return base.GetSystemByteRequired(TerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsNSAT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.GpsAltitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16 GpsAltitud
		{
			get
			{
				return base.GetSystemInt16Required(TerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud);
			}
			
			set
			{
				if(base.SetSystemInt16(TerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsAltitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.GpsRumbo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal GpsRumbo
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsRumbo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.GpsVelocidad
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 GpsVelocidad
		{
			get
			{
				return base.GetSystemInt32Required(TerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsVelocidad);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.GpsDistancia
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsDistancia
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsDistancia);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Total
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double OdometroTt
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalGpsInfoDataMetadata.ColumnNames.OdometroTt);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalGpsInfoDataMetadata.ColumnNames.OdometroTt, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.OdometroTt);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Parcial
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double OdometroPc
		{
			get
			{
				return base.GetSystemDoubleRequired(TerminalGpsInfoDataMetadata.ColumnNames.OdometroPc);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalGpsInfoDataMetadata.ColumnNames.OdometroPc, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.OdometroPc);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de entrada del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String InputState
		{
			get
			{
				return base.GetSystemStringRequired(TerminalGpsInfoDataMetadata.ColumnNames.InputState);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.InputState, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.InputState);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de salida del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String OutputState
		{
			get
			{
				return base.GetSystemStringRequired(TerminalGpsInfoDataMetadata.ColumnNames.OutputState);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.OutputState, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.OutputState);
				}
			}
		}		
		
		/// <summary>
		/// Servidor que gestiona la conexión de este Terminal. Esta vinculado a la conexion. Si estuviera desconectado,  este valor seria null.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? ServerID
		{
			get
			{
				return base.GetSystemByte(TerminalGpsInfoDataMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalGpsInfoDataMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.TryProcessed
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte TryProcessed
		{
			get
			{
				return base.GetSystemByteRequired(TerminalGpsInfoDataMetadata.ColumnNames.TryProcessed);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalGpsInfoDataMetadata.ColumnNames.TryProcessed, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.TryProcessed);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.GpsPoblacion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GpsPoblacion
		{
			get
			{
				return base.GetSystemStringRequired(TerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsPoblacion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.VehiculoCodigoADM
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigoADM
		{
			get
			{
				return base.GetSystemStringRequired(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.VehiculoCodigoADM);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.VehiculoCodigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigo
		{
			get
			{
				return base.GetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.VehiculoCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.AreaTrabajoColor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreaTrabajoColor
		{
			get
			{
				return base.GetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoColor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.EmpresaColor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpresaColor
		{
			get
			{
				return base.GetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.EmpresaColor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.Revisar
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean Revisar
		{
			get
			{
				return base.GetSystemBooleanRequired(TerminalGpsInfoDataMetadata.ColumnNames.Revisar);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalGpsInfoDataMetadata.ColumnNames.Revisar, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.Revisar);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.RevisarMotivo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String RevisarMotivo
		{
			get
			{
				return base.GetSystemStringRequired(TerminalGpsInfoDataMetadata.ColumnNames.RevisarMotivo);
			}
			
			set
			{
				if(base.SetSystemString(TerminalGpsInfoDataMetadata.ColumnNames.RevisarMotivo, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.RevisarMotivo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.NotificadoSIG
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal NotificadoSIG
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalGpsInfoDataMetadata.ColumnNames.NotificadoSIG);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.NotificadoSIG, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.NotificadoSIG);
				}
			}
		}		
		
		/// <summary>
		/// Indica si el Terminal esta en estado Especial: 1:Comida  2:Mantenimiento
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal EstadoEspecial
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.EstadoEspecial);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalGpsInfoData.TipoActividadID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TipoActividadID
		{
			get
			{
				return base.GetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID, value))
				{
					OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.TipoActividadID);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "VehiculoMatricula": this.str().VehiculoMatricula = (string)value; break;							
						case "CentroTrabajoID": this.str().CentroTrabajoID = (string)value; break;							
						case "AreaTrabajoID": this.str().AreaTrabajoID = (string)value; break;							
						case "EmpresaID": this.str().EmpresaID = (string)value; break;							
						case "PersonalID": this.str().PersonalID = (string)value; break;							
						case "TerminalEstado": this.str().TerminalEstado = (string)value; break;							
						case "NumSec": this.str().NumSec = (string)value; break;							
						case "OrigenTipo": this.str().OrigenTipo = (string)value; break;							
						case "ServicioID": this.str().ServicioID = (string)value; break;							
						case "ServicioEstado": this.str().ServicioEstado = (string)value; break;							
						case "FechaServerGet": this.str().FechaServerGet = (string)value; break;							
						case "GpsFecha": this.str().GpsFecha = (string)value; break;							
						case "GpsLatitud": this.str().GpsLatitud = (string)value; break;							
						case "GpsLongitud": this.str().GpsLongitud = (string)value; break;							
						case "GpsFixMode": this.str().GpsFixMode = (string)value; break;							
						case "GpsFixGeoTx": this.str().GpsFixGeoTx = (string)value; break;							
						case "GpsFixGeoTipoVia": this.str().GpsFixGeoTipoVia = (string)value; break;							
						case "GpsNSAT": this.str().GpsNSAT = (string)value; break;							
						case "GpsAltitud": this.str().GpsAltitud = (string)value; break;							
						case "GpsRumbo": this.str().GpsRumbo = (string)value; break;							
						case "GpsVelocidad": this.str().GpsVelocidad = (string)value; break;							
						case "GpsDistancia": this.str().GpsDistancia = (string)value; break;							
						case "OdometroTt": this.str().OdometroTt = (string)value; break;							
						case "OdometroPc": this.str().OdometroPc = (string)value; break;							
						case "InputState": this.str().InputState = (string)value; break;							
						case "OutputState": this.str().OutputState = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;							
						case "TryProcessed": this.str().TryProcessed = (string)value; break;							
						case "GpsPoblacion": this.str().GpsPoblacion = (string)value; break;							
						case "VehiculoCodigoADM": this.str().VehiculoCodigoADM = (string)value; break;							
						case "VehiculoCodigo": this.str().VehiculoCodigo = (string)value; break;							
						case "AreaTrabajoColor": this.str().AreaTrabajoColor = (string)value; break;							
						case "EmpresaColor": this.str().EmpresaColor = (string)value; break;							
						case "Revisar": this.str().Revisar = (string)value; break;							
						case "RevisarMotivo": this.str().RevisarMotivo = (string)value; break;							
						case "NotificadoSIG": this.str().NotificadoSIG = (string)value; break;							
						case "EstadoEspecial": this.str().EstadoEspecial = (string)value; break;							
						case "TipoActividadID": this.str().TipoActividadID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.Id);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.VehiculoID);
							break;
						
						case "CentroTrabajoID":
						
							if (value == null || value is System.Decimal)
								this.CentroTrabajoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.CentroTrabajoID);
							break;
						
						case "AreaTrabajoID":
						
							if (value == null || value is System.Decimal)
								this.AreaTrabajoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoID);
							break;
						
						case "EmpresaID":
						
							if (value == null || value is System.Decimal)
								this.EmpresaID = (System.Decimal?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.EmpresaID);
							break;
						
						case "PersonalID":
						
							if (value == null || value is System.Decimal)
								this.PersonalID = (System.Decimal?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.PersonalID);
							break;
						
						case "TerminalEstado":
						
							if (value == null || value is System.Byte)
								this.TerminalEstado = (System.Byte)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.TerminalEstado);
							break;
						
						case "OrigenTipo":
						
							if (value == null || value is System.Byte)
								this.OrigenTipo = (System.Byte)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.OrigenTipo);
							break;
						
						case "ServicioID":
						
							if (value == null || value is System.Decimal)
								this.ServicioID = (System.Decimal?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.ServicioID);
							break;
						
						case "ServicioEstado":
						
							if (value == null || value is System.Int16)
								this.ServicioEstado = (System.Int16)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.ServicioEstado);
							break;
						
						case "FechaServerGet":
						
							if (value == null || value is System.DateTime)
								this.FechaServerGet = (System.DateTime?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.FechaServerGet);
							break;
						
						case "GpsFecha":
						
							if (value == null || value is System.DateTime)
								this.GpsFecha = (System.DateTime?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsFecha);
							break;
						
						case "GpsLatitud":
						
							if (value == null || value is System.Double)
								this.GpsLatitud = (System.Double)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsLatitud);
							break;
						
						case "GpsLongitud":
						
							if (value == null || value is System.Double)
								this.GpsLongitud = (System.Double)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsLongitud);
							break;
						
						case "GpsFixMode":
						
							if (value == null || value is System.Byte)
								this.GpsFixMode = (System.Byte)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsFixMode);
							break;
						
						case "GpsFixGeoTipoVia":
						
							if (value == null || value is System.Byte)
								this.GpsFixGeoTipoVia = (System.Byte)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTipoVia);
							break;
						
						case "GpsNSAT":
						
							if (value == null || value is System.Byte)
								this.GpsNSAT = (System.Byte)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsNSAT);
							break;
						
						case "GpsAltitud":
						
							if (value == null || value is System.Int16)
								this.GpsAltitud = (System.Int16)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsAltitud);
							break;
						
						case "GpsRumbo":
						
							if (value == null || value is System.Decimal)
								this.GpsRumbo = (System.Decimal)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsRumbo);
							break;
						
						case "GpsVelocidad":
						
							if (value == null || value is System.Int32)
								this.GpsVelocidad = (System.Int32)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsVelocidad);
							break;
						
						case "GpsDistancia":
						
							if (value == null || value is System.Double)
								this.GpsDistancia = (System.Double)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.GpsDistancia);
							break;
						
						case "OdometroTt":
						
							if (value == null || value is System.Double)
								this.OdometroTt = (System.Double)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.OdometroTt);
							break;
						
						case "OdometroPc":
						
							if (value == null || value is System.Double)
								this.OdometroPc = (System.Double)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.OdometroPc);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Byte)
								this.ServerID = (System.Byte?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.ServerID);
							break;
						
						case "TryProcessed":
						
							if (value == null || value is System.Byte)
								this.TryProcessed = (System.Byte)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.TryProcessed);
							break;
						
						case "AreaTrabajoColor":
						
							if (value == null || value is System.Decimal)
								this.AreaTrabajoColor = (System.Decimal?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoColor);
							break;
						
						case "EmpresaColor":
						
							if (value == null || value is System.Decimal)
								this.EmpresaColor = (System.Decimal?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.EmpresaColor);
							break;
						
						case "Revisar":
						
							if (value == null || value is System.Boolean)
								this.Revisar = (System.Boolean)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.Revisar);
							break;
						
						case "NotificadoSIG":
						
							if (value == null || value is System.Decimal)
								this.NotificadoSIG = (System.Decimal)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.NotificadoSIG);
							break;
						
						case "EstadoEspecial":
						
							if (value == null || value is System.Decimal)
								this.EstadoEspecial = (System.Decimal)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.EstadoEspecial);
							break;
						
						case "TipoActividadID":
						
							if (value == null || value is System.Decimal)
								this.TipoActividadID = (System.Decimal?)value;
								OnPropertyChanged(TerminalGpsInfoDataMetadata.PropertyNames.TipoActividadID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTerminalGpsInfoData entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToDecimal(value);
				}
			}

			public System.String TerminalID
			{
				get
				{
					return Convert.ToString(entity.TerminalID);
				}

				set
				{
					entity.TerminalID = Convert.ToDecimal(value);
				}
			}
	
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
			
			public System.String VehiculoMatricula
			{
				get
				{
					return Convert.ToString(entity.VehiculoMatricula);
				}

				set
				{
					entity.VehiculoMatricula = Convert.ToString(value);
				}
			}
	
			public System.String CentroTrabajoID
			{
				get
				{
					System.Decimal? data = entity.CentroTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CentroTrabajoID = null;
					else entity.CentroTrabajoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreaTrabajoID
			{
				get
				{
					System.Decimal? data = entity.AreaTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaTrabajoID = null;
					else entity.AreaTrabajoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpresaID
			{
				get
				{
					System.Decimal? data = entity.EmpresaID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpresaID = null;
					else entity.EmpresaID = Convert.ToDecimal(value);
				}
			}
				
			public System.String PersonalID
			{
				get
				{
					System.Decimal? data = entity.PersonalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalID = null;
					else entity.PersonalID = Convert.ToDecimal(value);
				}
			}
			
			public System.String TerminalEstado
			{
				get
				{
					return Convert.ToString(entity.TerminalEstado);
				}

				set
				{
					entity.TerminalEstado = Convert.ToByte(value);
				}
			}

			public System.String NumSec
			{
				get
				{
					return Convert.ToString(entity.NumSec);
				}

				set
				{
					entity.NumSec = Convert.ToString(value);
				}
			}

			public System.String OrigenTipo
			{
				get
				{
					return Convert.ToString(entity.OrigenTipo);
				}

				set
				{
					entity.OrigenTipo = Convert.ToByte(value);
				}
			}
	
			public System.String ServicioID
			{
				get
				{
					System.Decimal? data = entity.ServicioID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioID = null;
					else entity.ServicioID = Convert.ToDecimal(value);
				}
			}
			
			public System.String ServicioEstado
			{
				get
				{
					return Convert.ToString(entity.ServicioEstado);
				}

				set
				{
					entity.ServicioEstado = Convert.ToInt16(value);
				}
			}
	
			public System.String FechaServerGet
			{
				get
				{
					System.DateTime? data = entity.FechaServerGet;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaServerGet = null;
					else entity.FechaServerGet = Convert.ToDateTime(value);
				}
			}
				
			public System.String GpsFecha
			{
				get
				{
					System.DateTime? data = entity.GpsFecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFecha = null;
					else entity.GpsFecha = Convert.ToDateTime(value);
				}
			}
			
			public System.String GpsLatitud
			{
				get
				{
					return Convert.ToString(entity.GpsLatitud);
				}

				set
				{
					entity.GpsLatitud = Convert.ToDouble(value);
				}
			}

			public System.String GpsLongitud
			{
				get
				{
					return Convert.ToString(entity.GpsLongitud);
				}

				set
				{
					entity.GpsLongitud = Convert.ToDouble(value);
				}
			}

			public System.String GpsFixMode
			{
				get
				{
					return Convert.ToString(entity.GpsFixMode);
				}

				set
				{
					entity.GpsFixMode = Convert.ToByte(value);
				}
			}

			public System.String GpsFixGeoTx
			{
				get
				{
					return Convert.ToString(entity.GpsFixGeoTx);
				}

				set
				{
					entity.GpsFixGeoTx = Convert.ToString(value);
				}
			}

			public System.String GpsFixGeoTipoVia
			{
				get
				{
					return Convert.ToString(entity.GpsFixGeoTipoVia);
				}

				set
				{
					entity.GpsFixGeoTipoVia = Convert.ToByte(value);
				}
			}

			public System.String GpsNSAT
			{
				get
				{
					return Convert.ToString(entity.GpsNSAT);
				}

				set
				{
					entity.GpsNSAT = Convert.ToByte(value);
				}
			}

			public System.String GpsAltitud
			{
				get
				{
					return Convert.ToString(entity.GpsAltitud);
				}

				set
				{
					entity.GpsAltitud = Convert.ToInt16(value);
				}
			}

			public System.String GpsRumbo
			{
				get
				{
					return Convert.ToString(entity.GpsRumbo);
				}

				set
				{
					entity.GpsRumbo = Convert.ToDecimal(value);
				}
			}

			public System.String GpsVelocidad
			{
				get
				{
					return Convert.ToString(entity.GpsVelocidad);
				}

				set
				{
					entity.GpsVelocidad = Convert.ToInt32(value);
				}
			}

			public System.String GpsDistancia
			{
				get
				{
					return Convert.ToString(entity.GpsDistancia);
				}

				set
				{
					entity.GpsDistancia = Convert.ToDouble(value);
				}
			}

			public System.String OdometroTt
			{
				get
				{
					return Convert.ToString(entity.OdometroTt);
				}

				set
				{
					entity.OdometroTt = Convert.ToDouble(value);
				}
			}

			public System.String OdometroPc
			{
				get
				{
					return Convert.ToString(entity.OdometroPc);
				}

				set
				{
					entity.OdometroPc = Convert.ToDouble(value);
				}
			}

			public System.String InputState
			{
				get
				{
					return Convert.ToString(entity.InputState);
				}

				set
				{
					entity.InputState = Convert.ToString(value);
				}
			}

			public System.String OutputState
			{
				get
				{
					return Convert.ToString(entity.OutputState);
				}

				set
				{
					entity.OutputState = Convert.ToString(value);
				}
			}
	
			public System.String ServerID
			{
				get
				{
					System.Byte? data = entity.ServerID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServerID = null;
					else entity.ServerID = Convert.ToByte(value);
				}
			}
			
			public System.String TryProcessed
			{
				get
				{
					return Convert.ToString(entity.TryProcessed);
				}

				set
				{
					entity.TryProcessed = Convert.ToByte(value);
				}
			}

			public System.String GpsPoblacion
			{
				get
				{
					return Convert.ToString(entity.GpsPoblacion);
				}

				set
				{
					entity.GpsPoblacion = Convert.ToString(value);
				}
			}

			public System.String VehiculoCodigoADM
			{
				get
				{
					return Convert.ToString(entity.VehiculoCodigoADM);
				}

				set
				{
					entity.VehiculoCodigoADM = Convert.ToString(value);
				}
			}
	
			public System.String VehiculoCodigo
			{
				get
				{
					System.String data = entity.VehiculoCodigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoCodigo = null;
					else entity.VehiculoCodigo = Convert.ToString(value);
				}
			}
				
			public System.String AreaTrabajoColor
			{
				get
				{
					System.Decimal? data = entity.AreaTrabajoColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaTrabajoColor = null;
					else entity.AreaTrabajoColor = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpresaColor
			{
				get
				{
					System.Decimal? data = entity.EmpresaColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpresaColor = null;
					else entity.EmpresaColor = Convert.ToDecimal(value);
				}
			}
			
			public System.String Revisar
			{
				get
				{
					return Convert.ToString(entity.Revisar);
				}

				set
				{
					entity.Revisar = Convert.ToBoolean(value);
				}
			}

			public System.String RevisarMotivo
			{
				get
				{
					return Convert.ToString(entity.RevisarMotivo);
				}

				set
				{
					entity.RevisarMotivo = Convert.ToString(value);
				}
			}

			public System.String NotificadoSIG
			{
				get
				{
					return Convert.ToString(entity.NotificadoSIG);
				}

				set
				{
					entity.NotificadoSIG = Convert.ToDecimal(value);
				}
			}

			public System.String EstadoEspecial
			{
				get
				{
					return Convert.ToString(entity.EstadoEspecial);
				}

				set
				{
					entity.EstadoEspecial = Convert.ToDecimal(value);
				}
			}
	
			public System.String TipoActividadID
			{
				get
				{
					System.Decimal? data = entity.TipoActividadID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoActividadID = null;
					else entity.TipoActividadID = Convert.ToDecimal(value);
				}
			}
			

			private esTerminalGpsInfoData entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TerminalGpsInfoDataMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TerminalGpsInfoDataQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalGpsInfoDataQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalGpsInfoDataQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TerminalGpsInfoDataQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TerminalGpsInfoDataQ query;		
	}



	[Serializable]
	abstract public partial class esTerminalGpsInfoDataCol : CollectionBase<TerminalGpsInfoData>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TerminalGpsInfoDataMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TerminalGpsInfoDataCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TerminalGpsInfoDataQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalGpsInfoDataQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalGpsInfoDataQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TerminalGpsInfoDataQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TerminalGpsInfoDataQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TerminalGpsInfoDataQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TerminalGpsInfoDataQ query;
	}



	[Serializable]
	abstract public partial class esTerminalGpsInfoDataQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TerminalGpsInfoDataMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.Id,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.TerminalID,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoID,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula, esSystemType.String));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.EmpresaID,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.EmpresaID, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.PersonalID,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.PersonalID, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado, esSystemType.Byte));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.NumSec,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.NumSec, esSystemType.String));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo, esSystemType.Byte));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.ServicioID,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.ServicioID, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado, esSystemType.Int16));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet, esSystemType.DateTime));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsFecha,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsFecha, esSystemType.DateTime));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud, esSystemType.Double));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud, esSystemType.Double));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode, esSystemType.Byte));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia, esSystemType.Byte));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT, esSystemType.Byte));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud, esSystemType.Int16));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad, esSystemType.Int32));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia, esSystemType.Double));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.OdometroTt,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.OdometroTt, esSystemType.Double));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.OdometroPc,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.OdometroPc, esSystemType.Double));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.InputState,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.InputState, esSystemType.String));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.OutputState,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.OutputState, esSystemType.String));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.ServerID,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.ServerID, esSystemType.Byte));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.TryProcessed,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.TryProcessed, esSystemType.Byte));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion, esSystemType.String));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM, esSystemType.String));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo, esSystemType.String));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.Revisar,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.Revisar, esSystemType.Boolean));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.RevisarMotivo,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.RevisarMotivo, esSystemType.String));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.NotificadoSIG,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.NotificadoSIG, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial, esSystemType.Decimal));
			_queryItems.Add(TerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID,new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoMatricula
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula, esSystemType.String); }
		} 
		
		public esQueryItem CentroTrabajoID
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreaTrabajoID
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpresaID
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.EmpresaID, esSystemType.Decimal); }
		} 
		
		public esQueryItem PersonalID
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.PersonalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalEstado
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado, esSystemType.Byte); }
		} 
		
		public esQueryItem NumSec
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.NumSec, esSystemType.String); }
		} 
		
		public esQueryItem OrigenTipo
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo, esSystemType.Byte); }
		} 
		
		public esQueryItem ServicioID
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.ServicioID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServicioEstado
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado, esSystemType.Int16); }
		} 
		
		public esQueryItem FechaServerGet
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet, esSystemType.DateTime); }
		} 
		
		public esQueryItem GpsFecha
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsFecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem GpsLatitud
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsLongitud
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsFixMode
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsFixGeoTx
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String); }
		} 
		
		public esQueryItem GpsFixGeoTipoVia
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsNSAT
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsAltitud
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud, esSystemType.Int16); }
		} 
		
		public esQueryItem GpsRumbo
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal); }
		} 
		
		public esQueryItem GpsVelocidad
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad, esSystemType.Int32); }
		} 
		
		public esQueryItem GpsDistancia
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia, esSystemType.Double); }
		} 
		
		public esQueryItem OdometroTt
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.OdometroTt, esSystemType.Double); }
		} 
		
		public esQueryItem OdometroPc
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.OdometroPc, esSystemType.Double); }
		} 
		
		public esQueryItem InputState
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.InputState, esSystemType.String); }
		} 
		
		public esQueryItem OutputState
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.OutputState, esSystemType.String); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.ServerID, esSystemType.Byte); }
		} 
		
		public esQueryItem TryProcessed
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.TryProcessed, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsPoblacion
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoCodigoADM
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoCodigo
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo, esSystemType.String); }
		} 
		
		public esQueryItem AreaTrabajoColor
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpresaColor
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor, esSystemType.Decimal); }
		} 
		
		public esQueryItem Revisar
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.Revisar, esSystemType.Boolean); }
		} 
		
		public esQueryItem RevisarMotivo
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.RevisarMotivo, esSystemType.String); }
		} 
		
		public esQueryItem NotificadoSIG
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.NotificadoSIG, esSystemType.Decimal); }
		} 
		
		public esQueryItem EstadoEspecial
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial, esSystemType.Decimal); }
		} 
		
		public esQueryItem TipoActividadID
		{
			get { return new esQueryItem(this, TerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class TerminalGpsInfoData : esTerminalGpsInfoData
	{

		
		
	}
	



	[Serializable]
	public partial class TerminalGpsInfoDataMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TerminalGpsInfoDataMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.TerminalID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoID, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.VehiculoMatricula;
			c.CharacterMaxLength = 12;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.CentroTrabajoID;
			c.NumericPrecision = 18;
			c.Description = "Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.EmpresaID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.EmpresaID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.PersonalID, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.PersonalID;
			c.NumericPrecision = 18;
			c.Description = "Conductor Logado actualmente en el Vehiculo.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.TerminalEstado;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul ";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.NumSec, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.NumSec;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.Description = "Nº de Secuencia de la Trama, que envio este registro de Posicionamiento.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo, 10, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.OrigenTipo;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Tipo de Origen que genero el Registro. 0- Trama de Localizacion. 1- Servicio Cambio de Estado. 2- Vehiculo/Terminal Cambio Estado.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.ServicioID, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.ServicioID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Servicio. Obtenido de GITS.dbo.Viajes.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado, 12, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.ServicioEstado;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet, 13, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.FechaServerGet;
			c.Description = "Fecha en la cual el Servidor obtuvo la informacion y la Inserto en la BD. La fecha de la Posicion obtendia es GpsFecha.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsFecha, 14, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsFecha;
			c.Description = "Fecha de la posicion GPS tomada por el Terminal. Si la posicion es Invalida, está fecha sera la del Terminal. El Terminal se sincroniza con el Reloj de Terminal, cada intervalo, definido en el Terminal. ";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud, 15, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsLatitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud, 16, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsLongitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode, 17, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsFixMode;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTx;
			c.CharacterMaxLength = 500;
			c.HasDefault = true;
			c.Default = @"('')";
			c.Description = "Georefencia textual de la posicion obtenida en GPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTipoVia;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "0:No concocido 1: Autovia 2:Nacional 3:Comarcal 4:Calle 5:Plaza";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT, 20, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsNSAT;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud, 21, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsAltitud;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsRumbo;
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsVelocidad;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia, 24, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsDistancia;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.OdometroTt, 25, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.OdometroTt;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Valor del Odometro Total";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.OdometroPc, 26, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.OdometroPc;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Valor del Odometro Parcial";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.InputState, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.InputState;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"(0x00)";
			c.Description = "Valor hexadecimal de las Señales de entrada del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.OutputState, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.OutputState;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"(0x00)";
			c.Description = "Valor hexadecimal de las Señales de salida del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.ServerID, 29, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 3;
			c.Description = "Servidor que gestiona la conexión de este Terminal. Esta vinculado a la conexion. Si estuviera desconectado,  este valor seria null.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.TryProcessed, 30, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.TryProcessed;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.GpsPoblacion;
			c.CharacterMaxLength = 200;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.VehiculoCodigoADM;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.VehiculoCodigo;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor, 34, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoColor;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.EmpresaColor;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.Revisar, 36, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.Revisar;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.RevisarMotivo, 37, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.RevisarMotivo;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.NotificadoSIG, 38, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.NotificadoSIG;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial, 39, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.EstadoEspecial;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Indica si el Terminal esta en estado Especial: 1:Comida  2:Mantenimiento";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID, 40, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalGpsInfoDataMetadata.PropertyNames.TipoActividadID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TerminalGpsInfoDataMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string EmpresaID = "EmpresaID";
			 public const string PersonalID = "PersonalID";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string NumSec = "NumSec";
			 public const string OrigenTipo = "OrigenTipo";
			 public const string ServicioID = "ServicioID";
			 public const string ServicioEstado = "ServicioEstado";
			 public const string FechaServerGet = "FechaServerGet";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsFixGeoTipoVia = "GpsFixGeoTipoVia";
			 public const string GpsNSAT = "GpsNSAT";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string ServerID = "ServerID";
			 public const string TryProcessed = "TryProcessed";
			 public const string GpsPoblacion = "GpsPoblacion";
			 public const string VehiculoCodigoADM = "VehiculoCodigoADM";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string AreaTrabajoColor = "AreaTrabajoColor";
			 public const string EmpresaColor = "EmpresaColor";
			 public const string Revisar = "Revisar";
			 public const string RevisarMotivo = "RevisarMotivo";
			 public const string NotificadoSIG = "NotificadoSIG";
			 public const string EstadoEspecial = "EstadoEspecial";
			 public const string TipoActividadID = "TipoActividadID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string EmpresaID = "EmpresaID";
			 public const string PersonalID = "PersonalID";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string NumSec = "NumSec";
			 public const string OrigenTipo = "OrigenTipo";
			 public const string ServicioID = "ServicioID";
			 public const string ServicioEstado = "ServicioEstado";
			 public const string FechaServerGet = "FechaServerGet";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsFixGeoTipoVia = "GpsFixGeoTipoVia";
			 public const string GpsNSAT = "GpsNSAT";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string ServerID = "ServerID";
			 public const string TryProcessed = "TryProcessed";
			 public const string GpsPoblacion = "GpsPoblacion";
			 public const string VehiculoCodigoADM = "VehiculoCodigoADM";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string AreaTrabajoColor = "AreaTrabajoColor";
			 public const string EmpresaColor = "EmpresaColor";
			 public const string Revisar = "Revisar";
			 public const string RevisarMotivo = "RevisarMotivo";
			 public const string NotificadoSIG = "NotificadoSIG";
			 public const string EstadoEspecial = "EstadoEspecial";
			 public const string TipoActividadID = "TipoActividadID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TerminalGpsInfoDataMetadata))
			{
				if(TerminalGpsInfoDataMetadata.mapDelegates == null)
				{
					TerminalGpsInfoDataMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TerminalGpsInfoDataMetadata.meta == null)
				{
					TerminalGpsInfoDataMetadata.meta = new TerminalGpsInfoDataMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoMatricula", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CentroTrabajoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreaTrabajoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpresaID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PersonalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalEstado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumSec", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("OrigenTipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ServicioID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServicioEstado", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("FechaServerGet", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GpsFecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GpsLatitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsLongitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsFixMode", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsFixGeoTx", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("GpsFixGeoTipoVia", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsNSAT", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsAltitud", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("GpsRumbo", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("GpsVelocidad", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("GpsDistancia", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("OdometroTt", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("OdometroPc", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("InputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("OutputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ServerID", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TryProcessed", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsPoblacion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehiculoCodigoADM", new esTypeMap("nchar", "System.String"));
				meta.AddTypeMap("VehiculoCodigo", new esTypeMap("nchar", "System.String"));
				meta.AddTypeMap("AreaTrabajoColor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpresaColor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Revisar", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("RevisarMotivo", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("NotificadoSIG", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EstadoEspecial", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TipoActividadID", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "TerminalGpsInfoData";
				meta.Destination = "TerminalGpsInfoData";
				
				meta.spInsert = "proc_TerminalGpsInfoDataInsert";				
				meta.spUpdate = "proc_TerminalGpsInfoDataUpdate";		
				meta.spDelete = "proc_TerminalGpsInfoDataDelete";
				meta.spLoadAll = "proc_TerminalGpsInfoDataLoadAll";
				meta.spLoadByPrimaryKey = "proc_TerminalGpsInfoDataLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TerminalGpsInfoDataMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
