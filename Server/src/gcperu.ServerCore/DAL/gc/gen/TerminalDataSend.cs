
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// ALMACENA LAS PETICIONES O MENSAJES QUE HAY QUE ENVIAR A LOS TERMINALES. LA PRIORIDAD ES UN DATO, QUE SE UTILZA PARA ORDENARACION DE LOS ENVIOS. Orden: Prioridad DESC, y por Fecha ASC
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TerminalDataSend")]
	public partial class TerminalDataSend : esTerminalDataSend
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TerminalDataSend();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new TerminalDataSend();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new TerminalDataSend();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new TerminalDataSend();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TerminalDataSend Get(System.Decimal ID)
        {
            try
            {
                var e = new TerminalDataSend();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TerminalDataSend(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public TerminalDataSend()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TerminalDataSendCol")]
	public partial class TerminalDataSendCol : esTerminalDataSendCol, IEnumerable<TerminalDataSend>
	{
	
		public TerminalDataSendCol()
		{
		}

	
	
		public TerminalDataSend FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TerminalDataSend))]
		public class TerminalDataSendColWCFPacket : esCollectionWCFPacket<TerminalDataSendCol>
		{
			public static implicit operator TerminalDataSendCol(TerminalDataSendColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TerminalDataSendColWCFPacket(TerminalDataSendCol collection)
			{
				return new TerminalDataSendColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TerminalDataSendQ : esTerminalDataSendQ
	{
		public TerminalDataSendQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TerminalDataSendQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TerminalDataSendQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new TerminalDataSendQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TerminalDataSendQ query)
		{
			return TerminalDataSendQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TerminalDataSendQ(string query)
		{
			return (TerminalDataSendQ)TerminalDataSendQ.SerializeHelper.FromXml(query, typeof(TerminalDataSendQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTerminalDataSend : EntityBase
	{
		public esTerminalDataSend()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			TerminalDataSendQ query = new TerminalDataSendQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to TerminalDataSend.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal Id
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalDataSendMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalDataSendMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de creación del Registro en la Tabla
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime Fecha
		{
			get
			{
				return base.GetSystemDateTimeRequired(TerminalDataSendMetadata.ColumnNames.Fecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalDataSendMetadata.ColumnNames.Fecha, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.Fecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalDataSend.ServicioID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ServicioID
		{
			get
			{
				return base.GetSystemStringRequired(TerminalDataSendMetadata.ColumnNames.ServicioID);
			}
			
			set
			{
				if(base.SetSystemString(TerminalDataSendMetadata.ColumnNames.ServicioID, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.ServicioID);
				}
			}
		}		
		
		/// <summary>
		/// Datos que componen el servicio o la peticion a envia al Terminal . Está formateado como XML.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Data
		{
			get
			{
				return base.GetSystemStringRequired(TerminalDataSendMetadata.ColumnNames.Data);
			}
			
			set
			{
				if(base.SetSystemString(TerminalDataSendMetadata.ColumnNames.Data, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.Data);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalDataSend.Data1
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Data1
		{
			get
			{
				return base.GetSystemStringRequired(TerminalDataSendMetadata.ColumnNames.Data1);
			}
			
			set
			{
				if(base.SetSystemString(TerminalDataSendMetadata.ColumnNames.Data1, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.Data1);
				}
			}
		}		
		
		/// <summary>
		/// Codigo de la Peticion. Codificado en el Software. Ej. Alta servicio,Modificacion Servicio, Anulacion o Borrado.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 PeticionCode
		{
			get
			{
				return base.GetSystemInt32Required(TerminalDataSendMetadata.ColumnNames.PeticionCode);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalDataSendMetadata.ColumnNames.PeticionCode, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.PeticionCode);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalDataSend.TerminalID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalID
		{
			get
			{
				return base.GetSystemDecimal(TerminalDataSendMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalDataSendMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalDataSend.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalDataSendMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalDataSendMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Indica la prioridad de la Peticion. Influye en como el Servidor la procesa.  1-Normal 2-High 3-Highest 4-Critical
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte Priority
		{
			get
			{
				return base.GetSystemByteRequired(TerminalDataSendMetadata.ColumnNames.Priority);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalDataSendMetadata.ColumnNames.Priority, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.Priority);
				}
			}
		}		
		
		/// <summary>
		/// Se utiliza para marcar el registro con un información sobre su estado de Proceso. Sus valores seran definidos en codigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? FlagState
		{
			get
			{
				return base.GetSystemInt32(TerminalDataSendMetadata.ColumnNames.FlagState);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalDataSendMetadata.ColumnNames.FlagState, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.FlagState);
				}
			}
		}		
		
		/// <summary>
		/// Ultima vez que el Servidor proceso este envio.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? LastTimeProcessed
		{
			get
			{
				return base.GetSystemDateTime(TerminalDataSendMetadata.ColumnNames.LastTimeProcessed);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalDataSendMetadata.ColumnNames.LastTimeProcessed, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.LastTimeProcessed);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalDataSend.IntentosProcessed
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 IntentosProcessed
		{
			get
			{
				return base.GetSystemInt32Required(TerminalDataSendMetadata.ColumnNames.IntentosProcessed);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalDataSendMetadata.ColumnNames.IntentosProcessed, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.IntentosProcessed);
				}
			}
		}		
		
		/// <summary>
		/// Indica el nº de veces que esta trama ha sido recibida por el Terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 RecibidaPorTerminal
		{
			get
			{
				return base.GetSystemInt32Required(TerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.RecibidaPorTerminal);
				}
			}
		}		
		
		/// <summary>
		/// Información del Motivo, por lo cual esta peticion no paso al Historico. Cuando no puede pasar, el PeticionCode se transforma en (-1)
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String NoPasoHistoricoPor
		{
			get
			{
				return base.GetSystemStringRequired(TerminalDataSendMetadata.ColumnNames.NoPasoHistoricoPor);
			}
			
			set
			{
				if(base.SetSystemString(TerminalDataSendMetadata.ColumnNames.NoPasoHistoricoPor, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.NoPasoHistoricoPor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalDataSend.NoEnviados
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String NoEnviados
		{
			get
			{
				return base.GetSystemStringRequired(TerminalDataSendMetadata.ColumnNames.NoEnviados);
			}
			
			set
			{
				if(base.SetSystemString(TerminalDataSendMetadata.ColumnNames.NoEnviados, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.NoEnviados);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalDataSend.ServerID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ServerID
		{
			get
			{
				return base.GetSystemInt16(TerminalDataSendMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemInt16(TerminalDataSendMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "Fecha": this.str().Fecha = (string)value; break;							
						case "ServicioID": this.str().ServicioID = (string)value; break;							
						case "Data": this.str().Data = (string)value; break;							
						case "Data1": this.str().Data1 = (string)value; break;							
						case "PeticionCode": this.str().PeticionCode = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "Priority": this.str().Priority = (string)value; break;							
						case "FlagState": this.str().FlagState = (string)value; break;							
						case "LastTimeProcessed": this.str().LastTimeProcessed = (string)value; break;							
						case "IntentosProcessed": this.str().IntentosProcessed = (string)value; break;							
						case "RecibidaPorTerminal": this.str().RecibidaPorTerminal = (string)value; break;							
						case "NoPasoHistoricoPor": this.str().NoPasoHistoricoPor = (string)value; break;							
						case "NoEnviados": this.str().NoEnviados = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.Id);
							break;
						
						case "Fecha":
						
							if (value == null || value is System.DateTime)
								this.Fecha = (System.DateTime)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.Fecha);
							break;
						
						case "PeticionCode":
						
							if (value == null || value is System.Int32)
								this.PeticionCode = (System.Int32)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.PeticionCode);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal?)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.VehiculoID);
							break;
						
						case "Priority":
						
							if (value == null || value is System.Byte)
								this.Priority = (System.Byte)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.Priority);
							break;
						
						case "FlagState":
						
							if (value == null || value is System.Int32)
								this.FlagState = (System.Int32?)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.FlagState);
							break;
						
						case "LastTimeProcessed":
						
							if (value == null || value is System.DateTime)
								this.LastTimeProcessed = (System.DateTime?)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.LastTimeProcessed);
							break;
						
						case "IntentosProcessed":
						
							if (value == null || value is System.Int32)
								this.IntentosProcessed = (System.Int32)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.IntentosProcessed);
							break;
						
						case "RecibidaPorTerminal":
						
							if (value == null || value is System.Int32)
								this.RecibidaPorTerminal = (System.Int32)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.RecibidaPorTerminal);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Int16)
								this.ServerID = (System.Int16?)value;
								OnPropertyChanged(TerminalDataSendMetadata.PropertyNames.ServerID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTerminalDataSend entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToDecimal(value);
				}
			}

			public System.String Fecha
			{
				get
				{
					return Convert.ToString(entity.Fecha);
				}

				set
				{
					entity.Fecha = Convert.ToDateTime(value);
				}
			}

			public System.String ServicioID
			{
				get
				{
					return Convert.ToString(entity.ServicioID);
				}

				set
				{
					entity.ServicioID = Convert.ToString(value);
				}
			}

			public System.String Data
			{
				get
				{
					return Convert.ToString(entity.Data);
				}

				set
				{
					entity.Data = Convert.ToString(value);
				}
			}

			public System.String Data1
			{
				get
				{
					return Convert.ToString(entity.Data1);
				}

				set
				{
					entity.Data1 = Convert.ToString(value);
				}
			}

			public System.String PeticionCode
			{
				get
				{
					return Convert.ToString(entity.PeticionCode);
				}

				set
				{
					entity.PeticionCode = Convert.ToInt32(value);
				}
			}
	
			public System.String TerminalID
			{
				get
				{
					System.Decimal? data = entity.TerminalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalID = null;
					else entity.TerminalID = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
			
			public System.String Priority
			{
				get
				{
					return Convert.ToString(entity.Priority);
				}

				set
				{
					entity.Priority = Convert.ToByte(value);
				}
			}
	
			public System.String FlagState
			{
				get
				{
					System.Int32? data = entity.FlagState;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlagState = null;
					else entity.FlagState = Convert.ToInt32(value);
				}
			}
				
			public System.String LastTimeProcessed
			{
				get
				{
					System.DateTime? data = entity.LastTimeProcessed;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LastTimeProcessed = null;
					else entity.LastTimeProcessed = Convert.ToDateTime(value);
				}
			}
			
			public System.String IntentosProcessed
			{
				get
				{
					return Convert.ToString(entity.IntentosProcessed);
				}

				set
				{
					entity.IntentosProcessed = Convert.ToInt32(value);
				}
			}

			public System.String RecibidaPorTerminal
			{
				get
				{
					return Convert.ToString(entity.RecibidaPorTerminal);
				}

				set
				{
					entity.RecibidaPorTerminal = Convert.ToInt32(value);
				}
			}

			public System.String NoPasoHistoricoPor
			{
				get
				{
					return Convert.ToString(entity.NoPasoHistoricoPor);
				}

				set
				{
					entity.NoPasoHistoricoPor = Convert.ToString(value);
				}
			}

			public System.String NoEnviados
			{
				get
				{
					return Convert.ToString(entity.NoEnviados);
				}

				set
				{
					entity.NoEnviados = Convert.ToString(value);
				}
			}
	
			public System.String ServerID
			{
				get
				{
					System.Int16? data = entity.ServerID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServerID = null;
					else entity.ServerID = Convert.ToInt16(value);
				}
			}
			

			private esTerminalDataSend entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TerminalDataSendMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TerminalDataSendQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalDataSendQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalDataSendQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TerminalDataSendQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TerminalDataSendQ query;		
	}



	[Serializable]
	abstract public partial class esTerminalDataSendCol : CollectionBase<TerminalDataSend>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TerminalDataSendMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TerminalDataSendCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TerminalDataSendQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalDataSendQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalDataSendQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TerminalDataSendQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TerminalDataSendQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TerminalDataSendQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TerminalDataSendQ query;
	}



	[Serializable]
	abstract public partial class esTerminalDataSendQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TerminalDataSendMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.Id,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.Fecha,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Fecha, esSystemType.DateTime));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.ServicioID,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.ServicioID, esSystemType.String));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.Data,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Data, esSystemType.String));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.Data1,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Data1, esSystemType.String));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.PeticionCode,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.PeticionCode, esSystemType.Int32));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.TerminalID,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.VehiculoID,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.Priority,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Priority, esSystemType.Byte));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.FlagState,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.FlagState, esSystemType.Int32));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.LastTimeProcessed,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.LastTimeProcessed, esSystemType.DateTime));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.IntentosProcessed,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.IntentosProcessed, esSystemType.Int32));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal, esSystemType.Int32));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.NoPasoHistoricoPor,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.NoPasoHistoricoPor, esSystemType.String));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.NoEnviados,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.NoEnviados, esSystemType.String));
			_queryItems.Add(TerminalDataSendMetadata.ColumnNames.ServerID,new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.ServerID, esSystemType.Int16));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem Fecha
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Fecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem ServicioID
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.ServicioID, esSystemType.String); }
		} 
		
		public esQueryItem Data
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Data, esSystemType.String); }
		} 
		
		public esQueryItem Data1
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Data1, esSystemType.String); }
		} 
		
		public esQueryItem PeticionCode
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.PeticionCode, esSystemType.Int32); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem Priority
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.Priority, esSystemType.Byte); }
		} 
		
		public esQueryItem FlagState
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.FlagState, esSystemType.Int32); }
		} 
		
		public esQueryItem LastTimeProcessed
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.LastTimeProcessed, esSystemType.DateTime); }
		} 
		
		public esQueryItem IntentosProcessed
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.IntentosProcessed, esSystemType.Int32); }
		} 
		
		public esQueryItem RecibidaPorTerminal
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal, esSystemType.Int32); }
		} 
		
		public esQueryItem NoPasoHistoricoPor
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.NoPasoHistoricoPor, esSystemType.String); }
		} 
		
		public esQueryItem NoEnviados
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.NoEnviados, esSystemType.String); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, TerminalDataSendMetadata.ColumnNames.ServerID, esSystemType.Int16); }
		} 
		
		#endregion
		
	}


	
	public partial class TerminalDataSend : esTerminalDataSend
	{

		
		
	}
	



	[Serializable]
	public partial class TerminalDataSendMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TerminalDataSendMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.Fecha, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.Fecha;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.Description = "Fecha de creación del Registro en la Tabla";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.ServicioID, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.ServicioID;
			c.CharacterMaxLength = 2000;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.Data, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.Data;
			c.CharacterMaxLength = 3000;
			c.HasDefault = true;
			c.Default = @"/****** Object:  Default [dbo].[EmptyString]    Script Date: 25/03/2015 13:04:57 ******/
CREATE DEFAULT [dbo].[EmptyString] 
AS
'';
";
			c.Description = "Datos que componen el servicio o la peticion a envia al Terminal . Está formateado como XML.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.Data1, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.Data1;
			c.CharacterMaxLength = 1000;
			c.HasDefault = true;
			c.Default = @"/****** Object:  Default [dbo].[EmptyString]    Script Date: 25/03/2015 13:04:57 ******/
CREATE DEFAULT [dbo].[EmptyString] 
AS
'';
";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.PeticionCode, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.PeticionCode;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Codigo de la Peticion. Codificado en el Software. Ej. Alta servicio,Modificacion Servicio, Anulacion o Borrado.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.TerminalID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.VehiculoID, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.Priority, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.Priority;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Indica la prioridad de la Peticion. Influye en como el Servidor la procesa.  1-Normal 2-High 3-Highest 4-Critical";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.FlagState, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.FlagState;
			c.NumericPrecision = 10;
			c.Description = "Se utiliza para marcar el registro con un información sobre su estado de Proceso. Sus valores seran definidos en codigo";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.LastTimeProcessed, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.LastTimeProcessed;
			c.Description = "Ultima vez que el Servidor proceso este envio.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.IntentosProcessed, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.IntentosProcessed;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.RecibidaPorTerminal, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.RecibidaPorTerminal;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Indica el nº de veces que esta trama ha sido recibida por el Terminal.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.NoPasoHistoricoPor, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.NoPasoHistoricoPor;
			c.CharacterMaxLength = 2000;
			c.HasDefault = true;
			c.Default = @"('')";
			c.Description = "Información del Motivo, por lo cual esta peticion no paso al Historico. Cuando no puede pasar, el PeticionCode se transforma en (-1)";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.NoEnviados, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.NoEnviados;
			c.CharacterMaxLength = 2000;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalDataSendMetadata.ColumnNames.ServerID, 15, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TerminalDataSendMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 5;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TerminalDataSendMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string Fecha = "Fecha";
			 public const string ServicioID = "ServicioID";
			 public const string Data = "Data";
			 public const string Data1 = "Data1";
			 public const string PeticionCode = "PeticionCode";
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string Priority = "Priority";
			 public const string FlagState = "FlagState";
			 public const string LastTimeProcessed = "LastTimeProcessed";
			 public const string IntentosProcessed = "IntentosProcessed";
			 public const string RecibidaPorTerminal = "RecibidaPorTerminal";
			 public const string NoPasoHistoricoPor = "NoPasoHistoricoPor";
			 public const string NoEnviados = "NoEnviados";
			 public const string ServerID = "ServerID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Fecha = "Fecha";
			 public const string ServicioID = "ServicioID";
			 public const string Data = "Data";
			 public const string Data1 = "Data1";
			 public const string PeticionCode = "PeticionCode";
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string Priority = "Priority";
			 public const string FlagState = "FlagState";
			 public const string LastTimeProcessed = "LastTimeProcessed";
			 public const string IntentosProcessed = "IntentosProcessed";
			 public const string RecibidaPorTerminal = "RecibidaPorTerminal";
			 public const string NoPasoHistoricoPor = "NoPasoHistoricoPor";
			 public const string NoEnviados = "NoEnviados";
			 public const string ServerID = "ServerID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TerminalDataSendMetadata))
			{
				if(TerminalDataSendMetadata.mapDelegates == null)
				{
					TerminalDataSendMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TerminalDataSendMetadata.meta == null)
				{
					TerminalDataSendMetadata.meta = new TerminalDataSendMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Fecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ServicioID", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data1", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PeticionCode", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Priority", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("FlagState", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("LastTimeProcessed", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IntentosProcessed", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RecibidaPorTerminal", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NoPasoHistoricoPor", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("NoEnviados", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ServerID", new esTypeMap("smallint", "System.Int16"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "TerminalDataSend";
				meta.Destination = "TerminalDataSend";
				
				meta.spInsert = "proc_TerminalDataSendInsert";				
				meta.spUpdate = "proc_TerminalDataSendUpdate";		
				meta.spDelete = "proc_TerminalDataSendDelete";
				meta.spLoadAll = "proc_TerminalDataSendLoadAll";
				meta.spLoadByPrimaryKey = "proc_TerminalDataSendLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TerminalDataSendMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
