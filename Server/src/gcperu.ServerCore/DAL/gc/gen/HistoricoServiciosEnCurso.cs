
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Encapsulates the 'HistoricoServiciosEnCurso' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("HistoricoServiciosEnCurso")]
	public partial class HistoricoServiciosEnCurso : esHistoricoServiciosEnCurso
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new HistoricoServiciosEnCurso();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal servicioCursoID)
		{
			var obj = new HistoricoServiciosEnCurso();
			obj.ServicioCursoID = servicioCursoID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal servicioCursoID, esSqlAccessType sqlAccessType)
		{
			var obj = new HistoricoServiciosEnCurso();
			obj.ServicioCursoID = servicioCursoID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal SERVICIOCURSOID)
        {
            try
            {
                var e = new HistoricoServiciosEnCurso();
                return (e.LoadByPrimaryKey(SERVICIOCURSOID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static HistoricoServiciosEnCurso Get(System.Decimal SERVICIOCURSOID)
        {
            try
            {
                var e = new HistoricoServiciosEnCurso();
                return (e.LoadByPrimaryKey(SERVICIOCURSOID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public HistoricoServiciosEnCurso(System.Decimal SERVICIOCURSOID)
        {
            this.LoadByPrimaryKey(SERVICIOCURSOID);
        }
		
		public HistoricoServiciosEnCurso()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("HistoricoServiciosEnCursoCol")]
	public partial class HistoricoServiciosEnCursoCol : esHistoricoServiciosEnCursoCol, IEnumerable<HistoricoServiciosEnCurso>
	{
	
		public HistoricoServiciosEnCursoCol()
		{
		}

	
	
		public HistoricoServiciosEnCurso FindByPrimaryKey(System.Decimal servicioCursoID)
		{
			return this.SingleOrDefault(e => e.ServicioCursoID == servicioCursoID);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(HistoricoServiciosEnCurso))]
		public class HistoricoServiciosEnCursoColWCFPacket : esCollectionWCFPacket<HistoricoServiciosEnCursoCol>
		{
			public static implicit operator HistoricoServiciosEnCursoCol(HistoricoServiciosEnCursoColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator HistoricoServiciosEnCursoColWCFPacket(HistoricoServiciosEnCursoCol collection)
			{
				return new HistoricoServiciosEnCursoColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class HistoricoServiciosEnCursoQ : esHistoricoServiciosEnCursoQ
	{
		public HistoricoServiciosEnCursoQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public HistoricoServiciosEnCursoQ()
		{
		}

		override protected string GetQueryName()
		{
			return "HistoricoServiciosEnCursoQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new HistoricoServiciosEnCursoQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(HistoricoServiciosEnCursoQ query)
		{
			return HistoricoServiciosEnCursoQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator HistoricoServiciosEnCursoQ(string query)
		{
			return (HistoricoServiciosEnCursoQ)HistoricoServiciosEnCursoQ.SerializeHelper.FromXml(query, typeof(HistoricoServiciosEnCursoQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esHistoricoServiciosEnCurso : EntityBase
	{
		public esHistoricoServiciosEnCurso()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal servicioCursoID)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(servicioCursoID);
			else
				return LoadByPrimaryKeyStoredProcedure(servicioCursoID);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal servicioCursoID)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(servicioCursoID);
			else
				return LoadByPrimaryKeyStoredProcedure(servicioCursoID);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal servicioCursoID)
		{
			HistoricoServiciosEnCursoQ query = new HistoricoServiciosEnCursoQ();
			query.Where(query.ServicioCursoID == servicioCursoID);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal servicioCursoID)
		{
			esParameters parms = new esParameters();
			parms.Add("ServicioCursoID", servicioCursoID);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// ID del Servicio respecto al SERVIDOR, mientras que ServicioID es respecto a GITS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal ServicioCursoID
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioCursoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioCursoID, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.ServicioCursoID);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Servicio en GITS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal ServicioID
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioID, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.ServicioID);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Terminal, para el cual va destinado este Servicio, vinculado al Vehiculo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal TerminalID
		{
			get
			{
				return base.GetSystemDecimalRequired(HistoricoServiciosEnCursoMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosEnCursoMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Indica el estado del Servicio. Se realizan por pesos, siendo mayor miestras mas avanzado en su progreso este el Servicio.  99 - Especial. Indica que está pendiente de Anulacion.  1 - Asignado. Estado incial.  2 - Confirmado Terminal. (C para GITS)  3 - Confirmado Conductor (CC para GITS)  4 - En Curso. (E para GITS)  5 - Finalizado (R para GITS)
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte Estado
		{
			get
			{
				return base.GetSystemByteRequired(HistoricoServiciosEnCursoMetadata.ColumnNames.Estado);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoServiciosEnCursoMetadata.ColumnNames.Estado, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.Estado);
				}
			}
		}		
		
		/// <summary>
		/// Fecha servicio entra en esta tabla, enviada por el GITS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaAlta
		{
			get
			{
				return base.GetSystemDateTime(HistoricoServiciosEnCursoMetadata.ColumnNames.FechaAlta);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoServiciosEnCursoMetadata.ColumnNames.FechaAlta, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.FechaAlta);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Vehiculo, asociado al Servicio
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoServiciosEnCursoMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosEnCursoMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Personal vinculado al Servicio (NO CONDUCTOR LOGADO)
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PersonalID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoServiciosEnCursoMetadata.ColumnNames.PersonalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosEnCursoMetadata.ColumnNames.PersonalID, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.PersonalID);
				}
			}
		}		
		
		/// <summary>
		/// Si tiene valor incida, el ID del Servidor que está gestionando este Servicio, si está null, el Servicio aun no se ha enviado, esta en la tabla [TerminalDataSend]
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? ServerID
		{
			get
			{
				return base.GetSystemByte(HistoricoServiciosEnCursoMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemByte(HistoricoServiciosEnCursoMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		/// <summary>
		/// Fecha en la entra en el Historico
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime FechaRegEntry
		{
			get
			{
				return base.GetSystemDateTimeRequired(HistoricoServiciosEnCursoMetadata.ColumnNames.FechaRegEntry);
			}
			
			set
			{
				if(base.SetSystemDateTime(HistoricoServiciosEnCursoMetadata.ColumnNames.FechaRegEntry, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.FechaRegEntry);
				}
			}
		}		
		
		/// <summary>
		/// Identificador de la Trama, que creo este Servicio.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String NumSec
		{
			get
			{
				return base.GetSystemStringRequired(HistoricoServiciosEnCursoMetadata.ColumnNames.NumSec);
			}
			
			set
			{
				if(base.SetSystemString(HistoricoServiciosEnCursoMetadata.ColumnNames.NumSec, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.NumSec);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HistoricoServiciosEnCurso.ServicioTempID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ServicioTempID
		{
			get
			{
				return base.GetSystemDecimal(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioTempID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioTempID, value))
				{
					OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.ServicioTempID);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "ServicioCursoID": this.str().ServicioCursoID = (string)value; break;							
						case "ServicioID": this.str().ServicioID = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "Estado": this.str().Estado = (string)value; break;							
						case "FechaAlta": this.str().FechaAlta = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "PersonalID": this.str().PersonalID = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;							
						case "FechaRegEntry": this.str().FechaRegEntry = (string)value; break;							
						case "NumSec": this.str().NumSec = (string)value; break;							
						case "ServicioTempID": this.str().ServicioTempID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "ServicioCursoID":
						
							if (value == null || value is System.Decimal)
								this.ServicioCursoID = (System.Decimal)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.ServicioCursoID);
							break;
						
						case "ServicioID":
						
							if (value == null || value is System.Decimal)
								this.ServicioID = (System.Decimal)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.ServicioID);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.TerminalID);
							break;
						
						case "Estado":
						
							if (value == null || value is System.Byte)
								this.Estado = (System.Byte)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.Estado);
							break;
						
						case "FechaAlta":
						
							if (value == null || value is System.DateTime)
								this.FechaAlta = (System.DateTime?)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.FechaAlta);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.VehiculoID);
							break;
						
						case "PersonalID":
						
							if (value == null || value is System.Decimal)
								this.PersonalID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.PersonalID);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Byte)
								this.ServerID = (System.Byte?)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.ServerID);
							break;
						
						case "FechaRegEntry":
						
							if (value == null || value is System.DateTime)
								this.FechaRegEntry = (System.DateTime)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.FechaRegEntry);
							break;
						
						case "ServicioTempID":
						
							if (value == null || value is System.Decimal)
								this.ServicioTempID = (System.Decimal?)value;
								OnPropertyChanged(HistoricoServiciosEnCursoMetadata.PropertyNames.ServicioTempID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esHistoricoServiciosEnCurso entity)
			{
				this.entity = entity;
			}
			

			public System.String ServicioCursoID
			{
				get
				{
					return Convert.ToString(entity.ServicioCursoID);
				}

				set
				{
					entity.ServicioCursoID = Convert.ToDecimal(value);
				}
			}

			public System.String ServicioID
			{
				get
				{
					return Convert.ToString(entity.ServicioID);
				}

				set
				{
					entity.ServicioID = Convert.ToDecimal(value);
				}
			}

			public System.String TerminalID
			{
				get
				{
					return Convert.ToString(entity.TerminalID);
				}

				set
				{
					entity.TerminalID = Convert.ToDecimal(value);
				}
			}

			public System.String Estado
			{
				get
				{
					return Convert.ToString(entity.Estado);
				}

				set
				{
					entity.Estado = Convert.ToByte(value);
				}
			}
	
			public System.String FechaAlta
			{
				get
				{
					System.DateTime? data = entity.FechaAlta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaAlta = null;
					else entity.FechaAlta = Convert.ToDateTime(value);
				}
			}
				
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String PersonalID
			{
				get
				{
					System.Decimal? data = entity.PersonalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalID = null;
					else entity.PersonalID = Convert.ToDecimal(value);
				}
			}
				
			public System.String ServerID
			{
				get
				{
					System.Byte? data = entity.ServerID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServerID = null;
					else entity.ServerID = Convert.ToByte(value);
				}
			}
			
			public System.String FechaRegEntry
			{
				get
				{
					return Convert.ToString(entity.FechaRegEntry);
				}

				set
				{
					entity.FechaRegEntry = Convert.ToDateTime(value);
				}
			}

			public System.String NumSec
			{
				get
				{
					return Convert.ToString(entity.NumSec);
				}

				set
				{
					entity.NumSec = Convert.ToString(value);
				}
			}
	
			public System.String ServicioTempID
			{
				get
				{
					System.Decimal? data = entity.ServicioTempID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioTempID = null;
					else entity.ServicioTempID = Convert.ToDecimal(value);
				}
			}
			

			private esHistoricoServiciosEnCurso entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return HistoricoServiciosEnCursoMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public HistoricoServiciosEnCursoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoServiciosEnCursoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoServiciosEnCursoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(HistoricoServiciosEnCursoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private HistoricoServiciosEnCursoQ query;		
	}



	[Serializable]
	abstract public partial class esHistoricoServiciosEnCursoCol : CollectionBase<HistoricoServiciosEnCurso>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoServiciosEnCursoMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "HistoricoServiciosEnCursoCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public HistoricoServiciosEnCursoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoServiciosEnCursoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HistoricoServiciosEnCursoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoServiciosEnCursoQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(HistoricoServiciosEnCursoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((HistoricoServiciosEnCursoQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private HistoricoServiciosEnCursoQ query;
	}



	[Serializable]
	abstract public partial class esHistoricoServiciosEnCursoQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoServiciosEnCursoMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioCursoID,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioCursoID, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioID,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioID, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.TerminalID,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.Estado,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.Estado, esSystemType.Byte));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.FechaAlta,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.FechaAlta, esSystemType.DateTime));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.VehiculoID,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.PersonalID,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.PersonalID, esSystemType.Decimal));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.ServerID,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.ServerID, esSystemType.Byte));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.FechaRegEntry,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.FechaRegEntry, esSystemType.DateTime));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.NumSec,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.NumSec, esSystemType.String));
			_queryItems.Add(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioTempID,new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioTempID, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem ServicioCursoID
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioCursoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServicioID
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem Estado
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.Estado, esSystemType.Byte); }
		} 
		
		public esQueryItem FechaAlta
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.FechaAlta, esSystemType.DateTime); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem PersonalID
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.PersonalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.ServerID, esSystemType.Byte); }
		} 
		
		public esQueryItem FechaRegEntry
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.FechaRegEntry, esSystemType.DateTime); }
		} 
		
		public esQueryItem NumSec
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.NumSec, esSystemType.String); }
		} 
		
		public esQueryItem ServicioTempID
		{
			get { return new esQueryItem(this, HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioTempID, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class HistoricoServiciosEnCurso : esHistoricoServiciosEnCurso
	{

		
		
	}
	



	[Serializable]
	public partial class HistoricoServiciosEnCursoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HistoricoServiciosEnCursoMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioCursoID, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.ServicioCursoID;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			c.Description = "ID del Servicio respecto al SERVIDOR, mientras que ServicioID es respecto a GITS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.ServicioID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Servicio en GITS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.TerminalID, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Terminal, para el cual va destinado este Servicio, vinculado al Vehiculo";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.Estado, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.Estado;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Indica el estado del Servicio. Se realizan por pesos, siendo mayor miestras mas avanzado en su progreso este el Servicio.  99 - Especial. Indica que está pendiente de Anulacion.  1 - Asignado. Estado incial.  2 - Confirmado Terminal. (C para GITS)  3 - Confirmado Conductor (CC para GITS)  4 - En Curso. (E para GITS)  5 - Finalizado (R para GITS)";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.FechaAlta, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.FechaAlta;
			c.Description = "Fecha servicio entra en esta tabla, enviada por el GITS";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.VehiculoID, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Vehiculo, asociado al Servicio";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.PersonalID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.PersonalID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Personal vinculado al Servicio (NO CONDUCTOR LOGADO)";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.ServerID, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 3;
			c.Description = "Si tiene valor incida, el ID del Servidor que está gestionando este Servicio, si está null, el Servicio aun no se ha enviado, esta en la tabla [TerminalDataSend]";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.FechaRegEntry, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.FechaRegEntry;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.Description = "Fecha en la entra en el Historico";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.NumSec, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.NumSec;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.Description = "Identificador de la Trama, que creo este Servicio.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HistoricoServiciosEnCursoMetadata.ColumnNames.ServicioTempID, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoServiciosEnCursoMetadata.PropertyNames.ServicioTempID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public HistoricoServiciosEnCursoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string ServicioCursoID = "ServicioCursoID";
			 public const string ServicioID = "ServicioID";
			 public const string TerminalID = "TerminalID";
			 public const string Estado = "Estado";
			 public const string FechaAlta = "FechaAlta";
			 public const string VehiculoID = "VehiculoID";
			 public const string PersonalID = "PersonalID";
			 public const string ServerID = "ServerID";
			 public const string FechaRegEntry = "FechaRegEntry";
			 public const string NumSec = "NumSec";
			 public const string ServicioTempID = "ServicioTempID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string ServicioCursoID = "ServicioCursoID";
			 public const string ServicioID = "ServicioID";
			 public const string TerminalID = "TerminalID";
			 public const string Estado = "Estado";
			 public const string FechaAlta = "FechaAlta";
			 public const string VehiculoID = "VehiculoID";
			 public const string PersonalID = "PersonalID";
			 public const string ServerID = "ServerID";
			 public const string FechaRegEntry = "FechaRegEntry";
			 public const string NumSec = "NumSec";
			 public const string ServicioTempID = "ServicioTempID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HistoricoServiciosEnCursoMetadata))
			{
				if(HistoricoServiciosEnCursoMetadata.mapDelegates == null)
				{
					HistoricoServiciosEnCursoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HistoricoServiciosEnCursoMetadata.meta == null)
				{
					HistoricoServiciosEnCursoMetadata.meta = new HistoricoServiciosEnCursoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("ServicioCursoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServicioID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Estado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("FechaAlta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PersonalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServerID", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("FechaRegEntry", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("NumSec", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ServicioTempID", new esTypeMap("decimal", "System.Decimal"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "HistoricoServiciosEnCurso";
				meta.Destination = "HistoricoServiciosEnCurso";
				
				meta.spInsert = "proc_HistoricoServiciosEnCursoInsert";				
				meta.spUpdate = "proc_HistoricoServiciosEnCursoUpdate";		
				meta.spDelete = "proc_HistoricoServiciosEnCursoDelete";
				meta.spLoadAll = "proc_HistoricoServiciosEnCursoLoadAll";
				meta.spLoadByPrimaryKey = "proc_HistoricoServiciosEnCursoLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HistoricoServiciosEnCursoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
