
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Guarda las incidencias emitidas por el Sistema. 
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("Incidencia")]
	public partial class Incidencia : esIncidencia
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new Incidencia();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Int32 id)
		{
			var obj = new Incidencia();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Int32 id, esSqlAccessType sqlAccessType)
		{
			var obj = new Incidencia();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Int32 ID)
        {
            try
            {
                var e = new Incidencia();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static Incidencia Get(System.Int32 ID)
        {
            try
            {
                var e = new Incidencia();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public Incidencia(System.Int32 ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public Incidencia()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("IncidenciaCol")]
	public partial class IncidenciaCol : esIncidenciaCol, IEnumerable<Incidencia>
	{
	
		public IncidenciaCol()
		{
		}

	
	
		public Incidencia FindByPrimaryKey(System.Int32 id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(Incidencia))]
		public class IncidenciaColWCFPacket : esCollectionWCFPacket<IncidenciaCol>
		{
			public static implicit operator IncidenciaCol(IncidenciaColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator IncidenciaColWCFPacket(IncidenciaCol collection)
			{
				return new IncidenciaColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class IncidenciaQ : esIncidenciaQ
	{
		public IncidenciaQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public IncidenciaQ()
		{
		}

		override protected string GetQueryName()
		{
			return "IncidenciaQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new IncidenciaQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(IncidenciaQ query)
		{
			return IncidenciaQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator IncidenciaQ(string query)
		{
			return (IncidenciaQ)IncidenciaQ.SerializeHelper.FromXml(query, typeof(IncidenciaQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esIncidencia : EntityBase
	{
		public esIncidencia()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 id)
		{
			IncidenciaQ query = new IncidenciaQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to Incidencia.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 Id
		{
			get
			{
				return base.GetSystemInt32Required(IncidenciaMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemInt32(IncidenciaMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(IncidenciaMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Incidencia.Codigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Codigo
		{
			get
			{
				return base.GetSystemStringRequired(IncidenciaMetadata.ColumnNames.Codigo);
			}
			
			set
			{
				if(base.SetSystemString(IncidenciaMetadata.ColumnNames.Codigo, value))
				{
					OnPropertyChanged(IncidenciaMetadata.PropertyNames.Codigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Incidencia.Descripcion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Descripcion
		{
			get
			{
				return base.GetSystemStringRequired(IncidenciaMetadata.ColumnNames.Descripcion);
			}
			
			set
			{
				if(base.SetSystemString(IncidenciaMetadata.ColumnNames.Descripcion, value))
				{
					OnPropertyChanged(IncidenciaMetadata.PropertyNames.Descripcion);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Incidencia.FechaEmision
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime FechaEmision
		{
			get
			{
				return base.GetSystemDateTimeRequired(IncidenciaMetadata.ColumnNames.FechaEmision);
			}
			
			set
			{
				if(base.SetSystemDateTime(IncidenciaMetadata.ColumnNames.FechaEmision, value))
				{
					OnPropertyChanged(IncidenciaMetadata.PropertyNames.FechaEmision);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Incidencia.FechaRead
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaRead
		{
			get
			{
				return base.GetSystemDateTime(IncidenciaMetadata.ColumnNames.FechaRead);
			}
			
			set
			{
				if(base.SetSystemDateTime(IncidenciaMetadata.ColumnNames.FechaRead, value))
				{
					OnPropertyChanged(IncidenciaMetadata.PropertyNames.FechaRead);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Incidencia.IncidenciaTipo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? IncidenciaTipo
		{
			get
			{
				return base.GetSystemInt32(IncidenciaMetadata.ColumnNames.IncidenciaTipo);
			}
			
			set
			{
				if(base.SetSystemInt32(IncidenciaMetadata.ColumnNames.IncidenciaTipo, value))
				{
					OnPropertyChanged(IncidenciaMetadata.PropertyNames.IncidenciaTipo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Incidencia.ServicioGitsID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ServicioGitsID
		{
			get
			{
				return base.GetSystemDecimal(IncidenciaMetadata.ColumnNames.ServicioGitsID);
			}
			
			set
			{
				if(base.SetSystemDecimal(IncidenciaMetadata.ColumnNames.ServicioGitsID, value))
				{
					OnPropertyChanged(IncidenciaMetadata.PropertyNames.ServicioGitsID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Incidencia.TerminalID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalID
		{
			get
			{
				return base.GetSystemDecimal(IncidenciaMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(IncidenciaMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(IncidenciaMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to Incidencia.ServerID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? ServerID
		{
			get
			{
				return base.GetSystemInt32(IncidenciaMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemInt32(IncidenciaMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(IncidenciaMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "Codigo": this.str().Codigo = (string)value; break;							
						case "Descripcion": this.str().Descripcion = (string)value; break;							
						case "FechaEmision": this.str().FechaEmision = (string)value; break;							
						case "FechaRead": this.str().FechaRead = (string)value; break;							
						case "IncidenciaTipo": this.str().IncidenciaTipo = (string)value; break;							
						case "ServicioGitsID": this.str().ServicioGitsID = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Int32)
								this.Id = (System.Int32)value;
								OnPropertyChanged(IncidenciaMetadata.PropertyNames.Id);
							break;
						
						case "FechaEmision":
						
							if (value == null || value is System.DateTime)
								this.FechaEmision = (System.DateTime)value;
								OnPropertyChanged(IncidenciaMetadata.PropertyNames.FechaEmision);
							break;
						
						case "FechaRead":
						
							if (value == null || value is System.DateTime)
								this.FechaRead = (System.DateTime?)value;
								OnPropertyChanged(IncidenciaMetadata.PropertyNames.FechaRead);
							break;
						
						case "IncidenciaTipo":
						
							if (value == null || value is System.Int32)
								this.IncidenciaTipo = (System.Int32?)value;
								OnPropertyChanged(IncidenciaMetadata.PropertyNames.IncidenciaTipo);
							break;
						
						case "ServicioGitsID":
						
							if (value == null || value is System.Decimal)
								this.ServicioGitsID = (System.Decimal?)value;
								OnPropertyChanged(IncidenciaMetadata.PropertyNames.ServicioGitsID);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal?)value;
								OnPropertyChanged(IncidenciaMetadata.PropertyNames.TerminalID);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Int32)
								this.ServerID = (System.Int32?)value;
								OnPropertyChanged(IncidenciaMetadata.PropertyNames.ServerID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esIncidencia entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToInt32(value);
				}
			}

			public System.String Codigo
			{
				get
				{
					return Convert.ToString(entity.Codigo);
				}

				set
				{
					entity.Codigo = Convert.ToString(value);
				}
			}

			public System.String Descripcion
			{
				get
				{
					return Convert.ToString(entity.Descripcion);
				}

				set
				{
					entity.Descripcion = Convert.ToString(value);
				}
			}

			public System.String FechaEmision
			{
				get
				{
					return Convert.ToString(entity.FechaEmision);
				}

				set
				{
					entity.FechaEmision = Convert.ToDateTime(value);
				}
			}
	
			public System.String FechaRead
			{
				get
				{
					System.DateTime? data = entity.FechaRead;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaRead = null;
					else entity.FechaRead = Convert.ToDateTime(value);
				}
			}
				
			public System.String IncidenciaTipo
			{
				get
				{
					System.Int32? data = entity.IncidenciaTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IncidenciaTipo = null;
					else entity.IncidenciaTipo = Convert.ToInt32(value);
				}
			}
				
			public System.String ServicioGitsID
			{
				get
				{
					System.Decimal? data = entity.ServicioGitsID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioGitsID = null;
					else entity.ServicioGitsID = Convert.ToDecimal(value);
				}
			}
				
			public System.String TerminalID
			{
				get
				{
					System.Decimal? data = entity.TerminalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalID = null;
					else entity.TerminalID = Convert.ToDecimal(value);
				}
			}
				
			public System.String ServerID
			{
				get
				{
					System.Int32? data = entity.ServerID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServerID = null;
					else entity.ServerID = Convert.ToInt32(value);
				}
			}
			

			private esIncidencia entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return IncidenciaMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public IncidenciaQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncidenciaQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(IncidenciaQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(IncidenciaQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private IncidenciaQ query;		
	}



	[Serializable]
	abstract public partial class esIncidenciaCol : CollectionBase<Incidencia>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IncidenciaMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "IncidenciaCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public IncidenciaQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncidenciaQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(IncidenciaQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IncidenciaQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(IncidenciaQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((IncidenciaQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private IncidenciaQ query;
	}



	[Serializable]
	abstract public partial class esIncidenciaQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return IncidenciaMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(IncidenciaMetadata.ColumnNames.Id,new esQueryItem(this, IncidenciaMetadata.ColumnNames.Id, esSystemType.Int32));
			_queryItems.Add(IncidenciaMetadata.ColumnNames.Codigo,new esQueryItem(this, IncidenciaMetadata.ColumnNames.Codigo, esSystemType.String));
			_queryItems.Add(IncidenciaMetadata.ColumnNames.Descripcion,new esQueryItem(this, IncidenciaMetadata.ColumnNames.Descripcion, esSystemType.String));
			_queryItems.Add(IncidenciaMetadata.ColumnNames.FechaEmision,new esQueryItem(this, IncidenciaMetadata.ColumnNames.FechaEmision, esSystemType.DateTime));
			_queryItems.Add(IncidenciaMetadata.ColumnNames.FechaRead,new esQueryItem(this, IncidenciaMetadata.ColumnNames.FechaRead, esSystemType.DateTime));
			_queryItems.Add(IncidenciaMetadata.ColumnNames.IncidenciaTipo,new esQueryItem(this, IncidenciaMetadata.ColumnNames.IncidenciaTipo, esSystemType.Int32));
			_queryItems.Add(IncidenciaMetadata.ColumnNames.ServicioGitsID,new esQueryItem(this, IncidenciaMetadata.ColumnNames.ServicioGitsID, esSystemType.Decimal));
			_queryItems.Add(IncidenciaMetadata.ColumnNames.TerminalID,new esQueryItem(this, IncidenciaMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(IncidenciaMetadata.ColumnNames.ServerID,new esQueryItem(this, IncidenciaMetadata.ColumnNames.ServerID, esSystemType.Int32));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, IncidenciaMetadata.ColumnNames.Id, esSystemType.Int32); }
		} 
		
		public esQueryItem Codigo
		{
			get { return new esQueryItem(this, IncidenciaMetadata.ColumnNames.Codigo, esSystemType.String); }
		} 
		
		public esQueryItem Descripcion
		{
			get { return new esQueryItem(this, IncidenciaMetadata.ColumnNames.Descripcion, esSystemType.String); }
		} 
		
		public esQueryItem FechaEmision
		{
			get { return new esQueryItem(this, IncidenciaMetadata.ColumnNames.FechaEmision, esSystemType.DateTime); }
		} 
		
		public esQueryItem FechaRead
		{
			get { return new esQueryItem(this, IncidenciaMetadata.ColumnNames.FechaRead, esSystemType.DateTime); }
		} 
		
		public esQueryItem IncidenciaTipo
		{
			get { return new esQueryItem(this, IncidenciaMetadata.ColumnNames.IncidenciaTipo, esSystemType.Int32); }
		} 
		
		public esQueryItem ServicioGitsID
		{
			get { return new esQueryItem(this, IncidenciaMetadata.ColumnNames.ServicioGitsID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, IncidenciaMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, IncidenciaMetadata.ColumnNames.ServerID, esSystemType.Int32); }
		} 
		
		#endregion
		
	}


	
	public partial class Incidencia : esIncidencia
	{

		
		
	}
	



	[Serializable]
	public partial class IncidenciaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IncidenciaMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IncidenciaMetadata.ColumnNames.Id, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IncidenciaMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 10;
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciaMetadata.ColumnNames.Codigo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = IncidenciaMetadata.PropertyNames.Codigo;
			c.CharacterMaxLength = 5;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciaMetadata.ColumnNames.Descripcion, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = IncidenciaMetadata.PropertyNames.Descripcion;
			c.CharacterMaxLength = 2000;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciaMetadata.ColumnNames.FechaEmision, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IncidenciaMetadata.PropertyNames.FechaEmision;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciaMetadata.ColumnNames.FechaRead, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IncidenciaMetadata.PropertyNames.FechaRead;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciaMetadata.ColumnNames.IncidenciaTipo, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IncidenciaMetadata.PropertyNames.IncidenciaTipo;
			c.NumericPrecision = 10;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciaMetadata.ColumnNames.ServicioGitsID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IncidenciaMetadata.PropertyNames.ServicioGitsID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciaMetadata.ColumnNames.TerminalID, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IncidenciaMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(IncidenciaMetadata.ColumnNames.ServerID, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IncidenciaMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 10;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public IncidenciaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string Codigo = "Codigo";
			 public const string Descripcion = "Descripcion";
			 public const string FechaEmision = "FechaEmision";
			 public const string FechaRead = "FechaRead";
			 public const string IncidenciaTipo = "IncidenciaTipo";
			 public const string ServicioGitsID = "ServicioGitsID";
			 public const string TerminalID = "TerminalID";
			 public const string ServerID = "ServerID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Codigo = "Codigo";
			 public const string Descripcion = "Descripcion";
			 public const string FechaEmision = "FechaEmision";
			 public const string FechaRead = "FechaRead";
			 public const string IncidenciaTipo = "IncidenciaTipo";
			 public const string ServicioGitsID = "ServicioGitsID";
			 public const string TerminalID = "TerminalID";
			 public const string ServerID = "ServerID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IncidenciaMetadata))
			{
				if(IncidenciaMetadata.mapDelegates == null)
				{
					IncidenciaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IncidenciaMetadata.meta == null)
				{
					IncidenciaMetadata.meta = new IncidenciaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Codigo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Descripcion", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FechaEmision", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FechaRead", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IncidenciaTipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ServicioGitsID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServerID", new esTypeMap("int", "System.Int32"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "Incidencia";
				meta.Destination = "Incidencia";
				
				meta.spInsert = "proc_IncidenciaInsert";				
				meta.spUpdate = "proc_IncidenciaUpdate";		
				meta.spDelete = "proc_IncidenciaDelete";
				meta.spLoadAll = "proc_IncidenciaLoadAll";
				meta.spLoadByPrimaryKey = "proc_IncidenciaLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IncidenciaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
