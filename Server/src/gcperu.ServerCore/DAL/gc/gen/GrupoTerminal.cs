
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:22
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Encapsulates the 'GrupoTerminal' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("GrupoTerminal")]
	public partial class GrupoTerminal : esGrupoTerminal
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new GrupoTerminal();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Int32 id)
		{
			var obj = new GrupoTerminal();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Int32 id, esSqlAccessType sqlAccessType)
		{
			var obj = new GrupoTerminal();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Int32 ID)
        {
            try
            {
                var e = new GrupoTerminal();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static GrupoTerminal Get(System.Int32 ID)
        {
            try
            {
                var e = new GrupoTerminal();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public GrupoTerminal(System.Int32 ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public GrupoTerminal()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("GrupoTerminalCol")]
	public partial class GrupoTerminalCol : esGrupoTerminalCol, IEnumerable<GrupoTerminal>
	{
	
		public GrupoTerminalCol()
		{
		}

	
	
		public GrupoTerminal FindByPrimaryKey(System.Int32 id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(GrupoTerminal))]
		public class GrupoTerminalColWCFPacket : esCollectionWCFPacket<GrupoTerminalCol>
		{
			public static implicit operator GrupoTerminalCol(GrupoTerminalColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator GrupoTerminalColWCFPacket(GrupoTerminalCol collection)
			{
				return new GrupoTerminalColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class GrupoTerminalQ : esGrupoTerminalQ
	{
		public GrupoTerminalQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public GrupoTerminalQ()
		{
		}

		override protected string GetQueryName()
		{
			return "GrupoTerminalQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new GrupoTerminalQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(GrupoTerminalQ query)
		{
			return GrupoTerminalQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator GrupoTerminalQ(string query)
		{
			return (GrupoTerminalQ)GrupoTerminalQ.SerializeHelper.FromXml(query, typeof(GrupoTerminalQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esGrupoTerminal : EntityBase
	{
		public esGrupoTerminal()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 id)
		{
			GrupoTerminalQ query = new GrupoTerminalQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to GrupoTerminal.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 Id
		{
			get
			{
				return base.GetSystemInt32Required(GrupoTerminalMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemInt32(GrupoTerminalMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(GrupoTerminalMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Nombre del Grupo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Nombre
		{
			get
			{
				return base.GetSystemStringRequired(GrupoTerminalMetadata.ColumnNames.Nombre);
			}
			
			set
			{
				if(base.SetSystemString(GrupoTerminalMetadata.ColumnNames.Nombre, value))
				{
					OnPropertyChanged(GrupoTerminalMetadata.PropertyNames.Nombre);
				}
			}
		}		
		
		/// <summary>
		/// Maps to GrupoTerminal.EnActivo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean EnActivo
		{
			get
			{
				return base.GetSystemBooleanRequired(GrupoTerminalMetadata.ColumnNames.EnActivo);
			}
			
			set
			{
				if(base.SetSystemBoolean(GrupoTerminalMetadata.ColumnNames.EnActivo, value))
				{
					OnPropertyChanged(GrupoTerminalMetadata.PropertyNames.EnActivo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to GrupoTerminal.xDefecto
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean XDefecto
		{
			get
			{
				return base.GetSystemBooleanRequired(GrupoTerminalMetadata.ColumnNames.XDefecto);
			}
			
			set
			{
				if(base.SetSystemBoolean(GrupoTerminalMetadata.ColumnNames.XDefecto, value))
				{
					OnPropertyChanged(GrupoTerminalMetadata.PropertyNames.XDefecto);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "Nombre": this.str().Nombre = (string)value; break;							
						case "EnActivo": this.str().EnActivo = (string)value; break;							
						case "XDefecto": this.str().XDefecto = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Int32)
								this.Id = (System.Int32)value;
								OnPropertyChanged(GrupoTerminalMetadata.PropertyNames.Id);
							break;
						
						case "EnActivo":
						
							if (value == null || value is System.Boolean)
								this.EnActivo = (System.Boolean)value;
								OnPropertyChanged(GrupoTerminalMetadata.PropertyNames.EnActivo);
							break;
						
						case "XDefecto":
						
							if (value == null || value is System.Boolean)
								this.XDefecto = (System.Boolean)value;
								OnPropertyChanged(GrupoTerminalMetadata.PropertyNames.XDefecto);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esGrupoTerminal entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToInt32(value);
				}
			}

			public System.String Nombre
			{
				get
				{
					return Convert.ToString(entity.Nombre);
				}

				set
				{
					entity.Nombre = Convert.ToString(value);
				}
			}

			public System.String EnActivo
			{
				get
				{
					return Convert.ToString(entity.EnActivo);
				}

				set
				{
					entity.EnActivo = Convert.ToBoolean(value);
				}
			}

			public System.String XDefecto
			{
				get
				{
					return Convert.ToString(entity.XDefecto);
				}

				set
				{
					entity.XDefecto = Convert.ToBoolean(value);
				}
			}


			private esGrupoTerminal entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return GrupoTerminalMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public GrupoTerminalQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoTerminalQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(GrupoTerminalQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(GrupoTerminalQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private GrupoTerminalQ query;		
	}



	[Serializable]
	abstract public partial class esGrupoTerminalCol : CollectionBase<GrupoTerminal>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GrupoTerminalMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "GrupoTerminalCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public GrupoTerminalQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoTerminalQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(GrupoTerminalQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoTerminalQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(GrupoTerminalQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((GrupoTerminalQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private GrupoTerminalQ query;
	}



	[Serializable]
	abstract public partial class esGrupoTerminalQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return GrupoTerminalMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(GrupoTerminalMetadata.ColumnNames.Id,new esQueryItem(this, GrupoTerminalMetadata.ColumnNames.Id, esSystemType.Int32));
			_queryItems.Add(GrupoTerminalMetadata.ColumnNames.Nombre,new esQueryItem(this, GrupoTerminalMetadata.ColumnNames.Nombre, esSystemType.String));
			_queryItems.Add(GrupoTerminalMetadata.ColumnNames.EnActivo,new esQueryItem(this, GrupoTerminalMetadata.ColumnNames.EnActivo, esSystemType.Boolean));
			_queryItems.Add(GrupoTerminalMetadata.ColumnNames.XDefecto,new esQueryItem(this, GrupoTerminalMetadata.ColumnNames.XDefecto, esSystemType.Boolean));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, GrupoTerminalMetadata.ColumnNames.Id, esSystemType.Int32); }
		} 
		
		public esQueryItem Nombre
		{
			get { return new esQueryItem(this, GrupoTerminalMetadata.ColumnNames.Nombre, esSystemType.String); }
		} 
		
		public esQueryItem EnActivo
		{
			get { return new esQueryItem(this, GrupoTerminalMetadata.ColumnNames.EnActivo, esSystemType.Boolean); }
		} 
		
		public esQueryItem XDefecto
		{
			get { return new esQueryItem(this, GrupoTerminalMetadata.ColumnNames.XDefecto, esSystemType.Boolean); }
		} 
		
		#endregion
		
	}


	
	public partial class GrupoTerminal : esGrupoTerminal
	{

		#region TerminalStateByGrupoID - Zero To Many
		
		static public esPrefetchMap Prefetch_TerminalStateByGrupoID
		{
			get
			{
				esPrefetchMap map = new esPrefetchMap();
				map.PrefetchDelegate = gcperu.Server.Core.DAL.GC.GrupoTerminal.TerminalStateByGrupoID_Delegate;
				map.PropertyName = "TerminalStateByGrupoID";
				map.MyColumnName = "GrupoID";
				map.ParentColumnName = "ID";
				map.IsMultiPartKey = false;
				return map;
			}
		}		
		
		static private void TerminalStateByGrupoID_Delegate(esPrefetchParameters data)
		{
			GrupoTerminalQ parent = new GrupoTerminalQ(data.NextAlias());

			TerminalStateQ me = data.You != null ? data.You as TerminalStateQ : new TerminalStateQ(data.NextAlias());

			if (data.Root == null)
			{
				data.Root = me;
			}
			
			data.Root.InnerJoin(parent).On(parent.Id == me.GrupoID);

			data.You = parent;
		}			
		
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_TerminalState_GrupoTerminal
		/// </summary>

		[XmlIgnore]
		public TerminalStateCol TerminalStateByGrupoID
		{
			get
			{
				if(this._TerminalStateByGrupoID == null)
				{
					this._TerminalStateByGrupoID = new TerminalStateCol();
					this._TerminalStateByGrupoID.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TerminalStateByGrupoID", this._TerminalStateByGrupoID);
				
					if (this.Id != null)
					{
						if (!this.es.IsLazyLoadDisabled)
						{
							this._TerminalStateByGrupoID.Query.Where(this._TerminalStateByGrupoID.Query.GrupoID == this.Id);
							this._TerminalStateByGrupoID.Query.Load();
						}

						// Auto-hookup Foreign Keys
						this._TerminalStateByGrupoID.fks.Add(TerminalStateMetadata.ColumnNames.GrupoID, this.Id);
					}
				}

				return this._TerminalStateByGrupoID;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TerminalStateByGrupoID != null) 
				{ 
					this.RemovePostSave("TerminalStateByGrupoID"); 
					this._TerminalStateByGrupoID = null;
					this.OnPropertyChanged("TerminalStateByGrupoID");
				} 
			} 			
		}
			
		
		private TerminalStateCol _TerminalStateByGrupoID;
		#endregion

		
		protected override esEntityCollectionBase CreateCollectionForPrefetch(string name)
		{
			esEntityCollectionBase coll = null;

			switch (name)
			{
				case "TerminalStateByGrupoID":
					coll = this.TerminalStateByGrupoID;
					break;	
			}

			return coll;
		}		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "TerminalStateByGrupoID", typeof(TerminalStateCol), new TerminalState()));
		
			return props;
		}
		
		/// <summary>
		/// Called by ApplyPostSaveKeys 
		/// </summary>
		/// <param name="coll">The collection to enumerate over</param>
		/// <param name="key">"The column name</param>
		/// <param name="value">The column value</param>
		private void Apply(esEntityCollectionBase coll, string key, object value)
		{
			foreach (esEntity obj in coll)
			{
				if (obj.es.IsAdded)
				{
					obj.SetColumn(key, value, false);
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._TerminalStateByGrupoID != null)
			{
				Apply(this._TerminalStateByGrupoID, "GrupoID", this.Id);
			}
		}
		
	}
	



	[Serializable]
	public partial class GrupoTerminalMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GrupoTerminalMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GrupoTerminalMetadata.ColumnNames.Id, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupoTerminalMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 10;
			m_columns.Add(c);
				
			c = new esColumnMetadata(GrupoTerminalMetadata.ColumnNames.Nombre, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = GrupoTerminalMetadata.PropertyNames.Nombre;
			c.CharacterMaxLength = 100;
			c.Description = "Nombre del Grupo";
			m_columns.Add(c);
				
			c = new esColumnMetadata(GrupoTerminalMetadata.ColumnNames.EnActivo, 2, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = GrupoTerminalMetadata.PropertyNames.EnActivo;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(GrupoTerminalMetadata.ColumnNames.XDefecto, 3, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = GrupoTerminalMetadata.PropertyNames.XDefecto;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public GrupoTerminalMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string Nombre = "Nombre";
			 public const string EnActivo = "EnActivo";
			 public const string XDefecto = "xDefecto";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Nombre = "Nombre";
			 public const string EnActivo = "EnActivo";
			 public const string XDefecto = "XDefecto";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GrupoTerminalMetadata))
			{
				if(GrupoTerminalMetadata.mapDelegates == null)
				{
					GrupoTerminalMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GrupoTerminalMetadata.meta == null)
				{
					GrupoTerminalMetadata.meta = new GrupoTerminalMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nombre", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("EnActivo", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("XDefecto", new esTypeMap("bit", "System.Boolean"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "GrupoTerminal";
				meta.Destination = "GrupoTerminal";
				
				meta.spInsert = "proc_GrupoTerminalInsert";				
				meta.spUpdate = "proc_GrupoTerminalUpdate";		
				meta.spDelete = "proc_GrupoTerminalDelete";
				meta.spLoadAll = "proc_GrupoTerminalLoadAll";
				meta.spLoadByPrimaryKey = "proc_GrupoTerminalLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GrupoTerminalMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
