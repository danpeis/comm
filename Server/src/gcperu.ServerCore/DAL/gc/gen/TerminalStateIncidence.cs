
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Encapsulates the 'TerminalStateIncidence' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TerminalStateIncidence")]
	public partial class TerminalStateIncidence : esTerminalStateIncidence
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TerminalStateIncidence();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new TerminalStateIncidence();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new TerminalStateIncidence();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new TerminalStateIncidence();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TerminalStateIncidence Get(System.Decimal ID)
        {
            try
            {
                var e = new TerminalStateIncidence();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TerminalStateIncidence(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public TerminalStateIncidence()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TerminalStateIncidenceCol")]
	public partial class TerminalStateIncidenceCol : esTerminalStateIncidenceCol, IEnumerable<TerminalStateIncidence>
	{
	
		public TerminalStateIncidenceCol()
		{
		}

	
	
		public TerminalStateIncidence FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TerminalStateIncidence))]
		public class TerminalStateIncidenceColWCFPacket : esCollectionWCFPacket<TerminalStateIncidenceCol>
		{
			public static implicit operator TerminalStateIncidenceCol(TerminalStateIncidenceColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TerminalStateIncidenceColWCFPacket(TerminalStateIncidenceCol collection)
			{
				return new TerminalStateIncidenceColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TerminalStateIncidenceQ : esTerminalStateIncidenceQ
	{
		public TerminalStateIncidenceQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TerminalStateIncidenceQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TerminalStateIncidenceQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new TerminalStateIncidenceQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TerminalStateIncidenceQ query)
		{
			return TerminalStateIncidenceQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TerminalStateIncidenceQ(string query)
		{
			return (TerminalStateIncidenceQ)TerminalStateIncidenceQ.SerializeHelper.FromXml(query, typeof(TerminalStateIncidenceQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTerminalStateIncidence : EntityBase
	{
		public esTerminalStateIncidence()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			TerminalStateIncidenceQ query = new TerminalStateIncidenceQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to TerminalStateIncidence.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal Id
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalStateIncidenceMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateIncidenceMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateIncidence.TerminalID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal TerminalID
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalStateIncidenceMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateIncidenceMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Nº de IMEI del model del Terminal. Se rellena cuando lo envia el Terminal a realizar la conexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalIMEI
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateIncidenceMetadata.ColumnNames.TerminalIMEI);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateIncidenceMetadata.ColumnNames.TerminalIMEI, value))
				{
					OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.TerminalIMEI);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateIncidence.Fecha
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime Fecha
		{
			get
			{
				return base.GetSystemDateTimeRequired(TerminalStateIncidenceMetadata.ColumnNames.Fecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateIncidenceMetadata.ColumnNames.Fecha, value))
				{
					OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.Fecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateIncidence.Tipo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 Tipo
		{
			get
			{
				return base.GetSystemInt32Required(TerminalStateIncidenceMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateIncidenceMetadata.ColumnNames.Tipo, value))
				{
					OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.Tipo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateIncidence.Descripcion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Descripcion
		{
			get
			{
				return base.GetSystemStringRequired(TerminalStateIncidenceMetadata.ColumnNames.Descripcion);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateIncidenceMetadata.ColumnNames.Descripcion, value))
				{
					OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.Descripcion);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "TerminalIMEI": this.str().TerminalIMEI = (string)value; break;							
						case "Fecha": this.str().Fecha = (string)value; break;							
						case "Tipo": this.str().Tipo = (string)value; break;							
						case "Descripcion": this.str().Descripcion = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal)value;
								OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.Id);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal)value;
								OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.TerminalID);
							break;
						
						case "Fecha":
						
							if (value == null || value is System.DateTime)
								this.Fecha = (System.DateTime)value;
								OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.Fecha);
							break;
						
						case "Tipo":
						
							if (value == null || value is System.Int32)
								this.Tipo = (System.Int32)value;
								OnPropertyChanged(TerminalStateIncidenceMetadata.PropertyNames.Tipo);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTerminalStateIncidence entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToDecimal(value);
				}
			}

			public System.String TerminalID
			{
				get
				{
					return Convert.ToString(entity.TerminalID);
				}

				set
				{
					entity.TerminalID = Convert.ToDecimal(value);
				}
			}

			public System.String TerminalIMEI
			{
				get
				{
					return Convert.ToString(entity.TerminalIMEI);
				}

				set
				{
					entity.TerminalIMEI = Convert.ToString(value);
				}
			}

			public System.String Fecha
			{
				get
				{
					return Convert.ToString(entity.Fecha);
				}

				set
				{
					entity.Fecha = Convert.ToDateTime(value);
				}
			}

			public System.String Tipo
			{
				get
				{
					return Convert.ToString(entity.Tipo);
				}

				set
				{
					entity.Tipo = Convert.ToInt32(value);
				}
			}

			public System.String Descripcion
			{
				get
				{
					return Convert.ToString(entity.Descripcion);
				}

				set
				{
					entity.Descripcion = Convert.ToString(value);
				}
			}


			private esTerminalStateIncidence entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TerminalStateIncidenceMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TerminalStateIncidenceQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalStateIncidenceQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalStateIncidenceQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TerminalStateIncidenceQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TerminalStateIncidenceQ query;		
	}



	[Serializable]
	abstract public partial class esTerminalStateIncidenceCol : CollectionBase<TerminalStateIncidence>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TerminalStateIncidenceMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TerminalStateIncidenceCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TerminalStateIncidenceQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalStateIncidenceQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalStateIncidenceQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TerminalStateIncidenceQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TerminalStateIncidenceQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TerminalStateIncidenceQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TerminalStateIncidenceQ query;
	}



	[Serializable]
	abstract public partial class esTerminalStateIncidenceQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TerminalStateIncidenceMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TerminalStateIncidenceMetadata.ColumnNames.Id,new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(TerminalStateIncidenceMetadata.ColumnNames.TerminalID,new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateIncidenceMetadata.ColumnNames.TerminalIMEI,new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.TerminalIMEI, esSystemType.String));
			_queryItems.Add(TerminalStateIncidenceMetadata.ColumnNames.Fecha,new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.Fecha, esSystemType.DateTime));
			_queryItems.Add(TerminalStateIncidenceMetadata.ColumnNames.Tipo,new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.Tipo, esSystemType.Int32));
			_queryItems.Add(TerminalStateIncidenceMetadata.ColumnNames.Descripcion,new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.Descripcion, esSystemType.String));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalIMEI
		{
			get { return new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.TerminalIMEI, esSystemType.String); }
		} 
		
		public esQueryItem Fecha
		{
			get { return new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.Fecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem Tipo
		{
			get { return new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.Tipo, esSystemType.Int32); }
		} 
		
		public esQueryItem Descripcion
		{
			get { return new esQueryItem(this, TerminalStateIncidenceMetadata.ColumnNames.Descripcion, esSystemType.String); }
		} 
		
		#endregion
		
	}


	
	public partial class TerminalStateIncidence : esTerminalStateIncidence
	{

		
		
	}
	



	[Serializable]
	public partial class TerminalStateIncidenceMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TerminalStateIncidenceMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TerminalStateIncidenceMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateIncidenceMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateIncidenceMetadata.ColumnNames.TerminalID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateIncidenceMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateIncidenceMetadata.ColumnNames.TerminalIMEI, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateIncidenceMetadata.PropertyNames.TerminalIMEI;
			c.CharacterMaxLength = 30;
			c.Description = "Nº de IMEI del model del Terminal. Se rellena cuando lo envia el Terminal a realizar la conexion.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateIncidenceMetadata.ColumnNames.Fecha, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateIncidenceMetadata.PropertyNames.Fecha;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateIncidenceMetadata.ColumnNames.Tipo, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateIncidenceMetadata.PropertyNames.Tipo;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateIncidenceMetadata.ColumnNames.Descripcion, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateIncidenceMetadata.PropertyNames.Descripcion;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TerminalStateIncidenceMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string TerminalID = "TerminalID";
			 public const string TerminalIMEI = "TerminalIMEI";
			 public const string Fecha = "Fecha";
			 public const string Tipo = "Tipo";
			 public const string Descripcion = "Descripcion";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string TerminalID = "TerminalID";
			 public const string TerminalIMEI = "TerminalIMEI";
			 public const string Fecha = "Fecha";
			 public const string Tipo = "Tipo";
			 public const string Descripcion = "Descripcion";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TerminalStateIncidenceMetadata))
			{
				if(TerminalStateIncidenceMetadata.mapDelegates == null)
				{
					TerminalStateIncidenceMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TerminalStateIncidenceMetadata.meta == null)
				{
					TerminalStateIncidenceMetadata.meta = new TerminalStateIncidenceMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalIMEI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Tipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descripcion", new esTypeMap("nvarchar", "System.String"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "TerminalStateIncidence";
				meta.Destination = "TerminalStateIncidence";
				
				meta.spInsert = "proc_TerminalStateIncidenceInsert";				
				meta.spUpdate = "proc_TerminalStateIncidenceUpdate";		
				meta.spDelete = "proc_TerminalStateIncidenceDelete";
				meta.spLoadAll = "proc_TerminalStateIncidenceLoadAll";
				meta.spLoadByPrimaryKey = "proc_TerminalStateIncidenceLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TerminalStateIncidenceMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
