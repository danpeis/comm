
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Encapsulates the 'TerminalSelectiveSoftwareUpdate' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TerminalSelectiveSoftwareUpdate")]
	public partial class TerminalSelectiveSoftwareUpdate : esTerminalSelectiveSoftwareUpdate
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TerminalSelectiveSoftwareUpdate();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal terminalID)
		{
			var obj = new TerminalSelectiveSoftwareUpdate();
			obj.TerminalID = terminalID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal terminalID, esSqlAccessType sqlAccessType)
		{
			var obj = new TerminalSelectiveSoftwareUpdate();
			obj.TerminalID = terminalID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal TERMINALID)
        {
            try
            {
                var e = new TerminalSelectiveSoftwareUpdate();
                return (e.LoadByPrimaryKey(TERMINALID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TerminalSelectiveSoftwareUpdate Get(System.Decimal TERMINALID)
        {
            try
            {
                var e = new TerminalSelectiveSoftwareUpdate();
                return (e.LoadByPrimaryKey(TERMINALID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TerminalSelectiveSoftwareUpdate(System.Decimal TERMINALID)
        {
            this.LoadByPrimaryKey(TERMINALID);
        }
		
		public TerminalSelectiveSoftwareUpdate()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TerminalSelectiveSoftwareUpdateCol")]
	public partial class TerminalSelectiveSoftwareUpdateCol : esTerminalSelectiveSoftwareUpdateCol, IEnumerable<TerminalSelectiveSoftwareUpdate>
	{
	
		public TerminalSelectiveSoftwareUpdateCol()
		{
		}

	
	
		public TerminalSelectiveSoftwareUpdate FindByPrimaryKey(System.Decimal terminalID)
		{
			return this.SingleOrDefault(e => e.TerminalID == terminalID);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TerminalSelectiveSoftwareUpdate))]
		public class TerminalSelectiveSoftwareUpdateColWCFPacket : esCollectionWCFPacket<TerminalSelectiveSoftwareUpdateCol>
		{
			public static implicit operator TerminalSelectiveSoftwareUpdateCol(TerminalSelectiveSoftwareUpdateColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TerminalSelectiveSoftwareUpdateColWCFPacket(TerminalSelectiveSoftwareUpdateCol collection)
			{
				return new TerminalSelectiveSoftwareUpdateColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TerminalSelectiveSoftwareUpdateQ : esTerminalSelectiveSoftwareUpdateQ
	{
		public TerminalSelectiveSoftwareUpdateQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TerminalSelectiveSoftwareUpdateQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TerminalSelectiveSoftwareUpdateQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new TerminalSelectiveSoftwareUpdateQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TerminalSelectiveSoftwareUpdateQ query)
		{
			return TerminalSelectiveSoftwareUpdateQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TerminalSelectiveSoftwareUpdateQ(string query)
		{
			return (TerminalSelectiveSoftwareUpdateQ)TerminalSelectiveSoftwareUpdateQ.SerializeHelper.FromXml(query, typeof(TerminalSelectiveSoftwareUpdateQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTerminalSelectiveSoftwareUpdate : EntityBase
	{
		public esTerminalSelectiveSoftwareUpdate()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal terminalID)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(terminalID);
			else
				return LoadByPrimaryKeyStoredProcedure(terminalID);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal terminalID)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(terminalID);
			else
				return LoadByPrimaryKeyStoredProcedure(terminalID);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal terminalID)
		{
			TerminalSelectiveSoftwareUpdateQ query = new TerminalSelectiveSoftwareUpdateQ();
			query.Where(query.TerminalID == terminalID);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal terminalID)
		{
			esParameters parms = new esParameters();
			parms.Add("TerminalID", terminalID);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal TerminalID
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalSelectiveSoftwareUpdate.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal VehiculoID
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalSelectiveSoftwareUpdate.VehiculoMatricula
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoMatricula
		{
			get
			{
				return base.GetSystemStringRequired(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoMatricula);
			}
			
			set
			{
				if(base.SetSystemString(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoMatricula, value))
				{
					OnPropertyChanged(TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.VehiculoMatricula);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalSelectiveSoftwareUpdate.MaxVersionUpdate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MaxVersionUpdate
		{
			get
			{
				return base.GetSystemStringRequired(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.MaxVersionUpdate);
			}
			
			set
			{
				if(base.SetSystemString(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.MaxVersionUpdate, value))
				{
					OnPropertyChanged(TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.MaxVersionUpdate);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "VehiculoMatricula": this.str().VehiculoMatricula = (string)value; break;							
						case "MaxVersionUpdate": this.str().MaxVersionUpdate = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal)value;
								OnPropertyChanged(TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal)value;
								OnPropertyChanged(TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.VehiculoID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTerminalSelectiveSoftwareUpdate entity)
			{
				this.entity = entity;
			}
			

			public System.String TerminalID
			{
				get
				{
					return Convert.ToString(entity.TerminalID);
				}

				set
				{
					entity.TerminalID = Convert.ToDecimal(value);
				}
			}

			public System.String VehiculoID
			{
				get
				{
					return Convert.ToString(entity.VehiculoID);
				}

				set
				{
					entity.VehiculoID = Convert.ToDecimal(value);
				}
			}

			public System.String VehiculoMatricula
			{
				get
				{
					return Convert.ToString(entity.VehiculoMatricula);
				}

				set
				{
					entity.VehiculoMatricula = Convert.ToString(value);
				}
			}

			public System.String MaxVersionUpdate
			{
				get
				{
					return Convert.ToString(entity.MaxVersionUpdate);
				}

				set
				{
					entity.MaxVersionUpdate = Convert.ToString(value);
				}
			}


			private esTerminalSelectiveSoftwareUpdate entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TerminalSelectiveSoftwareUpdateMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TerminalSelectiveSoftwareUpdateQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalSelectiveSoftwareUpdateQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalSelectiveSoftwareUpdateQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TerminalSelectiveSoftwareUpdateQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TerminalSelectiveSoftwareUpdateQ query;		
	}



	[Serializable]
	abstract public partial class esTerminalSelectiveSoftwareUpdateCol : CollectionBase<TerminalSelectiveSoftwareUpdate>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TerminalSelectiveSoftwareUpdateMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TerminalSelectiveSoftwareUpdateCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TerminalSelectiveSoftwareUpdateQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalSelectiveSoftwareUpdateQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalSelectiveSoftwareUpdateQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TerminalSelectiveSoftwareUpdateQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TerminalSelectiveSoftwareUpdateQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TerminalSelectiveSoftwareUpdateQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TerminalSelectiveSoftwareUpdateQ query;
	}



	[Serializable]
	abstract public partial class esTerminalSelectiveSoftwareUpdateQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TerminalSelectiveSoftwareUpdateMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.TerminalID,new esQueryItem(this, TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoID,new esQueryItem(this, TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoMatricula,new esQueryItem(this, TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoMatricula, esSystemType.String));
			_queryItems.Add(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.MaxVersionUpdate,new esQueryItem(this, TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.MaxVersionUpdate, esSystemType.String));
			
	    }
	
	    #region esQueryItems

		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoMatricula
		{
			get { return new esQueryItem(this, TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoMatricula, esSystemType.String); }
		} 
		
		public esQueryItem MaxVersionUpdate
		{
			get { return new esQueryItem(this, TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.MaxVersionUpdate, esSystemType.String); }
		} 
		
		#endregion
		
	}


	
	public partial class TerminalSelectiveSoftwareUpdate : esTerminalSelectiveSoftwareUpdate
	{

		#region UpToTerminalState - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - FK_TerminalSelectiveSoftwareUpdate_TerminalState
		/// </summary>

		[XmlIgnore]
		public TerminalState UpToTerminalState
		{
			get
			{
				if (this.es.IsLazyLoadDisabled) return null;
			
				if(this._UpToTerminalState == null && TerminalID != null)
				{
					this._UpToTerminalState = new TerminalState();
					this._UpToTerminalState.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTerminalState", this._UpToTerminalState);
					this._UpToTerminalState.Query.Where(this._UpToTerminalState.Query.TerminalID == this.TerminalID);
					this._UpToTerminalState.Query.Load();
				}

				return this._UpToTerminalState;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToTerminalState");

				if(value == null)
				{
					this._UpToTerminalState = null;
				}
				else
				{
					this._UpToTerminalState = value;
					this.SetPreSave("UpToTerminalState", this._UpToTerminalState);
				}
				
				this.OnPropertyChanged("UpToTerminalState");
			} 
		}
				
		
		private TerminalState _UpToTerminalState;
		#endregion

		
		
	}
	



	[Serializable]
	public partial class TerminalSelectiveSoftwareUpdateMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TerminalSelectiveSoftwareUpdateMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.TerminalID, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.TerminalID;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.VehiculoMatricula, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.VehiculoMatricula;
			c.CharacterMaxLength = 12;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalSelectiveSoftwareUpdateMetadata.ColumnNames.MaxVersionUpdate, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalSelectiveSoftwareUpdateMetadata.PropertyNames.MaxVersionUpdate;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TerminalSelectiveSoftwareUpdateMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string MaxVersionUpdate = "MaxVersionUpdate";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string MaxVersionUpdate = "MaxVersionUpdate";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TerminalSelectiveSoftwareUpdateMetadata))
			{
				if(TerminalSelectiveSoftwareUpdateMetadata.mapDelegates == null)
				{
					TerminalSelectiveSoftwareUpdateMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TerminalSelectiveSoftwareUpdateMetadata.meta == null)
				{
					TerminalSelectiveSoftwareUpdateMetadata.meta = new TerminalSelectiveSoftwareUpdateMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoMatricula", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("MaxVersionUpdate", new esTypeMap("nvarchar", "System.String"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "TerminalSelectiveSoftwareUpdate";
				meta.Destination = "TerminalSelectiveSoftwareUpdate";
				
				meta.spInsert = "proc_TerminalSelectiveSoftwareUpdateInsert";				
				meta.spUpdate = "proc_TerminalSelectiveSoftwareUpdateUpdate";		
				meta.spDelete = "proc_TerminalSelectiveSoftwareUpdateDelete";
				meta.spLoadAll = "proc_TerminalSelectiveSoftwareUpdateLoadAll";
				meta.spLoadByPrimaryKey = "proc_TerminalSelectiveSoftwareUpdateLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TerminalSelectiveSoftwareUpdateMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
