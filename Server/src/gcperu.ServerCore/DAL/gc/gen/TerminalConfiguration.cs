
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Encapsulates the 'TerminalConfiguration' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TerminalConfiguration")]
	public partial class TerminalConfiguration : esTerminalConfiguration
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TerminalConfiguration();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal terminalID)
		{
			var obj = new TerminalConfiguration();
			obj.TerminalID = terminalID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal terminalID, esSqlAccessType sqlAccessType)
		{
			var obj = new TerminalConfiguration();
			obj.TerminalID = terminalID;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal TERMINALID)
        {
            try
            {
                var e = new TerminalConfiguration();
                return (e.LoadByPrimaryKey(TERMINALID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TerminalConfiguration Get(System.Decimal TERMINALID)
        {
            try
            {
                var e = new TerminalConfiguration();
                return (e.LoadByPrimaryKey(TERMINALID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TerminalConfiguration(System.Decimal TERMINALID)
        {
            this.LoadByPrimaryKey(TERMINALID);
        }
		
		public TerminalConfiguration()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TerminalConfigurationCol")]
	public partial class TerminalConfigurationCol : esTerminalConfigurationCol, IEnumerable<TerminalConfiguration>
	{
	
		public TerminalConfigurationCol()
		{
		}

	
	
		public TerminalConfiguration FindByPrimaryKey(System.Decimal terminalID)
		{
			return this.SingleOrDefault(e => e.TerminalID == terminalID);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TerminalConfiguration))]
		public class TerminalConfigurationColWCFPacket : esCollectionWCFPacket<TerminalConfigurationCol>
		{
			public static implicit operator TerminalConfigurationCol(TerminalConfigurationColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TerminalConfigurationColWCFPacket(TerminalConfigurationCol collection)
			{
				return new TerminalConfigurationColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TerminalConfigurationQ : esTerminalConfigurationQ
	{
		public TerminalConfigurationQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TerminalConfigurationQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TerminalConfigurationQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new TerminalConfigurationQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TerminalConfigurationQ query)
		{
			return TerminalConfigurationQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TerminalConfigurationQ(string query)
		{
			return (TerminalConfigurationQ)TerminalConfigurationQ.SerializeHelper.FromXml(query, typeof(TerminalConfigurationQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTerminalConfiguration : EntityBase
	{
		public esTerminalConfiguration()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal terminalID)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(terminalID);
			else
				return LoadByPrimaryKeyStoredProcedure(terminalID);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal terminalID)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(terminalID);
			else
				return LoadByPrimaryKeyStoredProcedure(terminalID);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal terminalID)
		{
			TerminalConfigurationQ query = new TerminalConfigurationQ();
			query.Where(query.TerminalID == terminalID);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal terminalID)
		{
			esParameters parms = new esParameters();
			parms.Add("TerminalID", terminalID);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to TerminalConfiguration.TerminalID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal TerminalID
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalConfigurationMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalConfigurationMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(TerminalConfigurationMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalConfiguration.VehiculoCodigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigo
		{
			get
			{
				return base.GetSystemStringRequired(TerminalConfigurationMetadata.ColumnNames.VehiculoCodigo);
			}
			
			set
			{
				if(base.SetSystemString(TerminalConfigurationMetadata.ColumnNames.VehiculoCodigo, value))
				{
					OnPropertyChanged(TerminalConfigurationMetadata.PropertyNames.VehiculoCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalConfiguration.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalConfigurationMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalConfigurationMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(TerminalConfigurationMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalConfiguration.ConfigData
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ConfigData
		{
			get
			{
				return base.GetSystemStringRequired(TerminalConfigurationMetadata.ColumnNames.ConfigData);
			}
			
			set
			{
				if(base.SetSystemString(TerminalConfigurationMetadata.ColumnNames.ConfigData, value))
				{
					OnPropertyChanged(TerminalConfigurationMetadata.PropertyNames.ConfigData);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalConfiguration.Fecha
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? Fecha
		{
			get
			{
				return base.GetSystemDateTime(TerminalConfigurationMetadata.ColumnNames.Fecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalConfigurationMetadata.ColumnNames.Fecha, value))
				{
					OnPropertyChanged(TerminalConfigurationMetadata.PropertyNames.Fecha);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "VehiculoCodigo": this.str().VehiculoCodigo = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "ConfigData": this.str().ConfigData = (string)value; break;							
						case "Fecha": this.str().Fecha = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal)value;
								OnPropertyChanged(TerminalConfigurationMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalConfigurationMetadata.PropertyNames.VehiculoID);
							break;
						
						case "Fecha":
						
							if (value == null || value is System.DateTime)
								this.Fecha = (System.DateTime?)value;
								OnPropertyChanged(TerminalConfigurationMetadata.PropertyNames.Fecha);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTerminalConfiguration entity)
			{
				this.entity = entity;
			}
			

			public System.String TerminalID
			{
				get
				{
					return Convert.ToString(entity.TerminalID);
				}

				set
				{
					entity.TerminalID = Convert.ToDecimal(value);
				}
			}

			public System.String VehiculoCodigo
			{
				get
				{
					return Convert.ToString(entity.VehiculoCodigo);
				}

				set
				{
					entity.VehiculoCodigo = Convert.ToString(value);
				}
			}
	
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
			
			public System.String ConfigData
			{
				get
				{
					return Convert.ToString(entity.ConfigData);
				}

				set
				{
					entity.ConfigData = Convert.ToString(value);
				}
			}
	
			public System.String Fecha
			{
				get
				{
					System.DateTime? data = entity.Fecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fecha = null;
					else entity.Fecha = Convert.ToDateTime(value);
				}
			}
			

			private esTerminalConfiguration entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TerminalConfigurationMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TerminalConfigurationQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalConfigurationQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalConfigurationQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TerminalConfigurationQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TerminalConfigurationQ query;		
	}



	[Serializable]
	abstract public partial class esTerminalConfigurationCol : CollectionBase<TerminalConfiguration>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TerminalConfigurationMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TerminalConfigurationCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TerminalConfigurationQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalConfigurationQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalConfigurationQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TerminalConfigurationQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TerminalConfigurationQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TerminalConfigurationQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TerminalConfigurationQ query;
	}



	[Serializable]
	abstract public partial class esTerminalConfigurationQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TerminalConfigurationMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TerminalConfigurationMetadata.ColumnNames.TerminalID,new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(TerminalConfigurationMetadata.ColumnNames.VehiculoCodigo,new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.VehiculoCodigo, esSystemType.String));
			_queryItems.Add(TerminalConfigurationMetadata.ColumnNames.VehiculoID,new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(TerminalConfigurationMetadata.ColumnNames.ConfigData,new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.ConfigData, esSystemType.String));
			_queryItems.Add(TerminalConfigurationMetadata.ColumnNames.Fecha,new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.Fecha, esSystemType.DateTime));
			
	    }
	
	    #region esQueryItems

		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoCodigo
		{
			get { return new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.VehiculoCodigo, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ConfigData
		{
			get { return new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.ConfigData, esSystemType.String); }
		} 
		
		public esQueryItem Fecha
		{
			get { return new esQueryItem(this, TerminalConfigurationMetadata.ColumnNames.Fecha, esSystemType.DateTime); }
		} 
		
		#endregion
		
	}


	
	public partial class TerminalConfiguration : esTerminalConfiguration
	{

		
		
	}
	



	[Serializable]
	public partial class TerminalConfigurationMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TerminalConfigurationMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TerminalConfigurationMetadata.ColumnNames.TerminalID, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalConfigurationMetadata.PropertyNames.TerminalID;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalConfigurationMetadata.ColumnNames.VehiculoCodigo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalConfigurationMetadata.PropertyNames.VehiculoCodigo;
			c.CharacterMaxLength = 10;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalConfigurationMetadata.ColumnNames.VehiculoID, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalConfigurationMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalConfigurationMetadata.ColumnNames.ConfigData, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalConfigurationMetadata.PropertyNames.ConfigData;
			c.CharacterMaxLength = 5000;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalConfigurationMetadata.ColumnNames.Fecha, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalConfigurationMetadata.PropertyNames.Fecha;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TerminalConfigurationMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoID = "VehiculoID";
			 public const string ConfigData = "ConfigData";
			 public const string Fecha = "Fecha";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoID = "VehiculoID";
			 public const string ConfigData = "ConfigData";
			 public const string Fecha = "Fecha";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TerminalConfigurationMetadata))
			{
				if(TerminalConfigurationMetadata.mapDelegates == null)
				{
					TerminalConfigurationMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TerminalConfigurationMetadata.meta == null)
				{
					TerminalConfigurationMetadata.meta = new TerminalConfigurationMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoCodigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ConfigData", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fecha", new esTypeMap("datetime", "System.DateTime"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "TerminalConfiguration";
				meta.Destination = "TerminalConfiguration";
				
				meta.spInsert = "proc_TerminalConfigurationInsert";				
				meta.spUpdate = "proc_TerminalConfigurationUpdate";		
				meta.spDelete = "proc_TerminalConfigurationDelete";
				meta.spLoadAll = "proc_TerminalConfigurationLoadAll";
				meta.spLoadByPrimaryKey = "proc_TerminalConfigurationLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TerminalConfigurationMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
