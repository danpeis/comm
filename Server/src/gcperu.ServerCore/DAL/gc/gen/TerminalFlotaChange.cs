
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// Encapsulates the 'TerminalFlotaChange' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TerminalFlotaChange")]
	public partial class TerminalFlotaChange : esTerminalFlotaChange
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TerminalFlotaChange();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new TerminalFlotaChange();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new TerminalFlotaChange();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new TerminalFlotaChange();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TerminalFlotaChange Get(System.Decimal ID)
        {
            try
            {
                var e = new TerminalFlotaChange();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TerminalFlotaChange(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public TerminalFlotaChange()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TerminalFlotaChangeCol")]
	public partial class TerminalFlotaChangeCol : esTerminalFlotaChangeCol, IEnumerable<TerminalFlotaChange>
	{
	
		public TerminalFlotaChangeCol()
		{
		}

	
	
		public TerminalFlotaChange FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TerminalFlotaChange))]
		public class TerminalFlotaChangeColWCFPacket : esCollectionWCFPacket<TerminalFlotaChangeCol>
		{
			public static implicit operator TerminalFlotaChangeCol(TerminalFlotaChangeColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TerminalFlotaChangeColWCFPacket(TerminalFlotaChangeCol collection)
			{
				return new TerminalFlotaChangeColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TerminalFlotaChangeQ : esTerminalFlotaChangeQ
	{
		public TerminalFlotaChangeQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TerminalFlotaChangeQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TerminalFlotaChangeQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new TerminalFlotaChangeQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TerminalFlotaChangeQ query)
		{
			return TerminalFlotaChangeQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TerminalFlotaChangeQ(string query)
		{
			return (TerminalFlotaChangeQ)TerminalFlotaChangeQ.SerializeHelper.FromXml(query, typeof(TerminalFlotaChangeQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTerminalFlotaChange : EntityBase
	{
		public esTerminalFlotaChange()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			TerminalFlotaChangeQ query = new TerminalFlotaChangeQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to TerminalFlotaChange.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal Id
		{
			get
			{
				return base.GetSystemDecimalRequired(TerminalFlotaChangeMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalFlotaChangeMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalFlotaChange.Change
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? Change
		{
			get
			{
				return base.GetSystemInt32(TerminalFlotaChangeMetadata.ColumnNames.Change);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalFlotaChangeMetadata.ColumnNames.Change, value))
				{
					OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.Change);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalFlotaChange.Fecha
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? Fecha
		{
			get
			{
				return base.GetSystemDateTime(TerminalFlotaChangeMetadata.ColumnNames.Fecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalFlotaChangeMetadata.ColumnNames.Fecha, value))
				{
					OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.Fecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalFlotaChange.TerminalNewID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalNewID
		{
			get
			{
				return base.GetSystemDecimal(TerminalFlotaChangeMetadata.ColumnNames.TerminalNewID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalFlotaChangeMetadata.ColumnNames.TerminalNewID, value))
				{
					OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.TerminalNewID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalFlotaChange.TerminalOldID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalOldID
		{
			get
			{
				return base.GetSystemDecimal(TerminalFlotaChangeMetadata.ColumnNames.TerminalOldID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalFlotaChangeMetadata.ColumnNames.TerminalOldID, value))
				{
					OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.TerminalOldID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalFlotaChange.VehiculoNewID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoNewID
		{
			get
			{
				return base.GetSystemDecimal(TerminalFlotaChangeMetadata.ColumnNames.VehiculoNewID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalFlotaChangeMetadata.ColumnNames.VehiculoNewID, value))
				{
					OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.VehiculoNewID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalFlotaChange.VehiculoOldID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoOldID
		{
			get
			{
				return base.GetSystemDecimal(TerminalFlotaChangeMetadata.ColumnNames.VehiculoOldID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalFlotaChangeMetadata.ColumnNames.VehiculoOldID, value))
				{
					OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.VehiculoOldID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalFlotaChange.Intentos
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte Intentos
		{
			get
			{
				return base.GetSystemByteRequired(TerminalFlotaChangeMetadata.ColumnNames.Intentos);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalFlotaChangeMetadata.ColumnNames.Intentos, value))
				{
					OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.Intentos);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "Change": this.str().Change = (string)value; break;							
						case "Fecha": this.str().Fecha = (string)value; break;							
						case "TerminalNewID": this.str().TerminalNewID = (string)value; break;							
						case "TerminalOldID": this.str().TerminalOldID = (string)value; break;							
						case "VehiculoNewID": this.str().VehiculoNewID = (string)value; break;							
						case "VehiculoOldID": this.str().VehiculoOldID = (string)value; break;							
						case "Intentos": this.str().Intentos = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal)value;
								OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.Id);
							break;
						
						case "Change":
						
							if (value == null || value is System.Int32)
								this.Change = (System.Int32?)value;
								OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.Change);
							break;
						
						case "Fecha":
						
							if (value == null || value is System.DateTime)
								this.Fecha = (System.DateTime?)value;
								OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.Fecha);
							break;
						
						case "TerminalNewID":
						
							if (value == null || value is System.Decimal)
								this.TerminalNewID = (System.Decimal?)value;
								OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.TerminalNewID);
							break;
						
						case "TerminalOldID":
						
							if (value == null || value is System.Decimal)
								this.TerminalOldID = (System.Decimal?)value;
								OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.TerminalOldID);
							break;
						
						case "VehiculoNewID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoNewID = (System.Decimal?)value;
								OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.VehiculoNewID);
							break;
						
						case "VehiculoOldID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoOldID = (System.Decimal?)value;
								OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.VehiculoOldID);
							break;
						
						case "Intentos":
						
							if (value == null || value is System.Byte)
								this.Intentos = (System.Byte)value;
								OnPropertyChanged(TerminalFlotaChangeMetadata.PropertyNames.Intentos);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTerminalFlotaChange entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToDecimal(value);
				}
			}
	
			public System.String Change
			{
				get
				{
					System.Int32? data = entity.Change;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Change = null;
					else entity.Change = Convert.ToInt32(value);
				}
			}
				
			public System.String Fecha
			{
				get
				{
					System.DateTime? data = entity.Fecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fecha = null;
					else entity.Fecha = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalNewID
			{
				get
				{
					System.Decimal? data = entity.TerminalNewID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalNewID = null;
					else entity.TerminalNewID = Convert.ToDecimal(value);
				}
			}
				
			public System.String TerminalOldID
			{
				get
				{
					System.Decimal? data = entity.TerminalOldID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalOldID = null;
					else entity.TerminalOldID = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehiculoNewID
			{
				get
				{
					System.Decimal? data = entity.VehiculoNewID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoNewID = null;
					else entity.VehiculoNewID = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehiculoOldID
			{
				get
				{
					System.Decimal? data = entity.VehiculoOldID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoOldID = null;
					else entity.VehiculoOldID = Convert.ToDecimal(value);
				}
			}
			
			public System.String Intentos
			{
				get
				{
					return Convert.ToString(entity.Intentos);
				}

				set
				{
					entity.Intentos = Convert.ToByte(value);
				}
			}


			private esTerminalFlotaChange entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TerminalFlotaChangeMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TerminalFlotaChangeQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalFlotaChangeQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalFlotaChangeQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TerminalFlotaChangeQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TerminalFlotaChangeQ query;		
	}



	[Serializable]
	abstract public partial class esTerminalFlotaChangeCol : CollectionBase<TerminalFlotaChange>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TerminalFlotaChangeMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TerminalFlotaChangeCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TerminalFlotaChangeQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalFlotaChangeQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalFlotaChangeQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TerminalFlotaChangeQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TerminalFlotaChangeQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TerminalFlotaChangeQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TerminalFlotaChangeQ query;
	}



	[Serializable]
	abstract public partial class esTerminalFlotaChangeQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TerminalFlotaChangeMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TerminalFlotaChangeMetadata.ColumnNames.Id,new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(TerminalFlotaChangeMetadata.ColumnNames.Change,new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.Change, esSystemType.Int32));
			_queryItems.Add(TerminalFlotaChangeMetadata.ColumnNames.Fecha,new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.Fecha, esSystemType.DateTime));
			_queryItems.Add(TerminalFlotaChangeMetadata.ColumnNames.TerminalNewID,new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.TerminalNewID, esSystemType.Decimal));
			_queryItems.Add(TerminalFlotaChangeMetadata.ColumnNames.TerminalOldID,new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.TerminalOldID, esSystemType.Decimal));
			_queryItems.Add(TerminalFlotaChangeMetadata.ColumnNames.VehiculoNewID,new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.VehiculoNewID, esSystemType.Decimal));
			_queryItems.Add(TerminalFlotaChangeMetadata.ColumnNames.VehiculoOldID,new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.VehiculoOldID, esSystemType.Decimal));
			_queryItems.Add(TerminalFlotaChangeMetadata.ColumnNames.Intentos,new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.Intentos, esSystemType.Byte));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem Change
		{
			get { return new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.Change, esSystemType.Int32); }
		} 
		
		public esQueryItem Fecha
		{
			get { return new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.Fecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalNewID
		{
			get { return new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.TerminalNewID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalOldID
		{
			get { return new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.TerminalOldID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoNewID
		{
			get { return new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.VehiculoNewID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoOldID
		{
			get { return new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.VehiculoOldID, esSystemType.Decimal); }
		} 
		
		public esQueryItem Intentos
		{
			get { return new esQueryItem(this, TerminalFlotaChangeMetadata.ColumnNames.Intentos, esSystemType.Byte); }
		} 
		
		#endregion
		
	}


	
	public partial class TerminalFlotaChange : esTerminalFlotaChange
	{

		
		
	}
	



	[Serializable]
	public partial class TerminalFlotaChangeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TerminalFlotaChangeMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TerminalFlotaChangeMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalFlotaChangeMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalFlotaChangeMetadata.ColumnNames.Change, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalFlotaChangeMetadata.PropertyNames.Change;
			c.NumericPrecision = 10;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalFlotaChangeMetadata.ColumnNames.Fecha, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalFlotaChangeMetadata.PropertyNames.Fecha;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalFlotaChangeMetadata.ColumnNames.TerminalNewID, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalFlotaChangeMetadata.PropertyNames.TerminalNewID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalFlotaChangeMetadata.ColumnNames.TerminalOldID, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalFlotaChangeMetadata.PropertyNames.TerminalOldID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalFlotaChangeMetadata.ColumnNames.VehiculoNewID, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalFlotaChangeMetadata.PropertyNames.VehiculoNewID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalFlotaChangeMetadata.ColumnNames.VehiculoOldID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalFlotaChangeMetadata.PropertyNames.VehiculoOldID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalFlotaChangeMetadata.ColumnNames.Intentos, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalFlotaChangeMetadata.PropertyNames.Intentos;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TerminalFlotaChangeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string Change = "Change";
			 public const string Fecha = "Fecha";
			 public const string TerminalNewID = "TerminalNewID";
			 public const string TerminalOldID = "TerminalOldID";
			 public const string VehiculoNewID = "VehiculoNewID";
			 public const string VehiculoOldID = "VehiculoOldID";
			 public const string Intentos = "Intentos";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Change = "Change";
			 public const string Fecha = "Fecha";
			 public const string TerminalNewID = "TerminalNewID";
			 public const string TerminalOldID = "TerminalOldID";
			 public const string VehiculoNewID = "VehiculoNewID";
			 public const string VehiculoOldID = "VehiculoOldID";
			 public const string Intentos = "Intentos";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TerminalFlotaChangeMetadata))
			{
				if(TerminalFlotaChangeMetadata.mapDelegates == null)
				{
					TerminalFlotaChangeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TerminalFlotaChangeMetadata.meta == null)
				{
					TerminalFlotaChangeMetadata.meta = new TerminalFlotaChangeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Change", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Fecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalNewID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalOldID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoNewID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoOldID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Intentos", new esTypeMap("tinyint", "System.Byte"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "TerminalFlotaChange";
				meta.Destination = "TerminalFlotaChange";
				
				meta.spInsert = "proc_TerminalFlotaChangeInsert";				
				meta.spUpdate = "proc_TerminalFlotaChangeUpdate";		
				meta.spDelete = "proc_TerminalFlotaChangeDelete";
				meta.spLoadAll = "proc_TerminalFlotaChangeLoadAll";
				meta.spLoadByPrimaryKey = "proc_TerminalFlotaChangeLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TerminalFlotaChangeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
