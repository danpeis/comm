
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 14:10:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GC
{
	/// <summary>
	/// ALMACENA LOS CAMBIOS DE ESTADO QUE SE RECIBEN DE LOS SERVICIOS, QUE ESTAN POR FINALIZAR. CUANDO SE BORRA DE [ServiciosEnCurso], Servicio finalizó => AUTOMATICAMENTE, SE BORRAN DE ESTA TABLA Y SE PASAN [HistoricoServiciosCambioEstado]. Se hace a traves de un Trigger en la tabla [ServiciosEnCurso].
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("ServiciosCambioEstado")]
	public partial class ServiciosCambioEstado : esServiciosCambioEstado
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new ServiciosCambioEstado();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new ServiciosCambioEstado();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new ServiciosCambioEstado();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new ServiciosCambioEstado();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static ServiciosCambioEstado Get(System.Decimal ID)
        {
            try
            {
                var e = new ServiciosCambioEstado();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public ServiciosCambioEstado(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public ServiciosCambioEstado()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("ServiciosCambioEstadoCol")]
	public partial class ServiciosCambioEstadoCol : esServiciosCambioEstadoCol, IEnumerable<ServiciosCambioEstado>
	{
	
		public ServiciosCambioEstadoCol()
		{
		}

	
	
		public ServiciosCambioEstado FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(ServiciosCambioEstado))]
		public class ServiciosCambioEstadoColWCFPacket : esCollectionWCFPacket<ServiciosCambioEstadoCol>
		{
			public static implicit operator ServiciosCambioEstadoCol(ServiciosCambioEstadoColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator ServiciosCambioEstadoColWCFPacket(ServiciosCambioEstadoCol collection)
			{
				return new ServiciosCambioEstadoColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class ServiciosCambioEstadoQ : esServiciosCambioEstadoQ
	{
		public ServiciosCambioEstadoQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public ServiciosCambioEstadoQ()
		{
		}

		override protected string GetQueryName()
		{
			return "ServiciosCambioEstadoQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GC";
		}

		public override QueryBase CreateQuery()
        {
            return new ServiciosCambioEstadoQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(ServiciosCambioEstadoQ query)
		{
			return ServiciosCambioEstadoQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator ServiciosCambioEstadoQ(string query)
		{
			return (ServiciosCambioEstadoQ)ServiciosCambioEstadoQ.SerializeHelper.FromXml(query, typeof(ServiciosCambioEstadoQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esServiciosCambioEstado : EntityBase
	{
		public esServiciosCambioEstado()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			ServiciosCambioEstadoQ query = new ServiciosCambioEstadoQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// PK. Autonumerica
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal Id
		{
			get
			{
				return base.GetSystemDecimalRequired(ServiciosCambioEstadoMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// ID del Servicio respecto al SERVIDOR, FK de ServiciosEnCurso
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ServicioCursoID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID, value))
				{
					this._UpToServiciosEnCursoByServicioCursoID = null;
					this.OnPropertyChanged("UpToServiciosEnCursoByServicioCursoID");
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.ServicioCursoID);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Terminal. Obtenido tabla GITS.TerminalesGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Identificador de la Trama, que envio este Registro.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String NumSec
		{
			get
			{
				return base.GetSystemStringRequired(ServiciosCambioEstadoMetadata.ColumnNames.NumSec);
			}
			
			set
			{
				if(base.SetSystemString(ServiciosCambioEstadoMetadata.ColumnNames.NumSec, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.NumSec);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Vehiculo. Obtenido de GITS.Vehiculos
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Servicio. Obtenido de GITS.dbo.Viajes.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ServicioID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.ServicioID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.ServicioID, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.ServicioID);
				}
			}
		}		
		
		/// <summary>
		/// Indica el estado del Servicio. Se realizan por pesos, siendo mayor miestras mas avanzado en su progreso este el Servicio.  99 - Especial. Indica que está pendiente de Anulacion.  1 - Asignado. Estado incial.  2 - Confirmado Terminal. (C para GITS)  3 - Confirmado Conductor (CC para GITS)  4 - En Curso. (E para GITS)  5 - Finalizado (R para GITS)
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte Estado
		{
			get
			{
				return base.GetSystemByteRequired(ServiciosCambioEstadoMetadata.ColumnNames.Estado);
			}
			
			set
			{
				if(base.SetSystemByte(ServiciosCambioEstadoMetadata.ColumnNames.Estado, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.Estado);
				}
			}
		}		
		
		/// <summary>
		/// Maps to ServiciosCambioEstado.Fecha
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime Fecha
		{
			get
			{
				return base.GetSystemDateTimeRequired(ServiciosCambioEstadoMetadata.ColumnNames.Fecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(ServiciosCambioEstadoMetadata.ColumnNames.Fecha, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.Fecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to ServiciosCambioEstado.ServerID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte ServerID
		{
			get
			{
				return base.GetSystemByteRequired(ServiciosCambioEstadoMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemByte(ServiciosCambioEstadoMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		/// <summary>
		/// Se utiliza para marcar el registro con un información sobre su estado de Proceso. Sus valores seran definidos en codigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32 FlagState
		{
			get
			{
				return base.GetSystemInt32Required(ServiciosCambioEstadoMetadata.ColumnNames.FlagState);
			}
			
			set
			{
				if(base.SetSystemInt32(ServiciosCambioEstadoMetadata.ColumnNames.FlagState, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.FlagState);
				}
			}
		}		
		
		/// <summary>
		/// Maps to ServiciosCambioEstado.GpsLatitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLatitud
		{
			get
			{
				return base.GetSystemDoubleRequired(ServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud);
			}
			
			set
			{
				if(base.SetSystemDouble(ServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.GpsLatitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to ServiciosCambioEstado.GpsLongitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double GpsLongitud
		{
			get
			{
				return base.GetSystemDoubleRequired(ServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud);
			}
			
			set
			{
				if(base.SetSystemDouble(ServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.GpsLongitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to ServiciosCambioEstado.Odometro
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double Odometro
		{
			get
			{
				return base.GetSystemDoubleRequired(ServiciosCambioEstadoMetadata.ColumnNames.Odometro);
			}
			
			set
			{
				if(base.SetSystemDouble(ServiciosCambioEstadoMetadata.ColumnNames.Odometro, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.Odometro);
				}
			}
		}		
		
		/// <summary>
		/// Maps to ServiciosCambioEstado.GrupoCursadoRuta
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GrupoCursadoRuta
		{
			get
			{
				return base.GetSystemStringRequired(ServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta);
			}
			
			set
			{
				if(base.SetSystemString(ServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.GrupoCursadoRuta);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Vehiculo. Obtenido de GITS.Vehiculos
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PersonalID
		{
			get
			{
				return base.GetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.PersonalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(ServiciosCambioEstadoMetadata.ColumnNames.PersonalID, value))
				{
					OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.PersonalID);
				}
			}
		}		
		
		[CLSCompliant(false)]
		internal protected ServiciosEnCurso _UpToServiciosEnCursoByServicioCursoID;
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "ServicioCursoID": this.str().ServicioCursoID = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "NumSec": this.str().NumSec = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "ServicioID": this.str().ServicioID = (string)value; break;							
						case "Estado": this.str().Estado = (string)value; break;							
						case "Fecha": this.str().Fecha = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;							
						case "FlagState": this.str().FlagState = (string)value; break;							
						case "GpsLatitud": this.str().GpsLatitud = (string)value; break;							
						case "GpsLongitud": this.str().GpsLongitud = (string)value; break;							
						case "Odometro": this.str().Odometro = (string)value; break;							
						case "GrupoCursadoRuta": this.str().GrupoCursadoRuta = (string)value; break;							
						case "PersonalID": this.str().PersonalID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.Id);
							break;
						
						case "ServicioCursoID":
						
							if (value == null || value is System.Decimal)
								this.ServicioCursoID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.ServicioCursoID);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.VehiculoID);
							break;
						
						case "ServicioID":
						
							if (value == null || value is System.Decimal)
								this.ServicioID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.ServicioID);
							break;
						
						case "Estado":
						
							if (value == null || value is System.Byte)
								this.Estado = (System.Byte)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.Estado);
							break;
						
						case "Fecha":
						
							if (value == null || value is System.DateTime)
								this.Fecha = (System.DateTime)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.Fecha);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Byte)
								this.ServerID = (System.Byte)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.ServerID);
							break;
						
						case "FlagState":
						
							if (value == null || value is System.Int32)
								this.FlagState = (System.Int32)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.FlagState);
							break;
						
						case "GpsLatitud":
						
							if (value == null || value is System.Double)
								this.GpsLatitud = (System.Double)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.GpsLatitud);
							break;
						
						case "GpsLongitud":
						
							if (value == null || value is System.Double)
								this.GpsLongitud = (System.Double)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.GpsLongitud);
							break;
						
						case "Odometro":
						
							if (value == null || value is System.Double)
								this.Odometro = (System.Double)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.Odometro);
							break;
						
						case "PersonalID":
						
							if (value == null || value is System.Decimal)
								this.PersonalID = (System.Decimal?)value;
								OnPropertyChanged(ServiciosCambioEstadoMetadata.PropertyNames.PersonalID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esServiciosCambioEstado entity)
			{
				this.entity = entity;
			}
			

			public System.String Id
			{
				get
				{
					return Convert.ToString(entity.Id);
				}

				set
				{
					entity.Id = Convert.ToDecimal(value);
				}
			}
	
			public System.String ServicioCursoID
			{
				get
				{
					System.Decimal? data = entity.ServicioCursoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioCursoID = null;
					else entity.ServicioCursoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String TerminalID
			{
				get
				{
					System.Decimal? data = entity.TerminalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalID = null;
					else entity.TerminalID = Convert.ToDecimal(value);
				}
			}
			
			public System.String NumSec
			{
				get
				{
					return Convert.ToString(entity.NumSec);
				}

				set
				{
					entity.NumSec = Convert.ToString(value);
				}
			}
	
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String ServicioID
			{
				get
				{
					System.Decimal? data = entity.ServicioID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioID = null;
					else entity.ServicioID = Convert.ToDecimal(value);
				}
			}
			
			public System.String Estado
			{
				get
				{
					return Convert.ToString(entity.Estado);
				}

				set
				{
					entity.Estado = Convert.ToByte(value);
				}
			}

			public System.String Fecha
			{
				get
				{
					return Convert.ToString(entity.Fecha);
				}

				set
				{
					entity.Fecha = Convert.ToDateTime(value);
				}
			}

			public System.String ServerID
			{
				get
				{
					return Convert.ToString(entity.ServerID);
				}

				set
				{
					entity.ServerID = Convert.ToByte(value);
				}
			}

			public System.String FlagState
			{
				get
				{
					return Convert.ToString(entity.FlagState);
				}

				set
				{
					entity.FlagState = Convert.ToInt32(value);
				}
			}

			public System.String GpsLatitud
			{
				get
				{
					return Convert.ToString(entity.GpsLatitud);
				}

				set
				{
					entity.GpsLatitud = Convert.ToDouble(value);
				}
			}

			public System.String GpsLongitud
			{
				get
				{
					return Convert.ToString(entity.GpsLongitud);
				}

				set
				{
					entity.GpsLongitud = Convert.ToDouble(value);
				}
			}

			public System.String Odometro
			{
				get
				{
					return Convert.ToString(entity.Odometro);
				}

				set
				{
					entity.Odometro = Convert.ToDouble(value);
				}
			}

			public System.String GrupoCursadoRuta
			{
				get
				{
					return Convert.ToString(entity.GrupoCursadoRuta);
				}

				set
				{
					entity.GrupoCursadoRuta = Convert.ToString(value);
				}
			}
	
			public System.String PersonalID
			{
				get
				{
					System.Decimal? data = entity.PersonalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalID = null;
					else entity.PersonalID = Convert.ToDecimal(value);
				}
			}
			

			private esServiciosCambioEstado entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return ServiciosCambioEstadoMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public ServiciosCambioEstadoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ServiciosCambioEstadoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ServiciosCambioEstadoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(ServiciosCambioEstadoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private ServiciosCambioEstadoQ query;		
	}



	[Serializable]
	abstract public partial class esServiciosCambioEstadoCol : CollectionBase<ServiciosCambioEstado>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ServiciosCambioEstadoMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "ServiciosCambioEstadoCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public ServiciosCambioEstadoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ServiciosCambioEstadoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ServiciosCambioEstadoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ServiciosCambioEstadoQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(ServiciosCambioEstadoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((ServiciosCambioEstadoQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private ServiciosCambioEstadoQ query;
	}



	[Serializable]
	abstract public partial class esServiciosCambioEstadoQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return ServiciosCambioEstadoMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.Id,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID, esSystemType.Decimal));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.TerminalID,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.NumSec,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.NumSec, esSystemType.String));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.VehiculoID,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.ServicioID,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.ServicioID, esSystemType.Decimal));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.Estado,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.Estado, esSystemType.Byte));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.Fecha,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.Fecha, esSystemType.DateTime));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.ServerID,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.ServerID, esSystemType.Byte));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.FlagState,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.FlagState, esSystemType.Int32));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud, esSystemType.Double));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud, esSystemType.Double));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.Odometro,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.Odometro, esSystemType.Double));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta, esSystemType.String));
			_queryItems.Add(ServiciosCambioEstadoMetadata.ColumnNames.PersonalID,new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.PersonalID, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServicioCursoID
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem NumSec
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.NumSec, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServicioID
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.ServicioID, esSystemType.Decimal); }
		} 
		
		public esQueryItem Estado
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.Estado, esSystemType.Byte); }
		} 
		
		public esQueryItem Fecha
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.Fecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.ServerID, esSystemType.Byte); }
		} 
		
		public esQueryItem FlagState
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.FlagState, esSystemType.Int32); }
		} 
		
		public esQueryItem GpsLatitud
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsLongitud
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud, esSystemType.Double); }
		} 
		
		public esQueryItem Odometro
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.Odometro, esSystemType.Double); }
		} 
		
		public esQueryItem GrupoCursadoRuta
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta, esSystemType.String); }
		} 
		
		public esQueryItem PersonalID
		{
			get { return new esQueryItem(this, ServiciosCambioEstadoMetadata.ColumnNames.PersonalID, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class ServiciosCambioEstado : esServiciosCambioEstado
	{

				
		#region UpToServiciosEnCursoByServicioCursoID - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_ServiciosCambioEstado_ServiciosEnCurso
		/// </summary>

		[XmlIgnore]
					
		public ServiciosEnCurso UpToServiciosEnCursoByServicioCursoID
		{
			get
			{
				if (this.es.IsLazyLoadDisabled) return null;
				
				if(this._UpToServiciosEnCursoByServicioCursoID == null && ServicioCursoID != null)
				{
					this._UpToServiciosEnCursoByServicioCursoID = new ServiciosEnCurso();
					this._UpToServiciosEnCursoByServicioCursoID.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToServiciosEnCursoByServicioCursoID", this._UpToServiciosEnCursoByServicioCursoID);
					this._UpToServiciosEnCursoByServicioCursoID.Query.Where(this._UpToServiciosEnCursoByServicioCursoID.Query.Id == this.ServicioCursoID);
					this._UpToServiciosEnCursoByServicioCursoID.Query.Load();
				}	
				return this._UpToServiciosEnCursoByServicioCursoID;
			}
			
			set
			{
				this.RemovePreSave("UpToServiciosEnCursoByServicioCursoID");
				
				bool changed = this._UpToServiciosEnCursoByServicioCursoID != value;

				if(value == null)
				{
					this.ServicioCursoID = null;
					this._UpToServiciosEnCursoByServicioCursoID = null;
				}
				else
				{
					this.ServicioCursoID = value.Id;
					this._UpToServiciosEnCursoByServicioCursoID = value;
					this.SetPreSave("UpToServiciosEnCursoByServicioCursoID", this._UpToServiciosEnCursoByServicioCursoID);
				}
				
				if( changed )
				{
					this.OnPropertyChanged("UpToServiciosEnCursoByServicioCursoID");
				}
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToServiciosEnCursoByServicioCursoID != null)
			{
				this.ServicioCursoID = this._UpToServiciosEnCursoByServicioCursoID.Id;
			}
		}
		
	}
	



	[Serializable]
	public partial class ServiciosCambioEstadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ServiciosCambioEstadoMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 18;
			c.Description = "PK. Autonumerica";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.ServicioCursoID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.ServicioCursoID;
			c.NumericPrecision = 18;
			c.Description = "ID del Servicio respecto al SERVIDOR, FK de ServiciosEnCurso";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.TerminalID, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Terminal. Obtenido tabla GITS.TerminalesGPS";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.NumSec, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.NumSec;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.Description = "Identificador de la Trama, que envio este Registro.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.VehiculoID, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Vehiculo. Obtenido de GITS.Vehiculos";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.ServicioID, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.ServicioID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Servicio. Obtenido de GITS.dbo.Viajes.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.Estado, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.Estado;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Indica el estado del Servicio. Se realizan por pesos, siendo mayor miestras mas avanzado en su progreso este el Servicio.  99 - Especial. Indica que está pendiente de Anulacion.  1 - Asignado. Estado incial.  2 - Confirmado Terminal. (C para GITS)  3 - Confirmado Conductor (CC para GITS)  4 - En Curso. (E para GITS)  5 - Finalizado (R para GITS)";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.Fecha, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.Fecha;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.ServerID, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 3;
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.FlagState, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.FlagState;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			c.Description = "Se utiliza para marcar el registro con un información sobre su estado de Proceso. Sus valores seran definidos en codigo";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.GpsLatitud, 10, typeof(System.Double), esSystemType.Double);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.GpsLatitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.GpsLongitud, 11, typeof(System.Double), esSystemType.Double);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.GpsLongitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.Odometro, 12, typeof(System.Double), esSystemType.Double);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.Odometro;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT Zero  as 0;";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.GrupoCursadoRuta, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.GrupoCursadoRuta;
			c.CharacterMaxLength = 50;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(ServiciosCambioEstadoMetadata.ColumnNames.PersonalID, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ServiciosCambioEstadoMetadata.PropertyNames.PersonalID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Vehiculo. Obtenido de GITS.Vehiculos";
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public ServiciosCambioEstadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string ServicioCursoID = "ServicioCursoID";
			 public const string TerminalID = "TerminalID";
			 public const string NumSec = "NumSec";
			 public const string VehiculoID = "VehiculoID";
			 public const string ServicioID = "ServicioID";
			 public const string Estado = "Estado";
			 public const string Fecha = "Fecha";
			 public const string ServerID = "ServerID";
			 public const string FlagState = "FlagState";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string Odometro = "Odometro";
			 public const string GrupoCursadoRuta = "GrupoCursadoRuta";
			 public const string PersonalID = "PersonalID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string ServicioCursoID = "ServicioCursoID";
			 public const string TerminalID = "TerminalID";
			 public const string NumSec = "NumSec";
			 public const string VehiculoID = "VehiculoID";
			 public const string ServicioID = "ServicioID";
			 public const string Estado = "Estado";
			 public const string Fecha = "Fecha";
			 public const string ServerID = "ServerID";
			 public const string FlagState = "FlagState";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string Odometro = "Odometro";
			 public const string GrupoCursadoRuta = "GrupoCursadoRuta";
			 public const string PersonalID = "PersonalID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ServiciosCambioEstadoMetadata))
			{
				if(ServiciosCambioEstadoMetadata.mapDelegates == null)
				{
					ServiciosCambioEstadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ServiciosCambioEstadoMetadata.meta == null)
				{
					ServiciosCambioEstadoMetadata.meta = new ServiciosCambioEstadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServicioCursoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("NumSec", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServicioID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("Estado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Fecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ServerID", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("FlagState", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("GpsLatitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsLongitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("Odometro", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GrupoCursadoRuta", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PersonalID", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITSCOMUNICA";
				meta.Schema = "dbo";
				
				meta.Source = "ServiciosCambioEstado";
				meta.Destination = "ServiciosCambioEstado";
				
				meta.spInsert = "proc_ServiciosCambioEstadoInsert";				
				meta.spUpdate = "proc_ServiciosCambioEstadoUpdate";		
				meta.spDelete = "proc_ServiciosCambioEstadoDelete";
				meta.spLoadAll = "proc_ServiciosCambioEstadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ServiciosCambioEstadoLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ServiciosCambioEstadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
