﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Drawing;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using gcperu.Contract;
using gcperu.Server.Core.Comm;
using gcperu.Server.Core.DAL.GT;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.Core.Interfaces;
using gitspt.global.Core.Extension;
using gitspt.global.Core.Log;
using Gurock.SmartInspect;

namespace gcperu.Server.Core.DAL.GC
{
    #region Entity

    public partial class TerminalState : esTerminalState, IGeograficPosition
    {

        #region Static

        private static EmpresasCol _empresasCol;

        public static TerminalState GetByNS(string SerialNumber)
        {
            var ts = new TerminalStateCol();
            ts.Query.Where(ts.Query.TerminalNS == SerialNumber);
            if (ts.LoadAll())
            {
                return ts[0];
            }
            return null;
        }

        public static TerminalState GetByIMEI(string IMEI)
        {
            var ts = new TerminalStateCol();
            ts.Query.Where(ts.Query.TerminalIMEI == IMEI);
            if (ts.LoadAll())
            {
                return ts[0];
            }
            return null;
        }

        public static TerminalState GetByVehiculoID(decimal vehiuloId)
        {
            var ts = new TerminalStateCol();
            ts.Query.Where(ts.Query.VehiculoID == vehiuloId);
            if (ts.LoadAll())
            {
                return ts[0];
            }
            return null;
        }

        public static TerminalState GetBySN_IMEI(string SerialNumber, string IMEI)
        {
            var tsq = new TerminalStateQ();
            tsq.Where(tsq.TerminalNS == SerialNumber || tsq.TerminalNS == IMEI);

            var ts = new TerminalState();

            try
            {
                if (ts.Load(tsq))
                {
                    return ts;
                }
                return null;
            }
            catch (Exception ex)
            {
                TraceLog.LogException(new GCException("TerminalState.GetBySN_IMEI", ex));
                return null;
            }
        }

        /// <summary>
        /// Obtiene la informacion para el Terminal de Update Software selectiva.
        /// </summary>
        /// <param name="TerminalID"></param>
        /// <returns> Null si no esta incluido</returns>
        public static TerminalSelectiveSoftwareUpdate GetSelectiveUpdateInfo(decimal TerminalID)
        {
            var tu = new TerminalSelectiveSoftwareUpdateCol();
            tu.Query.Where(tu.Query.TerminalID == TerminalID);

            try
            {
                if (tu.LoadAll())
                    return tu[0];

                return null;

            }
            catch (Exception ex)
            {
                TraceLog.LogException(new Exception("ERROR DE DATABASE.", ex));
                return null;
            }
        }


        public static void CreatePeticionesSyncroMantenimientos(TerminalState ts,ISSTraceLog log)
        {
            //TODO: CreatePeticionesSyncroMantenimientos | Reformar todo este codigo a la nueva forma Reactive Extensions
            if (ts.FechaTerminalInfoMasActual == null)
            {
                //INDICA QUE ESTE TERMINAL SE ASIGNADO A UN VEHICULO NUEVO, Y POR TANTO HAY QUE MANDARLE LA CONFIGURACION PREDETERMINADA
                var tempconfig = TemplateConfiguration.GetDefault(ts.SoftwareTipoEnum);
                if (tempconfig == null)
                {
                    log.LogWarning(string.Format("[TERMINAL RECIEN ASIGNADO A UN NUEVO VEHICULO. ENVIAR PLANTILLA CONFIGURACION.] NO EXISTE DEFINIDA UNA PLANTILLA PREDETERMINADA PARA ESTE SOFTWARE ({0})\n{1}", ts.SoftwareTipo, ts));
                    return;
                }

                log.Main.LogColored(Level.Message, Color.CornflowerBlue, string.Format("[TERMINAL RECIEN ASIGNADO A UN NUEVO VEHICULO. ENVIAR PLANTILLA CONFIGURACION]. ENVIAMOS PLANTILLA [id={0}] DE CONFIGURACION 'POR DEFECTO' AL TERMINAL. {1}", tempconfig.Id, ts));

                var configdata = tempconfig.ConfigData;
                var templateId = 0; //Se le envia 0, para indicar que es la plantilla por defecto

                //Descomentar
                //ts.ColaSalida.AddItem(new ItemOutInfo(PeticionCodeSend.ConfiguracionSet, Constantes.SINPETICIONID, new TramaConfig(TramaTypeGenerica.ConfigSet, Constantes.SINPETICIONID, configdata, templateId)));
            }
        }

        public static void CreatePeticionesSyncroMantenimientos(TerminalSocketInfo ts)
        {
            CreatePeticionesSyncroMantenimientos(ts.TerminalStateBD,ts.Log);
        }



        #endregion

        public override void Save()
        {
            try
            {
                base.Save();
            }
            catch (esConcurrencyException _exception)
            {
                TraceLog.LogException(new Exception("ERROR DE CONCURRENCIA. SAVE TerminalState. REOBTENEMOS LOS DATOS", _exception));
                if (!this.LoadByPrimaryKey(this.TerminalID))
                    TraceLog.LogException(new Exception("ERROR REFRESCAR VALORES REGISTRO TERMINALDATASEND, DESPUES ERROR CONCURRENCIA. SAVE TerminalState. REOBTENEMOS LOS DATOS", _exception));
            }
        }


        public override string ToString()
        {
            //Copias todas las columnas
            var tx = new StringBuilder();
            foreach (esColumnMetadata column in this.es.Meta.Columns)
            {
                tx.AppendLine(string.Format("{0}:{1}", column.Name, this.GetColumn(column.Name)));
            }

            return tx.ToString();
        }

        #region Custom Propertys

        protected override List<esPropertyDescriptor> GetLocalBindingProperties()
        {
            return new List<esPropertyDescriptor>()
                {
                    new esPropertyDescriptor(this, "FunctionalStatus", typeof (FunctionalDeviceStatus)),
                    new esPropertyDescriptor(this, "OperationalStatus", typeof (OperationalDeviceStatus)),
                    new esPropertyDescriptor(this, "TerminalEstadoEnum", typeof (VehiculoEstado)),
                    new esPropertyDescriptor(this, "SoftwareTipoEnum", typeof (string))
                };
            //                    new esPropertyDescriptor(this, "InUpdateSelective", typeof (bool)),
            //                    new esPropertyDescriptor(this, "HasConfiguration", typeof (bool)),
                               //new esPropertyDescriptor(this, "EstadoConexion", typeof (bool)),
        }

        public bool EstadoConexion
        {
            get { return this.TerminalConnectedFrom.HasValue; }
        }

        public VehiculoEstado TerminalEstadoEnum
        {
            get
            {
                return this.TerminalEstado.NumToEnum(VehiculoEstado.Rojo);
            }
        }

        public FunctionalDeviceStatus FunctionalStatusEnum
        {
            get
            {
                //TODO: FunctionalStatusEnum. APLICAR NUMTOENUM A BD.COLUM
                return FunctionalDeviceStatus.Ready;
            }

        }

        public OperationalDeviceStatus OperationStatusEnum
        {
            get
            {
                //TODO: OperationStatusEnum. APLICAR NUMTOENUM A BD.COLUM
                return OperationalDeviceStatus.Running;
            }

        }

        public TerminalSoftwareTipo SoftwareTipoEnum
        {
            get
            {
                if (this.SoftwareTipo.HasValue)
                    return this.SoftwareTipo.Value.NumToEnum(TerminalSoftwareTipo.GNavWin8Dsk);

                return TerminalSoftwareTipo.GNavWin8Dsk;
            }
        }

        //public string Conductor
        //{
        //    get
        //    {
        //        try
        //        {
        //            if (!this.PersonalID.HasValue)
        //                return "";
        //            return Personal.GetByID(this.PersonalID.Value).PerNombreCompleto;
        //        }
        //        catch (Exception e)
        //        {
        //            return "";
        //        }
        //    }
        //}

        //public string Grupo
        //{
        //    get
        //    {
        //        if (this.UpToGrupoTerminalByGrupoID != null)
        //            return this.UpToGrupoTerminalByGrupoID.Nombre;

        //        return "";
        //    }
        //}

        public bool HasConfiguration
        {
            get
            {
                return (this.GetColumn("ConfigID") != null);
            }
        }

        //public bool InUpdateSelective
        //{
        //    get { return (this.TerminalSelectiveSoftwareUpdate.VehiculoMatricula != null); }
        //}

        #endregion

        //Necesario para la Interfaz. Este campo no existe fisicamente en la BD.
        public byte? GpsFixGeoTipoVia { get; set; }


        //private string _empresaNombre;
        //public string EmpresaNombre
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_empresaNombre))
        //        {
        //            if (_empresasCol==null)
        //            {
        //                _empresasCol = new EmpresasCol();
        //                _empresasCol.Query.Where(_empresasCol.Query.EmpEstado == 0);
        //                _empresasCol.LoadAll();
        //            }

        //            var emp = _empresasCol.FindByPrimaryKey(this.EmpresaID.Value);
        //            if (emp != null)
        //                _empresaNombre = emp.EmpNombre;
        //        }
        //        return _empresaNombre;
        //    }
        //}

        /// <summary>
        /// VERIFICA QUE EXISTA UNA TARJETA SIM, CON EL Nº PASADO EN PARAMETRO.
        /// 1º. Que el terminal tenga asignada esta tarjeta y que el Perfil mobil que utiliza el terminal sea el mismo, si no lo actualiza en la tarjeta.
        /// 2º. Si el terminal, no tiene esa tarjeta asignada, verifica si existe la tarjeta, si no la crea y lo asigna al terminal.
        /// </summary>
        /// <param name="ICCID">Nº de la tarjeta</param>
        /// <param name="mobileProfileID">Identificador del perfil mobil que está utilizando el Terminal</param>
        /// <returns>Perfil mobil que la tarjeta indica que el Terminal debe usar, en caso de que asi fuera.</returns>
        public void VerificarTarjetaSIM(string ICCID, string mobileProfileID)
        {

            bool verificarTarjetaSIM = false;

            try
            {

                var trmgps = TerminalGPS.Get(this.TerminalID);
                if (trmgps != null)
                {
                    if (trmgps.TjsCodPK != null)
                    {
                        var sim = TarjetaSIM.Get(trmgps.TjsCodPK.Value);
                        if (sim != null)
                        {
                            if (sim.TjsNumSerie.Trim() != ICCID)
                                verificarTarjetaSIM = true;
                            else
                            {
                                if (!string.IsNullOrEmpty(mobileProfileID))
                                    sim.MobileNetworkProfileID = mobileProfileID;

                                sim.FechaLastUse = DateTime.Now;
                                sim.Save();
                            }
                        }
                        else
                        {
                            verificarTarjetaSIM = true;
                        }

                    }
                    else
                    {
                        verificarTarjetaSIM = true;
                    }


                    if (verificarTarjetaSIM)
                    {
                        var simCol = new TarjetaSIMCol();
                        simCol.Query.Where(simCol.Query.TjsNumSerie == ICCID);

                        simCol.LoadAll();

                        TarjetaSIM sim;

                        if (simCol.Count == 0)
                        {
                            sim = new TarjetaSIM()
                            {
                                TjsEstado = 0,
                                TjsNumSerie = ICCID,
                                TjsProveedor = mobileProfileID,
                                Mcc = mobileProfileID.Length >= 6 ? mobileProfileID.Substring(0, 3) : "",
                                Mnc = mobileProfileID.Length >= 6 ? mobileProfileID.Substring(3, 3) : "",
                                TjsNumPin = "",
                                TjsNumPuk = "",
                                TjsNumTelefono = "",
                                TjsObservacion = "",
                                MobileNetworkProfileID = mobileProfileID
                            };

                            sim.FechaLastUse = DateTime.Now;
                            sim.Save();
                        }
                        else
                        {
                            sim = simCol[0];

                            if (string.IsNullOrEmpty(sim.MobileNetworkProfileID) || sim.MobileNetworkProfileID != mobileProfileID)
                            {
                                sim.MobileNetworkProfileID = mobileProfileID;
                                sim.TjsProveedor = mobileProfileID;
                                sim.Mcc = mobileProfileID.Length >= 6 ? mobileProfileID.Substring(0, 3) : "";
                                sim.Mnc = mobileProfileID.Length >= 6 ? mobileProfileID.Substring(3, 3) : "";
                                
                                sim.FechaLastUse = DateTime.Now;
                                sim.Save();
                            }

                        }

                        if (trmgps != null)
                        {
                            trmgps.TjsCodPK = sim.TjsCodPK;
                            trmgps.Save();
                            TraceLog.LogDebug(string.Format("VerificarTarjetaSIM. Asociamos tarjeta SIM: '{0}' al TerminalID: '{1}'", trmgps.TjsCodPK, this.VehiculoMatricula));
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                TraceLog.LogException("ERROR VerificarTarjetaSIM.", ex);
            }

        }
    }

    #endregion

    #region Collection

    public partial class TerminalStateCol : esTerminalStateCol
    {
        public void UpdateTerminalStateVehiculoInfoFromGITS()
        {

            //Obtenemos todos los registros de TErminaleState.
            try
            {
                var empCol = new EmpresasCol();
                empCol.Query.Where(empCol.Query.EmpEstado == 0);
                empCol.LoadAll();

                Vehiculo veh = null;
                Empresas emp = null;

                foreach (var ts in this)
                {
                    veh = Vehiculo.Get(ts.VehiculoID);
                    if (veh == null)
                        continue;

                    ts.VehiculoCodigoADM = veh.VehCSES;
                    ts.EmpresaID = veh.EmpCodPK;

                    emp = empCol.FindByPrimaryKey(ts.EmpresaID.Value);
                    if (emp == null)
                        continue;

                    ts.EmpresaColor = emp.EmpColor;
                }

                this.Save();
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR MODIFICAR REGISTROS TERMINALSTATE CON INFORMACIÓN DE LOS VEHICULOS EN EL GITS.");
            }
        }

    }

    #endregion

    #region Query

    public partial class TerminalStateQ : esTerminalStateQ
    {

    }

    #endregion
}