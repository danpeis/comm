﻿
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using gcperu.Server.Core.DAL.GC;

namespace gcperu.Server.Core.DAL.GC
{
    #region Entity

    public partial class TerminalConfiguration : esTerminalConfiguration
    {

        public static TerminalConfiguration GetConfigurationByVehiculoID(decimal vehiculoID)
        {
            var q = new TerminalConfigurationQ();
            q.Where(q.VehiculoID == vehiculoID);

            var tc = new TerminalConfigurationCol();
            if (tc.Load(q))
            {
                return tc[0];
            }
            else
                return null;
        }

    }

    #endregion

    #region Collection

    public partial class TerminalConfigurationCol : esTerminalConfigurationCol
    {

    }

    #endregion

    #region Query

    public partial class TerminalConfigurationQ : esTerminalConfigurationQ
    {

    }

    #endregion
}
