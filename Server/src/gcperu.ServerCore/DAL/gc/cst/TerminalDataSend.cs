﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.2.1214.0
EntitySpaces Driver  : SQL
Date Generated       : 10/03/2010 13:23:00
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data;
using EntitySpaces.DynamicQuery;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using gcperu.Server.Core.Exceptions;
using gitspt.global.Core.Extension;
using gitspt.global.Core.Log;
using GCPeru.Server.Core.Settings;
using Gurock.SmartInspect;
using String = System.String;

namespace gcperu.Server.Core.DAL.GC
{
    #region Entity

    public partial class TerminalDataSend : esTerminalDataSend
    {

        #region Static

        public static Dictionary<decimal, List<TerminalDataSend>> GetPeticionesSendToConnectedTerminals(IEnumerable<decimal> listaVehiculoConTerminalesConnectedYColaVaciaIds, ISSTraceLog log)
        {
            try
            {
                if (listaVehiculoConTerminalesConnectedYColaVaciaIds.Count() == 0) return null;

                DateTime fechaReSend = DateTime.Now.Subtract(new TimeSpan(0, ModuleConfig.AppConfig.Server.ComponentWatchReSendInterval, 0));

                var tdsq = new TerminalDataSendQ();
                tdsq.Where(tdsq.VehiculoID.In(listaVehiculoConTerminalesConnectedYColaVaciaIds));
                tdsq.Where(tdsq.Or(tdsq.LastTimeProcessed.IsNull(), tdsq.LastTimeProcessed <= fechaReSend));
                tdsq.Where((tdsq.PeticionCode != (int) PeticionCodeSend.Undefined));

                //tdsq.OrderBy(tdsq.Priority, esOrderByDirection.Descending); NO ORDENAR POR PRIORIDAD, SIEMPRE POR ORDEN DE ID Y ASCENDENTE.
                //ES FUNDAMENTAL ESTE ORDEN, PORQUE UN BORRADO, NO SE PRODUCE SI VA ANTES DE UNA INSERCION QUE ESTE BORRADO DEBIA BORRAR.
                tdsq.OrderBy(tdsq.VehiculoID, esOrderByDirection.Ascending);
                tdsq.OrderBy(tdsq.Id, esOrderByDirection.Ascending);

                var petxVeh = new Dictionary<decimal, List<TerminalDataSend>>();
                var tdsc = new TerminalDataSendCol();

                if (tdsc.Load(tdsq))
                {
                    /*AGRUPO POR VEHICULOID, Y CADA VEHIULO ES UN TERMINAL*/
                    decimal vehproc = 0;
                    int petTipo = 0;
                    bool agregadoALista = false;

                    var listgrupoveh = from peticiondtad in tdsc
                        group peticiondtad by peticiondtad.VehiculoID;

                    foreach (var peticionesXVeh in listgrupoveh)
                    {
                        vehproc = peticionesXVeh.Key.Value;
                        if (peticionesXVeh.Count() == 0)
                            continue;

                        petTipo = peticionesXVeh.First().PeticionCode;
                        var listPet = new List<TerminalDataSend>();

                        agregadoALista = false;

                        foreach (var pet in peticionesXVeh.OrderBy(p => p.Id))
                        {
                            if (petTipo == pet.PeticionCode)
                            {
                                //Agregamos
                                listPet.Add(pet);
                            }
                            else
                            {
                                //Esta peticion ya no es igual y por tanto agregamos este Vehiculo y su Lista de peticiones.
                                petxVeh.Add(vehproc, listPet);
                                agregadoALista = true;
                                break; //Salimos el ForEach
                            }
                        }

                        if (!agregadoALista)
                            petxVeh.Add(vehproc, listPet);

                    }

                    return petxVeh;
                }


                return null;
            }
            catch (Exception e)
            {
                log.LogException(new GCException("GetPeticionesSendToConnectedTerminals.",
                    string.Format("Error al obtener las Peticiones a enviar a los Terminales Conectados. ListaVehiculosIDs={0}", listaVehiculoConTerminalesConnectedYColaVaciaIds), e, null));

                return null;
            }

        }

        public static bool CreatePeticionSend(decimal VehiculoID, PeticionCodeSend peticionTipo, byte priority, String data, string data1, ISSTraceLog log)
        {
            try
            {
                var dsq = new TerminalDataSendQ();
                var ds = new TerminalDataSend();

                if (peticionTipo == PeticionCodeSend.ConfiguracionSet)
                {
                    //Buscamos que haya uno para obtener la configuracion y lo eliminamos, porque queremos actualizar la configuracion del Terminal.
                    dsq.Where(dsq.VehiculoID == VehiculoID, dsq.PeticionCode == (int) PeticionCodeSend.ConfiguracionGet);
                    if (ds.Load(dsq))
                    {
                        ds.MarkAsDeleted();
                        ds.Save();
                    }

                    //Buscamos que haya uno de esta misma peticion.
                    dsq = new TerminalDataSendQ();
                    dsq.Where(dsq.VehiculoID == VehiculoID, dsq.PeticionCode == peticionTipo);
                }

                else
                    dsq.Where(dsq.VehiculoID == VehiculoID, dsq.PeticionCode == peticionTipo, dsq.Data == data, dsq.Data1 == data1);


                if (ds.Load(dsq))
                {
                    TraceLog.LogWarning(string.Format("CreatePeticionSend. Ya existe una peticion igual en la lista por enviar. {0}", ds), Color.Gainsboro);
                    if (peticionTipo == PeticionCodeSend.ConfiguracionSet)
                    {
                        //Borramos y actualizamos con la nueva.
                        ds.MarkAsDeleted();
                        ds.Save();
                    }
                }

                ds = new TerminalDataSend();

                ds.Fecha = DateTime.Now;
                ds.ServicioID = "";
                ds.PeticionCode = (int) peticionTipo;
                ds.VehiculoID = VehiculoID;
                ds.Priority = priority;
                ds.Data = data;
                ds.Data1 = data1;

                ds.Save();

                return true;
            }
            catch (Exception e)
            {
                log.LogException(new GCException(string.Format("ERROR CREAR PETICION ENVIAR A VEHICULO {0}", VehiculoID), e));
                return false;
            }
        }

        public static bool SetLastTimeProcessed(decimal id, DateTime timeprocessed, decimal TerminalID, ISSTraceLog log)
        {
            var reg = new TerminalDataSend();

            try
            {
                if (reg.LoadByPrimaryKey(id))
                {
                    reg.LastTimeProcessed = timeprocessed;
                    reg.IntentosProcessed += 1;
                    reg.TerminalID = TerminalID;
                    reg.ServerID = (short) ModuleConfig.AppConfig.Server.ServerNumber;
                    reg.Save();

                }
                return true; //Si la encuentro como si no, es correcta el Borrado.
            }
            catch (Exception e)
            {
                log.LogException(new GCException("ERROR BORRAR PETICION A TERMINAL.", e));
                return false;
            }
        }

        public static bool SetServiciosNoEnviados(decimal id, string txNoEnviados, ISSTraceLog log)
        {
            var reg = new TerminalDataSend();

            try
            {
                if (reg.LoadByPrimaryKey(id))
                {
                    reg.NoEnviados = txNoEnviados;
                    reg.Save();

                }
                return true; //Si la encuentro como si no, es correcta el Borrado.
            }
            catch (Exception e)
            {
                log.LogException(new GCException(string.Format("ERROR GUARDAR PETICION CON SetServiciosNoEnviados: {0}", txNoEnviados), e));
                return false;
            }
        }

        public static bool Remove(decimal id, bool HandleException, ISSTraceLog log)
        {
            var reg = new TerminalDataSend();

            try
            {
                if (reg.LoadByPrimaryKey(id))
                {
                    reg.MarkAsDeleted();
                    reg.Save();

                }
                return true; //Si la encuentro como si no, es correcta el Borrado.
            }
            catch (Exception e)
            {
                var ne = new GCException("ERROR BORRAR PETICION A TERMINAL.", e);
                if (!HandleException)
                    throw ne;

                log.LogException(ne);
                return false;
            }
        }

        /// <summary>
        /// Mueve al Historico la Peticion al Terminal. Puede ser por 2 motivos.
        /// 1. Peticion enviada/confirmada por Termina.
        /// 2. Peticion antigua, y no se ha podido enviar al Terminal.
        /// </summary>
        /// <returns></returns>
        public static bool MoveToHistorico(decimal petID, MotivoPeticionMoveToHistorico motivo, bool HandleException, ISSTraceLog log)
        {
            var pet = new TerminalDataSend();
            if (pet.LoadByPrimaryKey(petID))
            {
                return MoveToHistorico(pet, motivo, HandleException, log);
            }

            log.LogWarning(string.Format("ERROR: MOVER PETICIÓN A HISTORICO. NO SE HA PODIDO OBTENER LA PETICICÓN {0}", petID), Color.Gainsboro);
            return true;
        }


        /// <summary>
        ///HAY QUE MARCAR LAS TRAMAS EN TABLA {TERMINALDATASEND} COMO RECIBIDAS POR EL TERMINAL.
        ///ESTO ES INTERSANTE, PARA PODER BORRARLAS PORSI EL TERMINAL NO PUEDE PROCESARLAS YA QUE NO LAS RECONOCE.
        ///DE ESTA FORMA, PODREMOS BORRARLAS SABIENDO BIEN QUE HAN SIDO RECIBIDAS POR EL TERMINAL.
        /// </summary>
        /// <param name="cola">La lista de las tramas que estan en la cola</param>
        /// <returns></returns>
        public static bool SetPeticionesRecibidasPorTerminal()
        {
            //TODO: SetPeticionesRecibidasPorTerminal (Asignar Peticiones Enviadas como recibidas)
            try
            {
                var tx = new StringBuilder();
                var q = new TerminalDataSendQ();

                //q.Select(q.Id, q.RecibidaPorTerminal, q.PeticionCode);
                //q.Where(q.Id.In(cola.Select(t => t.PeticionEnvioID)));

                var peticiones = new TerminalDataSendCol();
                if (peticiones.Load(q))
                {
                    //Actualizamos el Contador de recibidas.
                    foreach (var ptc in peticiones)
                    {
                        ptc.RecibidaPorTerminal++;
                        tx.Append(string.Format("| Peticion Envio:{0} recibida:{1} veces |", ptc.Id, ptc.RecibidaPorTerminal));
                    }

                    peticiones.Save();
                }

                //TraceLog.Main.LogColored(Gurock.SmartInspect.Level.Debug, System.Drawing.Color.Salmon, string.Format("PETICION RECIBIDAS POR EL TERMINAL:{0}\n{1}", cola.TerminalSocketInfo, tx.ToString()));

                return true;
            }
            catch (Exception ex)
            {
                //TraceLog.LogException(string.Format("[SetPeticionesRecibidasPorTerminal]. ERROR MARCAR PETICIONES [{0}] COMO RECIBIDAS POR EL TERMINAL.", cola.Select(t => t.PeticionCode).ToList()), ex);
                return false;
            }
        }

        public static bool MoveToHistoricoNoSoportada(ISSTraceLog log)
        {
            //TODO: MoveToHistoricoNoSoportada (Asignar Peticiones Enviadas como recibidas)
            PeticionCodeSend peticionTipo;

            var listaPetTipos = new List<byte>();

            ////Hay que obtener el PeticionTipo del TramaKey.
            //switch (trama.TramaTipoGenericaNoSoportada)
            //{
            //    case TramaTypeGenerica.ServicioAdd:
            //        listaPetTipos.Add((byte)PeticionCodeSend.ServicioAlta);
            //        break;

            //    case TramaTypeGenerica.ServicioModify:
            //        listaPetTipos.Add((byte)PeticionCodeSend.ServicioCambio);
            //        break;

            //    case TramaTypeGenerica.ServicioRemove:
            //        listaPetTipos.Add((byte)PeticionCodeSend.ServicioBaja);
            //        break;

            //    case TramaTypeGenerica.IncidenciaLista:
            //        listaPetTipos.Add((byte)PeticionCodeSend.IncidenciasSend);
            //        break;

            //    case TramaTypeGenerica.ConfigSet:
            //        listaPetTipos.Add((byte)PeticionCodeSend.ConfiguracionSet);
            //        break;

            //    case TramaTypeGenerica.ConfigGet:
            //        listaPetTipos.Add((byte)PeticionCodeSend.ConfiguracionGet);
            //        break;

            //    case TramaTypeGenerica.ActionSOS:
            //        listaPetTipos.Add((byte)PeticionCodeSend.AutoConnectSOS);
            //        break;

            //    case TramaTypeGenerica.ServicioEspecialTipoLista:
            //        listaPetTipos.Add((byte)PeticionCodeSend.ServiciosEspecialesTiposSend);
            //        break;

            //    case TramaTypeGenerica.FichMaestroCollection:
            //        //HAY IRIAN TODOS LOS FICHEROS MAESTROS QUE SE ENVIARAN A LOS TERMINALES.
            //        listaPetTipos.Add((byte)PeticionCodeSend.TiposActividadAmbulanciaSend);
            //        break;

            //    default:
            //        return false;
            //}

            //Hay que buscar las peticiones que esten recibidas y IntentosProcessed>0
            try
            {
                var q = new TerminalDataSendQ();

                q.Where(q.IntentosProcessed > 0 && q.RecibidaPorTerminal > 0 && q.PeticionCode.In(listaPetTipos));

                var peticiones = new TerminalDataSendCol();
                if (peticiones.Load(q))
                {
                    //LAS PETICIONES QUE ENCONTREMOS LAS ENVIAMOS AL HISTORICO INCIDANCO EL MOTIVO.
                    foreach (var pet in peticiones)
                    {
                        MoveToHistorico(pet, MotivoPeticionMoveToHistorico.PeticionUndefined, false, log);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                //TraceLog.LogException(string.Format("[MoveToHistoricoNoSoportada]. ERROR OBTENER PETICIONES POR NO SOPORTADA POR TERMINAL. TRAMA NO SOPORTADA={0}.", trama.TramaTipoGenericaNoSoportada), ex, Color.Gainsboro);
                return false;
            }
        }

        /// <summary>
        /// Mueve al Historico la Peticion al Terminal. Puede ser por 2 motivos.
        /// 1. Peticion enviada/confirmada por Termina.
        /// 2. Peticion antigua, y no se ha podido enviar al Terminal.
        /// </summary>
        /// <returns></returns>
        public static bool MoveToHistorico(TerminalDataSend pet, MotivoPeticionMoveToHistorico motivo, bool HandleException, ISSTraceLog log)
        {
            try
            {
                using (var tx = new esTransactionScope(esTransactionScopeOption.Required))
                {
                    bool pasoHistorico = false;
                    string noPasoPor = "";
                    decimal peticionID = pet.Id;
                    string petInfo = pet.ToString();
                    try
                    {
                        //Creamos el registro en HistoricoTerminalDataSend.
                        var hpet = new HistoricoTerminalDataSend();

                        hpet.Id = pet.Id;
                        hpet.Fecha = pet.Fecha;
                        hpet.ServicioID = pet.ServicioID;
                        hpet.Data = pet.Data;
                        hpet.Data1 = pet.Data1;
                        hpet.PeticionCode = pet.PeticionCode;
                        hpet.TerminalID = pet.TerminalID;
                        hpet.VehiculoID = pet.VehiculoID;
                        hpet.Priority = pet.Priority;
                        hpet.FlagState = pet.FlagState;
                        hpet.LastTimeProcessed = pet.LastTimeProcessed;
                        hpet.IntentosProcessed = pet.IntentosProcessed;
                        hpet.RecibidaPorTerminal = pet.RecibidaPorTerminal;
                        hpet.FechaEntroHistorico = DateTime.Now;
                        hpet.MotivoEntroHistorico = (byte) motivo;
                        hpet.NoEnviados = pet.NoEnviados;
                        hpet.RecibidaPorTerminal = pet.RecibidaPorTerminal;
                        hpet.ServerID = pet.ServerID;

                        //Creo el del historico
                        hpet.Save();
                        pasoHistorico = true;
                    }
                    catch (Exception ex)
                    {
                        if (ex.ToString().Contains("PRIMARY KEY"))
                        {
                            //LA TRAMA YA ESTÁ PASADA AL HISTORICO. LA BORRAMOS
                            pasoHistorico = true;
                        }
                        else
                        {
                            pasoHistorico = false;
                            log.LogException("MOVER [Peticion=>Historico]", ex);
                            noPasoPor = new GCException("MOVER [Peticion=>Historico]", ex).ToString();
                        }
                    }

                    //Borro la Peticion. Se ha pasado al Historico
                    if (pasoHistorico)
                    {
                        try
                        {
                            pet.MarkAsDeleted();
                            pet.Save();

                            tx.Complete(); //confirmamos. PETICION ESTÁ EN HISTORICO y SE HA BORRADO DEL DIARIO.
                            log.Main.LogColored(Level.Debug, Color.Gainsboro, string.Format("PETICION {0}.ENVIADA AL HISTORICO.\n{1}\n{2}", peticionID, petInfo, motivo.ToString()));
                        }
                        catch (Exception ex)
                        {
                            if (SetPeticionDisabled(pet.Id, new GCException("BORRAR PETICION. PASADA YA A HISTORICO. [Peticion=>Historico]", ex).ToString(), log))
                            {
                                tx.Complete(); //confirmamos. PETICION ESTÁ EN HISTORICO Y EN DIARIO, CON EL ERROR/PeticionCode=-1
                                log.Main.LogColored(Level.Warning, Color.Gainsboro, string.Format("PETICION {0}, ENVIADA AL HISTORICO, PERO NO SE PUDO ELIMINAR DEL DIARIO.\n{1}\n{2}\n{3}", peticionID,
                                    petInfo, motivo.ToString(), pet.NoPasoHistoricoPor));
                            }
                        }
                    }
                    else
                    {
                        if (SetPeticionDisabled(pet.Id, noPasoPor, log))
                        {
                            tx.Complete();
                            log.Main.LogColored(Level.Warning, Color.Gainsboro, string.Format("PETICION {0}, NO HA PODIDO SER ENVIADA AL HISTORICO, SE CAMBIA EN EL DIARIO A -1.\n{1}\n{2}\n{3}", peticionID,
                                petInfo, motivo.ToString(), pet.NoPasoHistoricoPor));
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                var ne = new GCException("ERROR MOVER [Peticion=>Historico].", ex);
                if (!HandleException)
                    throw ne;

                log.LogException(ne);
                return false;
            }
        }

        private static bool SetPeticionDisabled(decimal peticionId, string motivo, ISSTraceLog log)
        {
            var pet = new TerminalDataSend();
            try
            {
                if (pet.LoadByPrimaryKey(peticionId))
                {
                    pet.PeticionCode = -1;
                    pet.NoPasoHistoricoPor = motivo;
                    pet.Save();

                    return true;
                }
                else
                {
                    log.Main.LogColored(Level.Error, Color.Gainsboro, string.Format("ERROR.SetPeticionDisabled: NO SE PUDO LOCALIZAR LA PETICION. PARA DESACTIVARLA. {0}\n{1}", pet, motivo));
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Main.LogColored(Level.Error, Color.Gainsboro, string.Format("ERROR.SetPeticionDisabled: DESACTIVAR PETICION POR NO PODER PASARLA AL HISTORICO. {0}\n{1}", pet, motivo));
                return false;
            }


        }

        #endregion

        #region Custom Properties

        protected override List<esPropertyDescriptor> GetLocalBindingProperties()
        {
            return new List<esPropertyDescriptor>()
            {
                new esPropertyDescriptor(this, "PeticionCodeEnum", typeof (PeticionCodeSend)),
            };
        }

        public PeticionCodeSend PeticionCodeEnum
        {
            get
            {
                if (PeticionCode == null)
                    return PeticionCodeSend.Undefined;

                return PeticionCode.NumToEnum(PeticionCodeSend.Undefined);
            }
        }

        public override string ToString()
        {
            //Copias todas las columnas
            var tx = new StringBuilder();
            foreach (esColumnMetadata column in this.es.Meta.Columns)
            {
                tx.AppendLine(string.Format("{0}:{1}", column.Name, this.GetColumn(column.Name)));
            }

            return tx.ToString();
        }

        #endregion


    }

    #endregion

    #region Collection

    public partial class TerminalDataSendCol : esTerminalDataSendCol
    {

    }

    #endregion

    #region Query

    public partial class TerminalDataSendQ : esTerminalDataSendQ
    {

    }

   #endregion

}
