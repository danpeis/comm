﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using gcperu.Contract;
using gitspt.global.Core.Extension;

namespace gcperu.Server.Core.DAL.GC
{
	#region Entity
	
	public partial class TemplateConfiguration : esTemplateConfiguration
	{
	    public override void Save()
	    {

            if (this.IsNew)
            {
                if (this.Codigo==null || this.Codigo<=0)
                {
                    //Calcular el siguiente Codigo.
                    var calc = new TemplateConfigurationCol();
                    calc.Query.Select(calc.Query.Id.Max().As("MaximoId"));
                    calc.Load(calc.Query);

                    if (calc.Count==0)
                        throw new Exception(string.Format("ERROR CALCULAR NUEVO ID PARA NUEVO REGISTRO DE [{0}].\n\nNingún dato devuelto.", calc.es.Source));

                    Codigo= calc[0].GetColumn("MaximoId").ToInt() + 1;
                    
                    if (Codigo==null)
                        throw new Exception(string.Format("ERROR CALCULAR NUEVO ID PARA NUEVO REGISTRO DE [{0}]", calc.es.Source));

                    Id = Codigo;

                }
            }

            base.Save();

	    }

        /// <summary>
        /// Guarda la plantilla y gestiona el estado de Predeterminada. (Si la pasada como parametro ahora lo es, asigna la id inferior)
        /// </summary>
        /// <param name="row"></param>
        public static void SaveTemplate(TemplateConfiguration row)
        {
            using (var tx = new esTransactionScope(esTransactionScopeOption.Required))
            {

                if (row.ColumnModified(TemplateConfigurationMetadata.ColumnNames.Predeterminada))
                {
                    //SOLO SI SE HA MODIFICADO LA COLUMNA PREDETERMINADA.
                    var templC = new TemplateConfigurationCol();
                    templC.Query.Where(templC.Query.SoftwareTipo == row.SoftwareTipo);
                    templC.LoadAll();

                    foreach (var temp in templC)
                    {
                        temp.Predeterminada = false;
                    }

                    //SI AHORA NO LO ES SE PONE COMO PREDETERMINADA, LA DE ID INFERIOR
                    if (!row.Predeterminada)
                    {
                        var rowdefault = templC.OrderBy(r => r.Id).FirstOrDefault();
                        if (rowdefault != null)
                            rowdefault.Predeterminada = true;
                    }

                    templC.Save();
                }

                row.Save();

                tx.Complete();
            }
        }

        protected override List<esPropertyDescriptor> GetLocalBindingProperties()
        {
            return new List<esPropertyDescriptor>()
                {
                    new esPropertyDescriptor(this, "SoftwareTipoEnum", typeof (string))
                };
        }

        public TerminalSoftwareTipo SoftwareTipoEnum
        {
            get
            {
               return this.SoftwareTipo.NumToEnum(TerminalSoftwareTipo.GNavWin8Dsk);
            }
        }

	    public static TemplateConfiguration GetDefault(TerminalSoftwareTipo softtipo)
	    {
            var templC = new TemplateConfigurationCol();
            templC.Query.Where(templC.Query.SoftwareTipo == (byte) softtipo && templC.Query.Predeterminada==true);
	        if (templC.LoadAll())
	            return templC[0];

	        return null;
	    }
	}
	
	#endregion
	
	#region Collection

    public partial class TemplateConfigurationCol : esTemplateConfigurationCol
	{

	}
	
	#endregion
	
	#region Query

    public partial class TemplateConfigurationQ : esTemplateConfigurationQ
	{

	}
	
	#endregion
}
