
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 10:51:49
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GCG
{
	/// <summary>
	/// GUARDA CADA POSICION GPS EMITIDA POR LOS TERMINALES. ESTA POSICION PUEDE SER ENVIADA PERIODICAMENTE, O CUANDO OCURRE UN HECHO DESTACABLE. (Cursar un Servicio, Finalizar, ... Motor Parado, Motor en Marcha, ....)
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("HTerminalGpsInfoData")]
	public partial class HTerminalGpsInfoData : esHTerminalGpsInfoData
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new HTerminalGpsInfoData();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new HTerminalGpsInfoData();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new HTerminalGpsInfoData();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new HTerminalGpsInfoData();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static HTerminalGpsInfoData Get(System.Decimal ID)
        {
            try
            {
                var e = new HTerminalGpsInfoData();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public HTerminalGpsInfoData(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public HTerminalGpsInfoData()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GCG";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("HTerminalGpsInfoDataCol")]
	public partial class HTerminalGpsInfoDataCol : esHTerminalGpsInfoDataCol, IEnumerable<HTerminalGpsInfoData>
	{
	
		public HTerminalGpsInfoDataCol()
		{
		}

	
	
		public HTerminalGpsInfoData FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(HTerminalGpsInfoData))]
		public class HTerminalGpsInfoDataColWCFPacket : esCollectionWCFPacket<HTerminalGpsInfoDataCol>
		{
			public static implicit operator HTerminalGpsInfoDataCol(HTerminalGpsInfoDataColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator HTerminalGpsInfoDataColWCFPacket(HTerminalGpsInfoDataCol collection)
			{
				return new HTerminalGpsInfoDataColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GCG";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class HTerminalGpsInfoDataQ : esHTerminalGpsInfoDataQ
	{
		public HTerminalGpsInfoDataQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public HTerminalGpsInfoDataQ()
		{
		}

		override protected string GetQueryName()
		{
			return "HTerminalGpsInfoDataQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GCG";
		}

		public override QueryBase CreateQuery()
        {
            return new HTerminalGpsInfoDataQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(HTerminalGpsInfoDataQ query)
		{
			return HTerminalGpsInfoDataQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator HTerminalGpsInfoDataQ(string query)
		{
			return (HTerminalGpsInfoDataQ)HTerminalGpsInfoDataQ.SerializeHelper.FromXml(query, typeof(HTerminalGpsInfoDataQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esHTerminalGpsInfoData : EntityBase
	{
		public esHTerminalGpsInfoData()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			HTerminalGpsInfoDataQ query = new HTerminalGpsInfoDataQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalID
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.VehiculoCodigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigo
		{
			get
			{
				return base.GetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo);
			}
			
			set
			{
				if(base.SetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.VehiculoCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.VehiculoCodigoADM
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigoADM
		{
			get
			{
				return base.GetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM);
			}
			
			set
			{
				if(base.SetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.VehiculoCodigoADM);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.VehiculoMatricula
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoMatricula
		{
			get
			{
				return base.GetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula);
			}
			
			set
			{
				if(base.SetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.VehiculoMatricula);
				}
			}
		}		
		
		/// <summary>
		/// Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? CentroTrabajoID
		{
			get
			{
				return base.GetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID);
			}
			
			set
			{
				if(base.SetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.CentroTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.AreaTrabajoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? AreaTrabajoID
		{
			get
			{
				return base.GetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID);
			}
			
			set
			{
				if(base.SetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.AreaTrabajoColor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreaTrabajoColor
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoColor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.EmpresaID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? EmpresaID
		{
			get
			{
				return base.GetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaID);
			}
			
			set
			{
				if(base.SetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.EmpresaID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.EmpresaColor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpresaColor
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.EmpresaColor);
				}
			}
		}		
		
		/// <summary>
		/// Conductor Logado actualmente en el Vehiculo.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? PersonalID
		{
			get
			{
				return base.GetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.PersonalID);
			}
			
			set
			{
				if(base.SetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.PersonalID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.PersonalID);
				}
			}
		}		
		
		/// <summary>
		/// Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? TerminalEstado
		{
			get
			{
				return base.GetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado);
			}
			
			set
			{
				if(base.SetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TerminalEstado);
				}
			}
		}		
		
		/// <summary>
		/// Nº de Secuencia de la Trama, que envio este registro de Posicionamiento.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String NumSec
		{
			get
			{
				return base.GetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.NumSec);
			}
			
			set
			{
				if(base.SetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.NumSec, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.NumSec);
				}
			}
		}		
		
		/// <summary>
		/// Tipo de Origen que genero el Registro. 0- Trama de Localizacion. 1- Servicio Cambio de Estado. 2- Vehiculo/Terminal Cambio Estado.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? OrigenTipo
		{
			get
			{
				return base.GetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo);
			}
			
			set
			{
				if(base.SetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.OrigenTipo);
				}
			}
		}		
		
		/// <summary>
		/// Identificacion del Servicio. Obtenido de GITS.dbo.Viajes.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? ServicioID
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.ServicioID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.ServicioID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.ServicioID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.ServicioEstado
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? ServicioEstado
		{
			get
			{
				return base.GetSystemInt16(HTerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado);
			}
			
			set
			{
				if(base.SetSystemInt16(HTerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.ServicioEstado);
				}
			}
		}		
		
		/// <summary>
		/// Fecha en la cual el Servidor obtuvo la informacion y la Inserto en la BD. La fecha de la Posicion obtendia es GpsFecha.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaServerGet
		{
			get
			{
				return base.GetSystemDateTime(HTerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet);
			}
			
			set
			{
				if(base.SetSystemDateTime(HTerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.FechaServerGet);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la posicion GPS tomada por el Terminal. Si la posicion es Invalida, está fecha sera la del Terminal. El Terminal se sincroniza con el Reloj de Terminal, cada intervalo, definido en el Terminal. 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? GpsFecha
		{
			get
			{
				return base.GetSystemDateTime(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFecha, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsFecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.GpsLatitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? GpsLatitud
		{
			get
			{
				return base.GetSystemDouble(HTerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud);
			}
			
			set
			{
				if(base.SetSystemDouble(HTerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsLatitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.GpsLongitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? GpsLongitud
		{
			get
			{
				return base.GetSystemDouble(HTerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud);
			}
			
			set
			{
				if(base.SetSystemDouble(HTerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsLongitud);
				}
			}
		}		
		
		/// <summary>
		/// Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? GpsFixMode
		{
			get
			{
				return base.GetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode);
			}
			
			set
			{
				if(base.SetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsFixMode);
				}
			}
		}		
		
		/// <summary>
		/// Georefencia textual de la posicion obtenida en GPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GpsFixGeoTx
		{
			get
			{
				return base.GetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx);
			}
			
			set
			{
				if(base.SetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTx);
				}
			}
		}		
		
		/// <summary>
		/// 0:No concocido 1: Autovia 2:Nacional 3:Comarcal 4:Calle 5:Plaza
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? GpsFixGeoTipoVia
		{
			get
			{
				return base.GetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia);
			}
			
			set
			{
				if(base.SetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTipoVia);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.GpsNSAT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? GpsNSAT
		{
			get
			{
				return base.GetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT);
			}
			
			set
			{
				if(base.SetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsNSAT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.GpsAltitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? GpsAltitud
		{
			get
			{
				return base.GetSystemInt16(HTerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud);
			}
			
			set
			{
				if(base.SetSystemInt16(HTerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsAltitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.GpsRumbo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? GpsRumbo
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsRumbo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.GpsVelocidad
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? GpsVelocidad
		{
			get
			{
				return base.GetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad);
			}
			
			set
			{
				if(base.SetSystemInt32(HTerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsVelocidad);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.GpsDistancia
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? GpsDistancia
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsDistancia);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.GpsPoblacion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GpsPoblacion
		{
			get
			{
				return base.GetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion);
			}
			
			set
			{
				if(base.SetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsPoblacion);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Total
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? OdometroTt
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.OdometroTt);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.OdometroTt, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.OdometroTt);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Parcial
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? OdometroPc
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.OdometroPc);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.OdometroPc, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.OdometroPc);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de entrada del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String InputState
		{
			get
			{
				return base.GetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.InputState);
			}
			
			set
			{
				if(base.SetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.InputState, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.InputState);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de salida del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String OutputState
		{
			get
			{
				return base.GetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.OutputState);
			}
			
			set
			{
				if(base.SetSystemString(HTerminalGpsInfoDataMetadata.ColumnNames.OutputState, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.OutputState);
				}
			}
		}		
		
		/// <summary>
		/// Servidor que gestiona la conexión de este Terminal. Esta vinculado a la conexion. Si estuviera desconectado,  este valor seria null.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? ServerID
		{
			get
			{
				return base.GetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.TryProcessed
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? TryProcessed
		{
			get
			{
				return base.GetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.TryProcessed);
			}
			
			set
			{
				if(base.SetSystemByte(HTerminalGpsInfoDataMetadata.ColumnNames.TryProcessed, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TryProcessed);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.TramoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TramoID
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.TramoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.TramoID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TramoID);
				}
			}
		}		
		
		/// <summary>
		/// Indica si el Terminal esta en estado Especial: 1:Comida  2:Mantenimiento
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EstadoEspecial
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.EstadoEspecial);
				}
			}
		}		
		
		/// <summary>
		/// Maps to HTerminalGpsInfoData.TipoActividadID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TipoActividadID
		{
			get
			{
				return base.GetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID);
			}
			
			set
			{
				if(base.SetSystemDecimal(HTerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID, value))
				{
					OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TipoActividadID);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "VehiculoCodigo": this.str().VehiculoCodigo = (string)value; break;							
						case "VehiculoCodigoADM": this.str().VehiculoCodigoADM = (string)value; break;							
						case "VehiculoMatricula": this.str().VehiculoMatricula = (string)value; break;							
						case "CentroTrabajoID": this.str().CentroTrabajoID = (string)value; break;							
						case "AreaTrabajoID": this.str().AreaTrabajoID = (string)value; break;							
						case "AreaTrabajoColor": this.str().AreaTrabajoColor = (string)value; break;							
						case "EmpresaID": this.str().EmpresaID = (string)value; break;							
						case "EmpresaColor": this.str().EmpresaColor = (string)value; break;							
						case "PersonalID": this.str().PersonalID = (string)value; break;							
						case "TerminalEstado": this.str().TerminalEstado = (string)value; break;							
						case "NumSec": this.str().NumSec = (string)value; break;							
						case "OrigenTipo": this.str().OrigenTipo = (string)value; break;							
						case "ServicioID": this.str().ServicioID = (string)value; break;							
						case "ServicioEstado": this.str().ServicioEstado = (string)value; break;							
						case "FechaServerGet": this.str().FechaServerGet = (string)value; break;							
						case "GpsFecha": this.str().GpsFecha = (string)value; break;							
						case "GpsLatitud": this.str().GpsLatitud = (string)value; break;							
						case "GpsLongitud": this.str().GpsLongitud = (string)value; break;							
						case "GpsFixMode": this.str().GpsFixMode = (string)value; break;							
						case "GpsFixGeoTx": this.str().GpsFixGeoTx = (string)value; break;							
						case "GpsFixGeoTipoVia": this.str().GpsFixGeoTipoVia = (string)value; break;							
						case "GpsNSAT": this.str().GpsNSAT = (string)value; break;							
						case "GpsAltitud": this.str().GpsAltitud = (string)value; break;							
						case "GpsRumbo": this.str().GpsRumbo = (string)value; break;							
						case "GpsVelocidad": this.str().GpsVelocidad = (string)value; break;							
						case "GpsDistancia": this.str().GpsDistancia = (string)value; break;							
						case "GpsPoblacion": this.str().GpsPoblacion = (string)value; break;							
						case "OdometroTt": this.str().OdometroTt = (string)value; break;							
						case "OdometroPc": this.str().OdometroPc = (string)value; break;							
						case "InputState": this.str().InputState = (string)value; break;							
						case "OutputState": this.str().OutputState = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;							
						case "TryProcessed": this.str().TryProcessed = (string)value; break;							
						case "TramoID": this.str().TramoID = (string)value; break;							
						case "EstadoEspecial": this.str().EstadoEspecial = (string)value; break;							
						case "TipoActividadID": this.str().TipoActividadID = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.Id);
							break;
						
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.VehiculoID);
							break;
						
						case "CentroTrabajoID":
						
							if (value == null || value is System.Int32)
								this.CentroTrabajoID = (System.Int32?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.CentroTrabajoID);
							break;
						
						case "AreaTrabajoID":
						
							if (value == null || value is System.Int32)
								this.AreaTrabajoID = (System.Int32?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoID);
							break;
						
						case "AreaTrabajoColor":
						
							if (value == null || value is System.Decimal)
								this.AreaTrabajoColor = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoColor);
							break;
						
						case "EmpresaID":
						
							if (value == null || value is System.Int32)
								this.EmpresaID = (System.Int32?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.EmpresaID);
							break;
						
						case "EmpresaColor":
						
							if (value == null || value is System.Decimal)
								this.EmpresaColor = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.EmpresaColor);
							break;
						
						case "PersonalID":
						
							if (value == null || value is System.Int32)
								this.PersonalID = (System.Int32?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.PersonalID);
							break;
						
						case "TerminalEstado":
						
							if (value == null || value is System.Byte)
								this.TerminalEstado = (System.Byte?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TerminalEstado);
							break;
						
						case "OrigenTipo":
						
							if (value == null || value is System.Byte)
								this.OrigenTipo = (System.Byte?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.OrigenTipo);
							break;
						
						case "ServicioID":
						
							if (value == null || value is System.Decimal)
								this.ServicioID = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.ServicioID);
							break;
						
						case "ServicioEstado":
						
							if (value == null || value is System.Int16)
								this.ServicioEstado = (System.Int16?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.ServicioEstado);
							break;
						
						case "FechaServerGet":
						
							if (value == null || value is System.DateTime)
								this.FechaServerGet = (System.DateTime?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.FechaServerGet);
							break;
						
						case "GpsFecha":
						
							if (value == null || value is System.DateTime)
								this.GpsFecha = (System.DateTime?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsFecha);
							break;
						
						case "GpsLatitud":
						
							if (value == null || value is System.Double)
								this.GpsLatitud = (System.Double?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsLatitud);
							break;
						
						case "GpsLongitud":
						
							if (value == null || value is System.Double)
								this.GpsLongitud = (System.Double?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsLongitud);
							break;
						
						case "GpsFixMode":
						
							if (value == null || value is System.Byte)
								this.GpsFixMode = (System.Byte?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsFixMode);
							break;
						
						case "GpsFixGeoTipoVia":
						
							if (value == null || value is System.Byte)
								this.GpsFixGeoTipoVia = (System.Byte?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTipoVia);
							break;
						
						case "GpsNSAT":
						
							if (value == null || value is System.Byte)
								this.GpsNSAT = (System.Byte?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsNSAT);
							break;
						
						case "GpsAltitud":
						
							if (value == null || value is System.Int16)
								this.GpsAltitud = (System.Int16?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsAltitud);
							break;
						
						case "GpsRumbo":
						
							if (value == null || value is System.Decimal)
								this.GpsRumbo = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsRumbo);
							break;
						
						case "GpsVelocidad":
						
							if (value == null || value is System.Int32)
								this.GpsVelocidad = (System.Int32?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsVelocidad);
							break;
						
						case "GpsDistancia":
						
							if (value == null || value is System.Decimal)
								this.GpsDistancia = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.GpsDistancia);
							break;
						
						case "OdometroTt":
						
							if (value == null || value is System.Decimal)
								this.OdometroTt = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.OdometroTt);
							break;
						
						case "OdometroPc":
						
							if (value == null || value is System.Decimal)
								this.OdometroPc = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.OdometroPc);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Byte)
								this.ServerID = (System.Byte?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.ServerID);
							break;
						
						case "TryProcessed":
						
							if (value == null || value is System.Byte)
								this.TryProcessed = (System.Byte?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TryProcessed);
							break;
						
						case "TramoID":
						
							if (value == null || value is System.Decimal)
								this.TramoID = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TramoID);
							break;
						
						case "EstadoEspecial":
						
							if (value == null || value is System.Decimal)
								this.EstadoEspecial = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.EstadoEspecial);
							break;
						
						case "TipoActividadID":
						
							if (value == null || value is System.Decimal)
								this.TipoActividadID = (System.Decimal?)value;
								OnPropertyChanged(HTerminalGpsInfoDataMetadata.PropertyNames.TipoActividadID);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esHTerminalGpsInfoData entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String TerminalID
			{
				get
				{
					System.Decimal? data = entity.TerminalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalID = null;
					else entity.TerminalID = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehiculoCodigo
			{
				get
				{
					System.String data = entity.VehiculoCodigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoCodigo = null;
					else entity.VehiculoCodigo = Convert.ToString(value);
				}
			}
				
			public System.String VehiculoCodigoADM
			{
				get
				{
					System.String data = entity.VehiculoCodigoADM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoCodigoADM = null;
					else entity.VehiculoCodigoADM = Convert.ToString(value);
				}
			}
				
			public System.String VehiculoMatricula
			{
				get
				{
					System.String data = entity.VehiculoMatricula;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoMatricula = null;
					else entity.VehiculoMatricula = Convert.ToString(value);
				}
			}
				
			public System.String CentroTrabajoID
			{
				get
				{
					System.Int32? data = entity.CentroTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CentroTrabajoID = null;
					else entity.CentroTrabajoID = Convert.ToInt32(value);
				}
			}
				
			public System.String AreaTrabajoID
			{
				get
				{
					System.Int32? data = entity.AreaTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaTrabajoID = null;
					else entity.AreaTrabajoID = Convert.ToInt32(value);
				}
			}
				
			public System.String AreaTrabajoColor
			{
				get
				{
					System.Decimal? data = entity.AreaTrabajoColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaTrabajoColor = null;
					else entity.AreaTrabajoColor = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpresaID
			{
				get
				{
					System.Int32? data = entity.EmpresaID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpresaID = null;
					else entity.EmpresaID = Convert.ToInt32(value);
				}
			}
				
			public System.String EmpresaColor
			{
				get
				{
					System.Decimal? data = entity.EmpresaColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpresaColor = null;
					else entity.EmpresaColor = Convert.ToDecimal(value);
				}
			}
				
			public System.String PersonalID
			{
				get
				{
					System.Int32? data = entity.PersonalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalID = null;
					else entity.PersonalID = Convert.ToInt32(value);
				}
			}
				
			public System.String TerminalEstado
			{
				get
				{
					System.Byte? data = entity.TerminalEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalEstado = null;
					else entity.TerminalEstado = Convert.ToByte(value);
				}
			}
				
			public System.String NumSec
			{
				get
				{
					System.String data = entity.NumSec;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumSec = null;
					else entity.NumSec = Convert.ToString(value);
				}
			}
				
			public System.String OrigenTipo
			{
				get
				{
					System.Byte? data = entity.OrigenTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OrigenTipo = null;
					else entity.OrigenTipo = Convert.ToByte(value);
				}
			}
				
			public System.String ServicioID
			{
				get
				{
					System.Decimal? data = entity.ServicioID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioID = null;
					else entity.ServicioID = Convert.ToDecimal(value);
				}
			}
				
			public System.String ServicioEstado
			{
				get
				{
					System.Int16? data = entity.ServicioEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServicioEstado = null;
					else entity.ServicioEstado = Convert.ToInt16(value);
				}
			}
				
			public System.String FechaServerGet
			{
				get
				{
					System.DateTime? data = entity.FechaServerGet;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaServerGet = null;
					else entity.FechaServerGet = Convert.ToDateTime(value);
				}
			}
				
			public System.String GpsFecha
			{
				get
				{
					System.DateTime? data = entity.GpsFecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFecha = null;
					else entity.GpsFecha = Convert.ToDateTime(value);
				}
			}
				
			public System.String GpsLatitud
			{
				get
				{
					System.Double? data = entity.GpsLatitud;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsLatitud = null;
					else entity.GpsLatitud = Convert.ToDouble(value);
				}
			}
				
			public System.String GpsLongitud
			{
				get
				{
					System.Double? data = entity.GpsLongitud;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsLongitud = null;
					else entity.GpsLongitud = Convert.ToDouble(value);
				}
			}
				
			public System.String GpsFixMode
			{
				get
				{
					System.Byte? data = entity.GpsFixMode;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFixMode = null;
					else entity.GpsFixMode = Convert.ToByte(value);
				}
			}
				
			public System.String GpsFixGeoTx
			{
				get
				{
					System.String data = entity.GpsFixGeoTx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFixGeoTx = null;
					else entity.GpsFixGeoTx = Convert.ToString(value);
				}
			}
				
			public System.String GpsFixGeoTipoVia
			{
				get
				{
					System.Byte? data = entity.GpsFixGeoTipoVia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFixGeoTipoVia = null;
					else entity.GpsFixGeoTipoVia = Convert.ToByte(value);
				}
			}
				
			public System.String GpsNSAT
			{
				get
				{
					System.Byte? data = entity.GpsNSAT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsNSAT = null;
					else entity.GpsNSAT = Convert.ToByte(value);
				}
			}
				
			public System.String GpsAltitud
			{
				get
				{
					System.Int16? data = entity.GpsAltitud;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsAltitud = null;
					else entity.GpsAltitud = Convert.ToInt16(value);
				}
			}
				
			public System.String GpsRumbo
			{
				get
				{
					System.Decimal? data = entity.GpsRumbo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsRumbo = null;
					else entity.GpsRumbo = Convert.ToDecimal(value);
				}
			}
				
			public System.String GpsVelocidad
			{
				get
				{
					System.Int32? data = entity.GpsVelocidad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsVelocidad = null;
					else entity.GpsVelocidad = Convert.ToInt32(value);
				}
			}
				
			public System.String GpsDistancia
			{
				get
				{
					System.Decimal? data = entity.GpsDistancia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsDistancia = null;
					else entity.GpsDistancia = Convert.ToDecimal(value);
				}
			}
				
			public System.String GpsPoblacion
			{
				get
				{
					System.String data = entity.GpsPoblacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsPoblacion = null;
					else entity.GpsPoblacion = Convert.ToString(value);
				}
			}
				
			public System.String OdometroTt
			{
				get
				{
					System.Decimal? data = entity.OdometroTt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdometroTt = null;
					else entity.OdometroTt = Convert.ToDecimal(value);
				}
			}
				
			public System.String OdometroPc
			{
				get
				{
					System.Decimal? data = entity.OdometroPc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdometroPc = null;
					else entity.OdometroPc = Convert.ToDecimal(value);
				}
			}
				
			public System.String InputState
			{
				get
				{
					System.String data = entity.InputState;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InputState = null;
					else entity.InputState = Convert.ToString(value);
				}
			}
				
			public System.String OutputState
			{
				get
				{
					System.String data = entity.OutputState;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OutputState = null;
					else entity.OutputState = Convert.ToString(value);
				}
			}
				
			public System.String ServerID
			{
				get
				{
					System.Byte? data = entity.ServerID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServerID = null;
					else entity.ServerID = Convert.ToByte(value);
				}
			}
				
			public System.String TryProcessed
			{
				get
				{
					System.Byte? data = entity.TryProcessed;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TryProcessed = null;
					else entity.TryProcessed = Convert.ToByte(value);
				}
			}
				
			public System.String TramoID
			{
				get
				{
					System.Decimal? data = entity.TramoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TramoID = null;
					else entity.TramoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String EstadoEspecial
			{
				get
				{
					System.Decimal? data = entity.EstadoEspecial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EstadoEspecial = null;
					else entity.EstadoEspecial = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoActividadID
			{
				get
				{
					System.Decimal? data = entity.TipoActividadID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoActividadID = null;
					else entity.TipoActividadID = Convert.ToDecimal(value);
				}
			}
			

			private esHTerminalGpsInfoData entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return HTerminalGpsInfoDataMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public HTerminalGpsInfoDataQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HTerminalGpsInfoDataQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HTerminalGpsInfoDataQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(HTerminalGpsInfoDataQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private HTerminalGpsInfoDataQ query;		
	}



	[Serializable]
	abstract public partial class esHTerminalGpsInfoDataCol : CollectionBase<HTerminalGpsInfoData>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HTerminalGpsInfoDataMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "HTerminalGpsInfoDataCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public HTerminalGpsInfoDataQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HTerminalGpsInfoDataQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(HTerminalGpsInfoDataQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HTerminalGpsInfoDataQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(HTerminalGpsInfoDataQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((HTerminalGpsInfoDataQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private HTerminalGpsInfoDataQ query;
	}



	[Serializable]
	abstract public partial class esHTerminalGpsInfoDataQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return HTerminalGpsInfoDataMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.Id,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.TerminalID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo, esSystemType.String));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM, esSystemType.String));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula, esSystemType.String));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID, esSystemType.Int32));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID, esSystemType.Int32));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaID, esSystemType.Int32));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.PersonalID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.PersonalID, esSystemType.Int32));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado, esSystemType.Byte));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.NumSec,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.NumSec, esSystemType.String));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo, esSystemType.Byte));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.ServicioID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.ServicioID, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado, esSystemType.Int16));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet, esSystemType.DateTime));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFecha,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsFecha, esSystemType.DateTime));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud, esSystemType.Double));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud, esSystemType.Double));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode, esSystemType.Byte));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia, esSystemType.Byte));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT, esSystemType.Byte));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud, esSystemType.Int16));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad, esSystemType.Int32));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion, esSystemType.String));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.OdometroTt,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.OdometroTt, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.OdometroPc,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.OdometroPc, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.InputState,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.InputState, esSystemType.String));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.OutputState,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.OutputState, esSystemType.String));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.ServerID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.ServerID, esSystemType.Byte));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.TryProcessed,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TryProcessed, esSystemType.Byte));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.TramoID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TramoID, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial, esSystemType.Decimal));
			_queryItems.Add(HTerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID,new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID, esSystemType.Decimal));
			
	    }
	
	    #region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoCodigo
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoCodigoADM
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoMatricula
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula, esSystemType.String); }
		} 
		
		public esQueryItem CentroTrabajoID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID, esSystemType.Int32); }
		} 
		
		public esQueryItem AreaTrabajoID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID, esSystemType.Int32); }
		} 
		
		public esQueryItem AreaTrabajoColor
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpresaID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaID, esSystemType.Int32); }
		} 
		
		public esQueryItem EmpresaColor
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor, esSystemType.Decimal); }
		} 
		
		public esQueryItem PersonalID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.PersonalID, esSystemType.Int32); }
		} 
		
		public esQueryItem TerminalEstado
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado, esSystemType.Byte); }
		} 
		
		public esQueryItem NumSec
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.NumSec, esSystemType.String); }
		} 
		
		public esQueryItem OrigenTipo
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo, esSystemType.Byte); }
		} 
		
		public esQueryItem ServicioID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.ServicioID, esSystemType.Decimal); }
		} 
		
		public esQueryItem ServicioEstado
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado, esSystemType.Int16); }
		} 
		
		public esQueryItem FechaServerGet
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet, esSystemType.DateTime); }
		} 
		
		public esQueryItem GpsFecha
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsFecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem GpsLatitud
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsLongitud
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsFixMode
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsFixGeoTx
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String); }
		} 
		
		public esQueryItem GpsFixGeoTipoVia
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsNSAT
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsAltitud
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud, esSystemType.Int16); }
		} 
		
		public esQueryItem GpsRumbo
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal); }
		} 
		
		public esQueryItem GpsVelocidad
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad, esSystemType.Int32); }
		} 
		
		public esQueryItem GpsDistancia
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia, esSystemType.Decimal); }
		} 
		
		public esQueryItem GpsPoblacion
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion, esSystemType.String); }
		} 
		
		public esQueryItem OdometroTt
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.OdometroTt, esSystemType.Decimal); }
		} 
		
		public esQueryItem OdometroPc
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.OdometroPc, esSystemType.Decimal); }
		} 
		
		public esQueryItem InputState
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.InputState, esSystemType.String); }
		} 
		
		public esQueryItem OutputState
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.OutputState, esSystemType.String); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.ServerID, esSystemType.Byte); }
		} 
		
		public esQueryItem TryProcessed
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TryProcessed, esSystemType.Byte); }
		} 
		
		public esQueryItem TramoID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TramoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem EstadoEspecial
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial, esSystemType.Decimal); }
		} 
		
		public esQueryItem TipoActividadID
		{
			get { return new esQueryItem(this, HTerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class HTerminalGpsInfoData : esHTerminalGpsInfoData
	{

		
		
	}
	



	[Serializable]
	public partial class HTerminalGpsInfoDataMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HTerminalGpsInfoDataMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.TerminalID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoID, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigo, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.VehiculoCodigo;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoCodigoADM, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.VehiculoCodigoADM;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.VehiculoMatricula, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.VehiculoMatricula;
			c.CharacterMaxLength = 12;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.CentroTrabajoID, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.CentroTrabajoID;
			c.NumericPrecision = 10;
			c.Description = "Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoID, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoID;
			c.NumericPrecision = 10;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.AreaTrabajoColor, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.AreaTrabajoColor;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaID, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.EmpresaID;
			c.NumericPrecision = 10;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.EmpresaColor, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.EmpresaColor;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.PersonalID, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.PersonalID;
			c.NumericPrecision = 10;
			c.Description = "Conductor Logado actualmente en el Vehiculo.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.TerminalEstado, 12, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.TerminalEstado;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul ";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.NumSec, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.NumSec;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"('')";
			c.Description = "Nº de Secuencia de la Trama, que envio este registro de Posicionamiento.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.OrigenTipo, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.OrigenTipo;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Tipo de Origen que genero el Registro. 0- Trama de Localizacion. 1- Servicio Cambio de Estado. 2- Vehiculo/Terminal Cambio Estado.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.ServicioID, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.ServicioID;
			c.NumericPrecision = 18;
			c.Description = "Identificacion del Servicio. Obtenido de GITS.dbo.Viajes.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.ServicioEstado, 16, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.ServicioEstado;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.FechaServerGet, 17, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.FechaServerGet;
			c.Description = "Fecha en la cual el Servidor obtuvo la informacion y la Inserto en la BD. La fecha de la Posicion obtendia es GpsFecha.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFecha, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsFecha;
			c.Description = "Fecha de la posicion GPS tomada por el Terminal. Si la posicion es Invalida, está fecha sera la del Terminal. El Terminal se sincroniza con el Reloj de Terminal, cada intervalo, definido en el Terminal. ";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsLatitud, 19, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsLatitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsLongitud, 20, typeof(System.Double), esSystemType.Double);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsLongitud;
			c.NumericPrecision = 15;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixMode, 21, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsFixMode;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTx, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTx;
			c.CharacterMaxLength = 500;
			c.Description = "Georefencia textual de la posicion obtenida en GPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsFixGeoTipoVia, 23, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsFixGeoTipoVia;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"CREATE DEFAULT [dbo].[Zero] AS 0
";
			c.Description = "0:No concocido 1: Autovia 2:Nacional 3:Comarcal 4:Calle 5:Plaza";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsNSAT, 24, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsNSAT;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsAltitud, 25, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsAltitud;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsRumbo, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsRumbo;
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsVelocidad, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsVelocidad;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsDistancia, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsDistancia;
			c.NumericPrecision = 18;
			c.NumericScale = 1;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.GpsPoblacion, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.GpsPoblacion;
			c.CharacterMaxLength = 200;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.OdometroTt, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.OdometroTt;
			c.NumericPrecision = 18;
			c.NumericScale = 1;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Valor del Odometro Total";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.OdometroPc, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.OdometroPc;
			c.NumericPrecision = 18;
			c.NumericScale = 1;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Valor del Odometro Parcial";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.InputState, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.InputState;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"(0x00)";
			c.Description = "Valor hexadecimal de las Señales de entrada del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.OutputState, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.OutputState;
			c.CharacterMaxLength = 20;
			c.HasDefault = true;
			c.Default = @"(0x00)";
			c.Description = "Valor hexadecimal de las Señales de salida del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.ServerID, 34, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 3;
			c.Description = "Servidor que gestiona la conexión de este Terminal. Esta vinculado a la conexion. Si estuviera desconectado,  este valor seria null.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.TryProcessed, 35, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.TryProcessed;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.TramoID, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.TramoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.EstadoEspecial, 37, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.EstadoEspecial;
			c.NumericPrecision = 18;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.Description = "Indica si el Terminal esta en estado Especial: 1:Comida  2:Mantenimiento";
			m_columns.Add(c);
				
			c = new esColumnMetadata(HTerminalGpsInfoDataMetadata.ColumnNames.TipoActividadID, 38, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HTerminalGpsInfoDataMetadata.PropertyNames.TipoActividadID;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public HTerminalGpsInfoDataMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoCodigoADM = "VehiculoCodigoADM";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string AreaTrabajoColor = "AreaTrabajoColor";
			 public const string EmpresaID = "EmpresaID";
			 public const string EmpresaColor = "EmpresaColor";
			 public const string PersonalID = "PersonalID";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string NumSec = "NumSec";
			 public const string OrigenTipo = "OrigenTipo";
			 public const string ServicioID = "ServicioID";
			 public const string ServicioEstado = "ServicioEstado";
			 public const string FechaServerGet = "FechaServerGet";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsFixGeoTipoVia = "GpsFixGeoTipoVia";
			 public const string GpsNSAT = "GpsNSAT";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string GpsPoblacion = "GpsPoblacion";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string ServerID = "ServerID";
			 public const string TryProcessed = "TryProcessed";
			 public const string TramoID = "TramoID";
			 public const string EstadoEspecial = "EstadoEspecial";
			 public const string TipoActividadID = "TipoActividadID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoCodigoADM = "VehiculoCodigoADM";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string AreaTrabajoColor = "AreaTrabajoColor";
			 public const string EmpresaID = "EmpresaID";
			 public const string EmpresaColor = "EmpresaColor";
			 public const string PersonalID = "PersonalID";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string NumSec = "NumSec";
			 public const string OrigenTipo = "OrigenTipo";
			 public const string ServicioID = "ServicioID";
			 public const string ServicioEstado = "ServicioEstado";
			 public const string FechaServerGet = "FechaServerGet";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsFixGeoTipoVia = "GpsFixGeoTipoVia";
			 public const string GpsNSAT = "GpsNSAT";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string GpsPoblacion = "GpsPoblacion";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string ServerID = "ServerID";
			 public const string TryProcessed = "TryProcessed";
			 public const string TramoID = "TramoID";
			 public const string EstadoEspecial = "EstadoEspecial";
			 public const string TipoActividadID = "TipoActividadID";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HTerminalGpsInfoDataMetadata))
			{
				if(HTerminalGpsInfoDataMetadata.mapDelegates == null)
				{
					HTerminalGpsInfoDataMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HTerminalGpsInfoDataMetadata.meta == null)
				{
					HTerminalGpsInfoDataMetadata.meta = new HTerminalGpsInfoDataMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoCodigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoCodigoADM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoMatricula", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CentroTrabajoID", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AreaTrabajoID", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AreaTrabajoColor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpresaID", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("EmpresaColor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PersonalID", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TerminalEstado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumSec", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("OrigenTipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ServicioID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("ServicioEstado", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("FechaServerGet", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GpsFecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GpsLatitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsLongitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsFixMode", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsFixGeoTx", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("GpsFixGeoTipoVia", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsNSAT", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsAltitud", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("GpsRumbo", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("GpsVelocidad", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("GpsDistancia", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("GpsPoblacion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("OdometroTt", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("OdometroPc", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("InputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("OutputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ServerID", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TryProcessed", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TramoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EstadoEspecial", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TipoActividadID", new esTypeMap("numeric", "System.Decimal"));			
				meta.Catalog = "GITSCOMUNICAGPS";
				meta.Schema = "dbo";
				
				meta.Source = "HTerminalGpsInfoData";
				meta.Destination = "HTerminalGpsInfoData";
				
				meta.spInsert = "proc_HTerminalGpsInfoDataInsert";				
				meta.spUpdate = "proc_HTerminalGpsInfoDataUpdate";		
				meta.spDelete = "proc_HTerminalGpsInfoDataDelete";
				meta.spLoadAll = "proc_HTerminalGpsInfoDataLoadAll";
				meta.spLoadByPrimaryKey = "proc_HTerminalGpsInfoDataLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HTerminalGpsInfoDataMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
