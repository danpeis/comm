
/*
===============================================================================
                    EntitySpaces 2011 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 25/06/2015 10:51:49
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace gcperu.Server.Core.DAL.GCG
{
	/// <summary>
	/// MANTIENE EL ESTADO DE LOS TERMINALES ACTIVOS. RELLENA A TRAVES DE INTEGRACION CON GITS, AL ASIGNAR A UN VEHICULO UN TERMINAL, Y SE ELIMINA AL BORRAR ESTA VINCULACION. TAMBIEN AUTOMATICAMENTE SE COMPRUEBA AL INICIAR EL SERVIDOR,  EL ESTADO DEL TERMINAL Y VEHICULO. CUANDO EL TERMINAL ES DESVINCULADO O PUESTO INACTIVO, ESTE DATO SE PASA A LA TABLA [HistoricoTerminalStateHistorico].
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[XmlType("TerminalStateHistorico")]
	public partial class TerminalStateHistorico : esTerminalStateHistorico
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TerminalStateHistorico();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Decimal id)
		{
			var obj = new TerminalStateHistorico();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Decimal id, esSqlAccessType sqlAccessType)
		{
			var obj = new TerminalStateHistorico();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
		
		#region Find/Get
		
		public static bool Existe(System.Decimal ID)
        {
            try
            {
                var e = new TerminalStateHistorico();
                return (e.LoadByPrimaryKey(ID));
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return false;
            }
        }
		
		public static TerminalStateHistorico Get(System.Decimal ID)
        {
            try
            {
                var e = new TerminalStateHistorico();
                return (e.LoadByPrimaryKey(ID) ? e : null);
            }
            catch (Exception e)
            {
                //TraceLog.LogException(e);
                return null;
            }
        }
		
		#endregion
		
		#region custom properties
		
		public bool IsNew
	    {
            get { return this.RowState == esDataRowState.Added; }
	    }
		
		public TerminalStateHistorico(System.Decimal ID)
        {
            this.LoadByPrimaryKey(ID);
        }
		
		public TerminalStateHistorico()
        {
        }
		
		#endregion
		
		

		
		
		override protected string GetConnectionName()
		{
			return "GCG";
		}			
		
	
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[DebuggerVisualizer(typeof(EntitySpaces.DebuggerVisualizer.esVisualizer))]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TerminalStateHistoricoCol")]
	public partial class TerminalStateHistoricoCol : esTerminalStateHistoricoCol, IEnumerable<TerminalStateHistorico>
	{
	
		public TerminalStateHistoricoCol()
		{
		}

	
	
		public TerminalStateHistorico FindByPrimaryKey(System.Decimal id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TerminalStateHistorico))]
		public class TerminalStateHistoricoColWCFPacket : esCollectionWCFPacket<TerminalStateHistoricoCol>
		{
			public static implicit operator TerminalStateHistoricoCol(TerminalStateHistoricoColWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TerminalStateHistoricoColWCFPacket(TerminalStateHistoricoCol collection)
			{
				return new TerminalStateHistoricoColWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "GCG";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TerminalStateHistoricoQ : esTerminalStateHistoricoQ
	{
		public TerminalStateHistoricoQ(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
		public TerminalStateHistoricoQ()
		{
		}

		override protected string GetQueryName()
		{
			return "TerminalStateHistoricoQ";
		}
		
		
		override protected string GetConnectionName()
		{
			return "GCG";
		}

		public override QueryBase CreateQuery()
        {
            return new TerminalStateHistoricoQ();
        }
		
	
		#region Explicit Casts
		
		public static explicit operator string(TerminalStateHistoricoQ query)
		{
			return TerminalStateHistoricoQ.SerializeHelper.ToXml(query);
		}

		public static explicit operator TerminalStateHistoricoQ(string query)
		{
			return (TerminalStateHistoricoQ)TerminalStateHistoricoQ.SerializeHelper.FromXml(query, typeof(TerminalStateHistoricoQ));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTerminalStateHistorico : EntityBase
	{
		public esTerminalStateHistorico()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			TerminalStateHistoricoQ query = new TerminalStateHistoricoQ();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.TerminalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.TerminalID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.VehiculoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? VehiculoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.VehiculoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.VehiculoID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.VehiculoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.VehiculoMatricula
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoMatricula
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoMatricula);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoMatricula, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.VehiculoMatricula);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.VehiculoCodigo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigo
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigo);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigo, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.VehiculoCodigo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.VehiculoCodigoADM
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoCodigoADM
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigoADM);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigoADM, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.VehiculoCodigoADM);
				}
			}
		}		
		
		/// <summary>
		/// Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? CentroTrabajoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.CentroTrabajoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.CentroTrabajoID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.CentroTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.AreaTrabajoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreaTrabajoID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.AreaTrabajoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.AreaTrabajoColor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? AreaTrabajoColor
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoColor);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoColor, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.AreaTrabajoColor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.EmpresaID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpresaID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.EmpresaID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.EmpresaID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.EmpresaID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.EmpresaColor
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? EmpresaColor
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.EmpresaColor);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.EmpresaColor, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.EmpresaColor);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.FlotaID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? FlotaID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.FlotaID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.FlotaID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.FlotaID);
				}
			}
		}		
		
		/// <summary>
		/// Conductor Logado actualmente en el Vehiculo.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? PersonalID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.PersonalID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.PersonalID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.PersonalID);
				}
			}
		}		
		
		/// <summary>
		/// Fecha del Login mas actual. Si esta fecha tiene valor, la fecha del logout es Nula y a la inversa.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? LoginFrom
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.LoginFrom);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.LoginFrom, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.LoginFrom);
				}
			}
		}		
		
		/// <summary>
		/// Fecha del Logout mas actual. Si esta fecha tiene valor, la fecha del login es Nula y a la inversa.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? LogoutFrom
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.LogoutFrom);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.LogoutFrom, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.LogoutFrom);
				}
			}
		}		
		
		/// <summary>
		/// Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? TerminalEstado
		{
			get
			{
				return base.GetSystemInt32(TerminalStateHistoricoMetadata.ColumnNames.TerminalEstado);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateHistoricoMetadata.ColumnNames.TerminalEstado, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalEstado);
				}
			}
		}		
		
		/// <summary>
		/// Identifica el Tipo de Protocolo de comunicacion con el Terminal. 1- MDT. 2-MDV.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? TerminalProtocoloTipo
		{
			get
			{
				return base.GetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.TerminalProtocoloTipo);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.TerminalProtocoloTipo, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalProtocoloTipo);
				}
			}
		}		
		
		/// <summary>
		/// Nº de Serie del Terminal o Identificacion del Terminal Unico guardado en GITS. El Proceso debe comparar los datos mandados por el Terminal (NSerie, IMEI,NSerieSim) y revisar si coincide alguno de ellos con la Identificacion del Dispositivo Terminal en GITS. Se obtiene de la Tabla GITS.dbo.TerminalesGPS campo mdt_NS.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalNS
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.TerminalNS);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.TerminalNS, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalNS);
				}
			}
		}		
		
		/// <summary>
		/// Nº de IMEI del model del Terminal. Se rellena cuando lo envia el Terminal a realizar la conexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalIMEI
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.TerminalIMEI);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.TerminalIMEI, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalIMEI);
				}
			}
		}		
		
		/// <summary>
		/// Numero de Serie de la tarjeta sim del terminal. Simplemente se utiliza de control.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalNSSim
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.TerminalNSSim);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.TerminalNSSim, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalNSSim);
				}
			}
		}		
		
		/// <summary>
		/// Version del software del dispoisitvo, fundamental a la hora de update el software del Dispositvo.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TerminalVersionSW
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.TerminalVersionSW);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.TerminalVersionSW, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalVersionSW);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la Ultima posicion GPS válida recibida. Las tramas de Estado y EstadoServicio, que mandan informacion GPS, siempre envia una posicion válida si el Terminal ha podido obtener una, independientemente que en el momento del envio de la trama no esté obteniendo posiciones validas. Para calcular este fecha, hay que mirar el campo GpsFixMode sea >1, que indica que el Fix es valido. Si no este fecha, no se actualiza.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? GpsFecha
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.GpsFecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.GpsFecha, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsFecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsLatitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? GpsLatitud
		{
			get
			{
				return base.GetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsLatitud);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsLatitud, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsLatitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsLongitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? GpsLongitud
		{
			get
			{
				return base.GetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsLongitud);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsLongitud, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsLongitud);
				}
			}
		}		
		
		/// <summary>
		/// Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? GpsFixMode
		{
			get
			{
				return base.GetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.GpsFixMode);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.GpsFixMode, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsFixMode);
				}
			}
		}		
		
		/// <summary>
		/// Georefencia textual de la posicion obtenida en GPS
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GpsFixGeoTx
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.GpsFixGeoTx);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.GpsFixGeoTx, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsFixGeoTx);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsNSAT
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? GpsNSAT
		{
			get
			{
				return base.GetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.GpsNSAT);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.GpsNSAT, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsNSAT);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsAltitud
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? GpsAltitud
		{
			get
			{
				return base.GetSystemInt16(TerminalStateHistoricoMetadata.ColumnNames.GpsAltitud);
			}
			
			set
			{
				if(base.SetSystemInt16(TerminalStateHistoricoMetadata.ColumnNames.GpsAltitud, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsAltitud);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsRumbo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? GpsRumbo
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.GpsRumbo);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.GpsRumbo, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsRumbo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsVelocidad
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? GpsVelocidad
		{
			get
			{
				return base.GetSystemInt32(TerminalStateHistoricoMetadata.ColumnNames.GpsVelocidad);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateHistoricoMetadata.ColumnNames.GpsVelocidad, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsVelocidad);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsDistancia
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? GpsDistancia
		{
			get
			{
				return base.GetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsDistancia);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsDistancia, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsDistancia);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsFechaDetenido
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? GpsFechaDetenido
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.GpsFechaDetenido);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.GpsFechaDetenido, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsFechaDetenido);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsPoblacion
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String GpsPoblacion
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.GpsPoblacion);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.GpsPoblacion, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsPoblacion);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la Ultima trama recibida desde el Terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? FechaTerminalInfoMasActual
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.FechaTerminalInfoMasActual);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.FechaTerminalInfoMasActual, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.FechaTerminalInfoMasActual);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Total
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? OdometroTt
		{
			get
			{
				return base.GetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.OdometroTt);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.OdometroTt, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.OdometroTt);
				}
			}
		}		
		
		/// <summary>
		/// Valor del Odometro Parcial
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? OdometroPc
		{
			get
			{
				return base.GetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.OdometroPc);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.OdometroPc, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.OdometroPc);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de entrada del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String InputState
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.InputState);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.InputState, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.InputState);
				}
			}
		}		
		
		/// <summary>
		/// Valor hexadecimal de las Señales de salida del Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String OutputState
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.OutputState);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.OutputState, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.OutputState);
				}
			}
		}		
		
		/// <summary>
		/// Servidor que gestiona la conexión de este Terminal. Esta vinculado a la conexion. Si estuviera desconectado,  este valor seria null.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? ServerID
		{
			get
			{
				return base.GetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.ServerID);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.ServerID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.ServerID);
				}
			}
		}		
		
		/// <summary>
		/// Indica si el Terminal está conectado. Si es <> null => conectado sino Desconectado. Guarda la fecha del comienzo de conexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalConnectedFrom
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.TerminalConnectedFrom);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.TerminalConnectedFrom, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalConnectedFrom);
				}
			}
		}		
		
		/// <summary>
		/// Fecha de la ultima recepcion obtenida desde este Terminal. 
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalLastReception
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastReception);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastReception, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalLastReception);
				}
			}
		}		
		
		/// <summary>
		/// Si posee valor indica que el Terminal está desconectado, y marca la fecha de desconexion.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalDisconnect
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnect);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnect, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalDisconnect);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.TerminalDisconnectCount
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? TerminalDisconnectCount
		{
			get
			{
				return base.GetSystemInt16(TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnectCount);
			}
			
			set
			{
				if(base.SetSystemInt16(TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnectCount, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalDisconnectCount);
				}
			}
		}		
		
		/// <summary>
		/// Duración en Horas de la ultima sesion ininterrumpida que este terminal estuvo conectado.  Es null si nunca se conecto este Terminal
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TerminalLastSessionDuration
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastSessionDuration);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastSessionDuration, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalLastSessionDuration);
				}
			}
		}		
		
		/// <summary>
		/// Fecha del ultimo reinicio forzado. Bien por reparación o fallo electrico, o por fallo del Terminal.
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TerminalLastRestart
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastRestart);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastRestart, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalLastRestart);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.UsuariosSeguimiento
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String UsuariosSeguimiento
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.UsuariosSeguimiento);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.UsuariosSeguimiento, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.UsuariosSeguimiento);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.ModoHistorico
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? ModoHistorico
		{
			get
			{
				return base.GetSystemBoolean(TerminalStateHistoricoMetadata.ColumnNames.ModoHistorico);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalStateHistoricoMetadata.ColumnNames.ModoHistorico, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.ModoHistorico);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.ModoHistoricoDesde
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? ModoHistoricoDesde
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.ModoHistoricoDesde);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.ModoHistoricoDesde, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.ModoHistoricoDesde);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.PowerStateDesde
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? PowerStateDesde
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.PowerStateDesde);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.PowerStateDesde, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.PowerStateDesde);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.PowerState
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int16? PowerState
		{
			get
			{
				return base.GetSystemInt16(TerminalStateHistoricoMetadata.ColumnNames.PowerState);
			}
			
			set
			{
				if(base.SetSystemInt16(TerminalStateHistoricoMetadata.ColumnNames.PowerState, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.PowerState);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.PowerStateTx
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PowerStateTx
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.PowerStateTx);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.PowerStateTx, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.PowerStateTx);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GrupoID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? GrupoID
		{
			get
			{
				return base.GetSystemInt32(TerminalStateHistoricoMetadata.ColumnNames.GrupoID);
			}
			
			set
			{
				if(base.SetSystemInt32(TerminalStateHistoricoMetadata.ColumnNames.GrupoID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GrupoID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.VehiculoTelefono
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoTelefono
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoTelefono);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoTelefono, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.VehiculoTelefono);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.Comentario
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Comentario
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.Comentario);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.Comentario, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.Comentario);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.OnLine
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? OnLine
		{
			get
			{
				return base.GetSystemBoolean(TerminalStateHistoricoMetadata.ColumnNames.OnLine);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalStateHistoricoMetadata.ColumnNames.OnLine, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.OnLine);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.OffLineCausa
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String OffLineCausa
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.OffLineCausa);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.OffLineCausa, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.OffLineCausa);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.FlotaMadreID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? FlotaMadreID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.FlotaMadreID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.FlotaMadreID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.FlotaMadreID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.FlotaPadreID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? FlotaPadreID
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.FlotaPadreID);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.FlotaPadreID, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.FlotaPadreID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.PersonalNombre
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PersonalNombre
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.PersonalNombre);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.PersonalNombre, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.PersonalNombre);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.PersonalTelefono
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String PersonalTelefono
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.PersonalTelefono);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.PersonalTelefono, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.PersonalTelefono);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.VehiculoUsuarioSeguimiento
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VehiculoUsuarioSeguimiento
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoUsuarioSeguimiento);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.VehiculoUsuarioSeguimiento, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.VehiculoUsuarioSeguimiento);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.WorkingNow
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? WorkingNow
		{
			get
			{
				return base.GetSystemBoolean(TerminalStateHistoricoMetadata.ColumnNames.WorkingNow);
			}
			
			set
			{
				if(base.SetSystemBoolean(TerminalStateHistoricoMetadata.ColumnNames.WorkingNow, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.WorkingNow);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsLatitudAnterior
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? GpsLatitudAnterior
		{
			get
			{
				return base.GetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsLatitudAnterior);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsLatitudAnterior, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsLatitudAnterior);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.GpsLongitudAnterior
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Double? GpsLongitudAnterior
		{
			get
			{
				return base.GetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsLongitudAnterior);
			}
			
			set
			{
				if(base.SetSystemDouble(TerminalStateHistoricoMetadata.ColumnNames.GpsLongitudAnterior, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsLongitudAnterior);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.AltaFecha
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? AltaFecha
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.AltaFecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.AltaFecha, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.AltaFecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.BajaFecha
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? BajaFecha
		{
			get
			{
				return base.GetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.BajaFecha);
			}
			
			set
			{
				if(base.SetSystemDateTime(TerminalStateHistoricoMetadata.ColumnNames.BajaFecha, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.BajaFecha);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemDecimal(TerminalStateHistoricoMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.BajaMotivo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String BajaMotivo
		{
			get
			{
				return base.GetSystemString(TerminalStateHistoricoMetadata.ColumnNames.BajaMotivo);
			}
			
			set
			{
				if(base.SetSystemString(TerminalStateHistoricoMetadata.ColumnNames.BajaMotivo, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.BajaMotivo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to TerminalStateHistorico.SoftwareTipo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Byte? SoftwareTipo
		{
			get
			{
				return base.GetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.SoftwareTipo);
			}
			
			set
			{
				if(base.SetSystemByte(TerminalStateHistoricoMetadata.ColumnNames.SoftwareTipo, value))
				{
					OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.SoftwareTipo);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TerminalID": this.str().TerminalID = (string)value; break;							
						case "VehiculoID": this.str().VehiculoID = (string)value; break;							
						case "VehiculoMatricula": this.str().VehiculoMatricula = (string)value; break;							
						case "VehiculoCodigo": this.str().VehiculoCodigo = (string)value; break;							
						case "VehiculoCodigoADM": this.str().VehiculoCodigoADM = (string)value; break;							
						case "CentroTrabajoID": this.str().CentroTrabajoID = (string)value; break;							
						case "AreaTrabajoID": this.str().AreaTrabajoID = (string)value; break;							
						case "AreaTrabajoColor": this.str().AreaTrabajoColor = (string)value; break;							
						case "EmpresaID": this.str().EmpresaID = (string)value; break;							
						case "EmpresaColor": this.str().EmpresaColor = (string)value; break;							
						case "FlotaID": this.str().FlotaID = (string)value; break;							
						case "PersonalID": this.str().PersonalID = (string)value; break;							
						case "LoginFrom": this.str().LoginFrom = (string)value; break;							
						case "LogoutFrom": this.str().LogoutFrom = (string)value; break;							
						case "TerminalEstado": this.str().TerminalEstado = (string)value; break;							
						case "TerminalProtocoloTipo": this.str().TerminalProtocoloTipo = (string)value; break;							
						case "TerminalNS": this.str().TerminalNS = (string)value; break;							
						case "TerminalIMEI": this.str().TerminalIMEI = (string)value; break;							
						case "TerminalNSSim": this.str().TerminalNSSim = (string)value; break;							
						case "TerminalVersionSW": this.str().TerminalVersionSW = (string)value; break;							
						case "GpsFecha": this.str().GpsFecha = (string)value; break;							
						case "GpsLatitud": this.str().GpsLatitud = (string)value; break;							
						case "GpsLongitud": this.str().GpsLongitud = (string)value; break;							
						case "GpsFixMode": this.str().GpsFixMode = (string)value; break;							
						case "GpsFixGeoTx": this.str().GpsFixGeoTx = (string)value; break;							
						case "GpsNSAT": this.str().GpsNSAT = (string)value; break;							
						case "GpsAltitud": this.str().GpsAltitud = (string)value; break;							
						case "GpsRumbo": this.str().GpsRumbo = (string)value; break;							
						case "GpsVelocidad": this.str().GpsVelocidad = (string)value; break;							
						case "GpsDistancia": this.str().GpsDistancia = (string)value; break;							
						case "GpsFechaDetenido": this.str().GpsFechaDetenido = (string)value; break;							
						case "GpsPoblacion": this.str().GpsPoblacion = (string)value; break;							
						case "FechaTerminalInfoMasActual": this.str().FechaTerminalInfoMasActual = (string)value; break;							
						case "OdometroTt": this.str().OdometroTt = (string)value; break;							
						case "OdometroPc": this.str().OdometroPc = (string)value; break;							
						case "InputState": this.str().InputState = (string)value; break;							
						case "OutputState": this.str().OutputState = (string)value; break;							
						case "ServerID": this.str().ServerID = (string)value; break;							
						case "TerminalConnectedFrom": this.str().TerminalConnectedFrom = (string)value; break;							
						case "TerminalLastReception": this.str().TerminalLastReception = (string)value; break;							
						case "TerminalDisconnect": this.str().TerminalDisconnect = (string)value; break;							
						case "TerminalDisconnectCount": this.str().TerminalDisconnectCount = (string)value; break;							
						case "TerminalLastSessionDuration": this.str().TerminalLastSessionDuration = (string)value; break;							
						case "TerminalLastRestart": this.str().TerminalLastRestart = (string)value; break;							
						case "UsuariosSeguimiento": this.str().UsuariosSeguimiento = (string)value; break;							
						case "ModoHistorico": this.str().ModoHistorico = (string)value; break;							
						case "ModoHistoricoDesde": this.str().ModoHistoricoDesde = (string)value; break;							
						case "PowerStateDesde": this.str().PowerStateDesde = (string)value; break;							
						case "PowerState": this.str().PowerState = (string)value; break;							
						case "PowerStateTx": this.str().PowerStateTx = (string)value; break;							
						case "GrupoID": this.str().GrupoID = (string)value; break;							
						case "VehiculoTelefono": this.str().VehiculoTelefono = (string)value; break;							
						case "Comentario": this.str().Comentario = (string)value; break;							
						case "OnLine": this.str().OnLine = (string)value; break;							
						case "OffLineCausa": this.str().OffLineCausa = (string)value; break;							
						case "FlotaMadreID": this.str().FlotaMadreID = (string)value; break;							
						case "FlotaPadreID": this.str().FlotaPadreID = (string)value; break;							
						case "PersonalNombre": this.str().PersonalNombre = (string)value; break;							
						case "PersonalTelefono": this.str().PersonalTelefono = (string)value; break;							
						case "VehiculoUsuarioSeguimiento": this.str().VehiculoUsuarioSeguimiento = (string)value; break;							
						case "WorkingNow": this.str().WorkingNow = (string)value; break;							
						case "GpsLatitudAnterior": this.str().GpsLatitudAnterior = (string)value; break;							
						case "GpsLongitudAnterior": this.str().GpsLongitudAnterior = (string)value; break;							
						case "AltaFecha": this.str().AltaFecha = (string)value; break;							
						case "BajaFecha": this.str().BajaFecha = (string)value; break;							
						case "Id": this.str().Id = (string)value; break;							
						case "BajaMotivo": this.str().BajaMotivo = (string)value; break;							
						case "SoftwareTipo": this.str().SoftwareTipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TerminalID":
						
							if (value == null || value is System.Decimal)
								this.TerminalID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalID);
							break;
						
						case "VehiculoID":
						
							if (value == null || value is System.Decimal)
								this.VehiculoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.VehiculoID);
							break;
						
						case "CentroTrabajoID":
						
							if (value == null || value is System.Decimal)
								this.CentroTrabajoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.CentroTrabajoID);
							break;
						
						case "AreaTrabajoID":
						
							if (value == null || value is System.Decimal)
								this.AreaTrabajoID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.AreaTrabajoID);
							break;
						
						case "AreaTrabajoColor":
						
							if (value == null || value is System.Decimal)
								this.AreaTrabajoColor = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.AreaTrabajoColor);
							break;
						
						case "EmpresaID":
						
							if (value == null || value is System.Decimal)
								this.EmpresaID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.EmpresaID);
							break;
						
						case "EmpresaColor":
						
							if (value == null || value is System.Decimal)
								this.EmpresaColor = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.EmpresaColor);
							break;
						
						case "FlotaID":
						
							if (value == null || value is System.Decimal)
								this.FlotaID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.FlotaID);
							break;
						
						case "PersonalID":
						
							if (value == null || value is System.Decimal)
								this.PersonalID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.PersonalID);
							break;
						
						case "LoginFrom":
						
							if (value == null || value is System.DateTime)
								this.LoginFrom = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.LoginFrom);
							break;
						
						case "LogoutFrom":
						
							if (value == null || value is System.DateTime)
								this.LogoutFrom = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.LogoutFrom);
							break;
						
						case "TerminalEstado":
						
							if (value == null || value is System.Int32)
								this.TerminalEstado = (System.Int32?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalEstado);
							break;
						
						case "TerminalProtocoloTipo":
						
							if (value == null || value is System.Byte)
								this.TerminalProtocoloTipo = (System.Byte?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalProtocoloTipo);
							break;
						
						case "GpsFecha":
						
							if (value == null || value is System.DateTime)
								this.GpsFecha = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsFecha);
							break;
						
						case "GpsLatitud":
						
							if (value == null || value is System.Double)
								this.GpsLatitud = (System.Double?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsLatitud);
							break;
						
						case "GpsLongitud":
						
							if (value == null || value is System.Double)
								this.GpsLongitud = (System.Double?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsLongitud);
							break;
						
						case "GpsFixMode":
						
							if (value == null || value is System.Byte)
								this.GpsFixMode = (System.Byte?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsFixMode);
							break;
						
						case "GpsNSAT":
						
							if (value == null || value is System.Byte)
								this.GpsNSAT = (System.Byte?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsNSAT);
							break;
						
						case "GpsAltitud":
						
							if (value == null || value is System.Int16)
								this.GpsAltitud = (System.Int16?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsAltitud);
							break;
						
						case "GpsRumbo":
						
							if (value == null || value is System.Decimal)
								this.GpsRumbo = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsRumbo);
							break;
						
						case "GpsVelocidad":
						
							if (value == null || value is System.Int32)
								this.GpsVelocidad = (System.Int32?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsVelocidad);
							break;
						
						case "GpsDistancia":
						
							if (value == null || value is System.Double)
								this.GpsDistancia = (System.Double?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsDistancia);
							break;
						
						case "GpsFechaDetenido":
						
							if (value == null || value is System.DateTime)
								this.GpsFechaDetenido = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsFechaDetenido);
							break;
						
						case "FechaTerminalInfoMasActual":
						
							if (value == null || value is System.DateTime)
								this.FechaTerminalInfoMasActual = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.FechaTerminalInfoMasActual);
							break;
						
						case "OdometroTt":
						
							if (value == null || value is System.Double)
								this.OdometroTt = (System.Double?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.OdometroTt);
							break;
						
						case "OdometroPc":
						
							if (value == null || value is System.Double)
								this.OdometroPc = (System.Double?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.OdometroPc);
							break;
						
						case "ServerID":
						
							if (value == null || value is System.Byte)
								this.ServerID = (System.Byte?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.ServerID);
							break;
						
						case "TerminalConnectedFrom":
						
							if (value == null || value is System.DateTime)
								this.TerminalConnectedFrom = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalConnectedFrom);
							break;
						
						case "TerminalLastReception":
						
							if (value == null || value is System.DateTime)
								this.TerminalLastReception = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalLastReception);
							break;
						
						case "TerminalDisconnect":
						
							if (value == null || value is System.DateTime)
								this.TerminalDisconnect = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalDisconnect);
							break;
						
						case "TerminalDisconnectCount":
						
							if (value == null || value is System.Int16)
								this.TerminalDisconnectCount = (System.Int16?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalDisconnectCount);
							break;
						
						case "TerminalLastSessionDuration":
						
							if (value == null || value is System.Decimal)
								this.TerminalLastSessionDuration = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalLastSessionDuration);
							break;
						
						case "TerminalLastRestart":
						
							if (value == null || value is System.DateTime)
								this.TerminalLastRestart = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.TerminalLastRestart);
							break;
						
						case "ModoHistorico":
						
							if (value == null || value is System.Boolean)
								this.ModoHistorico = (System.Boolean?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.ModoHistorico);
							break;
						
						case "ModoHistoricoDesde":
						
							if (value == null || value is System.DateTime)
								this.ModoHistoricoDesde = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.ModoHistoricoDesde);
							break;
						
						case "PowerStateDesde":
						
							if (value == null || value is System.DateTime)
								this.PowerStateDesde = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.PowerStateDesde);
							break;
						
						case "PowerState":
						
							if (value == null || value is System.Int16)
								this.PowerState = (System.Int16?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.PowerState);
							break;
						
						case "GrupoID":
						
							if (value == null || value is System.Int32)
								this.GrupoID = (System.Int32?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GrupoID);
							break;
						
						case "OnLine":
						
							if (value == null || value is System.Boolean)
								this.OnLine = (System.Boolean?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.OnLine);
							break;
						
						case "FlotaMadreID":
						
							if (value == null || value is System.Decimal)
								this.FlotaMadreID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.FlotaMadreID);
							break;
						
						case "FlotaPadreID":
						
							if (value == null || value is System.Decimal)
								this.FlotaPadreID = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.FlotaPadreID);
							break;
						
						case "WorkingNow":
						
							if (value == null || value is System.Boolean)
								this.WorkingNow = (System.Boolean?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.WorkingNow);
							break;
						
						case "GpsLatitudAnterior":
						
							if (value == null || value is System.Double)
								this.GpsLatitudAnterior = (System.Double?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsLatitudAnterior);
							break;
						
						case "GpsLongitudAnterior":
						
							if (value == null || value is System.Double)
								this.GpsLongitudAnterior = (System.Double?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.GpsLongitudAnterior);
							break;
						
						case "AltaFecha":
						
							if (value == null || value is System.DateTime)
								this.AltaFecha = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.AltaFecha);
							break;
						
						case "BajaFecha":
						
							if (value == null || value is System.DateTime)
								this.BajaFecha = (System.DateTime?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.BajaFecha);
							break;
						
						case "Id":
						
							if (value == null || value is System.Decimal)
								this.Id = (System.Decimal?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.Id);
							break;
						
						case "SoftwareTipo":
						
							if (value == null || value is System.Byte)
								this.SoftwareTipo = (System.Byte?)value;
								OnPropertyChanged(TerminalStateHistoricoMetadata.PropertyNames.SoftwareTipo);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTerminalStateHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String TerminalID
			{
				get
				{
					System.Decimal? data = entity.TerminalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalID = null;
					else entity.TerminalID = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehiculoID
			{
				get
				{
					System.Decimal? data = entity.VehiculoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoID = null;
					else entity.VehiculoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String VehiculoMatricula
			{
				get
				{
					System.String data = entity.VehiculoMatricula;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoMatricula = null;
					else entity.VehiculoMatricula = Convert.ToString(value);
				}
			}
				
			public System.String VehiculoCodigo
			{
				get
				{
					System.String data = entity.VehiculoCodigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoCodigo = null;
					else entity.VehiculoCodigo = Convert.ToString(value);
				}
			}
				
			public System.String VehiculoCodigoADM
			{
				get
				{
					System.String data = entity.VehiculoCodigoADM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoCodigoADM = null;
					else entity.VehiculoCodigoADM = Convert.ToString(value);
				}
			}
				
			public System.String CentroTrabajoID
			{
				get
				{
					System.Decimal? data = entity.CentroTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CentroTrabajoID = null;
					else entity.CentroTrabajoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreaTrabajoID
			{
				get
				{
					System.Decimal? data = entity.AreaTrabajoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaTrabajoID = null;
					else entity.AreaTrabajoID = Convert.ToDecimal(value);
				}
			}
				
			public System.String AreaTrabajoColor
			{
				get
				{
					System.Decimal? data = entity.AreaTrabajoColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AreaTrabajoColor = null;
					else entity.AreaTrabajoColor = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpresaID
			{
				get
				{
					System.Decimal? data = entity.EmpresaID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpresaID = null;
					else entity.EmpresaID = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmpresaColor
			{
				get
				{
					System.Decimal? data = entity.EmpresaColor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpresaColor = null;
					else entity.EmpresaColor = Convert.ToDecimal(value);
				}
			}
				
			public System.String FlotaID
			{
				get
				{
					System.Decimal? data = entity.FlotaID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlotaID = null;
					else entity.FlotaID = Convert.ToDecimal(value);
				}
			}
				
			public System.String PersonalID
			{
				get
				{
					System.Decimal? data = entity.PersonalID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalID = null;
					else entity.PersonalID = Convert.ToDecimal(value);
				}
			}
				
			public System.String LoginFrom
			{
				get
				{
					System.DateTime? data = entity.LoginFrom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LoginFrom = null;
					else entity.LoginFrom = Convert.ToDateTime(value);
				}
			}
				
			public System.String LogoutFrom
			{
				get
				{
					System.DateTime? data = entity.LogoutFrom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LogoutFrom = null;
					else entity.LogoutFrom = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalEstado
			{
				get
				{
					System.Int32? data = entity.TerminalEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalEstado = null;
					else entity.TerminalEstado = Convert.ToInt32(value);
				}
			}
				
			public System.String TerminalProtocoloTipo
			{
				get
				{
					System.Byte? data = entity.TerminalProtocoloTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalProtocoloTipo = null;
					else entity.TerminalProtocoloTipo = Convert.ToByte(value);
				}
			}
				
			public System.String TerminalNS
			{
				get
				{
					System.String data = entity.TerminalNS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalNS = null;
					else entity.TerminalNS = Convert.ToString(value);
				}
			}
				
			public System.String TerminalIMEI
			{
				get
				{
					System.String data = entity.TerminalIMEI;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalIMEI = null;
					else entity.TerminalIMEI = Convert.ToString(value);
				}
			}
				
			public System.String TerminalNSSim
			{
				get
				{
					System.String data = entity.TerminalNSSim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalNSSim = null;
					else entity.TerminalNSSim = Convert.ToString(value);
				}
			}
				
			public System.String TerminalVersionSW
			{
				get
				{
					System.String data = entity.TerminalVersionSW;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalVersionSW = null;
					else entity.TerminalVersionSW = Convert.ToString(value);
				}
			}
				
			public System.String GpsFecha
			{
				get
				{
					System.DateTime? data = entity.GpsFecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFecha = null;
					else entity.GpsFecha = Convert.ToDateTime(value);
				}
			}
				
			public System.String GpsLatitud
			{
				get
				{
					System.Double? data = entity.GpsLatitud;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsLatitud = null;
					else entity.GpsLatitud = Convert.ToDouble(value);
				}
			}
				
			public System.String GpsLongitud
			{
				get
				{
					System.Double? data = entity.GpsLongitud;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsLongitud = null;
					else entity.GpsLongitud = Convert.ToDouble(value);
				}
			}
				
			public System.String GpsFixMode
			{
				get
				{
					System.Byte? data = entity.GpsFixMode;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFixMode = null;
					else entity.GpsFixMode = Convert.ToByte(value);
				}
			}
				
			public System.String GpsFixGeoTx
			{
				get
				{
					System.String data = entity.GpsFixGeoTx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFixGeoTx = null;
					else entity.GpsFixGeoTx = Convert.ToString(value);
				}
			}
				
			public System.String GpsNSAT
			{
				get
				{
					System.Byte? data = entity.GpsNSAT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsNSAT = null;
					else entity.GpsNSAT = Convert.ToByte(value);
				}
			}
				
			public System.String GpsAltitud
			{
				get
				{
					System.Int16? data = entity.GpsAltitud;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsAltitud = null;
					else entity.GpsAltitud = Convert.ToInt16(value);
				}
			}
				
			public System.String GpsRumbo
			{
				get
				{
					System.Decimal? data = entity.GpsRumbo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsRumbo = null;
					else entity.GpsRumbo = Convert.ToDecimal(value);
				}
			}
				
			public System.String GpsVelocidad
			{
				get
				{
					System.Int32? data = entity.GpsVelocidad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsVelocidad = null;
					else entity.GpsVelocidad = Convert.ToInt32(value);
				}
			}
				
			public System.String GpsDistancia
			{
				get
				{
					System.Double? data = entity.GpsDistancia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsDistancia = null;
					else entity.GpsDistancia = Convert.ToDouble(value);
				}
			}
				
			public System.String GpsFechaDetenido
			{
				get
				{
					System.DateTime? data = entity.GpsFechaDetenido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsFechaDetenido = null;
					else entity.GpsFechaDetenido = Convert.ToDateTime(value);
				}
			}
				
			public System.String GpsPoblacion
			{
				get
				{
					System.String data = entity.GpsPoblacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsPoblacion = null;
					else entity.GpsPoblacion = Convert.ToString(value);
				}
			}
				
			public System.String FechaTerminalInfoMasActual
			{
				get
				{
					System.DateTime? data = entity.FechaTerminalInfoMasActual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FechaTerminalInfoMasActual = null;
					else entity.FechaTerminalInfoMasActual = Convert.ToDateTime(value);
				}
			}
				
			public System.String OdometroTt
			{
				get
				{
					System.Double? data = entity.OdometroTt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdometroTt = null;
					else entity.OdometroTt = Convert.ToDouble(value);
				}
			}
				
			public System.String OdometroPc
			{
				get
				{
					System.Double? data = entity.OdometroPc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OdometroPc = null;
					else entity.OdometroPc = Convert.ToDouble(value);
				}
			}
				
			public System.String InputState
			{
				get
				{
					System.String data = entity.InputState;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InputState = null;
					else entity.InputState = Convert.ToString(value);
				}
			}
				
			public System.String OutputState
			{
				get
				{
					System.String data = entity.OutputState;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OutputState = null;
					else entity.OutputState = Convert.ToString(value);
				}
			}
				
			public System.String ServerID
			{
				get
				{
					System.Byte? data = entity.ServerID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ServerID = null;
					else entity.ServerID = Convert.ToByte(value);
				}
			}
				
			public System.String TerminalConnectedFrom
			{
				get
				{
					System.DateTime? data = entity.TerminalConnectedFrom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalConnectedFrom = null;
					else entity.TerminalConnectedFrom = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalLastReception
			{
				get
				{
					System.DateTime? data = entity.TerminalLastReception;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalLastReception = null;
					else entity.TerminalLastReception = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalDisconnect
			{
				get
				{
					System.DateTime? data = entity.TerminalDisconnect;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalDisconnect = null;
					else entity.TerminalDisconnect = Convert.ToDateTime(value);
				}
			}
				
			public System.String TerminalDisconnectCount
			{
				get
				{
					System.Int16? data = entity.TerminalDisconnectCount;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalDisconnectCount = null;
					else entity.TerminalDisconnectCount = Convert.ToInt16(value);
				}
			}
				
			public System.String TerminalLastSessionDuration
			{
				get
				{
					System.Decimal? data = entity.TerminalLastSessionDuration;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalLastSessionDuration = null;
					else entity.TerminalLastSessionDuration = Convert.ToDecimal(value);
				}
			}
				
			public System.String TerminalLastRestart
			{
				get
				{
					System.DateTime? data = entity.TerminalLastRestart;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TerminalLastRestart = null;
					else entity.TerminalLastRestart = Convert.ToDateTime(value);
				}
			}
				
			public System.String UsuariosSeguimiento
			{
				get
				{
					System.String data = entity.UsuariosSeguimiento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UsuariosSeguimiento = null;
					else entity.UsuariosSeguimiento = Convert.ToString(value);
				}
			}
				
			public System.String ModoHistorico
			{
				get
				{
					System.Boolean? data = entity.ModoHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ModoHistorico = null;
					else entity.ModoHistorico = Convert.ToBoolean(value);
				}
			}
				
			public System.String ModoHistoricoDesde
			{
				get
				{
					System.DateTime? data = entity.ModoHistoricoDesde;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ModoHistoricoDesde = null;
					else entity.ModoHistoricoDesde = Convert.ToDateTime(value);
				}
			}
				
			public System.String PowerStateDesde
			{
				get
				{
					System.DateTime? data = entity.PowerStateDesde;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PowerStateDesde = null;
					else entity.PowerStateDesde = Convert.ToDateTime(value);
				}
			}
				
			public System.String PowerState
			{
				get
				{
					System.Int16? data = entity.PowerState;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PowerState = null;
					else entity.PowerState = Convert.ToInt16(value);
				}
			}
				
			public System.String PowerStateTx
			{
				get
				{
					System.String data = entity.PowerStateTx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PowerStateTx = null;
					else entity.PowerStateTx = Convert.ToString(value);
				}
			}
				
			public System.String GrupoID
			{
				get
				{
					System.Int32? data = entity.GrupoID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GrupoID = null;
					else entity.GrupoID = Convert.ToInt32(value);
				}
			}
				
			public System.String VehiculoTelefono
			{
				get
				{
					System.String data = entity.VehiculoTelefono;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoTelefono = null;
					else entity.VehiculoTelefono = Convert.ToString(value);
				}
			}
				
			public System.String Comentario
			{
				get
				{
					System.String data = entity.Comentario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Comentario = null;
					else entity.Comentario = Convert.ToString(value);
				}
			}
				
			public System.String OnLine
			{
				get
				{
					System.Boolean? data = entity.OnLine;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OnLine = null;
					else entity.OnLine = Convert.ToBoolean(value);
				}
			}
				
			public System.String OffLineCausa
			{
				get
				{
					System.String data = entity.OffLineCausa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OffLineCausa = null;
					else entity.OffLineCausa = Convert.ToString(value);
				}
			}
				
			public System.String FlotaMadreID
			{
				get
				{
					System.Decimal? data = entity.FlotaMadreID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlotaMadreID = null;
					else entity.FlotaMadreID = Convert.ToDecimal(value);
				}
			}
				
			public System.String FlotaPadreID
			{
				get
				{
					System.Decimal? data = entity.FlotaPadreID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlotaPadreID = null;
					else entity.FlotaPadreID = Convert.ToDecimal(value);
				}
			}
				
			public System.String PersonalNombre
			{
				get
				{
					System.String data = entity.PersonalNombre;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalNombre = null;
					else entity.PersonalNombre = Convert.ToString(value);
				}
			}
				
			public System.String PersonalTelefono
			{
				get
				{
					System.String data = entity.PersonalTelefono;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PersonalTelefono = null;
					else entity.PersonalTelefono = Convert.ToString(value);
				}
			}
				
			public System.String VehiculoUsuarioSeguimiento
			{
				get
				{
					System.String data = entity.VehiculoUsuarioSeguimiento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VehiculoUsuarioSeguimiento = null;
					else entity.VehiculoUsuarioSeguimiento = Convert.ToString(value);
				}
			}
				
			public System.String WorkingNow
			{
				get
				{
					System.Boolean? data = entity.WorkingNow;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.WorkingNow = null;
					else entity.WorkingNow = Convert.ToBoolean(value);
				}
			}
				
			public System.String GpsLatitudAnterior
			{
				get
				{
					System.Double? data = entity.GpsLatitudAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsLatitudAnterior = null;
					else entity.GpsLatitudAnterior = Convert.ToDouble(value);
				}
			}
				
			public System.String GpsLongitudAnterior
			{
				get
				{
					System.Double? data = entity.GpsLongitudAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GpsLongitudAnterior = null;
					else entity.GpsLongitudAnterior = Convert.ToDouble(value);
				}
			}
				
			public System.String AltaFecha
			{
				get
				{
					System.DateTime? data = entity.AltaFecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AltaFecha = null;
					else entity.AltaFecha = Convert.ToDateTime(value);
				}
			}
				
			public System.String BajaFecha
			{
				get
				{
					System.DateTime? data = entity.BajaFecha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BajaFecha = null;
					else entity.BajaFecha = Convert.ToDateTime(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String BajaMotivo
			{
				get
				{
					System.String data = entity.BajaMotivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BajaMotivo = null;
					else entity.BajaMotivo = Convert.ToString(value);
				}
			}
				
			public System.String SoftwareTipo
			{
				get
				{
					System.Byte? data = entity.SoftwareTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SoftwareTipo = null;
					else entity.SoftwareTipo = Convert.ToByte(value);
				}
			}
			

			private esTerminalStateHistorico entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TerminalStateHistoricoMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TerminalStateHistoricoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalStateHistoricoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalStateHistoricoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TerminalStateHistoricoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
        [IgnoreDataMember]
		private TerminalStateHistoricoQ query;		
	}



	[Serializable]
	abstract public partial class esTerminalStateHistoricoCol : CollectionBase<TerminalStateHistorico>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TerminalStateHistoricoMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TerminalStateHistoricoCol";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TerminalStateHistoricoQ Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TerminalStateHistoricoQ();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TerminalStateHistoricoQ query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TerminalStateHistoricoQ();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TerminalStateHistoricoQ query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TerminalStateHistoricoQ)query);
		}
		
		public void QueryReset()
		{
			this.query = null;
		}

		#endregion
		
		#region Override CollectionBase

        public override bool ReLoad()
        {
            //Llama al metodo query, que devuelve la query ya existente, si la hubiera o una neuva en caso de que fuera null
            return Load(Query);
        }

	    #endregion
		
		private TerminalStateHistoricoQ query;
	}



	[Serializable]
	abstract public partial class esTerminalStateHistoricoQ : QueryBase
	{
		override protected IMetadata Meta
		{
			get
			{
				return TerminalStateHistoricoMetadata.Meta();
			}
		}	
		
		public override void CreateQueryItems()
	    {
            _queryItems = new Dictionary<string, esQueryItem>();
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.VehiculoID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.VehiculoMatricula,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoMatricula, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigo,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigo, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigoADM,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigoADM, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.CentroTrabajoID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.CentroTrabajoID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoColor,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoColor, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.EmpresaID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.EmpresaID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.EmpresaColor,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.EmpresaColor, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.FlotaID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.FlotaID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.PersonalID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PersonalID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.LoginFrom,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.LoginFrom, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.LogoutFrom,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.LogoutFrom, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalEstado,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalEstado, esSystemType.Int32));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalProtocoloTipo,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalProtocoloTipo, esSystemType.Byte));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalNS,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalNS, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalIMEI,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalIMEI, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalNSSim,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalNSSim, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalVersionSW,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalVersionSW, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsFecha,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsFecha, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsLatitud,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsLatitud, esSystemType.Double));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsLongitud,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsLongitud, esSystemType.Double));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsFixMode,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsFixMode, esSystemType.Byte));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsFixGeoTx,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsNSAT,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsNSAT, esSystemType.Byte));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsAltitud,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsAltitud, esSystemType.Int16));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsRumbo,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsVelocidad,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsVelocidad, esSystemType.Int32));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsDistancia,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsDistancia, esSystemType.Double));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsFechaDetenido,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsFechaDetenido, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsPoblacion,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsPoblacion, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.FechaTerminalInfoMasActual,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.FechaTerminalInfoMasActual, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.OdometroTt,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OdometroTt, esSystemType.Double));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.OdometroPc,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OdometroPc, esSystemType.Double));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.InputState,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.InputState, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.OutputState,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OutputState, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.ServerID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.ServerID, esSystemType.Byte));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalConnectedFrom,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalConnectedFrom, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastReception,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalLastReception, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnect,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnect, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnectCount,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnectCount, esSystemType.Int16));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastSessionDuration,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalLastSessionDuration, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastRestart,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalLastRestart, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.UsuariosSeguimiento,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.UsuariosSeguimiento, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.ModoHistorico,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.ModoHistorico, esSystemType.Boolean));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.ModoHistoricoDesde,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.ModoHistoricoDesde, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.PowerStateDesde,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PowerStateDesde, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.PowerState,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PowerState, esSystemType.Int16));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.PowerStateTx,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PowerStateTx, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GrupoID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GrupoID, esSystemType.Int32));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.VehiculoTelefono,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoTelefono, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.Comentario,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.Comentario, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.OnLine,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OnLine, esSystemType.Boolean));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.OffLineCausa,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OffLineCausa, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.FlotaMadreID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.FlotaMadreID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.FlotaPadreID,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.FlotaPadreID, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.PersonalNombre,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PersonalNombre, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.PersonalTelefono,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PersonalTelefono, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.VehiculoUsuarioSeguimiento,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoUsuarioSeguimiento, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.WorkingNow,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.WorkingNow, esSystemType.Boolean));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsLatitudAnterior,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsLatitudAnterior, esSystemType.Double));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.GpsLongitudAnterior,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsLongitudAnterior, esSystemType.Double));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.AltaFecha,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.AltaFecha, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.BajaFecha,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.BajaFecha, esSystemType.DateTime));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.Id,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.Id, esSystemType.Decimal));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.BajaMotivo,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.BajaMotivo, esSystemType.String));
			_queryItems.Add(TerminalStateHistoricoMetadata.ColumnNames.SoftwareTipo,new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.SoftwareTipo, esSystemType.Byte));
			
	    }
	
	    #region esQueryItems

		public esQueryItem TerminalID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem VehiculoMatricula
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoMatricula, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoCodigo
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigo, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoCodigoADM
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigoADM, esSystemType.String); }
		} 
		
		public esQueryItem CentroTrabajoID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.CentroTrabajoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreaTrabajoID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoID, esSystemType.Decimal); }
		} 
		
		public esQueryItem AreaTrabajoColor
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoColor, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpresaID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.EmpresaID, esSystemType.Decimal); }
		} 
		
		public esQueryItem EmpresaColor
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.EmpresaColor, esSystemType.Decimal); }
		} 
		
		public esQueryItem FlotaID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.FlotaID, esSystemType.Decimal); }
		} 
		
		public esQueryItem PersonalID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PersonalID, esSystemType.Decimal); }
		} 
		
		public esQueryItem LoginFrom
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.LoginFrom, esSystemType.DateTime); }
		} 
		
		public esQueryItem LogoutFrom
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.LogoutFrom, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalEstado
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalEstado, esSystemType.Int32); }
		} 
		
		public esQueryItem TerminalProtocoloTipo
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalProtocoloTipo, esSystemType.Byte); }
		} 
		
		public esQueryItem TerminalNS
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalNS, esSystemType.String); }
		} 
		
		public esQueryItem TerminalIMEI
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalIMEI, esSystemType.String); }
		} 
		
		public esQueryItem TerminalNSSim
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalNSSim, esSystemType.String); }
		} 
		
		public esQueryItem TerminalVersionSW
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalVersionSW, esSystemType.String); }
		} 
		
		public esQueryItem GpsFecha
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsFecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem GpsLatitud
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsLatitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsLongitud
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsLongitud, esSystemType.Double); }
		} 
		
		public esQueryItem GpsFixMode
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsFixMode, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsFixGeoTx
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsFixGeoTx, esSystemType.String); }
		} 
		
		public esQueryItem GpsNSAT
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsNSAT, esSystemType.Byte); }
		} 
		
		public esQueryItem GpsAltitud
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsAltitud, esSystemType.Int16); }
		} 
		
		public esQueryItem GpsRumbo
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsRumbo, esSystemType.Decimal); }
		} 
		
		public esQueryItem GpsVelocidad
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsVelocidad, esSystemType.Int32); }
		} 
		
		public esQueryItem GpsDistancia
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsDistancia, esSystemType.Double); }
		} 
		
		public esQueryItem GpsFechaDetenido
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsFechaDetenido, esSystemType.DateTime); }
		} 
		
		public esQueryItem GpsPoblacion
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsPoblacion, esSystemType.String); }
		} 
		
		public esQueryItem FechaTerminalInfoMasActual
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.FechaTerminalInfoMasActual, esSystemType.DateTime); }
		} 
		
		public esQueryItem OdometroTt
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OdometroTt, esSystemType.Double); }
		} 
		
		public esQueryItem OdometroPc
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OdometroPc, esSystemType.Double); }
		} 
		
		public esQueryItem InputState
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.InputState, esSystemType.String); }
		} 
		
		public esQueryItem OutputState
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OutputState, esSystemType.String); }
		} 
		
		public esQueryItem ServerID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.ServerID, esSystemType.Byte); }
		} 
		
		public esQueryItem TerminalConnectedFrom
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalConnectedFrom, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalLastReception
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalLastReception, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalDisconnect
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnect, esSystemType.DateTime); }
		} 
		
		public esQueryItem TerminalDisconnectCount
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnectCount, esSystemType.Int16); }
		} 
		
		public esQueryItem TerminalLastSessionDuration
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalLastSessionDuration, esSystemType.Decimal); }
		} 
		
		public esQueryItem TerminalLastRestart
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.TerminalLastRestart, esSystemType.DateTime); }
		} 
		
		public esQueryItem UsuariosSeguimiento
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.UsuariosSeguimiento, esSystemType.String); }
		} 
		
		public esQueryItem ModoHistorico
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.ModoHistorico, esSystemType.Boolean); }
		} 
		
		public esQueryItem ModoHistoricoDesde
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.ModoHistoricoDesde, esSystemType.DateTime); }
		} 
		
		public esQueryItem PowerStateDesde
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PowerStateDesde, esSystemType.DateTime); }
		} 
		
		public esQueryItem PowerState
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PowerState, esSystemType.Int16); }
		} 
		
		public esQueryItem PowerStateTx
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PowerStateTx, esSystemType.String); }
		} 
		
		public esQueryItem GrupoID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GrupoID, esSystemType.Int32); }
		} 
		
		public esQueryItem VehiculoTelefono
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoTelefono, esSystemType.String); }
		} 
		
		public esQueryItem Comentario
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.Comentario, esSystemType.String); }
		} 
		
		public esQueryItem OnLine
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OnLine, esSystemType.Boolean); }
		} 
		
		public esQueryItem OffLineCausa
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.OffLineCausa, esSystemType.String); }
		} 
		
		public esQueryItem FlotaMadreID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.FlotaMadreID, esSystemType.Decimal); }
		} 
		
		public esQueryItem FlotaPadreID
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.FlotaPadreID, esSystemType.Decimal); }
		} 
		
		public esQueryItem PersonalNombre
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PersonalNombre, esSystemType.String); }
		} 
		
		public esQueryItem PersonalTelefono
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.PersonalTelefono, esSystemType.String); }
		} 
		
		public esQueryItem VehiculoUsuarioSeguimiento
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.VehiculoUsuarioSeguimiento, esSystemType.String); }
		} 
		
		public esQueryItem WorkingNow
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.WorkingNow, esSystemType.Boolean); }
		} 
		
		public esQueryItem GpsLatitudAnterior
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsLatitudAnterior, esSystemType.Double); }
		} 
		
		public esQueryItem GpsLongitudAnterior
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.GpsLongitudAnterior, esSystemType.Double); }
		} 
		
		public esQueryItem AltaFecha
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.AltaFecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem BajaFecha
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.BajaFecha, esSystemType.DateTime); }
		} 
		
		public esQueryItem Id
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.Id, esSystemType.Decimal); }
		} 
		
		public esQueryItem BajaMotivo
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.BajaMotivo, esSystemType.String); }
		} 
		
		public esQueryItem SoftwareTipo
		{
			get { return new esQueryItem(this, TerminalStateHistoricoMetadata.ColumnNames.SoftwareTipo, esSystemType.Byte); }
		} 
		
		#endregion
		
	}


	
	public partial class TerminalStateHistorico : esTerminalStateHistorico
	{

		
		
	}
	



	[Serializable]
	public partial class TerminalStateHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TerminalStateHistoricoMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalID, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalID;
			c.NumericPrecision = 18;
			c.Description = "Identificador del Terminal en GITS. Obtenida de GITS.dbo.TerminalesGPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.VehiculoID, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.VehiculoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.VehiculoMatricula, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.VehiculoMatricula;
			c.CharacterMaxLength = 12;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigo, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.VehiculoCodigo;
			c.CharacterMaxLength = 10;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.VehiculoCodigoADM, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.VehiculoCodigoADM;
			c.CharacterMaxLength = 30;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.CentroTrabajoID, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.CentroTrabajoID;
			c.NumericPrecision = 18;
			c.Description = "Campos para obtener rapidamente datos y rellenar la  tabla HistoricoTerminalData";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoID, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.AreaTrabajoID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.AreaTrabajoColor, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.AreaTrabajoColor;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.EmpresaID, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.EmpresaID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.EmpresaColor, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.EmpresaColor;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.FlotaID, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.FlotaID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.PersonalID, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.PersonalID;
			c.NumericPrecision = 18;
			c.Description = "Conductor Logado actualmente en el Vehiculo.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.LoginFrom, 12, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.LoginFrom;
			c.Description = "Fecha del Login mas actual. Si esta fecha tiene valor, la fecha del logout es Nula y a la inversa.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.LogoutFrom, 13, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.LogoutFrom;
			c.Description = "Fecha del Logout mas actual. Si esta fecha tiene valor, la fecha del login es Nula y a la inversa.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalEstado, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalEstado;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.Description = "Estado del Terminal. 0-Red 1-Verde. 2-Naranja 3-Amarillo 4-Azul ";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalProtocoloTipo, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalProtocoloTipo;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((2))";
			c.Description = "Identifica el Tipo de Protocolo de comunicacion con el Terminal. 1- MDT. 2-MDV.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalNS, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalNS;
			c.CharacterMaxLength = 30;
			c.Description = "Nº de Serie del Terminal o Identificacion del Terminal Unico guardado en GITS. El Proceso debe comparar los datos mandados por el Terminal (NSerie, IMEI,NSerieSim) y revisar si coincide alguno de ellos con la Identificacion del Dispositivo Terminal en GITS. Se obtiene de la Tabla GITS.dbo.TerminalesGPS campo mdt_NS.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalIMEI, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalIMEI;
			c.CharacterMaxLength = 30;
			c.Description = "Nº de IMEI del model del Terminal. Se rellena cuando lo envia el Terminal a realizar la conexion.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalNSSim, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalNSSim;
			c.CharacterMaxLength = 30;
			c.Description = "Numero de Serie de la tarjeta sim del terminal. Simplemente se utiliza de control.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalVersionSW, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalVersionSW;
			c.CharacterMaxLength = 50;
			c.Description = "Version del software del dispoisitvo, fundamental a la hora de update el software del Dispositvo.";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsFecha, 20, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsFecha;
			c.Description = "Fecha de la Ultima posicion GPS válida recibida. Las tramas de Estado y EstadoServicio, que mandan informacion GPS, siempre envia una posicion válida si el Terminal ha podido obtener una, independientemente que en el momento del envio de la trama no esté obteniendo posiciones validas. Para calcular este fecha, hay que mirar el campo GpsFixMode sea >1, que indica que el Fix es valido. Si no este fecha, no se actualiza.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsLatitud, 21, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsLatitud;
			c.NumericPrecision = 15;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsLongitud, 22, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsLongitud;
			c.NumericPrecision = 15;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsFixMode, 23, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsFixMode;
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.Description = "Calidad del Fix. 1-No valido 2- Valido 2D  3-Valido 3D";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsFixGeoTx, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsFixGeoTx;
			c.CharacterMaxLength = 500;
			c.Description = "Georefencia textual de la posicion obtenida en GPS";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsNSAT, 25, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsNSAT;
			c.NumericPrecision = 3;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsAltitud, 26, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsAltitud;
			c.NumericPrecision = 5;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsRumbo, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsRumbo;
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsVelocidad, 28, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsVelocidad;
			c.NumericPrecision = 10;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsDistancia, 29, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsDistancia;
			c.NumericPrecision = 15;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsFechaDetenido, 30, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsFechaDetenido;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsPoblacion, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsPoblacion;
			c.CharacterMaxLength = 500;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.FechaTerminalInfoMasActual, 32, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.FechaTerminalInfoMasActual;
			c.Description = "Fecha de la Ultima trama recibida desde el Terminal.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.OdometroTt, 33, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.OdometroTt;
			c.NumericPrecision = 15;
			c.Description = "Valor del Odometro Total";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.OdometroPc, 34, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.OdometroPc;
			c.NumericPrecision = 15;
			c.Description = "Valor del Odometro Parcial";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.InputState, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.InputState;
			c.CharacterMaxLength = 20;
			c.Description = "Valor hexadecimal de las Señales de entrada del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.OutputState, 36, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.OutputState;
			c.CharacterMaxLength = 20;
			c.Description = "Valor hexadecimal de las Señales de salida del Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.ServerID, 37, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.ServerID;
			c.NumericPrecision = 3;
			c.Description = "Servidor que gestiona la conexión de este Terminal. Esta vinculado a la conexion. Si estuviera desconectado,  este valor seria null.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalConnectedFrom, 38, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalConnectedFrom;
			c.Description = "Indica si el Terminal está conectado. Si es <> null => conectado sino Desconectado. Guarda la fecha del comienzo de conexion.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastReception, 39, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalLastReception;
			c.Description = "Fecha de la ultima recepcion obtenida desde este Terminal. ";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnect, 40, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalDisconnect;
			c.Description = "Si posee valor indica que el Terminal está desconectado, y marca la fecha de desconexion.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalDisconnectCount, 41, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalDisconnectCount;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastSessionDuration, 42, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalLastSessionDuration;
			c.NumericPrecision = 18;
			c.Description = "Duración en Horas de la ultima sesion ininterrumpida que este terminal estuvo conectado.  Es null si nunca se conecto este Terminal";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.TerminalLastRestart, 43, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.TerminalLastRestart;
			c.Description = "Fecha del ultimo reinicio forzado. Bien por reparación o fallo electrico, o por fallo del Terminal.";
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.UsuariosSeguimiento, 44, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.UsuariosSeguimiento;
			c.CharacterMaxLength = 200;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.ModoHistorico, 45, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.ModoHistorico;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.ModoHistoricoDesde, 46, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.ModoHistoricoDesde;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.PowerStateDesde, 47, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.PowerStateDesde;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.PowerState, 48, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.PowerState;
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.PowerStateTx, 49, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.PowerStateTx;
			c.CharacterMaxLength = 100;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GrupoID, 50, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GrupoID;
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.VehiculoTelefono, 51, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.VehiculoTelefono;
			c.CharacterMaxLength = 30;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.Comentario, 52, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.Comentario;
			c.CharacterMaxLength = 500;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.OnLine, 53, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.OnLine;
			c.HasDefault = true;
			c.Default = @"((1))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.OffLineCausa, 54, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.OffLineCausa;
			c.CharacterMaxLength = 150;
			c.HasDefault = true;
			c.Default = @"('')";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.FlotaMadreID, 55, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.FlotaMadreID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.FlotaPadreID, 56, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.FlotaPadreID;
			c.NumericPrecision = 18;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.PersonalNombre, 57, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.PersonalNombre;
			c.CharacterMaxLength = 500;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.PersonalTelefono, 58, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.PersonalTelefono;
			c.CharacterMaxLength = 100;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.VehiculoUsuarioSeguimiento, 59, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.VehiculoUsuarioSeguimiento;
			c.CharacterMaxLength = 200;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.WorkingNow, 60, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.WorkingNow;
			c.HasDefault = true;
			c.Default = @"((0))";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsLatitudAnterior, 61, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsLatitudAnterior;
			c.NumericPrecision = 15;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.GpsLongitudAnterior, 62, typeof(System.Double), esSystemType.Double);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.GpsLongitudAnterior;
			c.NumericPrecision = 15;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.AltaFecha, 63, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.AltaFecha;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.BajaFecha, 64, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.BajaFecha;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.Id, 65, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 18;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.BajaMotivo, 66, typeof(System.String), esSystemType.String);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.BajaMotivo;
			c.CharacterMaxLength = 500;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TerminalStateHistoricoMetadata.ColumnNames.SoftwareTipo, 67, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TerminalStateHistoricoMetadata.PropertyNames.SoftwareTipo;
			c.NumericPrecision = 3;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TerminalStateHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoCodigoADM = "VehiculoCodigoADM";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string AreaTrabajoColor = "AreaTrabajoColor";
			 public const string EmpresaID = "EmpresaID";
			 public const string EmpresaColor = "EmpresaColor";
			 public const string FlotaID = "FlotaID";
			 public const string PersonalID = "PersonalID";
			 public const string LoginFrom = "LoginFrom";
			 public const string LogoutFrom = "LogoutFrom";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string TerminalProtocoloTipo = "TerminalProtocoloTipo";
			 public const string TerminalNS = "TerminalNS";
			 public const string TerminalIMEI = "TerminalIMEI";
			 public const string TerminalNSSim = "TerminalNSSim";
			 public const string TerminalVersionSW = "TerminalVersionSW";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsNSAT = "GpsNSAT";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string GpsFechaDetenido = "GpsFechaDetenido";
			 public const string GpsPoblacion = "GpsPoblacion";
			 public const string FechaTerminalInfoMasActual = "FechaTerminalInfoMasActual";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string ServerID = "ServerID";
			 public const string TerminalConnectedFrom = "TerminalConnectedFrom";
			 public const string TerminalLastReception = "TerminalLastReception";
			 public const string TerminalDisconnect = "TerminalDisconnect";
			 public const string TerminalDisconnectCount = "TerminalDisconnectCount";
			 public const string TerminalLastSessionDuration = "TerminalLastSessionDuration";
			 public const string TerminalLastRestart = "TerminalLastRestart";
			 public const string UsuariosSeguimiento = "UsuariosSeguimiento";
			 public const string ModoHistorico = "ModoHistorico";
			 public const string ModoHistoricoDesde = "ModoHistoricoDesde";
			 public const string PowerStateDesde = "PowerStateDesde";
			 public const string PowerState = "PowerState";
			 public const string PowerStateTx = "PowerStateTx";
			 public const string GrupoID = "GrupoID";
			 public const string VehiculoTelefono = "VehiculoTelefono";
			 public const string Comentario = "Comentario";
			 public const string OnLine = "OnLine";
			 public const string OffLineCausa = "OffLineCausa";
			 public const string FlotaMadreID = "FlotaMadreID";
			 public const string FlotaPadreID = "FlotaPadreID";
			 public const string PersonalNombre = "PersonalNombre";
			 public const string PersonalTelefono = "PersonalTelefono";
			 public const string VehiculoUsuarioSeguimiento = "VehiculoUsuarioSeguimiento";
			 public const string WorkingNow = "WorkingNow";
			 public const string GpsLatitudAnterior = "GpsLatitudAnterior";
			 public const string GpsLongitudAnterior = "GpsLongitudAnterior";
			 public const string AltaFecha = "AltaFecha";
			 public const string BajaFecha = "BajaFecha";
			 public const string Id = "ID";
			 public const string BajaMotivo = "BajaMotivo";
			 public const string SoftwareTipo = "SoftwareTipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TerminalID = "TerminalID";
			 public const string VehiculoID = "VehiculoID";
			 public const string VehiculoMatricula = "VehiculoMatricula";
			 public const string VehiculoCodigo = "VehiculoCodigo";
			 public const string VehiculoCodigoADM = "VehiculoCodigoADM";
			 public const string CentroTrabajoID = "CentroTrabajoID";
			 public const string AreaTrabajoID = "AreaTrabajoID";
			 public const string AreaTrabajoColor = "AreaTrabajoColor";
			 public const string EmpresaID = "EmpresaID";
			 public const string EmpresaColor = "EmpresaColor";
			 public const string FlotaID = "FlotaID";
			 public const string PersonalID = "PersonalID";
			 public const string LoginFrom = "LoginFrom";
			 public const string LogoutFrom = "LogoutFrom";
			 public const string TerminalEstado = "TerminalEstado";
			 public const string TerminalProtocoloTipo = "TerminalProtocoloTipo";
			 public const string TerminalNS = "TerminalNS";
			 public const string TerminalIMEI = "TerminalIMEI";
			 public const string TerminalNSSim = "TerminalNSSim";
			 public const string TerminalVersionSW = "TerminalVersionSW";
			 public const string GpsFecha = "GpsFecha";
			 public const string GpsLatitud = "GpsLatitud";
			 public const string GpsLongitud = "GpsLongitud";
			 public const string GpsFixMode = "GpsFixMode";
			 public const string GpsFixGeoTx = "GpsFixGeoTx";
			 public const string GpsNSAT = "GpsNSAT";
			 public const string GpsAltitud = "GpsAltitud";
			 public const string GpsRumbo = "GpsRumbo";
			 public const string GpsVelocidad = "GpsVelocidad";
			 public const string GpsDistancia = "GpsDistancia";
			 public const string GpsFechaDetenido = "GpsFechaDetenido";
			 public const string GpsPoblacion = "GpsPoblacion";
			 public const string FechaTerminalInfoMasActual = "FechaTerminalInfoMasActual";
			 public const string OdometroTt = "OdometroTt";
			 public const string OdometroPc = "OdometroPc";
			 public const string InputState = "InputState";
			 public const string OutputState = "OutputState";
			 public const string ServerID = "ServerID";
			 public const string TerminalConnectedFrom = "TerminalConnectedFrom";
			 public const string TerminalLastReception = "TerminalLastReception";
			 public const string TerminalDisconnect = "TerminalDisconnect";
			 public const string TerminalDisconnectCount = "TerminalDisconnectCount";
			 public const string TerminalLastSessionDuration = "TerminalLastSessionDuration";
			 public const string TerminalLastRestart = "TerminalLastRestart";
			 public const string UsuariosSeguimiento = "UsuariosSeguimiento";
			 public const string ModoHistorico = "ModoHistorico";
			 public const string ModoHistoricoDesde = "ModoHistoricoDesde";
			 public const string PowerStateDesde = "PowerStateDesde";
			 public const string PowerState = "PowerState";
			 public const string PowerStateTx = "PowerStateTx";
			 public const string GrupoID = "GrupoID";
			 public const string VehiculoTelefono = "VehiculoTelefono";
			 public const string Comentario = "Comentario";
			 public const string OnLine = "OnLine";
			 public const string OffLineCausa = "OffLineCausa";
			 public const string FlotaMadreID = "FlotaMadreID";
			 public const string FlotaPadreID = "FlotaPadreID";
			 public const string PersonalNombre = "PersonalNombre";
			 public const string PersonalTelefono = "PersonalTelefono";
			 public const string VehiculoUsuarioSeguimiento = "VehiculoUsuarioSeguimiento";
			 public const string WorkingNow = "WorkingNow";
			 public const string GpsLatitudAnterior = "GpsLatitudAnterior";
			 public const string GpsLongitudAnterior = "GpsLongitudAnterior";
			 public const string AltaFecha = "AltaFecha";
			 public const string BajaFecha = "BajaFecha";
			 public const string Id = "Id";
			 public const string BajaMotivo = "BajaMotivo";
			 public const string SoftwareTipo = "SoftwareTipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TerminalStateHistoricoMetadata))
			{
				if(TerminalStateHistoricoMetadata.mapDelegates == null)
				{
					TerminalStateHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TerminalStateHistoricoMetadata.meta == null)
				{
					TerminalStateHistoricoMetadata.meta = new TerminalStateHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("TerminalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("VehiculoMatricula", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoCodigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VehiculoCodigoADM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CentroTrabajoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreaTrabajoID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("AreaTrabajoColor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpresaID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("EmpresaColor", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("FlotaID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PersonalID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("LoginFrom", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("LogoutFrom", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalEstado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TerminalProtocoloTipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TerminalNS", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalIMEI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalNSSim", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TerminalVersionSW", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("GpsFecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GpsLatitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsLongitud", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsFixMode", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsFixGeoTx", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("GpsNSAT", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GpsAltitud", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("GpsRumbo", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("GpsVelocidad", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("GpsDistancia", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsFechaDetenido", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("GpsPoblacion", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("FechaTerminalInfoMasActual", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("OdometroTt", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("OdometroPc", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("InputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("OutputState", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ServerID", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TerminalConnectedFrom", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalLastReception", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalDisconnect", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TerminalDisconnectCount", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TerminalLastSessionDuration", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("TerminalLastRestart", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("UsuariosSeguimiento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ModoHistorico", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("ModoHistoricoDesde", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PowerStateDesde", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PowerState", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("PowerStateTx", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("GrupoID", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("VehiculoTelefono", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("Comentario", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("OnLine", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("OffLineCausa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FlotaMadreID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("FlotaPadreID", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("PersonalNombre", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PersonalTelefono", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("VehiculoUsuarioSeguimiento", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("WorkingNow", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("GpsLatitudAnterior", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("GpsLongitudAnterior", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("AltaFecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("BajaFecha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Id", new esTypeMap("numeric", "System.Decimal"));
				meta.AddTypeMap("BajaMotivo", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("SoftwareTipo", new esTypeMap("tinyint", "System.Byte"));			
				meta.Catalog = "GITSCOMUNICAGPS";
				meta.Schema = "dbo";
				
				meta.Source = "TerminalStateHistorico";
				meta.Destination = "TerminalStateHistorico";
				
				meta.spInsert = "proc_TerminalStateHistoricoInsert";				
				meta.spUpdate = "proc_TerminalStateHistoricoUpdate";		
				meta.spDelete = "proc_TerminalStateHistoricoDelete";
				meta.spLoadAll = "proc_TerminalStateHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_TerminalStateHistoricoLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TerminalStateHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
