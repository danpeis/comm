﻿using System;
using gitspt.global.Core.IoC;
using Ninject;
using Ninject.Modules;

namespace gcperu.Server.Core.IoC
{
    public class NinjectIocContainer : IIocContainer
    {
        public readonly IKernel Kernel;

        public NinjectIocContainer(params INinjectModule[] modules)
        {
            Kernel = new StandardKernel(modules);
        }

        private NinjectIocContainer()
        {
            Kernel = new StandardKernel();
        }

        public object Get(Type type)
        {
            try
            {
                return Kernel.Get(type);
            }
            catch (TypeNotResolvedException exception)
            {
                throw new TypeNotResolvedException(exception);
            }
        }

        public T TryGet<T>()
        {
            return Kernel.TryGet<T>();
        }

        public T Get<T>()
        {
            try
            {
                return Kernel.Get<T>();
            }
            catch (TypeNotResolvedException exception)
            {
                throw new TypeNotResolvedException(exception);
            }
        }

        public T Get<T>(string name, string value)
        {
            var result = Kernel.TryGet<T>(metadata => metadata.Has(name) &&
                         (string.Equals(metadata.Get<string>(name), value,
                                        StringComparison.InvariantCultureIgnoreCase)));

            if (Equals(result, default(T))) throw new TypeNotResolvedException((Exception)null);
            return result;
        }

        public void Inject(object item)
        {
            Kernel.Inject(item);
        }
    }

    public class TypeNotResolvedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TypeNotResolvedException"/> class.
        /// </summary>
        public TypeNotResolvedException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TypeNotResolvedException"/> class.
        /// </summary>
        public TypeNotResolvedException(Exception ex)
            : base("", ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TypeNotResolvedException"/> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public TypeNotResolvedException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TypeNotResolvedException"/> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="innerException">The inner exception.</param>
        public TypeNotResolvedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

}