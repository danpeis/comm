﻿using System;

namespace gcperu.Contract
{

}

namespace gcperu.Server.Core
{
    public enum OperacionResult
    {
        OK = 1,
        Cancel = -2,
        None = 0,
        Err = -1
    }

    public enum SoftwareUpateMode
    {
        Automatico = 1,
        Selectivo = 2
    }

    public enum EstadoDescripcionClase
    {
        OffLineCausa = 1
    }

    public enum MotivoPeticionMoveToHistorico : byte
    {
        /// <summary>
        /// Peticion procesada Ok por terminal.
        /// </summary>
        Ok = 1,
        /// <summary>
        ///Peticion antigua y no procesada por el Terminal al que iba asignada.
        /// </summary>
        Antigua = 2,
        /// <summary>
        /// Indica que todos los Servicios de la Peticion, eran no válidos.
        /// </summary>
        TodosServiciosNoValidos = 3,
        /// <summary>
        /// La peticion no es reconocida en el sistema.
        /// </summary>
        PeticionUndefined = 4
    }

    /// <summary>
    /// Estado de visto en que puede estar un mensaje de texto enviado a los terminales.
    /// </summary>
    public enum MessageStateView
    {
        ViewByDevice = 1,
        ViewByPerson = 2
    }

    public enum PeticionCodeSend : byte
    {
        Undefined = 99,
        /// <summary>
        /// Es una respuesta que el Servidor envia
        /// </summary>
        Respuesta = 4,
        ConfiguracionGet = 5,
        ConfiguracionSet = 6,
        IncidenciasSend = 7,
        AutoConnectSOS = 8,
        /// <summary>
        /// Enviar la lista de los Perfiles Mobiles para redes de Comunicacion
        /// </summary>
        MobileNetworkProfilesSend = 11,
        ///<sumary>
        /// Mensaje. Envio de un mensaje al Terminal
        /// </sumary>
        Mensaje = 12
    }

    [Flags]
    public enum ServerIncidenceTypes : int
    {
        Undefined = 0,
        TerminalNoRegistrado = 1,
        ExcepcionGeneral = 2,
        SinConexionInternet = 8,
        ServidorNoPermitirConexiones = 16,
        MaxPercenTermDisconnect = 32

    }



}