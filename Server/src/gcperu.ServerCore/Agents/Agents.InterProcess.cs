﻿using System;
using System.Diagnostics.CodeAnalysis;
using gcperu.Server.Core.InterComm;
using gcperu.Server.Core.InterComm.GComunica.Core.IPC;

namespace gcperu.Server.Core.Agents
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static partial class InterProcessAgent
    {
        private const string WSMessageChannel = "WSWorker-Message";

        public static IObservable<MessageIPCinfo> WSWorkerMessageListenerObservable
        {
            get
            {
                return WSMessageChannel
                    .RegisterObservableOnChannel<MessageIPCinfo>();
            }
        }

        public static IDisposableObserver<MessageIPCinfo> WSWorkerBroadcastObserver
        {
            get
            {
                return WSMessageChannel
                    .GetOrRegisterBroadcastObserverOnChannel<MessageIPCinfo>();
            }
        }
    }
}