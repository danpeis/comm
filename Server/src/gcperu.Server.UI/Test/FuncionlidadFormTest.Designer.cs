﻿namespace gcperu.Server.UI.Test
{
    partial class FuncionlidadFormTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bsData = new System.Windows.Forms.BindingSource(this.components);
            this.cmdOk = new DevExpress.XtraEditors.SimpleButton();
            this.txtListaVeh = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtCondID = new DevExpress.XtraEditors.TextEdit();
            this.rdTipo = new DevExpress.XtraEditors.RadioGroup();
            this.txtListIDServices = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.bsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListaVeh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCondID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListIDServices.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdOk
            // 
            this.cmdOk.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOk.Appearance.Options.UseFont = true;
            this.cmdOk.Image = global::gcperu.Server.UI.Properties.Resources.Select16;
            this.cmdOk.Location = new System.Drawing.Point(460, 369);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(185, 32);
            this.cmdOk.TabIndex = 2;
            this.cmdOk.Text = "Ejecutar Accion";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // txtListaVeh
            // 
            this.txtListaVeh.EditValue = "";
            this.txtListaVeh.EnterMoveNextControl = true;
            this.txtListaVeh.Location = new System.Drawing.Point(163, 22);
            this.txtListaVeh.Name = "txtListaVeh";
            this.txtListaVeh.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.txtListaVeh.Properties.Appearance.Options.UseFont = true;
            this.txtListaVeh.Size = new System.Drawing.Size(226, 26);
            this.txtListaVeh.TabIndex = 19;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(30, 25);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(123, 16);
            this.labelControl3.TabIndex = 20;
            this.labelControl3.Text = "LISTA DE VEHICULOS";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(30, 59);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(74, 16);
            this.labelControl5.TabIndex = 29;
            this.labelControl5.Text = "Conductor ID";
            // 
            // txtCondID
            // 
            this.txtCondID.EditValue = "";
            this.txtCondID.EnterMoveNextControl = true;
            this.txtCondID.Location = new System.Drawing.Point(163, 56);
            this.txtCondID.Name = "txtCondID";
            this.txtCondID.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.txtCondID.Properties.Appearance.Options.UseFont = true;
            this.txtCondID.Size = new System.Drawing.Size(226, 26);
            this.txtCondID.TabIndex = 30;
            // 
            // rdTipo
            // 
            this.rdTipo.Location = new System.Drawing.Point(30, 214);
            this.rdTipo.Name = "rdTipo";
            this.rdTipo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTipo.Properties.Appearance.Options.UseFont = true;
            this.rdTipo.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("logout", "Enviar Trama Logout Forzado"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("InsertServices", "Insertar Servicios"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("EditServices", "Editar Servicios"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("DeleteServices", "Borrar Servicios")});
            this.rdTipo.Size = new System.Drawing.Size(369, 187);
            this.rdTipo.TabIndex = 32;
            // 
            // txtListIDServices
            // 
            this.txtListIDServices.EditValue = "";
            this.txtListIDServices.EnterMoveNextControl = true;
            this.txtListIDServices.Location = new System.Drawing.Point(163, 88);
            this.txtListIDServices.Name = "txtListIDServices";
            this.txtListIDServices.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.txtListIDServices.Properties.Appearance.Options.UseFont = true;
            this.txtListIDServices.Size = new System.Drawing.Size(482, 26);
            this.txtListIDServices.TabIndex = 34;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(30, 91);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(124, 16);
            this.labelControl1.TabIndex = 33;
            this.labelControl1.Text = "Lista Servicios Aplicar";
            // 
            // FuncionlidadFormTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 413);
            this.Controls.Add(this.txtListIDServices);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.rdTipo);
            this.Controls.Add(this.txtCondID);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.txtListaVeh);
            this.Controls.Add(this.cmdOk);
            this.Name = "FuncionlidadFormTest";
            this.Text = "Test de Funcionalidades";
            ((System.ComponentModel.ISupportInitialize)(this.bsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListaVeh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCondID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListIDServices.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bsData;
        private DevExpress.XtraEditors.SimpleButton cmdOk;
        internal DevExpress.XtraEditors.TextEdit txtListaVeh;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        internal DevExpress.XtraEditors.TextEdit txtCondID;
        private DevExpress.XtraEditors.RadioGroup rdTipo;
        internal DevExpress.XtraEditors.TextEdit txtListIDServices;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}