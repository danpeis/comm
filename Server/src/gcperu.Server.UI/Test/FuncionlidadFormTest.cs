﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using EntitySpaces.Interfaces;
using gcperu.Server.Core;
using gitspt.global.Core.Extension;
using Gurock.SmartInspect;

namespace gcperu.Server.UI.Test
{
    public partial class FuncionlidadFormTest : DevExpress.XtraEditors.XtraForm
    {
        private List<decimal> _listaVehiculos;

        public FuncionlidadFormTest()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private bool ValidateData()
        {
            if (txtListaVeh.Text=="")
            {
                MessageBox.Show("INDIQUE EL VEHICULO/VEHCIULOS DESTINATARIOS...", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return false;
            }

            return true;
        }

        private void FillListaVehiculos()
        {
            _listaVehiculos = new List<decimal>();

            foreach (string strVehID in txtListaVeh.Text.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
            {
                _listaVehiculos.Add(strVehID.ToDecimal());
            }
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;

            FillListaVehiculos();

            foreach (var vehid in _listaVehiculos)
            {
                switch (rdTipo.EditValue.ToString().ToUpper())
                {

                }

            }

            MessageBox.Show("Ejecución OK.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

       
        #region Funciones


        #endregion

    }
}