//using System;
//using System.Windows.Forms;
//using gcperu.Server.UI.Exception;
//using GComunica.Core.Exceptions;
//using GComunica.Core.Log;

//namespace ExeServer.Gestion
//{
//    public static class GlobalEventsDomain
//    {

//        public static void SetDomainEvents()
//        {

//            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
//            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
//            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);


//            TraceLog.LogInformation("SetDomainEvents.SetEvents. ThreadException,ApplicationExit EVENTOS ASIGNADOS");
//        }

//        public static void SetUIThreadEvents()
//        {
//            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
//            TraceLog.LogInformation("SetUIThreadEvents.SetEvents. EVENTOS ASIGNADOS");
//        }

//        static void Application_ApplicationExit(object sender, EventArgs e)
//        {
//            TraceLog.LogInformation("APLICATION EXITED.");
//        }

//        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
//        {
//            var ex = new GCException("SE HA PRODUCIDO UN ERROR GRAVE EN EL PROGRAMA.\nPROGRAMA VA A SER CERRADO PARA PREVENIR UN MAL FUNCIONAMIENTO", e.Exception, null);
//            TraceLog.LogException(ex);

//            ManejarExceptions.ManejarException(ex,ExceptionErrorLevel.Critical,false,true);

//            Application.Exit();
//        }

//        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
//        {
//            var ex = new Exception("SE HA PRODUCIDO UN ERROR GRAVE EN EL PROGRAMA.\nEL PROGRAMA VA A SER CERRADO PARA PREVENIR UN MAL FUNCIONAMIENTO", (Exception)e.ExceptionObject);
//            TraceLog.LogException(ex);

//            ManejarExceptions.ManejarException(ex, ExceptionErrorLevel.Critical, false, true);

//            if (e.IsTerminating)
//            {
//                //SplashScreen.CloseSplash();

//                //if (Global.Device != null)
//                //{
//                //    //Guardamos el estado del Dispositivo, antes de cerrar la aplicación.
//                //    Global.Device.SaveState();
//                //}

//                //Application.Exit();
//                AppDomain.Unload(AppDomain.CurrentDomain);
//            }
//        }

//    }
//}