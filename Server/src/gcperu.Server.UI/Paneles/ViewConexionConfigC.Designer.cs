﻿namespace GComunica.ExeServer.Paneles
{
    partial class ViewConexionConfigC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teConexionServerPort3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerPort2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerPort1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionIntentos = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionIntentos = new DevExpress.XtraEditors.LabelControl();
            this.cbConexionDefaultIP = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbConexionDefaultPositioIP = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionIntentos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbConexionDefaultIP.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // teConexionServerPort3
            // 
            this.teConexionServerPort3.EditValue = "1980";
            this.teConexionServerPort3.Location = new System.Drawing.Point(555, 184);
            this.teConexionServerPort3.Name = "teConexionServerPort3";
            this.teConexionServerPort3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort3.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerPort3.TabIndex = 104;
            // 
            // lbConexionServerPort3
            // 
            this.lbConexionServerPort3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort3.Appearance.Options.UseFont = true;
            this.lbConexionServerPort3.Location = new System.Drawing.Point(382, 187);
            this.lbConexionServerPort3.Name = "lbConexionServerPort3";
            this.lbConexionServerPort3.Size = new System.Drawing.Size(149, 18);
            this.lbConexionServerPort3.TabIndex = 103;
            this.lbConexionServerPort3.Text = "Puerto servidor 3º";
            // 
            // teConexionServerIP3
            // 
            this.teConexionServerIP3.EditValue = "192.168.50.110";
            this.teConexionServerIP3.Location = new System.Drawing.Point(556, 152);
            this.teConexionServerIP3.Name = "teConexionServerIP3";
            this.teConexionServerIP3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP3.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerIP3.TabIndex = 102;
            // 
            // lbConexionServerIP3
            // 
            this.lbConexionServerIP3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP3.Appearance.Options.UseFont = true;
            this.lbConexionServerIP3.Location = new System.Drawing.Point(381, 155);
            this.lbConexionServerIP3.Name = "lbConexionServerIP3";
            this.lbConexionServerIP3.Size = new System.Drawing.Size(122, 18);
            this.lbConexionServerIP3.TabIndex = 101;
            this.lbConexionServerIP3.Text = "Dirección IP 3º";
            // 
            // teConexionServerName3
            // 
            this.teConexionServerName3.EditValue = "";
            this.teConexionServerName3.Location = new System.Drawing.Point(556, 120);
            this.teConexionServerName3.Name = "teConexionServerName3";
            this.teConexionServerName3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName3.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerName3.TabIndex = 100;
            // 
            // lbConexionServerName3
            // 
            this.lbConexionServerName3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName3.Appearance.Options.UseFont = true;
            this.lbConexionServerName3.Location = new System.Drawing.Point(381, 123);
            this.lbConexionServerName3.Name = "lbConexionServerName3";
            this.lbConexionServerName3.Size = new System.Drawing.Size(160, 18);
            this.lbConexionServerName3.TabIndex = 99;
            this.lbConexionServerName3.Text = "Nombre Servidor 3º";
            // 
            // teConexionServerPort2
            // 
            this.teConexionServerPort2.EditValue = "1980";
            this.teConexionServerPort2.Location = new System.Drawing.Point(556, 67);
            this.teConexionServerPort2.Name = "teConexionServerPort2";
            this.teConexionServerPort2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort2.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerPort2.TabIndex = 98;
            // 
            // lbConexionServerPort2
            // 
            this.lbConexionServerPort2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort2.Appearance.Options.UseFont = true;
            this.lbConexionServerPort2.Location = new System.Drawing.Point(381, 70);
            this.lbConexionServerPort2.Name = "lbConexionServerPort2";
            this.lbConexionServerPort2.Size = new System.Drawing.Size(149, 18);
            this.lbConexionServerPort2.TabIndex = 97;
            this.lbConexionServerPort2.Text = "Puerto servidor 2º";
            // 
            // teConexionServerIP2
            // 
            this.teConexionServerIP2.EditValue = "192.168.50.110";
            this.teConexionServerIP2.Location = new System.Drawing.Point(556, 35);
            this.teConexionServerIP2.Name = "teConexionServerIP2";
            this.teConexionServerIP2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP2.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerIP2.TabIndex = 96;
            // 
            // lbConexionServerIP2
            // 
            this.lbConexionServerIP2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP2.Appearance.Options.UseFont = true;
            this.lbConexionServerIP2.Location = new System.Drawing.Point(381, 38);
            this.lbConexionServerIP2.Name = "lbConexionServerIP2";
            this.lbConexionServerIP2.Size = new System.Drawing.Size(122, 18);
            this.lbConexionServerIP2.TabIndex = 95;
            this.lbConexionServerIP2.Text = "Dirección IP 2º";
            // 
            // teConexionServerName2
            // 
            this.teConexionServerName2.EditValue = "";
            this.teConexionServerName2.Location = new System.Drawing.Point(556, 3);
            this.teConexionServerName2.Name = "teConexionServerName2";
            this.teConexionServerName2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName2.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerName2.TabIndex = 94;
            // 
            // lbConexionServerName2
            // 
            this.lbConexionServerName2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName2.Appearance.Options.UseFont = true;
            this.lbConexionServerName2.Location = new System.Drawing.Point(381, 6);
            this.lbConexionServerName2.Name = "lbConexionServerName2";
            this.lbConexionServerName2.Size = new System.Drawing.Size(160, 18);
            this.lbConexionServerName2.TabIndex = 93;
            this.lbConexionServerName2.Text = "Nombre Servidor 2º";
            // 
            // teConexionServerPort1
            // 
            this.teConexionServerPort1.EditValue = "1980";
            this.teConexionServerPort1.Location = new System.Drawing.Point(176, 184);
            this.teConexionServerPort1.Name = "teConexionServerPort1";
            this.teConexionServerPort1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort1.Size = new System.Drawing.Size(180, 26);
            this.teConexionServerPort1.TabIndex = 92;
            // 
            // lbConexionServerPort1
            // 
            this.lbConexionServerPort1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort1.Appearance.Options.UseFont = true;
            this.lbConexionServerPort1.Location = new System.Drawing.Point(6, 187);
            this.lbConexionServerPort1.Name = "lbConexionServerPort1";
            this.lbConexionServerPort1.Size = new System.Drawing.Size(149, 18);
            this.lbConexionServerPort1.TabIndex = 91;
            this.lbConexionServerPort1.Text = "Puerto servidor 1º";
            // 
            // teConexionServerIP1
            // 
            this.teConexionServerIP1.EditValue = "192.168.50.110";
            this.teConexionServerIP1.Location = new System.Drawing.Point(175, 152);
            this.teConexionServerIP1.Name = "teConexionServerIP1";
            this.teConexionServerIP1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP1.Size = new System.Drawing.Size(180, 26);
            this.teConexionServerIP1.TabIndex = 90;
            // 
            // lbConexionServerIP1
            // 
            this.lbConexionServerIP1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP1.Appearance.Options.UseFont = true;
            this.lbConexionServerIP1.Location = new System.Drawing.Point(5, 155);
            this.lbConexionServerIP1.Name = "lbConexionServerIP1";
            this.lbConexionServerIP1.Size = new System.Drawing.Size(122, 18);
            this.lbConexionServerIP1.TabIndex = 89;
            this.lbConexionServerIP1.Text = "Dirección IP 1º";
            // 
            // teConexionServerName1
            // 
            this.teConexionServerName1.EditValue = "";
            this.teConexionServerName1.Location = new System.Drawing.Point(175, 120);
            this.teConexionServerName1.Name = "teConexionServerName1";
            this.teConexionServerName1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName1.Size = new System.Drawing.Size(180, 26);
            this.teConexionServerName1.TabIndex = 88;
            // 
            // lbConexionServerName1
            // 
            this.lbConexionServerName1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName1.Appearance.Options.UseFont = true;
            this.lbConexionServerName1.Location = new System.Drawing.Point(4, 123);
            this.lbConexionServerName1.Name = "lbConexionServerName1";
            this.lbConexionServerName1.Size = new System.Drawing.Size(160, 18);
            this.lbConexionServerName1.TabIndex = 87;
            this.lbConexionServerName1.Text = "Nombre Servidor 1º";
            // 
            // teConexionIntentos
            // 
            this.teConexionIntentos.EditValue = "3";
            this.teConexionIntentos.Location = new System.Drawing.Point(176, 35);
            this.teConexionIntentos.Name = "teConexionIntentos";
            this.teConexionIntentos.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionIntentos.Properties.Appearance.Options.UseFont = true;
            this.teConexionIntentos.Size = new System.Drawing.Size(180, 26);
            this.teConexionIntentos.TabIndex = 86;
            // 
            // lbConexionIntentos
            // 
            this.lbConexionIntentos.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionIntentos.Appearance.Options.UseFont = true;
            this.lbConexionIntentos.Location = new System.Drawing.Point(6, 38);
            this.lbConexionIntentos.Name = "lbConexionIntentos";
            this.lbConexionIntentos.Size = new System.Drawing.Size(138, 18);
            this.lbConexionIntentos.TabIndex = 85;
            this.lbConexionIntentos.Text = "Intetos conexion";
            // 
            // cbConexionDefaultIP
            // 
            this.cbConexionDefaultIP.EditValue = "1";
            this.cbConexionDefaultIP.Location = new System.Drawing.Point(175, 3);
            this.cbConexionDefaultIP.Name = "cbConexionDefaultIP";
            this.cbConexionDefaultIP.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConexionDefaultIP.Properties.Appearance.Options.UseFont = true;
            this.cbConexionDefaultIP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbConexionDefaultIP.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbConexionDefaultIP.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbConexionDefaultIP.Size = new System.Drawing.Size(180, 26);
            this.cbConexionDefaultIP.TabIndex = 84;
            // 
            // lbConexionDefaultPositioIP
            // 
            this.lbConexionDefaultPositioIP.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionDefaultPositioIP.Appearance.Options.UseFont = true;
            this.lbConexionDefaultPositioIP.Location = new System.Drawing.Point(5, 6);
            this.lbConexionDefaultPositioIP.Name = "lbConexionDefaultPositioIP";
            this.lbConexionDefaultPositioIP.Size = new System.Drawing.Size(143, 18);
            this.lbConexionDefaultPositioIP.TabIndex = 83;
            this.lbConexionDefaultPositioIP.Text = "Nº IP por defecto";
            // 
            // ViewConexionConfigC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.teConexionServerPort3);
            this.Controls.Add(this.lbConexionServerPort3);
            this.Controls.Add(this.teConexionServerIP3);
            this.Controls.Add(this.lbConexionServerIP3);
            this.Controls.Add(this.teConexionServerName3);
            this.Controls.Add(this.lbConexionServerName3);
            this.Controls.Add(this.teConexionServerPort2);
            this.Controls.Add(this.lbConexionServerPort2);
            this.Controls.Add(this.teConexionServerIP2);
            this.Controls.Add(this.lbConexionServerIP2);
            this.Controls.Add(this.teConexionServerName2);
            this.Controls.Add(this.lbConexionServerName2);
            this.Controls.Add(this.teConexionServerPort1);
            this.Controls.Add(this.lbConexionServerPort1);
            this.Controls.Add(this.teConexionServerIP1);
            this.Controls.Add(this.lbConexionServerIP1);
            this.Controls.Add(this.teConexionServerName1);
            this.Controls.Add(this.lbConexionServerName1);
            this.Controls.Add(this.teConexionIntentos);
            this.Controls.Add(this.lbConexionIntentos);
            this.Controls.Add(this.cbConexionDefaultIP);
            this.Controls.Add(this.lbConexionDefaultPositioIP);
            this.Name = "ViewConexionConfigC";
            this.Size = new System.Drawing.Size(782, 283);
            this.Controls.SetChildIndex(this.lbConexionDefaultPositioIP, 0);
            this.Controls.SetChildIndex(this.cbConexionDefaultIP, 0);
            this.Controls.SetChildIndex(this.lbConexionIntentos, 0);
            this.Controls.SetChildIndex(this.teConexionIntentos, 0);
            this.Controls.SetChildIndex(this.lbConexionServerName1, 0);
            this.Controls.SetChildIndex(this.teConexionServerName1, 0);
            this.Controls.SetChildIndex(this.lbConexionServerIP1, 0);
            this.Controls.SetChildIndex(this.teConexionServerIP1, 0);
            this.Controls.SetChildIndex(this.lbConexionServerPort1, 0);
            this.Controls.SetChildIndex(this.teConexionServerPort1, 0);
            this.Controls.SetChildIndex(this.lbConexionServerName2, 0);
            this.Controls.SetChildIndex(this.teConexionServerName2, 0);
            this.Controls.SetChildIndex(this.lbConexionServerIP2, 0);
            this.Controls.SetChildIndex(this.teConexionServerIP2, 0);
            this.Controls.SetChildIndex(this.lbConexionServerPort2, 0);
            this.Controls.SetChildIndex(this.teConexionServerPort2, 0);
            this.Controls.SetChildIndex(this.lbConexionServerName3, 0);
            this.Controls.SetChildIndex(this.teConexionServerName3, 0);
            this.Controls.SetChildIndex(this.lbConexionServerIP3, 0);
            this.Controls.SetChildIndex(this.teConexionServerIP3, 0);
            this.Controls.SetChildIndex(this.lbConexionServerPort3, 0);
            this.Controls.SetChildIndex(this.teConexionServerPort3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionIntentos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbConexionDefaultIP.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit teConexionServerPort3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort3;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP3;
        private DevExpress.XtraEditors.TextEdit teConexionServerName3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName3;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort2;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP2;
        private DevExpress.XtraEditors.TextEdit teConexionServerName2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName2;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort1;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP1;
        private DevExpress.XtraEditors.TextEdit teConexionServerName1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName1;
        private DevExpress.XtraEditors.TextEdit teConexionIntentos;
        private DevExpress.XtraEditors.LabelControl lbConexionIntentos;
        private DevExpress.XtraEditors.ComboBoxEdit cbConexionDefaultIP;
        private DevExpress.XtraEditors.LabelControl lbConexionDefaultPositioIP;
    }
}
