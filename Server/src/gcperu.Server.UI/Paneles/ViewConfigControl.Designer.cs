﻿namespace GComunica.ExeServer.Paneles
{
    partial class ViewConfigControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.frOK = new DevExpress.XtraEditors.PanelControl();
            this.btOk = new DevExpress.XtraEditors.SimpleButton();
            this.btCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.frOK)).BeginInit();
            this.frOK.SuspendLayout();
            this.SuspendLayout();
            // 
            // frOK
            // 
            this.frOK.Controls.Add(this.btOk);
            this.frOK.Controls.Add(this.btCancel);
            this.frOK.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.frOK.Location = new System.Drawing.Point(0, 193);
            this.frOK.Name = "frOK";
            this.frOK.Size = new System.Drawing.Size(776, 58);
            this.frOK.TabIndex = 0;
            // 
            // btOk
            // 
            this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btOk.Appearance.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btOk.Appearance.Options.UseFont = true;
            this.btOk.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btOk.Location = new System.Drawing.Point(436, 11);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(324, 42);
            this.btOk.TabIndex = 6;
            this.btOk.Text = "Enviar";
            // 
            // btCancel
            // 
            this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btCancel.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCancel.Appearance.Options.UseFont = true;
            this.btCancel.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btCancel.Location = new System.Drawing.Point(21, 11);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(324, 42);
            this.btCancel.TabIndex = 5;
            this.btCancel.Text = "Cancelar";
            // 
            // ViewConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.frOK);
            this.Name = "ViewConfigControl";
            this.Size = new System.Drawing.Size(776, 251);
            ((System.ComponentModel.ISupportInitialize)(this.frOK)).EndInit();
            this.frOK.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl frOK;
        private DevExpress.XtraEditors.SimpleButton btOk;
        private DevExpress.XtraEditors.SimpleButton btCancel;


    }
}
