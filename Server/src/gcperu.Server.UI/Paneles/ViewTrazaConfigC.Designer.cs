﻿namespace GComunica.ExeServer.Paneles
{
    partial class ViewTrazaConfigC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcTrace = new DevExpress.XtraEditors.GroupControl();
            this.cbTraceProtocol = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTraceProtocol = new DevExpress.XtraEditors.LabelControl();
            this.cbTracePFileRotate = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTracePFileRotate = new DevExpress.XtraEditors.LabelControl();
            this.teTracePFileName = new DevExpress.XtraEditors.TextEdit();
            this.lbTracePFileName = new DevExpress.XtraEditors.LabelControl();
            this.teTracePFileMaxSize = new DevExpress.XtraEditors.TextEdit();
            this.lbTracePFileMaxSize = new DevExpress.XtraEditors.LabelControl();
            this.cbTraceLevel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTraceLevel = new DevExpress.XtraEditors.LabelControl();
            this.gcFtp = new DevExpress.XtraEditors.GroupControl();
            this.cbFtpModoEnvioLog = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbFtpModoEnvio = new DevExpress.XtraEditors.LabelControl();
            this.lbFtpLastDate = new DevExpress.XtraEditors.LabelControl();
            this.deFtpLastDateLog = new DevExpress.XtraEditors.DateEdit();
            this.teFtpRemoteHost = new DevExpress.XtraEditors.TextEdit();
            this.lbFtpRemoteHost = new DevExpress.XtraEditors.LabelControl();
            this.teFtpPort = new DevExpress.XtraEditors.TextEdit();
            this.lbFtpPort = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).BeginInit();
            this.gcTrace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceProtocol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTracePFileRotate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileMaxSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFtp)).BeginInit();
            this.gcFtp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbFtpModoEnvioLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpRemoteHost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPort.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcTrace
            // 
            this.gcTrace.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcTrace.AppearanceCaption.Options.UseFont = true;
            this.gcTrace.Controls.Add(this.cbTraceProtocol);
            this.gcTrace.Controls.Add(this.lbTraceProtocol);
            this.gcTrace.Controls.Add(this.cbTracePFileRotate);
            this.gcTrace.Controls.Add(this.lbTracePFileRotate);
            this.gcTrace.Controls.Add(this.teTracePFileName);
            this.gcTrace.Controls.Add(this.lbTracePFileName);
            this.gcTrace.Controls.Add(this.teTracePFileMaxSize);
            this.gcTrace.Controls.Add(this.lbTracePFileMaxSize);
            this.gcTrace.Controls.Add(this.cbTraceLevel);
            this.gcTrace.Controls.Add(this.lbTraceLevel);
            this.gcTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTrace.Enabled = false;
            this.gcTrace.Location = new System.Drawing.Point(0, 176);
            this.gcTrace.Name = "gcTrace";
            this.gcTrace.Size = new System.Drawing.Size(796, 169);
            this.gcTrace.TabIndex = 15;
            this.gcTrace.Text = "Trace";
            // 
            // cbTraceProtocol
            // 
            this.cbTraceProtocol.EditValue = "File";
            this.cbTraceProtocol.Location = new System.Drawing.Point(571, 103);
            this.cbTraceProtocol.Name = "cbTraceProtocol";
            this.cbTraceProtocol.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.Appearance.Options.UseFont = true;
            this.cbTraceProtocol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTraceProtocol.Properties.Items.AddRange(new object[] {
            "Unknown",
            "Memory",
            "File",
            "TcpIp"});
            this.cbTraceProtocol.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTraceProtocol.Size = new System.Drawing.Size(202, 26);
            this.cbTraceProtocol.TabIndex = 52;
            // 
            // lbTraceProtocol
            // 
            this.lbTraceProtocol.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTraceProtocol.Appearance.Options.UseFont = true;
            this.lbTraceProtocol.Location = new System.Drawing.Point(432, 108);
            this.lbTraceProtocol.Name = "lbTraceProtocol";
            this.lbTraceProtocol.Size = new System.Drawing.Size(76, 18);
            this.lbTraceProtocol.TabIndex = 51;
            this.lbTraceProtocol.Text = "Protocolo";
            // 
            // cbTracePFileRotate
            // 
            this.cbTracePFileRotate.EditValue = "Ninguno";
            this.cbTracePFileRotate.Location = new System.Drawing.Point(571, 51);
            this.cbTracePFileRotate.Name = "cbTracePFileRotate";
            this.cbTracePFileRotate.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.Appearance.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTracePFileRotate.Properties.Items.AddRange(new object[] {
            "None",
            "Hourly",
            "Daily"});
            this.cbTracePFileRotate.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTracePFileRotate.Size = new System.Drawing.Size(202, 26);
            this.cbTracePFileRotate.TabIndex = 50;
            // 
            // lbTracePFileRotate
            // 
            this.lbTracePFileRotate.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileRotate.Appearance.Options.UseFont = true;
            this.lbTracePFileRotate.Appearance.Options.UseTextOptions = true;
            this.lbTracePFileRotate.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTracePFileRotate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTracePFileRotate.Location = new System.Drawing.Point(432, 47);
            this.lbTracePFileRotate.Name = "lbTracePFileRotate";
            this.lbTracePFileRotate.Size = new System.Drawing.Size(110, 50);
            this.lbTracePFileRotate.TabIndex = 49;
            this.lbTracePFileRotate.Text = "Perioricidad creación";
            // 
            // teTracePFileName
            // 
            this.teTracePFileName.EditValue = "gitnavega.sil";
            this.teTracePFileName.Location = new System.Drawing.Point(211, 108);
            this.teTracePFileName.Name = "teTracePFileName";
            this.teTracePFileName.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTracePFileName.Properties.Appearance.Options.UseFont = true;
            this.teTracePFileName.Size = new System.Drawing.Size(200, 26);
            this.teTracePFileName.TabIndex = 48;
            // 
            // lbTracePFileName
            // 
            this.lbTracePFileName.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileName.Appearance.Options.UseFont = true;
            this.lbTracePFileName.Location = new System.Drawing.Point(13, 113);
            this.lbTracePFileName.Name = "lbTracePFileName";
            this.lbTracePFileName.Size = new System.Drawing.Size(127, 18);
            this.lbTracePFileName.TabIndex = 47;
            this.lbTracePFileName.Text = "Nombre archivo";
            // 
            // teTracePFileMaxSize
            // 
            this.teTracePFileMaxSize.EditValue = "";
            this.teTracePFileMaxSize.Location = new System.Drawing.Point(211, 76);
            this.teTracePFileMaxSize.Name = "teTracePFileMaxSize";
            this.teTracePFileMaxSize.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTracePFileMaxSize.Properties.Appearance.Options.UseFont = true;
            this.teTracePFileMaxSize.Size = new System.Drawing.Size(200, 26);
            this.teTracePFileMaxSize.TabIndex = 46;
            // 
            // lbTracePFileMaxSize
            // 
            this.lbTracePFileMaxSize.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileMaxSize.Appearance.Options.UseFont = true;
            this.lbTracePFileMaxSize.Location = new System.Drawing.Point(13, 81);
            this.lbTracePFileMaxSize.Name = "lbTracePFileMaxSize";
            this.lbTracePFileMaxSize.Size = new System.Drawing.Size(164, 18);
            this.lbTracePFileMaxSize.TabIndex = 45;
            this.lbTracePFileMaxSize.Text = "Max tamaño archivo";
            // 
            // cbTraceLevel
            // 
            this.cbTraceLevel.EditValue = "Debug";
            this.cbTraceLevel.Location = new System.Drawing.Point(211, 44);
            this.cbTraceLevel.Name = "cbTraceLevel";
            this.cbTraceLevel.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.Appearance.Options.UseFont = true;
            this.cbTraceLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTraceLevel.Properties.Items.AddRange(new object[] {
            "Debug",
            "Verbose",
            "Message",
            "Warning",
            "Error",
            "Fatal"});
            this.cbTraceLevel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTraceLevel.Size = new System.Drawing.Size(200, 26);
            this.cbTraceLevel.TabIndex = 38;
            // 
            // lbTraceLevel
            // 
            this.lbTraceLevel.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTraceLevel.Appearance.Options.UseFont = true;
            this.lbTraceLevel.Location = new System.Drawing.Point(13, 49);
            this.lbTraceLevel.Name = "lbTraceLevel";
            this.lbTraceLevel.Size = new System.Drawing.Size(72, 18);
            this.lbTraceLevel.TabIndex = 37;
            this.lbTraceLevel.Text = "Nivel log";
            // 
            // gcFtp
            // 
            this.gcFtp.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcFtp.AppearanceCaption.Options.UseFont = true;
            this.gcFtp.Controls.Add(this.cbFtpModoEnvioLog);
            this.gcFtp.Controls.Add(this.lbFtpModoEnvio);
            this.gcFtp.Controls.Add(this.lbFtpLastDate);
            this.gcFtp.Controls.Add(this.deFtpLastDateLog);
            this.gcFtp.Controls.Add(this.teFtpRemoteHost);
            this.gcFtp.Controls.Add(this.lbFtpRemoteHost);
            this.gcFtp.Controls.Add(this.teFtpPort);
            this.gcFtp.Controls.Add(this.lbFtpPort);
            this.gcFtp.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFtp.Enabled = false;
            this.gcFtp.Location = new System.Drawing.Point(0, 0);
            this.gcFtp.Name = "gcFtp";
            this.gcFtp.Size = new System.Drawing.Size(796, 176);
            this.gcFtp.TabIndex = 14;
            this.gcFtp.Text = "Configuración Ftp. Envio de Logs";
            // 
            // cbFtpModoEnvioLog
            // 
            this.cbFtpModoEnvioLog.EditValue = "Ninguno";
            this.cbFtpModoEnvioLog.Location = new System.Drawing.Point(211, 128);
            this.cbFtpModoEnvioLog.Name = "cbFtpModoEnvioLog";
            this.cbFtpModoEnvioLog.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.Appearance.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbFtpModoEnvioLog.Properties.Items.AddRange(new object[] {
            "Nada",
            "Diaria",
            "Horaria"});
            this.cbFtpModoEnvioLog.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbFtpModoEnvioLog.Size = new System.Drawing.Size(183, 26);
            this.cbFtpModoEnvioLog.TabIndex = 36;
            // 
            // lbFtpModoEnvio
            // 
            this.lbFtpModoEnvio.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpModoEnvio.Appearance.Options.UseFont = true;
            this.lbFtpModoEnvio.Location = new System.Drawing.Point(16, 133);
            this.lbFtpModoEnvio.Name = "lbFtpModoEnvio";
            this.lbFtpModoEnvio.Size = new System.Drawing.Size(145, 18);
            this.lbFtpModoEnvio.TabIndex = 35;
            this.lbFtpModoEnvio.Text = "Perioricidad envio";
            // 
            // lbFtpLastDate
            // 
            this.lbFtpLastDate.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpLastDate.Appearance.Options.UseFont = true;
            this.lbFtpLastDate.Location = new System.Drawing.Point(16, 101);
            this.lbFtpLastDate.Name = "lbFtpLastDate";
            this.lbFtpLastDate.Size = new System.Drawing.Size(154, 18);
            this.lbFtpLastDate.TabIndex = 34;
            this.lbFtpLastDate.Text = "Ultima fecha envio";
            // 
            // deFtpLastDateLog
            // 
            this.deFtpLastDateLog.EditValue = new System.DateTime(2010, 9, 2, 0, 0, 0, 0);
            this.deFtpLastDateLog.Location = new System.Drawing.Point(211, 96);
            this.deFtpLastDateLog.Name = "deFtpLastDateLog";
            this.deFtpLastDateLog.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deFtpLastDateLog.Properties.Appearance.Options.UseFont = true;
            this.deFtpLastDateLog.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFtpLastDateLog.Properties.DisplayFormat.FormatString = "g";
            this.deFtpLastDateLog.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFtpLastDateLog.Properties.Mask.EditMask = "g";
            this.deFtpLastDateLog.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFtpLastDateLog.Size = new System.Drawing.Size(183, 26);
            this.deFtpLastDateLog.TabIndex = 33;
            // 
            // teFtpRemoteHost
            // 
            this.teFtpRemoteHost.EditValue = "ftp.amcoex.es";
            this.teFtpRemoteHost.Location = new System.Drawing.Point(211, 62);
            this.teFtpRemoteHost.Name = "teFtpRemoteHost";
            this.teFtpRemoteHost.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpRemoteHost.Properties.Appearance.Options.UseFont = true;
            this.teFtpRemoteHost.Size = new System.Drawing.Size(183, 26);
            this.teFtpRemoteHost.TabIndex = 32;
            // 
            // lbFtpRemoteHost
            // 
            this.lbFtpRemoteHost.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpRemoteHost.Appearance.Options.UseFont = true;
            this.lbFtpRemoteHost.Location = new System.Drawing.Point(16, 67);
            this.lbFtpRemoteHost.Name = "lbFtpRemoteHost";
            this.lbFtpRemoteHost.Size = new System.Drawing.Size(103, 18);
            this.lbFtpRemoteHost.TabIndex = 31;
            this.lbFtpRemoteHost.Text = "Dirección ftp";
            // 
            // teFtpPort
            // 
            this.teFtpPort.EditValue = "21";
            this.teFtpPort.Location = new System.Drawing.Point(211, 30);
            this.teFtpPort.Name = "teFtpPort";
            this.teFtpPort.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpPort.Properties.Appearance.Options.UseFont = true;
            this.teFtpPort.Size = new System.Drawing.Size(183, 26);
            this.teFtpPort.TabIndex = 30;
            // 
            // lbFtpPort
            // 
            this.lbFtpPort.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpPort.Appearance.Options.UseFont = true;
            this.lbFtpPort.Location = new System.Drawing.Point(16, 35);
            this.lbFtpPort.Name = "lbFtpPort";
            this.lbFtpPort.Size = new System.Drawing.Size(53, 18);
            this.lbFtpPort.TabIndex = 29;
            this.lbFtpPort.Text = "Puerto";
            // 
            // ViewTrazaConfigC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcTrace);
            this.Controls.Add(this.gcFtp);
            this.Name = "ViewTrazaConfigC";
            this.Size = new System.Drawing.Size(796, 403);
            this.Controls.SetChildIndex(this.gcFtp, 0);
            this.Controls.SetChildIndex(this.gcTrace, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).EndInit();
            this.gcTrace.ResumeLayout(false);
            this.gcTrace.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceProtocol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTracePFileRotate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileMaxSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFtp)).EndInit();
            this.gcFtp.ResumeLayout(false);
            this.gcFtp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbFtpModoEnvioLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpRemoteHost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPort.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl gcTrace;
        private DevExpress.XtraEditors.ComboBoxEdit cbTraceProtocol;
        private DevExpress.XtraEditors.LabelControl lbTraceProtocol;
        private DevExpress.XtraEditors.ComboBoxEdit cbTracePFileRotate;
        private DevExpress.XtraEditors.LabelControl lbTracePFileRotate;
        private DevExpress.XtraEditors.TextEdit teTracePFileName;
        private DevExpress.XtraEditors.LabelControl lbTracePFileName;
        private DevExpress.XtraEditors.TextEdit teTracePFileMaxSize;
        private DevExpress.XtraEditors.LabelControl lbTracePFileMaxSize;
        private DevExpress.XtraEditors.ComboBoxEdit cbTraceLevel;
        private DevExpress.XtraEditors.LabelControl lbTraceLevel;
        private DevExpress.XtraEditors.GroupControl gcFtp;
        private DevExpress.XtraEditors.ComboBoxEdit cbFtpModoEnvioLog;
        private DevExpress.XtraEditors.LabelControl lbFtpModoEnvio;
        private DevExpress.XtraEditors.LabelControl lbFtpLastDate;
        private DevExpress.XtraEditors.DateEdit deFtpLastDateLog;
        private DevExpress.XtraEditors.TextEdit teFtpRemoteHost;
        private DevExpress.XtraEditors.LabelControl lbFtpRemoteHost;
        private DevExpress.XtraEditors.TextEdit teFtpPort;
        private DevExpress.XtraEditors.LabelControl lbFtpPort;
    }
}
