﻿namespace gcperu.Server.UI.Paneles.UC
{
    partial class TerminalConfigurationUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.LabelControl labelControl9;
            DevExpress.XtraEditors.LabelControl labelControl10;
            DevExpress.XtraEditors.LabelControl labelControl7;
            DevExpress.XtraEditors.LabelControl labelControl8;
            this.tbAdmin = new DevExpress.XtraTab.XtraTabControl();
            this.tbpAdminTerm = new DevExpress.XtraTab.XtraTabPage();
            this.gcTerminal = new DevExpress.XtraEditors.PanelControl();
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.tcbBehNoGpsReceptionMaxTime = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.tbcBehMaxFramesByPackage = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.tcbBehNoConnectionEndpointMaxTime = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.TbcBehNoConnectionInternetMaxTime = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.tbcBehFrecuencyShippingTime = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcTerminalMinWaitRsp = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcTerminalMaxWaitRsp = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcBehFrecuencyBuildStateFrameModeStopTime = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcBehThresholdStopModeSpeed = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcBehThresholdStopModeTime = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.lbTerminalIntervalSendCola = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalMinWaitRsp = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalMaxWaitRsp = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalTxDelayOn = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalTxDelayOff = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminal = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalUmbralTempParada = new DevExpress.XtraEditors.LabelControl();
            this.tbpEndpoints = new DevExpress.XtraTab.XtraTabPage();
            this.gcConexion = new DevExpress.XtraEditors.PanelControl();
            this.teConexionServerPort3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerPort2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerPort1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName1 = new DevExpress.XtraEditors.LabelControl();
            this.cbConexionDefaultIP = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbConexionDefaultPositioIP = new DevExpress.XtraEditors.LabelControl();
            this.tbpAdminFtpTrace = new DevExpress.XtraTab.XtraTabPage();
            this.gcTrace = new DevExpress.XtraEditors.GroupControl();
            this.cbTraceProtocol = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTraceProtocol = new DevExpress.XtraEditors.LabelControl();
            this.cbTracePFileRotate = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTracePFileRotate = new DevExpress.XtraEditors.LabelControl();
            this.teTracePFileName = new DevExpress.XtraEditors.TextEdit();
            this.lbTracePFileName = new DevExpress.XtraEditors.LabelControl();
            this.teTracePFileMaxSize = new DevExpress.XtraEditors.TextEdit();
            this.lbTracePFileMaxSize = new DevExpress.XtraEditors.LabelControl();
            this.cbTraceLevel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTraceLevel = new DevExpress.XtraEditors.LabelControl();
            this.gcFtp = new DevExpress.XtraEditors.GroupControl();
            this.teUpdatePwd = new DevExpress.XtraEditors.TextEdit();
            this.teUpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.teUpdateRemoteHost = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.teUpdatePathRemote = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.teFtpPathRemote = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.teFtpPwd = new DevExpress.XtraEditors.TextEdit();
            this.teFtpUser = new DevExpress.XtraEditors.TextEdit();
            this.cbFtpModoEnvioLog = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbFtpModoEnvio = new DevExpress.XtraEditors.LabelControl();
            this.lbFtpLastDate = new DevExpress.XtraEditors.LabelControl();
            this.deFtpLastDateLog = new DevExpress.XtraEditors.DateEdit();
            this.teFtpRemoteHost = new DevExpress.XtraEditors.TextEdit();
            this.lbFtpRemoteHost = new DevExpress.XtraEditors.LabelControl();
            this.teFtpPort = new DevExpress.XtraEditors.TextEdit();
            this.lbFtpPort = new DevExpress.XtraEditors.LabelControl();
            this.tbcBehScreenSaverTime = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ckBehLogToGpsPositions = new DevExpress.XtraEditors.CheckEdit();
            this.myTrackBarControl1 = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcBehDistanceMaxBeetweenConsecutivePoints = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.tbcBehEndpointTimeout = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.tbcBehRetryNumberByEndpoint = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            labelControl9 = new DevExpress.XtraEditors.LabelControl();
            labelControl10 = new DevExpress.XtraEditors.LabelControl();
            labelControl7 = new DevExpress.XtraEditors.LabelControl();
            labelControl8 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.tbAdmin)).BeginInit();
            this.tbAdmin.SuspendLayout();
            this.tbpAdminTerm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTerminal)).BeginInit();
            this.gcTerminal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehNoConnectionInternetRetryWithoutRestarSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbBehNoGpsReceptionMaxTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbBehNoGpsReceptionMaxTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehMaxFramesByPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehMaxFramesByPackage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbBehNoConnectionEndpointMaxTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbBehNoConnectionEndpointMaxTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbcBehNoConnectionInternetMaxTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbcBehNoConnectionInternetMaxTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyShippingTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyShippingTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyBuildStateFrameModeStopTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyBuildStateFrameModeStopTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehThresholdStopModeSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehThresholdStopModeSpeed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehThresholdStopModeTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehThresholdStopModeTime.Properties)).BeginInit();
            this.tbpEndpoints.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcConexion)).BeginInit();
            this.gcConexion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbConexionDefaultIP.Properties)).BeginInit();
            this.tbpAdminFtpTrace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).BeginInit();
            this.gcTrace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceProtocol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTracePFileRotate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileMaxSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFtp)).BeginInit();
            this.gcFtp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdatePwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdateRemoteHost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdatePathRemote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPathRemote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFtpModoEnvioLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpRemoteHost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehScreenSaverTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehScreenSaverTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckBehLogToGpsPositions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myTrackBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myTrackBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyBuildStateFrameModeMotionTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehDistanceMaxBeetweenConsecutivePoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehDistanceMaxBeetweenConsecutivePoints.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehEndpointTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehEndpointTimeout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehRetryNumberByEndpoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehRetryNumberByEndpoint.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tbAdmin
            // 
            this.tbAdmin.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAdmin.AppearancePage.Header.Options.UseFont = true;
            this.tbAdmin.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAdmin.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbAdmin.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tbAdmin.AppearancePage.HeaderActive.Options.UseForeColor = true;
            this.tbAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbAdmin.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tbAdmin.Location = new System.Drawing.Point(0, 0);
            this.tbAdmin.Name = "tbAdmin";
            this.tbAdmin.PaintStyleName = "PropertyView";
            this.tbAdmin.SelectedTabPage = this.tbpAdminTerm;
            this.tbAdmin.Size = new System.Drawing.Size(1234, 587);
            this.tbAdmin.TabIndex = 1;
            this.tbAdmin.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbpAdminTerm,
            this.tbpEndpoints,
            this.tbpAdminFtpTrace});
            // 
            // tbpAdminTerm
            // 
            this.tbpAdminTerm.Controls.Add(this.gcTerminal);
            this.tbpAdminTerm.Name = "tbpAdminTerm";
            this.tbpAdminTerm.Size = new System.Drawing.Size(1232, 556);
            this.tbpAdminTerm.Text = "Terminal";
            // 
            // gcTerminal
            // 
            this.gcTerminal.Controls.Add(this.tbcBehDistanceMaxBeetweenConsecutivePoints);
            this.gcTerminal.Controls.Add(this.labelControl3);
            this.gcTerminal.Controls.Add(this.tbcBehFrecuencyBuildStateFrameModeMotionTime);
            this.gcTerminal.Controls.Add(this.myTrackBarControl1);
            this.gcTerminal.Controls.Add(this.labelControl2);
            this.gcTerminal.Controls.Add(this.ckBehLogToGpsPositions);
            this.gcTerminal.Controls.Add(this.tbcBehScreenSaverTime);
            this.gcTerminal.Controls.Add(this.labelControl1);
            this.gcTerminal.Controls.Add(this.tbcBehNoConnectionInternetRetryWithoutRestarSystem);
            this.gcTerminal.Controls.Add(this.labelControl15);
            this.gcTerminal.Controls.Add(this.tcbBehNoGpsReceptionMaxTime);
            this.gcTerminal.Controls.Add(this.labelControl16);
            this.gcTerminal.Controls.Add(this.tbcBehMaxFramesByPackage);
            this.gcTerminal.Controls.Add(this.labelControl14);
            this.gcTerminal.Controls.Add(this.tcbBehNoConnectionEndpointMaxTime);
            this.gcTerminal.Controls.Add(this.TbcBehNoConnectionInternetMaxTime);
            this.gcTerminal.Controls.Add(this.labelControl18);
            this.gcTerminal.Controls.Add(this.labelControl19);
            this.gcTerminal.Controls.Add(this.tbcBehFrecuencyShippingTime);
            this.gcTerminal.Controls.Add(this.tbcTerminalMinWaitRsp);
            this.gcTerminal.Controls.Add(this.tbcTerminalMaxWaitRsp);
            this.gcTerminal.Controls.Add(this.tbcBehFrecuencyBuildStateFrameModeStopTime);
            this.gcTerminal.Controls.Add(this.tbcBehThresholdStopModeSpeed);
            this.gcTerminal.Controls.Add(this.tbcBehThresholdStopModeTime);
            this.gcTerminal.Controls.Add(this.lbTerminalIntervalSendCola);
            this.gcTerminal.Controls.Add(this.lbTerminalMinWaitRsp);
            this.gcTerminal.Controls.Add(this.lbTerminalMaxWaitRsp);
            this.gcTerminal.Controls.Add(this.lbTerminalTxDelayOn);
            this.gcTerminal.Controls.Add(this.lbTerminalTxDelayOff);
            this.gcTerminal.Controls.Add(this.lbTerminal);
            this.gcTerminal.Controls.Add(this.lbTerminalUmbralTempParada);
            this.gcTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTerminal.Enabled = false;
            this.gcTerminal.Location = new System.Drawing.Point(0, 0);
            this.gcTerminal.Name = "gcTerminal";
            this.gcTerminal.Size = new System.Drawing.Size(1232, 556);
            this.gcTerminal.TabIndex = 109;
            // 
            // tbcBehNoConnectionInternetRetryWithoutRestarSystem
            // 
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.EditValue = 5;
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Location = new System.Drawing.Point(232, 212);
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Name = "tbcBehNoConnectionInternetRetryWithoutRestarSystem";
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Properties.Appearance.Options.UseFont = true;
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Properties.LargeChange = 1;
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Properties.Maximum = 20;
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Size = new System.Drawing.Size(128, 45);
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.TabIndex = 160;
            this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Value = 5;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl15.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Location = new System.Drawing.Point(55, 208);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(168, 37);
            this.labelControl15.TabIndex = 159;
            this.labelControl15.Text = "Nº Intentos reset modem sin reinicio";
            // 
            // tcbBehNoGpsReceptionMaxTime
            // 
            this.tcbBehNoGpsReceptionMaxTime.EditValue = 60;
            this.tcbBehNoGpsReceptionMaxTime.Location = new System.Drawing.Point(232, 349);
            this.tcbBehNoGpsReceptionMaxTime.Name = "tcbBehNoGpsReceptionMaxTime";
            this.tcbBehNoGpsReceptionMaxTime.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbBehNoGpsReceptionMaxTime.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbBehNoGpsReceptionMaxTime.Properties.Appearance.Options.UseFont = true;
            this.tcbBehNoGpsReceptionMaxTime.Properties.Appearance.Options.UseForeColor = true;
            this.tcbBehNoGpsReceptionMaxTime.Properties.Maximum = 360;
            this.tcbBehNoGpsReceptionMaxTime.Properties.TickFrequency = 30;
            this.tcbBehNoGpsReceptionMaxTime.Size = new System.Drawing.Size(128, 45);
            this.tcbBehNoGpsReceptionMaxTime.TabIndex = 158;
            this.tcbBehNoGpsReceptionMaxTime.Value = 60;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.labelControl16.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Location = new System.Drawing.Point(20, 349);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(206, 33);
            this.labelControl16.TabIndex = 157;
            this.labelControl16.Text = "GPS: Máximo Tiempo sin Recepción";
            // 
            // tbcBehMaxFramesByPackage
            // 
            this.tbcBehMaxFramesByPackage.EditValue = 15;
            this.tbcBehMaxFramesByPackage.Location = new System.Drawing.Point(602, 178);
            this.tbcBehMaxFramesByPackage.Name = "tbcBehMaxFramesByPackage";
            this.tbcBehMaxFramesByPackage.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehMaxFramesByPackage.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehMaxFramesByPackage.Properties.Appearance.Options.UseFont = true;
            this.tbcBehMaxFramesByPackage.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehMaxFramesByPackage.Properties.Maximum = 30;
            this.tbcBehMaxFramesByPackage.Properties.Minimum = 5;
            this.tbcBehMaxFramesByPackage.Properties.SmallChange = 5;
            this.tbcBehMaxFramesByPackage.Properties.TickFrequency = 5;
            this.tbcBehMaxFramesByPackage.Size = new System.Drawing.Size(128, 45);
            this.tbcBehMaxFramesByPackage.TabIndex = 155;
            this.tbcBehMaxFramesByPackage.Value = 15;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.labelControl14.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Location = new System.Drawing.Point(393, 178);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(218, 34);
            this.labelControl14.TabIndex = 154;
            this.labelControl14.Text = "Maximo Nº de Tramas por Paquete";
            // 
            // tcbBehNoConnectionEndpointMaxTime
            // 
            this.tcbBehNoConnectionEndpointMaxTime.EditValue = 60;
            this.tcbBehNoConnectionEndpointMaxTime.Location = new System.Drawing.Point(232, 263);
            this.tcbBehNoConnectionEndpointMaxTime.Name = "tcbBehNoConnectionEndpointMaxTime";
            this.tcbBehNoConnectionEndpointMaxTime.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbBehNoConnectionEndpointMaxTime.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbBehNoConnectionEndpointMaxTime.Properties.Appearance.Options.UseFont = true;
            this.tcbBehNoConnectionEndpointMaxTime.Properties.Appearance.Options.UseForeColor = true;
            this.tcbBehNoConnectionEndpointMaxTime.Properties.Maximum = 360;
            this.tcbBehNoConnectionEndpointMaxTime.Properties.TickFrequency = 30;
            this.tcbBehNoConnectionEndpointMaxTime.Size = new System.Drawing.Size(128, 45);
            this.tcbBehNoConnectionEndpointMaxTime.TabIndex = 150;
            this.tcbBehNoConnectionEndpointMaxTime.Value = 60;
            // 
            // TbcBehNoConnectionInternetMaxTime
            // 
            this.TbcBehNoConnectionInternetMaxTime.EditValue = 8;
            this.TbcBehNoConnectionInternetMaxTime.Location = new System.Drawing.Point(232, 161);
            this.TbcBehNoConnectionInternetMaxTime.Name = "TbcBehNoConnectionInternetMaxTime";
            this.TbcBehNoConnectionInternetMaxTime.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbcBehNoConnectionInternetMaxTime.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TbcBehNoConnectionInternetMaxTime.Properties.Appearance.Options.UseFont = true;
            this.TbcBehNoConnectionInternetMaxTime.Properties.Appearance.Options.UseForeColor = true;
            this.TbcBehNoConnectionInternetMaxTime.Properties.Maximum = 60;
            this.TbcBehNoConnectionInternetMaxTime.Properties.TickFrequency = 10;
            this.TbcBehNoConnectionInternetMaxTime.Size = new System.Drawing.Size(128, 45);
            this.TbcBehNoConnectionInternetMaxTime.TabIndex = 149;
            this.TbcBehNoConnectionInternetMaxTime.Value = 8;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Location = new System.Drawing.Point(20, 161);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(209, 39);
            this.labelControl18.TabIndex = 148;
            this.labelControl18.Text = "Máximo Tiempo sin Conexión a Internet.";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.labelControl19.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.Location = new System.Drawing.Point(20, 263);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(206, 42);
            this.labelControl19.TabIndex = 147;
            this.labelControl19.Text = "Máximo Tiempo sin Conexión con Servidor Central.";
            // 
            // tbcBehFrecuencyShippingTime
            // 
            this.tbcBehFrecuencyShippingTime.EditValue = 15;
            this.tbcBehFrecuencyShippingTime.Location = new System.Drawing.Point(602, 116);
            this.tbcBehFrecuencyShippingTime.Name = "tbcBehFrecuencyShippingTime";
            this.tbcBehFrecuencyShippingTime.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehFrecuencyShippingTime.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehFrecuencyShippingTime.Properties.Appearance.Options.UseFont = true;
            this.tbcBehFrecuencyShippingTime.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehFrecuencyShippingTime.Properties.Maximum = 30;
            this.tbcBehFrecuencyShippingTime.Properties.Minimum = 5;
            this.tbcBehFrecuencyShippingTime.Properties.SmallChange = 5;
            this.tbcBehFrecuencyShippingTime.Properties.TickFrequency = 5;
            this.tbcBehFrecuencyShippingTime.Size = new System.Drawing.Size(128, 45);
            this.tbcBehFrecuencyShippingTime.TabIndex = 131;
            this.tbcBehFrecuencyShippingTime.Value = 15;
            // 
            // tbcTerminalMinWaitRsp
            // 
            this.tbcTerminalMinWaitRsp.EditValue = 8;
            this.tbcTerminalMinWaitRsp.Location = new System.Drawing.Point(602, 280);
            this.tbcTerminalMinWaitRsp.Name = "tbcTerminalMinWaitRsp";
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalMinWaitRsp.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalMinWaitRsp.Properties.Maximum = 15;
            this.tbcTerminalMinWaitRsp.Properties.Minimum = 5;
            this.tbcTerminalMinWaitRsp.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalMinWaitRsp.TabIndex = 130;
            this.tbcTerminalMinWaitRsp.Value = 8;
            // 
            // tbcTerminalMaxWaitRsp
            // 
            this.tbcTerminalMaxWaitRsp.EditValue = 60;
            this.tbcTerminalMaxWaitRsp.Location = new System.Drawing.Point(602, 229);
            this.tbcTerminalMaxWaitRsp.Name = "tbcTerminalMaxWaitRsp";
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalMaxWaitRsp.Properties.Maximum = 90;
            this.tbcTerminalMaxWaitRsp.Properties.Minimum = 15;
            this.tbcTerminalMaxWaitRsp.Properties.TickFrequency = 10;
            this.tbcTerminalMaxWaitRsp.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalMaxWaitRsp.TabIndex = 129;
            this.tbcTerminalMaxWaitRsp.Value = 60;
            // 
            // tbcBehFrecuencyBuildStateFrameModeStopTime
            // 
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.EditValue = 15;
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Location = new System.Drawing.Point(602, 18);
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Name = "tbcBehFrecuencyBuildStateFrameModeStopTime";
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Properties.Appearance.Options.UseFont = true;
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Properties.Maximum = 30;
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Properties.Minimum = 5;
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Properties.TickFrequency = 3;
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Size = new System.Drawing.Size(128, 45);
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.TabIndex = 128;
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.Value = 15;
            this.tbcBehFrecuencyBuildStateFrameModeStopTime.EditValueChanged += new System.EventHandler(this.tbcBehFrecuencyBuildStateFrameModeStopTime_EditValueChanged);
            // 
            // tbcBehThresholdStopModeSpeed
            // 
            this.tbcBehThresholdStopModeSpeed.EditValue = 10;
            this.tbcBehThresholdStopModeSpeed.Location = new System.Drawing.Point(232, 59);
            this.tbcBehThresholdStopModeSpeed.Name = "tbcBehThresholdStopModeSpeed";
            this.tbcBehThresholdStopModeSpeed.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehThresholdStopModeSpeed.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehThresholdStopModeSpeed.Properties.Appearance.Options.UseFont = true;
            this.tbcBehThresholdStopModeSpeed.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehThresholdStopModeSpeed.Properties.Maximum = 20;
            this.tbcBehThresholdStopModeSpeed.Properties.Minimum = 5;
            this.tbcBehThresholdStopModeSpeed.Properties.TickFrequency = 2;
            this.tbcBehThresholdStopModeSpeed.Size = new System.Drawing.Size(128, 45);
            this.tbcBehThresholdStopModeSpeed.TabIndex = 125;
            this.tbcBehThresholdStopModeSpeed.Value = 10;
            // 
            // tbcBehThresholdStopModeTime
            // 
            this.tbcBehThresholdStopModeTime.EditValue = 7;
            this.tbcBehThresholdStopModeTime.Location = new System.Drawing.Point(232, 11);
            this.tbcBehThresholdStopModeTime.Name = "tbcBehThresholdStopModeTime";
            this.tbcBehThresholdStopModeTime.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehThresholdStopModeTime.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehThresholdStopModeTime.Properties.Appearance.Options.UseFont = true;
            this.tbcBehThresholdStopModeTime.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehThresholdStopModeTime.Properties.Minimum = 3;
            this.tbcBehThresholdStopModeTime.Size = new System.Drawing.Size(128, 45);
            this.tbcBehThresholdStopModeTime.TabIndex = 124;
            this.tbcBehThresholdStopModeTime.Value = 7;
            // 
            // lbTerminalIntervalSendCola
            // 
            this.lbTerminalIntervalSendCola.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalIntervalSendCola.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalIntervalSendCola.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalIntervalSendCola.Location = new System.Drawing.Point(393, 121);
            this.lbTerminalIntervalSendCola.Name = "lbTerminalIntervalSendCola";
            this.lbTerminalIntervalSendCola.Size = new System.Drawing.Size(200, 43);
            this.lbTerminalIntervalSendCola.TabIndex = 122;
            this.lbTerminalIntervalSendCola.Text = "Frecuencia Envio de Paquetes (seg.)";
            // 
            // lbTerminalMinWaitRsp
            // 
            this.lbTerminalMinWaitRsp.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMinWaitRsp.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMinWaitRsp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMinWaitRsp.Location = new System.Drawing.Point(393, 282);
            this.lbTerminalMinWaitRsp.Name = "lbTerminalMinWaitRsp";
            this.lbTerminalMinWaitRsp.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalMinWaitRsp.TabIndex = 121;
            this.lbTerminalMinWaitRsp.Text = "Conexion Servidor Central: Mínimo Tiempo espera respuesta (seg)";
            // 
            // lbTerminalMaxWaitRsp
            // 
            this.lbTerminalMaxWaitRsp.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMaxWaitRsp.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMaxWaitRsp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMaxWaitRsp.Location = new System.Drawing.Point(393, 229);
            this.lbTerminalMaxWaitRsp.Name = "lbTerminalMaxWaitRsp";
            this.lbTerminalMaxWaitRsp.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalMaxWaitRsp.TabIndex = 120;
            this.lbTerminalMaxWaitRsp.Text = "Conexion Servidor Central: Máximo Tiempo espera respuesta (seg)";
            // 
            // lbTerminalTxDelayOn
            // 
            this.lbTerminalTxDelayOn.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalTxDelayOn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalTxDelayOn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalTxDelayOn.Location = new System.Drawing.Point(393, 67);
            this.lbTerminalTxDelayOn.Name = "lbTerminalTxDelayOn";
            this.lbTerminalTxDelayOn.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalTxDelayOn.TabIndex = 119;
            this.lbTerminalTxDelayOn.Text = "Frecuencia Envio Tramas en Movimiento (Tramas/min.)";
            // 
            // lbTerminalTxDelayOff
            // 
            this.lbTerminalTxDelayOff.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalTxDelayOff.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalTxDelayOff.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalTxDelayOff.Location = new System.Drawing.Point(393, 18);
            this.lbTerminalTxDelayOff.Name = "lbTerminalTxDelayOff";
            this.lbTerminalTxDelayOff.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalTxDelayOff.TabIndex = 118;
            this.lbTerminalTxDelayOff.Text = "Frecuencia Generar Trama Estado en Parada (Tramas/min.)";
            this.lbTerminalTxDelayOff.Click += new System.EventHandler(this.lbTerminalTxDelayOff_Click);
            // 
            // lbTerminal
            // 
            this.lbTerminal.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminal.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminal.Location = new System.Drawing.Point(20, 64);
            this.lbTerminal.Name = "lbTerminal";
            this.lbTerminal.Size = new System.Drawing.Size(209, 34);
            this.lbTerminal.TabIndex = 112;
            this.lbTerminal.Text = "Umbral de Parada: Velocidad (km/h)";
            // 
            // lbTerminalUmbralTempParada
            // 
            this.lbTerminalUmbralTempParada.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalUmbralTempParada.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalUmbralTempParada.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalUmbralTempParada.Location = new System.Drawing.Point(23, 13);
            this.lbTerminalUmbralTempParada.Name = "lbTerminalUmbralTempParada";
            this.lbTerminalUmbralTempParada.Size = new System.Drawing.Size(203, 40);
            this.lbTerminalUmbralTempParada.TabIndex = 111;
            this.lbTerminalUmbralTempParada.Text = "Umbral Modo Parada: (Tiempo|min)";
            // 
            // tbpEndpoints
            // 
            this.tbpEndpoints.Controls.Add(this.gcConexion);
            this.tbpEndpoints.Name = "tbpEndpoints";
            this.tbpEndpoints.Size = new System.Drawing.Size(1232, 556);
            this.tbpEndpoints.Text = "Servidor Central";
            // 
            // gcConexion
            // 
            this.gcConexion.Controls.Add(this.tbcBehRetryNumberByEndpoint);
            this.gcConexion.Controls.Add(this.labelControl5);
            this.gcConexion.Controls.Add(this.tbcBehEndpointTimeout);
            this.gcConexion.Controls.Add(this.labelControl4);
            this.gcConexion.Controls.Add(this.teConexionServerPort3);
            this.gcConexion.Controls.Add(this.lbConexionServerPort3);
            this.gcConexion.Controls.Add(this.teConexionServerIP3);
            this.gcConexion.Controls.Add(this.lbConexionServerIP3);
            this.gcConexion.Controls.Add(this.teConexionServerName3);
            this.gcConexion.Controls.Add(this.lbConexionServerName3);
            this.gcConexion.Controls.Add(this.teConexionServerPort2);
            this.gcConexion.Controls.Add(this.lbConexionServerPort2);
            this.gcConexion.Controls.Add(this.teConexionServerIP2);
            this.gcConexion.Controls.Add(this.lbConexionServerIP2);
            this.gcConexion.Controls.Add(this.teConexionServerName2);
            this.gcConexion.Controls.Add(this.lbConexionServerName2);
            this.gcConexion.Controls.Add(this.teConexionServerPort1);
            this.gcConexion.Controls.Add(this.lbConexionServerPort1);
            this.gcConexion.Controls.Add(this.teConexionServerIP1);
            this.gcConexion.Controls.Add(this.lbConexionServerIP1);
            this.gcConexion.Controls.Add(this.teConexionServerName1);
            this.gcConexion.Controls.Add(this.lbConexionServerName1);
            this.gcConexion.Controls.Add(this.cbConexionDefaultIP);
            this.gcConexion.Controls.Add(this.lbConexionDefaultPositioIP);
            this.gcConexion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcConexion.Enabled = false;
            this.gcConexion.Location = new System.Drawing.Point(0, 0);
            this.gcConexion.Name = "gcConexion";
            this.gcConexion.Size = new System.Drawing.Size(1232, 556);
            this.gcConexion.TabIndex = 83;
            // 
            // teConexionServerPort3
            // 
            this.teConexionServerPort3.EditValue = "1980";
            this.teConexionServerPort3.Location = new System.Drawing.Point(605, 238);
            this.teConexionServerPort3.Name = "teConexionServerPort3";
            this.teConexionServerPort3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort3.Size = new System.Drawing.Size(192, 22);
            this.teConexionServerPort3.TabIndex = 108;
            // 
            // lbConexionServerPort3
            // 
            this.lbConexionServerPort3.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort3.Location = new System.Drawing.Point(462, 238);
            this.lbConexionServerPort3.Name = "lbConexionServerPort3";
            this.lbConexionServerPort3.Size = new System.Drawing.Size(120, 16);
            this.lbConexionServerPort3.TabIndex = 107;
            this.lbConexionServerPort3.Text = "Puerto servidor 3º";
            // 
            // teConexionServerIP3
            // 
            this.teConexionServerIP3.EditValue = "192.168.50.110";
            this.teConexionServerIP3.Location = new System.Drawing.Point(606, 206);
            this.teConexionServerIP3.Name = "teConexionServerIP3";
            this.teConexionServerIP3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP3.Size = new System.Drawing.Size(192, 22);
            this.teConexionServerIP3.TabIndex = 106;
            // 
            // lbConexionServerIP3
            // 
            this.lbConexionServerIP3.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP3.Location = new System.Drawing.Point(461, 206);
            this.lbConexionServerIP3.Name = "lbConexionServerIP3";
            this.lbConexionServerIP3.Size = new System.Drawing.Size(98, 16);
            this.lbConexionServerIP3.TabIndex = 105;
            this.lbConexionServerIP3.Text = "Dirección IP 3º";
            // 
            // teConexionServerName3
            // 
            this.teConexionServerName3.EditValue = "";
            this.teConexionServerName3.Location = new System.Drawing.Point(606, 174);
            this.teConexionServerName3.Name = "teConexionServerName3";
            this.teConexionServerName3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName3.Size = new System.Drawing.Size(192, 22);
            this.teConexionServerName3.TabIndex = 104;
            // 
            // lbConexionServerName3
            // 
            this.lbConexionServerName3.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName3.Location = new System.Drawing.Point(461, 174);
            this.lbConexionServerName3.Name = "lbConexionServerName3";
            this.lbConexionServerName3.Size = new System.Drawing.Size(128, 16);
            this.lbConexionServerName3.TabIndex = 103;
            this.lbConexionServerName3.Text = "Nombre Servidor 3º";
            // 
            // teConexionServerPort2
            // 
            this.teConexionServerPort2.EditValue = "1980";
            this.teConexionServerPort2.Location = new System.Drawing.Point(236, 345);
            this.teConexionServerPort2.Name = "teConexionServerPort2";
            this.teConexionServerPort2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort2.Size = new System.Drawing.Size(192, 22);
            this.teConexionServerPort2.TabIndex = 102;
            // 
            // lbConexionServerPort2
            // 
            this.lbConexionServerPort2.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort2.Location = new System.Drawing.Point(23, 348);
            this.lbConexionServerPort2.Name = "lbConexionServerPort2";
            this.lbConexionServerPort2.Size = new System.Drawing.Size(120, 16);
            this.lbConexionServerPort2.TabIndex = 101;
            this.lbConexionServerPort2.Text = "Puerto servidor 2º";
            // 
            // teConexionServerIP2
            // 
            this.teConexionServerIP2.EditValue = "192.168.50.110";
            this.teConexionServerIP2.Location = new System.Drawing.Point(236, 313);
            this.teConexionServerIP2.Name = "teConexionServerIP2";
            this.teConexionServerIP2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP2.Size = new System.Drawing.Size(192, 22);
            this.teConexionServerIP2.TabIndex = 100;
            // 
            // lbConexionServerIP2
            // 
            this.lbConexionServerIP2.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP2.Location = new System.Drawing.Point(23, 316);
            this.lbConexionServerIP2.Name = "lbConexionServerIP2";
            this.lbConexionServerIP2.Size = new System.Drawing.Size(98, 16);
            this.lbConexionServerIP2.TabIndex = 99;
            this.lbConexionServerIP2.Text = "Dirección IP 2º";
            // 
            // teConexionServerName2
            // 
            this.teConexionServerName2.EditValue = "";
            this.teConexionServerName2.Location = new System.Drawing.Point(236, 281);
            this.teConexionServerName2.Name = "teConexionServerName2";
            this.teConexionServerName2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName2.Size = new System.Drawing.Size(192, 22);
            this.teConexionServerName2.TabIndex = 98;
            // 
            // lbConexionServerName2
            // 
            this.lbConexionServerName2.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName2.Location = new System.Drawing.Point(23, 284);
            this.lbConexionServerName2.Name = "lbConexionServerName2";
            this.lbConexionServerName2.Size = new System.Drawing.Size(128, 16);
            this.lbConexionServerName2.TabIndex = 97;
            this.lbConexionServerName2.Text = "Nombre Servidor 2º";
            // 
            // teConexionServerPort1
            // 
            this.teConexionServerPort1.EditValue = "1980";
            this.teConexionServerPort1.Location = new System.Drawing.Point(233, 238);
            this.teConexionServerPort1.Name = "teConexionServerPort1";
            this.teConexionServerPort1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort1.Size = new System.Drawing.Size(195, 22);
            this.teConexionServerPort1.TabIndex = 96;
            // 
            // lbConexionServerPort1
            // 
            this.lbConexionServerPort1.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort1.Location = new System.Drawing.Point(25, 241);
            this.lbConexionServerPort1.Name = "lbConexionServerPort1";
            this.lbConexionServerPort1.Size = new System.Drawing.Size(120, 16);
            this.lbConexionServerPort1.TabIndex = 95;
            this.lbConexionServerPort1.Text = "Puerto servidor 1º";
            // 
            // teConexionServerIP1
            // 
            this.teConexionServerIP1.EditValue = "192.168.50.110";
            this.teConexionServerIP1.Location = new System.Drawing.Point(232, 206);
            this.teConexionServerIP1.Name = "teConexionServerIP1";
            this.teConexionServerIP1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP1.Size = new System.Drawing.Size(195, 22);
            this.teConexionServerIP1.TabIndex = 94;
            // 
            // lbConexionServerIP1
            // 
            this.lbConexionServerIP1.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP1.Location = new System.Drawing.Point(24, 209);
            this.lbConexionServerIP1.Name = "lbConexionServerIP1";
            this.lbConexionServerIP1.Size = new System.Drawing.Size(98, 16);
            this.lbConexionServerIP1.TabIndex = 93;
            this.lbConexionServerIP1.Text = "Dirección IP 1º";
            // 
            // teConexionServerName1
            // 
            this.teConexionServerName1.EditValue = "";
            this.teConexionServerName1.Location = new System.Drawing.Point(232, 174);
            this.teConexionServerName1.Name = "teConexionServerName1";
            this.teConexionServerName1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName1.Size = new System.Drawing.Size(195, 22);
            this.teConexionServerName1.TabIndex = 92;
            // 
            // lbConexionServerName1
            // 
            this.lbConexionServerName1.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName1.Location = new System.Drawing.Point(23, 177);
            this.lbConexionServerName1.Name = "lbConexionServerName1";
            this.lbConexionServerName1.Size = new System.Drawing.Size(128, 16);
            this.lbConexionServerName1.TabIndex = 91;
            this.lbConexionServerName1.Text = "Nombre Servidor 1º";
            // 
            // cbConexionDefaultIP
            // 
            this.cbConexionDefaultIP.EditValue = "1";
            this.cbConexionDefaultIP.Location = new System.Drawing.Point(232, 29);
            this.cbConexionDefaultIP.Name = "cbConexionDefaultIP";
            this.cbConexionDefaultIP.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConexionDefaultIP.Properties.Appearance.Options.UseFont = true;
            this.cbConexionDefaultIP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConexionDefaultIP.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbConexionDefaultIP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbConexionDefaultIP.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbConexionDefaultIP.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbConexionDefaultIP.Size = new System.Drawing.Size(128, 22);
            this.cbConexionDefaultIP.TabIndex = 86;
            // 
            // lbConexionDefaultPositioIP
            // 
            this.lbConexionDefaultPositioIP.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionDefaultPositioIP.Location = new System.Drawing.Point(23, 32);
            this.lbConexionDefaultPositioIP.Name = "lbConexionDefaultPositioIP";
            this.lbConexionDefaultPositioIP.Size = new System.Drawing.Size(116, 16);
            this.lbConexionDefaultPositioIP.TabIndex = 85;
            this.lbConexionDefaultPositioIP.Text = "Nº IP por defecto";
            // 
            // tbpAdminFtpTrace
            // 
            this.tbpAdminFtpTrace.Controls.Add(this.gcTrace);
            this.tbpAdminFtpTrace.Controls.Add(this.gcFtp);
            this.tbpAdminFtpTrace.Name = "tbpAdminFtpTrace";
            this.tbpAdminFtpTrace.Size = new System.Drawing.Size(1232, 556);
            this.tbpAdminFtpTrace.Text = "Traza/Log";
            // 
            // gcTrace
            // 
            this.gcTrace.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcTrace.AppearanceCaption.Options.UseFont = true;
            this.gcTrace.Controls.Add(this.cbTraceProtocol);
            this.gcTrace.Controls.Add(this.lbTraceProtocol);
            this.gcTrace.Controls.Add(this.cbTracePFileRotate);
            this.gcTrace.Controls.Add(this.lbTracePFileRotate);
            this.gcTrace.Controls.Add(this.teTracePFileName);
            this.gcTrace.Controls.Add(this.lbTracePFileName);
            this.gcTrace.Controls.Add(this.teTracePFileMaxSize);
            this.gcTrace.Controls.Add(this.lbTracePFileMaxSize);
            this.gcTrace.Controls.Add(this.cbTraceLevel);
            this.gcTrace.Controls.Add(this.lbTraceLevel);
            this.gcTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTrace.Enabled = false;
            this.gcTrace.Location = new System.Drawing.Point(0, 234);
            this.gcTrace.Name = "gcTrace";
            this.gcTrace.Size = new System.Drawing.Size(1232, 322);
            this.gcTrace.TabIndex = 13;
            this.gcTrace.Text = "Trace";
            // 
            // cbTraceProtocol
            // 
            this.cbTraceProtocol.EditValue = "File";
            this.cbTraceProtocol.Enabled = false;
            this.cbTraceProtocol.Location = new System.Drawing.Point(571, 103);
            this.cbTraceProtocol.Name = "cbTraceProtocol";
            this.cbTraceProtocol.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.Appearance.Options.UseFont = true;
            this.cbTraceProtocol.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTraceProtocol.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTraceProtocol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTraceProtocol.Properties.Items.AddRange(new object[] {
            "Unknown",
            "Memory",
            "File",
            "TcpIp"});
            this.cbTraceProtocol.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTraceProtocol.Size = new System.Drawing.Size(185, 24);
            this.cbTraceProtocol.TabIndex = 52;
            // 
            // lbTraceProtocol
            // 
            this.lbTraceProtocol.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTraceProtocol.Location = new System.Drawing.Point(432, 108);
            this.lbTraceProtocol.Name = "lbTraceProtocol";
            this.lbTraceProtocol.Size = new System.Drawing.Size(76, 18);
            this.lbTraceProtocol.TabIndex = 51;
            this.lbTraceProtocol.Text = "Protocolo";
            // 
            // cbTracePFileRotate
            // 
            this.cbTracePFileRotate.EditValue = "Ninguno";
            this.cbTracePFileRotate.Location = new System.Drawing.Point(571, 51);
            this.cbTracePFileRotate.Name = "cbTracePFileRotate";
            this.cbTracePFileRotate.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.Appearance.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTracePFileRotate.Properties.Items.AddRange(new object[] {
            "None",
            "Hourly",
            "Daily"});
            this.cbTracePFileRotate.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTracePFileRotate.Size = new System.Drawing.Size(185, 24);
            this.cbTracePFileRotate.TabIndex = 50;
            // 
            // lbTracePFileRotate
            // 
            this.lbTracePFileRotate.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileRotate.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTracePFileRotate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTracePFileRotate.Location = new System.Drawing.Point(432, 47);
            this.lbTracePFileRotate.Name = "lbTracePFileRotate";
            this.lbTracePFileRotate.Size = new System.Drawing.Size(110, 50);
            this.lbTracePFileRotate.TabIndex = 49;
            this.lbTracePFileRotate.Text = "Perioricidad creación";
            // 
            // teTracePFileName
            // 
            this.teTracePFileName.EditValue = "gitnavega.sil";
            this.teTracePFileName.Location = new System.Drawing.Point(211, 108);
            this.teTracePFileName.Name = "teTracePFileName";
            this.teTracePFileName.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTracePFileName.Properties.Appearance.Options.UseFont = true;
            this.teTracePFileName.Size = new System.Drawing.Size(200, 24);
            this.teTracePFileName.TabIndex = 48;
            // 
            // lbTracePFileName
            // 
            this.lbTracePFileName.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileName.Location = new System.Drawing.Point(13, 113);
            this.lbTracePFileName.Name = "lbTracePFileName";
            this.lbTracePFileName.Size = new System.Drawing.Size(127, 18);
            this.lbTracePFileName.TabIndex = 47;
            this.lbTracePFileName.Text = "Nombre archivo";
            // 
            // teTracePFileMaxSize
            // 
            this.teTracePFileMaxSize.EditValue = "";
            this.teTracePFileMaxSize.Location = new System.Drawing.Point(211, 76);
            this.teTracePFileMaxSize.Name = "teTracePFileMaxSize";
            this.teTracePFileMaxSize.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTracePFileMaxSize.Properties.Appearance.Options.UseFont = true;
            this.teTracePFileMaxSize.Size = new System.Drawing.Size(200, 24);
            this.teTracePFileMaxSize.TabIndex = 46;
            // 
            // lbTracePFileMaxSize
            // 
            this.lbTracePFileMaxSize.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileMaxSize.Location = new System.Drawing.Point(13, 81);
            this.lbTracePFileMaxSize.Name = "lbTracePFileMaxSize";
            this.lbTracePFileMaxSize.Size = new System.Drawing.Size(164, 18);
            this.lbTracePFileMaxSize.TabIndex = 45;
            this.lbTracePFileMaxSize.Text = "Max tamaño archivo";
            // 
            // cbTraceLevel
            // 
            this.cbTraceLevel.EditValue = "Debug";
            this.cbTraceLevel.Location = new System.Drawing.Point(211, 44);
            this.cbTraceLevel.Name = "cbTraceLevel";
            this.cbTraceLevel.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.Appearance.Options.UseFont = true;
            this.cbTraceLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTraceLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTraceLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTraceLevel.Properties.Items.AddRange(new object[] {
            "Debug",
            "Verbose",
            "Message",
            "Warning",
            "Error",
            "Fatal"});
            this.cbTraceLevel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTraceLevel.Size = new System.Drawing.Size(200, 24);
            this.cbTraceLevel.TabIndex = 38;
            // 
            // lbTraceLevel
            // 
            this.lbTraceLevel.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTraceLevel.Location = new System.Drawing.Point(13, 49);
            this.lbTraceLevel.Name = "lbTraceLevel";
            this.lbTraceLevel.Size = new System.Drawing.Size(72, 18);
            this.lbTraceLevel.TabIndex = 37;
            this.lbTraceLevel.Text = "Nivel log";
            // 
            // gcFtp
            // 
            this.gcFtp.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcFtp.AppearanceCaption.Options.UseFont = true;
            this.gcFtp.Controls.Add(this.teUpdatePwd);
            this.gcFtp.Controls.Add(labelControl9);
            this.gcFtp.Controls.Add(this.teUpdateUser);
            this.gcFtp.Controls.Add(labelControl10);
            this.gcFtp.Controls.Add(this.teUpdateRemoteHost);
            this.gcFtp.Controls.Add(this.labelControl11);
            this.gcFtp.Controls.Add(this.teUpdatePathRemote);
            this.gcFtp.Controls.Add(this.labelControl12);
            this.gcFtp.Controls.Add(this.teFtpPathRemote);
            this.gcFtp.Controls.Add(this.labelControl13);
            this.gcFtp.Controls.Add(this.teFtpPwd);
            this.gcFtp.Controls.Add(labelControl7);
            this.gcFtp.Controls.Add(this.teFtpUser);
            this.gcFtp.Controls.Add(labelControl8);
            this.gcFtp.Controls.Add(this.cbFtpModoEnvioLog);
            this.gcFtp.Controls.Add(this.lbFtpModoEnvio);
            this.gcFtp.Controls.Add(this.lbFtpLastDate);
            this.gcFtp.Controls.Add(this.deFtpLastDateLog);
            this.gcFtp.Controls.Add(this.teFtpRemoteHost);
            this.gcFtp.Controls.Add(this.lbFtpRemoteHost);
            this.gcFtp.Controls.Add(this.teFtpPort);
            this.gcFtp.Controls.Add(this.lbFtpPort);
            this.gcFtp.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFtp.Enabled = false;
            this.gcFtp.Location = new System.Drawing.Point(0, 0);
            this.gcFtp.Name = "gcFtp";
            this.gcFtp.Size = new System.Drawing.Size(1232, 234);
            this.gcFtp.TabIndex = 12;
            this.gcFtp.Text = "Configuración Ftp. Envio de Logs";
            // 
            // teUpdatePwd
            // 
            this.teUpdatePwd.EditValue = "3141516";
            this.teUpdatePwd.Location = new System.Drawing.Point(620, 173);
            this.teUpdatePwd.Name = "teUpdatePwd";
            this.teUpdatePwd.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teUpdatePwd.Properties.Appearance.Options.UseFont = true;
            this.teUpdatePwd.Size = new System.Drawing.Size(136, 22);
            this.teUpdatePwd.TabIndex = 60;
            // 
            // labelControl9
            // 
            labelControl9.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl9.Location = new System.Drawing.Point(619, 151);
            labelControl9.Name = "labelControl9";
            labelControl9.Size = new System.Drawing.Size(36, 16);
            labelControl9.TabIndex = 59;
            labelControl9.Text = "Clave";
            // 
            // teUpdateUser
            // 
            this.teUpdateUser.EditValue = "soft2867";
            this.teUpdateUser.Location = new System.Drawing.Point(411, 173);
            this.teUpdateUser.Name = "teUpdateUser";
            this.teUpdateUser.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teUpdateUser.Properties.Appearance.Options.UseFont = true;
            this.teUpdateUser.Size = new System.Drawing.Size(203, 22);
            this.teUpdateUser.TabIndex = 58;
            // 
            // labelControl10
            // 
            labelControl10.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl10.Location = new System.Drawing.Point(414, 151);
            labelControl10.Name = "labelControl10";
            labelControl10.Size = new System.Drawing.Size(48, 16);
            labelControl10.TabIndex = 57;
            labelControl10.Text = "Usuario";
            // 
            // teUpdateRemoteHost
            // 
            this.teUpdateRemoteHost.EditValue = "ftp.amcoex.es";
            this.teUpdateRemoteHost.Location = new System.Drawing.Point(214, 174);
            this.teUpdateRemoteHost.Name = "teUpdateRemoteHost";
            this.teUpdateRemoteHost.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teUpdateRemoteHost.Properties.Appearance.Options.UseFont = true;
            this.teUpdateRemoteHost.Size = new System.Drawing.Size(183, 22);
            this.teUpdateRemoteHost.TabIndex = 56;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(16, 177);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(143, 16);
            this.labelControl11.TabIndex = 55;
            this.labelControl11.Text = "Servidor FTP Updates";
            // 
            // teUpdatePathRemote
            // 
            this.teUpdatePathRemote.AllowDrop = true;
            this.teUpdatePathRemote.EditValue = "";
            this.teUpdatePathRemote.Location = new System.Drawing.Point(211, 201);
            this.teUpdatePathRemote.Name = "teUpdatePathRemote";
            this.teUpdatePathRemote.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teUpdatePathRemote.Properties.Appearance.Options.UseFont = true;
            this.teUpdatePathRemote.Size = new System.Drawing.Size(542, 22);
            this.teUpdatePathRemote.TabIndex = 54;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(16, 204);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(143, 16);
            this.labelControl12.TabIndex = 53;
            this.labelControl12.Text = "Path Remoto Updates";
            // 
            // teFtpPathRemote
            // 
            this.teFtpPathRemote.AllowDrop = true;
            this.teFtpPathRemote.EditValue = "";
            this.teFtpPathRemote.Location = new System.Drawing.Point(211, 109);
            this.teFtpPathRemote.Name = "teFtpPathRemote";
            this.teFtpPathRemote.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpPathRemote.Properties.Appearance.Options.UseFont = true;
            this.teFtpPathRemote.Size = new System.Drawing.Size(542, 22);
            this.teFtpPathRemote.TabIndex = 52;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(16, 112);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(112, 16);
            this.labelControl13.TabIndex = 51;
            this.labelControl13.Text = "Path Remoto Log";
            // 
            // teFtpPwd
            // 
            this.teFtpPwd.EditValue = "3141516";
            this.teFtpPwd.Location = new System.Drawing.Point(623, 82);
            this.teFtpPwd.Name = "teFtpPwd";
            this.teFtpPwd.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpPwd.Properties.Appearance.Options.UseFont = true;
            this.teFtpPwd.Size = new System.Drawing.Size(136, 22);
            this.teFtpPwd.TabIndex = 44;
            // 
            // labelControl7
            // 
            labelControl7.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl7.Location = new System.Drawing.Point(623, 60);
            labelControl7.Name = "labelControl7";
            labelControl7.Size = new System.Drawing.Size(36, 16);
            labelControl7.TabIndex = 43;
            labelControl7.Text = "Clave";
            // 
            // teFtpUser
            // 
            this.teFtpUser.EditValue = "soft2867";
            this.teFtpUser.Location = new System.Drawing.Point(418, 82);
            this.teFtpUser.Name = "teFtpUser";
            this.teFtpUser.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpUser.Properties.Appearance.Options.UseFont = true;
            this.teFtpUser.Size = new System.Drawing.Size(199, 22);
            this.teFtpUser.TabIndex = 42;
            // 
            // labelControl8
            // 
            labelControl8.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl8.Location = new System.Drawing.Point(418, 60);
            labelControl8.Name = "labelControl8";
            labelControl8.Size = new System.Drawing.Size(48, 16);
            labelControl8.TabIndex = 41;
            labelControl8.Text = "Usuario";
            // 
            // cbFtpModoEnvioLog
            // 
            this.cbFtpModoEnvioLog.EditValue = "Ninguno";
            this.cbFtpModoEnvioLog.Location = new System.Drawing.Point(558, 30);
            this.cbFtpModoEnvioLog.Name = "cbFtpModoEnvioLog";
            this.cbFtpModoEnvioLog.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.Appearance.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbFtpModoEnvioLog.Properties.Items.AddRange(new object[] {
            "Nada",
            "Diaria",
            "Horaria"});
            this.cbFtpModoEnvioLog.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbFtpModoEnvioLog.Size = new System.Drawing.Size(183, 22);
            this.cbFtpModoEnvioLog.TabIndex = 36;
            // 
            // lbFtpModoEnvio
            // 
            this.lbFtpModoEnvio.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpModoEnvio.Location = new System.Drawing.Point(418, 33);
            this.lbFtpModoEnvio.Name = "lbFtpModoEnvio";
            this.lbFtpModoEnvio.Size = new System.Drawing.Size(115, 16);
            this.lbFtpModoEnvio.TabIndex = 35;
            this.lbFtpModoEnvio.Text = "Perioricidad envio";
            // 
            // lbFtpLastDate
            // 
            this.lbFtpLastDate.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpLastDate.Location = new System.Drawing.Point(16, 85);
            this.lbFtpLastDate.Name = "lbFtpLastDate";
            this.lbFtpLastDate.Size = new System.Drawing.Size(122, 16);
            this.lbFtpLastDate.TabIndex = 34;
            this.lbFtpLastDate.Text = "Ultima fecha envio";
            // 
            // deFtpLastDateLog
            // 
            this.deFtpLastDateLog.EditValue = new System.DateTime(2010, 9, 2, 0, 0, 0, 0);
            this.deFtpLastDateLog.Location = new System.Drawing.Point(211, 82);
            this.deFtpLastDateLog.Name = "deFtpLastDateLog";
            this.deFtpLastDateLog.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deFtpLastDateLog.Properties.Appearance.Options.UseFont = true;
            this.deFtpLastDateLog.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFtpLastDateLog.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFtpLastDateLog.Properties.DisplayFormat.FormatString = "g";
            this.deFtpLastDateLog.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFtpLastDateLog.Properties.Mask.EditMask = "g";
            this.deFtpLastDateLog.Size = new System.Drawing.Size(183, 22);
            this.deFtpLastDateLog.TabIndex = 33;
            // 
            // teFtpRemoteHost
            // 
            this.teFtpRemoteHost.EditValue = "ftp.amcoex.es";
            this.teFtpRemoteHost.Location = new System.Drawing.Point(211, 55);
            this.teFtpRemoteHost.Name = "teFtpRemoteHost";
            this.teFtpRemoteHost.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpRemoteHost.Properties.Appearance.Options.UseFont = true;
            this.teFtpRemoteHost.Size = new System.Drawing.Size(183, 22);
            this.teFtpRemoteHost.TabIndex = 32;
            // 
            // lbFtpRemoteHost
            // 
            this.lbFtpRemoteHost.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpRemoteHost.Location = new System.Drawing.Point(16, 58);
            this.lbFtpRemoteHost.Name = "lbFtpRemoteHost";
            this.lbFtpRemoteHost.Size = new System.Drawing.Size(84, 16);
            this.lbFtpRemoteHost.TabIndex = 31;
            this.lbFtpRemoteHost.Text = "Dirección ftp";
            // 
            // teFtpPort
            // 
            this.teFtpPort.EditValue = "21";
            this.teFtpPort.Location = new System.Drawing.Point(211, 30);
            this.teFtpPort.Name = "teFtpPort";
            this.teFtpPort.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpPort.Properties.Appearance.Options.UseFont = true;
            this.teFtpPort.Size = new System.Drawing.Size(183, 22);
            this.teFtpPort.TabIndex = 30;
            // 
            // lbFtpPort
            // 
            this.lbFtpPort.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpPort.Location = new System.Drawing.Point(16, 35);
            this.lbFtpPort.Name = "lbFtpPort";
            this.lbFtpPort.Size = new System.Drawing.Size(43, 16);
            this.lbFtpPort.TabIndex = 29;
            this.lbFtpPort.Text = "Puerto";
            // 
            // tbcBehScreenSaverTime
            // 
            this.tbcBehScreenSaverTime.EditValue = 3;
            this.tbcBehScreenSaverTime.Location = new System.Drawing.Point(232, 110);
            this.tbcBehScreenSaverTime.Name = "tbcBehScreenSaverTime";
            this.tbcBehScreenSaverTime.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehScreenSaverTime.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehScreenSaverTime.Properties.Appearance.Options.UseFont = true;
            this.tbcBehScreenSaverTime.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehScreenSaverTime.Properties.Maximum = 15;
            this.tbcBehScreenSaverTime.Size = new System.Drawing.Size(128, 45);
            this.tbcBehScreenSaverTime.TabIndex = 162;
            this.tbcBehScreenSaverTime.Value = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(20, 104);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(203, 43);
            this.labelControl1.TabIndex = 161;
            this.labelControl1.Text = "Apagar Pantalla Tras (min)";
            // 
            // ckBehLogToGpsPositions
            // 
            this.ckBehLogToGpsPositions.Location = new System.Drawing.Point(20, 455);
            this.ckBehLogToGpsPositions.Name = "ckBehLogToGpsPositions";
            this.ckBehLogToGpsPositions.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBehLogToGpsPositions.Properties.Appearance.Options.UseFont = true;
            this.ckBehLogToGpsPositions.Properties.Caption = "GPS: Registrar en Log";
            this.ckBehLogToGpsPositions.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ckBehLogToGpsPositions.Size = new System.Drawing.Size(233, 20);
            this.ckBehLogToGpsPositions.TabIndex = 163;
            // 
            // myTrackBarControl1
            // 
            this.myTrackBarControl1.EditValue = 5;
            this.myTrackBarControl1.Location = new System.Drawing.Point(602, 430);
            this.myTrackBarControl1.Name = "myTrackBarControl1";
            this.myTrackBarControl1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myTrackBarControl1.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.myTrackBarControl1.Properties.Appearance.Options.UseFont = true;
            this.myTrackBarControl1.Properties.Appearance.Options.UseForeColor = true;
            this.myTrackBarControl1.Properties.Minimum = 4;
            this.myTrackBarControl1.Properties.TickFrequency = 30;
            this.myTrackBarControl1.Size = new System.Drawing.Size(128, 45);
            this.myTrackBarControl1.TabIndex = 165;
            this.myTrackBarControl1.Value = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.labelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(393, 442);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(206, 33);
            this.labelControl2.TabIndex = 164;
            this.labelControl2.Text = "Salud Sistema: Maximo dias sin apagar";
            // 
            // tbcBehFrecuencyBuildStateFrameModeMotionTime
            // 
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.EditValue = 1;
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Location = new System.Drawing.Point(602, 71);
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Name = "tbcBehFrecuencyBuildStateFrameModeMotionTime";
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Properties.Appearance.Options.UseFont = true;
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Properties.Maximum = 3;
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Properties.Minimum = 1;
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Properties.TickFrequency = 3;
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Size = new System.Drawing.Size(128, 45);
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.TabIndex = 166;
            this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Value = 1;
            // 
            // tbcBehDistanceMaxBeetweenConsecutivePoints
            // 
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.EditValue = 8;
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Location = new System.Drawing.Point(232, 400);
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Name = "tbcBehDistanceMaxBeetweenConsecutivePoints";
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Properties.Appearance.Options.UseFont = true;
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Properties.Maximum = 30;
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Properties.Minimum = 5;
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Properties.TickFrequency = 5;
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Size = new System.Drawing.Size(128, 45);
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.TabIndex = 168;
            this.tbcBehDistanceMaxBeetweenConsecutivePoints.Value = 8;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(20, 400);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(203, 43);
            this.labelControl3.TabIndex = 167;
            this.labelControl3.Text = "GPS: Maxima distancia entre 2 puntos consecutivos (Km)";
            // 
            // tbcBehEndpointTimeout
            // 
            this.tbcBehEndpointTimeout.EditValue = 8;
            this.tbcBehEndpointTimeout.Location = new System.Drawing.Point(232, 55);
            this.tbcBehEndpointTimeout.Name = "tbcBehEndpointTimeout";
            this.tbcBehEndpointTimeout.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehEndpointTimeout.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehEndpointTimeout.Properties.Appearance.Options.UseFont = true;
            this.tbcBehEndpointTimeout.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehEndpointTimeout.Properties.Maximum = 15;
            this.tbcBehEndpointTimeout.Properties.Minimum = 5;
            this.tbcBehEndpointTimeout.Size = new System.Drawing.Size(128, 45);
            this.tbcBehEndpointTimeout.TabIndex = 132;
            this.tbcBehEndpointTimeout.Value = 8;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(23, 57);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(203, 43);
            this.labelControl4.TabIndex = 131;
            this.labelControl4.Text = "Tiempo Espera intento de Conexión (seg.)";
            // 
            // tbcBehRetryNumberByEndpoint
            // 
            this.tbcBehRetryNumberByEndpoint.EditValue = 3;
            this.tbcBehRetryNumberByEndpoint.Location = new System.Drawing.Point(232, 106);
            this.tbcBehRetryNumberByEndpoint.Name = "tbcBehRetryNumberByEndpoint";
            this.tbcBehRetryNumberByEndpoint.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcBehRetryNumberByEndpoint.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcBehRetryNumberByEndpoint.Properties.Appearance.Options.UseFont = true;
            this.tbcBehRetryNumberByEndpoint.Properties.Appearance.Options.UseForeColor = true;
            this.tbcBehRetryNumberByEndpoint.Properties.Maximum = 5;
            this.tbcBehRetryNumberByEndpoint.Properties.Minimum = 1;
            this.tbcBehRetryNumberByEndpoint.Size = new System.Drawing.Size(128, 45);
            this.tbcBehRetryNumberByEndpoint.TabIndex = 134;
            this.tbcBehRetryNumberByEndpoint.Value = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(23, 106);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(203, 43);
            this.labelControl5.TabIndex = 133;
            this.labelControl5.Text = "Nº de Intentos por Servidor:";
            // 
            // TerminalConfigurationUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tbAdmin);
            this.Name = "TerminalConfigurationUC";
            this.Size = new System.Drawing.Size(1234, 587);
            ((System.ComponentModel.ISupportInitialize)(this.tbAdmin)).EndInit();
            this.tbAdmin.ResumeLayout(false);
            this.tbpAdminTerm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTerminal)).EndInit();
            this.gcTerminal.ResumeLayout(false);
            this.gcTerminal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehNoConnectionInternetRetryWithoutRestarSystem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehNoConnectionInternetRetryWithoutRestarSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbBehNoGpsReceptionMaxTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbBehNoGpsReceptionMaxTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehMaxFramesByPackage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehMaxFramesByPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbBehNoConnectionEndpointMaxTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbBehNoConnectionEndpointMaxTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbcBehNoConnectionInternetMaxTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbcBehNoConnectionInternetMaxTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyShippingTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyShippingTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyBuildStateFrameModeStopTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyBuildStateFrameModeStopTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehThresholdStopModeSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehThresholdStopModeSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehThresholdStopModeTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehThresholdStopModeTime)).EndInit();
            this.tbpEndpoints.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcConexion)).EndInit();
            this.gcConexion.ResumeLayout(false);
            this.gcConexion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbConexionDefaultIP.Properties)).EndInit();
            this.tbpAdminFtpTrace.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).EndInit();
            this.gcTrace.ResumeLayout(false);
            this.gcTrace.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceProtocol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTracePFileRotate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileMaxSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFtp)).EndInit();
            this.gcFtp.ResumeLayout(false);
            this.gcFtp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdatePwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdateRemoteHost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdatePathRemote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPathRemote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFtpModoEnvioLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpRemoteHost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehScreenSaverTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehScreenSaverTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckBehLogToGpsPositions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myTrackBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myTrackBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyBuildStateFrameModeMotionTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehFrecuencyBuildStateFrameModeMotionTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehDistanceMaxBeetweenConsecutivePoints.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehDistanceMaxBeetweenConsecutivePoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehEndpointTimeout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehEndpointTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehRetryNumberByEndpoint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcBehRetryNumberByEndpoint)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tbAdmin;
        private DevExpress.XtraTab.XtraTabPage tbpAdminTerm;
        private DevExpress.XtraEditors.PanelControl gcTerminal;
        private Controls.MyTrackBarControl tbcBehNoConnectionInternetRetryWithoutRestarSystem;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private Controls.MyTrackBarControl tcbBehNoGpsReceptionMaxTime;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private Controls.MyTrackBarControl tbcBehMaxFramesByPackage;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private Controls.MyTrackBarControl tcbBehNoConnectionEndpointMaxTime;
        private Controls.MyTrackBarControl TbcBehNoConnectionInternetMaxTime;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private Controls.MyTrackBarControl tbcBehFrecuencyShippingTime;
        private Controls.MyTrackBarControl tbcTerminalMinWaitRsp;
        private Controls.MyTrackBarControl tbcTerminalMaxWaitRsp;
        private Controls.MyTrackBarControl tbcBehFrecuencyBuildStateFrameModeStopTime;
        private Controls.MyTrackBarControl tbcBehThresholdStopModeSpeed;
        private Controls.MyTrackBarControl tbcBehThresholdStopModeTime;
        private DevExpress.XtraEditors.LabelControl lbTerminalIntervalSendCola;
        private DevExpress.XtraEditors.LabelControl lbTerminalMinWaitRsp;
        private DevExpress.XtraEditors.LabelControl lbTerminalMaxWaitRsp;
        private DevExpress.XtraEditors.LabelControl lbTerminalTxDelayOn;
        private DevExpress.XtraEditors.LabelControl lbTerminalTxDelayOff;
        private DevExpress.XtraEditors.LabelControl lbTerminal;
        private DevExpress.XtraEditors.LabelControl lbTerminalUmbralTempParada;
        private DevExpress.XtraTab.XtraTabPage tbpEndpoints;
        private DevExpress.XtraEditors.PanelControl gcConexion;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort3;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP3;
        private DevExpress.XtraEditors.TextEdit teConexionServerName3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName3;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort2;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP2;
        private DevExpress.XtraEditors.TextEdit teConexionServerName2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName2;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort1;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP1;
        private DevExpress.XtraEditors.TextEdit teConexionServerName1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName1;
        private DevExpress.XtraEditors.ComboBoxEdit cbConexionDefaultIP;
        private DevExpress.XtraEditors.LabelControl lbConexionDefaultPositioIP;
        private DevExpress.XtraTab.XtraTabPage tbpAdminFtpTrace;
        private DevExpress.XtraEditors.GroupControl gcTrace;
        private DevExpress.XtraEditors.ComboBoxEdit cbTraceProtocol;
        private DevExpress.XtraEditors.LabelControl lbTraceProtocol;
        private DevExpress.XtraEditors.ComboBoxEdit cbTracePFileRotate;
        private DevExpress.XtraEditors.LabelControl lbTracePFileRotate;
        private DevExpress.XtraEditors.TextEdit teTracePFileName;
        private DevExpress.XtraEditors.LabelControl lbTracePFileName;
        private DevExpress.XtraEditors.TextEdit teTracePFileMaxSize;
        private DevExpress.XtraEditors.LabelControl lbTracePFileMaxSize;
        private DevExpress.XtraEditors.ComboBoxEdit cbTraceLevel;
        private DevExpress.XtraEditors.LabelControl lbTraceLevel;
        private DevExpress.XtraEditors.GroupControl gcFtp;
        private DevExpress.XtraEditors.TextEdit teUpdatePwd;
        private DevExpress.XtraEditors.TextEdit teUpdateUser;
        private DevExpress.XtraEditors.TextEdit teUpdateRemoteHost;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit teUpdatePathRemote;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit teFtpPathRemote;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit teFtpPwd;
        private DevExpress.XtraEditors.TextEdit teFtpUser;
        private DevExpress.XtraEditors.ComboBoxEdit cbFtpModoEnvioLog;
        private DevExpress.XtraEditors.LabelControl lbFtpModoEnvio;
        private DevExpress.XtraEditors.LabelControl lbFtpLastDate;
        private DevExpress.XtraEditors.DateEdit deFtpLastDateLog;
        private DevExpress.XtraEditors.TextEdit teFtpRemoteHost;
        private DevExpress.XtraEditors.LabelControl lbFtpRemoteHost;
        private DevExpress.XtraEditors.TextEdit teFtpPort;
        private DevExpress.XtraEditors.LabelControl lbFtpPort;
        private Controls.MyTrackBarControl tbcBehScreenSaverTime;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit ckBehLogToGpsPositions;
        private Controls.MyTrackBarControl myTrackBarControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private Controls.MyTrackBarControl tbcBehFrecuencyBuildStateFrameModeMotionTime;
        private Controls.MyTrackBarControl tbcBehDistanceMaxBeetweenConsecutivePoints;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private Controls.MyTrackBarControl tbcBehEndpointTimeout;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private Controls.MyTrackBarControl tbcBehRetryNumberByEndpoint;
        private DevExpress.XtraEditors.LabelControl labelControl5;
    }
}
