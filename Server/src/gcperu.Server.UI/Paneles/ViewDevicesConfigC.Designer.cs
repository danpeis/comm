﻿namespace GComunica.ExeServer.Paneles
{
    partial class ViewDevicesConfigC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcModem = new DevExpress.XtraEditors.GroupControl();
            this.teModemNumber = new DevExpress.XtraEditors.TextEdit();
            this.lbModemNumber = new DevExpress.XtraEditors.LabelControl();
            this.teModemPassword = new DevExpress.XtraEditors.TextEdit();
            this.lbModemPassword = new DevExpress.XtraEditors.LabelControl();
            this.teModemUserName = new DevExpress.XtraEditors.TextEdit();
            this.lbModemUserName = new DevExpress.XtraEditors.LabelControl();
            this.teModemName = new DevExpress.XtraEditors.TextEdit();
            this.lbModemName = new DevExpress.XtraEditors.LabelControl();
            this.teModemDomain = new DevExpress.XtraEditors.TextEdit();
            this.lbModemDomain = new DevExpress.XtraEditors.LabelControl();
            this.ceModemCheckPin = new DevExpress.XtraEditors.CheckEdit();
            this.cbModemFlowControl = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbModemFowControl = new DevExpress.XtraEditors.LabelControl();
            this.cbModemStopBits = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbModemStopBits = new DevExpress.XtraEditors.LabelControl();
            this.lbModemParity = new DevExpress.XtraEditors.LabelControl();
            this.cbModemParity = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbModemDataBits = new DevExpress.XtraEditors.LabelControl();
            this.cbModemDataBits = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbModemBaudeRate = new DevExpress.XtraEditors.LabelControl();
            this.cbModemBaudeRate = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbModemPort = new DevExpress.XtraEditors.LabelControl();
            this.cbModemPort = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gcGPS = new DevExpress.XtraEditors.GroupControl();
            this.cbGpsFlowControl = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbGpsFlowControl = new DevExpress.XtraEditors.LabelControl();
            this.cbGpsStopBits = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbGpsStopBits = new DevExpress.XtraEditors.LabelControl();
            this.lbGpsParity = new DevExpress.XtraEditors.LabelControl();
            this.cbGpsParity = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbGpsDataBits = new DevExpress.XtraEditors.LabelControl();
            this.cbGpsDataBits = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbGpsBaudRate = new DevExpress.XtraEditors.LabelControl();
            this.cbGpsBaudRate = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbGpsPort = new DevExpress.XtraEditors.LabelControl();
            this.cbGpsPort = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcModem)).BeginInit();
            this.gcModem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teModemNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teModemPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teModemUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teModemName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teModemDomain.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceModemCheckPin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemFlowControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemStopBits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemParity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemDataBits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemBaudeRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGPS)).BeginInit();
            this.gcGPS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsFlowControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsStopBits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsParity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsDataBits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsBaudRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsPort.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcModem
            // 
            this.gcModem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcModem.Appearance.Options.UseFont = true;
            this.gcModem.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcModem.AppearanceCaption.Options.UseFont = true;
            this.gcModem.Controls.Add(this.teModemNumber);
            this.gcModem.Controls.Add(this.lbModemNumber);
            this.gcModem.Controls.Add(this.teModemPassword);
            this.gcModem.Controls.Add(this.lbModemPassword);
            this.gcModem.Controls.Add(this.teModemUserName);
            this.gcModem.Controls.Add(this.lbModemUserName);
            this.gcModem.Controls.Add(this.teModemName);
            this.gcModem.Controls.Add(this.lbModemName);
            this.gcModem.Controls.Add(this.teModemDomain);
            this.gcModem.Controls.Add(this.lbModemDomain);
            this.gcModem.Controls.Add(this.ceModemCheckPin);
            this.gcModem.Controls.Add(this.cbModemFlowControl);
            this.gcModem.Controls.Add(this.lbModemFowControl);
            this.gcModem.Controls.Add(this.cbModemStopBits);
            this.gcModem.Controls.Add(this.lbModemStopBits);
            this.gcModem.Controls.Add(this.lbModemParity);
            this.gcModem.Controls.Add(this.cbModemParity);
            this.gcModem.Controls.Add(this.lbModemDataBits);
            this.gcModem.Controls.Add(this.cbModemDataBits);
            this.gcModem.Controls.Add(this.lbModemBaudeRate);
            this.gcModem.Controls.Add(this.cbModemBaudeRate);
            this.gcModem.Controls.Add(this.lbModemPort);
            this.gcModem.Controls.Add(this.cbModemPort);
            this.gcModem.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcModem.Enabled = false;
            this.gcModem.Location = new System.Drawing.Point(0, 0);
            this.gcModem.Name = "gcModem";
            this.gcModem.Size = new System.Drawing.Size(775, 217);
            this.gcModem.TabIndex = 9;
            this.gcModem.Text = "Modem";
            // 
            // teModemNumber
            // 
            this.teModemNumber.AllowDrop = true;
            this.teModemNumber.EditValue = "*99#";
            this.teModemNumber.Location = new System.Drawing.Point(475, 185);
            this.teModemNumber.Name = "teModemNumber";
            this.teModemNumber.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teModemNumber.Properties.Appearance.Options.UseFont = true;
            this.teModemNumber.Size = new System.Drawing.Size(209, 26);
            this.teModemNumber.TabIndex = 36;
            // 
            // lbModemNumber
            // 
            this.lbModemNumber.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemNumber.Appearance.Options.UseFont = true;
            this.lbModemNumber.Location = new System.Drawing.Point(289, 190);
            this.lbModemNumber.Name = "lbModemNumber";
            this.lbModemNumber.Size = new System.Drawing.Size(63, 18);
            this.lbModemNumber.TabIndex = 35;
            this.lbModemNumber.Text = "Número";
            // 
            // teModemPassword
            // 
            this.teModemPassword.EditValue = "MOVISTAR";
            this.teModemPassword.Location = new System.Drawing.Point(475, 153);
            this.teModemPassword.Name = "teModemPassword";
            this.teModemPassword.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teModemPassword.Properties.Appearance.Options.UseFont = true;
            this.teModemPassword.Size = new System.Drawing.Size(209, 26);
            this.teModemPassword.TabIndex = 34;
            // 
            // lbModemPassword
            // 
            this.lbModemPassword.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemPassword.Appearance.Options.UseFont = true;
            this.lbModemPassword.Location = new System.Drawing.Point(289, 158);
            this.lbModemPassword.Name = "lbModemPassword";
            this.lbModemPassword.Size = new System.Drawing.Size(45, 18);
            this.lbModemPassword.TabIndex = 33;
            this.lbModemPassword.Text = "Clave";
            // 
            // teModemUserName
            // 
            this.teModemUserName.EditValue = "MOVISTAR";
            this.teModemUserName.Location = new System.Drawing.Point(475, 119);
            this.teModemUserName.Name = "teModemUserName";
            this.teModemUserName.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teModemUserName.Properties.Appearance.Options.UseFont = true;
            this.teModemUserName.Size = new System.Drawing.Size(209, 26);
            this.teModemUserName.TabIndex = 32;
            // 
            // lbModemUserName
            // 
            this.lbModemUserName.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemUserName.Appearance.Options.UseFont = true;
            this.lbModemUserName.Location = new System.Drawing.Point(289, 124);
            this.lbModemUserName.Name = "lbModemUserName";
            this.lbModemUserName.Size = new System.Drawing.Size(62, 18);
            this.lbModemUserName.TabIndex = 31;
            this.lbModemUserName.Text = "Usuario";
            // 
            // teModemName
            // 
            this.teModemName.EditValue = "MOVISTAR";
            this.teModemName.Location = new System.Drawing.Point(475, 89);
            this.teModemName.Name = "teModemName";
            this.teModemName.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teModemName.Properties.Appearance.Options.UseFont = true;
            this.teModemName.Size = new System.Drawing.Size(209, 26);
            this.teModemName.TabIndex = 30;
            // 
            // lbModemName
            // 
            this.lbModemName.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemName.Appearance.Options.UseFont = true;
            this.lbModemName.Location = new System.Drawing.Point(289, 92);
            this.lbModemName.Name = "lbModemName";
            this.lbModemName.Size = new System.Drawing.Size(130, 18);
            this.lbModemName.TabIndex = 29;
            this.lbModemName.Text = "Nombre Dial-Up";
            // 
            // teModemDomain
            // 
            this.teModemDomain.Location = new System.Drawing.Point(475, 55);
            this.teModemDomain.Name = "teModemDomain";
            this.teModemDomain.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teModemDomain.Properties.Appearance.Options.UseFont = true;
            this.teModemDomain.Size = new System.Drawing.Size(209, 26);
            this.teModemDomain.TabIndex = 28;
            // 
            // lbModemDomain
            // 
            this.lbModemDomain.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemDomain.Appearance.Options.UseFont = true;
            this.lbModemDomain.Location = new System.Drawing.Point(289, 62);
            this.lbModemDomain.Name = "lbModemDomain";
            this.lbModemDomain.Size = new System.Drawing.Size(67, 18);
            this.lbModemDomain.TabIndex = 27;
            this.lbModemDomain.Text = "Dominio";
            // 
            // ceModemCheckPin
            // 
            this.ceModemCheckPin.Location = new System.Drawing.Point(287, 24);
            this.ceModemCheckPin.Name = "ceModemCheckPin";
            this.ceModemCheckPin.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ceModemCheckPin.Properties.Appearance.Options.UseFont = true;
            this.ceModemCheckPin.Properties.Caption = "Activar chequeo de pin";
            this.ceModemCheckPin.Size = new System.Drawing.Size(295, 23);
            this.ceModemCheckPin.TabIndex = 26;
            // 
            // cbModemFlowControl
            // 
            this.cbModemFlowControl.EditValue = "Ninguno";
            this.cbModemFlowControl.Location = new System.Drawing.Point(180, 185);
            this.cbModemFlowControl.Name = "cbModemFlowControl";
            this.cbModemFlowControl.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModemFlowControl.Properties.Appearance.Options.UseFont = true;
            this.cbModemFlowControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbModemFlowControl.Properties.Items.AddRange(new object[] {
            "None",
            "RTS_CTS",
            "Xon_Xoff"});
            this.cbModemFlowControl.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbModemFlowControl.Size = new System.Drawing.Size(100, 26);
            this.cbModemFlowControl.TabIndex = 23;
            // 
            // lbModemFowControl
            // 
            this.lbModemFowControl.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemFowControl.Appearance.Options.UseFont = true;
            this.lbModemFowControl.Location = new System.Drawing.Point(16, 189);
            this.lbModemFowControl.Name = "lbModemFowControl";
            this.lbModemFowControl.Size = new System.Drawing.Size(128, 18);
            this.lbModemFowControl.TabIndex = 22;
            this.lbModemFowControl.Text = "Control de flujo";
            // 
            // cbModemStopBits
            // 
            this.cbModemStopBits.EditValue = "1";
            this.cbModemStopBits.Location = new System.Drawing.Point(181, 153);
            this.cbModemStopBits.Name = "cbModemStopBits";
            this.cbModemStopBits.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModemStopBits.Properties.Appearance.Options.UseFont = true;
            this.cbModemStopBits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbModemStopBits.Properties.Items.AddRange(new object[] {
            "1",
            "1,5",
            "2"});
            this.cbModemStopBits.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbModemStopBits.Size = new System.Drawing.Size(100, 26);
            this.cbModemStopBits.TabIndex = 21;
            // 
            // lbModemStopBits
            // 
            this.lbModemStopBits.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemStopBits.Appearance.Options.UseFont = true;
            this.lbModemStopBits.Location = new System.Drawing.Point(17, 157);
            this.lbModemStopBits.Name = "lbModemStopBits";
            this.lbModemStopBits.Size = new System.Drawing.Size(120, 18);
            this.lbModemStopBits.TabIndex = 20;
            this.lbModemStopBits.Text = "Bits de parada";
            // 
            // lbModemParity
            // 
            this.lbModemParity.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemParity.Appearance.Options.UseFont = true;
            this.lbModemParity.Location = new System.Drawing.Point(17, 125);
            this.lbModemParity.Name = "lbModemParity";
            this.lbModemParity.Size = new System.Drawing.Size(61, 18);
            this.lbModemParity.TabIndex = 19;
            this.lbModemParity.Text = "Paridad";
            // 
            // cbModemParity
            // 
            this.cbModemParity.EditValue = "Ninguno";
            this.cbModemParity.Location = new System.Drawing.Point(181, 121);
            this.cbModemParity.Name = "cbModemParity";
            this.cbModemParity.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModemParity.Properties.Appearance.Options.UseFont = true;
            this.cbModemParity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbModemParity.Properties.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even",
            "Mark",
            "Space"});
            this.cbModemParity.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbModemParity.Size = new System.Drawing.Size(100, 26);
            this.cbModemParity.TabIndex = 18;
            // 
            // lbModemDataBits
            // 
            this.lbModemDataBits.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemDataBits.Appearance.Options.UseFont = true;
            this.lbModemDataBits.Location = new System.Drawing.Point(17, 93);
            this.lbModemDataBits.Name = "lbModemDataBits";
            this.lbModemDataBits.Size = new System.Drawing.Size(110, 18);
            this.lbModemDataBits.TabIndex = 17;
            this.lbModemDataBits.Text = "Bits de datos";
            // 
            // cbModemDataBits
            // 
            this.cbModemDataBits.EditValue = "8";
            this.cbModemDataBits.Location = new System.Drawing.Point(181, 89);
            this.cbModemDataBits.Name = "cbModemDataBits";
            this.cbModemDataBits.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModemDataBits.Properties.Appearance.Options.UseFont = true;
            this.cbModemDataBits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbModemDataBits.Properties.Items.AddRange(new object[] {
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cbModemDataBits.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbModemDataBits.Size = new System.Drawing.Size(100, 26);
            this.cbModemDataBits.TabIndex = 16;
            // 
            // lbModemBaudeRate
            // 
            this.lbModemBaudeRate.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemBaudeRate.Appearance.Options.UseFont = true;
            this.lbModemBaudeRate.Location = new System.Drawing.Point(17, 61);
            this.lbModemBaudeRate.Name = "lbModemBaudeRate";
            this.lbModemBaudeRate.Size = new System.Drawing.Size(139, 18);
            this.lbModemBaudeRate.TabIndex = 15;
            this.lbModemBaudeRate.Text = "Bits por segundo";
            // 
            // cbModemBaudeRate
            // 
            this.cbModemBaudeRate.EditValue = "460800";
            this.cbModemBaudeRate.Location = new System.Drawing.Point(181, 57);
            this.cbModemBaudeRate.Name = "cbModemBaudeRate";
            this.cbModemBaudeRate.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModemBaudeRate.Properties.Appearance.Options.UseFont = true;
            this.cbModemBaudeRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbModemBaudeRate.Properties.Items.AddRange(new object[] {
            "4800",
            "9600",
            "57600",
            "115200",
            "230400",
            "460800",
            "921600"});
            this.cbModemBaudeRate.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbModemBaudeRate.Size = new System.Drawing.Size(100, 26);
            this.cbModemBaudeRate.TabIndex = 14;
            // 
            // lbModemPort
            // 
            this.lbModemPort.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemPort.Appearance.Options.UseFont = true;
            this.lbModemPort.Location = new System.Drawing.Point(17, 29);
            this.lbModemPort.Name = "lbModemPort";
            this.lbModemPort.Size = new System.Drawing.Size(53, 18);
            this.lbModemPort.TabIndex = 13;
            this.lbModemPort.Text = "Puerto";
            // 
            // cbModemPort
            // 
            this.cbModemPort.EditValue = "COM10";
            this.cbModemPort.Location = new System.Drawing.Point(181, 25);
            this.cbModemPort.Name = "cbModemPort";
            this.cbModemPort.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModemPort.Properties.Appearance.Options.UseFont = true;
            this.cbModemPort.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbModemPort.Properties.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10"});
            this.cbModemPort.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbModemPort.Size = new System.Drawing.Size(100, 26);
            this.cbModemPort.TabIndex = 12;
            // 
            // gcGPS
            // 
            this.gcGPS.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcGPS.Appearance.Options.UseFont = true;
            this.gcGPS.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcGPS.AppearanceCaption.Options.UseFont = true;
            this.gcGPS.Controls.Add(this.cbGpsFlowControl);
            this.gcGPS.Controls.Add(this.lbGpsFlowControl);
            this.gcGPS.Controls.Add(this.cbGpsStopBits);
            this.gcGPS.Controls.Add(this.lbGpsStopBits);
            this.gcGPS.Controls.Add(this.lbGpsParity);
            this.gcGPS.Controls.Add(this.cbGpsParity);
            this.gcGPS.Controls.Add(this.lbGpsDataBits);
            this.gcGPS.Controls.Add(this.cbGpsDataBits);
            this.gcGPS.Controls.Add(this.lbGpsBaudRate);
            this.gcGPS.Controls.Add(this.cbGpsBaudRate);
            this.gcGPS.Controls.Add(this.lbGpsPort);
            this.gcGPS.Controls.Add(this.cbGpsPort);
            this.gcGPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcGPS.Enabled = false;
            this.gcGPS.Location = new System.Drawing.Point(0, 217);
            this.gcGPS.Name = "gcGPS";
            this.gcGPS.Size = new System.Drawing.Size(775, 161);
            this.gcGPS.TabIndex = 11;
            this.gcGPS.Text = "GPS";
            // 
            // cbGpsFlowControl
            // 
            this.cbGpsFlowControl.EditValue = "Ninguno";
            this.cbGpsFlowControl.Location = new System.Drawing.Point(482, 96);
            this.cbGpsFlowControl.Name = "cbGpsFlowControl";
            this.cbGpsFlowControl.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGpsFlowControl.Properties.Appearance.Options.UseFont = true;
            this.cbGpsFlowControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGpsFlowControl.Properties.Items.AddRange(new object[] {
            "None",
            "Xon/Xoff",
            "Hardware"});
            this.cbGpsFlowControl.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGpsFlowControl.Size = new System.Drawing.Size(202, 26);
            this.cbGpsFlowControl.TabIndex = 11;
            // 
            // lbGpsFlowControl
            // 
            this.lbGpsFlowControl.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGpsFlowControl.Appearance.Options.UseFont = true;
            this.lbGpsFlowControl.Location = new System.Drawing.Point(289, 101);
            this.lbGpsFlowControl.Name = "lbGpsFlowControl";
            this.lbGpsFlowControl.Size = new System.Drawing.Size(128, 18);
            this.lbGpsFlowControl.TabIndex = 10;
            this.lbGpsFlowControl.Text = "Control de flujo";
            // 
            // cbGpsStopBits
            // 
            this.cbGpsStopBits.EditValue = "1";
            this.cbGpsStopBits.Location = new System.Drawing.Point(482, 65);
            this.cbGpsStopBits.Name = "cbGpsStopBits";
            this.cbGpsStopBits.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGpsStopBits.Properties.Appearance.Options.UseFont = true;
            this.cbGpsStopBits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGpsStopBits.Properties.Items.AddRange(new object[] {
            "1",
            "1,5",
            "2"});
            this.cbGpsStopBits.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGpsStopBits.Size = new System.Drawing.Size(202, 26);
            this.cbGpsStopBits.TabIndex = 9;
            // 
            // lbGpsStopBits
            // 
            this.lbGpsStopBits.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGpsStopBits.Appearance.Options.UseFont = true;
            this.lbGpsStopBits.Location = new System.Drawing.Point(291, 70);
            this.lbGpsStopBits.Name = "lbGpsStopBits";
            this.lbGpsStopBits.Size = new System.Drawing.Size(120, 18);
            this.lbGpsStopBits.TabIndex = 8;
            this.lbGpsStopBits.Text = "Bits de parada";
            // 
            // lbGpsParity
            // 
            this.lbGpsParity.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGpsParity.Appearance.Options.UseFont = true;
            this.lbGpsParity.Location = new System.Drawing.Point(291, 38);
            this.lbGpsParity.Name = "lbGpsParity";
            this.lbGpsParity.Size = new System.Drawing.Size(61, 18);
            this.lbGpsParity.TabIndex = 7;
            this.lbGpsParity.Text = "Paridad";
            // 
            // cbGpsParity
            // 
            this.cbGpsParity.EditValue = "Ninguno";
            this.cbGpsParity.Location = new System.Drawing.Point(482, 33);
            this.cbGpsParity.Name = "cbGpsParity";
            this.cbGpsParity.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGpsParity.Properties.Appearance.Options.UseFont = true;
            this.cbGpsParity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGpsParity.Properties.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even",
            "Mark",
            "Space"});
            this.cbGpsParity.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGpsParity.Size = new System.Drawing.Size(202, 26);
            this.cbGpsParity.TabIndex = 6;
            // 
            // lbGpsDataBits
            // 
            this.lbGpsDataBits.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGpsDataBits.Appearance.Options.UseFont = true;
            this.lbGpsDataBits.Location = new System.Drawing.Point(8, 107);
            this.lbGpsDataBits.Name = "lbGpsDataBits";
            this.lbGpsDataBits.Size = new System.Drawing.Size(110, 18);
            this.lbGpsDataBits.TabIndex = 5;
            this.lbGpsDataBits.Text = "Bits de datos";
            // 
            // cbGpsDataBits
            // 
            this.cbGpsDataBits.EditValue = "8";
            this.cbGpsDataBits.Location = new System.Drawing.Point(181, 97);
            this.cbGpsDataBits.Name = "cbGpsDataBits";
            this.cbGpsDataBits.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGpsDataBits.Properties.Appearance.Options.UseFont = true;
            this.cbGpsDataBits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGpsDataBits.Properties.Items.AddRange(new object[] {
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cbGpsDataBits.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGpsDataBits.Size = new System.Drawing.Size(100, 26);
            this.cbGpsDataBits.TabIndex = 4;
            // 
            // lbGpsBaudRate
            // 
            this.lbGpsBaudRate.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGpsBaudRate.Appearance.Options.UseFont = true;
            this.lbGpsBaudRate.Location = new System.Drawing.Point(8, 75);
            this.lbGpsBaudRate.Name = "lbGpsBaudRate";
            this.lbGpsBaudRate.Size = new System.Drawing.Size(139, 18);
            this.lbGpsBaudRate.TabIndex = 3;
            this.lbGpsBaudRate.Text = "Bits por segundo";
            // 
            // cbGpsBaudRate
            // 
            this.cbGpsBaudRate.EditValue = "4800";
            this.cbGpsBaudRate.Location = new System.Drawing.Point(181, 65);
            this.cbGpsBaudRate.Name = "cbGpsBaudRate";
            this.cbGpsBaudRate.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGpsBaudRate.Properties.Appearance.Options.UseFont = true;
            this.cbGpsBaudRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGpsBaudRate.Properties.Items.AddRange(new object[] {
            "4800",
            "9600"});
            this.cbGpsBaudRate.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGpsBaudRate.Size = new System.Drawing.Size(100, 26);
            this.cbGpsBaudRate.TabIndex = 2;
            // 
            // lbGpsPort
            // 
            this.lbGpsPort.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGpsPort.Appearance.Options.UseFont = true;
            this.lbGpsPort.Location = new System.Drawing.Point(7, 43);
            this.lbGpsPort.Name = "lbGpsPort";
            this.lbGpsPort.Size = new System.Drawing.Size(53, 18);
            this.lbGpsPort.TabIndex = 1;
            this.lbGpsPort.Text = "Puerto";
            // 
            // cbGpsPort
            // 
            this.cbGpsPort.EditValue = "COM2";
            this.cbGpsPort.Location = new System.Drawing.Point(180, 33);
            this.cbGpsPort.Name = "cbGpsPort";
            this.cbGpsPort.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGpsPort.Properties.Appearance.Options.UseFont = true;
            this.cbGpsPort.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGpsPort.Properties.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10"});
            this.cbGpsPort.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGpsPort.Size = new System.Drawing.Size(100, 26);
            this.cbGpsPort.TabIndex = 0;
            // 
            // ViewDevicesConfigC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcGPS);
            this.Controls.Add(this.gcModem);
            this.Name = "ViewDevicesConfigC";
            this.Size = new System.Drawing.Size(775, 436);
            this.Controls.SetChildIndex(this.gcModem, 0);
            this.Controls.SetChildIndex(this.gcGPS, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gcModem)).EndInit();
            this.gcModem.ResumeLayout(false);
            this.gcModem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teModemNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teModemPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teModemUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teModemName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teModemDomain.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceModemCheckPin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemFlowControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemStopBits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemParity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemDataBits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemBaudeRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModemPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGPS)).EndInit();
            this.gcGPS.ResumeLayout(false);
            this.gcGPS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsFlowControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsStopBits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsParity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsDataBits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsBaudRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGpsPort.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl gcModem;
        private DevExpress.XtraEditors.TextEdit teModemNumber;
        private DevExpress.XtraEditors.LabelControl lbModemNumber;
        private DevExpress.XtraEditors.TextEdit teModemPassword;
        private DevExpress.XtraEditors.LabelControl lbModemPassword;
        private DevExpress.XtraEditors.TextEdit teModemUserName;
        private DevExpress.XtraEditors.LabelControl lbModemUserName;
        private DevExpress.XtraEditors.TextEdit teModemName;
        private DevExpress.XtraEditors.LabelControl lbModemName;
        private DevExpress.XtraEditors.TextEdit teModemDomain;
        private DevExpress.XtraEditors.LabelControl lbModemDomain;
        private DevExpress.XtraEditors.CheckEdit ceModemCheckPin;
        private DevExpress.XtraEditors.ComboBoxEdit cbModemFlowControl;
        private DevExpress.XtraEditors.LabelControl lbModemFowControl;
        private DevExpress.XtraEditors.ComboBoxEdit cbModemStopBits;
        private DevExpress.XtraEditors.LabelControl lbModemStopBits;
        private DevExpress.XtraEditors.LabelControl lbModemParity;
        private DevExpress.XtraEditors.ComboBoxEdit cbModemParity;
        private DevExpress.XtraEditors.LabelControl lbModemDataBits;
        private DevExpress.XtraEditors.ComboBoxEdit cbModemDataBits;
        private DevExpress.XtraEditors.LabelControl lbModemBaudeRate;
        private DevExpress.XtraEditors.ComboBoxEdit cbModemBaudeRate;
        private DevExpress.XtraEditors.LabelControl lbModemPort;
        private DevExpress.XtraEditors.ComboBoxEdit cbModemPort;
        private DevExpress.XtraEditors.GroupControl gcGPS;
        private DevExpress.XtraEditors.ComboBoxEdit cbGpsFlowControl;
        private DevExpress.XtraEditors.LabelControl lbGpsFlowControl;
        private DevExpress.XtraEditors.ComboBoxEdit cbGpsStopBits;
        private DevExpress.XtraEditors.LabelControl lbGpsStopBits;
        private DevExpress.XtraEditors.LabelControl lbGpsParity;
        private DevExpress.XtraEditors.ComboBoxEdit cbGpsParity;
        private DevExpress.XtraEditors.LabelControl lbGpsDataBits;
        private DevExpress.XtraEditors.ComboBoxEdit cbGpsDataBits;
        private DevExpress.XtraEditors.LabelControl lbGpsBaudRate;
        private DevExpress.XtraEditors.ComboBoxEdit cbGpsBaudRate;
        private DevExpress.XtraEditors.LabelControl lbGpsPort;
        private DevExpress.XtraEditors.ComboBoxEdit cbGpsPort;
    }
}
