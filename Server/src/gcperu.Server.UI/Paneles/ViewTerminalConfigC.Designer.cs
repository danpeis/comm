﻿namespace GComunica.ExeServer.Paneles
{
    partial class ViewTerminalConfigC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbcTerminalUmbralTempParada = new GComunica.ExeServer.Controls.MyTrackBarControl();
            this.tbcTerminalUmbralVelocidadParada = new ExeServer.Controls.MyTrackBarControl();
            this.tbcTerminalSleepLogout = new ExeServer.Controls.MyTrackBarControl();
            this.teTerminalVersion = new DevExpress.XtraEditors.TextEdit();
            this.lbTerminalVersion = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalSleepLogout = new DevExpress.XtraEditors.LabelControl();
            this.teTerminalSN = new DevExpress.XtraEditors.TextEdit();
            this.lbTerminalSN = new DevExpress.XtraEditors.LabelControl();
            this.cbTerminalProtocol = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTerminalProtocol = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminal = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalUmbralTempParada = new DevExpress.XtraEditors.LabelControl();
            this.teTerminalMaxDistancia = new DevExpress.XtraEditors.TextEdit();
            this.lbTerminalMaxDistancia = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalIntervalSendCola = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalMinWaitRsp = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalMaxWaitRsp = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalTxDelayOn = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalTxDelayOff = new DevExpress.XtraEditors.LabelControl();
            this.tbcTerminalMaxWaitRsp = new ExeServer.Controls.MyTrackBarControl();
            this.tbcTerminalTxDelayOn = new ExeServer.Controls.MyTrackBarControl();
            this.tcbTerminalTxDelayOff = new ExeServer.Controls.MyTrackBarControl();
            this.tcbTerminalIntervalSendCola = new ExeServer.Controls.MyTrackBarControl();
            this.tbcTerminalMinWaitRsp = new ExeServer.Controls.MyTrackBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalVersion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTerminalProtocol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalMaxDistancia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalTxDelayOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalTxDelayOn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tbcTerminalUmbralTempParada
            // 
            this.tbcTerminalUmbralTempParada.EditValue = 1;
            this.tbcTerminalUmbralTempParada.Location = new System.Drawing.Point(234, 147);
            this.tbcTerminalUmbralTempParada.Name = "tbcTerminalUmbralTempParada";
            this.tbcTerminalUmbralTempParada.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalUmbralTempParada.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalUmbralTempParada.Size = new System.Drawing.Size(198, 42);
            this.tbcTerminalUmbralTempParada.TabIndex = 1;
            this.tbcTerminalUmbralTempParada.Value = 1;
            // 
            // tbcTerminalUmbralVelocidadParada
            // 
            this.tbcTerminalUmbralVelocidadParada.EditValue = 1;
            this.tbcTerminalUmbralVelocidadParada.Location = new System.Drawing.Point(234, 195);
            this.tbcTerminalUmbralVelocidadParada.Name = "tbcTerminalUmbralVelocidadParada";
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalUmbralVelocidadParada.Size = new System.Drawing.Size(198, 42);
            this.tbcTerminalUmbralVelocidadParada.TabIndex = 2;
            this.tbcTerminalUmbralVelocidadParada.Value = 1;
            // 
            // tbcTerminalSleepLogout
            // 
            this.tbcTerminalSleepLogout.EditValue = 1;
            this.tbcTerminalSleepLogout.Location = new System.Drawing.Point(234, 243);
            this.tbcTerminalSleepLogout.Name = "tbcTerminalSleepLogout";
            this.tbcTerminalSleepLogout.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalSleepLogout.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalSleepLogout.Size = new System.Drawing.Size(198, 42);
            this.tbcTerminalSleepLogout.TabIndex = 3;
            this.tbcTerminalSleepLogout.Value = 1;
            // 
            // teTerminalVersion
            // 
            this.teTerminalVersion.EditValue = "";
            this.teTerminalVersion.Location = new System.Drawing.Point(234, 103);
            this.teTerminalVersion.Name = "teTerminalVersion";
            this.teTerminalVersion.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTerminalVersion.Properties.Appearance.Options.UseFont = true;
            this.teTerminalVersion.Properties.ReadOnly = true;
            this.teTerminalVersion.Size = new System.Drawing.Size(128, 22);
            this.teTerminalVersion.TabIndex = 119;
            // 
            // lbTerminalVersion
            // 
            this.lbTerminalVersion.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalVersion.Appearance.Options.UseFont = true;
            this.lbTerminalVersion.Appearance.Options.UseTextOptions = true;
            this.lbTerminalVersion.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalVersion.Location = new System.Drawing.Point(12, 107);
            this.lbTerminalVersion.Name = "lbTerminalVersion";
            this.lbTerminalVersion.Size = new System.Drawing.Size(61, 18);
            this.lbTerminalVersion.TabIndex = 118;
            this.lbTerminalVersion.Text = "Versión";
            // 
            // lbTerminalSleepLogout
            // 
            this.lbTerminalSleepLogout.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalSleepLogout.Appearance.Options.UseFont = true;
            this.lbTerminalSleepLogout.Appearance.Options.UseTextOptions = true;
            this.lbTerminalSleepLogout.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalSleepLogout.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalSleepLogout.Location = new System.Drawing.Point(12, 250);
            this.lbTerminalSleepLogout.Name = "lbTerminalSleepLogout";
            this.lbTerminalSleepLogout.Size = new System.Drawing.Size(203, 34);
            this.lbTerminalSleepLogout.TabIndex = 117;
            this.lbTerminalSleepLogout.Text = "Tiempo desde el logout hasta suspensión";
            // 
            // teTerminalSN
            // 
            this.teTerminalSN.EditValue = "";
            this.teTerminalSN.Location = new System.Drawing.Point(234, 75);
            this.teTerminalSN.Name = "teTerminalSN";
            this.teTerminalSN.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTerminalSN.Properties.Appearance.Options.UseFont = true;
            this.teTerminalSN.Size = new System.Drawing.Size(128, 22);
            this.teTerminalSN.TabIndex = 116;
            // 
            // lbTerminalSN
            // 
            this.lbTerminalSN.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalSN.Appearance.Options.UseFont = true;
            this.lbTerminalSN.Appearance.Options.UseTextOptions = true;
            this.lbTerminalSN.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalSN.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalSN.Location = new System.Drawing.Point(12, 77);
            this.lbTerminalSN.Name = "lbTerminalSN";
            this.lbTerminalSN.Size = new System.Drawing.Size(203, 16);
            this.lbTerminalSN.TabIndex = 115;
            this.lbTerminalSN.Text = "Número de serie";
            // 
            // cbTerminalProtocol
            // 
            this.cbTerminalProtocol.EditValue = "MDV";
            this.cbTerminalProtocol.Location = new System.Drawing.Point(234, 47);
            this.cbTerminalProtocol.Name = "cbTerminalProtocol";
            this.cbTerminalProtocol.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTerminalProtocol.Properties.Appearance.Options.UseFont = true;
            this.cbTerminalProtocol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTerminalProtocol.Properties.Items.AddRange(new object[] {
            "Indefinido",
            "MDV",
            "MDT"});
            this.cbTerminalProtocol.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTerminalProtocol.Size = new System.Drawing.Size(128, 22);
            this.cbTerminalProtocol.TabIndex = 114;
            // 
            // lbTerminalProtocol
            // 
            this.lbTerminalProtocol.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalProtocol.Appearance.Options.UseFont = true;
            this.lbTerminalProtocol.Appearance.Options.UseTextOptions = true;
            this.lbTerminalProtocol.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalProtocol.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalProtocol.Location = new System.Drawing.Point(12, 49);
            this.lbTerminalProtocol.Name = "lbTerminalProtocol";
            this.lbTerminalProtocol.Size = new System.Drawing.Size(203, 16);
            this.lbTerminalProtocol.TabIndex = 113;
            this.lbTerminalProtocol.Text = "Protocolo";
            // 
            // lbTerminal
            // 
            this.lbTerminal.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminal.Appearance.Options.UseFont = true;
            this.lbTerminal.Appearance.Options.UseTextOptions = true;
            this.lbTerminal.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminal.Location = new System.Drawing.Point(12, 202);
            this.lbTerminal.Name = "lbTerminal";
            this.lbTerminal.Size = new System.Drawing.Size(203, 34);
            this.lbTerminal.TabIndex = 112;
            this.lbTerminal.Text = "Umbral de velocidad parada Km/h";
            // 
            // lbTerminalUmbralTempParada
            // 
            this.lbTerminalUmbralTempParada.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalUmbralTempParada.Appearance.Options.UseFont = true;
            this.lbTerminalUmbralTempParada.Appearance.Options.UseTextOptions = true;
            this.lbTerminalUmbralTempParada.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalUmbralTempParada.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalUmbralTempParada.Location = new System.Drawing.Point(12, 154);
            this.lbTerminalUmbralTempParada.Name = "lbTerminalUmbralTempParada";
            this.lbTerminalUmbralTempParada.Size = new System.Drawing.Size(203, 40);
            this.lbTerminalUmbralTempParada.TabIndex = 111;
            this.lbTerminalUmbralTempParada.Text = "Umbral de tiempo parada en min";
            // 
            // teTerminalMaxDistancia
            // 
            this.teTerminalMaxDistancia.EditValue = "20";
            this.teTerminalMaxDistancia.Location = new System.Drawing.Point(234, 5);
            this.teTerminalMaxDistancia.Name = "teTerminalMaxDistancia";
            this.teTerminalMaxDistancia.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTerminalMaxDistancia.Properties.Appearance.Options.UseFont = true;
            this.teTerminalMaxDistancia.Size = new System.Drawing.Size(128, 22);
            this.teTerminalMaxDistancia.TabIndex = 110;
            // 
            // lbTerminalMaxDistancia
            // 
            this.lbTerminalMaxDistancia.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMaxDistancia.Appearance.Options.UseFont = true;
            this.lbTerminalMaxDistancia.Appearance.Options.UseTextOptions = true;
            this.lbTerminalMaxDistancia.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMaxDistancia.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMaxDistancia.Location = new System.Drawing.Point(12, 0);
            this.lbTerminalMaxDistancia.Name = "lbTerminalMaxDistancia";
            this.lbTerminalMaxDistancia.Size = new System.Drawing.Size(203, 48);
            this.lbTerminalMaxDistancia.TabIndex = 109;
            this.lbTerminalMaxDistancia.Text = "Maxima distancia entre dos puntos en km";
            // 
            // lbTerminalIntervalSendCola
            // 
            this.lbTerminalIntervalSendCola.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalIntervalSendCola.Appearance.Options.UseFont = true;
            this.lbTerminalIntervalSendCola.Appearance.Options.UseTextOptions = true;
            this.lbTerminalIntervalSendCola.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalIntervalSendCola.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalIntervalSendCola.Location = new System.Drawing.Point(470, 241);
            this.lbTerminalIntervalSendCola.Name = "lbTerminalIntervalSendCola";
            this.lbTerminalIntervalSendCola.Size = new System.Drawing.Size(200, 43);
            this.lbTerminalIntervalSendCola.TabIndex = 124;
            this.lbTerminalIntervalSendCola.Text = "Frecuencia Envio de Cola (seg)";
            // 
            // lbTerminalMinWaitRsp
            // 
            this.lbTerminalMinWaitRsp.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMinWaitRsp.Appearance.Options.UseFont = true;
            this.lbTerminalMinWaitRsp.Appearance.Options.UseTextOptions = true;
            this.lbTerminalMinWaitRsp.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMinWaitRsp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMinWaitRsp.Location = new System.Drawing.Point(467, 192);
            this.lbTerminalMinWaitRsp.Name = "lbTerminalMinWaitRsp";
            this.lbTerminalMinWaitRsp.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalMinWaitRsp.TabIndex = 123;
            this.lbTerminalMinWaitRsp.Text = "Mínimo Tiempo espera respuesta (seg)";
            // 
            // lbTerminalMaxWaitRsp
            // 
            this.lbTerminalMaxWaitRsp.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMaxWaitRsp.Appearance.Options.UseFont = true;
            this.lbTerminalMaxWaitRsp.Appearance.Options.UseTextOptions = true;
            this.lbTerminalMaxWaitRsp.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMaxWaitRsp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMaxWaitRsp.Location = new System.Drawing.Point(470, 146);
            this.lbTerminalMaxWaitRsp.Name = "lbTerminalMaxWaitRsp";
            this.lbTerminalMaxWaitRsp.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalMaxWaitRsp.TabIndex = 122;
            this.lbTerminalMaxWaitRsp.Text = "Máximo Tiempo espera respuesta (seg)";
            // 
            // lbTerminalTxDelayOn
            // 
            this.lbTerminalTxDelayOn.Appearance.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalTxDelayOn.Appearance.Options.UseFont = true;
            this.lbTerminalTxDelayOn.Appearance.Options.UseTextOptions = true;
            this.lbTerminalTxDelayOn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalTxDelayOn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalTxDelayOn.Location = new System.Drawing.Point(470, 71);
            this.lbTerminalTxDelayOn.Name = "lbTerminalTxDelayOn";
            this.lbTerminalTxDelayOn.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalTxDelayOn.TabIndex = 121;
            this.lbTerminalTxDelayOn.Text = "Frecuencia Envio Tramas en Movimiento (min.)";
            // 
            // lbTerminalTxDelayOff
            // 
            this.lbTerminalTxDelayOff.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalTxDelayOff.Appearance.Options.UseFont = true;
            this.lbTerminalTxDelayOff.Appearance.Options.UseTextOptions = true;
            this.lbTerminalTxDelayOff.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalTxDelayOff.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalTxDelayOff.Location = new System.Drawing.Point(470, 8);
            this.lbTerminalTxDelayOff.Name = "lbTerminalTxDelayOff";
            this.lbTerminalTxDelayOff.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalTxDelayOff.TabIndex = 120;
            this.lbTerminalTxDelayOff.Text = "Frecuencia Envio Tramas en Parado (min)";
            // 
            // tbcTerminalMaxWaitRsp
            // 
            this.tbcTerminalMaxWaitRsp.EditValue = 1;
            this.tbcTerminalMaxWaitRsp.Location = new System.Drawing.Point(711, 144);
            this.tbcTerminalMaxWaitRsp.Name = "tbcTerminalMaxWaitRsp";
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalMaxWaitRsp.Size = new System.Drawing.Size(198, 42);
            this.tbcTerminalMaxWaitRsp.TabIndex = 127;
            this.tbcTerminalMaxWaitRsp.Value = 1;
            // 
            // tbcTerminalTxDelayOn
            // 
            this.tbcTerminalTxDelayOn.EditValue = 1;
            this.tbcTerminalTxDelayOn.Location = new System.Drawing.Point(711, 71);
            this.tbcTerminalTxDelayOn.Name = "tbcTerminalTxDelayOn";
            this.tbcTerminalTxDelayOn.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalTxDelayOn.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalTxDelayOn.Size = new System.Drawing.Size(198, 42);
            this.tbcTerminalTxDelayOn.TabIndex = 126;
            this.tbcTerminalTxDelayOn.Value = 1;
            // 
            // tcbTerminalTxDelayOff
            // 
            this.tcbTerminalTxDelayOff.EditValue = 1;
            this.tcbTerminalTxDelayOff.Location = new System.Drawing.Point(711, 9);
            this.tcbTerminalTxDelayOff.Name = "tcbTerminalTxDelayOff";
            this.tcbTerminalTxDelayOff.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalTxDelayOff.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalTxDelayOff.Size = new System.Drawing.Size(198, 42);
            this.tcbTerminalTxDelayOff.TabIndex = 125;
            this.tcbTerminalTxDelayOff.Value = 1;
            // 
            // tcbTerminalIntervalSendCola
            // 
            this.tcbTerminalIntervalSendCola.EditValue = 1;
            this.tcbTerminalIntervalSendCola.Location = new System.Drawing.Point(711, 243);
            this.tcbTerminalIntervalSendCola.Name = "tcbTerminalIntervalSendCola";
            this.tcbTerminalIntervalSendCola.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalIntervalSendCola.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalIntervalSendCola.Size = new System.Drawing.Size(198, 42);
            this.tcbTerminalIntervalSendCola.TabIndex = 129;
            this.tcbTerminalIntervalSendCola.Value = 1;
            // 
            // tbcTerminalMinWaitRsp
            // 
            this.tbcTerminalMinWaitRsp.EditValue = 1;
            this.tbcTerminalMinWaitRsp.Location = new System.Drawing.Point(711, 192);
            this.tbcTerminalMinWaitRsp.Name = "tbcTerminalMinWaitRsp";
            this.tbcTerminalMinWaitRsp.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalMinWaitRsp.Size = new System.Drawing.Size(198, 42);
            this.tbcTerminalMinWaitRsp.TabIndex = 128;
            this.tbcTerminalMinWaitRsp.Value = 1;
            // 
            // ViewTerminalConfigC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tcbTerminalIntervalSendCola);
            this.Controls.Add(this.tbcTerminalMinWaitRsp);
            this.Controls.Add(this.tbcTerminalMaxWaitRsp);
            this.Controls.Add(this.tbcTerminalTxDelayOn);
            this.Controls.Add(this.tcbTerminalTxDelayOff);
            this.Controls.Add(this.lbTerminalIntervalSendCola);
            this.Controls.Add(this.lbTerminalMinWaitRsp);
            this.Controls.Add(this.lbTerminalMaxWaitRsp);
            this.Controls.Add(this.lbTerminalTxDelayOn);
            this.Controls.Add(this.lbTerminalTxDelayOff);
            this.Controls.Add(this.teTerminalVersion);
            this.Controls.Add(this.lbTerminalVersion);
            this.Controls.Add(this.lbTerminalSleepLogout);
            this.Controls.Add(this.teTerminalSN);
            this.Controls.Add(this.lbTerminalSN);
            this.Controls.Add(this.cbTerminalProtocol);
            this.Controls.Add(this.lbTerminalProtocol);
            this.Controls.Add(this.lbTerminal);
            this.Controls.Add(this.lbTerminalUmbralTempParada);
            this.Controls.Add(this.teTerminalMaxDistancia);
            this.Controls.Add(this.lbTerminalMaxDistancia);
            this.Controls.Add(this.tbcTerminalSleepLogout);
            this.Controls.Add(this.tbcTerminalUmbralVelocidadParada);
            this.Controls.Add(this.tbcTerminalUmbralTempParada);
            this.Name = "ViewTerminalConfigC";
            this.Size = new System.Drawing.Size(942, 358);
            this.Controls.SetChildIndex(this.tbcTerminalUmbralTempParada, 0);
            this.Controls.SetChildIndex(this.tbcTerminalUmbralVelocidadParada, 0);
            this.Controls.SetChildIndex(this.tbcTerminalSleepLogout, 0);
            this.Controls.SetChildIndex(this.lbTerminalMaxDistancia, 0);
            this.Controls.SetChildIndex(this.teTerminalMaxDistancia, 0);
            this.Controls.SetChildIndex(this.lbTerminalUmbralTempParada, 0);
            this.Controls.SetChildIndex(this.lbTerminal, 0);
            this.Controls.SetChildIndex(this.lbTerminalProtocol, 0);
            this.Controls.SetChildIndex(this.cbTerminalProtocol, 0);
            this.Controls.SetChildIndex(this.lbTerminalSN, 0);
            this.Controls.SetChildIndex(this.teTerminalSN, 0);
            this.Controls.SetChildIndex(this.lbTerminalSleepLogout, 0);
            this.Controls.SetChildIndex(this.lbTerminalVersion, 0);
            this.Controls.SetChildIndex(this.teTerminalVersion, 0);
            this.Controls.SetChildIndex(this.lbTerminalTxDelayOff, 0);
            this.Controls.SetChildIndex(this.lbTerminalTxDelayOn, 0);
            this.Controls.SetChildIndex(this.lbTerminalMaxWaitRsp, 0);
            this.Controls.SetChildIndex(this.lbTerminalMinWaitRsp, 0);
            this.Controls.SetChildIndex(this.lbTerminalIntervalSendCola, 0);
            this.Controls.SetChildIndex(this.tcbTerminalTxDelayOff, 0);
            this.Controls.SetChildIndex(this.tbcTerminalTxDelayOn, 0);
            this.Controls.SetChildIndex(this.tbcTerminalMaxWaitRsp, 0);
            this.Controls.SetChildIndex(this.tbcTerminalMinWaitRsp, 0);
            this.Controls.SetChildIndex(this.tcbTerminalIntervalSendCola, 0);
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalVersion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTerminalProtocol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalMaxDistancia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalTxDelayOn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalTxDelayOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private global::GComunica.ExeServer.Controls.MyTrackBarControl tbcTerminalUmbralTempParada;
        private global::GComunica.ExeServer.Controls.MyTrackBarControl tbcTerminalUmbralVelocidadParada;
        private global::GComunica.ExeServer.Controls.MyTrackBarControl tbcTerminalSleepLogout;
        private DevExpress.XtraEditors.TextEdit teTerminalVersion;
        private DevExpress.XtraEditors.LabelControl lbTerminalVersion;
        private DevExpress.XtraEditors.LabelControl lbTerminalSleepLogout;
        private DevExpress.XtraEditors.TextEdit teTerminalSN;
        private DevExpress.XtraEditors.LabelControl lbTerminalSN;
        private DevExpress.XtraEditors.ComboBoxEdit cbTerminalProtocol;
        private DevExpress.XtraEditors.LabelControl lbTerminalProtocol;
        private DevExpress.XtraEditors.LabelControl lbTerminal;
        private DevExpress.XtraEditors.LabelControl lbTerminalUmbralTempParada;
        private DevExpress.XtraEditors.TextEdit teTerminalMaxDistancia;
        private DevExpress.XtraEditors.LabelControl lbTerminalMaxDistancia;
        private DevExpress.XtraEditors.LabelControl lbTerminalIntervalSendCola;
        private DevExpress.XtraEditors.LabelControl lbTerminalMinWaitRsp;
        private DevExpress.XtraEditors.LabelControl lbTerminalMaxWaitRsp;
        private DevExpress.XtraEditors.LabelControl lbTerminalTxDelayOn;
        private DevExpress.XtraEditors.LabelControl lbTerminalTxDelayOff;
        private global::GComunica.ExeServer.Controls.MyTrackBarControl tbcTerminalMaxWaitRsp;
        private global::GComunica.ExeServer.Controls.MyTrackBarControl tbcTerminalTxDelayOn;
        private global::GComunica.ExeServer.Controls.MyTrackBarControl tcbTerminalTxDelayOff;
        private global::GComunica.ExeServer.Controls.MyTrackBarControl tcbTerminalIntervalSendCola;
        private global::GComunica.ExeServer.Controls.MyTrackBarControl tbcTerminalMinWaitRsp;

    }
}
