﻿namespace gcperu.Server.UI.Paneles.Forms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
			this.txtClave = new DevExpress.XtraEditors.TextEdit();
			this.lbFechaConfig = new DevExpress.XtraEditors.LabelControl();
			this.cmdOK = new DevExpress.XtraEditors.SimpleButton();
			((System.ComponentModel.ISupportInitialize)(this.txtClave.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// txtClave
			// 
			this.txtClave.EditValue = "";
			this.txtClave.EnterMoveNextControl = true;
			this.txtClave.Location = new System.Drawing.Point(187, 24);
			this.txtClave.Name = "txtClave";
			this.txtClave.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 15.75F);
			this.txtClave.Properties.Appearance.Options.UseFont = true;
			this.txtClave.Properties.Appearance.Options.UseTextOptions = true;
			this.txtClave.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.txtClave.Properties.NullText = "<Clave>";
			this.txtClave.Properties.PasswordChar = '*';
			this.txtClave.Size = new System.Drawing.Size(207, 32);
			this.txtClave.TabIndex = 0;
			// 
			// lbFechaConfig
			// 
			this.lbFechaConfig.Appearance.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbFechaConfig.Appearance.ForeColor = System.Drawing.Color.DarkBlue;
			this.lbFechaConfig.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.lbFechaConfig.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.lbFechaConfig.Location = new System.Drawing.Point(105, 27);
			this.lbFechaConfig.Name = "lbFechaConfig";
			this.lbFechaConfig.Size = new System.Drawing.Size(59, 25);
			this.lbFechaConfig.TabIndex = 142;
			this.lbFechaConfig.Text = "Clave";
			// 
			// cmdOK
			// 
			this.cmdOK.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmdOK.Appearance.Options.UseFont = true;
			this.cmdOK.Image = global::gcperu.Server.UI.Properties.Resources.Select16;
			this.cmdOK.Location = new System.Drawing.Point(231, 84);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(163, 28);
			this.cmdOK.TabIndex = 1;
			this.cmdOK.Text = "Aceptar";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// LoginForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(450, 124);
			this.Controls.Add(this.cmdOK);
			this.Controls.Add(this.txtClave);
			this.Controls.Add(this.lbFechaConfig);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "LoginForm";
			this.Text = "Acceso Administración";
			((System.ComponentModel.ISupportInitialize)(this.txtClave.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtClave;
        private DevExpress.XtraEditors.LabelControl lbFechaConfig;
        private DevExpress.XtraEditors.SimpleButton cmdOK;
    }
}