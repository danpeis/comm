﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class SelectiveUpdateListForm : DevExpress.XtraEditors.XtraForm
    {
        private TerminalSelectiveSoftwareUpdateCol _listaTerm;

        public SelectiveUpdateListForm()
        {
            InitializeComponent();

            this.CenterToScreen();

            try
            {
                _listaTerm = new TerminalSelectiveSoftwareUpdateCol();
                _listaTerm.LoadAll();
            
                bsTerm.DataSource = _listaTerm;
            }
            catch (System.Exception e)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL OBTENER LOS TERMINALES ASOCIADOS A LA ACTUALIZACIÓN SELECTIVA.",e));
            }
            
        }

        private void viewList_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                _listaTerm.Save();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR GUARDAR DATOS.", ex));
            }
        }

        private void viewList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode==Keys.Delete)
            {
                if (MessageBox.Show("¿Eliminar terminal de la lista?","Pregunta",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2)==DialogResult.No)
                    return;

                try
                {
                    //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
                    foreach (int rowhandle in viewList.GetSelectedRows())
                    {
                        try
                        {
                            var ts = bsTerm[viewList.GetDataSourceRowIndex(rowhandle)] as TerminalSelectiveSoftwareUpdate;
                            if (ts == null) continue;
                        
                            ts.MarkAsDeleted();
                        }
                        catch {}
                    }

                    _listaTerm.Save();

                }
                catch (System.Exception ex)
                {
                    ManejarExceptions.ManejarException(new GCException("ERROR ELIMINAR TERMINAL/ES DE LA LISTA.", ex));
                }
            }
        }
    }
}