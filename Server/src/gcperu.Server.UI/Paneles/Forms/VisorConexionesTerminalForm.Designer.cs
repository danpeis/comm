﻿namespace gcperu.Server.UI.Paneles.Forms
{
    partial class VisoConexionesTerminalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.Columns.GridColumn FechaConex;
            DevExpress.XtraGrid.Columns.GridColumn TerminalLastSessionDuration;
            this.bsTerm = new System.Windows.Forms.BindingSource(this.components);
            this.frMain = new DevExpress.XtraEditors.GroupControl();
            this.dbgIncid = new DevExpress.XtraGrid.GridControl();
            this.viewIncid = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TerminalDisconnect = new DevExpress.XtraGrid.Columns.GridColumn();
            FechaConex = new DevExpress.XtraGrid.Columns.GridColumn();
            TerminalLastSessionDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frMain)).BeginInit();
            this.frMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dbgIncid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewIncid)).BeginInit();
            this.SuspendLayout();
            // 
            // FechaConex
            // 
            FechaConex.Caption = "Inicio Conexión";
            FechaConex.DisplayFormat.FormatString = "dd-MM-yy HH:mm:ss";
            FechaConex.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            FechaConex.FieldName = "TerminalConnectedFrom";
            FechaConex.Name = "FechaConex";
            FechaConex.Visible = true;
            FechaConex.VisibleIndex = 0;
            FechaConex.Width = 137;
            // 
            // TerminalLastSessionDuration
            // 
            TerminalLastSessionDuration.AppearanceCell.Options.UseTextOptions = true;
            TerminalLastSessionDuration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            TerminalLastSessionDuration.Caption = "Duración (min)";
            TerminalLastSessionDuration.DisplayFormat.FormatString = "0";
            TerminalLastSessionDuration.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            TerminalLastSessionDuration.FieldName = "TerminalLastSessionDuration";
            TerminalLastSessionDuration.Name = "TerminalLastSessionDuration";
            TerminalLastSessionDuration.ToolTip = "Duracion de la ultima sesión de conexion del Terminal en minutos";
            TerminalLastSessionDuration.Visible = true;
            TerminalLastSessionDuration.VisibleIndex = 2;
            TerminalLastSessionDuration.Width = 230;
            // 
            // frMain
            // 
            this.frMain.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frMain.AppearanceCaption.Options.UseFont = true;
            this.frMain.CaptionImage = global::gcperu.Server.UI.Properties.Resources.Grafico24;
            this.frMain.CaptionImagePadding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.frMain.Controls.Add(this.dbgIncid);
            this.frMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frMain.Location = new System.Drawing.Point(0, 0);
            this.frMain.Name = "frMain";
            this.frMain.Size = new System.Drawing.Size(779, 548);
            this.frMain.TabIndex = 0;
            this.frMain.Text = "Conexiones del Vehiculo.  Matricula:{0}   IMEI:{1}";
            // 
            // dbgIncid
            // 
            this.dbgIncid.DataSource = this.bsTerm;
            this.dbgIncid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbgIncid.Location = new System.Drawing.Point(2, 32);
            this.dbgIncid.MainView = this.viewIncid;
            this.dbgIncid.Name = "dbgIncid";
            this.dbgIncid.Size = new System.Drawing.Size(775, 514);
            this.dbgIncid.TabIndex = 0;
            this.dbgIncid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewIncid});
            // 
            // viewIncid
            // 
            this.viewIncid.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gray;
            this.viewIncid.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Gray;
            this.viewIncid.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewIncid.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewIncid.Appearance.FocusedRow.Options.UseFont = true;
            this.viewIncid.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            FechaConex,
            this.TerminalDisconnect,
            TerminalLastSessionDuration});
            this.viewIncid.GridControl = this.dbgIncid;
            this.viewIncid.Name = "viewIncid";
            this.viewIncid.OptionsBehavior.Editable = false;
            this.viewIncid.OptionsView.ShowAutoFilterRow = true;
            this.viewIncid.OptionsView.ShowFooter = true;
            this.viewIncid.OptionsView.ShowGroupPanel = false;
            this.viewIncid.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(FechaConex, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // TerminalDisconnect
            // 
            this.TerminalDisconnect.Caption = "Fin Conexión";
            this.TerminalDisconnect.DisplayFormat.FormatString = "dd-MM-yy HH:mm:ss";
            this.TerminalDisconnect.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TerminalDisconnect.FieldName = "TerminalDisconnect";
            this.TerminalDisconnect.Name = "TerminalDisconnect";
            this.TerminalDisconnect.Visible = true;
            this.TerminalDisconnect.VisibleIndex = 1;
            this.TerminalDisconnect.Width = 160;
            // 
            // VisoConexionesTerminalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 548);
            this.Controls.Add(this.frMain);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VisoConexionesTerminalForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Historial de Conexiones asociadas a Terminal";
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frMain)).EndInit();
            this.frMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dbgIncid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewIncid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bsTerm;
        private DevExpress.XtraEditors.GroupControl frMain;
        private DevExpress.XtraGrid.GridControl dbgIncid;
        private DevExpress.XtraGrid.Views.Grid.GridView viewIncid;
        private DevExpress.XtraGrid.Columns.GridColumn TerminalDisconnect;
    }
}