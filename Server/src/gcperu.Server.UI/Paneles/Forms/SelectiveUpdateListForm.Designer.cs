﻿namespace gcperu.Server.UI.Paneles.Forms
{
    partial class SelectiveUpdateListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bsTerm = new System.Windows.Forms.BindingSource(this.components);
            this.dbgIncid = new DevExpress.XtraGrid.GridControl();
            this.viewList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVehID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehMcla = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgIncid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewList)).BeginInit();
            this.SuspendLayout();
            // 
            // dbgIncid
            // 
            this.dbgIncid.DataSource = this.bsTerm;
            this.dbgIncid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbgIncid.Location = new System.Drawing.Point(0, 0);
            this.dbgIncid.MainView = this.viewList;
            this.dbgIncid.Name = "dbgIncid";
            this.dbgIncid.Size = new System.Drawing.Size(598, 484);
            this.dbgIncid.TabIndex = 2;
            this.dbgIncid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewList});
            // 
            // viewList
            // 
            this.viewList.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gray;
            this.viewList.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Gray;
            this.viewList.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewList.Appearance.FocusedRow.Options.UseFont = true;
            this.viewList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVehID,
            this.colVehMcla,
            this.colMaxVersion});
            this.viewList.GridControl = this.dbgIncid;
            this.viewList.Name = "viewList";
            this.viewList.OptionsSelection.MultiSelect = true;
            this.viewList.OptionsView.ShowAutoFilterRow = true;
            this.viewList.OptionsView.ShowFooter = true;
            this.viewList.OptionsView.ShowGroupPanel = false;
            this.viewList.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.viewList_CellValueChanged);
            this.viewList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.viewList_KeyDown);
            // 
            // colVehID
            // 
            this.colVehID.Caption = "Vehiculo ID";
            this.colVehID.FieldName = "VehiculoID";
            this.colVehID.Name = "colVehID";
            this.colVehID.OptionsColumn.AllowEdit = false;
            this.colVehID.Visible = true;
            this.colVehID.VisibleIndex = 0;
            this.colVehID.Width = 123;
            // 
            // colVehMcla
            // 
            this.colVehMcla.Caption = "Matricula";
            this.colVehMcla.FieldName = "VehiculoMatricula";
            this.colVehMcla.Name = "colVehMcla";
            this.colVehMcla.OptionsColumn.AllowEdit = false;
            this.colVehMcla.Visible = true;
            this.colVehMcla.VisibleIndex = 1;
            this.colVehMcla.Width = 166;
            // 
            // colMaxVersion
            // 
            this.colMaxVersion.Caption = "Max. Version Software a Aplicar";
            this.colMaxVersion.FieldName = "MaxVersionUpdate";
            this.colMaxVersion.Name = "colMaxVersion";
            this.colMaxVersion.Visible = true;
            this.colMaxVersion.VisibleIndex = 2;
            this.colMaxVersion.Width = 263;
            // 
            // SelectiveUpdateListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 484);
            this.Controls.Add(this.dbgIncid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectiveUpdateListForm";
            this.Text = "Lista de Terminales Actualización Selectiva";
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgIncid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bsTerm;
        private DevExpress.XtraGrid.GridControl dbgIncid;
        private DevExpress.XtraGrid.Views.Grid.GridView viewList;
        private DevExpress.XtraGrid.Columns.GridColumn colVehID;
        private DevExpress.XtraGrid.Columns.GridColumn colVehMcla;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxVersion;
    }
}