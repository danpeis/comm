﻿namespace gcperu.Server.UI.Paneles.Forms
{
    partial class AutoLogoutEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.LabelControl lbConexionIntentos;
            DevExpress.XtraEditors.LabelControl labelControl1;
            DevExpress.XtraEditors.LabelControl labelControl2;
            DevExpress.XtraEditors.LabelControl labelControl3;
            this.cmdOk = new DevExpress.XtraEditors.SimpleButton();
            this.ckEnable = new DevExpress.XtraEditors.CheckEdit();
            this.txtTimeElapse = new DevExpress.XtraEditors.TextEdit();
            this.ckTimeAllDay = new DevExpress.XtraEditors.CheckEdit();
            this.frCriterios = new DevExpress.XtraEditors.GroupControl();
            this.ckStatesAllowed = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.txtTimeMin = new DevExpress.XtraEditors.TextEdit();
            this.tmNotBeforeTime = new DevExpress.XtraEditors.TimeEdit();
            this.bsData = new System.Windows.Forms.BindingSource(this.components);
            lbConexionIntentos = new DevExpress.XtraEditors.LabelControl();
            labelControl1 = new DevExpress.XtraEditors.LabelControl();
            labelControl2 = new DevExpress.XtraEditors.LabelControl();
            labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.ckEnable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeElapse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckTimeAllDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frCriterios)).BeginInit();
            this.frCriterios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ckStatesAllowed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmNotBeforeTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsData)).BeginInit();
            this.SuspendLayout();
            // 
            // lbConexionIntentos
            // 
            lbConexionIntentos.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lbConexionIntentos.Appearance.Options.UseFont = true;
            lbConexionIntentos.Location = new System.Drawing.Point(25, 106);
            lbConexionIntentos.Name = "lbConexionIntentos";
            lbConexionIntentos.Size = new System.Drawing.Size(183, 18);
            lbConexionIntentos.TabIndex = 89;
            lbConexionIntentos.Text = "Cerrar Sesión pasados";
            // 
            // labelControl1
            // 
            labelControl1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl1.Appearance.Options.UseFont = true;
            labelControl1.Location = new System.Drawing.Point(26, 79);
            labelControl1.Name = "labelControl1";
            labelControl1.Size = new System.Drawing.Size(258, 18);
            labelControl1.TabIndex = 92;
            labelControl1.Text = "No tiene que tener servicios en";
            // 
            // labelControl2
            // 
            labelControl2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl2.Appearance.Options.UseFont = true;
            labelControl2.Location = new System.Drawing.Point(25, 65);
            labelControl2.Name = "labelControl2";
            labelControl2.Size = new System.Drawing.Size(215, 18);
            labelControl2.TabIndex = 93;
            labelControl2.Text = "No activar antes de las ...";
            // 
            // labelControl3
            // 
            labelControl3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl3.Appearance.Options.UseFont = true;
            labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            labelControl3.Location = new System.Drawing.Point(26, 119);
            labelControl3.Name = "labelControl3";
            labelControl3.Size = new System.Drawing.Size(285, 36);
            labelControl3.TabIndex = 95;
            labelControl3.Text = "Solo si el Terminal esta es uno de estos estos Estados";
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.Image = global::gcperu.Server.UI.Properties.Resources.Select16;
            this.cmdOk.Location = new System.Drawing.Point(391, 372);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(152, 32);
            this.cmdOk.TabIndex = 2;
            this.cmdOk.Text = "Guardar";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // ckEnable
            // 
            this.ckEnable.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsData, "Enable", true));
            this.ckEnable.Location = new System.Drawing.Point(23, 26);
            this.ckEnable.Name = "ckEnable";
            this.ckEnable.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckEnable.Properties.Appearance.Options.UseFont = true;
            this.ckEnable.Properties.Caption = "Activar Auto Cierre de Sesión";
            this.ckEnable.Size = new System.Drawing.Size(268, 23);
            this.ckEnable.TabIndex = 49;
            // 
            // txtTimeElapse
            // 
            this.txtTimeElapse.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsData, "TimeElapse", true));
            this.txtTimeElapse.EditValue = "30";
            this.txtTimeElapse.Location = new System.Drawing.Point(452, 103);
            this.txtTimeElapse.Name = "txtTimeElapse";
            this.txtTimeElapse.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTimeElapse.Properties.Appearance.Options.UseFont = true;
            this.txtTimeElapse.Properties.Mask.EditMask = "0## min";
            this.txtTimeElapse.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTimeElapse.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTimeElapse.Size = new System.Drawing.Size(95, 26);
            this.txtTimeElapse.TabIndex = 90;
            // 
            // ckTimeAllDay
            // 
            this.ckTimeAllDay.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bsData, "TimeMinAllDay", true));
            this.ckTimeAllDay.Location = new System.Drawing.Point(24, 42);
            this.ckTimeAllDay.Name = "ckTimeAllDay";
            this.ckTimeAllDay.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckTimeAllDay.Properties.Appearance.Options.UseFont = true;
            this.ckTimeAllDay.Properties.Caption = "No tiene que haber servicios en TODO el Dia";
            this.ckTimeAllDay.Size = new System.Drawing.Size(411, 23);
            this.ckTimeAllDay.TabIndex = 91;
            // 
            // frCriterios
            // 
            this.frCriterios.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frCriterios.Appearance.Options.UseFont = true;
            this.frCriterios.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frCriterios.AppearanceCaption.Options.UseFont = true;
            this.frCriterios.Controls.Add(labelControl3);
            this.frCriterios.Controls.Add(this.ckStatesAllowed);
            this.frCriterios.Controls.Add(this.txtTimeMin);
            this.frCriterios.Controls.Add(labelControl1);
            this.frCriterios.Controls.Add(this.ckTimeAllDay);
            this.frCriterios.Enabled = false;
            this.frCriterios.Location = new System.Drawing.Point(29, 148);
            this.frCriterios.Name = "frCriterios";
            this.frCriterios.Size = new System.Drawing.Size(538, 218);
            this.frCriterios.TabIndex = 92;
            this.frCriterios.Text = "Aplicar Auto Cierre si se cumplen ...";
            // 
            // ckStatesAllowed
            // 
            this.ckStatesAllowed.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckStatesAllowed.Appearance.Options.UseFont = true;
            this.ckStatesAllowed.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("0", "Rojo (Estado Especial)"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("1", "Verde"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("2", "Naranja")});
            this.ckStatesAllowed.Location = new System.Drawing.Point(317, 108);
            this.ckStatesAllowed.Name = "ckStatesAllowed";
            this.ckStatesAllowed.Size = new System.Drawing.Size(216, 84);
            this.ckStatesAllowed.TabIndex = 94;
            // 
            // txtTimeMin
            // 
            this.txtTimeMin.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsData, "TimeMinWithoutServices", true));
            this.txtTimeMin.EditValue = "300";
            this.txtTimeMin.Location = new System.Drawing.Point(317, 76);
            this.txtTimeMin.Name = "txtTimeMin";
            this.txtTimeMin.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTimeMin.Properties.Appearance.Options.UseFont = true;
            this.txtTimeMin.Properties.Mask.EditMask = "0## min";
            this.txtTimeMin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTimeMin.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTimeMin.Size = new System.Drawing.Size(118, 26);
            this.txtTimeMin.TabIndex = 93;
            // 
            // tmNotBeforeTime
            // 
            this.tmNotBeforeTime.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsData, "NotBeforeTime", true));
            this.tmNotBeforeTime.EditValue = null;
            this.tmNotBeforeTime.Location = new System.Drawing.Point(452, 62);
            this.tmNotBeforeTime.Name = "tmNotBeforeTime";
            this.tmNotBeforeTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.tmNotBeforeTime.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.tmNotBeforeTime.Properties.Appearance.Options.UseFont = true;
            this.tmNotBeforeTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tmNotBeforeTime.Size = new System.Drawing.Size(95, 26);
            this.tmNotBeforeTime.TabIndex = 94;
            // 
            // bsData
            // 
            this.bsData.DataSource = typeof(GComunica.Core.Info.AutologoutInfo);
            // 
            // AutoLogoutEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 416);
            this.Controls.Add(this.tmNotBeforeTime);
            this.Controls.Add(labelControl2);
            this.Controls.Add(this.frCriterios);
            this.Controls.Add(this.txtTimeElapse);
            this.Controls.Add(lbConexionIntentos);
            this.Controls.Add(this.ckEnable);
            this.Controls.Add(this.cmdOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AutoLogoutEditForm";
            this.Text = "Configuración del Auto Cierre de Sesión";
            ((System.ComponentModel.ISupportInitialize)(this.ckEnable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeElapse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckTimeAllDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frCriterios)).EndInit();
            this.frCriterios.ResumeLayout(false);
            this.frCriterios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ckStatesAllowed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmNotBeforeTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bsData;
        private DevExpress.XtraEditors.SimpleButton cmdOk;
        private DevExpress.XtraEditors.CheckEdit ckEnable;
        private DevExpress.XtraEditors.TextEdit txtTimeElapse;
        private DevExpress.XtraEditors.CheckEdit ckTimeAllDay;
        private DevExpress.XtraEditors.GroupControl frCriterios;
        private DevExpress.XtraEditors.TextEdit txtTimeMin;
        private DevExpress.XtraEditors.TimeEdit tmNotBeforeTime;
        private DevExpress.XtraEditors.CheckedListBoxControl ckStatesAllowed;
    }
}