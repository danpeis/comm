﻿namespace gcperu.Server.UI.Paneles.Forms
{
    partial class AssignGrupoTerminalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bsData = new System.Windows.Forms.BindingSource(this.components);
            this.cmdOk = new DevExpress.XtraEditors.SimpleButton();
            this.cbGrupos = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.bsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGrupos.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdOk
            // 
            this.cmdOk.Image = global::gcperu.Server.UI.Properties.Resources.Select16;
            this.cmdOk.Location = new System.Drawing.Point(391, 105);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(152, 32);
            this.cmdOk.TabIndex = 1;
            this.cmdOk.Text = "Asignar";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // cbGrupos
            // 
            this.cbGrupos.EditValue = "";
            this.cbGrupos.Enabled = false;
            this.cbGrupos.Location = new System.Drawing.Point(12, 41);
            this.cbGrupos.Name = "cbGrupos";
            this.cbGrupos.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGrupos.Properties.Appearance.Options.UseFont = true;
            this.cbGrupos.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGrupos.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cbGrupos.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGrupos.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbGrupos.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGrupos.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbGrupos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGrupos.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGrupos.Size = new System.Drawing.Size(531, 26);
            this.cbGrupos.TabIndex = 15;
            // 
            // AssignGrupoTerminalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 160);
            this.Controls.Add(this.cbGrupos);
            this.Controls.Add(this.cmdOk);
            this.Name = "AssignGrupoTerminalForm";
            this.Text = "Asociar un Grupo a los Terminales ...";
            ((System.ComponentModel.ISupportInitialize)(this.bsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGrupos.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bsData;
        private DevExpress.XtraEditors.SimpleButton cmdOk;
        private DevExpress.XtraEditors.ComboBoxEdit cbGrupos;
    }
}