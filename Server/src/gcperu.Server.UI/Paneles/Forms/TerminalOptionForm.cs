using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using gcperu.Server.UI.Exception;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class TerminalOptionForm : DevExpress.XtraEditors.XtraForm
    {
        //TODO: TerminalOptionForm | CREAR UN USERCONTROL PARA LA CONFIGURACION. Y UTILIZARLO EN TODOS LOS PANELES QUE SEAN NECESARIOSS

        //private HashTableTrama _hashdata;
        //private HashTableTrama _hashNew;
        //private TerminalState _ts;
        //private bool _editable;

        //public TerminalOptionForm(TerminalState ts,TerminalConfiguration tc, HashTableTrama hashdata,bool editable)
        //{
        //    InitializeComponent();

        //    _ts = ts;
        //    _hashdata = hashdata;
        //    _editable = editable;

        //    lbTermInfo.Text = string.Format("{0} - {1}", ts.VehiculoMatricula, ts.VehiculoCodigo);
        //    txtFechaConfig.Text = tc.Fecha.HasValue ? tc.Fecha.Value.ToLongDateString() : "";
        //    //gcGeneral.Enabled = true;
           
        //    RellenarCombosEnum();
        //    AssigLimitsValues();
        //    RellenarControles();

        //    cmdOK.Visible = _editable;
        //    ckConfigFull.Visible = _editable;
        //    cmdCrearTemplate.Visible = ServerConfig.SessionAdminActive;

        //    if (_editable)
        //    {
        //        EnabledAllControls();
        //    }
        //}

        //private void AssigLimitsValues()
        //{
        //    tbcTerminalMaxWaitRsp.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYMAXTIMEOUTWAITRSP.ToInt();
        //    tbcTerminalMaxWaitRsp.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYMAXTIMEOUTWAITRSP.ToInt();

        //    tcbTerminalTxDelayOff.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYENGINEOFF.ToInt();
        //    tcbTerminalTxDelayOff.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYENGINEOFF.ToInt();

        //    tbcTerminalMinWaitRsp.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYYMINTIMEOUTWAITRSP.ToInt();
        //    tbcTerminalMinWaitRsp.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYYMINTIMEOUTWAITRSP.ToInt();

        //    tcbTerminalIntervalSendCola.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYSENDCOLASALIDA.ToInt();
        //    tcbTerminalIntervalSendCola.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYSENDCOLASALIDA.ToInt();

        //    tcbTerminalSinConexionServidor.Properties.Minimum = LimitsValues.MinTiempoSinConexionServidor.ToInt();
        //    tcbTerminalSinConexionServidor.Properties.Maximum = LimitsValues.MaxTiempoSinConexionServidor.ToInt();

        //    tcbTerminalSinConexionInternet.Properties.Minimum = LimitsValues.MinTiempoSinConexionInternet.ToInt();
        //    tcbTerminalSinConexionInternet.Properties.Maximum = LimitsValues.MaxTiempoSinConexionInternet.ToInt();

        //    tcbTerminalSinRecepcionGps.Properties.Minimum = LimitsValues.MinTiempoSinRecepcionGps.ToInt();
        //    tcbTerminalSinRecepcionGps.Properties.Maximum = LimitsValues.MaxTiempoSinRecepcionGps.ToInt();

        //    tcbTerminalIntentosHotResetModem.Properties.Minimum = LimitsValues.MinIntentosHotResetModem.ToInt();
        //    tcbTerminalIntentosHotResetModem.Properties.Maximum = LimitsValues.MaxIntentosHotResetModem.ToInt();

        //    tcbTerminalMaxColaSalida.Properties.Minimum = LimitsValues.MinTramasQueueOut.ToInt();
        //    tcbTerminalMaxColaSalida.Properties.Maximum = LimitsValues.MaxTramasQueueOut.ToInt();
        //}


        //private void RellenarControles()
        //{
        //    DateTime dateValue;
            
        //    //Terminal
        //    teTerminalMaxDistancia.Text = _hashdata[Core.Config.Constantes.TerminalODOMETROMAXDISTANCIA2PUNTOS].ToString();
        //    cbTerminalProtocol.Text = _hashdata.AsEnum(Core.Config.Constantes.TerminalProtocolType, ProtocolType.Undefined).ToString();
        //    teTerminalSN.Text = _hashdata[Core.Config.Constantes.TerminalSerialNumber].ToString();
        //    teTerminalVersion.Text = _hashdata[Core.Config.Constantes.TerminalVERSION].ToString();
        //    tbcTerminalUmbralTempParada.Value = _hashdata[Core.Config.Constantes.TerminalPARADATIEMPOVELOCIDADPORDEBAJO].ToInt();
        //    tbcTerminalUmbralVelocidadParada.Value = _hashdata[Core.Config.Constantes.TerminalPARADAUMBRALVELOCIDAD].ToInt();
        //    tbcTerminalSleepLogout.Value = _hashdata[Core.Config.Constantes.TerminalSleepLogout].ToInt();
        //    tcbTerminalTxDelayOff.Value = _hashdata[Core.Config.Constantes.TerminalTXDELAYENGINEOFF].ToInt();
        //    txtTerminalTxDelayOn.EditValue = _hashdata[Core.Config.Constantes.TerminalTXDELAYENGINEON].ToInt();
        //    tbcTerminalMaxWaitRsp.Value = _hashdata[Core.Config.Constantes.TerminalTXDELAYMAXTIMEOUTWAITRSP].ToInt();
        //    tbcTerminalMinWaitRsp.Value = _hashdata[Core.Config.Constantes.TerminalTXDELAYYMINTIMEOUTWAITRSP].ToInt();
        //    tcbTerminalIntervalSendCola.Value = _hashdata[Core.Config.Constantes.TerminalTXDELAYSENDCOLASALIDA].ToInt();
        //    teDaysForReboot.Text = _hashdata.AsInt(Core.Config.Constantes.TerminalDaysForReboot).ToString();
        //    cbTerminalModoColores.Text = _hashdata[Core.Config.Constantes.TerminalModoVehiculoEstadoColores].ToString();
        //    teDaysForShutDown.Text = _hashdata[Core.Config.Constantes.TerminalMaxDaysToShutDown].ToString();
        //    teLastShutDown.Text = _hashdata[Core.Config.Constantes.TerminalDateLastShutDown].ToString();
        //    tcbTerminalMaxColaSalida.Value = _hashdata[Core.Config.Constantes.TerminalQueueOutMaxTramas].ToInt();

        //    //Terminal 2
        //    tcbTerminalSinConexionServidor.Value = _hashdata[Core.Config.Constantes.TerminalSINCONEXIONSERVIDOR_TIEMPOMAXIMO].ToInt();
        //    tcbTerminalSinConexionInternet.Value = _hashdata[Core.Config.Constantes.TerminalSINCONEXIONINTERNET_TIEMPOMAXIMO].ToInt();
        //    tcbTerminalIntentosHotResetModem.Value = _hashdata[Core.Config.Constantes.TerminalINTENTOS_HOT_RESET_MODEM].ToInt();
        //    tcbTerminalSinRecepcionGps.Value = _hashdata[Core.Config.Constantes.TerminalSinRecepcionGps_TIEMPOMAXIMO].ToInt();
        //    cbModoConexionInternet.Text = _hashdata[Core.Config.Constantes.TerminalModoConexionInternet].ToString();
        //    tcbTerminalMaxColaSalida.Value = _hashdata[Core.Config.Constantes.TerminalQueueOutMaxTramas].ToInt();

        //    //Energia
        //    cbUserMonitorTimeout.Text = _hashdata[Core.Config.Constantes.TerminalMonitorTurnoffTimeout].ToString();
            

        //    //Perfil Movil
        //    teProfileMobileForceUse.Checked = _hashdata.AsBool(Core.Config.Constantes.DialupForceUse);
        //    teProfileMobileName.Text = _hashdata[Constantes.DialupName].ToString();
        //    teProfileMobileAPN.Text = _hashdata[Constantes.DialupAPN].ToString();
        //    teProfileMobileUserName.Text = _hashdata[Constantes.DialupUserName].ToString();
        //    teProfileMobilePassword.Text = _hashdata[Constantes.DialupUserPass].ToString();
        //    teProfileMobileNumber.Text = _hashdata[Constantes.DialupPhoneNumber].ToString();

        //    //GPS
        //    cbGpsPort.Text = _hashdata[Core.Config.Constantes.GpsPort].ToString();
        //    cbGpsBaudRate.Text = _hashdata[Core.Config.Constantes.GpsBaudeRate].ToString();
        //    cbGpsDataBits.Text = _hashdata[Core.Config.Constantes.GpsDataBits].ToString();
        //    cbGpsParity.Text = _hashdata[Core.Config.Constantes.GpsParity].ToString();
        //    cbGpsStopBits.Text = _hashdata[Core.Config.Constantes.GpsStopBits].ToString();
        //    cbGpsFlowControl.Text = _hashdata[Core.Config.Constantes.GpsFlowControl].ToString();

        //    //FTP
        //    teFtpRemoteHost.Text = _hashdata[Core.Config.Constantes.FTPTerminalRemoteHost].ToString();
        //    teFtpPort.Text = _hashdata[Core.Config.Constantes.FTPPort].ToString();
        //    teFtpPathRemote.Text = _hashdata[Core.Config.Constantes.FTPRemotePath].ToString();
        //    teFtpUser.Text = _hashdata[Core.Config.Constantes.FTPRemoteUser].ToString();
        //    teFtpPwd.Text = _hashdata[Core.Config.Constantes.FTPRemotePwd].ToString();

        //    if (DateTime.TryParse(_hashdata[Core.Config.Constantes.FTPTerminalLastDateLogSend].ToString(), out dateValue))
        //        deFtpLastDateLog.DateTime = dateValue;

        //    cbFtpModoEnvioLog.Text = _hashdata[Core.Config.Constantes.FTPTerminalMODOEnvioLOG].ToString();

        //    //UPDATES
        //    teUpdateRemoteHost.Text = _hashdata[Core.Config.Constantes.AutoUpdateRemoteHost].ToString();
        //    teUpdatePathRemote.Text = _hashdata[Core.Config.Constantes.AutoUpdateRemotePath].ToString();
        //    teUpdateUser.Text = _hashdata[Core.Config.Constantes.AutoUpdateUser].ToString();
        //    teUpdatePwd.Text = _hashdata[Core.Config.Constantes.AutoUpdatePwd].ToString();
            
        //    //Trace
        //    cbTraceLevel.Text = _hashdata[Core.Config.Constantes.TraceLevel].ToString();
        //    teTracePFileMaxSize.Text = _hashdata[Core.Config.Constantes.TracePFileMaxSize].ToString();
        //    teTracePFileName.Text = _hashdata[Core.Config.Constantes.TracePFileName].ToString();
        //    cbTracePFileRotate.Text = _hashdata[Core.Config.Constantes.TracePFileRotateMode].ToString();
        //    cbTraceProtocol.Text = _hashdata[Core.Config.Constantes.TraceProtocol].ToString();

        //    //Conexion
        //    teConexionIMEI.Text = _hashdata[Core.Config.Constantes.ModemIMEI].ToString();
        //    teConexionSN.Text = _hashdata[Core.Config.Constantes.ModemSIMSN].ToString();
        //    cbConexionDefaultIP.Text = _hashdata[Core.Config.Constantes.ModemSERVERDefaultIP].ToString();
        //    teConexionIntentos.Text = _hashdata[Core.Config.Constantes.ModemSERVERNumConexIntentos].ToString();
        //    teConexionServerName1.Text = _hashdata[Core.Config.Constantes.ModemSERVERNAME1].ToString();
        //    teConexionServerIP1.Text = _hashdata[Core.Config.Constantes.ModemSERVERIP1].ToString();
        //    teConexionServerPort1.Text = _hashdata[Core.Config.Constantes.ModemSERVERPORT1].ToString();
        //    teConexionServerName2.Text = _hashdata[Core.Config.Constantes.ModemSERVERNAME2].ToString();
        //    teConexionServerIP2.Text = _hashdata[Core.Config.Constantes.ModemSERVERIP2].ToString();
        //    teConexionServerPort2.Text = _hashdata[Core.Config.Constantes.ModemSERVERPORT2].ToString();
        //    teConexionServerName3.Text = _hashdata[Core.Config.Constantes.ModemSERVERNAME3].ToString();
        //    teConexionServerIP3.Text = _hashdata[Core.Config.Constantes.ModemSERVERIP3].ToString();
        //    teConexionServerPort3.Text = _hashdata[Core.Config.Constantes.ModemSERVERPORT3].ToString();

        //    //***** General ********************************
        //    //Comida
        //    ckComidaActivo.Checked = _hashdata.AsBool(Core.Config.Constantes.TerminalComidaGestionActiva);
        //    ckComidaAutoDesactivar.Checked =_hashdata.AsBool(Constantes.TerminalComidaAutoDeactive);
        //    tbcComidaDesactivarDespuesDe.Properties.Minimum = LimitsValues.MinTiempoEstadoComidaActivo.ToInt();
        //    tbcComidaDesactivarDespuesDe.Properties.Maximum = LimitsValues.MaxTiempoEstadoComidaActivo.ToInt();
        //    tbcComidaDesactivarDespuesDe.Value = _hashdata.AsInt(Constantes.TerminalComidaMaximoTiempoActivo, LimitsValues.DefaultTiempoEstadoComidaActivo.ToInt());

        //    //Gesti�n cambio de Vehiculo
        //    ckCambioVehiculoActivo.Checked = _hashdata.AsBool(Constantes.TerminalVehiculoCambioGestionActiva);
        //    //Gestion Mantenimiento
        //    ckMntoActivo.Checked = _hashdata.AsBool(Constantes.TerminalMantenimientoGestionActiva);
        //    //Gesti�n Catastrofe
        //    ckCatastrofeActivo.Checked = _hashdata.AsBool(Constantes.TerminalVehiculoCatastrofeActiva);
        //    //Preguntar por Tipo de Actividad
        //    ckLoginAskActivityType.Checked = _hashdata.AsBool(Constantes.TerminalLoginTipoActividadActiva);
        //    //Mostrar agregar WayTrack en los ServiciosEspeciales
        //    ckSE_ShowMarker.Checked = _hashdata.AsBool(Constantes.TerminalServEspShowButtonMarker);
        //    //NoFiltrar servicios by login.
        //    ckNoFiltrarServiciosByLoginActivo.Checked = _hashdata.AsBool(Constantes.TerminalNoFiltrarServicioByLogin);
        //    //***********************************************


        //}

        //private HashTableTrama SaveValueControls()
        //{
        //    //VALIDACIONES.
        //    if (txtTerminalTxDelayOn.EditValue.ToByte() == 0 ||
        //        txtTerminalTxDelayOn.EditValue.ToByte() < LimitsValues.MinTerminalTXDELAYENGINEON.ToInt() ||
        //        txtTerminalTxDelayOn.EditValue.ToByte() > LimitsValues.MaxTerminalTXDELAYENGINEON.ToInt())
        //    {
        //        MessageBox.Show("Frecuencia Envio Tramas en Movimiento (seg.): Valor no v�lido.", "Aviso",
        //                        MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1,
        //                        MessageBoxOptions.DefaultDesktopOnly);
        //        return null;
        //    }

        //    var hdnew = new HashTableTrama();// Global.Device.Config.HashData;
            
        //    try
        //    {
        //        //Terminal 1
        //        hdnew[Core.Config.Constantes.TerminalODOMETROMAXDISTANCIA2PUNTOS] = teTerminalMaxDistancia.Text.ToInt();
        //        hdnew[Core.Config.Constantes.TerminalProtocolType] = cbTerminalProtocol.Text.ToInt();
        //        hdnew[Core.Config.Constantes.TerminalSerialNumber] = teTerminalSN.Text;
        //        hdnew[Core.Config.Constantes.TerminalVERSION] = teTerminalVersion.Text;
        //        hdnew[Core.Config.Constantes.TerminalPARADATIEMPOVELOCIDADPORDEBAJO] = tbcTerminalUmbralTempParada.Value;
        //        hdnew[Core.Config.Constantes.TerminalPARADAUMBRALVELOCIDAD] = tbcTerminalUmbralVelocidadParada.Value;
        //        hdnew[Core.Config.Constantes.TerminalSleepLogout] = tbcTerminalSleepLogout.Value;
        //        hdnew[Core.Config.Constantes.TerminalTXDELAYENGINEOFF] = tcbTerminalTxDelayOff.Value;
        //        hdnew[Core.Config.Constantes.TerminalTXDELAYENGINEON]=txtTerminalTxDelayOn.EditValue;
        //        hdnew[Core.Config.Constantes.TerminalTXDELAYMAXTIMEOUTWAITRSP] = tbcTerminalMaxWaitRsp.Value;
        //        hdnew[Core.Config.Constantes.TerminalTXDELAYYMINTIMEOUTWAITRSP] = tbcTerminalMinWaitRsp.Value;
        //        hdnew[Core.Config.Constantes.TerminalTXDELAYSENDCOLASALIDA] = tcbTerminalIntervalSendCola.Value;
        //        hdnew[Core.Config.Constantes.TerminalDaysForReboot] = teDaysForReboot.Text.ToInt();
        //        hdnew[Core.Config.Constantes.TerminalModoVehiculoEstadoColores] = cbTerminalModoColores.Text;
        //        hdnew[Core.Config.Constantes.TerminalMonitorTurnoffTimeout] = cbUserMonitorTimeout.Text;
        //        hdnew[Core.Config.Constantes.TerminalMaxDaysToShutDown] = teDaysForShutDown.Text;
        //        hdnew[Core.Config.Constantes.TerminalQueueOutMaxTramas] = tcbTerminalMaxColaSalida.Value;

        //        //Terminal 2
        //        hdnew[Core.Config.Constantes.TerminalSINCONEXIONINTERNET_TIEMPOMAXIMO] = tcbTerminalSinConexionInternet.Value;
        //        hdnew[Core.Config.Constantes.TerminalSINCONEXIONSERVIDOR_TIEMPOMAXIMO] = tcbTerminalSinConexionServidor.Value;
        //        hdnew[Core.Config.Constantes.TerminalSinRecepcionGps_TIEMPOMAXIMO] = tcbTerminalSinRecepcionGps.Value;
        //        hdnew[Core.Config.Constantes.TerminalINTENTOS_HOT_RESET_MODEM] = tcbTerminalIntentosHotResetModem.Value;
        //        hdnew[Core.Config.Constantes.TerminalModoConexionInternet] = cbModoConexionInternet.Text;

        //        //Perfil Movil
        //        hdnew[Core.Config.Constantes.DialupForceUse] = teProfileMobileForceUse.Checked;
        //        hdnew[Core.Config.Constantes.DialupName] = teProfileMobileName.Text;
        //        hdnew[Core.Config.Constantes.DialupAPN] = teProfileMobileAPN.Text;
        //        hdnew[Core.Config.Constantes.DialupUserName] = teProfileMobileUserName.Text;
        //        hdnew[Core.Config.Constantes.DialupUserPass] = teProfileMobilePassword.Text;
        //        hdnew[Core.Config.Constantes.DialupPhoneNumber] = teProfileMobileNumber.Text;

        //        //GPS
        //        hdnew[Core.Config.Constantes.GpsPort] = cbGpsPort.Text;
        //        hdnew[Core.Config.Constantes.GpsBaudeRate]=cbGpsBaudRate.Text;
        //        hdnew[Core.Config.Constantes.GpsDataBits]=cbGpsDataBits.Text;
        //        hdnew[Core.Config.Constantes.GpsParity] = cbGpsParity.Text;
        //        hdnew[Core.Config.Constantes.GpsStopBits] = cbGpsStopBits.Text;
        //        hdnew[Core.Config.Constantes.GpsStopBits] =cbGpsStopBits.Text;
        //        hdnew[Core.Config.Constantes.GpsFlowControl] = cbGpsFlowControl.Text;

        //        //FTP
        //        hdnew[Core.Config.Constantes.FTPTerminalRemoteHost] = teFtpRemoteHost.Text;
        //        hdnew[Core.Config.Constantes.FTPPort] = teFtpPort.Text.ToInt();
        //        hdnew[Core.Config.Constantes.FTPRemotePath] = teFtpPathRemote.Text;
        //        hdnew[Core.Config.Constantes.FTPRemoteUser] = teFtpUser.Text;
        //        hdnew[Core.Config.Constantes.FTPRemotePwd] = teFtpPwd.Text;
        //        hdnew[Core.Config.Constantes.FTPTerminalLastDateLogSend] = deFtpLastDateLog.DateTime;
        //        hdnew[Core.Config.Constantes.FTPTerminalMODOEnvioLOG] = cbFtpModoEnvioLog.Text;

        //        //UPDATES
        //        hdnew[Core.Config.Constantes.AutoUpdateRemoteHost] = teUpdateRemoteHost.Text;
        //        hdnew[Core.Config.Constantes.AutoUpdateUser] = teUpdateUser.Text;
        //        hdnew[Core.Config.Constantes.AutoUpdatePwd] = teUpdatePwd.Text;
        //        hdnew[Core.Config.Constantes.AutoUpdateRemotePath] = teUpdatePathRemote.Text;


        //        //Trace
        //        hdnew[Core.Config.Constantes.TraceLevel] =cbTraceLevel.Text;
        //        hdnew[Core.Config.Constantes.TracePFileMaxSize]=teTracePFileMaxSize.Text;
        //        hdnew[Core.Config.Constantes.TracePFileName]=teTracePFileName.Text ;
        //        hdnew[Core.Config.Constantes.TracePFileRotateMode] =cbTracePFileRotate.Text;
        //        hdnew[Core.Config.Constantes.TraceProtocol] =cbTraceProtocol.Text;

        //        //Conexion
        //        hdnew[Core.Config.Constantes.ModemIMEI]=teConexionIMEI.Text;
        //        hdnew[Core.Config.Constantes.ModemSIMSN]=teConexionSN.Text;
        //        hdnew[Core.Config.Constantes.ModemSERVERDefaultIP]=cbConexionDefaultIP.Text.ToInt();
        //        hdnew[Core.Config.Constantes.ModemSERVERNumConexIntentos]=teConexionIntentos.Text.ToInt();
        //        hdnew[Core.Config.Constantes.ModemSERVERNAME1]=teConexionServerName1.Text;
        //        hdnew[Core.Config.Constantes.ModemSERVERIP1]=teConexionServerIP1.Text;
        //        hdnew[Core.Config.Constantes.ModemSERVERPORT1]=teConexionServerPort1.Text;
        //        hdnew[Core.Config.Constantes.ModemSERVERNAME2]=teConexionServerName2.Text;
        //        hdnew[Core.Config.Constantes.ModemSERVERIP2]=teConexionServerIP2.Text;
        //        hdnew[Core.Config.Constantes.ModemSERVERPORT2]=teConexionServerPort2.Text;
        //        hdnew[Core.Config.Constantes.ModemSERVERNAME3]=teConexionServerName3.Text;
        //        hdnew[Core.Config.Constantes.ModemSERVERIP3]=teConexionServerIP3.Text;
        //        hdnew[Core.Config.Constantes.ModemSERVERPORT3]=teConexionServerPort3.Text;

        //        //***** General ********************************
        //        //Comida
        //        _hashdata[Core.Config.Constantes.TerminalComidaGestionActiva] = ckComidaActivo.Checked;
        //        _hashdata[Constantes.TerminalComidaAutoDeactive] = ckComidaAutoDesactivar.Checked;
        //        _hashdata[Constantes.TerminalComidaMaximoTiempoActivo] = tbcComidaDesactivarDespuesDe.Value;

        //        //Gestion Mantenimiento
        //        _hashdata[Constantes.TerminalMantenimientoGestionActiva] = ckMntoActivo.Checked;
        //        //Gesti�n cambio de Vehiculo
        //        _hashdata[Constantes.TerminalVehiculoCambioGestionActiva] = ckCambioVehiculoActivo.Checked;
        //        //Gestion Mantenimiento
        //        _hashdata[Constantes.TerminalMantenimientoGestionActiva] = ckMntoActivo.Checked;
        //        //Gesti�n Catastrofe
        //        _hashdata[Constantes.TerminalVehiculoCatastrofeActiva] = ckCatastrofeActivo.Checked;
        //        //Preguntar por Tipo de Actividad
        //        _hashdata[Constantes.TerminalLoginTipoActividadActiva] = ckLoginAskActivityType.Checked;
        //        //Mostrar agregar WayTrack en los ServiciosEspeciales
        //        _hashdata[Constantes.TerminalServEspShowButtonMarker] = ckSE_ShowMarker.Checked;
        //        //NoFiltrar servicios by login.
        //        _hashdata[Constantes.TerminalNoFiltrarServicioByLogin] = ckNoFiltrarServiciosByLoginActivo.Checked;

        //        //***********************************************

        //        return hdnew;

        //    }
        //    catch (System.Exception ex)
        //    {
        //        TraceLog.LogException(new GCException("SaveValueControls ERROR. LA CONFIGURACION NO SE HA GUARDADO.", ex, this.GetType()));
        //        return null;
        //    }
        //}

        //private HashTableTrama GetHashChanges(Hashtable hsnewchanges)
        //{
        //    try
        //    {
        //        if (hsnewchanges == null)
        //            return null;

        //        var hschanges = new HashTableTrama();

        //        if (!ckConfigFull.Checked)
        //        {
        //            foreach (var entry in _hashdata.Keys)
        //            {
        //                if (_hashdata[entry].ToString() != hsnewchanges[entry].ToString())
        //                    hschanges.Add(entry, hsnewchanges[entry]);
        //            }
        //            //Borramos ciertos cambios si los hubiera, que no interesan que vayan al terminal.
        //            hschanges.Remove(Core.Config.Constantes.TerminalProtocolType);
        //        }
        //        else
        //        {
        //            hschanges = _hashdata;
        //            //Borramos aquello que no nos interesa
        //            //GPS
        //            RemoveDataElemNoShared(hschanges);
        //        }

        //        return hschanges;
        //    }
        //    catch (System.Exception e)
        //    {
        //        ManejarExceptions.ManejarException(new GCException("ERROR OBTENER CAMBIOS EN LA CONFIGURACION\r\n", e));
        //        return null;
        //    }
        //}

        //private static void RemoveDataElemNoShared(HashTableTrama hschanges)
        //{
        //    hschanges.Remove(Core.Config.Constantes.GpsPort);
        //    hschanges.Remove(Core.Config.Constantes.GpsBaudeRate);
        //    hschanges.Remove(Core.Config.Constantes.GpsDataBits);
        //    hschanges.Remove(Core.Config.Constantes.GpsParity);
        //    hschanges.Remove(Core.Config.Constantes.GpsStopBits);
        //    hschanges.Remove(Core.Config.Constantes.GpsFlowControl);

        //    hschanges.Remove(Core.Config.Constantes.TerminalProtocolType);
        //    hschanges.Remove(Core.Config.Constantes.TerminalSerialNumber);
        //    hschanges.Remove(Core.Config.Constantes.TerminalVERSION);
        //    hschanges.Remove(Core.Config.Constantes.ModemIMEI);
        //    hschanges.Remove(Core.Config.Constantes.ModemSIMSN);

        //    hschanges.Remove(Core.Config.Constantes.HardPORT);
        //    hschanges.Remove(Core.Config.Constantes.HardBAUDRATE);
        //    hschanges.Remove(Core.Config.Constantes.HardDATABITS);
        //    hschanges.Remove(Core.Config.Constantes.HardPARITY);
        //    hschanges.Remove(Core.Config.Constantes.HardSTOPBITS);
        //    hschanges.Remove(Core.Config.Constantes.HardFLOWCONTROL);
        //}

        //private void EnabledAllControls()
        //{
   
        //    gcTerminal1.Enabled = true;
        //    gcTerminal2.Enabled = true;
        //    gcConexion.Enabled = true;
        //    gcModem.Enabled = true;
        //    gcGPS.Enabled = true;
        //    gcFtp.Enabled = true;
        //    gcTrace.Enabled = true;
        //    gcGeneral.Enabled = true;

        //}

       

        //#region eventos

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            //if (_editable)
            //{
            //    if (MessageBox.Show("� CERRAR CONIGURACION SIN GUARDAR Y ENVIAR AL TERMINAL?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            //        return;

            //}
            this.Close();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("� GUARDAR Y ENVIAR ESTA CONFIGURACI�N AL TERMINAL?. El servidor crear� una envio, que ser� enviado al conectarse el Terminal.", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            //    return;

            ////Obtenemos solo los cambios, que ha habido.
            //var hsnewchanges = SaveValueControls();
            //if (hsnewchanges==null)
            //    return;

            //var hssend = GetHashChanges(hsnewchanges);
            //if (hssend==null)
            //{
            //    MessageBox.Show("NO HA HABIDO CAMBIOS EN LA CONFIGURACI�N O OCURRI� ALGUN ERROR. IMPOSIBLE ENVIAR NADA AL TERMINAL","Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}

            ////Serializamos los cambios, y obtenemos un string.
            //string xmlsend = hssend.Serialize();
            //string xmlsave = hsnewchanges.Serialize();

            ////Guardamos la nueva configuracion en BD.
            //bool errores = false;
            //if (UpdateConfiguracion(xmlsave))
            //{
            //    //Creamos la Peticion al Terminal.
            //    if (TerminalDataSend.CreatePeticionSend(_ts.VehiculoID.Value, "", PeticionCodeSend.ConfiguracionSet, 1, xmlsend, ""))
            //    {

            //        MessageBox.Show("ENVIO DE ACTUALIZACI�N CREADO CORRECTAMENTE.", "Aviso", MessageBoxButtons.OK,
            //                        MessageBoxIcon.Information);
            //        this.Close();
            //    }
            //    else
            //        errores = true;
            //}
            //else
            //    errores = true;

            //if (errores)
            //{
            //    MessageBox.Show("OCURRIERON ERRORES AL GUARDAR CONFIGURACION O CREAR EL ENVIO.", "Aviso", MessageBoxButtons.OK,
            //                       MessageBoxIcon.Error);
            //    return;
            //}
        }

        //#endregion

        //public bool UpdateConfiguracion(string xmlconfig)
        //{
        //    try
        //    {
        //        var tc = new TerminalConfiguration();
        //        if (!tc.LoadByPrimaryKey(_ts.TerminalID.Value))
        //        {
        //            tc.TerminalID = _ts.TerminalID.Value;
        //            tc.VehiculoCodigo = _ts.VehiculoCodigo;
        //            tc.VehiculoID = _ts.VehiculoID.Value;

        //            tc.Save();
        //        }

        //        //Guardamos la configuracion
        //        tc.ConfigData = xmlconfig;
        //        tc.Save();

        //        return true;
        //    }
        //    catch (System.Exception e)
        //    {
        //        ManejarExceptions.ManejarException(new GCException(string.Format("ERROR GUARDAR CONFIGURACION DEL TERMINAL {0}", this), e),ExceptionErrorLevel.Critical);
        //        return false;
        //    }
        //}

        //private void RellenarCombosEnum()
        //{

        //    cbGpsBaudRate.Properties.Items.Clear();
        //    cbGpsBaudRate.Properties.Items.AddRange(Enum.GetNames(typeof(GeoFramework.IO.Serial.BaudRate)));

        //    cbGpsDataBits.Properties.Items.Clear();
        //    cbGpsDataBits.Properties.Items.AddRange(Enum.GetNames(typeof(GeoFramework.IO.Serial.DataBits)));

        //    cbGpsParity.Properties.Items.Clear();
        //    cbGpsParity.Properties.Items.AddRange(Enum.GetNames(typeof(GeoFramework.IO.Serial.Parity)));

        //    cbGpsStopBits.Properties.Items.Clear();
        //    cbGpsStopBits.Properties.Items.AddRange(Enum.GetNames(typeof(GeoFramework.IO.Serial.StopBits)));

        //    cbGpsFlowControl.Properties.Items.Clear();
        //    cbGpsFlowControl.Properties.Items.AddRange(Enum.GetNames(typeof(GeoFramework.IO.Serial.FlowControl)));

        //    cbTraceLevel.Properties.Items.Clear();
        //    cbTraceLevel.Properties.Items.AddRange(Enum.GetNames(typeof(LevelLog)));

        //    cbTraceProtocol.Properties.Items.Clear();
        //    cbTraceProtocol.Properties.Items.AddRange(Enum.GetNames(typeof(TraceProtocol)));

        //    cbTracePFileRotate.Properties.Items.Clear();
        //    cbTracePFileRotate.Properties.Items.AddRange(Enum.GetNames(typeof(TraceFileRotateModeEnum)));

        //    cbFtpModoEnvioLog.Properties.Items.Clear();
        //    cbFtpModoEnvioLog.Properties.Items.AddRange(Enum.GetNames(typeof(ModeEnvioLog)));

        //    cbTerminalModoColores.Properties.Items.Clear();
        //    cbTerminalModoColores.Properties.Items.AddRange(Enum.GetNames(typeof(ModosVehiculoEstadoColores)));

        //    cbModoConexionInternet.Properties.Items.Clear();
        //    cbModoConexionInternet.Properties.Items.AddRange(Enum.GetNames(typeof(ModoConexionInternet)));

        //    cbUserMonitorTimeout.Properties.Items.Clear();
        //    cbUserMonitorTimeout.Properties.Items.AddRange(Enum.GetNames(typeof(PowerMonitorTimeout)));
        //}

        private void TerminalOptionForm_Load(object sender, EventArgs e)
        {

        }

        private void cmdCrearTemplate_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("� CREAR PLANTILLA DE CONFIGURACI�N DESDE ESTA CONFIGURACI�N ?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            //    return;

            ////Serializamos los cambios, y obtenemos un string.
            //try
            //{
            //    //Obtenemos solo los cambios, que ha habido.
            //    var hsTemplate = SaveValueControls();
            //    if (hsTemplate == null)
            //        return;

            //    RemoveDataElemNoShared(hsTemplate);

            //    var _newTemplate = new TemplateConfiguration();

            //    _newTemplate.Nombre = string.Format("Plantilla Terminal {0}", _hashdata[Core.Config.Constantes.ModemIMEI]);
            //    _newTemplate.ConfigData = hsTemplate.Serialize();
            //    _newTemplate.Comentario = "";
            //    _newTemplate.Save();

            //    MessageBox.Show(string.Format("Plantilla Generada Correctamente. [{0}]", _newTemplate.Nombre), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //    Close();
            //}
            //catch (System.Exception ex)
            //{
            //    ManejarExceptions.ManejarException(new System.Exception("ERROR PRODUCIDO AL CREAR PLANTILLA DE CONFIGURACI�N.", ex));
            //}
        }

        //private void ckComidaActivo_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ckComidaActivo.Checked)
        //    {
        //        ckComidaAutoDesactivar.Enabled = true;
        //        ckComidaAutoDesactivar_CheckedChanged(null, null);
        //    }
        //    else
        //    {
        //        ckComidaAutoDesactivar.Enabled = false;
        //        ckComidaAutoDesactivar.Checked = false;
        //        ckComidaAutoDesactivar_CheckedChanged(null, null);
        //    }
        //}

        //private void ckComidaAutoDesactivar_CheckedChanged(object sender, EventArgs e)
        //{
        //    tbcComidaDesactivarDespuesDe.Enabled = ckComidaAutoDesactivar.Checked;
        //}
    }
    
}
