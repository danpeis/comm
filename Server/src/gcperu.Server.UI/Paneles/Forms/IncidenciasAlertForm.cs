﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using gcperu.Server.Core.DAL;
using gitspt.global.Core.Exceptions;
using gitspt.global.Core.Extension;
using GCPeru.Server.Core.Settings;
using ManejarExceptions = gcperu.Server.UI.Exception.ManejarExceptions;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class IncidenciasAlertForm : DevExpress.XtraEditors.XtraForm
    {
        private Core.DAL.GC.IncidenciasDescripcionCol _lista;

        public IncidenciasAlertForm()
        {
            InitializeComponent();
        }

        private void IncidenciasAlertForm_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            _lista = new Core.DAL.GC.IncidenciasDescripcionCol();

            _lista.LoadAll();
            RellenarLista();
        }

        private void RellenarLista()
        {
            listIncid.Items.Clear();

            foreach (int p in Enum.GetValues(typeof(Core.ServerIncidenceTypes)))
            {
                if (p == 0) continue;
                listIncid.Items.Add(new InfoIncidencia(p, p.NumToEnum(Core.ServerIncidenceTypes.Undefined).ToString()), (ModuleConfig.AppConfig.Server.ThrowIncendeBy & p) == p);
            }
        }

        private void cmdAceptar_Click(object sender, EventArgs e)
        {
            int IncActivas = 0;

            foreach (CheckedListBoxItem item in listIncid.Items)
            {
                if (item.CheckState == CheckState.Checked)
                    IncActivas += (item.Value as InfoIncidencia).ID;
            }

            try
            {
                ModuleConfig.AppConfig.Server.ThrowIncendeBy = IncActivas;
                ModuleConfig.AppConfig.Manager.Save(ModuleConfig.AppConfig);

                this.Close();
            }
            catch (System.Exception ex)
            {       
                ManejarExceptions.ManejarException(ex,"",ExceptionErrorLevel.Error,false,true);
            }
        }
    }

    class InfoIncidencia
    {
        public string Nombre { get; set; }
        public int ID { get; set; }

        public InfoIncidencia(int id, string nombre)
        {
            Nombre = nombre;
            ID = id;
        }

        public override string ToString()
        {
            return Nombre;
        }
    }

}