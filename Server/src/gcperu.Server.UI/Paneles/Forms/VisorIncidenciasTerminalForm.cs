﻿using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class VisorIncidenciasTerminalForm : DevExpress.XtraEditors.XtraForm
    {

        private TerminalStateIncidenceCol _listaIncid;

        public VisorIncidenciasTerminalForm(TerminalState terminalState)
        {
            InitializeComponent();

            try
            {
                frMain.Text = string.Format(frMain.Text, terminalState.VehiculoMatricula, terminalState.TerminalIMEI);

                _listaIncid=new TerminalStateIncidenceCol();
                _listaIncid.Query.Where(_listaIncid.Query.TerminalIMEI == terminalState.TerminalIMEI);
                if (_listaIncid.Count==0)
                    _listaIncid.Query.Where(_listaIncid.Query.TerminalID == terminalState.TerminalID);

                _listaIncid.LoadAll();
            
                bsTerm.DataSource = _listaIncid;
            }
            catch (System.Exception e)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL OBTENER LAS INCIDENCIAS ASOCIADAS AL TERMINAL.",e));
            }
            
        }
    }
}