﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EntitySpaces.Interfaces;
using System.Linq;
using gcperu.Server.Core;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class TemplateConfigurationAssignForm : DevExpress.XtraEditors.XtraForm
    {
        private List<decimal> _lista;
        private TemplateConfigurationCol _listaTemplates;
        private ISSTraceLog _log;

        public bool Result { get; private set; }

        public TemplateConfigurationAssignForm(List<decimal> listaTerminalIDs)
        {
            InitializeComponent();

            _log = Ioc.Container.Get<ISSTraceLog>();
            _lista = listaTerminalIDs;

            this.CenterToScreen();

            LoadTemplates();
            RellenarTemplates();

            cbTemplates.Focus();
        }

        private void LoadTemplates()
        {
            try
            {
                _listaTemplates = new TemplateConfigurationCol();
                _listaTemplates.LoadAll();
            }
            catch (System.Exception e)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL OBTENER LA LISTA DE PLANTILLAS.", e));
            }
        }

        private void RellenarTemplates()
        {
            cbTemplates.Enabled = true;
            cbTemplates.Properties.Items.Clear();
            
            foreach (var template in _listaTemplates)
            {
                cbTemplates.Properties.Items.Add(new TemplateInfo(){Codigo=template.Id,Nombre=template.Nombre,Defecto = template.Predeterminada});    
            }
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            if (cbTemplates.EditValue==null || cbTemplates.EditValue.ToString()=="")
            {
                MessageBox.Show("DEBE ESPECIFICAR UNA PLANTILLA A APLICAR.", "Aviso", MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }

            var _terminalCol = new TerminalStateCol();
            try
            {
                //Obtener la lista de los Terminales.
                _terminalCol.Query.Where(_terminalCol.Query.TerminalID.In(_lista));

                _terminalCol.LoadAll();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("Error al obtener la lista de terminales para Aplicar la lista plantilla de configuración.",ex));
                return;
            }

            if (MessageBox.Show("¿ APLICAR Y ENVIAR ESTA CONFIGURACIÓN A LOS TERMINALES SELECCIONADOS ?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            //Obtener la Plantilla Seleccionada para Asignarsela a los Terminales y Guardarla.
            var tempApply = GetTemplateSelected();
            if (tempApply==null)
            {
                MessageBox.Show("LA PLANTILLA SELECCIONADA NO SE PUEDE OBTENER O NO ES VÁLIDA", "Aviso", MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }

            //Aplicamos la Plantilla y la Enviamos a los Terminales la nueva configuracion en BD.

            try
            {
                using (var tx = new esTransactionScope())
                {
                    foreach (var vehid in _lista)
                    {
                        //Creamos la Peticion al Terminal.
                        var ts = TerminalState.GetByVehiculoID(vehid);
                        if (ts==null)
                        {
                            throw new System.Exception(string.Format("NO SE PUEDE OBTENER EL REGISTRO [TerminalState] PARA EL VEHICULO ID [{0}]", vehid));
                        }

                        ts.TemplateConfigID = tempApply.Id;
                        ts.Save();

                        if (!TerminalDataSend.CreatePeticionSend(vehid, PeticionCodeSend.ConfiguracionSet, 1, tempApply.ConfigData,tempApply.Id.ToString(),_log))
                        {
                            throw new System.Exception(string.Format("ERROR CREAR PETICION DE ENVIO PARA EL VEHICULO ID [{0}]", vehid));
                        }

                        if (!TerminalDataSend.CreatePeticionSend(vehid, PeticionCodeSend.ConfiguracionGet, 1,"","",_log))
                        {
                            throw new System.Exception(string.Format("ERROR CREAR PETICION DE ENVIO PARA EL VEHICULO ID [{0}]", vehid));
                        }
                    }

                    tx.Complete();
                }

                this.DialogResult=DialogResult.OK;
                Close();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new System.Exception("ERROR AL APLICAR Y ENVIAR LA PLANTILLA DE CONFIGURACIÓN.", ex));
            }

        }

        private TemplateConfiguration GetTemplateSelected()
        {
            if (cbTemplates.EditValue == null || cbTemplates.EditValue.ToString() == "")
                return null;

            var tempInfo = cbTemplates.EditValue as TemplateInfo;
            if (tempInfo==null)
                return null;

            return _listaTemplates.FirstOrDefault(t => t.Nombre == tempInfo.Nombre);

        }


        private class TemplateInfo
        {
            public int Codigo { get; set; }
            public string Nombre { get; set; }

            public bool Defecto { get; set; }

            public override string ToString()
            {
                return string.Format("{0} - {1} (Defecto:{2})", Codigo, Nombre,Defecto);
            }
        }
      
    }

    
}