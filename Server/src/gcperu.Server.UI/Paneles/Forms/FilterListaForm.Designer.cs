﻿namespace gcperu.Server.UI.Paneles.Forms
{
    partial class FilterListaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.Columns.GridColumn ID;
            this.dbgFilter = new DevExpress.XtraGrid.GridControl();
            this.bsTerm = new System.Windows.Forms.BindingSource(this.components);
            this.viewFilters = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNombre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.codFilter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpMmDesc = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cmdOK = new DevExpress.XtraEditors.SimpleButton();
            ID = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dbgFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpMmDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ID
            // 
            ID.Caption = "ID";
            ID.FieldName = "Id";
            ID.Name = "ID";
            ID.OptionsColumn.AllowEdit = false;
            ID.Visible = true;
            ID.VisibleIndex = 0;
            ID.Width = 57;
            // 
            // dbgFilter
            // 
            this.dbgFilter.DataSource = this.bsTerm;
            this.dbgFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbgFilter.Location = new System.Drawing.Point(0, 0);
            this.dbgFilter.MainView = this.viewFilters;
            this.dbgFilter.Name = "dbgFilter";
            this.dbgFilter.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpMmDesc});
            this.dbgFilter.Size = new System.Drawing.Size(903, 504);
            this.dbgFilter.TabIndex = 1;
            this.dbgFilter.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewFilters});
            // 
            // viewFilters
            // 
            this.viewFilters.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gray;
            this.viewFilters.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Gray;
            this.viewFilters.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewFilters.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewFilters.Appearance.FocusedRow.Options.UseFont = true;
            this.viewFilters.Appearance.TopNewRow.BackColor = System.Drawing.Color.PapayaWhip;
            this.viewFilters.Appearance.TopNewRow.Options.UseBackColor = true;
            this.viewFilters.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            ID,
            this.colNombre,
            this.codFilter,
            this.colDesc});
            this.viewFilters.GridControl = this.dbgFilter;
            this.viewFilters.Name = "viewFilters";
            this.viewFilters.NewItemRowText = "Click aqui para agregar un nuevo grupo";
            this.viewFilters.OptionsCustomization.AllowRowSizing = true;
            this.viewFilters.OptionsDetail.EnableMasterViewMode = false;
            this.viewFilters.OptionsView.RowAutoHeight = true;
            this.viewFilters.OptionsView.ShowDetailButtons = false;
            this.viewFilters.OptionsView.ShowFooter = true;
            this.viewFilters.OptionsView.ShowGroupPanel = false;
            this.viewFilters.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(ID, DevExpress.Data.ColumnSortOrder.Descending)});
            this.viewFilters.KeyDown += new System.Windows.Forms.KeyEventHandler(this.viewFilters_KeyDown);
            // 
            // colNombre
            // 
            this.colNombre.Caption = "Nombre";
            this.colNombre.FieldName = "Nombre";
            this.colNombre.Name = "colNombre";
            this.colNombre.ToolTip = "Indica que este grupo pertenece a terminales que estan activos, y no son para pru" +
                "ebas o demos";
            this.colNombre.Visible = true;
            this.colNombre.VisibleIndex = 1;
            this.colNombre.Width = 230;
            // 
            // codFilter
            // 
            this.codFilter.Caption = "Filtro";
            this.codFilter.ColumnEdit = this.rpMmDesc;
            this.codFilter.FieldName = "Filter";
            this.codFilter.Name = "codFilter";
            this.codFilter.Visible = true;
            this.codFilter.VisibleIndex = 3;
            this.codFilter.Width = 369;
            // 
            // rpMmDesc
            // 
            this.rpMmDesc.Name = "rpMmDesc";
            // 
            // colDesc
            // 
            this.colDesc.AppearanceCell.Options.UseTextOptions = true;
            this.colDesc.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDesc.Caption = "Función / Utilidad";
            this.colDesc.ColumnEdit = this.rpMmDesc;
            this.colDesc.FieldName = "Descripcion";
            this.colDesc.Name = "colDesc";
            this.colDesc.Visible = true;
            this.colDesc.VisibleIndex = 2;
            this.colDesc.Width = 240;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cmdOK);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 504);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(903, 34);
            this.panelControl1.TabIndex = 2;
            // 
            // cmdOK
            // 
            this.cmdOK.Image = global::gcperu.Server.UI.Properties.Resources.Select16;
            this.cmdOK.Location = new System.Drawing.Point(739, 5);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(152, 23);
            this.cmdOK.TabIndex = 0;
            this.cmdOK.Text = "Aceptar";
            this.cmdOK.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // FilterListaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 538);
            this.Controls.Add(this.dbgFilter);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FilterListaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lista de Filtros Guardados";
            ((System.ComponentModel.ISupportInitialize)(this.dbgFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpMmDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl dbgFilter;
        private DevExpress.XtraGrid.Views.Grid.GridView viewFilters;
        private DevExpress.XtraGrid.Columns.GridColumn colDesc;
        private System.Windows.Forms.BindingSource bsTerm;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton cmdOK;
        private DevExpress.XtraGrid.Columns.GridColumn colNombre;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit rpMmDesc;
        private DevExpress.XtraGrid.Columns.GridColumn codFilter;
    }
}