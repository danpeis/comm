﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class VisoConexionesTerminalForm : DevExpress.XtraEditors.XtraForm
    {

        private HistoricoConexionesCol _listaConex;

        public VisoConexionesTerminalForm(TerminalState terminalState)
        {
            InitializeComponent();

            try
            {
                frMain.Text = string.Format(frMain.Text, terminalState.VehiculoMatricula, terminalState.TerminalIMEI);

                _listaConex = new HistoricoConexionesCol();
                _listaConex.Query.Where(_listaConex.Query.TerminalID == terminalState.TerminalID);

                _listaConex.LoadAll();
            
                bsTerm.DataSource = _listaConex;
            }
            catch (System.Exception e)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL OBTENER LAS CONEXIONES ASOCIADAS AL TERMINAL.",e));
            }
            
        }
    }
}