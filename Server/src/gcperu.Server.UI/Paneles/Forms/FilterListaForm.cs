﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;
using GCPeru.Server.Core.Settings;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class FilterListaForm : DevExpress.XtraEditors.XtraForm
    {

        private bool _changedFilters=false;

        public FilterListaForm()
        {
            InitializeComponent();

            ObtenerFiltros();
        }

        private void ObtenerFiltros()
        {
            try
            {
                bsTerm.DataSource = ModuleConfig.UserConfig.ConsoleFiltersNamed;
            }
            catch (System.Exception e)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL OBTENER LOS FILTROS DEFINIDOS.", e));
            }
        }

        private void viewFilters_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Delete)
            {
                if (MessageBox.Show("¿Eliminar FILTRO de la lista?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return;

                try
                {
                    //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
                    foreach (int rowhandle in viewFilters.GetSelectedRows())
                    {
                        var ts = bsTerm[viewFilters.GetDataSourceRowIndex(rowhandle)] as FilterNamedInfo;
                        if (ts == null) continue;

                        bsTerm.RemoveCurrent();
                        _changedFilters = true;
                    }

                    ModuleConfig.UserConfig.Manager.Save(ModuleConfig.UserConfig);
                }
                catch (System.Exception ex)
                {
                    ManejarExceptions.ManejarException(new GCException("ERROR ELIMINAR FILTRO DE LA LISTA.", ex));
                }
            }
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            try
            {
                ModuleConfig.UserConfig.Manager.Save(ModuleConfig.UserConfig);

                if (_changedFilters)
                    this.DialogResult=DialogResult.OK;
                else
                    this.DialogResult = DialogResult.None;

                this.Close();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL GUARDAR LOS CAMBIOS EN LA LISTA DE FILTROS.", ex));
            }
        }
    }
}