using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using gcperu.Server.UI.Exception;
using gitspt.global.Core.Log;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class ChangeGlobalOptionForm : DevExpress.XtraEditors.XtraForm
    {
        private Contract.Config.TerminalConfig _config;
        private Contract.Config.TerminalConfig _hashNew;
        private Core.DAL.GC.TerminalState _ts;
        private bool _editable;

        private List<decimal> _listaVehiculos;

        public ChangeGlobalOptionForm(List<decimal> listavehiculs, Contract.Config.TerminalConfig config)
        {
            InitializeComponent();

            _editable = true;
            _listaVehiculos = listavehiculs;
            _config = config;

            RellenarCombosEnum();
            AssigLimitsValues();
            RellenarControles();

            cmdOK.Visible = _editable;
            ckConfigFull.Visible = _editable;

            if (_editable)
            {
                EnabledAllControls();
            }
        }

        private void AssigLimitsValues()
        {
            //TODO: ChangeGlobalOptionForm:AssigLimitsValues.  Obtener el Rango de valores permitidos de los Attributos de la clase
            //tbcTerminalMaxWaitRsp.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYMAXTIMEOUTWAITRSP.ToInt();
            //tbcTerminalMaxWaitRsp.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYMAXTIMEOUTWAITRSP.ToInt();

            //tcbTerminalTxDelayOff.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYENGINEOFF.ToInt();
            //tcbTerminalTxDelayOff.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYENGINEOFF.ToInt();

            //tbcTerminalMinWaitRsp.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYYMINTIMEOUTWAITRSP.ToInt();
            //tbcTerminalMinWaitRsp.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYYMINTIMEOUTWAITRSP.ToInt();

            //tcbTerminalIntervalSendCola.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYSENDCOLASALIDA.ToInt();
            //tcbTerminalIntervalSendCola.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYSENDCOLASALIDA.ToInt();


        }

        private void RellenarControles()
        {
            DateTime dateValue;
            
            //Terminal
            teTerminalMaxDistancia.Text =  _config.Behavior.Odometer.DistanceMaxBeetweenConsecutivePoints.ToString();
            tbcTerminalUmbralTempParada.Value = _config.Behavior.General.ThresholdStopModeTime;
            tbcTerminalUmbralVelocidadParada.Value =  _config.Behavior.General.ThresholdStopModeSpeed;
            tcbTerminalTxDelayOff.Value = _config.Behavior.Frames.FrecuencyBuildStateFrameModeStopTime;
            tbcTerminalTxDelayOn.Value =  _config.Behavior.Frames.FrecuencyBuildStateFrameModeMotionTime;


            tbcTerminalMaxWaitRsp.Value = _config.Behavior.Comms.TimeoutMaxAck;
            tbcTerminalMinWaitRsp.Value = _config.Behavior.Comms.TimeoutMinAck;
            tcbTerminalIntervalSendCola.Value = _config.Behavior.Frames.FrecuencyShippingTime;
            teDaysForReboot.Text = _config.Behavior.General.ForceRebootSystemDays.ToString();
            teDaysForShutDown.Text = _config.Behavior.General.ForceRebootSystemDays.ToString();
            //Energia
            cbUserMonitorTimeout.Text = _config.Behavior.General.ScreenSaverTime.ToString();

            //FTP
            //_config.
            //todo: ChangeGlobalOptionForm. FTP fillvalues
            //teFtpPort.Text = _config[Core.Config.Constantes.FTPPort].ToString();
            //teFtpRemoteHost.Text = _config[Core.Config.Constantes.FTPTerminalRemoteHost].ToString();
            //if (DateTime.TryParse(_config[Core.Config.Constantes.FTPTerminalLastDateLogSend].ToString(), out dateValue))
            //    deFtpLastDateLog.DateTime = dateValue;

            cbFtpModoEnvioLog.Text = _config.LogShipping.ModeShipping.ToString();

            //Trace
            cbTraceLevel.Text = _config.Behavior.Logging.DefaultLevelLog.ToString();
            teTracePFileName.Text = _config.Behavior.Logging.LogFile;
            cbTracePFileRotate.Text = _config.Behavior.Logging.FileRotationMode.ToString();

            //Conexion
            cbConexionDefaultIP.Text = _config.Behavior.Endpoint.FirstEndpoint.ToString();
            teConexionIntentos.Text =  _config.Behavior.Endpoint.RetryNumberByEndpoint.ToString();

            var cnx = _config.EndPoins.FirstOrDefault(it => it.Id == 1);
            if (cnx != null)
            {
                teConexionServerName1.Text = cnx.Name;
                teConexionServerIP1.Text = cnx.Url;
            }

            cnx = _config.EndPoins.FirstOrDefault(it => it.Id == 2);
            if (cnx != null)
            {
                teConexionServerName2.Text = cnx.Name;
                teConexionServerIP2.Text = cnx.Url;
            }


            cnx = _config.EndPoins.FirstOrDefault(it => it.Id == 3);
            if (cnx != null)
            {
                teConexionServerName3.Text = cnx.Name;
                teConexionServerIP3.Text = cnx.Url;
            }
        }

        private void SaveValueControls()
        {
            //todo: ChangeGlobalOptionForm. FTP Savevaluecontrols
            //var hdnew = new HashTableTrama();// Global.Device.Config.HashData;

            //try
            //{
            //    //Terminal
            //    hdnew[Core.Config.Constantes.TerminalODOMETROMAXDISTANCIA2PUNTOS] = teTerminalMaxDistancia.Text.ToInt();
            //    hdnew[Core.Config.Constantes.TerminalPARADATIEMPOVELOCIDADPORDEBAJO] = tbcTerminalUmbralTempParada.Value;
            //    hdnew[Core.Config.Constantes.TerminalPARADAUMBRALVELOCIDAD] = tbcTerminalUmbralVelocidadParada.Value;
            //    hdnew[Core.Config.Constantes.TerminalSleepLogout] = tbcTerminalSleepLogout.Value;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYENGINEOFF] = tcbTerminalTxDelayOff.Value;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYENGINEON]=tbcTerminalTxDelayOn.Value;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYMAXTIMEOUTWAITRSP] = tbcTerminalMaxWaitRsp.Value;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYYMINTIMEOUTWAITRSP] = tbcTerminalMinWaitRsp.Value;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYSENDCOLASALIDA] = tcbTerminalIntervalSendCola.Value;
            //    hdnew[Core.Config.Constantes.TerminalDaysForReboot] = teDaysForReboot.Text.ToInt();
            //    hdnew[Core.Config.Constantes.TerminalModoVehiculoEstadoColores] = cbTerminalModoColores.Text;
            //    hdnew[Core.Config.Constantes.TerminalMonitorTurnoffTimeout] = cbUserMonitorTimeout.Text;
            //    hdnew[Core.Config.Constantes.TerminalMaxDaysToShutDown] = teDaysForShutDown.Text;

            //    //FTP
            //    hdnew[Core.Config.Constantes.FTPPort] = teFtpPort.Text.ToInt();
            //    hdnew[Core.Config.Constantes.FTPTerminalRemoteHost] = teFtpRemoteHost.Text;
            //    hdnew[Core.Config.Constantes.FTPTerminalLastDateLogSend] = deFtpLastDateLog.DateTime;
            //    hdnew[Core.Config.Constantes.FTPTerminalMODOEnvioLOG] =cbFtpModoEnvioLog.Text;

            //    //Trace
            //    hdnew[Core.Config.Constantes.TraceLevel] =cbTraceLevel.Text;
            //    hdnew[Core.Config.Constantes.TracePFileMaxSize]=teTracePFileMaxSize.Text;
            //    hdnew[Core.Config.Constantes.TracePFileName]=teTracePFileName.Text ;
            //    hdnew[Core.Config.Constantes.TracePFileRotateMode] =cbTracePFileRotate.Text;
            //    hdnew[Core.Config.Constantes.TraceProtocol] =cbTraceProtocol.Text;

            //    //Conexion
            //    hdnew[Core.Config.Constantes.ModemSERVERDefaultIP]=cbConexionDefaultIP.Text.ToInt();
            //    hdnew[Core.Config.Constantes.ModemSERVERNumConexIntentos]=teConexionIntentos.Text.ToInt();
            //    hdnew[Core.Config.Constantes.ModemSERVERNAME1]=teConexionServerName1.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERIP1]=teConexionServerIP1.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERPORT1]=teConexionServerPort1.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERNAME2]=teConexionServerName2.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERIP2]=teConexionServerIP2.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERPORT2]=teConexionServerPort2.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERNAME3]=teConexionServerName3.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERIP3]=teConexionServerIP3.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERPORT3]=teConexionServerPort3.Text;

            //    return hdnew;

            //}
            //catch (System.Exception ex)
            //{
            //    TraceLog.LogException(new GCException("SaveValueControls ERROR. LA CONFIGURACION NO SE HA GUARDADO.", ex, this.GetType()));
            //    return null;
            //}
        }

        private void EnabledAllControls()
        {
   
            gcTerminal.Enabled = true;
            gcConexion.Enabled = true;
            gcFtp.Enabled = true;
            gcTrace.Enabled = true;

        }

       

        #region eventos

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            if (_editable)
            {
                if (MessageBox.Show("¿ CERRAR CONIGURACION SIN GUARDAR Y ENVIAR AL TERMINAL?","Pregunta",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2)==DialogResult.No)
                    return;

            }
            this.Close();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            //todo: ChangeGlobalOptionForm. cmdOK_Click
            //if (MessageBox.Show("¿ ENVIAR ESTA CONFIGURACIÓN A LOS TERMINALES SELECCIONADOS?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            //    return;

            ////Obtenemos solo los cambios, que ha habido.
            //var hsnewchanges = SaveValueControls();
            //var hssend = GetHashChanges(hsnewchanges);
            //if (hssend==null)
            //{
            //    MessageBox.Show("NO HA HABIDO CAMBIOS EN LA CONFIGURACIÓN O OCURRIÓ ALGUN ERROR. IMPOSIBLE ENVIAR NADA A LOS TERMINALES","Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}

            ////Serializamos los cambios, y obtenemos un string.
            //string xmlsend = hssend.Serialize();
            //string xmlsave = hsnewchanges.Serialize();

            ////Guardamos la nueva configuracion en BD.
            //int counterrs = 0;

            //foreach (var vehid in _listaVehiculos)
            //{
            //     //Creamos la Peticion al Terminal.
            //    if (!TerminalDataSend.CreatePeticionSend(vehid, "", PeticionCodeSend.ConfiguracionSet, 1, xmlsend, ""))
            //    {
            //        counterrs+=1;
            //    }
            //}

            //if (counterrs>0)
            //{
            //    MessageBox.Show("NO SE PUDÓ ENVIAR LA CONFIGURACIÓN PARA {0} TERMINALES.", "Aviso", MessageBoxButtons.OK,
            //                       MessageBoxIcon.Error);
            //    Close();
            //}
            //else
            //{
            //    MessageBox.Show("LAS PETICIONES DE ENVIO SE CREARON CORRECTAMENTE.", "Aviso", MessageBoxButtons.OK,MessageBoxIcon.Information);

            //    Close();
            //}
        }

        #endregion

        private void RellenarCombosEnum()
        {

            cbTraceLevel.Properties.Items.Clear();
            cbTraceLevel.Properties.Items.AddRange(Enum.GetNames(typeof(gcperu.Contract.LevelLog)));

            cbTracePFileRotate.Properties.Items.Clear();
            cbTracePFileRotate.Properties.Items.AddRange(Enum.GetNames(typeof(TraceFileRotateModeEnum)));

            cbFtpModoEnvioLog.Properties.Items.Clear();
            cbFtpModoEnvioLog.Properties.Items.AddRange(Enum.GetNames(typeof(gcperu.Contract.LogModeShipping)));
        }

        private void TerminalOptionForm_Load(object sender, EventArgs e)
        {

        }
    }
    

    
}
