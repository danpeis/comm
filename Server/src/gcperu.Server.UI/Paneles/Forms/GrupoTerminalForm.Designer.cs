﻿namespace gcperu.Server.UI.Paneles.Forms
{
    partial class GrupoTerminalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.Columns.GridColumn ID;
            this.dbgIncid = new DevExpress.XtraGrid.GridControl();
            this.bsTerm = new System.Windows.Forms.BindingSource(this.components);
            this.viewGrupos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colShowEnTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TerminalDisconnect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cmdOk = new DevExpress.XtraEditors.SimpleButton();
            ID = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dbgIncid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewGrupos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ID
            // 
            ID.Caption = "ID";
            ID.FieldName = "ID";
            ID.Name = "ID";
            ID.OptionsColumn.AllowEdit = false;
            ID.Visible = true;
            ID.VisibleIndex = 0;
            ID.Width = 45;
            // 
            // dbgIncid
            // 
            this.dbgIncid.DataSource = this.bsTerm;
            this.dbgIncid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbgIncid.Location = new System.Drawing.Point(0, 0);
            this.dbgIncid.MainView = this.viewGrupos;
            this.dbgIncid.Name = "dbgIncid";
            this.dbgIncid.Size = new System.Drawing.Size(566, 350);
            this.dbgIncid.TabIndex = 1;
            this.dbgIncid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewGrupos});
            // 
            // viewGrupos
            // 
            this.viewGrupos.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gray;
            this.viewGrupos.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Gray;
            this.viewGrupos.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewGrupos.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewGrupos.Appearance.FocusedRow.Options.UseFont = true;
            this.viewGrupos.Appearance.TopNewRow.BackColor = System.Drawing.Color.PapayaWhip;
            this.viewGrupos.Appearance.TopNewRow.Options.UseBackColor = true;
            this.viewGrupos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            ID,
            this.colShowEnTotal,
            this.TerminalDisconnect});
            this.viewGrupos.GridControl = this.dbgIncid;
            this.viewGrupos.Name = "viewGrupos";
            this.viewGrupos.NewItemRowText = "Click aqui para agregar un nuevo grupo";
            this.viewGrupos.OptionsDetail.EnableMasterViewMode = false;
            this.viewGrupos.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.viewGrupos.OptionsView.ShowDetailButtons = false;
            this.viewGrupos.OptionsView.ShowFooter = true;
            this.viewGrupos.OptionsView.ShowGroupPanel = false;
            this.viewGrupos.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(ID, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colShowEnTotal
            // 
            this.colShowEnTotal.Caption = "En Activo";
            this.colShowEnTotal.FieldName = "EnActivo";
            this.colShowEnTotal.Name = "colShowEnTotal";
            this.colShowEnTotal.ToolTip = "Indica que este grupo pertenece a terminales que estan activos, y no son para pru" +
                "ebas o demos";
            this.colShowEnTotal.Visible = true;
            this.colShowEnTotal.VisibleIndex = 1;
            this.colShowEnTotal.Width = 117;
            // 
            // TerminalDisconnect
            // 
            this.TerminalDisconnect.Caption = "Nombre del Grupo";
            this.TerminalDisconnect.FieldName = "Nombre";
            this.TerminalDisconnect.Name = "TerminalDisconnect";
            this.TerminalDisconnect.Visible = true;
            this.TerminalDisconnect.VisibleIndex = 2;
            this.TerminalDisconnect.Width = 383;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cmdOk);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 350);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(566, 34);
            this.panelControl1.TabIndex = 2;
            // 
            // cmdOk
            // 
            this.cmdOk.Image = global::gcperu.Server.UI.Properties.Resources.Select16;
            this.cmdOk.Location = new System.Drawing.Point(390, 6);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(152, 23);
            this.cmdOk.TabIndex = 0;
            this.cmdOk.Text = "Guardar";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // GrupoTerminalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 384);
            this.Controls.Add(this.dbgIncid);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GrupoTerminalForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grupos de Terminales";
            ((System.ComponentModel.ISupportInitialize)(this.dbgIncid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewGrupos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl dbgIncid;
        private DevExpress.XtraGrid.Views.Grid.GridView viewGrupos;
        private DevExpress.XtraGrid.Columns.GridColumn TerminalDisconnect;
        private System.Windows.Forms.BindingSource bsTerm;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton cmdOk;
        private DevExpress.XtraGrid.Columns.GridColumn colShowEnTotal;
    }
}