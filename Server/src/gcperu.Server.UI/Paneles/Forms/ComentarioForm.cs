﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class ComentarioForm : DevExpress.XtraEditors.XtraForm
    {
        public string Comentario { get; set; }

        public ComentarioForm()
        {
            InitializeComponent();
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            Comentario = txtInfo.Text;
            this.Close();
        }

        private void ComentarioForm_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
            txtInfo.Text = Comentario;
        }
    }
}