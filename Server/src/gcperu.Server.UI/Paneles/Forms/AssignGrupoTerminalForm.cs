﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EntitySpaces.DynamicQuery;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class AssignGrupoTerminalForm : DevExpress.XtraEditors.XtraForm
    {
        private List<decimal> _lista;
        private GrupoTerminalCol _listaGrupos;

        public bool Result { get; private set; }

        public AssignGrupoTerminalForm(List<decimal> listaTerminalIDs)
        {
            InitializeComponent();
            _lista = listaTerminalIDs;

            ObtenerGrupos();

        }

        private void ObtenerGrupos()
        {
            try
            {
                _listaGrupos = new GrupoTerminalCol();
                _listaGrupos.Query.OrderBy(_listaGrupos.Query.Nombre, esOrderByDirection.Ascending);
                _listaGrupos.LoadAll();

                bsData.DataSource = _listaGrupos;

                cbGrupos.Enabled = true;
                foreach (var grupo in _listaGrupos)
                {
                    cbGrupos.Properties.Items.Add(grupo.Nombre);
                }

            }
            catch (System.Exception e)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL OBTENER LAS GRUPOS DE TERMINALES.", e));
            }
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            if (cbGrupos.EditValue == "")
            {
                MessageBox.Show("Seleccione el grupo a asignar a los Terminales.", "Aviso", MessageBoxButtons.OK,
                                MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                return;
            }

            var _terminalCol = new TerminalStateCol();
            try
            {
                //Obtener la lista de los Terminales.
                _terminalCol.Query.Where(_terminalCol.Query.TerminalID.In(_lista));

                _terminalCol.LoadAll();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("Error al obtener la lista de terminales para asignar los grupos.",ex));
                return;
            }

            try
            {
                //Asignamos el Grupo a los Terminales de la lista.
                var grupoTerminal = new GrupoTerminal();
                grupoTerminal.Query.Where(grupoTerminal.Query.Nombre == cbGrupos.EditValue);

                if (!grupoTerminal.Load(grupoTerminal.Query))
                {
                    MessageBox.Show("No se ha podido obtener el grupo para asignarlo.", "Error", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }

                foreach (var terminal in _terminalCol)
                {
                    terminal.GrupoID = (int)grupoTerminal.Id;
                }

                //Asignamos los Grupos a la lista.
                _terminalCol.Save();

                Result = true;
                this.Close();

            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("Error al asignar el grupo a la lista de Terminales.",ex));
                return;
            }


        }
      
    }
}