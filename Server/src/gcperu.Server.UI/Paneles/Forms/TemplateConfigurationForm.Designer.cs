using gcperu.Server.UI.Controls;

namespace gcperu.Server.UI.Paneles.Forms
{
    partial class TemplateConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.LabelControl labelControl7;
            DevExpress.XtraEditors.LabelControl labelControl8;
            DevExpress.XtraEditors.LabelControl labelControl9;
            DevExpress.XtraEditors.LabelControl labelControl10;
            this.tbControl = new DevExpress.XtraTab.XtraTabControl();
            this.tpControlAdmin = new DevExpress.XtraTab.XtraTabPage();
            this.tbAdmin = new DevExpress.XtraTab.XtraTabControl();
            this.tbpAdminTerm = new DevExpress.XtraTab.XtraTabPage();
            this.gcTerminal = new DevExpress.XtraEditors.PanelControl();
            this.tcbTerminalIntentosHotResetModem = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.tcbTerminalSinRecepcionGps = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.tcbTerminalMaxColaSalida = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtTerminalTxDelayOn = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.cbModoConexionInternet = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tcbTerminalSinConexionServidor = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tcbTerminalSinConexionInternet = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lbLasShutDown = new DevExpress.XtraEditors.LabelControl();
            this.teLastShutDown = new DevExpress.XtraEditors.TextEdit();
            this.lbDaysForShutDown = new DevExpress.XtraEditors.LabelControl();
            this.teDaysForShutDown = new DevExpress.XtraEditors.TextEdit();
            this.cbUserMonitorTimeout = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cbTerminalModoColores = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.teDaysForReboot = new DevExpress.XtraEditors.TextEdit();
            this.lbDaysForReboot = new DevExpress.XtraEditors.LabelControl();
            this.tcbTerminalIntervalSendCola = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcTerminalMinWaitRsp = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcTerminalMaxWaitRsp = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tcbTerminalTxDelayOff = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcTerminalSleepLogout = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcTerminalUmbralVelocidadParada = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.tbcTerminalUmbralTempParada = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.lbTerminalIntervalSendCola = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalMinWaitRsp = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalMaxWaitRsp = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalTxDelayOn = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalTxDelayOff = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalSleepLogout = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminal = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalUmbralTempParada = new DevExpress.XtraEditors.LabelControl();
            this.teTerminalMaxDistancia = new DevExpress.XtraEditors.TextEdit();
            this.lbTerminalMaxDistancia = new DevExpress.XtraEditors.LabelControl();
            this.tbpAdminCnx = new DevExpress.XtraTab.XtraTabPage();
            this.gcConexion = new DevExpress.XtraEditors.PanelControl();
            this.teConexionServerPort3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerPort2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerPort1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionIntentos = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionIntentos = new DevExpress.XtraEditors.LabelControl();
            this.cbConexionDefaultIP = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbConexionDefaultPositioIP = new DevExpress.XtraEditors.LabelControl();
            this.tbpAdminFtpTrace = new DevExpress.XtraTab.XtraTabPage();
            this.gcTrace = new DevExpress.XtraEditors.GroupControl();
            this.cbTraceProtocol = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTraceProtocol = new DevExpress.XtraEditors.LabelControl();
            this.cbTracePFileRotate = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTracePFileRotate = new DevExpress.XtraEditors.LabelControl();
            this.teTracePFileName = new DevExpress.XtraEditors.TextEdit();
            this.lbTracePFileName = new DevExpress.XtraEditors.LabelControl();
            this.teTracePFileMaxSize = new DevExpress.XtraEditors.TextEdit();
            this.lbTracePFileMaxSize = new DevExpress.XtraEditors.LabelControl();
            this.cbTraceLevel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTraceLevel = new DevExpress.XtraEditors.LabelControl();
            this.gcFtp = new DevExpress.XtraEditors.GroupControl();
            this.teUpdatePwd = new DevExpress.XtraEditors.TextEdit();
            this.teUpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.teUpdateRemoteHost = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.teUpdatePathRemote = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.teFtpPathRemote = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.teFtpPwd = new DevExpress.XtraEditors.TextEdit();
            this.teFtpUser = new DevExpress.XtraEditors.TextEdit();
            this.cbFtpModoEnvioLog = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbFtpModoEnvio = new DevExpress.XtraEditors.LabelControl();
            this.lbFtpLastDate = new DevExpress.XtraEditors.LabelControl();
            this.deFtpLastDateLog = new DevExpress.XtraEditors.DateEdit();
            this.teFtpRemoteHost = new DevExpress.XtraEditors.TextEdit();
            this.lbFtpRemoteHost = new DevExpress.XtraEditors.LabelControl();
            this.teFtpPort = new DevExpress.XtraEditors.TextEdit();
            this.lbFtpPort = new DevExpress.XtraEditors.LabelControl();
            this.tbpAdminVarios = new DevExpress.XtraTab.XtraTabPage();
            this.gcGeneral = new DevExpress.XtraEditors.PanelControl();
            this.ckNoFiltrarServiciosByLoginActivo = new DevExpress.XtraEditors.CheckEdit();
            this.ckSE_ShowMarker = new DevExpress.XtraEditors.CheckEdit();
            this.ckLoginAskActivityType = new DevExpress.XtraEditors.CheckEdit();
            this.ckCatastrofeActivo = new DevExpress.XtraEditors.CheckEdit();
            this.ckCambioVehiculoActivo = new DevExpress.XtraEditors.CheckEdit();
            this.ckMntoActivo = new DevExpress.XtraEditors.CheckEdit();
            this.frGenComida = new System.Windows.Forms.GroupBox();
            this.tbcComidaDesactivarDespuesDe = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.ckComidaAutoDesactivar = new DevExpress.XtraEditors.CheckEdit();
            this.ckComidaActivo = new DevExpress.XtraEditors.CheckEdit();
            this.tbpProfile = new DevExpress.XtraTab.XtraTabPage();
            this.gcPerfil = new DevExpress.XtraEditors.PanelControl();
            this.teProfileMobileForceUse = new DevExpress.XtraEditors.CheckEdit();
            this.teProfileMobileNumber = new DevExpress.XtraEditors.TextEdit();
            this.lbModemNumber = new DevExpress.XtraEditors.LabelControl();
            this.teProfileMobilePassword = new DevExpress.XtraEditors.TextEdit();
            this.lbModemPassword = new DevExpress.XtraEditors.LabelControl();
            this.teProfileMobileUserName = new DevExpress.XtraEditors.TextEdit();
            this.lbModemUserName = new DevExpress.XtraEditors.LabelControl();
            this.teProfileMobileName = new DevExpress.XtraEditors.TextEdit();
            this.lbModemName = new DevExpress.XtraEditors.LabelControl();
            this.teProfileMobileAPN = new DevExpress.XtraEditors.TextEdit();
            this.lbModemDomain = new DevExpress.XtraEditors.LabelControl();
            this.cmdCancel = new DevExpress.XtraEditors.SimpleButton();
            this.cmdOK = new DevExpress.XtraEditors.SimpleButton();
            this.txtNombre = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.txtComentario = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.ckDefault = new DevExpress.XtraEditors.CheckEdit();
            this.cbSoftwareTipo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            labelControl7 = new DevExpress.XtraEditors.LabelControl();
            labelControl8 = new DevExpress.XtraEditors.LabelControl();
            labelControl9 = new DevExpress.XtraEditors.LabelControl();
            labelControl10 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl)).BeginInit();
            this.tbControl.SuspendLayout();
            this.tpControlAdmin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAdmin)).BeginInit();
            this.tbAdmin.SuspendLayout();
            this.tbpAdminTerm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTerminal)).BeginInit();
            this.gcTerminal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntentosHotResetModem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntentosHotResetModem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinRecepcionGps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinRecepcionGps.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalMaxColaSalida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalMaxColaSalida.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTerminalTxDelayOn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModoConexionInternet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinConexionServidor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinConexionServidor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinConexionInternet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinConexionInternet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLastShutDown.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDaysForShutDown.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbUserMonitorTimeout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTerminalModoColores.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDaysForReboot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalMaxDistancia.Properties)).BeginInit();
            this.tbpAdminCnx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcConexion)).BeginInit();
            this.gcConexion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionIntentos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbConexionDefaultIP.Properties)).BeginInit();
            this.tbpAdminFtpTrace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).BeginInit();
            this.gcTrace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceProtocol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTracePFileRotate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileMaxSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFtp)).BeginInit();
            this.gcFtp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdatePwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdateRemoteHost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdatePathRemote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPathRemote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFtpModoEnvioLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpRemoteHost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPort.Properties)).BeginInit();
            this.tbpAdminVarios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcGeneral)).BeginInit();
            this.gcGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ckNoFiltrarServiciosByLoginActivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckSE_ShowMarker.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckLoginAskActivityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckCatastrofeActivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckCambioVehiculoActivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckMntoActivo.Properties)).BeginInit();
            this.frGenComida.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbcComidaDesactivarDespuesDe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcComidaDesactivarDespuesDe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckComidaAutoDesactivar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckComidaActivo.Properties)).BeginInit();
            this.tbpProfile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcPerfil)).BeginInit();
            this.gcPerfil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileForceUse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobilePassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileAPN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComentario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckDefault.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSoftwareTipo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl7
            // 
            labelControl7.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl7.Location = new System.Drawing.Point(623, 60);
            labelControl7.Name = "labelControl7";
            labelControl7.Size = new System.Drawing.Size(36, 16);
            labelControl7.TabIndex = 43;
            labelControl7.Text = "Clave";
            // 
            // labelControl8
            // 
            labelControl8.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl8.Location = new System.Drawing.Point(418, 60);
            labelControl8.Name = "labelControl8";
            labelControl8.Size = new System.Drawing.Size(48, 16);
            labelControl8.TabIndex = 41;
            labelControl8.Text = "Usuario";
            // 
            // labelControl9
            // 
            labelControl9.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl9.Location = new System.Drawing.Point(619, 151);
            labelControl9.Name = "labelControl9";
            labelControl9.Size = new System.Drawing.Size(36, 16);
            labelControl9.TabIndex = 59;
            labelControl9.Text = "Clave";
            // 
            // labelControl10
            // 
            labelControl10.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl10.Location = new System.Drawing.Point(414, 151);
            labelControl10.Name = "labelControl10";
            labelControl10.Size = new System.Drawing.Size(48, 16);
            labelControl10.TabIndex = 57;
            labelControl10.Text = "Usuario";
            // 
            // tbControl
            // 
            this.tbControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbControl.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbControl.AppearancePage.Header.Options.UseFont = true;
            this.tbControl.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbControl.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tbControl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbControl.Location = new System.Drawing.Point(8, 156);
            this.tbControl.Name = "tbControl";
            this.tbControl.SelectedTabPage = this.tpControlAdmin;
            this.tbControl.Size = new System.Drawing.Size(790, 568);
            this.tbControl.TabIndex = 4;
            this.tbControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpControlAdmin});
            // 
            // tpControlAdmin
            // 
            this.tpControlAdmin.AutoScroll = true;
            this.tpControlAdmin.AutoScrollMargin = new System.Drawing.Size(0, 20);
            this.tpControlAdmin.Controls.Add(this.tbAdmin);
            this.tpControlAdmin.Name = "tpControlAdmin";
            this.tpControlAdmin.Size = new System.Drawing.Size(784, 530);
            this.tpControlAdmin.Text = "Configuracion";
            // 
            // tbAdmin
            // 
            this.tbAdmin.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAdmin.AppearancePage.Header.Options.UseFont = true;
            this.tbAdmin.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAdmin.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbAdmin.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tbAdmin.AppearancePage.HeaderActive.Options.UseForeColor = true;
            this.tbAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbAdmin.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tbAdmin.Location = new System.Drawing.Point(0, 0);
            this.tbAdmin.Name = "tbAdmin";
            this.tbAdmin.PaintStyleName = "PropertyView";
            this.tbAdmin.SelectedTabPage = this.tbpAdminTerm;
            this.tbAdmin.Size = new System.Drawing.Size(784, 530);
            this.tbAdmin.TabIndex = 0;
            this.tbAdmin.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbpAdminTerm,
            this.tbpAdminCnx,
            this.tbpAdminFtpTrace,
            this.tbpAdminVarios,
            this.tbpProfile});
            // 
            // tbpAdminTerm
            // 
            this.tbpAdminTerm.Controls.Add(this.gcTerminal);
            this.tbpAdminTerm.Name = "tbpAdminTerm";
            this.tbpAdminTerm.Size = new System.Drawing.Size(782, 499);
            this.tbpAdminTerm.Text = "Terminal";
            // 
            // gcTerminal
            // 
            this.gcTerminal.Controls.Add(this.tcbTerminalIntentosHotResetModem);
            this.gcTerminal.Controls.Add(this.labelControl15);
            this.gcTerminal.Controls.Add(this.tcbTerminalSinRecepcionGps);
            this.gcTerminal.Controls.Add(this.labelControl16);
            this.gcTerminal.Controls.Add(this.tcbTerminalMaxColaSalida);
            this.gcTerminal.Controls.Add(this.labelControl14);
            this.gcTerminal.Controls.Add(this.txtTerminalTxDelayOn);
            this.gcTerminal.Controls.Add(this.labelControl6);
            this.gcTerminal.Controls.Add(this.cbModoConexionInternet);
            this.gcTerminal.Controls.Add(this.tcbTerminalSinConexionServidor);
            this.gcTerminal.Controls.Add(this.tcbTerminalSinConexionInternet);
            this.gcTerminal.Controls.Add(this.labelControl18);
            this.gcTerminal.Controls.Add(this.labelControl19);
            this.gcTerminal.Controls.Add(this.lbLasShutDown);
            this.gcTerminal.Controls.Add(this.teLastShutDown);
            this.gcTerminal.Controls.Add(this.lbDaysForShutDown);
            this.gcTerminal.Controls.Add(this.teDaysForShutDown);
            this.gcTerminal.Controls.Add(this.cbUserMonitorTimeout);
            this.gcTerminal.Controls.Add(this.labelControl2);
            this.gcTerminal.Controls.Add(this.cbTerminalModoColores);
            this.gcTerminal.Controls.Add(this.labelControl1);
            this.gcTerminal.Controls.Add(this.teDaysForReboot);
            this.gcTerminal.Controls.Add(this.lbDaysForReboot);
            this.gcTerminal.Controls.Add(this.tcbTerminalIntervalSendCola);
            this.gcTerminal.Controls.Add(this.tbcTerminalMinWaitRsp);
            this.gcTerminal.Controls.Add(this.tbcTerminalMaxWaitRsp);
            this.gcTerminal.Controls.Add(this.tcbTerminalTxDelayOff);
            this.gcTerminal.Controls.Add(this.tbcTerminalSleepLogout);
            this.gcTerminal.Controls.Add(this.tbcTerminalUmbralVelocidadParada);
            this.gcTerminal.Controls.Add(this.tbcTerminalUmbralTempParada);
            this.gcTerminal.Controls.Add(this.lbTerminalIntervalSendCola);
            this.gcTerminal.Controls.Add(this.lbTerminalMinWaitRsp);
            this.gcTerminal.Controls.Add(this.lbTerminalMaxWaitRsp);
            this.gcTerminal.Controls.Add(this.lbTerminalTxDelayOn);
            this.gcTerminal.Controls.Add(this.lbTerminalTxDelayOff);
            this.gcTerminal.Controls.Add(this.lbTerminalSleepLogout);
            this.gcTerminal.Controls.Add(this.lbTerminal);
            this.gcTerminal.Controls.Add(this.lbTerminalUmbralTempParada);
            this.gcTerminal.Controls.Add(this.teTerminalMaxDistancia);
            this.gcTerminal.Controls.Add(this.lbTerminalMaxDistancia);
            this.gcTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTerminal.Enabled = false;
            this.gcTerminal.Location = new System.Drawing.Point(0, 0);
            this.gcTerminal.Name = "gcTerminal";
            this.gcTerminal.Size = new System.Drawing.Size(782, 499);
            this.gcTerminal.TabIndex = 109;
            // 
            // tcbTerminalIntentosHotResetModem
            // 
            this.tcbTerminalIntentosHotResetModem.EditValue = 5;
            this.tcbTerminalIntentosHotResetModem.Location = new System.Drawing.Point(229, 330);
            this.tcbTerminalIntentosHotResetModem.Name = "tcbTerminalIntentosHotResetModem";
            this.tcbTerminalIntentosHotResetModem.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbTerminalIntentosHotResetModem.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalIntentosHotResetModem.Properties.Appearance.Options.UseFont = true;
            this.tcbTerminalIntentosHotResetModem.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalIntentosHotResetModem.Properties.LargeChange = 1;
            this.tcbTerminalIntentosHotResetModem.Properties.Maximum = 20;
            this.tcbTerminalIntentosHotResetModem.Size = new System.Drawing.Size(128, 45);
            this.tcbTerminalIntentosHotResetModem.TabIndex = 160;
            this.tcbTerminalIntentosHotResetModem.Value = 5;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Location = new System.Drawing.Point(52, 326);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(168, 37);
            this.labelControl15.TabIndex = 159;
            this.labelControl15.Text = "N� Intentos reset modem sin reinicio";
            // 
            // tcbTerminalSinRecepcionGps
            // 
            this.tcbTerminalSinRecepcionGps.EditValue = 60;
            this.tcbTerminalSinRecepcionGps.Location = new System.Drawing.Point(229, 424);
            this.tcbTerminalSinRecepcionGps.Name = "tcbTerminalSinRecepcionGps";
            this.tcbTerminalSinRecepcionGps.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbTerminalSinRecepcionGps.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalSinRecepcionGps.Properties.Appearance.Options.UseFont = true;
            this.tcbTerminalSinRecepcionGps.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalSinRecepcionGps.Properties.Maximum = 360;
            this.tcbTerminalSinRecepcionGps.Properties.TickFrequency = 30;
            this.tcbTerminalSinRecepcionGps.Size = new System.Drawing.Size(128, 45);
            this.tcbTerminalSinRecepcionGps.TabIndex = 158;
            this.tcbTerminalSinRecepcionGps.Value = 60;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.labelControl16.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Location = new System.Drawing.Point(17, 422);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(206, 33);
            this.labelControl16.TabIndex = 157;
            this.labelControl16.Text = "M�ximo Tiempo sin Recepci�n GPS";
            // 
            // tcbTerminalMaxColaSalida
            // 
            this.tcbTerminalMaxColaSalida.EditValue = 15;
            this.tcbTerminalMaxColaSalida.Location = new System.Drawing.Point(619, 377);
            this.tcbTerminalMaxColaSalida.Name = "tcbTerminalMaxColaSalida";
            this.tcbTerminalMaxColaSalida.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbTerminalMaxColaSalida.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalMaxColaSalida.Properties.Appearance.Options.UseFont = true;
            this.tcbTerminalMaxColaSalida.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalMaxColaSalida.Properties.Maximum = 30;
            this.tcbTerminalMaxColaSalida.Properties.Minimum = 5;
            this.tcbTerminalMaxColaSalida.Properties.SmallChange = 5;
            this.tcbTerminalMaxColaSalida.Properties.TickFrequency = 5;
            this.tcbTerminalMaxColaSalida.Size = new System.Drawing.Size(128, 45);
            this.tcbTerminalMaxColaSalida.TabIndex = 155;
            this.tcbTerminalMaxColaSalida.Value = 15;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.labelControl14.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Location = new System.Drawing.Point(380, 377);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(218, 34);
            this.labelControl14.TabIndex = 154;
            this.labelControl14.Text = "M�xima Capacidad Cola Salida (tramas)";
            // 
            // txtTerminalTxDelayOn
            // 
            this.txtTerminalTxDelayOn.EditValue = "0";
            this.txtTerminalTxDelayOn.Location = new System.Drawing.Point(649, 189);
            this.txtTerminalTxDelayOn.Name = "txtTerminalTxDelayOn";
            this.txtTerminalTxDelayOn.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTerminalTxDelayOn.Properties.Appearance.Options.UseFont = true;
            this.txtTerminalTxDelayOn.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTerminalTxDelayOn.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTerminalTxDelayOn.Properties.Mask.EditMask = "n0";
            this.txtTerminalTxDelayOn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTerminalTxDelayOn.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTerminalTxDelayOn.Size = new System.Drawing.Size(98, 24);
            this.txtTerminalTxDelayOn.TabIndex = 153;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(381, 424);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(276, 18);
            this.labelControl6.TabIndex = 152;
            this.labelControl6.Text = "Modo Conexi�n Internet Preferida";
            // 
            // cbModoConexionInternet
            // 
            this.cbModoConexionInternet.EditValue = "Defecto";
            this.cbModoConexionInternet.Location = new System.Drawing.Point(400, 442);
            this.cbModoConexionInternet.Name = "cbModoConexionInternet";
            this.cbModoConexionInternet.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModoConexionInternet.Properties.Appearance.Options.UseFont = true;
            this.cbModoConexionInternet.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModoConexionInternet.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cbModoConexionInternet.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModoConexionInternet.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbModoConexionInternet.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModoConexionInternet.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbModoConexionInternet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbModoConexionInternet.Properties.Items.AddRange(new object[] {
            "Defecto",
            "DialUp",
            "Banda Ancha Movil"});
            this.cbModoConexionInternet.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbModoConexionInternet.Size = new System.Drawing.Size(347, 24);
            this.cbModoConexionInternet.TabIndex = 151;
            // 
            // tcbTerminalSinConexionServidor
            // 
            this.tcbTerminalSinConexionServidor.EditValue = 60;
            this.tcbTerminalSinConexionServidor.Location = new System.Drawing.Point(229, 373);
            this.tcbTerminalSinConexionServidor.Name = "tcbTerminalSinConexionServidor";
            this.tcbTerminalSinConexionServidor.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbTerminalSinConexionServidor.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalSinConexionServidor.Properties.Appearance.Options.UseFont = true;
            this.tcbTerminalSinConexionServidor.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalSinConexionServidor.Properties.Maximum = 360;
            this.tcbTerminalSinConexionServidor.Properties.TickFrequency = 30;
            this.tcbTerminalSinConexionServidor.Size = new System.Drawing.Size(128, 45);
            this.tcbTerminalSinConexionServidor.TabIndex = 150;
            this.tcbTerminalSinConexionServidor.Value = 60;
            // 
            // tcbTerminalSinConexionInternet
            // 
            this.tcbTerminalSinConexionInternet.EditValue = 8;
            this.tcbTerminalSinConexionInternet.Location = new System.Drawing.Point(229, 281);
            this.tcbTerminalSinConexionInternet.Name = "tcbTerminalSinConexionInternet";
            this.tcbTerminalSinConexionInternet.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbTerminalSinConexionInternet.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalSinConexionInternet.Properties.Appearance.Options.UseFont = true;
            this.tcbTerminalSinConexionInternet.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalSinConexionInternet.Properties.Maximum = 60;
            this.tcbTerminalSinConexionInternet.Properties.TickFrequency = 10;
            this.tcbTerminalSinConexionInternet.Size = new System.Drawing.Size(128, 45);
            this.tcbTerminalSinConexionInternet.TabIndex = 149;
            this.tcbTerminalSinConexionInternet.Value = 8;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Location = new System.Drawing.Point(17, 281);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(209, 39);
            this.labelControl18.TabIndex = 148;
            this.labelControl18.Text = "M�ximo Tiempo sin Conexi�n a Internet.";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.labelControl19.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.Location = new System.Drawing.Point(17, 373);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(206, 42);
            this.labelControl19.TabIndex = 147;
            this.labelControl19.Text = "M�ximo Tiempo sin Conexi�n con Servidor Central.";
            // 
            // lbLasShutDown
            // 
            this.lbLasShutDown.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLasShutDown.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbLasShutDown.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbLasShutDown.Location = new System.Drawing.Point(20, 250);
            this.lbLasShutDown.Name = "lbLasShutDown";
            this.lbLasShutDown.Size = new System.Drawing.Size(203, 19);
            this.lbLasShutDown.TabIndex = 146;
            this.lbLasShutDown.Text = "Desde";
            // 
            // teLastShutDown
            // 
            this.teLastShutDown.EditValue = "20";
            this.teLastShutDown.Enabled = false;
            this.teLastShutDown.Location = new System.Drawing.Point(232, 247);
            this.teLastShutDown.Name = "teLastShutDown";
            this.teLastShutDown.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teLastShutDown.Properties.Appearance.Options.UseFont = true;
            this.teLastShutDown.Size = new System.Drawing.Size(128, 22);
            this.teLastShutDown.TabIndex = 145;
            // 
            // lbDaysForShutDown
            // 
            this.lbDaysForShutDown.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDaysForShutDown.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbDaysForShutDown.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbDaysForShutDown.Location = new System.Drawing.Point(17, 208);
            this.lbDaysForShutDown.Name = "lbDaysForShutDown";
            this.lbDaysForShutDown.Size = new System.Drawing.Size(203, 34);
            this.lbDaysForShutDown.TabIndex = 144;
            this.lbDaysForShutDown.Text = "N�mero de dias sin apagar";
            // 
            // teDaysForShutDown
            // 
            this.teDaysForShutDown.EditValue = "20";
            this.teDaysForShutDown.Location = new System.Drawing.Point(232, 219);
            this.teDaysForShutDown.Name = "teDaysForShutDown";
            this.teDaysForShutDown.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teDaysForShutDown.Properties.Appearance.Options.UseFont = true;
            this.teDaysForShutDown.Size = new System.Drawing.Size(128, 22);
            this.teDaysForShutDown.TabIndex = 143;
            // 
            // cbUserMonitorTimeout
            // 
            this.cbUserMonitorTimeout.EditValue = "Tras_3_minutos";
            this.cbUserMonitorTimeout.Location = new System.Drawing.Point(380, 106);
            this.cbUserMonitorTimeout.Name = "cbUserMonitorTimeout";
            this.cbUserMonitorTimeout.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.cbUserMonitorTimeout.Properties.Appearance.Options.UseFont = true;
            this.cbUserMonitorTimeout.Properties.Appearance.Options.UseTextOptions = true;
            this.cbUserMonitorTimeout.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUserMonitorTimeout.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbUserMonitorTimeout.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUserMonitorTimeout.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbUserMonitorTimeout.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbUserMonitorTimeout.Properties.Items.AddRange(new object[] {
            "Indefinido",
            "MDV",
            "MDT"});
            this.cbUserMonitorTimeout.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbUserMonitorTimeout.Size = new System.Drawing.Size(367, 24);
            this.cbUserMonitorTimeout.TabIndex = 142;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(380, 81);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(356, 22);
            this.labelControl2.TabIndex = 141;
            this.labelControl2.Text = "Apagar Pantalla Tras (minutos)";
            // 
            // cbTerminalModoColores
            // 
            this.cbTerminalModoColores.EditValue = "Todos";
            this.cbTerminalModoColores.Location = new System.Drawing.Point(380, 49);
            this.cbTerminalModoColores.Name = "cbTerminalModoColores";
            this.cbTerminalModoColores.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTerminalModoColores.Properties.Appearance.Options.UseFont = true;
            this.cbTerminalModoColores.Properties.Appearance.Options.UseTextOptions = true;
            this.cbTerminalModoColores.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTerminalModoColores.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTerminalModoColores.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTerminalModoColores.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTerminalModoColores.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTerminalModoColores.Properties.Items.AddRange(new object[] {
            "Indefinido",
            "MDV",
            "MDT"});
            this.cbTerminalModoColores.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTerminalModoColores.Size = new System.Drawing.Size(367, 24);
            this.cbTerminalModoColores.TabIndex = 140;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(380, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(203, 16);
            this.labelControl1.TabIndex = 139;
            this.labelControl1.Text = "Vehiculo Estado Colores";
            // 
            // teDaysForReboot
            // 
            this.teDaysForReboot.EditValue = "20";
            this.teDaysForReboot.Location = new System.Drawing.Point(717, 15);
            this.teDaysForReboot.Name = "teDaysForReboot";
            this.teDaysForReboot.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teDaysForReboot.Properties.Appearance.Options.UseFont = true;
            this.teDaysForReboot.Size = new System.Drawing.Size(30, 22);
            this.teDaysForReboot.TabIndex = 136;
            this.teDaysForReboot.Visible = false;
            // 
            // lbDaysForReboot
            // 
            this.lbDaysForReboot.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDaysForReboot.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbDaysForReboot.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbDaysForReboot.Location = new System.Drawing.Point(612, 7);
            this.lbDaysForReboot.Name = "lbDaysForReboot";
            this.lbDaysForReboot.Size = new System.Drawing.Size(109, 34);
            this.lbDaysForReboot.TabIndex = 135;
            this.lbDaysForReboot.Text = "N�mero de dias para reiniciar terminal";
            this.lbDaysForReboot.Visible = false;
            // 
            // tcbTerminalIntervalSendCola
            // 
            this.tcbTerminalIntervalSendCola.EditValue = 30;
            this.tcbTerminalIntervalSendCola.Location = new System.Drawing.Point(619, 326);
            this.tcbTerminalIntervalSendCola.Name = "tcbTerminalIntervalSendCola";
            this.tcbTerminalIntervalSendCola.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbTerminalIntervalSendCola.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalIntervalSendCola.Properties.Appearance.Options.UseFont = true;
            this.tcbTerminalIntervalSendCola.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalIntervalSendCola.Properties.Maximum = 60;
            this.tcbTerminalIntervalSendCola.Properties.Minimum = 15;
            this.tcbTerminalIntervalSendCola.Properties.SmallChange = 5;
            this.tcbTerminalIntervalSendCola.Properties.TickFrequency = 5;
            this.tcbTerminalIntervalSendCola.Size = new System.Drawing.Size(128, 45);
            this.tcbTerminalIntervalSendCola.TabIndex = 131;
            this.tcbTerminalIntervalSendCola.Value = 30;
            // 
            // tbcTerminalMinWaitRsp
            // 
            this.tbcTerminalMinWaitRsp.EditValue = 1;
            this.tbcTerminalMinWaitRsp.Location = new System.Drawing.Point(619, 277);
            this.tbcTerminalMinWaitRsp.Name = "tbcTerminalMinWaitRsp";
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalMinWaitRsp.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalMinWaitRsp.Properties.Maximum = 15;
            this.tbcTerminalMinWaitRsp.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalMinWaitRsp.TabIndex = 130;
            this.tbcTerminalMinWaitRsp.Value = 1;
            // 
            // tbcTerminalMaxWaitRsp
            // 
            this.tbcTerminalMaxWaitRsp.EditValue = 60;
            this.tbcTerminalMaxWaitRsp.Location = new System.Drawing.Point(619, 224);
            this.tbcTerminalMaxWaitRsp.Name = "tbcTerminalMaxWaitRsp";
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalMaxWaitRsp.Properties.Maximum = 90;
            this.tbcTerminalMaxWaitRsp.Properties.Minimum = 15;
            this.tbcTerminalMaxWaitRsp.Properties.TickFrequency = 10;
            this.tbcTerminalMaxWaitRsp.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalMaxWaitRsp.TabIndex = 129;
            this.tbcTerminalMaxWaitRsp.Value = 60;
            // 
            // tcbTerminalTxDelayOff
            // 
            this.tcbTerminalTxDelayOff.EditValue = 15;
            this.tcbTerminalTxDelayOff.Location = new System.Drawing.Point(619, 138);
            this.tcbTerminalTxDelayOff.Name = "tcbTerminalTxDelayOff";
            this.tcbTerminalTxDelayOff.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbTerminalTxDelayOff.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalTxDelayOff.Properties.Appearance.Options.UseFont = true;
            this.tcbTerminalTxDelayOff.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalTxDelayOff.Properties.Maximum = 30;
            this.tcbTerminalTxDelayOff.Properties.Minimum = 5;
            this.tcbTerminalTxDelayOff.Properties.TickFrequency = 3;
            this.tcbTerminalTxDelayOff.Size = new System.Drawing.Size(128, 45);
            this.tcbTerminalTxDelayOff.TabIndex = 128;
            this.tcbTerminalTxDelayOff.Value = 15;
            // 
            // tbcTerminalSleepLogout
            // 
            this.tbcTerminalSleepLogout.EditValue = 10;
            this.tbcTerminalSleepLogout.Location = new System.Drawing.Point(229, 155);
            this.tbcTerminalSleepLogout.Name = "tbcTerminalSleepLogout";
            this.tbcTerminalSleepLogout.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalSleepLogout.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalSleepLogout.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalSleepLogout.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalSleepLogout.Properties.Maximum = 30;
            this.tbcTerminalSleepLogout.Properties.TickFrequency = 3;
            this.tbcTerminalSleepLogout.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalSleepLogout.TabIndex = 126;
            this.tbcTerminalSleepLogout.Value = 10;
            // 
            // tbcTerminalUmbralVelocidadParada
            // 
            this.tbcTerminalUmbralVelocidadParada.EditValue = 10;
            this.tbcTerminalUmbralVelocidadParada.Location = new System.Drawing.Point(229, 110);
            this.tbcTerminalUmbralVelocidadParada.Name = "tbcTerminalUmbralVelocidadParada";
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalUmbralVelocidadParada.Properties.Maximum = 20;
            this.tbcTerminalUmbralVelocidadParada.Properties.Minimum = 5;
            this.tbcTerminalUmbralVelocidadParada.Properties.TickFrequency = 2;
            this.tbcTerminalUmbralVelocidadParada.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalUmbralVelocidadParada.TabIndex = 125;
            this.tbcTerminalUmbralVelocidadParada.Value = 10;
            // 
            // tbcTerminalUmbralTempParada
            // 
            this.tbcTerminalUmbralTempParada.EditValue = 7;
            this.tbcTerminalUmbralTempParada.Location = new System.Drawing.Point(229, 62);
            this.tbcTerminalUmbralTempParada.Name = "tbcTerminalUmbralTempParada";
            this.tbcTerminalUmbralTempParada.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalUmbralTempParada.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalUmbralTempParada.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalUmbralTempParada.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalUmbralTempParada.Properties.Minimum = 3;
            this.tbcTerminalUmbralTempParada.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalUmbralTempParada.TabIndex = 124;
            this.tbcTerminalUmbralTempParada.Value = 7;
            // 
            // lbTerminalIntervalSendCola
            // 
            this.lbTerminalIntervalSendCola.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalIntervalSendCola.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalIntervalSendCola.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalIntervalSendCola.Location = new System.Drawing.Point(380, 328);
            this.lbTerminalIntervalSendCola.Name = "lbTerminalIntervalSendCola";
            this.lbTerminalIntervalSendCola.Size = new System.Drawing.Size(200, 43);
            this.lbTerminalIntervalSendCola.TabIndex = 122;
            this.lbTerminalIntervalSendCola.Text = "Frecuencia Envio de Cola (seg)";
            // 
            // lbTerminalMinWaitRsp
            // 
            this.lbTerminalMinWaitRsp.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMinWaitRsp.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMinWaitRsp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMinWaitRsp.Location = new System.Drawing.Point(380, 279);
            this.lbTerminalMinWaitRsp.Name = "lbTerminalMinWaitRsp";
            this.lbTerminalMinWaitRsp.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalMinWaitRsp.TabIndex = 121;
            this.lbTerminalMinWaitRsp.Text = "M�nimo Tiempo espera respuesta (seg)";
            // 
            // lbTerminalMaxWaitRsp
            // 
            this.lbTerminalMaxWaitRsp.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMaxWaitRsp.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMaxWaitRsp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMaxWaitRsp.Location = new System.Drawing.Point(380, 236);
            this.lbTerminalMaxWaitRsp.Name = "lbTerminalMaxWaitRsp";
            this.lbTerminalMaxWaitRsp.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalMaxWaitRsp.TabIndex = 120;
            this.lbTerminalMaxWaitRsp.Text = "M�ximo Tiempo espera respuesta (seg)";
            // 
            // lbTerminalTxDelayOn
            // 
            this.lbTerminalTxDelayOn.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalTxDelayOn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalTxDelayOn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalTxDelayOn.Location = new System.Drawing.Point(380, 187);
            this.lbTerminalTxDelayOn.Name = "lbTerminalTxDelayOn";
            this.lbTerminalTxDelayOn.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalTxDelayOn.TabIndex = 119;
            this.lbTerminalTxDelayOn.Text = "Frecuencia Envio Tramas en Movimiento (min.)";
            // 
            // lbTerminalTxDelayOff
            // 
            this.lbTerminalTxDelayOff.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalTxDelayOff.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalTxDelayOff.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalTxDelayOff.Location = new System.Drawing.Point(380, 138);
            this.lbTerminalTxDelayOff.Name = "lbTerminalTxDelayOff";
            this.lbTerminalTxDelayOff.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalTxDelayOff.TabIndex = 118;
            this.lbTerminalTxDelayOff.Text = "Frecuencia Envio Tramas en Parado (min)";
            // 
            // lbTerminalSleepLogout
            // 
            this.lbTerminalSleepLogout.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalSleepLogout.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalSleepLogout.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalSleepLogout.Location = new System.Drawing.Point(20, 160);
            this.lbTerminalSleepLogout.Name = "lbTerminalSleepLogout";
            this.lbTerminalSleepLogout.Size = new System.Drawing.Size(203, 34);
            this.lbTerminalSleepLogout.TabIndex = 117;
            this.lbTerminalSleepLogout.Text = "Tiempo desde el logout hasta suspensi�n";
            // 
            // lbTerminal
            // 
            this.lbTerminal.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminal.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminal.Location = new System.Drawing.Point(17, 115);
            this.lbTerminal.Name = "lbTerminal";
            this.lbTerminal.Size = new System.Drawing.Size(203, 34);
            this.lbTerminal.TabIndex = 112;
            this.lbTerminal.Text = "Umbral de velocidad parada Km/h";
            // 
            // lbTerminalUmbralTempParada
            // 
            this.lbTerminalUmbralTempParada.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalUmbralTempParada.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalUmbralTempParada.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalUmbralTempParada.Location = new System.Drawing.Point(20, 64);
            this.lbTerminalUmbralTempParada.Name = "lbTerminalUmbralTempParada";
            this.lbTerminalUmbralTempParada.Size = new System.Drawing.Size(203, 40);
            this.lbTerminalUmbralTempParada.TabIndex = 111;
            this.lbTerminalUmbralTempParada.Text = "Umbral de tiempo parada en min";
            // 
            // teTerminalMaxDistancia
            // 
            this.teTerminalMaxDistancia.EditValue = "20";
            this.teTerminalMaxDistancia.Location = new System.Drawing.Point(232, 30);
            this.teTerminalMaxDistancia.Name = "teTerminalMaxDistancia";
            this.teTerminalMaxDistancia.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTerminalMaxDistancia.Properties.Appearance.Options.UseFont = true;
            this.teTerminalMaxDistancia.Size = new System.Drawing.Size(128, 22);
            this.teTerminalMaxDistancia.TabIndex = 110;
            // 
            // lbTerminalMaxDistancia
            // 
            this.lbTerminalMaxDistancia.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMaxDistancia.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMaxDistancia.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMaxDistancia.Location = new System.Drawing.Point(23, 18);
            this.lbTerminalMaxDistancia.Name = "lbTerminalMaxDistancia";
            this.lbTerminalMaxDistancia.Size = new System.Drawing.Size(203, 48);
            this.lbTerminalMaxDistancia.TabIndex = 109;
            this.lbTerminalMaxDistancia.Text = "Maxima distancia entre dos puntos en km";
            // 
            // tbpAdminCnx
            // 
            this.tbpAdminCnx.Controls.Add(this.gcConexion);
            this.tbpAdminCnx.Name = "tbpAdminCnx";
            this.tbpAdminCnx.Size = new System.Drawing.Size(770, 498);
            this.tbpAdminCnx.Text = "Conexi�n";
            // 
            // gcConexion
            // 
            this.gcConexion.Controls.Add(this.teConexionServerPort3);
            this.gcConexion.Controls.Add(this.lbConexionServerPort3);
            this.gcConexion.Controls.Add(this.teConexionServerIP3);
            this.gcConexion.Controls.Add(this.lbConexionServerIP3);
            this.gcConexion.Controls.Add(this.teConexionServerName3);
            this.gcConexion.Controls.Add(this.lbConexionServerName3);
            this.gcConexion.Controls.Add(this.teConexionServerPort2);
            this.gcConexion.Controls.Add(this.lbConexionServerPort2);
            this.gcConexion.Controls.Add(this.teConexionServerIP2);
            this.gcConexion.Controls.Add(this.lbConexionServerIP2);
            this.gcConexion.Controls.Add(this.teConexionServerName2);
            this.gcConexion.Controls.Add(this.lbConexionServerName2);
            this.gcConexion.Controls.Add(this.teConexionServerPort1);
            this.gcConexion.Controls.Add(this.lbConexionServerPort1);
            this.gcConexion.Controls.Add(this.teConexionServerIP1);
            this.gcConexion.Controls.Add(this.lbConexionServerIP1);
            this.gcConexion.Controls.Add(this.teConexionServerName1);
            this.gcConexion.Controls.Add(this.lbConexionServerName1);
            this.gcConexion.Controls.Add(this.teConexionIntentos);
            this.gcConexion.Controls.Add(this.lbConexionIntentos);
            this.gcConexion.Controls.Add(this.cbConexionDefaultIP);
            this.gcConexion.Controls.Add(this.lbConexionDefaultPositioIP);
            this.gcConexion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcConexion.Enabled = false;
            this.gcConexion.Location = new System.Drawing.Point(0, 0);
            this.gcConexion.Name = "gcConexion";
            this.gcConexion.Size = new System.Drawing.Size(770, 498);
            this.gcConexion.TabIndex = 83;
            // 
            // teConexionServerPort3
            // 
            this.teConexionServerPort3.EditValue = "1980";
            this.teConexionServerPort3.Location = new System.Drawing.Point(573, 227);
            this.teConexionServerPort3.Name = "teConexionServerPort3";
            this.teConexionServerPort3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort3.Size = new System.Drawing.Size(192, 24);
            this.teConexionServerPort3.TabIndex = 108;
            // 
            // lbConexionServerPort3
            // 
            this.lbConexionServerPort3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort3.Location = new System.Drawing.Point(400, 230);
            this.lbConexionServerPort3.Name = "lbConexionServerPort3";
            this.lbConexionServerPort3.Size = new System.Drawing.Size(149, 18);
            this.lbConexionServerPort3.TabIndex = 107;
            this.lbConexionServerPort3.Text = "Puerto servidor 3�";
            // 
            // teConexionServerIP3
            // 
            this.teConexionServerIP3.EditValue = "192.168.50.110";
            this.teConexionServerIP3.Location = new System.Drawing.Point(574, 195);
            this.teConexionServerIP3.Name = "teConexionServerIP3";
            this.teConexionServerIP3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP3.Size = new System.Drawing.Size(192, 24);
            this.teConexionServerIP3.TabIndex = 106;
            // 
            // lbConexionServerIP3
            // 
            this.lbConexionServerIP3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP3.Location = new System.Drawing.Point(399, 198);
            this.lbConexionServerIP3.Name = "lbConexionServerIP3";
            this.lbConexionServerIP3.Size = new System.Drawing.Size(122, 18);
            this.lbConexionServerIP3.TabIndex = 105;
            this.lbConexionServerIP3.Text = "Direcci�n IP 3�";
            // 
            // teConexionServerName3
            // 
            this.teConexionServerName3.EditValue = "";
            this.teConexionServerName3.Location = new System.Drawing.Point(574, 163);
            this.teConexionServerName3.Name = "teConexionServerName3";
            this.teConexionServerName3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName3.Size = new System.Drawing.Size(192, 24);
            this.teConexionServerName3.TabIndex = 104;
            // 
            // lbConexionServerName3
            // 
            this.lbConexionServerName3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName3.Location = new System.Drawing.Point(399, 166);
            this.lbConexionServerName3.Name = "lbConexionServerName3";
            this.lbConexionServerName3.Size = new System.Drawing.Size(160, 18);
            this.lbConexionServerName3.TabIndex = 103;
            this.lbConexionServerName3.Text = "Nombre Servidor 3�";
            // 
            // teConexionServerPort2
            // 
            this.teConexionServerPort2.EditValue = "1980";
            this.teConexionServerPort2.Location = new System.Drawing.Point(574, 110);
            this.teConexionServerPort2.Name = "teConexionServerPort2";
            this.teConexionServerPort2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort2.Size = new System.Drawing.Size(192, 24);
            this.teConexionServerPort2.TabIndex = 102;
            // 
            // lbConexionServerPort2
            // 
            this.lbConexionServerPort2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort2.Location = new System.Drawing.Point(399, 113);
            this.lbConexionServerPort2.Name = "lbConexionServerPort2";
            this.lbConexionServerPort2.Size = new System.Drawing.Size(149, 18);
            this.lbConexionServerPort2.TabIndex = 101;
            this.lbConexionServerPort2.Text = "Puerto servidor 2�";
            // 
            // teConexionServerIP2
            // 
            this.teConexionServerIP2.EditValue = "192.168.50.110";
            this.teConexionServerIP2.Location = new System.Drawing.Point(574, 78);
            this.teConexionServerIP2.Name = "teConexionServerIP2";
            this.teConexionServerIP2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP2.Size = new System.Drawing.Size(192, 24);
            this.teConexionServerIP2.TabIndex = 100;
            // 
            // lbConexionServerIP2
            // 
            this.lbConexionServerIP2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP2.Location = new System.Drawing.Point(399, 81);
            this.lbConexionServerIP2.Name = "lbConexionServerIP2";
            this.lbConexionServerIP2.Size = new System.Drawing.Size(122, 18);
            this.lbConexionServerIP2.TabIndex = 99;
            this.lbConexionServerIP2.Text = "Direcci�n IP 2�";
            // 
            // teConexionServerName2
            // 
            this.teConexionServerName2.EditValue = "";
            this.teConexionServerName2.Location = new System.Drawing.Point(574, 46);
            this.teConexionServerName2.Name = "teConexionServerName2";
            this.teConexionServerName2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName2.Size = new System.Drawing.Size(192, 24);
            this.teConexionServerName2.TabIndex = 98;
            // 
            // lbConexionServerName2
            // 
            this.lbConexionServerName2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName2.Location = new System.Drawing.Point(399, 49);
            this.lbConexionServerName2.Name = "lbConexionServerName2";
            this.lbConexionServerName2.Size = new System.Drawing.Size(160, 18);
            this.lbConexionServerName2.TabIndex = 97;
            this.lbConexionServerName2.Text = "Nombre Servidor 2�";
            // 
            // teConexionServerPort1
            // 
            this.teConexionServerPort1.EditValue = "1980";
            this.teConexionServerPort1.Location = new System.Drawing.Point(194, 227);
            this.teConexionServerPort1.Name = "teConexionServerPort1";
            this.teConexionServerPort1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort1.Size = new System.Drawing.Size(180, 24);
            this.teConexionServerPort1.TabIndex = 96;
            // 
            // lbConexionServerPort1
            // 
            this.lbConexionServerPort1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort1.Location = new System.Drawing.Point(24, 230);
            this.lbConexionServerPort1.Name = "lbConexionServerPort1";
            this.lbConexionServerPort1.Size = new System.Drawing.Size(149, 18);
            this.lbConexionServerPort1.TabIndex = 95;
            this.lbConexionServerPort1.Text = "Puerto servidor 1�";
            // 
            // teConexionServerIP1
            // 
            this.teConexionServerIP1.EditValue = "192.168.50.110";
            this.teConexionServerIP1.Location = new System.Drawing.Point(193, 195);
            this.teConexionServerIP1.Name = "teConexionServerIP1";
            this.teConexionServerIP1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP1.Size = new System.Drawing.Size(180, 24);
            this.teConexionServerIP1.TabIndex = 94;
            // 
            // lbConexionServerIP1
            // 
            this.lbConexionServerIP1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP1.Location = new System.Drawing.Point(23, 198);
            this.lbConexionServerIP1.Name = "lbConexionServerIP1";
            this.lbConexionServerIP1.Size = new System.Drawing.Size(122, 18);
            this.lbConexionServerIP1.TabIndex = 93;
            this.lbConexionServerIP1.Text = "Direcci�n IP 1�";
            // 
            // teConexionServerName1
            // 
            this.teConexionServerName1.EditValue = "";
            this.teConexionServerName1.Location = new System.Drawing.Point(193, 163);
            this.teConexionServerName1.Name = "teConexionServerName1";
            this.teConexionServerName1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName1.Size = new System.Drawing.Size(180, 24);
            this.teConexionServerName1.TabIndex = 92;
            // 
            // lbConexionServerName1
            // 
            this.lbConexionServerName1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName1.Location = new System.Drawing.Point(22, 166);
            this.lbConexionServerName1.Name = "lbConexionServerName1";
            this.lbConexionServerName1.Size = new System.Drawing.Size(160, 18);
            this.lbConexionServerName1.TabIndex = 91;
            this.lbConexionServerName1.Text = "Nombre Servidor 1�";
            // 
            // teConexionIntentos
            // 
            this.teConexionIntentos.EditValue = "3";
            this.teConexionIntentos.Location = new System.Drawing.Point(194, 78);
            this.teConexionIntentos.Name = "teConexionIntentos";
            this.teConexionIntentos.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionIntentos.Properties.Appearance.Options.UseFont = true;
            this.teConexionIntentos.Size = new System.Drawing.Size(180, 24);
            this.teConexionIntentos.TabIndex = 88;
            // 
            // lbConexionIntentos
            // 
            this.lbConexionIntentos.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionIntentos.Location = new System.Drawing.Point(24, 81);
            this.lbConexionIntentos.Name = "lbConexionIntentos";
            this.lbConexionIntentos.Size = new System.Drawing.Size(138, 18);
            this.lbConexionIntentos.TabIndex = 87;
            this.lbConexionIntentos.Text = "Intetos conexion";
            // 
            // cbConexionDefaultIP
            // 
            this.cbConexionDefaultIP.EditValue = "1";
            this.cbConexionDefaultIP.Location = new System.Drawing.Point(193, 46);
            this.cbConexionDefaultIP.Name = "cbConexionDefaultIP";
            this.cbConexionDefaultIP.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConexionDefaultIP.Properties.Appearance.Options.UseFont = true;
            this.cbConexionDefaultIP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConexionDefaultIP.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbConexionDefaultIP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbConexionDefaultIP.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbConexionDefaultIP.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbConexionDefaultIP.Size = new System.Drawing.Size(180, 24);
            this.cbConexionDefaultIP.TabIndex = 86;
            // 
            // lbConexionDefaultPositioIP
            // 
            this.lbConexionDefaultPositioIP.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionDefaultPositioIP.Location = new System.Drawing.Point(23, 49);
            this.lbConexionDefaultPositioIP.Name = "lbConexionDefaultPositioIP";
            this.lbConexionDefaultPositioIP.Size = new System.Drawing.Size(143, 18);
            this.lbConexionDefaultPositioIP.TabIndex = 85;
            this.lbConexionDefaultPositioIP.Text = "N� IP por defecto";
            // 
            // tbpAdminFtpTrace
            // 
            this.tbpAdminFtpTrace.Controls.Add(this.gcTrace);
            this.tbpAdminFtpTrace.Controls.Add(this.gcFtp);
            this.tbpAdminFtpTrace.Name = "tbpAdminFtpTrace";
            this.tbpAdminFtpTrace.Size = new System.Drawing.Size(770, 498);
            this.tbpAdminFtpTrace.Text = "Traza/Log";
            // 
            // gcTrace
            // 
            this.gcTrace.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcTrace.AppearanceCaption.Options.UseFont = true;
            this.gcTrace.Controls.Add(this.cbTraceProtocol);
            this.gcTrace.Controls.Add(this.lbTraceProtocol);
            this.gcTrace.Controls.Add(this.cbTracePFileRotate);
            this.gcTrace.Controls.Add(this.lbTracePFileRotate);
            this.gcTrace.Controls.Add(this.teTracePFileName);
            this.gcTrace.Controls.Add(this.lbTracePFileName);
            this.gcTrace.Controls.Add(this.teTracePFileMaxSize);
            this.gcTrace.Controls.Add(this.lbTracePFileMaxSize);
            this.gcTrace.Controls.Add(this.cbTraceLevel);
            this.gcTrace.Controls.Add(this.lbTraceLevel);
            this.gcTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTrace.Enabled = false;
            this.gcTrace.Location = new System.Drawing.Point(0, 234);
            this.gcTrace.Name = "gcTrace";
            this.gcTrace.Size = new System.Drawing.Size(770, 264);
            this.gcTrace.TabIndex = 13;
            this.gcTrace.Text = "Trace";
            // 
            // cbTraceProtocol
            // 
            this.cbTraceProtocol.EditValue = "File";
            this.cbTraceProtocol.Enabled = false;
            this.cbTraceProtocol.Location = new System.Drawing.Point(571, 103);
            this.cbTraceProtocol.Name = "cbTraceProtocol";
            this.cbTraceProtocol.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.Appearance.Options.UseFont = true;
            this.cbTraceProtocol.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTraceProtocol.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTraceProtocol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTraceProtocol.Properties.Items.AddRange(new object[] {
            "Unknown",
            "Memory",
            "File",
            "TcpIp"});
            this.cbTraceProtocol.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTraceProtocol.Size = new System.Drawing.Size(185, 24);
            this.cbTraceProtocol.TabIndex = 52;
            // 
            // lbTraceProtocol
            // 
            this.lbTraceProtocol.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTraceProtocol.Location = new System.Drawing.Point(432, 108);
            this.lbTraceProtocol.Name = "lbTraceProtocol";
            this.lbTraceProtocol.Size = new System.Drawing.Size(76, 18);
            this.lbTraceProtocol.TabIndex = 51;
            this.lbTraceProtocol.Text = "Protocolo";
            // 
            // cbTracePFileRotate
            // 
            this.cbTracePFileRotate.EditValue = "Ninguno";
            this.cbTracePFileRotate.Location = new System.Drawing.Point(571, 51);
            this.cbTracePFileRotate.Name = "cbTracePFileRotate";
            this.cbTracePFileRotate.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.Appearance.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTracePFileRotate.Properties.Items.AddRange(new object[] {
            "None",
            "Hourly",
            "Daily"});
            this.cbTracePFileRotate.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTracePFileRotate.Size = new System.Drawing.Size(185, 24);
            this.cbTracePFileRotate.TabIndex = 50;
            // 
            // lbTracePFileRotate
            // 
            this.lbTracePFileRotate.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileRotate.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTracePFileRotate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTracePFileRotate.Location = new System.Drawing.Point(432, 47);
            this.lbTracePFileRotate.Name = "lbTracePFileRotate";
            this.lbTracePFileRotate.Size = new System.Drawing.Size(110, 50);
            this.lbTracePFileRotate.TabIndex = 49;
            this.lbTracePFileRotate.Text = "Perioricidad creaci�n";
            // 
            // teTracePFileName
            // 
            this.teTracePFileName.EditValue = "gitnavega.sil";
            this.teTracePFileName.Location = new System.Drawing.Point(211, 108);
            this.teTracePFileName.Name = "teTracePFileName";
            this.teTracePFileName.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTracePFileName.Properties.Appearance.Options.UseFont = true;
            this.teTracePFileName.Size = new System.Drawing.Size(200, 24);
            this.teTracePFileName.TabIndex = 48;
            // 
            // lbTracePFileName
            // 
            this.lbTracePFileName.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileName.Location = new System.Drawing.Point(13, 113);
            this.lbTracePFileName.Name = "lbTracePFileName";
            this.lbTracePFileName.Size = new System.Drawing.Size(127, 18);
            this.lbTracePFileName.TabIndex = 47;
            this.lbTracePFileName.Text = "Nombre archivo";
            // 
            // teTracePFileMaxSize
            // 
            this.teTracePFileMaxSize.EditValue = "";
            this.teTracePFileMaxSize.Location = new System.Drawing.Point(211, 76);
            this.teTracePFileMaxSize.Name = "teTracePFileMaxSize";
            this.teTracePFileMaxSize.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTracePFileMaxSize.Properties.Appearance.Options.UseFont = true;
            this.teTracePFileMaxSize.Size = new System.Drawing.Size(200, 24);
            this.teTracePFileMaxSize.TabIndex = 46;
            // 
            // lbTracePFileMaxSize
            // 
            this.lbTracePFileMaxSize.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileMaxSize.Location = new System.Drawing.Point(13, 81);
            this.lbTracePFileMaxSize.Name = "lbTracePFileMaxSize";
            this.lbTracePFileMaxSize.Size = new System.Drawing.Size(164, 18);
            this.lbTracePFileMaxSize.TabIndex = 45;
            this.lbTracePFileMaxSize.Text = "Max tama�o archivo";
            // 
            // cbTraceLevel
            // 
            this.cbTraceLevel.EditValue = "Debug";
            this.cbTraceLevel.Location = new System.Drawing.Point(211, 44);
            this.cbTraceLevel.Name = "cbTraceLevel";
            this.cbTraceLevel.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.Appearance.Options.UseFont = true;
            this.cbTraceLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTraceLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTraceLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTraceLevel.Properties.Items.AddRange(new object[] {
            "Debug",
            "Verbose",
            "Message",
            "Warning",
            "Error",
            "Fatal"});
            this.cbTraceLevel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTraceLevel.Size = new System.Drawing.Size(200, 24);
            this.cbTraceLevel.TabIndex = 38;
            // 
            // lbTraceLevel
            // 
            this.lbTraceLevel.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTraceLevel.Location = new System.Drawing.Point(13, 49);
            this.lbTraceLevel.Name = "lbTraceLevel";
            this.lbTraceLevel.Size = new System.Drawing.Size(72, 18);
            this.lbTraceLevel.TabIndex = 37;
            this.lbTraceLevel.Text = "Nivel log";
            // 
            // gcFtp
            // 
            this.gcFtp.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcFtp.AppearanceCaption.Options.UseFont = true;
            this.gcFtp.Controls.Add(this.teUpdatePwd);
            this.gcFtp.Controls.Add(labelControl9);
            this.gcFtp.Controls.Add(this.teUpdateUser);
            this.gcFtp.Controls.Add(labelControl10);
            this.gcFtp.Controls.Add(this.teUpdateRemoteHost);
            this.gcFtp.Controls.Add(this.labelControl11);
            this.gcFtp.Controls.Add(this.teUpdatePathRemote);
            this.gcFtp.Controls.Add(this.labelControl12);
            this.gcFtp.Controls.Add(this.teFtpPathRemote);
            this.gcFtp.Controls.Add(this.labelControl13);
            this.gcFtp.Controls.Add(this.teFtpPwd);
            this.gcFtp.Controls.Add(labelControl7);
            this.gcFtp.Controls.Add(this.teFtpUser);
            this.gcFtp.Controls.Add(labelControl8);
            this.gcFtp.Controls.Add(this.cbFtpModoEnvioLog);
            this.gcFtp.Controls.Add(this.lbFtpModoEnvio);
            this.gcFtp.Controls.Add(this.lbFtpLastDate);
            this.gcFtp.Controls.Add(this.deFtpLastDateLog);
            this.gcFtp.Controls.Add(this.teFtpRemoteHost);
            this.gcFtp.Controls.Add(this.lbFtpRemoteHost);
            this.gcFtp.Controls.Add(this.teFtpPort);
            this.gcFtp.Controls.Add(this.lbFtpPort);
            this.gcFtp.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFtp.Enabled = false;
            this.gcFtp.Location = new System.Drawing.Point(0, 0);
            this.gcFtp.Name = "gcFtp";
            this.gcFtp.Size = new System.Drawing.Size(770, 234);
            this.gcFtp.TabIndex = 12;
            this.gcFtp.Text = "Configuraci�n Ftp. Envio de Logs";
            // 
            // teUpdatePwd
            // 
            this.teUpdatePwd.EditValue = "3141516";
            this.teUpdatePwd.Location = new System.Drawing.Point(620, 173);
            this.teUpdatePwd.Name = "teUpdatePwd";
            this.teUpdatePwd.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teUpdatePwd.Properties.Appearance.Options.UseFont = true;
            this.teUpdatePwd.Size = new System.Drawing.Size(136, 22);
            this.teUpdatePwd.TabIndex = 60;
            // 
            // teUpdateUser
            // 
            this.teUpdateUser.EditValue = "soft2867";
            this.teUpdateUser.Location = new System.Drawing.Point(411, 173);
            this.teUpdateUser.Name = "teUpdateUser";
            this.teUpdateUser.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teUpdateUser.Properties.Appearance.Options.UseFont = true;
            this.teUpdateUser.Size = new System.Drawing.Size(203, 22);
            this.teUpdateUser.TabIndex = 58;
            // 
            // teUpdateRemoteHost
            // 
            this.teUpdateRemoteHost.EditValue = "ftp.amcoex.es";
            this.teUpdateRemoteHost.Location = new System.Drawing.Point(214, 174);
            this.teUpdateRemoteHost.Name = "teUpdateRemoteHost";
            this.teUpdateRemoteHost.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teUpdateRemoteHost.Properties.Appearance.Options.UseFont = true;
            this.teUpdateRemoteHost.Size = new System.Drawing.Size(183, 22);
            this.teUpdateRemoteHost.TabIndex = 56;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(16, 177);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(143, 16);
            this.labelControl11.TabIndex = 55;
            this.labelControl11.Text = "Servidor FTP Updates";
            // 
            // teUpdatePathRemote
            // 
            this.teUpdatePathRemote.AllowDrop = true;
            this.teUpdatePathRemote.EditValue = "";
            this.teUpdatePathRemote.Location = new System.Drawing.Point(211, 201);
            this.teUpdatePathRemote.Name = "teUpdatePathRemote";
            this.teUpdatePathRemote.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teUpdatePathRemote.Properties.Appearance.Options.UseFont = true;
            this.teUpdatePathRemote.Size = new System.Drawing.Size(542, 22);
            this.teUpdatePathRemote.TabIndex = 54;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(16, 204);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(143, 16);
            this.labelControl12.TabIndex = 53;
            this.labelControl12.Text = "Path Remoto Updates";
            // 
            // teFtpPathRemote
            // 
            this.teFtpPathRemote.AllowDrop = true;
            this.teFtpPathRemote.EditValue = "";
            this.teFtpPathRemote.Location = new System.Drawing.Point(211, 109);
            this.teFtpPathRemote.Name = "teFtpPathRemote";
            this.teFtpPathRemote.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpPathRemote.Properties.Appearance.Options.UseFont = true;
            this.teFtpPathRemote.Size = new System.Drawing.Size(542, 22);
            this.teFtpPathRemote.TabIndex = 52;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(16, 112);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(112, 16);
            this.labelControl13.TabIndex = 51;
            this.labelControl13.Text = "Path Remoto Log";
            // 
            // teFtpPwd
            // 
            this.teFtpPwd.EditValue = "3141516";
            this.teFtpPwd.Location = new System.Drawing.Point(623, 82);
            this.teFtpPwd.Name = "teFtpPwd";
            this.teFtpPwd.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpPwd.Properties.Appearance.Options.UseFont = true;
            this.teFtpPwd.Size = new System.Drawing.Size(136, 22);
            this.teFtpPwd.TabIndex = 44;
            // 
            // teFtpUser
            // 
            this.teFtpUser.EditValue = "soft2867";
            this.teFtpUser.Location = new System.Drawing.Point(418, 82);
            this.teFtpUser.Name = "teFtpUser";
            this.teFtpUser.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpUser.Properties.Appearance.Options.UseFont = true;
            this.teFtpUser.Size = new System.Drawing.Size(199, 22);
            this.teFtpUser.TabIndex = 42;
            // 
            // cbFtpModoEnvioLog
            // 
            this.cbFtpModoEnvioLog.EditValue = "Ninguno";
            this.cbFtpModoEnvioLog.Location = new System.Drawing.Point(558, 30);
            this.cbFtpModoEnvioLog.Name = "cbFtpModoEnvioLog";
            this.cbFtpModoEnvioLog.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.Appearance.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbFtpModoEnvioLog.Properties.Items.AddRange(new object[] {
            "Nada",
            "Diaria",
            "Horaria"});
            this.cbFtpModoEnvioLog.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbFtpModoEnvioLog.Size = new System.Drawing.Size(183, 22);
            this.cbFtpModoEnvioLog.TabIndex = 36;
            // 
            // lbFtpModoEnvio
            // 
            this.lbFtpModoEnvio.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpModoEnvio.Location = new System.Drawing.Point(418, 33);
            this.lbFtpModoEnvio.Name = "lbFtpModoEnvio";
            this.lbFtpModoEnvio.Size = new System.Drawing.Size(115, 16);
            this.lbFtpModoEnvio.TabIndex = 35;
            this.lbFtpModoEnvio.Text = "Perioricidad envio";
            // 
            // lbFtpLastDate
            // 
            this.lbFtpLastDate.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpLastDate.Location = new System.Drawing.Point(16, 85);
            this.lbFtpLastDate.Name = "lbFtpLastDate";
            this.lbFtpLastDate.Size = new System.Drawing.Size(122, 16);
            this.lbFtpLastDate.TabIndex = 34;
            this.lbFtpLastDate.Text = "Ultima fecha envio";
            // 
            // deFtpLastDateLog
            // 
            this.deFtpLastDateLog.EditValue = new System.DateTime(2010, 9, 2, 0, 0, 0, 0);
            this.deFtpLastDateLog.Location = new System.Drawing.Point(211, 82);
            this.deFtpLastDateLog.Name = "deFtpLastDateLog";
            this.deFtpLastDateLog.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deFtpLastDateLog.Properties.Appearance.Options.UseFont = true;
            this.deFtpLastDateLog.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFtpLastDateLog.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFtpLastDateLog.Properties.DisplayFormat.FormatString = "g";
            this.deFtpLastDateLog.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFtpLastDateLog.Properties.Mask.EditMask = "g";
            this.deFtpLastDateLog.Size = new System.Drawing.Size(183, 22);
            this.deFtpLastDateLog.TabIndex = 33;
            // 
            // teFtpRemoteHost
            // 
            this.teFtpRemoteHost.EditValue = "ftp.amcoex.es";
            this.teFtpRemoteHost.Location = new System.Drawing.Point(211, 55);
            this.teFtpRemoteHost.Name = "teFtpRemoteHost";
            this.teFtpRemoteHost.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpRemoteHost.Properties.Appearance.Options.UseFont = true;
            this.teFtpRemoteHost.Size = new System.Drawing.Size(183, 22);
            this.teFtpRemoteHost.TabIndex = 32;
            // 
            // lbFtpRemoteHost
            // 
            this.lbFtpRemoteHost.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpRemoteHost.Location = new System.Drawing.Point(16, 58);
            this.lbFtpRemoteHost.Name = "lbFtpRemoteHost";
            this.lbFtpRemoteHost.Size = new System.Drawing.Size(84, 16);
            this.lbFtpRemoteHost.TabIndex = 31;
            this.lbFtpRemoteHost.Text = "Direcci�n ftp";
            // 
            // teFtpPort
            // 
            this.teFtpPort.EditValue = "21";
            this.teFtpPort.Location = new System.Drawing.Point(211, 30);
            this.teFtpPort.Name = "teFtpPort";
            this.teFtpPort.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpPort.Properties.Appearance.Options.UseFont = true;
            this.teFtpPort.Size = new System.Drawing.Size(183, 22);
            this.teFtpPort.TabIndex = 30;
            // 
            // lbFtpPort
            // 
            this.lbFtpPort.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpPort.Location = new System.Drawing.Point(16, 35);
            this.lbFtpPort.Name = "lbFtpPort";
            this.lbFtpPort.Size = new System.Drawing.Size(43, 16);
            this.lbFtpPort.TabIndex = 29;
            this.lbFtpPort.Text = "Puerto";
            // 
            // tbpAdminVarios
            // 
            this.tbpAdminVarios.Controls.Add(this.gcGeneral);
            this.tbpAdminVarios.Name = "tbpAdminVarios";
            this.tbpAdminVarios.Size = new System.Drawing.Size(770, 498);
            this.tbpAdminVarios.Text = "General";
            // 
            // gcGeneral
            // 
            this.gcGeneral.Controls.Add(this.ckNoFiltrarServiciosByLoginActivo);
            this.gcGeneral.Controls.Add(this.ckSE_ShowMarker);
            this.gcGeneral.Controls.Add(this.ckLoginAskActivityType);
            this.gcGeneral.Controls.Add(this.ckCatastrofeActivo);
            this.gcGeneral.Controls.Add(this.ckCambioVehiculoActivo);
            this.gcGeneral.Controls.Add(this.ckMntoActivo);
            this.gcGeneral.Controls.Add(this.frGenComida);
            this.gcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcGeneral.Enabled = false;
            this.gcGeneral.Location = new System.Drawing.Point(0, 0);
            this.gcGeneral.Name = "gcGeneral";
            this.gcGeneral.Size = new System.Drawing.Size(770, 498);
            this.gcGeneral.TabIndex = 2;
            // 
            // ckNoFiltrarServiciosByLoginActivo
            // 
            this.ckNoFiltrarServiciosByLoginActivo.Location = new System.Drawing.Point(405, 31);
            this.ckNoFiltrarServiciosByLoginActivo.Name = "ckNoFiltrarServiciosByLoginActivo";
            this.ckNoFiltrarServiciosByLoginActivo.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckNoFiltrarServiciosByLoginActivo.Properties.Appearance.Options.UseFont = true;
            this.ckNoFiltrarServiciosByLoginActivo.Properties.Appearance.Options.UseTextOptions = true;
            this.ckNoFiltrarServiciosByLoginActivo.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ckNoFiltrarServiciosByLoginActivo.Properties.Caption = "Activar: Mostrar todos los Servicios en el Terminal, Sin filtro de conductor.";
            this.ckNoFiltrarServiciosByLoginActivo.Size = new System.Drawing.Size(358, 40);
            this.ckNoFiltrarServiciosByLoginActivo.TabIndex = 136;
            this.ckNoFiltrarServiciosByLoginActivo.ToolTipTitle = "Si se activa, permite mostrar todos los servicios que existan en el Terminal, sin" +
    " filtro de conductor. Esto permite, que un conductor vea servicios que no sean p" +
    "ara �l.";
            // 
            // ckSE_ShowMarker
            // 
            this.ckSE_ShowMarker.Location = new System.Drawing.Point(28, 290);
            this.ckSE_ShowMarker.Name = "ckSE_ShowMarker";
            this.ckSE_ShowMarker.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckSE_ShowMarker.Properties.Appearance.Options.UseFont = true;
            this.ckSE_ShowMarker.Properties.Caption = "Activar: Ver Bot�n Marcar Recorrido en Servicios Especiales";
            this.ckSE_ShowMarker.Size = new System.Drawing.Size(517, 22);
            this.ckSE_ShowMarker.TabIndex = 135;
            // 
            // ckLoginAskActivityType
            // 
            this.ckLoginAskActivityType.Location = new System.Drawing.Point(28, 261);
            this.ckLoginAskActivityType.Name = "ckLoginAskActivityType";
            this.ckLoginAskActivityType.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckLoginAskActivityType.Properties.Appearance.Options.UseFont = true;
            this.ckLoginAskActivityType.Properties.Caption = "Activar: Preguntar Tipo Actividad al Logarse";
            this.ckLoginAskActivityType.Size = new System.Drawing.Size(402, 22);
            this.ckLoginAskActivityType.TabIndex = 134;
            // 
            // ckCatastrofeActivo
            // 
            this.ckCatastrofeActivo.Location = new System.Drawing.Point(28, 219);
            this.ckCatastrofeActivo.Name = "ckCatastrofeActivo";
            this.ckCatastrofeActivo.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckCatastrofeActivo.Properties.Appearance.Options.UseFont = true;
            this.ckCatastrofeActivo.Properties.Caption = "Activar: Opci�n \'Cat�strofe\'";
            this.ckCatastrofeActivo.Size = new System.Drawing.Size(302, 22);
            this.ckCatastrofeActivo.TabIndex = 133;
            // 
            // ckCambioVehiculoActivo
            // 
            this.ckCambioVehiculoActivo.Location = new System.Drawing.Point(28, 192);
            this.ckCambioVehiculoActivo.Name = "ckCambioVehiculoActivo";
            this.ckCambioVehiculoActivo.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckCambioVehiculoActivo.Properties.Appearance.Options.UseFont = true;
            this.ckCambioVehiculoActivo.Properties.Caption = "Activar Opci�n \'Cambio de Vehiculo\'";
            this.ckCambioVehiculoActivo.Size = new System.Drawing.Size(360, 22);
            this.ckCambioVehiculoActivo.TabIndex = 130;
            // 
            // ckMntoActivo
            // 
            this.ckMntoActivo.Location = new System.Drawing.Point(28, 163);
            this.ckMntoActivo.Name = "ckMntoActivo";
            this.ckMntoActivo.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckMntoActivo.Properties.Appearance.Options.UseFont = true;
            this.ckMntoActivo.Properties.Caption = "Activar Gesti�n Estado Fuera de Servicio";
            this.ckMntoActivo.Size = new System.Drawing.Size(360, 22);
            this.ckMntoActivo.TabIndex = 128;
            // 
            // frGenComida
            // 
            this.frGenComida.Controls.Add(this.tbcComidaDesactivarDespuesDe);
            this.frGenComida.Controls.Add(this.ckComidaAutoDesactivar);
            this.frGenComida.Controls.Add(this.ckComidaActivo);
            this.frGenComida.Location = new System.Drawing.Point(5, 5);
            this.frGenComida.Name = "frGenComida";
            this.frGenComida.Size = new System.Drawing.Size(383, 133);
            this.frGenComida.TabIndex = 0;
            this.frGenComida.TabStop = false;
            this.frGenComida.Text = "Opciones Comida";
            // 
            // tbcComidaDesactivarDespuesDe
            // 
            this.tbcComidaDesactivarDespuesDe.EditValue = 60;
            this.tbcComidaDesactivarDespuesDe.Location = new System.Drawing.Point(66, 82);
            this.tbcComidaDesactivarDespuesDe.Name = "tbcComidaDesactivarDespuesDe";
            this.tbcComidaDesactivarDespuesDe.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcComidaDesactivarDespuesDe.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcComidaDesactivarDespuesDe.Properties.Appearance.Options.UseFont = true;
            this.tbcComidaDesactivarDespuesDe.Properties.Appearance.Options.UseForeColor = true;
            this.tbcComidaDesactivarDespuesDe.Properties.LargeChange = 10;
            this.tbcComidaDesactivarDespuesDe.Properties.Maximum = 360;
            this.tbcComidaDesactivarDespuesDe.Properties.Minimum = 30;
            this.tbcComidaDesactivarDespuesDe.Properties.SmallChange = 5;
            this.tbcComidaDesactivarDespuesDe.Properties.TickFrequency = 5;
            this.tbcComidaDesactivarDespuesDe.Size = new System.Drawing.Size(300, 45);
            this.tbcComidaDesactivarDespuesDe.TabIndex = 129;
            this.tbcComidaDesactivarDespuesDe.Value = 60;
            // 
            // ckComidaAutoDesactivar
            // 
            this.ckComidaAutoDesactivar.Location = new System.Drawing.Point(58, 55);
            this.ckComidaAutoDesactivar.Name = "ckComidaAutoDesactivar";
            this.ckComidaAutoDesactivar.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckComidaAutoDesactivar.Properties.Appearance.Options.UseFont = true;
            this.ckComidaAutoDesactivar.Properties.Caption = "Desactivar despues de... (min)";
            this.ckComidaAutoDesactivar.Size = new System.Drawing.Size(308, 22);
            this.ckComidaAutoDesactivar.TabIndex = 128;
            // 
            // ckComidaActivo
            // 
            this.ckComidaActivo.Location = new System.Drawing.Point(23, 26);
            this.ckComidaActivo.Name = "ckComidaActivo";
            this.ckComidaActivo.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckComidaActivo.Properties.Appearance.Options.UseFont = true;
            this.ckComidaActivo.Properties.Caption = "Activar Gesti�n Comida";
            this.ckComidaActivo.Size = new System.Drawing.Size(227, 22);
            this.ckComidaActivo.TabIndex = 127;
            // 
            // tbpProfile
            // 
            this.tbpProfile.Controls.Add(this.gcPerfil);
            this.tbpProfile.Name = "tbpProfile";
            this.tbpProfile.Size = new System.Drawing.Size(770, 498);
            this.tbpProfile.Text = "Perfil Movil";
            // 
            // gcPerfil
            // 
            this.gcPerfil.Controls.Add(this.teProfileMobileForceUse);
            this.gcPerfil.Controls.Add(this.teProfileMobileNumber);
            this.gcPerfil.Controls.Add(this.lbModemNumber);
            this.gcPerfil.Controls.Add(this.teProfileMobilePassword);
            this.gcPerfil.Controls.Add(this.lbModemPassword);
            this.gcPerfil.Controls.Add(this.teProfileMobileUserName);
            this.gcPerfil.Controls.Add(this.lbModemUserName);
            this.gcPerfil.Controls.Add(this.teProfileMobileName);
            this.gcPerfil.Controls.Add(this.lbModemName);
            this.gcPerfil.Controls.Add(this.teProfileMobileAPN);
            this.gcPerfil.Controls.Add(this.lbModemDomain);
            this.gcPerfil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcPerfil.Enabled = false;
            this.gcPerfil.Location = new System.Drawing.Point(0, 0);
            this.gcPerfil.Name = "gcPerfil";
            this.gcPerfil.Size = new System.Drawing.Size(770, 498);
            this.gcPerfil.TabIndex = 3;
            // 
            // teProfileMobileForceUse
            // 
            this.teProfileMobileForceUse.Location = new System.Drawing.Point(21, 26);
            this.teProfileMobileForceUse.Name = "teProfileMobileForceUse";
            this.teProfileMobileForceUse.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teProfileMobileForceUse.Properties.Appearance.Options.UseFont = true;
            this.teProfileMobileForceUse.Properties.Caption = "Forzar Usar estos datos de configuraci�n para conectar a la red movil";
            this.teProfileMobileForceUse.Size = new System.Drawing.Size(622, 22);
            this.teProfileMobileForceUse.TabIndex = 48;
            // 
            // teProfileMobileNumber
            // 
            this.teProfileMobileNumber.AllowDrop = true;
            this.teProfileMobileNumber.EditValue = "*99#";
            this.teProfileMobileNumber.Location = new System.Drawing.Point(172, 168);
            this.teProfileMobileNumber.Name = "teProfileMobileNumber";
            this.teProfileMobileNumber.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teProfileMobileNumber.Properties.Appearance.Options.UseFont = true;
            this.teProfileMobileNumber.Size = new System.Drawing.Size(209, 24);
            this.teProfileMobileNumber.TabIndex = 47;
            // 
            // lbModemNumber
            // 
            this.lbModemNumber.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemNumber.Location = new System.Drawing.Point(27, 171);
            this.lbModemNumber.Name = "lbModemNumber";
            this.lbModemNumber.Size = new System.Drawing.Size(63, 18);
            this.lbModemNumber.TabIndex = 46;
            this.lbModemNumber.Text = "N�mero";
            // 
            // teProfileMobilePassword
            // 
            this.teProfileMobilePassword.EditValue = "MOVISTAR";
            this.teProfileMobilePassword.Location = new System.Drawing.Point(172, 140);
            this.teProfileMobilePassword.Name = "teProfileMobilePassword";
            this.teProfileMobilePassword.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teProfileMobilePassword.Properties.Appearance.Options.UseFont = true;
            this.teProfileMobilePassword.Size = new System.Drawing.Size(209, 24);
            this.teProfileMobilePassword.TabIndex = 45;
            // 
            // lbModemPassword
            // 
            this.lbModemPassword.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemPassword.Location = new System.Drawing.Point(24, 143);
            this.lbModemPassword.Name = "lbModemPassword";
            this.lbModemPassword.Size = new System.Drawing.Size(45, 18);
            this.lbModemPassword.TabIndex = 44;
            this.lbModemPassword.Text = "Clave";
            // 
            // teProfileMobileUserName
            // 
            this.teProfileMobileUserName.EditValue = "MOVISTAR";
            this.teProfileMobileUserName.Location = new System.Drawing.Point(172, 111);
            this.teProfileMobileUserName.Name = "teProfileMobileUserName";
            this.teProfileMobileUserName.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teProfileMobileUserName.Properties.Appearance.Options.UseFont = true;
            this.teProfileMobileUserName.Size = new System.Drawing.Size(209, 24);
            this.teProfileMobileUserName.TabIndex = 43;
            // 
            // lbModemUserName
            // 
            this.lbModemUserName.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemUserName.Location = new System.Drawing.Point(24, 114);
            this.lbModemUserName.Name = "lbModemUserName";
            this.lbModemUserName.Size = new System.Drawing.Size(62, 18);
            this.lbModemUserName.TabIndex = 42;
            this.lbModemUserName.Text = "Usuario";
            // 
            // teProfileMobileName
            // 
            this.teProfileMobileName.EditValue = "MOVISTAR";
            this.teProfileMobileName.Location = new System.Drawing.Point(172, 55);
            this.teProfileMobileName.Name = "teProfileMobileName";
            this.teProfileMobileName.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teProfileMobileName.Properties.Appearance.Options.UseFont = true;
            this.teProfileMobileName.Size = new System.Drawing.Size(209, 24);
            this.teProfileMobileName.TabIndex = 41;
            // 
            // lbModemName
            // 
            this.lbModemName.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemName.Location = new System.Drawing.Point(23, 58);
            this.lbModemName.Name = "lbModemName";
            this.lbModemName.Size = new System.Drawing.Size(111, 18);
            this.lbModemName.TabIndex = 40;
            this.lbModemName.Text = "Nombre Perfil";
            // 
            // teProfileMobileAPN
            // 
            this.teProfileMobileAPN.Location = new System.Drawing.Point(172, 83);
            this.teProfileMobileAPN.Name = "teProfileMobileAPN";
            this.teProfileMobileAPN.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teProfileMobileAPN.Properties.Appearance.Options.UseFont = true;
            this.teProfileMobileAPN.Size = new System.Drawing.Size(209, 24);
            this.teProfileMobileAPN.TabIndex = 39;
            // 
            // lbModemDomain
            // 
            this.lbModemDomain.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModemDomain.Location = new System.Drawing.Point(23, 86);
            this.lbModemDomain.Name = "lbModemDomain";
            this.lbModemDomain.Size = new System.Drawing.Size(33, 18);
            this.lbModemDomain.TabIndex = 38;
            this.lbModemDomain.Text = "APN";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCancel.Appearance.Options.UseFont = true;
            this.cmdCancel.Image = global::gcperu.Server.UI.Properties.Resources.Return1;
            this.cmdCancel.Location = new System.Drawing.Point(8, 730);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(126, 37);
            this.cmdCancel.TabIndex = 1;
            this.cmdCancel.Text = "Cancelar";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOK.Appearance.Options.UseFont = true;
            this.cmdOK.Image = global::gcperu.Server.UI.Properties.Resources.MailSend;
            this.cmdOK.Location = new System.Drawing.Point(513, 730);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(271, 37);
            this.cmdOK.TabIndex = 2;
            this.cmdOK.Text = "GUARDAR PLANTILLA";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.EditValue = "";
            this.txtNombre.Location = new System.Drawing.Point(82, 12);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Properties.Appearance.Options.UseFont = true;
            this.txtNombre.Properties.MaxLength = 100;
            this.txtNombre.Size = new System.Drawing.Size(682, 24);
            this.txtNombre.TabIndex = 0;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl3.Location = new System.Drawing.Point(6, 15);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(70, 18);
            this.labelControl3.TabIndex = 111;
            this.labelControl3.Text = "NOMBRE";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl4.Location = new System.Drawing.Point(17, 47);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(44, 16);
            this.labelControl4.TabIndex = 113;
            this.labelControl4.Text = "Codigo";
            // 
            // txtCode
            // 
            this.txtCode.EditValue = "";
            this.txtCode.Location = new System.Drawing.Point(82, 44);
            this.txtCode.Name = "txtCode";
            this.txtCode.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCode.Properties.Appearance.Options.UseFont = true;
            this.txtCode.Size = new System.Drawing.Size(29, 22);
            this.txtCode.TabIndex = 1;
            // 
            // txtComentario
            // 
            this.txtComentario.EditValue = "";
            this.txtComentario.Location = new System.Drawing.Point(17, 103);
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComentario.Properties.Appearance.Options.UseFont = true;
            this.txtComentario.Properties.AutoHeight = false;
            this.txtComentario.Properties.MaxLength = 500;
            this.txtComentario.Size = new System.Drawing.Size(759, 47);
            this.txtComentario.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl5.Location = new System.Drawing.Point(17, 81);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(74, 16);
            this.labelControl5.TabIndex = 116;
            this.labelControl5.Text = "Comentario";
            // 
            // ckDefault
            // 
            this.ckDefault.Location = new System.Drawing.Point(604, 43);
            this.ckDefault.Name = "ckDefault";
            this.ckDefault.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.ckDefault.Properties.Appearance.Options.UseFont = true;
            this.ckDefault.Properties.Caption = "Predeterminada";
            this.ckDefault.Size = new System.Drawing.Size(160, 22);
            this.ckDefault.TabIndex = 129;
            // 
            // cbSoftwareTipo
            // 
            this.cbSoftwareTipo.EditValue = "Ninguno";
            this.cbSoftwareTipo.Location = new System.Drawing.Point(359, 44);
            this.cbSoftwareTipo.Name = "cbSoftwareTipo";
            this.cbSoftwareTipo.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSoftwareTipo.Properties.Appearance.Options.UseFont = true;
            this.cbSoftwareTipo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSoftwareTipo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbSoftwareTipo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSoftwareTipo.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbSoftwareTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSoftwareTipo.Properties.Items.AddRange(new object[] {
            "None",
            "Hourly",
            "Daily"});
            this.cbSoftwareTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbSoftwareTipo.Size = new System.Drawing.Size(207, 22);
            this.cbSoftwareTipo.TabIndex = 2;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Location = new System.Drawing.Point(157, 40);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(205, 30);
            this.labelControl17.TabIndex = 130;
            this.labelControl17.Text = "Valida para programa tipo:";
            // 
            // TemplateConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 775);
            this.Controls.Add(this.cbSoftwareTipo);
            this.Controls.Add(this.labelControl17);
            this.Controls.Add(this.ckDefault);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.txtComentario);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.tbControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "TemplateConfigurationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Plantilla de Configuraci�n de Terminales";
            this.Load += new System.EventHandler(this.TerminalOptionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbControl)).EndInit();
            this.tbControl.ResumeLayout(false);
            this.tpControlAdmin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbAdmin)).EndInit();
            this.tbAdmin.ResumeLayout(false);
            this.tbpAdminTerm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTerminal)).EndInit();
            this.gcTerminal.ResumeLayout(false);
            this.gcTerminal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntentosHotResetModem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntentosHotResetModem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinRecepcionGps.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinRecepcionGps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalMaxColaSalida.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalMaxColaSalida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTerminalTxDelayOn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbModoConexionInternet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinConexionServidor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinConexionServidor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinConexionInternet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalSinConexionInternet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLastShutDown.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDaysForShutDown.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbUserMonitorTimeout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTerminalModoColores.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDaysForReboot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalMaxDistancia.Properties)).EndInit();
            this.tbpAdminCnx.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcConexion)).EndInit();
            this.gcConexion.ResumeLayout(false);
            this.gcConexion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionIntentos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbConexionDefaultIP.Properties)).EndInit();
            this.tbpAdminFtpTrace.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).EndInit();
            this.gcTrace.ResumeLayout(false);
            this.gcTrace.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceProtocol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTracePFileRotate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileMaxSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFtp)).EndInit();
            this.gcFtp.ResumeLayout(false);
            this.gcFtp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdatePwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdateRemoteHost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUpdatePathRemote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPathRemote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFtpModoEnvioLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpRemoteHost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPort.Properties)).EndInit();
            this.tbpAdminVarios.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcGeneral)).EndInit();
            this.gcGeneral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ckNoFiltrarServiciosByLoginActivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckSE_ShowMarker.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckLoginAskActivityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckCatastrofeActivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckCambioVehiculoActivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckMntoActivo.Properties)).EndInit();
            this.frGenComida.ResumeLayout(false);
            this.frGenComida.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbcComidaDesactivarDespuesDe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcComidaDesactivarDespuesDe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckComidaAutoDesactivar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckComidaActivo.Properties)).EndInit();
            this.tbpProfile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcPerfil)).EndInit();
            this.gcPerfil.ResumeLayout(false);
            this.gcPerfil.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileForceUse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobilePassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teProfileMobileAPN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComentario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckDefault.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSoftwareTipo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tbControl;
        private DevExpress.XtraTab.XtraTabPage tpControlAdmin;
        private DevExpress.XtraTab.XtraTabControl tbAdmin;
        private DevExpress.XtraTab.XtraTabPage tbpAdminTerm;
        private DevExpress.XtraTab.XtraTabPage tbpAdminCnx;
        private DevExpress.XtraTab.XtraTabPage tbpAdminFtpTrace;
        private DevExpress.XtraEditors.GroupControl gcTrace;
        private DevExpress.XtraEditors.ComboBoxEdit cbTraceProtocol;
        private DevExpress.XtraEditors.LabelControl lbTraceProtocol;
        private DevExpress.XtraEditors.ComboBoxEdit cbTracePFileRotate;
        private DevExpress.XtraEditors.LabelControl lbTracePFileRotate;
        private DevExpress.XtraEditors.TextEdit teTracePFileName;
        private DevExpress.XtraEditors.LabelControl lbTracePFileName;
        private DevExpress.XtraEditors.TextEdit teTracePFileMaxSize;
        private DevExpress.XtraEditors.LabelControl lbTracePFileMaxSize;
        private DevExpress.XtraEditors.ComboBoxEdit cbTraceLevel;
        private DevExpress.XtraEditors.LabelControl lbTraceLevel;
        private DevExpress.XtraEditors.GroupControl gcFtp;
        private DevExpress.XtraEditors.ComboBoxEdit cbFtpModoEnvioLog;
        private DevExpress.XtraEditors.LabelControl lbFtpModoEnvio;
        private DevExpress.XtraEditors.LabelControl lbFtpLastDate;
        private DevExpress.XtraEditors.DateEdit deFtpLastDateLog;
        private DevExpress.XtraEditors.TextEdit teFtpRemoteHost;
        private DevExpress.XtraEditors.LabelControl lbFtpRemoteHost;
        private DevExpress.XtraEditors.TextEdit teFtpPort;
        private DevExpress.XtraEditors.LabelControl lbFtpPort;
        private DevExpress.XtraEditors.PanelControl gcTerminal;
        private MyTrackBarControl tcbTerminalIntervalSendCola;
        private MyTrackBarControl tbcTerminalMinWaitRsp;
        private MyTrackBarControl tbcTerminalMaxWaitRsp;
        private MyTrackBarControl tcbTerminalTxDelayOff;
        private MyTrackBarControl tbcTerminalSleepLogout;
        private MyTrackBarControl tbcTerminalUmbralVelocidadParada;
        private MyTrackBarControl tbcTerminalUmbralTempParada;
        private DevExpress.XtraEditors.LabelControl lbTerminalIntervalSendCola;
        private DevExpress.XtraEditors.LabelControl lbTerminalMinWaitRsp;
        private DevExpress.XtraEditors.LabelControl lbTerminalMaxWaitRsp;
        private DevExpress.XtraEditors.LabelControl lbTerminalTxDelayOn;
        private DevExpress.XtraEditors.LabelControl lbTerminalTxDelayOff;
        private DevExpress.XtraEditors.LabelControl lbTerminalSleepLogout;
        private DevExpress.XtraEditors.LabelControl lbTerminal;
        private DevExpress.XtraEditors.LabelControl lbTerminalUmbralTempParada;
        private DevExpress.XtraEditors.TextEdit teTerminalMaxDistancia;
        private DevExpress.XtraEditors.LabelControl lbTerminalMaxDistancia;
        private DevExpress.XtraEditors.PanelControl gcConexion;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort3;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP3;
        private DevExpress.XtraEditors.TextEdit teConexionServerName3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName3;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort2;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP2;
        private DevExpress.XtraEditors.TextEdit teConexionServerName2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName2;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort1;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP1;
        private DevExpress.XtraEditors.TextEdit teConexionServerName1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName1;
        private DevExpress.XtraEditors.TextEdit teConexionIntentos;
        private DevExpress.XtraEditors.LabelControl lbConexionIntentos;
        private DevExpress.XtraEditors.ComboBoxEdit cbConexionDefaultIP;
        private DevExpress.XtraEditors.LabelControl lbConexionDefaultPositioIP;
        private DevExpress.XtraEditors.SimpleButton cmdCancel;
        private DevExpress.XtraEditors.SimpleButton cmdOK;
        private DevExpress.XtraEditors.TextEdit teDaysForReboot;
        private DevExpress.XtraEditors.LabelControl lbDaysForReboot;
        private DevExpress.XtraEditors.ComboBoxEdit cbTerminalModoColores;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbUserMonitorTimeout;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lbLasShutDown;
        private DevExpress.XtraEditors.TextEdit teLastShutDown;
        private DevExpress.XtraEditors.LabelControl lbDaysForShutDown;
        private DevExpress.XtraEditors.TextEdit teDaysForShutDown;
        private DevExpress.XtraEditors.TextEdit txtNombre;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.TextEdit txtComentario;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraTab.XtraTabPage tbpAdminVarios;
        private DevExpress.XtraEditors.PanelControl gcGeneral;
        private DevExpress.XtraEditors.CheckEdit ckMntoActivo;
        private System.Windows.Forms.GroupBox frGenComida;
        private MyTrackBarControl tbcComidaDesactivarDespuesDe;
        private DevExpress.XtraEditors.CheckEdit ckComidaAutoDesactivar;
        private DevExpress.XtraEditors.CheckEdit ckComidaActivo;
        private DevExpress.XtraEditors.CheckEdit ckCambioVehiculoActivo;
        private DevExpress.XtraEditors.CheckEdit ckSE_ShowMarker;
        private DevExpress.XtraEditors.CheckEdit ckLoginAskActivityType;
        private DevExpress.XtraEditors.CheckEdit ckCatastrofeActivo;
        private DevExpress.XtraEditors.CheckEdit ckNoFiltrarServiciosByLoginActivo;
        private MyTrackBarControl tcbTerminalSinConexionServidor;
        private MyTrackBarControl tcbTerminalSinConexionInternet;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.ComboBoxEdit cbModoConexionInternet;
        private DevExpress.XtraEditors.TextEdit txtTerminalTxDelayOn;
        private DevExpress.XtraEditors.TextEdit teFtpPwd;
        private DevExpress.XtraEditors.TextEdit teFtpUser;
        private DevExpress.XtraEditors.TextEdit teUpdatePwd;
        private DevExpress.XtraEditors.TextEdit teUpdateUser;
        private DevExpress.XtraEditors.TextEdit teUpdateRemoteHost;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit teUpdatePathRemote;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit teFtpPathRemote;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private MyTrackBarControl tcbTerminalMaxColaSalida;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraTab.XtraTabPage tbpProfile;
        private DevExpress.XtraEditors.PanelControl gcPerfil;
        private DevExpress.XtraEditors.CheckEdit teProfileMobileForceUse;
        private DevExpress.XtraEditors.TextEdit teProfileMobileNumber;
        private DevExpress.XtraEditors.LabelControl lbModemNumber;
        private DevExpress.XtraEditors.TextEdit teProfileMobilePassword;
        private DevExpress.XtraEditors.LabelControl lbModemPassword;
        private DevExpress.XtraEditors.TextEdit teProfileMobileUserName;
        private DevExpress.XtraEditors.LabelControl lbModemUserName;
        private DevExpress.XtraEditors.TextEdit teProfileMobileName;
        private DevExpress.XtraEditors.LabelControl lbModemName;
        private DevExpress.XtraEditors.TextEdit teProfileMobileAPN;
        private DevExpress.XtraEditors.LabelControl lbModemDomain;
        private MyTrackBarControl tcbTerminalSinRecepcionGps;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private MyTrackBarControl tcbTerminalIntentosHotResetModem;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.CheckEdit ckDefault;
        private DevExpress.XtraEditors.ComboBoxEdit cbSoftwareTipo;
        private DevExpress.XtraEditors.LabelControl labelControl17;
    }
}