﻿namespace gcperu.Server.UI.Paneles.Forms
{
    partial class TemplateConfigurationListaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.Columns.GridColumn ID;
            this.dbgTemp = new DevExpress.XtraGrid.GridControl();
            this.bsTemp = new System.Windows.Forms.BindingSource(this.components);
            this.viewTempl = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNombre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpMmDesc = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colSoftwarTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cmdNew = new DevExpress.XtraEditors.SimpleButton();
            this.cmdRemove = new DevExpress.XtraEditors.SimpleButton();
            this.cmdEdit = new DevExpress.XtraEditors.SimpleButton();
            this.cmdDuplicar = new DevExpress.XtraEditors.SimpleButton();
            ID = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dbgTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewTempl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpMmDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ID
            // 
            ID.Caption = "ID";
            ID.FieldName = "Id";
            ID.Name = "ID";
            ID.OptionsColumn.AllowEdit = false;
            ID.Visible = true;
            ID.VisibleIndex = 0;
            ID.Width = 121;
            // 
            // dbgTemp
            // 
            this.dbgTemp.DataSource = this.bsTemp;
            this.dbgTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbgTemp.Location = new System.Drawing.Point(0, 48);
            this.dbgTemp.MainView = this.viewTempl;
            this.dbgTemp.Name = "dbgTemp";
            this.dbgTemp.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpMmDesc});
            this.dbgTemp.Size = new System.Drawing.Size(816, 318);
            this.dbgTemp.TabIndex = 1;
            this.dbgTemp.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewTempl});
            // 
            // viewTempl
            // 
            this.viewTempl.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gray;
            this.viewTempl.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Gray;
            this.viewTempl.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.viewTempl.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewTempl.Appearance.FocusedRow.Options.UseFont = true;
            this.viewTempl.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.viewTempl.Appearance.HeaderPanel.Options.UseFont = true;
            this.viewTempl.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.viewTempl.Appearance.Row.Options.UseFont = true;
            this.viewTempl.Appearance.TopNewRow.BackColor = System.Drawing.Color.PapayaWhip;
            this.viewTempl.Appearance.TopNewRow.Options.UseBackColor = true;
            this.viewTempl.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            ID,
            this.colNombre,
            this.colObs,
            this.colSoftwarTipo,
            this.colDefault});
            this.viewTempl.GridControl = this.dbgTemp;
            this.viewTempl.Name = "viewTempl";
            this.viewTempl.NewItemRowText = "Click aqui para agregar un nuevo grupo";
            this.viewTempl.OptionsBehavior.Editable = false;
            this.viewTempl.OptionsCustomization.AllowRowSizing = true;
            this.viewTempl.OptionsDetail.EnableMasterViewMode = false;
            this.viewTempl.OptionsView.RowAutoHeight = true;
            this.viewTempl.OptionsView.ShowDetailButtons = false;
            this.viewTempl.OptionsView.ShowFooter = true;
            this.viewTempl.OptionsView.ShowGroupPanel = false;
            this.viewTempl.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(ID, DevExpress.Data.ColumnSortOrder.Descending)});
            this.viewTempl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.viewFilters_KeyDown);
            // 
            // colNombre
            // 
            this.colNombre.Caption = "Nombre";
            this.colNombre.FieldName = "Nombre";
            this.colNombre.Name = "colNombre";
            this.colNombre.ToolTip = "Indica que este grupo pertenece a terminales que estan activos, y no son para pru" +
    "ebas o demos";
            this.colNombre.Visible = true;
            this.colNombre.VisibleIndex = 2;
            this.colNombre.Width = 517;
            // 
            // colObs
            // 
            this.colObs.AppearanceCell.Options.UseTextOptions = true;
            this.colObs.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colObs.Caption = "Observaciones";
            this.colObs.ColumnEdit = this.rpMmDesc;
            this.colObs.FieldName = "Comentario";
            this.colObs.Name = "colObs";
            this.colObs.Visible = true;
            this.colObs.VisibleIndex = 4;
            this.colObs.Width = 868;
            // 
            // rpMmDesc
            // 
            this.rpMmDesc.Name = "rpMmDesc";
            // 
            // colSoftwarTipo
            // 
            this.colSoftwarTipo.Caption = "Programa Tipo";
            this.colSoftwarTipo.FieldName = "SoftwareTipoEnum";
            this.colSoftwarTipo.Name = "colSoftwarTipo";
            this.colSoftwarTipo.Visible = true;
            this.colSoftwarTipo.VisibleIndex = 3;
            this.colSoftwarTipo.Width = 279;
            // 
            // colDefault
            // 
            this.colDefault.Caption = "Defecto";
            this.colDefault.FieldName = "Predeterminada";
            this.colDefault.Name = "colDefault";
            this.colDefault.Visible = true;
            this.colDefault.VisibleIndex = 1;
            this.colDefault.Width = 73;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cmdNew);
            this.panelControl1.Controls.Add(this.cmdRemove);
            this.panelControl1.Controls.Add(this.cmdEdit);
            this.panelControl1.Controls.Add(this.cmdDuplicar);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(816, 48);
            this.panelControl1.TabIndex = 2;
            // 
            // cmdNew
            // 
            this.cmdNew.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdNew.Appearance.Options.UseFont = true;
            this.cmdNew.Enabled = false;
            this.cmdNew.Image = global::gcperu.Server.UI.Properties.Resources.Grafico24;
            this.cmdNew.Location = new System.Drawing.Point(216, 12);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(128, 27);
            this.cmdNew.TabIndex = 4;
            this.cmdNew.Text = "Crear";
            this.cmdNew.Visible = false;
            // 
            // cmdRemove
            // 
            this.cmdRemove.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRemove.Appearance.Options.UseFont = true;
            this.cmdRemove.Image = global::gcperu.Server.UI.Properties.Resources.UnSelect16;
            this.cmdRemove.Location = new System.Drawing.Point(484, 12);
            this.cmdRemove.Name = "cmdRemove";
            this.cmdRemove.Size = new System.Drawing.Size(128, 27);
            this.cmdRemove.TabIndex = 3;
            this.cmdRemove.Text = "Eliminar";
            this.cmdRemove.Click += new System.EventHandler(this.cmdRemove_Click);
            // 
            // cmdEdit
            // 
            this.cmdEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEdit.Appearance.Options.UseFont = true;
            this.cmdEdit.Image = global::gcperu.Server.UI.Properties.Resources.ConfigEdit;
            this.cmdEdit.Location = new System.Drawing.Point(350, 12);
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Size = new System.Drawing.Size(128, 27);
            this.cmdEdit.TabIndex = 2;
            this.cmdEdit.Text = "Editar";
            this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click);
            // 
            // cmdDuplicar
            // 
            this.cmdDuplicar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDuplicar.Appearance.Options.UseFont = true;
            this.cmdDuplicar.Location = new System.Drawing.Point(12, 12);
            this.cmdDuplicar.Name = "cmdDuplicar";
            this.cmdDuplicar.Size = new System.Drawing.Size(128, 27);
            this.cmdDuplicar.TabIndex = 1;
            this.cmdDuplicar.Text = "Duplicar";
            this.cmdDuplicar.Click += new System.EventHandler(this.cmdDuplicar_Click);
            // 
            // TemplateConfigurationListaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 366);
            this.Controls.Add(this.dbgTemp);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TemplateConfigurationListaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lista de Plantillas de Configuración para los Terminales";
            ((System.ComponentModel.ISupportInitialize)(this.dbgTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewTempl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpMmDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl dbgTemp;
        private DevExpress.XtraGrid.Views.Grid.GridView viewTempl;
        private DevExpress.XtraGrid.Columns.GridColumn colObs;
        private System.Windows.Forms.BindingSource bsTemp;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colNombre;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit rpMmDesc;
        private DevExpress.XtraEditors.SimpleButton cmdNew;
        private DevExpress.XtraEditors.SimpleButton cmdRemove;
        private DevExpress.XtraEditors.SimpleButton cmdEdit;
        private DevExpress.XtraEditors.SimpleButton cmdDuplicar;
        private DevExpress.XtraGrid.Columns.GridColumn colSoftwarTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colDefault;
    }
}