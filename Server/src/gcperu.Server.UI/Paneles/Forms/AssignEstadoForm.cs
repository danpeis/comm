﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using gcperu.Contract;
using gcperu.Server.Core;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;
using gitspt.global.Core.Extension;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class AssignEstadoForm : DevExpress.XtraEditors.XtraForm
    {
        private List<decimal> _lista;
        private GrupoTerminalCol _listaGrupos;

        public bool Result { get; private set; }

        public AssignEstadoForm(List<decimal> listaTerminalIDs)
        {
            InitializeComponent();
            _lista = listaTerminalIDs;
            this.CenterToScreen();

            RellenarEstados();
        }

        private void RellenarEstados()
        {
            cbEstados.Properties.Items.Clear();
            cbEstados.Properties.Items.Add(OperationalDeviceStatus.Shutdown);
            cbEstados.Properties.Items.Add(OperationalDeviceStatus.AppClosed);
            cbEstados.Properties.Items.Add(OperationalDeviceStatus.Running);
            cbEstados.EditValue = OperationalDeviceStatus.Shutdown;

            cbEstados.Enabled = true;
            cbColor.Enabled = true;
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            string estadoAssing = "", colorAssign = "";


            estadoAssing = cbEstados.EditValue.ToString();
            colorAssign = cbColor.EditValue.ToString();

            var _terminalCol = new TerminalStateCol();
            try
            {
                //Obtener la lista de los Terminales.
                _terminalCol.Query.Where(_terminalCol.Query.TerminalID.In(_lista));

                _terminalCol.LoadAll();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("Error al obtener la lista de terminales para asignar los estados/colores.",ex));
                return;
            }

            try
            {
                //Asignamos el Grupo a los Terminales de la lista.
                foreach (var terminal in _terminalCol)
                {
                    if (estadoAssing!="")
                    {
                        terminal.PowerState = (short)estadoAssing.StringToEnum(OperationalDeviceStatus.Shutdown);
                        if (estadoAssing.StringToEnum(OperationalDeviceStatus.Shutdown) == OperationalDeviceStatus.Shutdown)
                        {
                            if (terminal.LoginFrom.HasValue)
                            {
                                terminal.LoginFrom = null;
                                terminal.LogoutFrom = DateTime.Now;
                                terminal.PersonalID = null;//Quitamos el Conductor.
                            }
                            terminal.TerminalConnectedFrom = null;
                            terminal.TerminalDisconnect = System.DateTime.Now;
                        }
                    }

                    if (colorAssign != "")
                    {
                        terminal.TerminalEstado = (int)colorAssign.StringToEnum(VehiculoEstado.Rojo);
                    }

                }

                //Asignamos los Grupos a la lista.
                _terminalCol.Save();

                Result = true;
                this.Close();

            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("Error al asignar los estados/colores a la lista de Terminales.", ex));
                return;
            }


        }

        private void cbEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbEstados.EditValue.ToString() == OperationalDeviceStatus.Shutdown.ToString())
                cbColor.EditValue = VehiculoEstado.Rojo.ToString();
        }
      
    }
}