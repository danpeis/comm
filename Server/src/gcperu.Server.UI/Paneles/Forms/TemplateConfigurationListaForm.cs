﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class TemplateConfigurationListaForm : DevExpress.XtraEditors.XtraForm
    {

        private TemplateConfigurationCol _listaTemplates;

        public TemplateConfigurationListaForm()
        {
            InitializeComponent();
            this.CenterToScreen();

            LoadTemplates();
        }

        private void LoadTemplates()
        {
            try
            {
                _listaTemplates = new TemplateConfigurationCol();
                _listaTemplates.LoadAll();

                bsTemp.DataSource = _listaTemplates;
            }
            catch (System.Exception e)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL OBTENER LA LISTA DE PLANTILLAS.", e));
            }
        }

        private void viewFilters_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Delete)
            {
                DeleteTemplate();
                return;
            }
        }

        private void DeleteTemplate()
        {
            if (viewTempl.SelectedRowsCount==0) return;

            if (MessageBox.Show("¿ELIMINAR PLANTILLA DE LA LISTA?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            try
            {
                //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
                foreach (int rowhandle in viewTempl.GetSelectedRows())
                {
                    var ts = bsTemp[viewTempl.GetDataSourceRowIndex(rowhandle)] as TemplateConfiguration;
                    if (ts == null) continue;

                    if (ts.Predeterminada)
                    {
                        //SI LA PLANTILLA ES PREDETERMINADA, HAY QUE REASIGNAR LA XDEFECTO
                        var rowdefault = _listaTemplates.OrderBy(r => r.Id).FirstOrDefault();
                        if (rowdefault != null)
                            rowdefault.Predeterminada = true;
                    }

                    bsTemp.RemoveCurrent();
                }

                _listaTemplates.Save();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR ELIMINAR FILTRO DE LA LISTA.", ex));
            }
        }

        private void cmdRemove_Click(object sender, EventArgs e)
        {
            DeleteTemplate();
        }

        private void cmdEdit_Click(object sender, EventArgs e)
        {
            if (viewTempl.SelectedRowsCount == 0)
            {
                MessageBox.Show("Seleccione un registro para su modificación.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var fedit = new Paneles.Forms.TemplateConfigurationForm(bsTemp[viewTempl.GetDataSourceRowIndex(viewTempl.FocusedRowHandle)] as TemplateConfiguration);
            fedit.ShowDialog();

            if (fedit.DialogResult==DialogResult.OK)
                LoadTemplates();
        }

        private void cmdDuplicar_Click(object sender, EventArgs e)
        {
            if (viewTempl.SelectedRowsCount == 0)
            {
                MessageBox.Show("Seleccione un registro para su Duplicación.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (MessageBox.Show("¿ DUPLICAR LA CONFIGURACIÓN SELECCIONADA ?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            try
            {
                var cnfgSource = bsTemp[viewTempl.GetDataSourceRowIndex(viewTempl.FocusedRowHandle)] as TemplateConfiguration;

                var cnfgCopy = new TemplateConfiguration();

                cnfgCopy.Codigo = -1;
                cnfgCopy.Predeterminada = cnfgSource.Predeterminada;
                cnfgCopy.SoftwareTipo = cnfgSource.SoftwareTipo;
                cnfgCopy.Nombre = string.Format("{0} - Copia",cnfgSource.Nombre);
                cnfgCopy.ConfigData = cnfgSource.ConfigData;
                cnfgCopy.Comentario = cnfgSource.Comentario;

                cnfgCopy.Save();

                LoadTemplates();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL DUPLICAR CONFIGURACIÓN.", ex));
            }


        }

    }
}