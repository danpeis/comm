﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using GCPeru.Server.Core.Settings;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {
        public LoginForm()
        {
            InitializeComponent();

			this.DialogResult=DialogResult.None;

            this.FormBorderStyle=FormBorderStyle.FixedDialog;
            this.CenterToScreen();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            if (txtClave.Text != "28051117")
            {
                txtClave.Text = "";
	            this.DialogResult=DialogResult.Cancel;
	            txtClave.Focus();
	            return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }

    public static class ValidateAdministrador
    {
        public static bool Validate()
        {
            if (ModuleConfig.Global.SessionAdminActive)
                return true;

            var f = new LoginForm();
	        f.ShowDialog();
			
            return (f.DialogResult == DialogResult.OK);
        }
    }
}