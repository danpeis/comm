﻿namespace gcperu.Server.UI.Paneles.Forms.Internal
{
    partial class ListaPeticionesEnviarTerminalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListaPeticionesEnviarTerminalForm));
            this.lbMatricula = new DevExpress.XtraEditors.LabelControl();
            this.lbFecha = new DevExpress.XtraEditors.LabelControl();
            this.bsLista = new System.Windows.Forms.BindingSource(this.components);
            this.dbgC = new DevExpress.XtraGrid.GridControl();
            this.dbgV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFecha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServicioID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeticionCodeEnum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTerminalID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehiculoID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehMatricula = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastTimeProcessed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIntentosProcessed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecibidaPorTerminal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoPasoHistoricoPor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoEnviados = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMotivoPasoHistorico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaEntroHistorico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.images = new DevExpress.Utils.ImageCollection(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.frFilter = new DevExpress.XtraEditors.GroupControl();
            this.cmdRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.cmdView = new DevExpress.XtraEditors.CheckButton();
            this.ucFechas = new Controls.IntervalDatesUC();
            ((System.ComponentModel.ISupportInitialize)(this.bsLista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.images)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frFilter)).BeginInit();
            this.frFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbMatricula
            // 
            this.lbMatricula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMatricula.Appearance.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMatricula.Appearance.Options.UseFont = true;
            this.lbMatricula.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbMatricula.Location = new System.Drawing.Point(1365, 27);
            this.lbMatricula.Name = "lbMatricula";
            this.lbMatricula.Size = new System.Drawing.Size(123, 25);
            this.lbMatricula.TabIndex = 95;
            this.lbMatricula.Text = "9999-XGH";
            // 
            // lbFecha
            // 
            this.lbFecha.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFecha.Appearance.Options.UseFont = true;
            this.lbFecha.Location = new System.Drawing.Point(640, 35);
            this.lbFecha.Name = "lbFecha";
            this.lbFecha.Size = new System.Drawing.Size(119, 16);
            this.lbFecha.TabIndex = 92;
            this.lbFecha.Text = "Fecha a Visualizar";
            // 
            // bsLista
            // 
            this.bsLista.DataSource = typeof(Core.DAL.GC.TerminalDataSend);
            // 
            // dbgC
            // 
            this.dbgC.DataSource = this.bsLista;
            this.dbgC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbgC.Location = new System.Drawing.Point(0, 69);
            this.dbgC.MainView = this.dbgV;
            this.dbgC.Name = "dbgC";
            this.dbgC.Size = new System.Drawing.Size(1500, 587);
            this.dbgC.TabIndex = 2;
            this.dbgC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dbgV});
            // 
            // dbgV
            // 
            this.dbgV.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbgV.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbgV.Appearance.HeaderPanel.Options.UseFont = true;
            this.dbgV.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbgV.Appearance.Row.Options.UseFont = true;
            this.dbgV.Appearance.TopNewRow.BackColor = System.Drawing.Color.PapayaWhip;
            this.dbgV.Appearance.TopNewRow.Options.UseBackColor = true;
            this.dbgV.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colFecha,
            this.colServicioID,
            this.colData,
            this.colPeticionCodeEnum,
            this.colTerminalID,
            this.colVehiculoID,
            this.colVehMatricula,
            this.colLastTimeProcessed,
            this.colIntentosProcessed,
            this.colRecibidaPorTerminal,
            this.colNoPasoHistoricoPor,
            this.colNoEnviados,
            this.colServerID,
            this.colMotivoPasoHistorico,
            this.colFechaEntroHistorico});
            this.dbgV.GridControl = this.dbgC;
            this.dbgV.Name = "dbgV";
            this.dbgV.NewItemRowText = "Click aqui para agregar un nuevo grupo";
            this.dbgV.OptionsBehavior.Editable = false;
            this.dbgV.OptionsDetail.EnableMasterViewMode = false;
            this.dbgV.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.dbgV.OptionsView.EnableAppearanceEvenRow = true;
            this.dbgV.OptionsView.ShowAutoFilterRow = true;
            this.dbgV.OptionsView.ShowDetailButtons = false;
            this.dbgV.OptionsView.ShowFooter = true;
            this.dbgV.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFecha, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            this.colId.Width = 39;
            // 
            // colFecha
            // 
            this.colFecha.DisplayFormat.FormatString = "dd/MM/yy HH:mm:ss";
            this.colFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colFecha.FieldName = "Fecha";
            this.colFecha.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.colFecha.Name = "colFecha";
            this.colFecha.Visible = true;
            this.colFecha.VisibleIndex = 1;
            this.colFecha.Width = 116;
            // 
            // colServicioID
            // 
            this.colServicioID.FieldName = "ServicioID";
            this.colServicioID.MaxWidth = 350;
            this.colServicioID.Name = "colServicioID";
            this.colServicioID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colServicioID.Visible = true;
            this.colServicioID.VisibleIndex = 2;
            this.colServicioID.Width = 101;
            // 
            // colData
            // 
            this.colData.FieldName = "Data";
            this.colData.MaxWidth = 350;
            this.colData.Name = "colData";
            this.colData.Visible = true;
            this.colData.VisibleIndex = 3;
            this.colData.Width = 155;
            // 
            // colPeticionCodeEnum
            // 
            this.colPeticionCodeEnum.AppearanceCell.Options.UseTextOptions = true;
            this.colPeticionCodeEnum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colPeticionCodeEnum.Caption = "Tipo";
            this.colPeticionCodeEnum.FieldName = "PeticionCodeEnum";
            this.colPeticionCodeEnum.Name = "colPeticionCodeEnum";
            this.colPeticionCodeEnum.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPeticionCodeEnum.Visible = true;
            this.colPeticionCodeEnum.VisibleIndex = 4;
            this.colPeticionCodeEnum.Width = 121;
            // 
            // colTerminalID
            // 
            this.colTerminalID.Caption = "Term.ID";
            this.colTerminalID.FieldName = "TerminalID";
            this.colTerminalID.Name = "colTerminalID";
            this.colTerminalID.Visible = true;
            this.colTerminalID.VisibleIndex = 5;
            this.colTerminalID.Width = 56;
            // 
            // colVehiculoID
            // 
            this.colVehiculoID.Caption = "Veh.ID";
            this.colVehiculoID.FieldName = "VehiculoID";
            this.colVehiculoID.Name = "colVehiculoID";
            this.colVehiculoID.Visible = true;
            this.colVehiculoID.VisibleIndex = 6;
            this.colVehiculoID.Width = 44;
            // 
            // colVehMatricula
            // 
            this.colVehMatricula.Caption = "Matricula";
            this.colVehMatricula.FieldName = "Matricula";
            this.colVehMatricula.Name = "colVehMatricula";
            this.colVehMatricula.Visible = true;
            this.colVehMatricula.VisibleIndex = 7;
            this.colVehMatricula.Width = 62;
            // 
            // colLastTimeProcessed
            // 
            this.colLastTimeProcessed.Caption = "Ultimo Envio";
            this.colLastTimeProcessed.DisplayFormat.FormatString = "dd/MM/yy HH:mm:ss";
            this.colLastTimeProcessed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colLastTimeProcessed.FieldName = "LastTimeProcessed";
            this.colLastTimeProcessed.Name = "colLastTimeProcessed";
            this.colLastTimeProcessed.Visible = true;
            this.colLastTimeProcessed.VisibleIndex = 8;
            this.colLastTimeProcessed.Width = 111;
            // 
            // colIntentosProcessed
            // 
            this.colIntentosProcessed.Caption = "Nº Intentos";
            this.colIntentosProcessed.FieldName = "IntentosProcessed";
            this.colIntentosProcessed.Name = "colIntentosProcessed";
            this.colIntentosProcessed.Visible = true;
            this.colIntentosProcessed.VisibleIndex = 9;
            this.colIntentosProcessed.Width = 58;
            // 
            // colRecibidaPorTerminal
            // 
            this.colRecibidaPorTerminal.Caption = "Nº Recibida";
            this.colRecibidaPorTerminal.FieldName = "RecibidaPorTerminal";
            this.colRecibidaPorTerminal.Name = "colRecibidaPorTerminal";
            this.colRecibidaPorTerminal.Visible = true;
            this.colRecibidaPorTerminal.VisibleIndex = 11;
            this.colRecibidaPorTerminal.Width = 70;
            // 
            // colNoPasoHistoricoPor
            // 
            this.colNoPasoHistoricoPor.FieldName = "NoPasoHistoricoPor";
            this.colNoPasoHistoricoPor.Name = "colNoPasoHistoricoPor";
            this.colNoPasoHistoricoPor.Visible = true;
            this.colNoPasoHistoricoPor.VisibleIndex = 12;
            this.colNoPasoHistoricoPor.Width = 116;
            // 
            // colNoEnviados
            // 
            this.colNoEnviados.FieldName = "NoEnviados";
            this.colNoEnviados.Name = "colNoEnviados";
            this.colNoEnviados.Visible = true;
            this.colNoEnviados.VisibleIndex = 14;
            // 
            // colServerID
            // 
            this.colServerID.FieldName = "ServerID";
            this.colServerID.Name = "colServerID";
            this.colServerID.Visible = true;
            this.colServerID.VisibleIndex = 15;
            this.colServerID.Width = 99;
            // 
            // colMotivoPasoHistorico
            // 
            this.colMotivoPasoHistorico.AppearanceCell.ForeColor = System.Drawing.Color.Red;
            this.colMotivoPasoHistorico.AppearanceCell.Options.UseForeColor = true;
            this.colMotivoPasoHistorico.AppearanceCell.Options.UseTextOptions = true;
            this.colMotivoPasoHistorico.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colMotivoPasoHistorico.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.colMotivoPasoHistorico.AppearanceHeader.Options.UseForeColor = true;
            this.colMotivoPasoHistorico.Caption = "Porque en Historico?";
            this.colMotivoPasoHistorico.FieldName = "MotivoEntroHistoricoEnum";
            this.colMotivoPasoHistorico.Name = "colMotivoPasoHistorico";
            this.colMotivoPasoHistorico.Visible = true;
            this.colMotivoPasoHistorico.VisibleIndex = 13;
            this.colMotivoPasoHistorico.Width = 151;
            // 
            // colFechaEntroHistorico
            // 
            this.colFechaEntroHistorico.Caption = "=> Historico";
            this.colFechaEntroHistorico.DisplayFormat.FormatString = "dd/MM/yy HH:mm:ss";
            this.colFechaEntroHistorico.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colFechaEntroHistorico.FieldName = "LastTimeProcessed";
            this.colFechaEntroHistorico.Name = "colFechaEntroHistorico";
            this.colFechaEntroHistorico.Visible = true;
            this.colFechaEntroHistorico.VisibleIndex = 10;
            this.colFechaEntroHistorico.Width = 105;
            // 
            // images
            // 
            this.images.ImageSize = new System.Drawing.Size(24, 24);
            this.images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("images.ImageStream")));
            this.images.Images.SetKeyName(0, "Refresh24.png");
            // 
            // panelControl1
            // 
            this.panelControl1.Location = new System.Drawing.Point(267, 75);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(95, 36);
            this.panelControl1.TabIndex = 97;
            // 
            // frFilter
            // 
            this.frFilter.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frFilter.Appearance.Options.UseFont = true;
            this.frFilter.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frFilter.AppearanceCaption.Options.UseFont = true;
            this.frFilter.Controls.Add(this.cmdRefresh);
            this.frFilter.Controls.Add(this.cmdView);
            this.frFilter.Controls.Add(this.ucFechas);
            this.frFilter.Controls.Add(this.lbMatricula);
            this.frFilter.Controls.Add(this.lbFecha);
            this.frFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.frFilter.Enabled = false;
            this.frFilter.Location = new System.Drawing.Point(0, 0);
            this.frFilter.Name = "frFilter";
            this.frFilter.Size = new System.Drawing.Size(1500, 69);
            this.frFilter.TabIndex = 98;
            this.frFilter.Text = "Filtros";
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cmdRefresh.Appearance.Options.UseFont = true;
            this.cmdRefresh.ImageIndex = 0;
            this.cmdRefresh.ImageList = this.images;
            this.cmdRefresh.Location = new System.Drawing.Point(387, 28);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(68, 32);
            this.cmdRefresh.TabIndex = 98;
            this.cmdRefresh.Text = "   F5";
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // cmdView
            // 
            this.cmdView.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdView.Appearance.Options.UseFont = true;
            this.cmdView.Location = new System.Drawing.Point(23, 28);
            this.cmdView.Name = "cmdView";
            this.cmdView.Size = new System.Drawing.Size(275, 32);
            this.cmdView.TabIndex = 97;
            this.cmdView.Text = "Ver Historico";
            this.cmdView.CheckedChanged += new System.EventHandler(this.cmdView_CheckedChanged);
            // 
            // ucFechas
            // 
            this.ucFechas.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucFechas.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucFechas.Appearance.Options.UseBackColor = true;
            this.ucFechas.Appearance.Options.UseFont = true;
            this.ucFechas.AutoSize = true;
            this.ucFechas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ucFechas.ButtonMesVisible = false;
            this.ucFechas.ButtonSemanaVisible = false;
            this.ucFechas.ButtonTodasVisible = false;
            this.ucFechas.ButtonTrimestreVisible = false;
            this.ucFechas.Location = new System.Drawing.Point(768, 32);
            this.ucFechas.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.ucFechas.Name = "ucFechas";
            this.ucFechas.PeriodoDefault = UI.Controls.IntervalDatesUC.PeriodosFecha.Dia;
            this.ucFechas.Size = new System.Drawing.Size(397, 25);
            this.ucFechas.TabIndex = 96;
            this.ucFechas.ValueChanged += new System.EventHandler<Controls.IntervalDatesChangedEventArgs>(this.ucFechas_ValueChanged);
            // 
            // ListaPeticionesEnviarTerminalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1500, 656);
            this.Controls.Add(this.dbgC);
            this.Controls.Add(this.frFilter);
            this.Controls.Add(this.panelControl1);
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "ListaPeticionesEnviarTerminalForm";
            this.Text = "Lista Peticiones de Envio para el Vehiculo \'{0}\'";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListaPeticionesEnviarTerminalForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.bsLista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.images)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frFilter)).EndInit();
            this.frFilter.ResumeLayout(false);
            this.frFilter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bsLista;
        private DevExpress.XtraGrid.GridControl dbgC;
        private DevExpress.XtraGrid.Views.Grid.GridView dbgV;
        private DevExpress.Utils.ImageCollection images;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl frFilter;
        private Controls.IntervalDatesUC ucFechas;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colFecha;
        private DevExpress.XtraGrid.Columns.GridColumn colServicioID;
        private DevExpress.XtraGrid.Columns.GridColumn colPeticionCodeEnum;
        private DevExpress.XtraGrid.Columns.GridColumn colTerminalID;
        private DevExpress.XtraGrid.Columns.GridColumn colVehiculoID;
        private DevExpress.XtraGrid.Columns.GridColumn colLastTimeProcessed;
        private DevExpress.XtraGrid.Columns.GridColumn colIntentosProcessed;
        private DevExpress.XtraGrid.Columns.GridColumn colRecibidaPorTerminal;
        private DevExpress.XtraGrid.Columns.GridColumn colNoPasoHistoricoPor;
        private DevExpress.XtraGrid.Columns.GridColumn colNoEnviados;
        private DevExpress.XtraGrid.Columns.GridColumn colServerID;
        private DevExpress.XtraGrid.Columns.GridColumn colData;
        private DevExpress.XtraGrid.Columns.GridColumn colMotivoPasoHistorico;
        private DevExpress.XtraGrid.Columns.GridColumn colVehMatricula;
        private DevExpress.XtraEditors.CheckButton cmdView;
        private DevExpress.XtraEditors.LabelControl lbFecha;
        private DevExpress.XtraEditors.LabelControl lbMatricula;
        private DevExpress.XtraEditors.SimpleButton cmdRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaEntroHistorico;
    }
}