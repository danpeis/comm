﻿using System;
using System.Windows.Forms;
using EntitySpaces.DynamicQuery;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.DAL.GCG;
using gcperu.Server.Core.DAL.GT;
using gcperu.Server.UI.Controls;
using gitspt.global.Core.Exceptions;
using ManejarExceptions = gcperu.Server.UI.Exception.ManejarExceptions;

namespace gcperu.Server.UI.Paneles.Forms.Internal
{
    public partial class ListaTerminalDataReceivedForm : DevExpress.XtraEditors.XtraForm
    {

        private decimal _vehid;
        
        private TerminalGpsInfoDataCol _listaDiario;
        private HTerminalGpsInfoDataCol _listaHist;

        public ListaTerminalDataReceivedForm(decimal vehid, string vehmatricula)
        {
            InitializeComponent();
            this.CenterToScreen();

            _vehid = vehid;
            lbMatricula.Text = vehmatricula;

            this.Text = string.Format(this.Text, vehmatricula);

            frFilter.Enabled = true;

            cmdView_CheckedChanged(null,null);

            this.WindowState=FormWindowState.Maximized;
        }

        private void LoadPeticionesHistorico()
        {
            var q = new HTerminalGpsInfoDataQ("TDS");
            var qper = new PersonalQ("PER");

            q.SelectAll().Select(qper.PerNombreCompleto.As("ConductorName"));
            q.LeftJoin(qper).On(q.PersonalID == qper.PerCodPK);

            q.Where(q.VehiculoID == _vehid && q.GpsFecha.Between(ucFechas.Desde, ucFechas.Hasta));
            q.OrderBy(q.GpsFecha.Descending);

            if (_listaHist == null)
                _listaHist = new HTerminalGpsInfoDataCol();

            _listaHist.Load(q);

            bsLista.DataSource = _listaHist;

        }

        private void LoadPeticionesDiario()
        {
            var q = new TerminalGpsInfoDataQ("TDS");
            var qper = new PersonalQ("PER");

            q.SelectAll().Select( qper.PerNombreCompleto.As("ConductorName"));
            q.LeftJoin(qper).On(q.PersonalID == qper.PerCodPK);

            q.Where(q.VehiculoID == _vehid && q.GpsFecha.Between(ucFechas.Desde, ucFechas.Hasta));
            q.OrderBy(q.GpsFecha.Descending);

            if (_listaDiario==null)
                _listaDiario = new TerminalGpsInfoDataCol();

            _listaDiario.Load(q);

            bsLista.DataSource = _listaDiario;
        }

        private void ucFechas_ValueChanged(object sender, IntervalDatesChangedEventArgs e)
        {
            try
            {
                cmdRefresh.PerformClick();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(ex, "", ExceptionErrorLevel.Error, false, true);
            }
        }

        private void cmdView_CheckedChanged(object sender, EventArgs e)
        {
            if (!cmdView.Checked)
            {
                //DIARIO
                LoadPeticionesDiario();
                cmdView.Text = "Ver Historico";
            }
            else
            {
                //HISTORICO
                cmdView.Text = "Ver Diario";

                LoadPeticionesHistorico();
            }

        }

        private void ListaPeticionesEnviarTerminalForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                cmdRefresh.PerformClick();
            }
        }

        private void cmdRefresh_Click(object sender, EventArgs e)
        {
            if (!cmdView.Checked)
            {
                //DIARIO
                LoadPeticionesDiario();
            }
            else
            {
                //HISTORICO
                LoadPeticionesHistorico();
            }
        }
    }

    
}