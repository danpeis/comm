﻿using gcperu.Server.UI.Controls;

namespace gcperu.Server.UI.Paneles.Forms.Internal
{
    partial class ListaTerminalDataReceivedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListaTerminalDataReceivedForm));
            this.lbMatricula = new DevExpress.XtraEditors.LabelControl();
            this.lbFecha = new DevExpress.XtraEditors.LabelControl();
            this.bsLista = new System.Windows.Forms.BindingSource(this.components);
            this.dbgC = new DevExpress.XtraGrid.GridControl();
            this.dbgV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colServerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTerminalEstado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConductor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrigenTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumSec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServicioID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServicioEstado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaServerGet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGpsFecha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGpsFixGeoTipoVia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGpsFixGeoTx = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGpsFixMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGpsVelocidad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGpsLatitud = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGpsLongitud = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOdometroTt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOdometroPc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstadoEspecial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.images = new DevExpress.Utils.ImageCollection(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.frFilter = new DevExpress.XtraEditors.GroupControl();
            this.cmdRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.cmdView = new DevExpress.XtraEditors.CheckButton();
            this.ucFechas = new Controls.IntervalDatesUC();
            ((System.ComponentModel.ISupportInitialize)(this.bsLista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.images)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frFilter)).BeginInit();
            this.frFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbMatricula
            // 
            this.lbMatricula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMatricula.Appearance.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMatricula.Appearance.Options.UseFont = true;
            this.lbMatricula.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbMatricula.Location = new System.Drawing.Point(1416, 27);
            this.lbMatricula.Name = "lbMatricula";
            this.lbMatricula.Size = new System.Drawing.Size(123, 25);
            this.lbMatricula.TabIndex = 95;
            this.lbMatricula.Text = "9999-XGH";
            // 
            // lbFecha
            // 
            this.lbFecha.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFecha.Appearance.Options.UseFont = true;
            this.lbFecha.Location = new System.Drawing.Point(640, 35);
            this.lbFecha.Name = "lbFecha";
            this.lbFecha.Size = new System.Drawing.Size(119, 16);
            this.lbFecha.TabIndex = 92;
            this.lbFecha.Text = "Fecha a Visualizar";
            // 
            // bsLista
            // 
            this.bsLista.DataSource = typeof(Core.DAL.GC.TerminalGpsInfoData);
            // 
            // dbgC
            // 
            this.dbgC.DataSource = this.bsLista;
            this.dbgC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbgC.Location = new System.Drawing.Point(0, 69);
            this.dbgC.MainView = this.dbgV;
            this.dbgC.Name = "dbgC";
            this.dbgC.Size = new System.Drawing.Size(1551, 567);
            this.dbgC.TabIndex = 2;
            this.dbgC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dbgV});
            // 
            // dbgV
            // 
            this.dbgV.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gray;
            this.dbgV.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Gray;
            this.dbgV.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbgV.Appearance.FocusedRow.Options.UseBackColor = true;
            this.dbgV.Appearance.FocusedRow.Options.UseFont = true;
            this.dbgV.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbgV.Appearance.HeaderPanel.Options.UseFont = true;
            this.dbgV.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbgV.Appearance.Row.Options.UseFont = true;
            this.dbgV.Appearance.TopNewRow.BackColor = System.Drawing.Color.PapayaWhip;
            this.dbgV.Appearance.TopNewRow.Options.UseBackColor = true;
            this.dbgV.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colServerID,
            this.colTerminalEstado,
            this.colConductor,
            this.colOrigenTipo,
            this.colNumSec,
            this.colServicioID,
            this.colServicioEstado,
            this.colFechaServerGet,
            this.colGpsFecha,
            this.colGpsFixGeoTipoVia,
            this.colGpsFixGeoTx,
            this.colGpsFixMode,
            this.colGpsVelocidad,
            this.colGpsLatitud,
            this.colGpsLongitud,
            this.colOdometroTt,
            this.colOdometroPc,
            this.colEstadoEspecial,
            this.colTipoActividad,
            this.colInputState,
            this.colOutputState});
            this.dbgV.GridControl = this.dbgC;
            this.dbgV.Name = "dbgV";
            this.dbgV.NewItemRowText = "Click aqui para agregar un nuevo grupo";
            this.dbgV.OptionsBehavior.Editable = false;
            this.dbgV.OptionsDetail.EnableMasterViewMode = false;
            this.dbgV.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.dbgV.OptionsView.EnableAppearanceEvenRow = true;
            this.dbgV.OptionsView.ShowAutoFilterRow = true;
            this.dbgV.OptionsView.ShowDetailButtons = false;
            this.dbgV.OptionsView.ShowFooter = true;
            this.dbgV.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGpsFecha, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colServerID
            // 
            this.colServerID.AppearanceCell.Options.UseTextOptions = true;
            this.colServerID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colServerID.FieldName = "ServerID";
            this.colServerID.Name = "colServerID";
            this.colServerID.Visible = true;
            this.colServerID.VisibleIndex = 0;
            this.colServerID.Width = 61;
            // 
            // colTerminalEstado
            // 
            this.colTerminalEstado.AppearanceCell.Options.UseTextOptions = true;
            this.colTerminalEstado.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colTerminalEstado.Caption = "Veh. Estado";
            this.colTerminalEstado.FieldName = "TerminalEstadoEnum";
            this.colTerminalEstado.Name = "colTerminalEstado";
            this.colTerminalEstado.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTerminalEstado.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTerminalEstado.Visible = true;
            this.colTerminalEstado.VisibleIndex = 1;
            this.colTerminalEstado.Width = 80;
            // 
            // colConductor
            // 
            this.colConductor.Caption = "Conductor";
            this.colConductor.FieldName = "ConductorName";
            this.colConductor.Name = "colConductor";
            this.colConductor.Visible = true;
            this.colConductor.VisibleIndex = 2;
            this.colConductor.Width = 98;
            // 
            // colOrigenTipo
            // 
            this.colOrigenTipo.Caption = "Trama Tipo";
            this.colOrigenTipo.FieldName = "OrigenTipoEnum";
            this.colOrigenTipo.Name = "colOrigenTipo";
            this.colOrigenTipo.Visible = true;
            this.colOrigenTipo.VisibleIndex = 3;
            this.colOrigenTipo.Width = 73;
            // 
            // colNumSec
            // 
            this.colNumSec.Caption = "Nº Secuencia";
            this.colNumSec.FieldName = "NumSec";
            this.colNumSec.Name = "colNumSec";
            this.colNumSec.Visible = true;
            this.colNumSec.VisibleIndex = 4;
            this.colNumSec.Width = 86;
            // 
            // colServicioID
            // 
            this.colServicioID.AppearanceCell.Options.UseTextOptions = true;
            this.colServicioID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colServicioID.FieldName = "ServicioID";
            this.colServicioID.Name = "colServicioID";
            this.colServicioID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Like;
            this.colServicioID.Visible = true;
            this.colServicioID.VisibleIndex = 5;
            this.colServicioID.Width = 78;
            // 
            // colServicioEstado
            // 
            this.colServicioEstado.AppearanceCell.Options.UseTextOptions = true;
            this.colServicioEstado.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colServicioEstado.Caption = "Serv.Estado";
            this.colServicioEstado.FieldName = "ServicioEstadoEnum";
            this.colServicioEstado.Name = "colServicioEstado";
            this.colServicioEstado.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colServicioEstado.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colServicioEstado.Visible = true;
            this.colServicioEstado.VisibleIndex = 6;
            this.colServicioEstado.Width = 90;
            // 
            // colFechaServerGet
            // 
            this.colFechaServerGet.Caption = "Fecha Server";
            this.colFechaServerGet.DisplayFormat.FormatString = "dd/MM/yyy HH:mm:ss";
            this.colFechaServerGet.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colFechaServerGet.FieldName = "FechaServerGet";
            this.colFechaServerGet.Name = "colFechaServerGet";
            this.colFechaServerGet.Visible = true;
            this.colFechaServerGet.VisibleIndex = 7;
            this.colFechaServerGet.Width = 116;
            // 
            // colGpsFecha
            // 
            this.colGpsFecha.Caption = "Fecha Trama Terminal";
            this.colGpsFecha.DisplayFormat.FormatString = "dd/MM/yyy HH:mm:ss";
            this.colGpsFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colGpsFecha.FieldName = "GpsFecha";
            this.colGpsFecha.Name = "colGpsFecha";
            this.colGpsFecha.Visible = true;
            this.colGpsFecha.VisibleIndex = 8;
            this.colGpsFecha.Width = 123;
            // 
            // colGpsFixGeoTipoVia
            // 
            this.colGpsFixGeoTipoVia.AppearanceCell.Options.UseTextOptions = true;
            this.colGpsFixGeoTipoVia.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colGpsFixGeoTipoVia.Caption = "Via Tipo";
            this.colGpsFixGeoTipoVia.FieldName = "GpsFixGeoTipoVia";
            this.colGpsFixGeoTipoVia.Name = "colGpsFixGeoTipoVia";
            this.colGpsFixGeoTipoVia.Visible = true;
            this.colGpsFixGeoTipoVia.VisibleIndex = 9;
            this.colGpsFixGeoTipoVia.Width = 50;
            // 
            // colGpsFixGeoTx
            // 
            this.colGpsFixGeoTx.Caption = "Georeferencia";
            this.colGpsFixGeoTx.FieldName = "GpsFixGeoTx";
            this.colGpsFixGeoTx.Name = "colGpsFixGeoTx";
            this.colGpsFixGeoTx.Visible = true;
            this.colGpsFixGeoTx.VisibleIndex = 10;
            this.colGpsFixGeoTx.Width = 80;
            // 
            // colGpsFixMode
            // 
            this.colGpsFixMode.AppearanceCell.Options.UseTextOptions = true;
            this.colGpsFixMode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colGpsFixMode.Caption = "Gps Calidad";
            this.colGpsFixMode.FieldName = "GpsFixMode";
            this.colGpsFixMode.Name = "colGpsFixMode";
            this.colGpsFixMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colGpsFixMode.Visible = true;
            this.colGpsFixMode.VisibleIndex = 11;
            this.colGpsFixMode.Width = 45;
            // 
            // colGpsVelocidad
            // 
            this.colGpsVelocidad.Caption = "Velocidad";
            this.colGpsVelocidad.DisplayFormat.FormatString = "{0:n0}";
            this.colGpsVelocidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGpsVelocidad.FieldName = "GpsVelocidad";
            this.colGpsVelocidad.Name = "colGpsVelocidad";
            this.colGpsVelocidad.Visible = true;
            this.colGpsVelocidad.VisibleIndex = 12;
            this.colGpsVelocidad.Width = 35;
            // 
            // colGpsLatitud
            // 
            this.colGpsLatitud.AppearanceCell.Options.UseTextOptions = true;
            this.colGpsLatitud.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colGpsLatitud.Caption = "Lat";
            this.colGpsLatitud.FieldName = "GpsLatitud";
            this.colGpsLatitud.Name = "colGpsLatitud";
            this.colGpsLatitud.Visible = true;
            this.colGpsLatitud.VisibleIndex = 13;
            this.colGpsLatitud.Width = 32;
            // 
            // colGpsLongitud
            // 
            this.colGpsLongitud.AppearanceCell.Options.UseTextOptions = true;
            this.colGpsLongitud.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colGpsLongitud.Caption = "Long";
            this.colGpsLongitud.FieldName = "GpsLongitud";
            this.colGpsLongitud.Name = "colGpsLongitud";
            this.colGpsLongitud.Visible = true;
            this.colGpsLongitud.VisibleIndex = 14;
            this.colGpsLongitud.Width = 32;
            // 
            // colOdometroTt
            // 
            this.colOdometroTt.AppearanceCell.Options.UseTextOptions = true;
            this.colOdometroTt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colOdometroTt.Caption = "Odom.Total";
            this.colOdometroTt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOdometroTt.FieldName = "OdometroTt";
            this.colOdometroTt.Name = "colOdometroTt";
            this.colOdometroTt.Visible = true;
            this.colOdometroTt.VisibleIndex = 15;
            this.colOdometroTt.Width = 39;
            // 
            // colOdometroPc
            // 
            this.colOdometroPc.AppearanceCell.Options.UseTextOptions = true;
            this.colOdometroPc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colOdometroPc.Caption = "Odom.Par";
            this.colOdometroPc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOdometroPc.FieldName = "OdometroPc";
            this.colOdometroPc.Name = "colOdometroPc";
            this.colOdometroPc.Visible = true;
            this.colOdometroPc.VisibleIndex = 16;
            this.colOdometroPc.Width = 40;
            // 
            // colEstadoEspecial
            // 
            this.colEstadoEspecial.AppearanceCell.Options.UseTextOptions = true;
            this.colEstadoEspecial.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colEstadoEspecial.Caption = "Est.Especial";
            this.colEstadoEspecial.FieldName = "EstadoEspecialEnum";
            this.colEstadoEspecial.Name = "colEstadoEspecial";
            this.colEstadoEspecial.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colEstadoEspecial.Visible = true;
            this.colEstadoEspecial.VisibleIndex = 17;
            this.colEstadoEspecial.Width = 56;
            // 
            // colTipoActividad
            // 
            this.colTipoActividad.AppearanceCell.Options.UseTextOptions = true;
            this.colTipoActividad.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colTipoActividad.Caption = "Tipo Actividad";
            this.colTipoActividad.FieldName = "TipoActividadName";
            this.colTipoActividad.Name = "colTipoActividad";
            this.colTipoActividad.Visible = true;
            this.colTipoActividad.VisibleIndex = 18;
            this.colTipoActividad.Width = 97;
            // 
            // colInputState
            // 
            this.colInputState.AppearanceCell.Options.UseTextOptions = true;
            this.colInputState.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colInputState.FieldName = "InputState";
            this.colInputState.MaxWidth = 50;
            this.colInputState.Name = "colInputState";
            this.colInputState.Visible = true;
            this.colInputState.VisibleIndex = 19;
            this.colInputState.Width = 50;
            // 
            // colOutputState
            // 
            this.colOutputState.AppearanceCell.Options.UseTextOptions = true;
            this.colOutputState.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colOutputState.FieldName = "OutputState";
            this.colOutputState.MaxWidth = 50;
            this.colOutputState.Name = "colOutputState";
            this.colOutputState.Visible = true;
            this.colOutputState.VisibleIndex = 20;
            this.colOutputState.Width = 50;
            // 
            // images
            // 
            this.images.ImageSize = new System.Drawing.Size(24, 24);
            this.images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("images.ImageStream")));
            this.images.Images.SetKeyName(0, "Refresh24.png");
            // 
            // panelControl1
            // 
            this.panelControl1.Location = new System.Drawing.Point(267, 75);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(95, 36);
            this.panelControl1.TabIndex = 97;
            // 
            // frFilter
            // 
            this.frFilter.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frFilter.Appearance.Options.UseFont = true;
            this.frFilter.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frFilter.AppearanceCaption.Options.UseFont = true;
            this.frFilter.Controls.Add(this.cmdRefresh);
            this.frFilter.Controls.Add(this.cmdView);
            this.frFilter.Controls.Add(this.ucFechas);
            this.frFilter.Controls.Add(this.lbMatricula);
            this.frFilter.Controls.Add(this.lbFecha);
            this.frFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.frFilter.Enabled = false;
            this.frFilter.Location = new System.Drawing.Point(0, 0);
            this.frFilter.Name = "frFilter";
            this.frFilter.Size = new System.Drawing.Size(1551, 69);
            this.frFilter.TabIndex = 98;
            this.frFilter.Text = "Filtros";
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cmdRefresh.Appearance.Options.UseFont = true;
            this.cmdRefresh.ImageIndex = 0;
            this.cmdRefresh.ImageList = this.images;
            this.cmdRefresh.Location = new System.Drawing.Point(336, 27);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(68, 32);
            this.cmdRefresh.TabIndex = 98;
            this.cmdRefresh.Text = "   F5";
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // cmdView
            // 
            this.cmdView.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdView.Appearance.Options.UseFont = true;
            this.cmdView.Location = new System.Drawing.Point(22, 27);
            this.cmdView.Name = "cmdView";
            this.cmdView.Size = new System.Drawing.Size(275, 32);
            this.cmdView.TabIndex = 97;
            this.cmdView.Text = "Ver Historico";
            this.cmdView.CheckedChanged += new System.EventHandler(this.cmdView_CheckedChanged);
            // 
            // ucFechas
            // 
            this.ucFechas.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucFechas.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucFechas.Appearance.Options.UseBackColor = true;
            this.ucFechas.Appearance.Options.UseFont = true;
            this.ucFechas.AutoSize = true;
            this.ucFechas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ucFechas.ButtonMesVisible = false;
            this.ucFechas.ButtonSemanaVisible = false;
            this.ucFechas.ButtonTodasVisible = false;
            this.ucFechas.ButtonTrimestreVisible = false;
            this.ucFechas.Location = new System.Drawing.Point(768, 32);
            this.ucFechas.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.ucFechas.Name = "ucFechas";
            this.ucFechas.PeriodoDefault = UI.Controls.IntervalDatesUC.PeriodosFecha.Dia;
            this.ucFechas.Size = new System.Drawing.Size(397, 25);
            this.ucFechas.TabIndex = 96;
            this.ucFechas.ValueChanged += new System.EventHandler<Controls.IntervalDatesChangedEventArgs>(this.ucFechas_ValueChanged);
            // 
            // ListaTerminalDataReceivedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1551, 636);
            this.Controls.Add(this.dbgC);
            this.Controls.Add(this.frFilter);
            this.Controls.Add(this.panelControl1);
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "ListaTerminalDataReceivedForm";
            this.Text = "Tramas Recibidas del Vehiculo \'{0}\'";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListaPeticionesEnviarTerminalForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.bsLista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.images)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frFilter)).EndInit();
            this.frFilter.ResumeLayout(false);
            this.frFilter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bsLista;
        private DevExpress.XtraGrid.GridControl dbgC;
        private DevExpress.XtraGrid.Views.Grid.GridView dbgV;
        private DevExpress.Utils.ImageCollection images;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl frFilter;
        private IntervalDatesUC ucFechas;
        private DevExpress.XtraEditors.CheckButton cmdView;
        private DevExpress.XtraEditors.LabelControl lbFecha;
        private DevExpress.XtraEditors.LabelControl lbMatricula;
        private DevExpress.XtraEditors.SimpleButton cmdRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn colServerID;
        private DevExpress.XtraGrid.Columns.GridColumn colTerminalEstado;
        private DevExpress.XtraGrid.Columns.GridColumn colConductor;
        private DevExpress.XtraGrid.Columns.GridColumn colOrigenTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colNumSec;
        private DevExpress.XtraGrid.Columns.GridColumn colServicioID;
        private DevExpress.XtraGrid.Columns.GridColumn colServicioEstado;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaServerGet;
        private DevExpress.XtraGrid.Columns.GridColumn colGpsFecha;
        private DevExpress.XtraGrid.Columns.GridColumn colGpsFixGeoTipoVia;
        private DevExpress.XtraGrid.Columns.GridColumn colGpsFixGeoTx;
        private DevExpress.XtraGrid.Columns.GridColumn colGpsFixMode;
        private DevExpress.XtraGrid.Columns.GridColumn colGpsVelocidad;
        private DevExpress.XtraGrid.Columns.GridColumn colGpsLatitud;
        private DevExpress.XtraGrid.Columns.GridColumn colGpsLongitud;
        private DevExpress.XtraGrid.Columns.GridColumn colOdometroTt;
        private DevExpress.XtraGrid.Columns.GridColumn colOdometroPc;
        private DevExpress.XtraGrid.Columns.GridColumn colEstadoEspecial;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoActividad;
        private DevExpress.XtraGrid.Columns.GridColumn colInputState;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputState;
    }
}