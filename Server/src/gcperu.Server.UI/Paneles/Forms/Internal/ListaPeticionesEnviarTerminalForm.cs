﻿using System;
using System.Windows.Forms;
using gcperu.Server.Core.DAL.GC;
using gitspt.global.Core.Exceptions;
using ManejarExceptions = gcperu.Server.UI.Exception.ManejarExceptions;

namespace gcperu.Server.UI.Paneles.Forms.Internal
{
    public partial class ListaPeticionesEnviarTerminalForm : DevExpress.XtraEditors.XtraForm
    {

        private decimal _vehid;

        private TerminalDataSendCol _listaDiario;
        private HistoricoTerminalDataSendCol _listaHist;

        public ListaPeticionesEnviarTerminalForm(decimal vehid,string vehmatricula)
        {
            InitializeComponent();
            this.CenterToScreen();

            _vehid = vehid;
            lbMatricula.Text = vehmatricula;

            this.Text = string.Format(this.Text, vehmatricula);

            frFilter.Enabled = true;

            cmdView_CheckedChanged(null,null);
        }

        private void LoadPeticionesHistorico()
        {
            var q = new HistoricoTerminalDataSendQ("TDS");
            var qveh = new Core.DAL.GT.VehiculoQ("VEH");

            q.SelectAll().Select(qveh.VehMatricula.As("Matricula"));
            q.InnerJoin(qveh).On(q.VehiculoID == qveh.VehCodPK);
            q.Where(q.VehiculoID == _vehid && q.Fecha.Between(ucFechas.Desde,ucFechas.Hasta));
            q.OrderBy(q.Fecha.Descending);

            if (_listaHist == null)
                _listaHist = new HistoricoTerminalDataSendCol();

            _listaHist.Load(q);

            bsLista.DataSource = _listaHist;

        }

        private void LoadPeticionesDiario()
        {
            var q = new TerminalDataSendQ("TDS");
            var qveh = new Core.DAL.GT.VehiculoQ("VEH");

            q.SelectAll().Select(qveh.VehMatricula.As("Matricula"));
            q.InnerJoin(qveh).On(q.VehiculoID == qveh.VehCodPK);
            q.Where(q.VehiculoID == _vehid);
            q.OrderBy(q.Fecha.Descending);

            if (_listaDiario==null)
                _listaDiario = new TerminalDataSendCol();

            _listaDiario.Load(q);

            bsLista.DataSource = _listaDiario;
        }

        private void ucFechas_ValueChanged(object sender, Controls.IntervalDatesChangedEventArgs e)
        {
            try
            {
                if (ucFechas.Desde.Value <= DateTime.Now.Date)
                {
                    LoadPeticionesHistorico();
                }
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(ex, "", ExceptionErrorLevel.Error, false, true);
            }
        }

        private void cmdView_CheckedChanged(object sender, EventArgs e)
        {
            if (!cmdView.Checked)
            {
                //DIARIO
                LoadPeticionesDiario();
                cmdView.Text = "Ver Historico";
                colMotivoPasoHistorico.Visible = false;
                colFechaEntroHistorico.Visible = false;
                lbFecha.Visible = false;
                ucFechas.Visible = false;
            }
            else
            {
                //HISTORICO
                cmdView.Text = "Ver Diario";
                LoadPeticionesHistorico();
                colMotivoPasoHistorico.Visible = true;
                colFechaEntroHistorico.Visible = true;
                lbFecha.Visible = true;
                ucFechas.Visible = true;
            }

            dbgV.BestFitColumns();
        }

        private void ListaPeticionesEnviarTerminalForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                cmdRefresh.PerformClick();
            }
        }

        private void cmdRefresh_Click(object sender, EventArgs e)
        {
            if (!cmdView.Checked)
            {
                //DIARIO
                LoadPeticionesDiario();
            }
            else
            {
                //HISTORICO
                LoadPeticionesHistorico();
            }
        }
    }

    
}