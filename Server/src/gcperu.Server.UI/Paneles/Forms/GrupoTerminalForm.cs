﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class GrupoTerminalForm : DevExpress.XtraEditors.XtraForm
    {

        private GrupoTerminalCol _listaGrupos;

        public GrupoTerminalForm()
        {
            InitializeComponent();

            ObtenerGrupos();
        }

        private void ObtenerGrupos()
        {
            try
            {
                _listaGrupos = new GrupoTerminalCol();
                _listaGrupos.LoadAll();

                bsTerm.DataSource = _listaGrupos;
            }
            catch (System.Exception e)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL OBTENER LAS GRUPOS DE TERMINALES.", e));
            }
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            try
            {
                _listaGrupos.Save();

                this.Close();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR AL GUARDAR LOS CAMBIOS EN LA LISTA DE GRUPOS.",ex));
            }

        }
    }
}