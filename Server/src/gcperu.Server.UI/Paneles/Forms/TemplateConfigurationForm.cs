using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EntitySpaces.Interfaces;
using gcperu.Contract;
using gcperu.Server.Core;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;
using gitspt.global.Core.Extension;
using gitspt.global.Core.Log;
using LevelLog = gitspt.global.Core.Log.LevelLog;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class TemplateConfigurationForm : DevExpress.XtraEditors.XtraForm
    {
        private Contract.Config.TerminalConfig _config;
        private TemplateConfiguration _templateConfiguration;

        private Contract.Config.TerminalConfig _hashNew;
        private TerminalState _ts;

        public TemplateConfigurationForm(TemplateConfiguration template)
        {
            InitializeComponent();

            if (template==null)
            {
                ChangeEnableAllControls(false);
                cmdOK.Enabled = false;
                throw new ArgumentException("Argumento Plantilla no ha sido pasado como parametro.");
            }

            try
            {
                _templateConfiguration = template;
                _config = template.ConfigData.FromJSON<Contract.Config.TerminalConfig>();
            }
            catch (System.Exception ex)
            {
                ChangeEnableAllControls(false);
                cmdOK.Enabled = false;
                throw new InvalidOperationException("ERROR AL CONVERTIR LA INFORMACI�N GUARDADA EN DATOS VALIDOS DE CONFIGURACI�N");
            }

            RellenarCombosEnum();
            AssigLimitsValues();
            RellenarControles();

            ChangeEnableAllControls(true);
        }

        private void AssigLimitsValues()
        {
            //TODO:TemplateConfigurationForm | AssigLimitsValues
        //    tbcTerminalMaxWaitRsp.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYMAXTIMEOUTWAITRSP.ToInt();
        //    tbcTerminalMaxWaitRsp.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYMAXTIMEOUTWAITRSP.ToInt();

        //    tcbTerminalTxDelayOff.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYENGINEOFF.ToInt();
        //    tcbTerminalTxDelayOff.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYENGINEOFF.ToInt();

        //    tbcTerminalMinWaitRsp.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYYMINTIMEOUTWAITRSP.ToInt();
        //    tbcTerminalMinWaitRsp.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYYMINTIMEOUTWAITRSP.ToInt();

        //    tcbTerminalIntervalSendCola.Properties.Minimum = Core.Config.LimitsValues.MinTerminalTXDELAYSENDCOLASALIDA.ToInt();
        //    tcbTerminalIntervalSendCola.Properties.Maximum = Core.Config.LimitsValues.MaxTerminalTXDELAYSENDCOLASALIDA.ToInt();

        //    tcbTerminalSinConexionServidor.Properties.Minimum = LimitsValues.MinTiempoSinConexionServidor.ToInt();
        //    tcbTerminalSinConexionServidor.Properties.Maximum = LimitsValues.MaxTiempoSinConexionServidor.ToInt();

        //    tcbTerminalSinConexionInternet.Properties.Minimum = LimitsValues.MinTiempoSinConexionInternet.ToInt();
        //    tcbTerminalSinConexionInternet.Properties.Maximum = LimitsValues.MaxTiempoSinConexionInternet.ToInt();

        //    tcbTerminalSinRecepcionGps.Properties.Minimum = LimitsValues.MinTiempoSinRecepcionGps.ToInt();
        //    tcbTerminalSinRecepcionGps.Properties.Maximum = LimitsValues.MaxTiempoSinRecepcionGps.ToInt();

        //    tcbTerminalIntentosHotResetModem.Properties.Minimum = LimitsValues.MinIntentosHotResetModem.ToInt();
        //    tcbTerminalIntentosHotResetModem.Properties.Maximum = LimitsValues.MaxIntentosHotResetModem.ToInt();

        //    tcbTerminalMaxColaSalida.Properties.Minimum = LimitsValues.MinTramasQueueOut.ToInt();
        //    tcbTerminalMaxColaSalida.Properties.Maximum = LimitsValues.MaxTramasQueueOut.ToInt();
        }


        private void RellenarControles()
        {
            //TODO:TemplateConfigurationForm | RellenarControles
            DateTime dateValue;

            //txtCode.Text = _templateConfiguration.Codigo.ToString();
            //txtNombre.Text = _templateConfiguration.Nombre;
            //txtComentario.Text = _templateConfiguration.Comentario;
            //cbSoftwareTipo.Text = _templateConfiguration.SoftwareTipo.NumToEnum( TerminalSoftwareTipo.GITSNavega).ToString();
            //ckDefault.Checked = _templateConfiguration.Predeterminada;

            ////Terminal
            //teTerminalMaxDistancia.Text = _config[Core.Config.Constantes.TerminalODOMETROMAXDISTANCIA2PUNTOS].ToString();
            //tbcTerminalUmbralTempParada.Value = _config[Core.Config.Constantes.TerminalPARADATIEMPOVELOCIDADPORDEBAJO].ToInt();
            //tbcTerminalUmbralVelocidadParada.Value = _config[Core.Config.Constantes.TerminalPARADAUMBRALVELOCIDAD].ToInt();
            //tbcTerminalSleepLogout.Value = _config[Core.Config.Constantes.TerminalSleepLogout].ToInt();
            //tcbTerminalTxDelayOff.Value = _config[Core.Config.Constantes.TerminalTXDELAYENGINEOFF].ToInt();
            //txtTerminalTxDelayOn.EditValue = _config[Core.Config.Constantes.TerminalTXDELAYENGINEON].ToInt();
            //tbcTerminalMaxWaitRsp.Value = _config[Core.Config.Constantes.TerminalTXDELAYMAXTIMEOUTWAITRSP].ToInt();
            //tbcTerminalMinWaitRsp.Value = _config[Core.Config.Constantes.TerminalTXDELAYYMINTIMEOUTWAITRSP].ToInt();
            //tcbTerminalIntervalSendCola.Value = _config[Core.Config.Constantes.TerminalTXDELAYSENDCOLASALIDA].ToInt();
            //teDaysForReboot.Text = _config.AsInt(Core.Config.Constantes.TerminalDaysForReboot).ToString();
            //cbTerminalModoColores.Text = _config[Core.Config.Constantes.TerminalModoVehiculoEstadoColores].ToString();
            //teDaysForShutDown.Text = _config[Core.Config.Constantes.TerminalMaxDaysToShutDown].ToString();
            //teLastShutDown.Text = _config[Core.Config.Constantes.TerminalDateLastShutDown].ToString();
            //tcbTerminalSinConexionInternet.Value = _config[Core.Config.Constantes.TerminalSINCONEXIONINTERNET_TIEMPOMAXIMO].ToInt();
            //tcbTerminalSinConexionServidor.Value = _config[Core.Config.Constantes.TerminalSINCONEXIONSERVIDOR_TIEMPOMAXIMO].ToInt();
            //tcbTerminalIntentosHotResetModem.Value = _config[Core.Config.Constantes.TerminalINTENTOS_HOT_RESET_MODEM].ToInt();
            //tcbTerminalSinRecepcionGps.Value = _config[Core.Config.Constantes.TerminalSinRecepcionGps_TIEMPOMAXIMO].ToInt();
            //cbModoConexionInternet.Text = _config[Core.Config.Constantes.TerminalModoConexionInternet].ToString();
            //tcbTerminalMaxColaSalida.Value=_config[Core.Config.Constantes.TerminalQueueOutMaxTramas].ToInt();

            ////Energia
            //cbUserMonitorTimeout.Text = _config[Core.Config.Constantes.TerminalMonitorTurnoffTimeout].ToString();
            
            ////FTP
            //teFtpRemoteHost.Text = _config[Core.Config.Constantes.FTPTerminalRemoteHost].ToString();
            //teFtpPort.Text = _config[Core.Config.Constantes.FTPPort].ToString();
            //teFtpPathRemote.Text = _config[Core.Config.Constantes.FTPRemotePath].ToString();
            //teFtpUser.Text = _config[Core.Config.Constantes.FTPRemoteUser].ToString();
            //teFtpPwd.Text = _config[Core.Config.Constantes.FTPRemotePwd].ToString();

            //if (DateTime.TryParse(_config[Core.Config.Constantes.FTPTerminalLastDateLogSend].ToString(), out dateValue))
            //    deFtpLastDateLog.DateTime = dateValue;
            //cbFtpModoEnvioLog.Text = _config[Core.Config.Constantes.FTPTerminalMODOEnvioLOG].ToString();

            ////UPDATES
            //teUpdateRemoteHost.Text = _config[Core.Config.Constantes.AutoUpdateRemoteHost].ToString();
            //teUpdatePathRemote.Text = _config[Core.Config.Constantes.AutoUpdateRemotePath].ToString();
            //teUpdateUser.Text = _config[Core.Config.Constantes.AutoUpdateUser].ToString();
            //teUpdatePwd.Text = _config[Core.Config.Constantes.AutoUpdatePwd].ToString();

            ////Trace
            //cbTraceLevel.Text = _config[Core.Config.Constantes.TraceLevel].ToString();
            //teTracePFileMaxSize.Text = _config[Core.Config.Constantes.TracePFileMaxSize].ToString();
            //teTracePFileName.Text = _config[Core.Config.Constantes.TracePFileName].ToString();
            //cbTracePFileRotate.Text = _config[Core.Config.Constantes.TracePFileRotateMode].ToString();
            //cbTraceProtocol.Text = _config[Core.Config.Constantes.TraceProtocol].ToString();

            ////Conexion
            //cbConexionDefaultIP.Text = _config[Core.Config.Constantes.ModemSERVERDefaultIP].ToString();
            //teConexionIntentos.Text = _config[Core.Config.Constantes.ModemSERVERNumConexIntentos].ToString();
            //teConexionServerName1.Text = _config[Core.Config.Constantes.ModemSERVERNAME1].ToString();
            //teConexionServerIP1.Text = _config[Core.Config.Constantes.ModemSERVERIP1].ToString();
            //teConexionServerPort1.Text = _config[Core.Config.Constantes.ModemSERVERPORT1].ToString();
            //teConexionServerName2.Text = _config[Core.Config.Constantes.ModemSERVERNAME2].ToString();
            //teConexionServerIP2.Text = _config[Core.Config.Constantes.ModemSERVERIP2].ToString();
            //teConexionServerPort2.Text = _config[Core.Config.Constantes.ModemSERVERPORT2].ToString();
            //teConexionServerName3.Text = _config[Core.Config.Constantes.ModemSERVERNAME3].ToString();
            //teConexionServerIP3.Text = _config[Core.Config.Constantes.ModemSERVERIP3].ToString();
            //teConexionServerPort3.Text = _config[Core.Config.Constantes.ModemSERVERPORT3].ToString();

            ////***** General ********************************
            ////Comida
            //ckComidaActivo.Checked = _config.AsBool(Core.Config.Constantes.TerminalComidaGestionActiva);
            //ckComidaAutoDesactivar.Checked = _config.AsBool(Core.Config.Constantes.TerminalComidaAutoDeactive);
            //tbcComidaDesactivarDespuesDe.Properties.Minimum = Core.Config.LimitsValues.MinTiempoEstadoComidaActivo.ToInt();
            //tbcComidaDesactivarDespuesDe.Properties.Maximum = Core.Config.LimitsValues.MaxTiempoEstadoComidaActivo.ToInt();
            //tbcComidaDesactivarDespuesDe.Value = _config.AsInt(Core.Config.Constantes.TerminalComidaMaximoTiempoActivo, Core.Config.LimitsValues.DefaultTiempoEstadoComidaActivo.ToInt());

            ////Gesti�n cambio de Vehiculo
            //ckCambioVehiculoActivo.Checked = _config.AsBool(Core.Config.Constantes.TerminalVehiculoCambioGestionActiva);
            ////Gesti�n Catastrofe
            //ckCatastrofeActivo.Checked = _config.AsBool(Core.Config.Constantes.TerminalVehiculoCatastrofeActiva);
            ////Gestion Mantenimiento
            //ckMntoActivo.Checked = _config.AsBool(Core.Config.Constantes.TerminalMantenimientoGestionActiva);
            ////Login Preguntar Tipo Actividad
            //ckLoginAskActivityType.Checked = _config.AsBool(Core.Config.Constantes.TerminalLoginTipoActividadActiva);
            ////ServicioEspecial Show Button Marker
            //ckSE_ShowMarker.Checked = _config.AsBool(Core.Config.Constantes.TerminalServEspShowButtonMarker);
            ////NoFiltrar Servicio by Login
            //ckNoFiltrarServiciosByLoginActivo.Checked = _config.AsBool(Core.Config.Constantes.TerminalNoFiltrarServicioByLogin);
            ////***********************************************

            ////***** PERFIL MOVIL ****************************
            //teProfileMobileForceUse.Checked = _config.AsBool(Core.Config.Constantes.DialupForceUse);
            //teProfileMobileName.Text = _config[Constantes.DialupName].ToString();
            //teProfileMobileAPN.Text = _config[Core.Config.Constantes.DialupAPN].ToString();
            //teProfileMobileUserName.Text = _config[Core.Config.Constantes.DialupUserName].ToString();
            //teProfileMobilePassword.Text = _config[Core.Config.Constantes.DialupUserPass].ToString();
            //teProfileMobileNumber.Text = _config[Core.Config.Constantes.DialupPhoneNumber].ToString();
            ////***********************************************

        }

        private void SaveValueControls()
        {

            ////VALIDACIONES.
            //if (txtTerminalTxDelayOn.EditValue.ToByte() == 0 ||
            //    txtTerminalTxDelayOn.EditValue.ToByte() < LimitsValues.MinTerminalTXDELAYENGINEON.ToInt() ||
            //    txtTerminalTxDelayOn.EditValue.ToByte() > LimitsValues.MaxTerminalTXDELAYENGINEON.ToInt())
            //{
            //    MessageBox.Show("Frecuencia Envio Tramas en Movimiento (seg.): Valor no v�lido.", "Aviso",
            //                    MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1,
            //                    MessageBoxOptions.DefaultDesktopOnly);
            //    return null;
            //}

            //if (string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtCode.Text) || string.IsNullOrEmpty(cbSoftwareTipo.Text.Trim()))
            //{
            //    MessageBox.Show("NOMBRE, TIPO DE SOFTWARE Y CODIGO SON DATOS OBLIGATORIOS.", "Aviso",
            //                    MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1,
            //                    MessageBoxOptions.DefaultDesktopOnly);
            //    return null;
            //}

            //var hdnew = new HashTableTrama();// Global.Device.Config.HashData;

            //_templateConfiguration.Codigo = txtCode.Text.ToInt();
            //_templateConfiguration.Nombre = txtNombre.Text;
            //_templateConfiguration.Comentario = txtComentario.Text;
            //_templateConfiguration.SoftwareTipo = (byte)cbSoftwareTipo.Text.StringToEnum(TerminalSoftwareTipo.GITSNavega);
            //_templateConfiguration.Predeterminada = ckDefault.Checked;
            
            //try
            //{
            //    //Terminal
            //    hdnew[Core.Config.Constantes.TerminalODOMETROMAXDISTANCIA2PUNTOS] = teTerminalMaxDistancia.Text.ToInt();
            //    hdnew[Core.Config.Constantes.TerminalPARADATIEMPOVELOCIDADPORDEBAJO] = tbcTerminalUmbralTempParada.Value;
            //    hdnew[Core.Config.Constantes.TerminalPARADAUMBRALVELOCIDAD] = tbcTerminalUmbralVelocidadParada.Value;
            //    hdnew[Core.Config.Constantes.TerminalSleepLogout] = tbcTerminalSleepLogout.Value;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYENGINEOFF] = tcbTerminalTxDelayOff.Value;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYENGINEON]=txtTerminalTxDelayOn.EditValue;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYMAXTIMEOUTWAITRSP] = tbcTerminalMaxWaitRsp.Value;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYYMINTIMEOUTWAITRSP] = tbcTerminalMinWaitRsp.Value;
            //    hdnew[Core.Config.Constantes.TerminalTXDELAYSENDCOLASALIDA] = tcbTerminalIntervalSendCola.Value;
            //    hdnew[Core.Config.Constantes.TerminalDaysForReboot] = teDaysForReboot.Text.ToInt();
            //    hdnew[Core.Config.Constantes.TerminalModoVehiculoEstadoColores] = cbTerminalModoColores.Text;
            //    hdnew[Core.Config.Constantes.TerminalMonitorTurnoffTimeout] = cbUserMonitorTimeout.Text;
            //    hdnew[Core.Config.Constantes.TerminalMaxDaysToShutDown] = teDaysForShutDown.Text;
            //    hdnew[Core.Config.Constantes.TerminalSINCONEXIONINTERNET_TIEMPOMAXIMO] = tcbTerminalSinConexionInternet.Value;
            //    hdnew[Core.Config.Constantes.TerminalSINCONEXIONSERVIDOR_TIEMPOMAXIMO] = tcbTerminalSinConexionServidor.Value;
            //    hdnew[Core.Config.Constantes.TerminalSinRecepcionGps_TIEMPOMAXIMO] = tcbTerminalSinRecepcionGps.Value;
            //    hdnew[Core.Config.Constantes.TerminalINTENTOS_HOT_RESET_MODEM] = tcbTerminalIntentosHotResetModem.Value;
            //    hdnew[Core.Config.Constantes.TerminalModoConexionInternet] = cbModoConexionInternet.Text;
            //    hdnew[Core.Config.Constantes.TerminalQueueOutMaxTramas] = tcbTerminalMaxColaSalida.Value;
                
            //    //FTP
            //    hdnew[Core.Config.Constantes.FTPTerminalRemoteHost] = teFtpRemoteHost.Text;
            //    hdnew[Core.Config.Constantes.FTPPort] = teFtpPort.Text.ToInt();
            //    hdnew[Core.Config.Constantes.FTPRemotePath] = teFtpPathRemote.Text;
            //    hdnew[Core.Config.Constantes.FTPRemoteUser] = teFtpUser.Text;
            //    hdnew[Core.Config.Constantes.FTPRemotePwd] = teFtpPwd.Text;
            //    hdnew[Core.Config.Constantes.FTPTerminalLastDateLogSend] = deFtpLastDateLog.DateTime;
            //    hdnew[Core.Config.Constantes.FTPTerminalMODOEnvioLOG] = cbFtpModoEnvioLog.Text;

            //    //UPDATES
            //    hdnew[Core.Config.Constantes.AutoUpdateRemoteHost] = teUpdateRemoteHost.Text;
            //    hdnew[Core.Config.Constantes.AutoUpdateUser] = teUpdateUser.Text;
            //    hdnew[Core.Config.Constantes.AutoUpdatePwd] = teUpdatePwd.Text;
            //    hdnew[Core.Config.Constantes.AutoUpdateRemotePath] = teUpdatePathRemote.Text;

            //    //Trace
            //    hdnew[Core.Config.Constantes.TraceLevel] =cbTraceLevel.Text;
            //    hdnew[Core.Config.Constantes.TracePFileMaxSize]=teTracePFileMaxSize.Text;
            //    hdnew[Core.Config.Constantes.TracePFileName]=teTracePFileName.Text ;
            //    hdnew[Core.Config.Constantes.TracePFileRotateMode] =cbTracePFileRotate.Text;
            //    hdnew[Core.Config.Constantes.TraceProtocol] =cbTraceProtocol.Text;

            //    //Conexion
            //    hdnew[Core.Config.Constantes.ModemSERVERDefaultIP]=cbConexionDefaultIP.Text.ToInt();
            //    hdnew[Core.Config.Constantes.ModemSERVERNumConexIntentos]=teConexionIntentos.Text.ToInt();
            //    hdnew[Core.Config.Constantes.ModemSERVERNAME1]=teConexionServerName1.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERIP1]=teConexionServerIP1.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERPORT1]=teConexionServerPort1.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERNAME2]=teConexionServerName2.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERIP2]=teConexionServerIP2.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERPORT2]=teConexionServerPort2.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERNAME3]=teConexionServerName3.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERIP3]=teConexionServerIP3.Text;
            //    hdnew[Core.Config.Constantes.ModemSERVERPORT3]=teConexionServerPort3.Text;

            //    //***** General ********************************
            //    //Comida
            //    hdnew[Core.Config.Constantes.TerminalComidaGestionActiva] = ckComidaActivo.Checked;
            //    hdnew[Core.Config.Constantes.TerminalComidaAutoDeactive] = ckComidaAutoDesactivar.Checked;
            //    hdnew[Core.Config.Constantes.TerminalComidaMaximoTiempoActivo] = tbcComidaDesactivarDespuesDe.Value;

            //    //Gesti�n cambio de Vehiculo
            //    hdnew[Core.Config.Constantes.TerminalVehiculoCambioGestionActiva] = ckCambioVehiculoActivo.Checked;
            //    //Gestion Mantenimiento
            //    hdnew[Core.Config.Constantes.TerminalMantenimientoGestionActiva] = ckMntoActivo.Checked;
            //    //Gesti�n Catastrofe
            //    hdnew[Core.Config.Constantes.TerminalVehiculoCatastrofeActiva] = ckCatastrofeActivo.Checked;
            //    //Login Preguntar Tipo Actividad
            //    hdnew[Core.Config.Constantes.TerminalLoginTipoActividadActiva] = ckLoginAskActivityType.Checked;
            //    //ServicioEspecial Show Button Marker
            //    hdnew[Core.Config.Constantes.TerminalServEspShowButtonMarker] = ckSE_ShowMarker.Checked;
            //    //NoFiltrar Servicio by Login
            //    hdnew[Core.Config.Constantes.TerminalNoFiltrarServicioByLogin] = ckNoFiltrarServiciosByLoginActivo.Checked;
            //    //***********************************************

            //    //***** PERFIL MOVIL ****************************
            //    hdnew[Core.Config.Constantes.DialupForceUse] = teProfileMobileForceUse.Checked;
            //    hdnew[Core.Config.Constantes.DialupName] = teProfileMobileName.Text;
            //    hdnew[Core.Config.Constantes.DialupAPN] = teProfileMobileAPN.Text;
            //    hdnew[Core.Config.Constantes.DialupUserName] = teProfileMobileUserName.Text;
            //    hdnew[Core.Config.Constantes.DialupUserPass] = teProfileMobilePassword.Text;
            //    hdnew[Core.Config.Constantes.DialupPhoneNumber] = teProfileMobileNumber.Text;
            //    //***********************************************

            //    return hdnew;

            //}
            //catch (System.Exception ex)
            //{
            //    TraceLog.LogException(new GCException("SaveValueControls ERROR. LA CONFIGURACION NO SE HA GUARDADO.", ex, this.GetType()));
            //    return null;
            //}
        }


        private void ChangeEnableAllControls(bool enable)
        {

            gcTerminal.Enabled = enable;
            gcConexion.Enabled = enable;
            gcFtp.Enabled = enable;
            gcTrace.Enabled = enable;
            gcGeneral.Enabled = enable;
            gcPerfil.Enabled = enable;

        }

       

        #region eventos

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("� CANCELAR LA EDICI�N SIN GUARDAR ?","Pregunta",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2)==DialogResult.No)
                return;

            this.Close();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("� GUARDAR LOS CAMBIOS EN LA CONFIGURACI�N ?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            //Serializamos los cambios, y obtenemos un string.
            //try
            //{
            //    //Obtenemos solo los cambios, que ha habido.
            //    var hsTemplate = SaveValueControls();
            //    if (hsTemplate==null)
            //        return;

            //    if (_templateConfiguration.ColumnModified(TemplateConfigurationMetadata.ColumnNames.Predeterminada))
            //    {
            //        if (MessageBox.Show("� ASIGNAR ESTA PLANTILLA COMO PREDETERMINADA ?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            //            return;
            //    }

            //    _templateConfiguration.ConfigData = hsTemplate.Serialize();

            //    TemplateConfiguration.SaveTemplate(_templateConfiguration);

            //    this.DialogResult=DialogResult.OK;

            //    Close();
            //}
            //catch (System.Exception ex)
            //{
            //    ManejarExceptions.ManejarException(new System.Exception("ERROR PRODUCIDO AL GUARDAR LOS CAMBIOS EN LA PLANTILLA.",ex));
            //}
        }

        #endregion

        private void RellenarCombosEnum()
        {

            cbTraceLevel.Properties.Items.Clear();
            cbTraceLevel.Properties.Items.AddRange(Enum.GetNames(typeof(LevelLog)));

            cbTraceProtocol.Properties.Items.Clear();
            cbTraceProtocol.Properties.Items.AddRange(Enum.GetNames(typeof(TraceProtocol)));

            cbSoftwareTipo.Properties.Items.Clear();
            cbSoftwareTipo.Properties.Items.AddRange(Enum.GetNames(typeof(TerminalSoftwareTipo)));

            cbTracePFileRotate.Properties.Items.Clear();
            cbTracePFileRotate.Properties.Items.AddRange(Enum.GetNames(typeof(TraceFileRotateModeEnum)));

            cbFtpModoEnvioLog.Properties.Items.Clear();
            cbFtpModoEnvioLog.Properties.Items.AddRange(Enum.GetNames(typeof(ModeEnvioLog)));

            //cbModoConexionInternet.Properties.Items.Clear();
            //cbModoConexionInternet.Properties.Items.AddRange(Enum.GetNames(typeof(ModoConexionInternet)));

            //cbUserMonitorTimeout.Properties.Items.Clear();
            //cbUserMonitorTimeout.Properties.Items.AddRange(Enum.GetNames(typeof(PowerMonitorTimeout)));
        }

        private void TerminalOptionForm_Load(object sender, EventArgs e)
        {

        }
    }
    

    
}
