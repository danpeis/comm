﻿using System;
using System.Windows.Forms;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.UI.Exception;
using GCPeru.Server.Core.Settings;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class AddFilterForm : DevExpress.XtraEditors.XtraForm
    {
        public AddFilterForm(string filter)
        {
            InitializeComponent();

            this.CenterToScreen();

            txtFilter.Text = filter;
            txtNombre.Focus();
        }


        private void cmdOk_Click(object sender, EventArgs e)
        {

            if (txtFilter.Text=="")
            {
                MessageBox.Show("Debe especificar el Filtro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation,
                                MessageBoxDefaultButton.Button1);
                return;
            }

            if (txtNombre.Text == "")
            {
                MessageBox.Show("Debe especificar un Nombre para el Filtro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation,
                                MessageBoxDefaultButton.Button1);
                return;
            }

            if (txtDesc.Text == "")
            {
                MessageBox.Show("Debe especificar una Descripción de la Utilidad o función del Filtro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation,
                                MessageBoxDefaultButton.Button1);
                return;
            }

            //AGREGAMOS EL FILTRO.
            var _newfilter = new FilterNamedInfo();
            try
            {
                //Obtener la lista de los Terminales.
                _newfilter.Id = ModuleConfig.UserConfig.ConsoleFiltersNamed.Count + 1;
                _newfilter.Filter = txtFilter.Text;
                _newfilter.Name = txtNombre.Text;
                _newfilter.Description = txtDesc.Text;

                ModuleConfig.UserConfig.Manager.Save(ModuleConfig.UserConfig);

                this.DialogResult=DialogResult.OK;
                this.Close();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new GCException("Error agregar nuevo filtro.",ex));
                return;
            }

        }
      
    }
}