﻿namespace gcperu.Server.UI.Paneles.Forms
{
    partial class IncidenciasAlertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cmdAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.listIncid = new DevExpress.XtraEditors.CheckedListBoxControl();
            ((System.ComponentModel.ISupportInitialize)(this.listIncid)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelControl1
            // 
            this.LabelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl1.Appearance.Options.UseFont = true;
            this.LabelControl1.Location = new System.Drawing.Point(12, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(339, 16);
            this.LabelControl1.TabIndex = 12;
            this.LabelControl1.Text = "Marque las incidencias que desea que el Sistema Gestione:";
            // 
            // cmdAceptar
            // 
            this.cmdAceptar.Location = new System.Drawing.Point(428, 362);
            this.cmdAceptar.Name = "cmdAceptar";
            this.cmdAceptar.Size = new System.Drawing.Size(127, 34);
            this.cmdAceptar.TabIndex = 11;
            this.cmdAceptar.Text = "Aceptar";
            this.cmdAceptar.ToolTip = "Guarda la configuración";
            this.cmdAceptar.Click += new System.EventHandler(this.cmdAceptar_Click);
            // 
            // listIncid
            // 
            this.listIncid.Location = new System.Drawing.Point(12, 35);
            this.listIncid.Name = "listIncid";
            this.listIncid.Size = new System.Drawing.Size(543, 321);
            this.listIncid.TabIndex = 10;
            // 
            // IncidenciasAlertForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 412);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.cmdAceptar);
            this.Controls.Add(this.listIncid);
            this.Name = "IncidenciasAlertForm";
            this.Text = "Selección de las Incidencias que envian Alertas";
            this.Load += new System.EventHandler(this.IncidenciasAlertForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listIncid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.SimpleButton cmdAceptar;
        internal DevExpress.XtraEditors.CheckedListBoxControl listIncid;
    }
}