﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using EntitySpaces.DynamicQuery;
using GComunica.Core;
using GComunica.Core.Exceptions;
using GComunica.Core.Info;
using GComunica.CoreServer.data.gc;
using GComunica.Core.Extension;
using System.Linq;

namespace gcperu.Server.UI.Paneles.Forms
{
    public partial class AutoLogoutEditForm : DevExpress.XtraEditors.XtraForm
    {

        private AutologoutInfo _item;

        public AutoLogoutEditForm(AutologoutInfo item)
        {
            InitializeComponent();
            _item = item;

            bsData.DataSource = item;
            frCriterios.Enabled = true;

            FillCheckedStatesAllowed();

            this.CenterToParent();
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {

            if (ckEnable.Checked)
            {
                if (ckTimeAllDay.Checked==false && txtTimeMin.EditValue.ToInt()==0)
                {
                    MessageBox.Show("Debe definir algun criterio.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button1);
                    return;
                }

                if (txtTimeElapse.EditValue.ToInt()==0)
                {
                    MessageBox.Show("Debe especificar 'Cerrar pasados ...'", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    txtTimeElapse.Focus();
                    return;
                }

                SetStatesAllowed();

            }

            bsData.EndEdit();

            //Simplemente cerramos y los datos se guardan en _item
            this.DialogResult=DialogResult.OK;
            this.Close();
        }

        private void FillCheckedStatesAllowed()
        {
            if (_item.StatesAllowed == null)
                return;

            foreach (VehiculoEstado state in _item.StatesAllowed)
            {
                var itemCombo = ckStatesAllowed.Items.OfType<CheckedListBoxItem>().FirstOrDefault(i => i.Description.Contains(state.ToString()));
                if (itemCombo!=null)
                    itemCombo.CheckState=CheckState.Checked;
            }
        }

        private void SetStatesAllowed()
        {
            if (_item.StatesAllowed != null)
                _item.StatesAllowed.Clear();
            else
            {
                _item.StatesAllowed=new List<VehiculoEstado>();
            }

            foreach (CheckedListBoxItem checkedItem in ckStatesAllowed.CheckedItems)
            {
               _item.StatesAllowed.Add(checkedItem.Value.ToInt().NumToEnum(VehiculoEstado.Verde));
            }
        }
      
    }
}