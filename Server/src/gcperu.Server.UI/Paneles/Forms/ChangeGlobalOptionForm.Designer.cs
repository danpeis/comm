using gcperu.Server.UI.Controls;

namespace gcperu.Server.UI.Paneles.Forms
{
    partial class ChangeGlobalOptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbControl = new DevExpress.XtraTab.XtraTabControl();
            this.tpControlAdmin = new DevExpress.XtraTab.XtraTabPage();
            this.tbAdmin = new DevExpress.XtraTab.XtraTabControl();
            this.tbpAdminTerm = new DevExpress.XtraTab.XtraTabPage();
            this.gcTerminal = new DevExpress.XtraEditors.PanelControl();
            this.lbLasShutDown = new DevExpress.XtraEditors.LabelControl();
            this.teLastShutDown = new DevExpress.XtraEditors.TextEdit();
            this.lbDaysForShutDown = new DevExpress.XtraEditors.LabelControl();
            this.teDaysForShutDown = new DevExpress.XtraEditors.TextEdit();
            this.cbUserMonitorTimeout = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cbTerminalModoColores = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.teDaysForReboot = new DevExpress.XtraEditors.TextEdit();
            this.lbDaysForReboot = new DevExpress.XtraEditors.LabelControl();
            this.tcbTerminalIntervalSendCola = new Controls.MyTrackBarControl();
            this.tbcTerminalMinWaitRsp = new Controls.MyTrackBarControl();
            this.tbcTerminalMaxWaitRsp = new Controls.MyTrackBarControl();
            this.tcbTerminalTxDelayOff = new Controls.MyTrackBarControl();
            this.tbcTerminalTxDelayOn = new Controls.MyTrackBarControl();
            this.tbcTerminalSleepLogout = new Controls.MyTrackBarControl();
            this.tbcTerminalUmbralVelocidadParada = new Controls.MyTrackBarControl();
            this.tbcTerminalUmbralTempParada = new Controls.MyTrackBarControl();
            this.lbTerminalIntervalSendCola = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalMinWaitRsp = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalMaxWaitRsp = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalTxDelayOn = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalTxDelayOff = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalSleepLogout = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminal = new DevExpress.XtraEditors.LabelControl();
            this.lbTerminalUmbralTempParada = new DevExpress.XtraEditors.LabelControl();
            this.teTerminalMaxDistancia = new DevExpress.XtraEditors.TextEdit();
            this.lbTerminalMaxDistancia = new DevExpress.XtraEditors.LabelControl();
            this.tbpAdminCnx = new DevExpress.XtraTab.XtraTabPage();
            this.gcConexion = new DevExpress.XtraEditors.PanelControl();
            this.teConexionServerPort3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName3 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName3 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerPort2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName2 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName2 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerPort1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerPort1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerIP1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerIP1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionServerName1 = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionServerName1 = new DevExpress.XtraEditors.LabelControl();
            this.teConexionIntentos = new DevExpress.XtraEditors.TextEdit();
            this.lbConexionIntentos = new DevExpress.XtraEditors.LabelControl();
            this.cbConexionDefaultIP = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbConexionDefaultPositioIP = new DevExpress.XtraEditors.LabelControl();
            this.tbpAdminFtpTrace = new DevExpress.XtraTab.XtraTabPage();
            this.gcTrace = new DevExpress.XtraEditors.GroupControl();
            this.cbTraceProtocol = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTraceProtocol = new DevExpress.XtraEditors.LabelControl();
            this.cbTracePFileRotate = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTracePFileRotate = new DevExpress.XtraEditors.LabelControl();
            this.teTracePFileName = new DevExpress.XtraEditors.TextEdit();
            this.lbTracePFileName = new DevExpress.XtraEditors.LabelControl();
            this.teTracePFileMaxSize = new DevExpress.XtraEditors.TextEdit();
            this.lbTracePFileMaxSize = new DevExpress.XtraEditors.LabelControl();
            this.cbTraceLevel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbTraceLevel = new DevExpress.XtraEditors.LabelControl();
            this.gcFtp = new DevExpress.XtraEditors.GroupControl();
            this.cbFtpModoEnvioLog = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbFtpModoEnvio = new DevExpress.XtraEditors.LabelControl();
            this.lbFtpLastDate = new DevExpress.XtraEditors.LabelControl();
            this.deFtpLastDateLog = new DevExpress.XtraEditors.DateEdit();
            this.teFtpRemoteHost = new DevExpress.XtraEditors.TextEdit();
            this.lbFtpRemoteHost = new DevExpress.XtraEditors.LabelControl();
            this.teFtpPort = new DevExpress.XtraEditors.TextEdit();
            this.lbFtpPort = new DevExpress.XtraEditors.LabelControl();
            this.cmdCancel = new DevExpress.XtraEditors.SimpleButton();
            this.cmdOK = new DevExpress.XtraEditors.SimpleButton();
            this.ckConfigFull = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl)).BeginInit();
            this.tbControl.SuspendLayout();
            this.tpControlAdmin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAdmin)).BeginInit();
            this.tbAdmin.SuspendLayout();
            this.tbpAdminTerm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTerminal)).BeginInit();
            this.gcTerminal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teLastShutDown.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDaysForShutDown.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbUserMonitorTimeout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTerminalModoColores.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDaysForReboot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalTxDelayOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalTxDelayOn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalMaxDistancia.Properties)).BeginInit();
            this.tbpAdminCnx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcConexion)).BeginInit();
            this.gcConexion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionIntentos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbConexionDefaultIP.Properties)).BeginInit();
            this.tbpAdminFtpTrace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).BeginInit();
            this.gcTrace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceProtocol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTracePFileRotate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileMaxSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFtp)).BeginInit();
            this.gcFtp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbFtpModoEnvioLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpRemoteHost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckConfigFull.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tbControl
            // 
            this.tbControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbControl.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbControl.AppearancePage.Header.Options.UseFont = true;
            this.tbControl.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbControl.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tbControl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbControl.Location = new System.Drawing.Point(4, 12);
            this.tbControl.Name = "tbControl";
            this.tbControl.SelectedTabPage = this.tpControlAdmin;
            this.tbControl.Size = new System.Drawing.Size(792, 473);
            this.tbControl.TabIndex = 0;
            this.tbControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpControlAdmin});
            // 
            // tpControlAdmin
            // 
            this.tpControlAdmin.AutoScroll = true;
            this.tpControlAdmin.AutoScrollMargin = new System.Drawing.Size(0, 20);
            this.tpControlAdmin.Controls.Add(this.tbAdmin);
            this.tpControlAdmin.Name = "tpControlAdmin";
            this.tpControlAdmin.Size = new System.Drawing.Size(784, 434);
            this.tpControlAdmin.Text = "Configuracion";
            // 
            // tbAdmin
            // 
            this.tbAdmin.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAdmin.AppearancePage.Header.Options.UseFont = true;
            this.tbAdmin.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAdmin.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbAdmin.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tbAdmin.AppearancePage.HeaderActive.Options.UseForeColor = true;
            this.tbAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbAdmin.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tbAdmin.Location = new System.Drawing.Point(0, 0);
            this.tbAdmin.Name = "tbAdmin";
            this.tbAdmin.PaintStyleName = "PropertyView";
            this.tbAdmin.SelectedTabPage = this.tbpAdminTerm;
            this.tbAdmin.Size = new System.Drawing.Size(784, 434);
            this.tbAdmin.TabIndex = 0;
            this.tbAdmin.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbpAdminTerm,
            this.tbpAdminCnx,
            this.tbpAdminFtpTrace});
            // 
            // tbpAdminTerm
            // 
            this.tbpAdminTerm.Controls.Add(this.gcTerminal);
            this.tbpAdminTerm.Name = "tbpAdminTerm";
            this.tbpAdminTerm.Size = new System.Drawing.Size(782, 403);
            this.tbpAdminTerm.Text = "Terminal";
            // 
            // gcTerminal
            // 
            this.gcTerminal.Controls.Add(this.lbLasShutDown);
            this.gcTerminal.Controls.Add(this.teLastShutDown);
            this.gcTerminal.Controls.Add(this.lbDaysForShutDown);
            this.gcTerminal.Controls.Add(this.teDaysForShutDown);
            this.gcTerminal.Controls.Add(this.cbUserMonitorTimeout);
            this.gcTerminal.Controls.Add(this.labelControl2);
            this.gcTerminal.Controls.Add(this.cbTerminalModoColores);
            this.gcTerminal.Controls.Add(this.labelControl1);
            this.gcTerminal.Controls.Add(this.teDaysForReboot);
            this.gcTerminal.Controls.Add(this.lbDaysForReboot);
            this.gcTerminal.Controls.Add(this.tcbTerminalIntervalSendCola);
            this.gcTerminal.Controls.Add(this.tbcTerminalMinWaitRsp);
            this.gcTerminal.Controls.Add(this.tbcTerminalMaxWaitRsp);
            this.gcTerminal.Controls.Add(this.tcbTerminalTxDelayOff);
            this.gcTerminal.Controls.Add(this.tbcTerminalTxDelayOn);
            this.gcTerminal.Controls.Add(this.tbcTerminalSleepLogout);
            this.gcTerminal.Controls.Add(this.tbcTerminalUmbralVelocidadParada);
            this.gcTerminal.Controls.Add(this.tbcTerminalUmbralTempParada);
            this.gcTerminal.Controls.Add(this.lbTerminalIntervalSendCola);
            this.gcTerminal.Controls.Add(this.lbTerminalMinWaitRsp);
            this.gcTerminal.Controls.Add(this.lbTerminalMaxWaitRsp);
            this.gcTerminal.Controls.Add(this.lbTerminalTxDelayOn);
            this.gcTerminal.Controls.Add(this.lbTerminalTxDelayOff);
            this.gcTerminal.Controls.Add(this.lbTerminalSleepLogout);
            this.gcTerminal.Controls.Add(this.lbTerminal);
            this.gcTerminal.Controls.Add(this.lbTerminalUmbralTempParada);
            this.gcTerminal.Controls.Add(this.teTerminalMaxDistancia);
            this.gcTerminal.Controls.Add(this.lbTerminalMaxDistancia);
            this.gcTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTerminal.Enabled = false;
            this.gcTerminal.Location = new System.Drawing.Point(0, 0);
            this.gcTerminal.Name = "gcTerminal";
            this.gcTerminal.Size = new System.Drawing.Size(782, 403);
            this.gcTerminal.TabIndex = 109;
            // 
            // lbLasShutDown
            // 
            this.lbLasShutDown.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLasShutDown.Appearance.Options.UseFont = true;
            this.lbLasShutDown.Appearance.Options.UseTextOptions = true;
            this.lbLasShutDown.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbLasShutDown.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbLasShutDown.Location = new System.Drawing.Point(23, 344);
            this.lbLasShutDown.Name = "lbLasShutDown";
            this.lbLasShutDown.Size = new System.Drawing.Size(203, 19);
            this.lbLasShutDown.TabIndex = 146;
            this.lbLasShutDown.Text = "Desde";
            // 
            // teLastShutDown
            // 
            this.teLastShutDown.EditValue = "20";
            this.teLastShutDown.Enabled = false;
            this.teLastShutDown.Location = new System.Drawing.Point(235, 341);
            this.teLastShutDown.Name = "teLastShutDown";
            this.teLastShutDown.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teLastShutDown.Properties.Appearance.Options.UseFont = true;
            this.teLastShutDown.Size = new System.Drawing.Size(128, 22);
            this.teLastShutDown.TabIndex = 145;
            // 
            // lbDaysForShutDown
            // 
            this.lbDaysForShutDown.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDaysForShutDown.Appearance.Options.UseFont = true;
            this.lbDaysForShutDown.Appearance.Options.UseTextOptions = true;
            this.lbDaysForShutDown.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbDaysForShutDown.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbDaysForShutDown.Location = new System.Drawing.Point(20, 302);
            this.lbDaysForShutDown.Name = "lbDaysForShutDown";
            this.lbDaysForShutDown.Size = new System.Drawing.Size(203, 34);
            this.lbDaysForShutDown.TabIndex = 144;
            this.lbDaysForShutDown.Text = "N�mero de dias sin apagar";
            // 
            // teDaysForShutDown
            // 
            this.teDaysForShutDown.EditValue = "20";
            this.teDaysForShutDown.Location = new System.Drawing.Point(235, 313);
            this.teDaysForShutDown.Name = "teDaysForShutDown";
            this.teDaysForShutDown.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teDaysForShutDown.Properties.Appearance.Options.UseFont = true;
            this.teDaysForShutDown.Size = new System.Drawing.Size(128, 22);
            this.teDaysForShutDown.TabIndex = 143;
            // 
            // cbUserMonitorTimeout
            // 
            this.cbUserMonitorTimeout.EditValue = "Tras_3_minutos";
            this.cbUserMonitorTimeout.Location = new System.Drawing.Point(380, 106);
            this.cbUserMonitorTimeout.Name = "cbUserMonitorTimeout";
            this.cbUserMonitorTimeout.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.cbUserMonitorTimeout.Properties.Appearance.Options.UseFont = true;
            this.cbUserMonitorTimeout.Properties.Appearance.Options.UseTextOptions = true;
            this.cbUserMonitorTimeout.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUserMonitorTimeout.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbUserMonitorTimeout.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUserMonitorTimeout.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbUserMonitorTimeout.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbUserMonitorTimeout.Properties.Items.AddRange(new object[] {
            "Indefinido",
            "MDV",
            "MDT"});
            this.cbUserMonitorTimeout.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbUserMonitorTimeout.Size = new System.Drawing.Size(367, 26);
            this.cbUserMonitorTimeout.TabIndex = 142;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(380, 81);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(356, 22);
            this.labelControl2.TabIndex = 141;
            this.labelControl2.Text = "Apagar Pantalla Tras (minutos)";
            // 
            // cbTerminalModoColores
            // 
            this.cbTerminalModoColores.EditValue = "Todos";
            this.cbTerminalModoColores.Location = new System.Drawing.Point(380, 49);
            this.cbTerminalModoColores.Name = "cbTerminalModoColores";
            this.cbTerminalModoColores.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTerminalModoColores.Properties.Appearance.Options.UseFont = true;
            this.cbTerminalModoColores.Properties.Appearance.Options.UseTextOptions = true;
            this.cbTerminalModoColores.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTerminalModoColores.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTerminalModoColores.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTerminalModoColores.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTerminalModoColores.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTerminalModoColores.Properties.Items.AddRange(new object[] {
            "Indefinido",
            "MDV",
            "MDT"});
            this.cbTerminalModoColores.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTerminalModoColores.Size = new System.Drawing.Size(367, 26);
            this.cbTerminalModoColores.TabIndex = 140;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(380, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(203, 16);
            this.labelControl1.TabIndex = 139;
            this.labelControl1.Text = "Vehiculo Estado Colores";
            // 
            // teDaysForReboot
            // 
            this.teDaysForReboot.EditValue = "20";
            this.teDaysForReboot.Location = new System.Drawing.Point(717, 15);
            this.teDaysForReboot.Name = "teDaysForReboot";
            this.teDaysForReboot.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teDaysForReboot.Properties.Appearance.Options.UseFont = true;
            this.teDaysForReboot.Size = new System.Drawing.Size(30, 22);
            this.teDaysForReboot.TabIndex = 136;
            this.teDaysForReboot.Visible = false;
            // 
            // lbDaysForReboot
            // 
            this.lbDaysForReboot.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDaysForReboot.Appearance.Options.UseFont = true;
            this.lbDaysForReboot.Appearance.Options.UseTextOptions = true;
            this.lbDaysForReboot.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbDaysForReboot.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbDaysForReboot.Location = new System.Drawing.Point(612, 7);
            this.lbDaysForReboot.Name = "lbDaysForReboot";
            this.lbDaysForReboot.Size = new System.Drawing.Size(109, 34);
            this.lbDaysForReboot.TabIndex = 135;
            this.lbDaysForReboot.Text = "N�mero de dias para reiniciar terminal";
            this.lbDaysForReboot.Visible = false;
            // 
            // tcbTerminalIntervalSendCola
            // 
            this.tcbTerminalIntervalSendCola.EditValue = 30;
            this.tcbTerminalIntervalSendCola.Location = new System.Drawing.Point(619, 343);
            this.tcbTerminalIntervalSendCola.Name = "tcbTerminalIntervalSendCola";
            this.tcbTerminalIntervalSendCola.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbTerminalIntervalSendCola.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalIntervalSendCola.Properties.Appearance.Options.UseFont = true;
            this.tcbTerminalIntervalSendCola.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalIntervalSendCola.Properties.Maximum = 60;
            this.tcbTerminalIntervalSendCola.Properties.Minimum = 15;
            this.tcbTerminalIntervalSendCola.Properties.SmallChange = 5;
            this.tcbTerminalIntervalSendCola.Properties.TickFrequency = 5;
            this.tcbTerminalIntervalSendCola.Size = new System.Drawing.Size(128, 45);
            this.tcbTerminalIntervalSendCola.TabIndex = 131;
            this.tcbTerminalIntervalSendCola.Value = 30;
            // 
            // tbcTerminalMinWaitRsp
            // 
            this.tbcTerminalMinWaitRsp.EditValue = 1;
            this.tbcTerminalMinWaitRsp.Location = new System.Drawing.Point(619, 289);
            this.tbcTerminalMinWaitRsp.Name = "tbcTerminalMinWaitRsp";
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalMinWaitRsp.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalMinWaitRsp.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalMinWaitRsp.Properties.Maximum = 15;
            this.tbcTerminalMinWaitRsp.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalMinWaitRsp.TabIndex = 130;
            this.tbcTerminalMinWaitRsp.Value = 1;
            // 
            // tbcTerminalMaxWaitRsp
            // 
            this.tbcTerminalMaxWaitRsp.EditValue = 60;
            this.tbcTerminalMaxWaitRsp.Location = new System.Drawing.Point(619, 241);
            this.tbcTerminalMaxWaitRsp.Name = "tbcTerminalMaxWaitRsp";
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalMaxWaitRsp.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalMaxWaitRsp.Properties.Maximum = 90;
            this.tbcTerminalMaxWaitRsp.Properties.Minimum = 15;
            this.tbcTerminalMaxWaitRsp.Properties.TickFrequency = 10;
            this.tbcTerminalMaxWaitRsp.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalMaxWaitRsp.TabIndex = 129;
            this.tbcTerminalMaxWaitRsp.Value = 60;
            // 
            // tcbTerminalTxDelayOff
            // 
            this.tcbTerminalTxDelayOff.EditValue = 15;
            this.tcbTerminalTxDelayOff.Location = new System.Drawing.Point(619, 146);
            this.tcbTerminalTxDelayOff.Name = "tcbTerminalTxDelayOff";
            this.tcbTerminalTxDelayOff.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbTerminalTxDelayOff.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tcbTerminalTxDelayOff.Properties.Appearance.Options.UseFont = true;
            this.tcbTerminalTxDelayOff.Properties.Appearance.Options.UseForeColor = true;
            this.tcbTerminalTxDelayOff.Properties.Maximum = 30;
            this.tcbTerminalTxDelayOff.Properties.Minimum = 5;
            this.tcbTerminalTxDelayOff.Properties.TickFrequency = 3;
            this.tcbTerminalTxDelayOff.Size = new System.Drawing.Size(128, 45);
            this.tcbTerminalTxDelayOff.TabIndex = 128;
            this.tcbTerminalTxDelayOff.Value = 15;
            // 
            // tbcTerminalTxDelayOn
            // 
            this.tbcTerminalTxDelayOn.EditValue = 1;
            this.tbcTerminalTxDelayOn.Location = new System.Drawing.Point(619, 190);
            this.tbcTerminalTxDelayOn.Name = "tbcTerminalTxDelayOn";
            this.tbcTerminalTxDelayOn.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalTxDelayOn.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalTxDelayOn.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalTxDelayOn.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalTxDelayOn.Properties.Maximum = 15;
            this.tbcTerminalTxDelayOn.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalTxDelayOn.TabIndex = 127;
            this.tbcTerminalTxDelayOn.Value = 1;
            // 
            // tbcTerminalSleepLogout
            // 
            this.tbcTerminalSleepLogout.EditValue = 10;
            this.tbcTerminalSleepLogout.Location = new System.Drawing.Point(232, 249);
            this.tbcTerminalSleepLogout.Name = "tbcTerminalSleepLogout";
            this.tbcTerminalSleepLogout.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalSleepLogout.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalSleepLogout.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalSleepLogout.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalSleepLogout.Properties.Maximum = 30;
            this.tbcTerminalSleepLogout.Properties.Minimum = 2;
            this.tbcTerminalSleepLogout.Properties.TickFrequency = 3;
            this.tbcTerminalSleepLogout.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalSleepLogout.TabIndex = 126;
            this.tbcTerminalSleepLogout.Value = 10;
            // 
            // tbcTerminalUmbralVelocidadParada
            // 
            this.tbcTerminalUmbralVelocidadParada.EditValue = 10;
            this.tbcTerminalUmbralVelocidadParada.Location = new System.Drawing.Point(232, 204);
            this.tbcTerminalUmbralVelocidadParada.Name = "tbcTerminalUmbralVelocidadParada";
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalUmbralVelocidadParada.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalUmbralVelocidadParada.Properties.Maximum = 20;
            this.tbcTerminalUmbralVelocidadParada.Properties.Minimum = 5;
            this.tbcTerminalUmbralVelocidadParada.Properties.TickFrequency = 2;
            this.tbcTerminalUmbralVelocidadParada.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalUmbralVelocidadParada.TabIndex = 125;
            this.tbcTerminalUmbralVelocidadParada.Value = 10;
            // 
            // tbcTerminalUmbralTempParada
            // 
            this.tbcTerminalUmbralTempParada.EditValue = 7;
            this.tbcTerminalUmbralTempParada.Location = new System.Drawing.Point(232, 156);
            this.tbcTerminalUmbralTempParada.Name = "tbcTerminalUmbralTempParada";
            this.tbcTerminalUmbralTempParada.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcTerminalUmbralTempParada.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.tbcTerminalUmbralTempParada.Properties.Appearance.Options.UseFont = true;
            this.tbcTerminalUmbralTempParada.Properties.Appearance.Options.UseForeColor = true;
            this.tbcTerminalUmbralTempParada.Properties.Minimum = 3;
            this.tbcTerminalUmbralTempParada.Size = new System.Drawing.Size(128, 45);
            this.tbcTerminalUmbralTempParada.TabIndex = 124;
            this.tbcTerminalUmbralTempParada.Value = 7;
            // 
            // lbTerminalIntervalSendCola
            // 
            this.lbTerminalIntervalSendCola.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalIntervalSendCola.Appearance.Options.UseFont = true;
            this.lbTerminalIntervalSendCola.Appearance.Options.UseTextOptions = true;
            this.lbTerminalIntervalSendCola.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalIntervalSendCola.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalIntervalSendCola.Location = new System.Drawing.Point(380, 342);
            this.lbTerminalIntervalSendCola.Name = "lbTerminalIntervalSendCola";
            this.lbTerminalIntervalSendCola.Size = new System.Drawing.Size(200, 43);
            this.lbTerminalIntervalSendCola.TabIndex = 122;
            this.lbTerminalIntervalSendCola.Text = "Frecuencia Envio de Cola (seg)";
            // 
            // lbTerminalMinWaitRsp
            // 
            this.lbTerminalMinWaitRsp.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMinWaitRsp.Appearance.Options.UseFont = true;
            this.lbTerminalMinWaitRsp.Appearance.Options.UseTextOptions = true;
            this.lbTerminalMinWaitRsp.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMinWaitRsp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMinWaitRsp.Location = new System.Drawing.Point(380, 293);
            this.lbTerminalMinWaitRsp.Name = "lbTerminalMinWaitRsp";
            this.lbTerminalMinWaitRsp.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalMinWaitRsp.TabIndex = 121;
            this.lbTerminalMinWaitRsp.Text = "M�nimo Tiempo espera respuesta (seg)";
            // 
            // lbTerminalMaxWaitRsp
            // 
            this.lbTerminalMaxWaitRsp.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMaxWaitRsp.Appearance.Options.UseFont = true;
            this.lbTerminalMaxWaitRsp.Appearance.Options.UseTextOptions = true;
            this.lbTerminalMaxWaitRsp.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMaxWaitRsp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMaxWaitRsp.Location = new System.Drawing.Point(380, 245);
            this.lbTerminalMaxWaitRsp.Name = "lbTerminalMaxWaitRsp";
            this.lbTerminalMaxWaitRsp.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalMaxWaitRsp.TabIndex = 120;
            this.lbTerminalMaxWaitRsp.Text = "M�ximo Tiempo espera respuesta (seg)";
            // 
            // lbTerminalTxDelayOn
            // 
            this.lbTerminalTxDelayOn.Appearance.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalTxDelayOn.Appearance.Options.UseFont = true;
            this.lbTerminalTxDelayOn.Appearance.Options.UseTextOptions = true;
            this.lbTerminalTxDelayOn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalTxDelayOn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalTxDelayOn.Location = new System.Drawing.Point(380, 195);
            this.lbTerminalTxDelayOn.Name = "lbTerminalTxDelayOn";
            this.lbTerminalTxDelayOn.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalTxDelayOn.TabIndex = 119;
            this.lbTerminalTxDelayOn.Text = "Frecuencia Envio Tramas en Movimiento (min.)";
            // 
            // lbTerminalTxDelayOff
            // 
            this.lbTerminalTxDelayOff.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalTxDelayOff.Appearance.Options.UseFont = true;
            this.lbTerminalTxDelayOff.Appearance.Options.UseTextOptions = true;
            this.lbTerminalTxDelayOff.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalTxDelayOff.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalTxDelayOff.Location = new System.Drawing.Point(380, 146);
            this.lbTerminalTxDelayOff.Name = "lbTerminalTxDelayOff";
            this.lbTerminalTxDelayOff.Size = new System.Drawing.Size(203, 43);
            this.lbTerminalTxDelayOff.TabIndex = 118;
            this.lbTerminalTxDelayOff.Text = "Frecuencia Envio Tramas en Parado (min)";
            // 
            // lbTerminalSleepLogout
            // 
            this.lbTerminalSleepLogout.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalSleepLogout.Appearance.Options.UseFont = true;
            this.lbTerminalSleepLogout.Appearance.Options.UseTextOptions = true;
            this.lbTerminalSleepLogout.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalSleepLogout.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalSleepLogout.Location = new System.Drawing.Point(23, 254);
            this.lbTerminalSleepLogout.Name = "lbTerminalSleepLogout";
            this.lbTerminalSleepLogout.Size = new System.Drawing.Size(203, 34);
            this.lbTerminalSleepLogout.TabIndex = 117;
            this.lbTerminalSleepLogout.Text = "Tiempo desde el logout hasta suspensi�n";
            // 
            // lbTerminal
            // 
            this.lbTerminal.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminal.Appearance.Options.UseFont = true;
            this.lbTerminal.Appearance.Options.UseTextOptions = true;
            this.lbTerminal.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminal.Location = new System.Drawing.Point(20, 209);
            this.lbTerminal.Name = "lbTerminal";
            this.lbTerminal.Size = new System.Drawing.Size(203, 34);
            this.lbTerminal.TabIndex = 112;
            this.lbTerminal.Text = "Umbral de velocidad parada Km/h";
            // 
            // lbTerminalUmbralTempParada
            // 
            this.lbTerminalUmbralTempParada.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalUmbralTempParada.Appearance.Options.UseFont = true;
            this.lbTerminalUmbralTempParada.Appearance.Options.UseTextOptions = true;
            this.lbTerminalUmbralTempParada.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalUmbralTempParada.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalUmbralTempParada.Location = new System.Drawing.Point(23, 158);
            this.lbTerminalUmbralTempParada.Name = "lbTerminalUmbralTempParada";
            this.lbTerminalUmbralTempParada.Size = new System.Drawing.Size(203, 40);
            this.lbTerminalUmbralTempParada.TabIndex = 111;
            this.lbTerminalUmbralTempParada.Text = "Umbral de tiempo parada en min";
            // 
            // teTerminalMaxDistancia
            // 
            this.teTerminalMaxDistancia.EditValue = "20";
            this.teTerminalMaxDistancia.Location = new System.Drawing.Point(232, 30);
            this.teTerminalMaxDistancia.Name = "teTerminalMaxDistancia";
            this.teTerminalMaxDistancia.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTerminalMaxDistancia.Properties.Appearance.Options.UseFont = true;
            this.teTerminalMaxDistancia.Size = new System.Drawing.Size(128, 22);
            this.teTerminalMaxDistancia.TabIndex = 110;
            // 
            // lbTerminalMaxDistancia
            // 
            this.lbTerminalMaxDistancia.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerminalMaxDistancia.Appearance.Options.UseFont = true;
            this.lbTerminalMaxDistancia.Appearance.Options.UseTextOptions = true;
            this.lbTerminalMaxDistancia.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTerminalMaxDistancia.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTerminalMaxDistancia.Location = new System.Drawing.Point(23, 18);
            this.lbTerminalMaxDistancia.Name = "lbTerminalMaxDistancia";
            this.lbTerminalMaxDistancia.Size = new System.Drawing.Size(203, 48);
            this.lbTerminalMaxDistancia.TabIndex = 109;
            this.lbTerminalMaxDistancia.Text = "Maxima distancia entre dos puntos en km";
            // 
            // tbpAdminCnx
            // 
            this.tbpAdminCnx.Controls.Add(this.gcConexion);
            this.tbpAdminCnx.Name = "tbpAdminCnx";
            this.tbpAdminCnx.Size = new System.Drawing.Size(783, 403);
            this.tbpAdminCnx.Text = "Conexi�n";
            // 
            // gcConexion
            // 
            this.gcConexion.Controls.Add(this.teConexionServerPort3);
            this.gcConexion.Controls.Add(this.lbConexionServerPort3);
            this.gcConexion.Controls.Add(this.teConexionServerIP3);
            this.gcConexion.Controls.Add(this.lbConexionServerIP3);
            this.gcConexion.Controls.Add(this.teConexionServerName3);
            this.gcConexion.Controls.Add(this.lbConexionServerName3);
            this.gcConexion.Controls.Add(this.teConexionServerPort2);
            this.gcConexion.Controls.Add(this.lbConexionServerPort2);
            this.gcConexion.Controls.Add(this.teConexionServerIP2);
            this.gcConexion.Controls.Add(this.lbConexionServerIP2);
            this.gcConexion.Controls.Add(this.teConexionServerName2);
            this.gcConexion.Controls.Add(this.lbConexionServerName2);
            this.gcConexion.Controls.Add(this.teConexionServerPort1);
            this.gcConexion.Controls.Add(this.lbConexionServerPort1);
            this.gcConexion.Controls.Add(this.teConexionServerIP1);
            this.gcConexion.Controls.Add(this.lbConexionServerIP1);
            this.gcConexion.Controls.Add(this.teConexionServerName1);
            this.gcConexion.Controls.Add(this.lbConexionServerName1);
            this.gcConexion.Controls.Add(this.teConexionIntentos);
            this.gcConexion.Controls.Add(this.lbConexionIntentos);
            this.gcConexion.Controls.Add(this.cbConexionDefaultIP);
            this.gcConexion.Controls.Add(this.lbConexionDefaultPositioIP);
            this.gcConexion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcConexion.Enabled = false;
            this.gcConexion.Location = new System.Drawing.Point(0, 0);
            this.gcConexion.Name = "gcConexion";
            this.gcConexion.Size = new System.Drawing.Size(783, 403);
            this.gcConexion.TabIndex = 83;
            // 
            // teConexionServerPort3
            // 
            this.teConexionServerPort3.EditValue = "1980";
            this.teConexionServerPort3.Location = new System.Drawing.Point(573, 227);
            this.teConexionServerPort3.Name = "teConexionServerPort3";
            this.teConexionServerPort3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort3.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerPort3.TabIndex = 108;
            // 
            // lbConexionServerPort3
            // 
            this.lbConexionServerPort3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort3.Appearance.Options.UseFont = true;
            this.lbConexionServerPort3.Location = new System.Drawing.Point(400, 230);
            this.lbConexionServerPort3.Name = "lbConexionServerPort3";
            this.lbConexionServerPort3.Size = new System.Drawing.Size(149, 18);
            this.lbConexionServerPort3.TabIndex = 107;
            this.lbConexionServerPort3.Text = "Puerto servidor 3�";
            // 
            // teConexionServerIP3
            // 
            this.teConexionServerIP3.EditValue = "192.168.50.110";
            this.teConexionServerIP3.Location = new System.Drawing.Point(574, 195);
            this.teConexionServerIP3.Name = "teConexionServerIP3";
            this.teConexionServerIP3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP3.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerIP3.TabIndex = 106;
            // 
            // lbConexionServerIP3
            // 
            this.lbConexionServerIP3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP3.Appearance.Options.UseFont = true;
            this.lbConexionServerIP3.Location = new System.Drawing.Point(399, 198);
            this.lbConexionServerIP3.Name = "lbConexionServerIP3";
            this.lbConexionServerIP3.Size = new System.Drawing.Size(122, 18);
            this.lbConexionServerIP3.TabIndex = 105;
            this.lbConexionServerIP3.Text = "Direcci�n IP 3�";
            // 
            // teConexionServerName3
            // 
            this.teConexionServerName3.EditValue = "";
            this.teConexionServerName3.Location = new System.Drawing.Point(574, 163);
            this.teConexionServerName3.Name = "teConexionServerName3";
            this.teConexionServerName3.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName3.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName3.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerName3.TabIndex = 104;
            // 
            // lbConexionServerName3
            // 
            this.lbConexionServerName3.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName3.Appearance.Options.UseFont = true;
            this.lbConexionServerName3.Location = new System.Drawing.Point(399, 166);
            this.lbConexionServerName3.Name = "lbConexionServerName3";
            this.lbConexionServerName3.Size = new System.Drawing.Size(160, 18);
            this.lbConexionServerName3.TabIndex = 103;
            this.lbConexionServerName3.Text = "Nombre Servidor 3�";
            // 
            // teConexionServerPort2
            // 
            this.teConexionServerPort2.EditValue = "1980";
            this.teConexionServerPort2.Location = new System.Drawing.Point(574, 110);
            this.teConexionServerPort2.Name = "teConexionServerPort2";
            this.teConexionServerPort2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort2.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerPort2.TabIndex = 102;
            // 
            // lbConexionServerPort2
            // 
            this.lbConexionServerPort2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort2.Appearance.Options.UseFont = true;
            this.lbConexionServerPort2.Location = new System.Drawing.Point(399, 113);
            this.lbConexionServerPort2.Name = "lbConexionServerPort2";
            this.lbConexionServerPort2.Size = new System.Drawing.Size(149, 18);
            this.lbConexionServerPort2.TabIndex = 101;
            this.lbConexionServerPort2.Text = "Puerto servidor 2�";
            // 
            // teConexionServerIP2
            // 
            this.teConexionServerIP2.EditValue = "192.168.50.110";
            this.teConexionServerIP2.Location = new System.Drawing.Point(574, 78);
            this.teConexionServerIP2.Name = "teConexionServerIP2";
            this.teConexionServerIP2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP2.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerIP2.TabIndex = 100;
            // 
            // lbConexionServerIP2
            // 
            this.lbConexionServerIP2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP2.Appearance.Options.UseFont = true;
            this.lbConexionServerIP2.Location = new System.Drawing.Point(399, 81);
            this.lbConexionServerIP2.Name = "lbConexionServerIP2";
            this.lbConexionServerIP2.Size = new System.Drawing.Size(122, 18);
            this.lbConexionServerIP2.TabIndex = 99;
            this.lbConexionServerIP2.Text = "Direcci�n IP 2�";
            // 
            // teConexionServerName2
            // 
            this.teConexionServerName2.EditValue = "";
            this.teConexionServerName2.Location = new System.Drawing.Point(574, 46);
            this.teConexionServerName2.Name = "teConexionServerName2";
            this.teConexionServerName2.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName2.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName2.Size = new System.Drawing.Size(192, 26);
            this.teConexionServerName2.TabIndex = 98;
            // 
            // lbConexionServerName2
            // 
            this.lbConexionServerName2.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName2.Appearance.Options.UseFont = true;
            this.lbConexionServerName2.Location = new System.Drawing.Point(399, 49);
            this.lbConexionServerName2.Name = "lbConexionServerName2";
            this.lbConexionServerName2.Size = new System.Drawing.Size(160, 18);
            this.lbConexionServerName2.TabIndex = 97;
            this.lbConexionServerName2.Text = "Nombre Servidor 2�";
            // 
            // teConexionServerPort1
            // 
            this.teConexionServerPort1.EditValue = "1980";
            this.teConexionServerPort1.Location = new System.Drawing.Point(194, 227);
            this.teConexionServerPort1.Name = "teConexionServerPort1";
            this.teConexionServerPort1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerPort1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerPort1.Size = new System.Drawing.Size(180, 26);
            this.teConexionServerPort1.TabIndex = 96;
            // 
            // lbConexionServerPort1
            // 
            this.lbConexionServerPort1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerPort1.Appearance.Options.UseFont = true;
            this.lbConexionServerPort1.Location = new System.Drawing.Point(24, 230);
            this.lbConexionServerPort1.Name = "lbConexionServerPort1";
            this.lbConexionServerPort1.Size = new System.Drawing.Size(149, 18);
            this.lbConexionServerPort1.TabIndex = 95;
            this.lbConexionServerPort1.Text = "Puerto servidor 1�";
            // 
            // teConexionServerIP1
            // 
            this.teConexionServerIP1.EditValue = "192.168.50.110";
            this.teConexionServerIP1.Location = new System.Drawing.Point(193, 195);
            this.teConexionServerIP1.Name = "teConexionServerIP1";
            this.teConexionServerIP1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerIP1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerIP1.Size = new System.Drawing.Size(180, 26);
            this.teConexionServerIP1.TabIndex = 94;
            // 
            // lbConexionServerIP1
            // 
            this.lbConexionServerIP1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerIP1.Appearance.Options.UseFont = true;
            this.lbConexionServerIP1.Location = new System.Drawing.Point(23, 198);
            this.lbConexionServerIP1.Name = "lbConexionServerIP1";
            this.lbConexionServerIP1.Size = new System.Drawing.Size(122, 18);
            this.lbConexionServerIP1.TabIndex = 93;
            this.lbConexionServerIP1.Text = "Direcci�n IP 1�";
            // 
            // teConexionServerName1
            // 
            this.teConexionServerName1.EditValue = "";
            this.teConexionServerName1.Location = new System.Drawing.Point(193, 163);
            this.teConexionServerName1.Name = "teConexionServerName1";
            this.teConexionServerName1.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionServerName1.Properties.Appearance.Options.UseFont = true;
            this.teConexionServerName1.Size = new System.Drawing.Size(180, 26);
            this.teConexionServerName1.TabIndex = 92;
            // 
            // lbConexionServerName1
            // 
            this.lbConexionServerName1.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionServerName1.Appearance.Options.UseFont = true;
            this.lbConexionServerName1.Location = new System.Drawing.Point(22, 166);
            this.lbConexionServerName1.Name = "lbConexionServerName1";
            this.lbConexionServerName1.Size = new System.Drawing.Size(160, 18);
            this.lbConexionServerName1.TabIndex = 91;
            this.lbConexionServerName1.Text = "Nombre Servidor 1�";
            // 
            // teConexionIntentos
            // 
            this.teConexionIntentos.EditValue = "3";
            this.teConexionIntentos.Location = new System.Drawing.Point(194, 78);
            this.teConexionIntentos.Name = "teConexionIntentos";
            this.teConexionIntentos.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teConexionIntentos.Properties.Appearance.Options.UseFont = true;
            this.teConexionIntentos.Size = new System.Drawing.Size(180, 26);
            this.teConexionIntentos.TabIndex = 88;
            // 
            // lbConexionIntentos
            // 
            this.lbConexionIntentos.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionIntentos.Appearance.Options.UseFont = true;
            this.lbConexionIntentos.Location = new System.Drawing.Point(24, 81);
            this.lbConexionIntentos.Name = "lbConexionIntentos";
            this.lbConexionIntentos.Size = new System.Drawing.Size(138, 18);
            this.lbConexionIntentos.TabIndex = 87;
            this.lbConexionIntentos.Text = "Intetos conexion";
            // 
            // cbConexionDefaultIP
            // 
            this.cbConexionDefaultIP.EditValue = "1";
            this.cbConexionDefaultIP.Location = new System.Drawing.Point(193, 46);
            this.cbConexionDefaultIP.Name = "cbConexionDefaultIP";
            this.cbConexionDefaultIP.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConexionDefaultIP.Properties.Appearance.Options.UseFont = true;
            this.cbConexionDefaultIP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConexionDefaultIP.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbConexionDefaultIP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbConexionDefaultIP.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbConexionDefaultIP.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbConexionDefaultIP.Size = new System.Drawing.Size(180, 26);
            this.cbConexionDefaultIP.TabIndex = 86;
            // 
            // lbConexionDefaultPositioIP
            // 
            this.lbConexionDefaultPositioIP.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConexionDefaultPositioIP.Appearance.Options.UseFont = true;
            this.lbConexionDefaultPositioIP.Location = new System.Drawing.Point(23, 49);
            this.lbConexionDefaultPositioIP.Name = "lbConexionDefaultPositioIP";
            this.lbConexionDefaultPositioIP.Size = new System.Drawing.Size(143, 18);
            this.lbConexionDefaultPositioIP.TabIndex = 85;
            this.lbConexionDefaultPositioIP.Text = "N� IP por defecto";
            // 
            // tbpAdminFtpTrace
            // 
            this.tbpAdminFtpTrace.Controls.Add(this.gcTrace);
            this.tbpAdminFtpTrace.Controls.Add(this.gcFtp);
            this.tbpAdminFtpTrace.Name = "tbpAdminFtpTrace";
            this.tbpAdminFtpTrace.Size = new System.Drawing.Size(783, 403);
            this.tbpAdminFtpTrace.Text = "Traza/Log";
            // 
            // gcTrace
            // 
            this.gcTrace.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcTrace.AppearanceCaption.Options.UseFont = true;
            this.gcTrace.Controls.Add(this.cbTraceProtocol);
            this.gcTrace.Controls.Add(this.lbTraceProtocol);
            this.gcTrace.Controls.Add(this.cbTracePFileRotate);
            this.gcTrace.Controls.Add(this.lbTracePFileRotate);
            this.gcTrace.Controls.Add(this.teTracePFileName);
            this.gcTrace.Controls.Add(this.lbTracePFileName);
            this.gcTrace.Controls.Add(this.teTracePFileMaxSize);
            this.gcTrace.Controls.Add(this.lbTracePFileMaxSize);
            this.gcTrace.Controls.Add(this.cbTraceLevel);
            this.gcTrace.Controls.Add(this.lbTraceLevel);
            this.gcTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTrace.Enabled = false;
            this.gcTrace.Location = new System.Drawing.Point(0, 176);
            this.gcTrace.Name = "gcTrace";
            this.gcTrace.Size = new System.Drawing.Size(783, 227);
            this.gcTrace.TabIndex = 13;
            this.gcTrace.Text = "Trace";
            // 
            // cbTraceProtocol
            // 
            this.cbTraceProtocol.EditValue = "File";
            this.cbTraceProtocol.Enabled = false;
            this.cbTraceProtocol.Location = new System.Drawing.Point(571, 103);
            this.cbTraceProtocol.Name = "cbTraceProtocol";
            this.cbTraceProtocol.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.Appearance.Options.UseFont = true;
            this.cbTraceProtocol.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTraceProtocol.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceProtocol.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTraceProtocol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTraceProtocol.Properties.Items.AddRange(new object[] {
            "Unknown",
            "Memory",
            "File",
            "TcpIp"});
            this.cbTraceProtocol.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTraceProtocol.Size = new System.Drawing.Size(202, 26);
            this.cbTraceProtocol.TabIndex = 52;
            // 
            // lbTraceProtocol
            // 
            this.lbTraceProtocol.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTraceProtocol.Appearance.Options.UseFont = true;
            this.lbTraceProtocol.Location = new System.Drawing.Point(432, 108);
            this.lbTraceProtocol.Name = "lbTraceProtocol";
            this.lbTraceProtocol.Size = new System.Drawing.Size(76, 18);
            this.lbTraceProtocol.TabIndex = 51;
            this.lbTraceProtocol.Text = "Protocolo";
            // 
            // cbTracePFileRotate
            // 
            this.cbTracePFileRotate.EditValue = "Ninguno";
            this.cbTracePFileRotate.Location = new System.Drawing.Point(571, 51);
            this.cbTracePFileRotate.Name = "cbTracePFileRotate";
            this.cbTracePFileRotate.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.Appearance.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTracePFileRotate.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTracePFileRotate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTracePFileRotate.Properties.Items.AddRange(new object[] {
            "None",
            "Hourly",
            "Daily"});
            this.cbTracePFileRotate.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTracePFileRotate.Size = new System.Drawing.Size(202, 26);
            this.cbTracePFileRotate.TabIndex = 50;
            // 
            // lbTracePFileRotate
            // 
            this.lbTracePFileRotate.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileRotate.Appearance.Options.UseFont = true;
            this.lbTracePFileRotate.Appearance.Options.UseTextOptions = true;
            this.lbTracePFileRotate.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTracePFileRotate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTracePFileRotate.Location = new System.Drawing.Point(432, 47);
            this.lbTracePFileRotate.Name = "lbTracePFileRotate";
            this.lbTracePFileRotate.Size = new System.Drawing.Size(110, 50);
            this.lbTracePFileRotate.TabIndex = 49;
            this.lbTracePFileRotate.Text = "Perioricidad creaci�n";
            // 
            // teTracePFileName
            // 
            this.teTracePFileName.EditValue = "gitnavega.sil";
            this.teTracePFileName.Location = new System.Drawing.Point(211, 108);
            this.teTracePFileName.Name = "teTracePFileName";
            this.teTracePFileName.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTracePFileName.Properties.Appearance.Options.UseFont = true;
            this.teTracePFileName.Size = new System.Drawing.Size(200, 26);
            this.teTracePFileName.TabIndex = 48;
            // 
            // lbTracePFileName
            // 
            this.lbTracePFileName.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileName.Appearance.Options.UseFont = true;
            this.lbTracePFileName.Location = new System.Drawing.Point(13, 113);
            this.lbTracePFileName.Name = "lbTracePFileName";
            this.lbTracePFileName.Size = new System.Drawing.Size(127, 18);
            this.lbTracePFileName.TabIndex = 47;
            this.lbTracePFileName.Text = "Nombre archivo";
            // 
            // teTracePFileMaxSize
            // 
            this.teTracePFileMaxSize.EditValue = "";
            this.teTracePFileMaxSize.Location = new System.Drawing.Point(211, 76);
            this.teTracePFileMaxSize.Name = "teTracePFileMaxSize";
            this.teTracePFileMaxSize.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTracePFileMaxSize.Properties.Appearance.Options.UseFont = true;
            this.teTracePFileMaxSize.Size = new System.Drawing.Size(200, 26);
            this.teTracePFileMaxSize.TabIndex = 46;
            // 
            // lbTracePFileMaxSize
            // 
            this.lbTracePFileMaxSize.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTracePFileMaxSize.Appearance.Options.UseFont = true;
            this.lbTracePFileMaxSize.Location = new System.Drawing.Point(13, 81);
            this.lbTracePFileMaxSize.Name = "lbTracePFileMaxSize";
            this.lbTracePFileMaxSize.Size = new System.Drawing.Size(164, 18);
            this.lbTracePFileMaxSize.TabIndex = 45;
            this.lbTracePFileMaxSize.Text = "Max tama�o archivo";
            // 
            // cbTraceLevel
            // 
            this.cbTraceLevel.EditValue = "Debug";
            this.cbTraceLevel.Location = new System.Drawing.Point(211, 44);
            this.cbTraceLevel.Name = "cbTraceLevel";
            this.cbTraceLevel.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.Appearance.Options.UseFont = true;
            this.cbTraceLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbTraceLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraceLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbTraceLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTraceLevel.Properties.Items.AddRange(new object[] {
            "Debug",
            "Verbose",
            "Message",
            "Warning",
            "Error",
            "Fatal"});
            this.cbTraceLevel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTraceLevel.Size = new System.Drawing.Size(200, 26);
            this.cbTraceLevel.TabIndex = 38;
            // 
            // lbTraceLevel
            // 
            this.lbTraceLevel.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTraceLevel.Appearance.Options.UseFont = true;
            this.lbTraceLevel.Location = new System.Drawing.Point(13, 49);
            this.lbTraceLevel.Name = "lbTraceLevel";
            this.lbTraceLevel.Size = new System.Drawing.Size(72, 18);
            this.lbTraceLevel.TabIndex = 37;
            this.lbTraceLevel.Text = "Nivel log";
            // 
            // gcFtp
            // 
            this.gcFtp.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcFtp.AppearanceCaption.Options.UseFont = true;
            this.gcFtp.Controls.Add(this.cbFtpModoEnvioLog);
            this.gcFtp.Controls.Add(this.lbFtpModoEnvio);
            this.gcFtp.Controls.Add(this.lbFtpLastDate);
            this.gcFtp.Controls.Add(this.deFtpLastDateLog);
            this.gcFtp.Controls.Add(this.teFtpRemoteHost);
            this.gcFtp.Controls.Add(this.lbFtpRemoteHost);
            this.gcFtp.Controls.Add(this.teFtpPort);
            this.gcFtp.Controls.Add(this.lbFtpPort);
            this.gcFtp.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFtp.Enabled = false;
            this.gcFtp.Location = new System.Drawing.Point(0, 0);
            this.gcFtp.Name = "gcFtp";
            this.gcFtp.Size = new System.Drawing.Size(783, 176);
            this.gcFtp.TabIndex = 12;
            this.gcFtp.Text = "Configuraci�n Ftp. Envio de Logs";
            // 
            // cbFtpModoEnvioLog
            // 
            this.cbFtpModoEnvioLog.EditValue = "Ninguno";
            this.cbFtpModoEnvioLog.Location = new System.Drawing.Point(211, 128);
            this.cbFtpModoEnvioLog.Name = "cbFtpModoEnvioLog";
            this.cbFtpModoEnvioLog.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.Appearance.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFtpModoEnvioLog.Properties.AppearanceFocused.Options.UseFont = true;
            this.cbFtpModoEnvioLog.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbFtpModoEnvioLog.Properties.Items.AddRange(new object[] {
            "Nada",
            "Diaria",
            "Horaria"});
            this.cbFtpModoEnvioLog.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbFtpModoEnvioLog.Size = new System.Drawing.Size(183, 26);
            this.cbFtpModoEnvioLog.TabIndex = 36;
            // 
            // lbFtpModoEnvio
            // 
            this.lbFtpModoEnvio.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpModoEnvio.Appearance.Options.UseFont = true;
            this.lbFtpModoEnvio.Location = new System.Drawing.Point(16, 133);
            this.lbFtpModoEnvio.Name = "lbFtpModoEnvio";
            this.lbFtpModoEnvio.Size = new System.Drawing.Size(145, 18);
            this.lbFtpModoEnvio.TabIndex = 35;
            this.lbFtpModoEnvio.Text = "Perioricidad envio";
            // 
            // lbFtpLastDate
            // 
            this.lbFtpLastDate.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpLastDate.Appearance.Options.UseFont = true;
            this.lbFtpLastDate.Location = new System.Drawing.Point(16, 101);
            this.lbFtpLastDate.Name = "lbFtpLastDate";
            this.lbFtpLastDate.Size = new System.Drawing.Size(154, 18);
            this.lbFtpLastDate.TabIndex = 34;
            this.lbFtpLastDate.Text = "Ultima fecha envio";
            // 
            // deFtpLastDateLog
            // 
            this.deFtpLastDateLog.EditValue = new System.DateTime(2010, 9, 2, 0, 0, 0, 0);
            this.deFtpLastDateLog.Location = new System.Drawing.Point(211, 96);
            this.deFtpLastDateLog.Name = "deFtpLastDateLog";
            this.deFtpLastDateLog.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deFtpLastDateLog.Properties.Appearance.Options.UseFont = true;
            this.deFtpLastDateLog.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFtpLastDateLog.Properties.DisplayFormat.FormatString = "g";
            this.deFtpLastDateLog.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFtpLastDateLog.Properties.Mask.EditMask = "g";
            this.deFtpLastDateLog.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFtpLastDateLog.Size = new System.Drawing.Size(183, 26);
            this.deFtpLastDateLog.TabIndex = 33;
            // 
            // teFtpRemoteHost
            // 
            this.teFtpRemoteHost.EditValue = "ftp.amcoex.es";
            this.teFtpRemoteHost.Location = new System.Drawing.Point(211, 62);
            this.teFtpRemoteHost.Name = "teFtpRemoteHost";
            this.teFtpRemoteHost.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpRemoteHost.Properties.Appearance.Options.UseFont = true;
            this.teFtpRemoteHost.Size = new System.Drawing.Size(183, 26);
            this.teFtpRemoteHost.TabIndex = 32;
            // 
            // lbFtpRemoteHost
            // 
            this.lbFtpRemoteHost.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpRemoteHost.Appearance.Options.UseFont = true;
            this.lbFtpRemoteHost.Location = new System.Drawing.Point(16, 67);
            this.lbFtpRemoteHost.Name = "lbFtpRemoteHost";
            this.lbFtpRemoteHost.Size = new System.Drawing.Size(103, 18);
            this.lbFtpRemoteHost.TabIndex = 31;
            this.lbFtpRemoteHost.Text = "Direcci�n ftp";
            // 
            // teFtpPort
            // 
            this.teFtpPort.EditValue = "21";
            this.teFtpPort.Location = new System.Drawing.Point(211, 30);
            this.teFtpPort.Name = "teFtpPort";
            this.teFtpPort.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teFtpPort.Properties.Appearance.Options.UseFont = true;
            this.teFtpPort.Size = new System.Drawing.Size(183, 26);
            this.teFtpPort.TabIndex = 30;
            // 
            // lbFtpPort
            // 
            this.lbFtpPort.Appearance.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFtpPort.Appearance.Options.UseFont = true;
            this.lbFtpPort.Location = new System.Drawing.Point(16, 35);
            this.lbFtpPort.Name = "lbFtpPort";
            this.lbFtpPort.Size = new System.Drawing.Size(53, 18);
            this.lbFtpPort.TabIndex = 29;
            this.lbFtpPort.Text = "Puerto";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCancel.Appearance.Options.UseFont = true;
            this.cmdCancel.Image = global::gcperu.Server.UI.Properties.Resources.Return1;
            this.cmdCancel.Location = new System.Drawing.Point(8, 502);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(126, 37);
            this.cmdCancel.TabIndex = 1;
            this.cmdCancel.Text = "Cancelar";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOK.Appearance.Options.UseFont = true;
            this.cmdOK.Image = global::gcperu.Server.UI.Properties.Resources.MailSend;
            this.cmdOK.Location = new System.Drawing.Point(525, 502);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(271, 37);
            this.cmdOK.TabIndex = 2;
            this.cmdOK.Text = "GUARDAR Y ENVIAR";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // ckConfigFull
            // 
            this.ckConfigFull.Location = new System.Drawing.Point(279, 500);
            this.ckConfigFull.Name = "ckConfigFull";
            this.ckConfigFull.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckConfigFull.Properties.Appearance.Options.UseFont = true;
            this.ckConfigFull.Properties.Caption = "Enviar configuraci�n completa";
            this.ckConfigFull.Size = new System.Drawing.Size(221, 21);
            this.ckConfigFull.TabIndex = 3;
            // 
            // ChangeGlobalOptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 546);
            this.Controls.Add(this.ckConfigFull);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.tbControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ChangeGlobalOptionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Opciones a Aplicar a Terminales Seleccionados";
            this.Load += new System.EventHandler(this.TerminalOptionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbControl)).EndInit();
            this.tbControl.ResumeLayout(false);
            this.tpControlAdmin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbAdmin)).EndInit();
            this.tbAdmin.ResumeLayout(false);
            this.tbpAdminTerm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTerminal)).EndInit();
            this.gcTerminal.ResumeLayout(false);
            this.gcTerminal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teLastShutDown.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDaysForShutDown.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbUserMonitorTimeout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTerminalModoColores.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDaysForReboot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalIntervalSendCola)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMinWaitRsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalMaxWaitRsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcbTerminalTxDelayOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalTxDelayOn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalTxDelayOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalSleepLogout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralVelocidadParada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTerminalUmbralTempParada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTerminalMaxDistancia.Properties)).EndInit();
            this.tbpAdminCnx.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcConexion)).EndInit();
            this.gcConexion.ResumeLayout(false);
            this.gcConexion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerPort1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerIP1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionServerName1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConexionIntentos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbConexionDefaultIP.Properties)).EndInit();
            this.tbpAdminFtpTrace.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTrace)).EndInit();
            this.gcTrace.ResumeLayout(false);
            this.gcTrace.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceProtocol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTracePFileRotate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTracePFileMaxSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTraceLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFtp)).EndInit();
            this.gcFtp.ResumeLayout(false);
            this.gcFtp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbFtpModoEnvioLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFtpLastDateLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpRemoteHost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFtpPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckConfigFull.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tbControl;
        private DevExpress.XtraTab.XtraTabPage tpControlAdmin;
        private DevExpress.XtraTab.XtraTabControl tbAdmin;
        private DevExpress.XtraTab.XtraTabPage tbpAdminTerm;
        private DevExpress.XtraTab.XtraTabPage tbpAdminCnx;
        private DevExpress.XtraTab.XtraTabPage tbpAdminFtpTrace;
        private DevExpress.XtraEditors.GroupControl gcTrace;
        private DevExpress.XtraEditors.ComboBoxEdit cbTraceProtocol;
        private DevExpress.XtraEditors.LabelControl lbTraceProtocol;
        private DevExpress.XtraEditors.ComboBoxEdit cbTracePFileRotate;
        private DevExpress.XtraEditors.LabelControl lbTracePFileRotate;
        private DevExpress.XtraEditors.TextEdit teTracePFileName;
        private DevExpress.XtraEditors.LabelControl lbTracePFileName;
        private DevExpress.XtraEditors.TextEdit teTracePFileMaxSize;
        private DevExpress.XtraEditors.LabelControl lbTracePFileMaxSize;
        private DevExpress.XtraEditors.ComboBoxEdit cbTraceLevel;
        private DevExpress.XtraEditors.LabelControl lbTraceLevel;
        private DevExpress.XtraEditors.GroupControl gcFtp;
        private DevExpress.XtraEditors.ComboBoxEdit cbFtpModoEnvioLog;
        private DevExpress.XtraEditors.LabelControl lbFtpModoEnvio;
        private DevExpress.XtraEditors.LabelControl lbFtpLastDate;
        private DevExpress.XtraEditors.DateEdit deFtpLastDateLog;
        private DevExpress.XtraEditors.TextEdit teFtpRemoteHost;
        private DevExpress.XtraEditors.LabelControl lbFtpRemoteHost;
        private DevExpress.XtraEditors.TextEdit teFtpPort;
        private DevExpress.XtraEditors.LabelControl lbFtpPort;
        private DevExpress.XtraEditors.PanelControl gcTerminal;
        private MyTrackBarControl tcbTerminalIntervalSendCola;
        private MyTrackBarControl tbcTerminalMinWaitRsp;
        private MyTrackBarControl tbcTerminalMaxWaitRsp;
        private MyTrackBarControl tcbTerminalTxDelayOff;
        private MyTrackBarControl tbcTerminalTxDelayOn;
        private MyTrackBarControl tbcTerminalSleepLogout;
        private MyTrackBarControl tbcTerminalUmbralVelocidadParada;
        private MyTrackBarControl tbcTerminalUmbralTempParada;
        private DevExpress.XtraEditors.LabelControl lbTerminalIntervalSendCola;
        private DevExpress.XtraEditors.LabelControl lbTerminalMinWaitRsp;
        private DevExpress.XtraEditors.LabelControl lbTerminalMaxWaitRsp;
        private DevExpress.XtraEditors.LabelControl lbTerminalTxDelayOn;
        private DevExpress.XtraEditors.LabelControl lbTerminalTxDelayOff;
        private DevExpress.XtraEditors.LabelControl lbTerminalSleepLogout;
        private DevExpress.XtraEditors.LabelControl lbTerminal;
        private DevExpress.XtraEditors.LabelControl lbTerminalUmbralTempParada;
        private DevExpress.XtraEditors.TextEdit teTerminalMaxDistancia;
        private DevExpress.XtraEditors.LabelControl lbTerminalMaxDistancia;
        private DevExpress.XtraEditors.PanelControl gcConexion;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort3;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP3;
        private DevExpress.XtraEditors.TextEdit teConexionServerName3;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName3;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort2;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP2;
        private DevExpress.XtraEditors.TextEdit teConexionServerName2;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName2;
        private DevExpress.XtraEditors.TextEdit teConexionServerPort1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerPort1;
        private DevExpress.XtraEditors.TextEdit teConexionServerIP1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerIP1;
        private DevExpress.XtraEditors.TextEdit teConexionServerName1;
        private DevExpress.XtraEditors.LabelControl lbConexionServerName1;
        private DevExpress.XtraEditors.TextEdit teConexionIntentos;
        private DevExpress.XtraEditors.LabelControl lbConexionIntentos;
        private DevExpress.XtraEditors.ComboBoxEdit cbConexionDefaultIP;
        private DevExpress.XtraEditors.LabelControl lbConexionDefaultPositioIP;
        private DevExpress.XtraEditors.SimpleButton cmdCancel;
        private DevExpress.XtraEditors.SimpleButton cmdOK;
        private DevExpress.XtraEditors.CheckEdit ckConfigFull;
        private DevExpress.XtraEditors.TextEdit teDaysForReboot;
        private DevExpress.XtraEditors.LabelControl lbDaysForReboot;
        private DevExpress.XtraEditors.ComboBoxEdit cbTerminalModoColores;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbUserMonitorTimeout;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lbLasShutDown;
        private DevExpress.XtraEditors.TextEdit teLastShutDown;
        private DevExpress.XtraEditors.LabelControl lbDaysForShutDown;
        private DevExpress.XtraEditors.TextEdit teDaysForShutDown;
    }
}