﻿using System;
using System.Windows.Forms;
using gitspt.global.Core.Enums;
using gitspt.global.Core.Exceptions;
using gitspt.global.Core.Log;
using gitspt.global.Core.Util;
using GCPeru.Server.Core.Settings;

namespace gcperu.Server.UI
{
    public class UnhandlerExceptionHandler
    {

        public static void SetDomainEvents()
        {

            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);


            TraceLog.LogInformation("SetDomainEvents.SetEvents. EVENTOS ASIGNADOS");
        }

        public static void SetUiThreadEvents()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            TraceLog.LogInformation("SetUiThreadEvents.SetEvents. EVENTOS ASIGNADOS");
        }

        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            TraceLog.LogInformation("APLICATION EXITED.");
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            if (e.Exception is GpNoAuthorizationPanelException)
            {
                MsgBox.Show(e.Exception.Message, SeverityLevel.Warning);
                return;
            }

            //Hay que obtener el Formulario activo.
            var activeform = ModuleConfig.Global.ActiveForm ?? ModuleConfig.Global.MainForm;

            var excr = e.Exception as gpCrystalReportException;
            if (excr != null)
            {
                if (excr.FormThrowException != null)
                    try
                    {
                        excr.FormThrowException.Close();
                    }
                    catch (System.Exception ex1)
                    {
                        TraceLog.LogException("Application_ThreadException.", ex1);
                    }

                ManejarExceptions.ManejarException((e.Exception as System.Exception), ExceptionErrorLevel.Critical, false, true, (Form) activeform);
                return;
            }

            var ex = new ApplicationException("SE HA PRODUCIDO UN ERROR GRAVE EN EL PROGRAMA.\n", e.Exception);
            ManejarExceptions.ManejarException(ex, ExceptionErrorLevel.Critical, false, true, (Form)activeform);

            //Application.Exit();
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject is GpNoAuthorizationPanelException)
            {
                MsgBox.Show((e.ExceptionObject as System.Exception).Message, SeverityLevel.Warning);
                return;
            }

            //Hay que obtener el Formulario activo.
            var activeform = ModuleConfig.Global.ActiveForm ?? ModuleConfig.Global.MainForm;

            var excr = e.ExceptionObject as gpCrystalReportException;
            if (excr != null)
            {
                if (excr.FormThrowException != null)
                    excr.FormThrowException.Close();

                ManejarExceptions.ManejarException((e.ExceptionObject as System.Exception), ExceptionErrorLevel.Critical, false, true, null);
                return;
            }

            var ex = new System.Exception("SE HA PRODUCIDO UN ERROR GRAVE EN EL PROGRAMA.\nEL PROGRAMA VA A SER CERRADO PARA PREVENIR UN MAL FUNCIONAMIENTO", (System.Exception)e.ExceptionObject);

            if (ex is GpNoAuthorizationPanelException)
            {
                MsgBox.Show(ex.InnerException.Message, SeverityLevel.Warning);
                return;
            }

            ManejarExceptions.ManejarException(ex, ExceptionErrorLevel.Critical, false, true, (Form)activeform);


            if (e.IsTerminating)
            {
                try
                {
                    ModuleConfig.Save();
                }
                catch
                {
                }

                AppDomain.Unload(AppDomain.CurrentDomain);
            }
        }

    }
}