﻿using System;
using System.Windows.Forms;
using gcperu.Server.Core.Initialization;
using gcperu.Server.Core.IoC;
using gcperu.Server.UI.Xtra;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;
using gcperu.Server.UI;

namespace gcperu.Server.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Ioc.Container = new NinjectIocContainer();
            LoadIoCSetting();

            var runcase = Ioc.Container.Get<IRunCase>();
            runcase.Run();
            runcase.Dispose();

            //***************************************************
            //TraceLog.LoadSmartInpectFile(Contantes.UI_FILE_LOG_CONFIG);   //Cargamos el fichero de Configuracion, cualquiera con .sic.
            //TraceLog.LogInformation("MAIN. GCSERVER. INICIA ...");
            //GComunica.CoreServer.InitializationProcess.Initialize();

            //XLayoutGrid.LoadInitialOptionsLayouts();

            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);

            //Application.Run(MDIForm.Create()); 
        }

        private static void LoadIoCSetting()
        {
            var njContainer =  (NinjectIocContainer)Ioc.Container;

#if (EnProduccion)
            njContainer.Kernel.Bind<IRunCase>().To<ProducionCase>();
#else
            //njContainer.Kernel.Bind<IRunCase>().To<TestRun>();
            njContainer.Kernel.Bind<IRunCase>().To<DebugRunCase>();
            //njContainer.Kernel.Bind<IRunCase>().To<RunTaskUtilities>();
            //njContainer.Kernel.Bind<IRunCase>().To<CreatePollsRun>();
#endif

        }
        
    }
}
