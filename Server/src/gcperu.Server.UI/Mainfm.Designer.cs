﻿namespace gcperu.Server.UI
{
    partial class Mainfm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGFecha;
            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGLatitud;
            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLastSession;
            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEstConexion;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mainfm));
            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLoginFrom;
            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHasConfiguration;
            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkingNow;
            DevExpress.XtraBars.BarSubItem barSubItem3;
            DevExpress.XtraBars.BarSubItem barSubItem4;
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraBars.BarSubItem barSubItem5;
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraBars.BarSubItem barSubItem6;
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraBars.BarSubItem barSubItem7;
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.cbrepImgCnxEstado = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageVehEstado = new DevExpress.Utils.ImageCollection(this.components);
            this.rpCbHasConfig = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.rpCbWorkingNow = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.barTaskResendMntos = new DevExpress.XtraBars.BarButtonItem();
            this.barTerminalConfigView = new DevExpress.XtraBars.BarButtonItem();
            this.barTerminalConfigEdit = new DevExpress.XtraBars.BarButtonItem();
            this.btAccGetConfig = new DevExpress.XtraBars.BarButtonItem();
            this.barAssignEstados = new DevExpress.XtraBars.BarButtonItem();
            this.btAccDiscconnect = new DevExpress.XtraBars.BarButtonItem();
            this.barSetOFFLine = new DevExpress.XtraBars.BarButtonItem();
            this.barTerminalHIncid = new DevExpress.XtraBars.BarButtonItem();
            this.barTerminalHConex = new DevExpress.XtraBars.BarButtonItem();
            this.barAssignToGrupo = new DevExpress.XtraBars.BarButtonItem();
            this.barAccTelefono = new DevExpress.XtraBars.BarButtonItem();
            this.barAsignarComment = new DevExpress.XtraBars.BarButtonItem();
            this.barPositionMap = new DevExpress.XtraBars.BarButtonItem();
            this.colEmpID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colEmpresa = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLogoutFrom = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barControlService = new DevExpress.XtraBars.BarButtonItem();
            this.barControlListen = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barGrupos = new DevExpress.XtraBars.BarButtonItem();
            this.barCausaOffline = new DevExpress.XtraBars.BarButtonItem();
            this.barTemplateConfig = new DevExpress.XtraBars.BarButtonItem();
            this.barTiposActividad = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barViewServVeh = new DevExpress.XtraBars.BarButtonItem();
            this.btAccSendAllLI = new DevExpress.XtraBars.BarButtonItem();
            this.btAccSendConfiguracion = new DevExpress.XtraBars.BarButtonItem();
            this.btAccApplyTemplConfig = new DevExpress.XtraBars.BarButtonItem();
            this.btAccTelefonos = new DevExpress.XtraBars.BarButtonItem();
            this.barAccConnectSOS = new DevExpress.XtraBars.BarButtonItem();
            this.barAccConnectSOS2 = new DevExpress.XtraBars.BarButtonItem();
            this.barFilters = new DevExpress.XtraBars.BarSubItem();
            this.barFiltersAdd = new DevExpress.XtraBars.BarButtonItem();
            this.barFiltersLista = new DevExpress.XtraBars.BarButtonItem();
            this.barRefreshData = new DevExpress.XtraBars.BarButtonItem();
            this.barOption = new DevExpress.XtraBars.BarButtonItem();
            this.barRefreshoOption = new DevExpress.XtraBars.BarButtonItem();
            this.barUnLockAdmin = new DevExpress.XtraBars.BarCheckItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barFootInfo = new DevExpress.XtraBars.BarStaticItem();
            this.barOptionCountFooter = new DevExpress.XtraBars.BarSubItem();
            this.barOptionCountFooter_ShowAll = new DevExpress.XtraBars.BarButtonItem();
            this.barOptionCountFooter_ShowOnlyActive = new DevExpress.XtraBars.BarButtonItem();
            this.barOptionCountFooter_ShowOnlyWorking = new DevExpress.XtraBars.BarButtonItem();
            this.barFootInfoCnx = new DevExpress.XtraBars.BarStaticItem();
            this.barFootInfoDCnx = new DevExpress.XtraBars.BarStaticItem();
            this.barFootInfoNum = new DevExpress.XtraBars.BarStaticItem();
            this.barFootRed = new DevExpress.XtraBars.BarStaticItem();
            this.barFootGreen = new DevExpress.XtraBars.BarStaticItem();
            this.barFootYellow = new DevExpress.XtraBars.BarStaticItem();
            this.barFootBlue = new DevExpress.XtraBars.BarStaticItem();
            this.barFootOrange = new DevExpress.XtraBars.BarStaticItem();
            this.barFootProgress = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemMarqueeProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.barUtil = new DevExpress.XtraBars.Bar();
            this.barSelectAll = new DevExpress.XtraBars.BarButtonItem();
            this.barUnSelect = new DevExpress.XtraBars.BarButtonItem();
            this.barSelectiveUpdateLista = new DevExpress.XtraBars.BarButtonItem();
            this.barUpdateStateWorkingNow = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageCol32 = new DevExpress.Utils.ImageCollection(this.components);
            this.barTerminalAddSelectUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barViewPetVeh = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barSubItem8 = new DevExpress.XtraBars.BarSubItem();
            this.barViewSession = new DevExpress.XtraBars.BarButtonItem();
            this.barViewTerminalDataReceived = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.bsTerm = new System.Windows.Forms.BindingSource(this.components);
            this.bsLog = new System.Windows.Forms.BindingSource(this.components);
            this.dbgTerm = new DevExpress.XtraGrid.GridControl();
            this.dbgTermView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.dbgTermBd5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTermEstado = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.cbrepImgVehEstado = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colPwState = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOnLine = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rpCbOnLine = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colOffLineCausa = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPwStateDesde = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMHistoricoDesde = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.dbgTermBd1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colServerID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colInSelectiveUpdate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rpCbInSelUpdate = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colCnfgTemplateID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGrupo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colVC = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colVCAdm = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colVM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTfoVeh = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTipoActividad = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colConductor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNS = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSoftwareTipoEnum = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colIccid = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colVersion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colUltInfo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLastReception = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFDetenido = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.dbgTermBd2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colGLongitud = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.GpsFixGeoTx = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGNSat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGFM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOdometro = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colGVelocidad = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.dbgTermBd3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colConnectFrom = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDisconnectFrom = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDisconnectCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ttc = new DevExpress.Utils.ToolTipController(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.imageMini = new DevExpress.Utils.ImageCollection(this.components);
            this.popupGridOptions = new DevExpress.XtraBars.PopupMenu(this.components);
            colGFecha = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            colGLatitud = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            colLastSession = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            colEstConexion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            colLoginFrom = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            colHasConfiguration = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            colWorkingNow = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            barSubItem6 = new DevExpress.XtraBars.BarSubItem();
            barSubItem7 = new DevExpress.XtraBars.BarSubItem();
            ((System.ComponentModel.ISupportInitialize)(this.cbrepImgCnxEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageVehEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpCbHasConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpCbWorkingNow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCol32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgTerm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgTermView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbrepImgVehEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpCbOnLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpCbInSelUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageMini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupGridOptions)).BeginInit();
            this.SuspendLayout();
            // 
            // colGFecha
            // 
            colGFecha.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            colGFecha.AppearanceCell.Options.UseFont = true;
            colGFecha.Caption = "Gps Fecha";
            colGFecha.DisplayFormat.FormatString = "dd-MM-yy HH:mm:ss";
            colGFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            colGFecha.FieldName = "GpsFecha";
            colGFecha.Name = "colGFecha";
            colGFecha.Visible = true;
            colGFecha.Width = 106;
            // 
            // colGLatitud
            // 
            colGLatitud.Caption = "Latitud";
            colGLatitud.FieldName = "GpsLatitud";
            colGLatitud.Name = "colGLatitud";
            colGLatitud.Visible = true;
            colGLatitud.Width = 63;
            // 
            // colLastSession
            // 
            colLastSession.Caption = "Ult.Sesion";
            colLastSession.DisplayFormat.FormatString = "d2";
            colLastSession.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            colLastSession.FieldName = "TerminalLastSessionDuration";
            colLastSession.Name = "colLastSession";
            colLastSession.ToolTip = "Duracion entre la Ultima Conexion/Desconexion";
            colLastSession.Visible = true;
            colLastSession.Width = 48;
            // 
            // colEstConexion
            // 
            colEstConexion.AppearanceCell.Options.UseTextOptions = true;
            colEstConexion.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            colEstConexion.Caption = " ";
            colEstConexion.ColumnEdit = this.cbrepImgCnxEstado;
            colEstConexion.FieldName = "EstadoConexion";
            colEstConexion.ImageIndex = 6;
            colEstConexion.MinWidth = 40;
            colEstConexion.Name = "colEstConexion";
            colEstConexion.OptionsColumn.AllowSize = false;
            colEstConexion.OptionsColumn.FixedWidth = true;
            colEstConexion.OptionsColumn.ShowCaption = false;
            colEstConexion.ToolTip = "Indica el estado de Conexion del Terminal";
            colEstConexion.Visible = true;
            colEstConexion.Width = 40;
            // 
            // cbrepImgCnxEstado
            // 
            this.cbrepImgCnxEstado.AutoHeight = false;
            this.cbrepImgCnxEstado.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbrepImgCnxEstado.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cbrepImgCnxEstado.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Conectado", 1, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Sin Conexion", 0, 5)});
            this.cbrepImgCnxEstado.Name = "cbrepImgCnxEstado";
            this.cbrepImgCnxEstado.SmallImages = this.imageVehEstado;
            // 
            // imageVehEstado
            // 
            this.imageVehEstado.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageVehEstado.ImageStream")));
            this.imageVehEstado.Images.SetKeyName(7, "Configuration16");
            this.imageVehEstado.Images.SetKeyName(8, "Cancel16");
            this.imageVehEstado.Images.SetKeyName(9, "TerminalUpdate16.ico");
            this.imageVehEstado.Images.SetKeyName(10, "OnLine16.ico");
            this.imageVehEstado.Images.SetKeyName(11, "OffLine16.ico");
            this.imageVehEstado.Images.SetKeyName(12, "CarOrange16.ico");
            // 
            // colLoginFrom
            // 
            colLoginFrom.Caption = "Ini.Sesión";
            colLoginFrom.DisplayFormat.FormatString = "dd-MM-yy HH:mm";
            colLoginFrom.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            colLoginFrom.FieldName = "LoginFrom";
            colLoginFrom.Name = "colLoginFrom";
            colLoginFrom.ToolTip = "Fecha de inicio de Sesión del conductor en el Terminal";
            colLoginFrom.Visible = true;
            colLoginFrom.Width = 100;
            // 
            // colHasConfiguration
            // 
            colHasConfiguration.AppearanceCell.Options.UseTextOptions = true;
            colHasConfiguration.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            colHasConfiguration.ColumnEdit = this.rpCbHasConfig;
            colHasConfiguration.FieldName = "HasConfiguration";
            colHasConfiguration.Name = "colHasConfiguration";
            colHasConfiguration.OptionsColumn.FixedWidth = true;
            colHasConfiguration.OptionsColumn.ShowCaption = false;
            colHasConfiguration.ToolTip = "Indica si el Terminal posee una configuración obtenida.";
            colHasConfiguration.Visible = true;
            colHasConfiguration.Width = 20;
            // 
            // rpCbHasConfig
            // 
            this.rpCbHasConfig.AutoHeight = false;
            this.rpCbHasConfig.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpCbHasConfig.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rpCbHasConfig.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 8)});
            this.rpCbHasConfig.Name = "rpCbHasConfig";
            this.rpCbHasConfig.SmallImages = this.imageVehEstado;
            // 
            // colWorkingNow
            // 
            colWorkingNow.AppearanceCell.Options.UseTextOptions = true;
            colWorkingNow.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            colWorkingNow.ColumnEdit = this.rpCbWorkingNow;
            colWorkingNow.FieldName = "WorkingNow";
            colWorkingNow.ImageIndex = 12;
            colWorkingNow.MinWidth = 40;
            colWorkingNow.Name = "colWorkingNow";
            colWorkingNow.OptionsColumn.AllowSize = false;
            colWorkingNow.OptionsColumn.ShowCaption = false;
            colWorkingNow.Visible = true;
            colWorkingNow.Width = 40;
            // 
            // rpCbWorkingNow
            // 
            this.rpCbWorkingNow.AutoHeight = false;
            this.rpCbWorkingNow.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpCbWorkingNow.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rpCbWorkingNow.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Trabajando", true, 12),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Descanso", false, -1)});
            this.rpCbWorkingNow.Name = "rpCbWorkingNow";
            this.rpCbWorkingNow.SmallImages = this.imageVehEstado;
            // 
            // barSubItem3
            // 
            barSubItem3.Caption = "Tareas";
            barSubItem3.Id = 66;
            barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barTaskResendMntos)});
            barSubItem3.Name = "barSubItem3";
            // 
            // barTaskResendMntos
            // 
            this.barTaskResendMntos.Caption = "Re-enviar Mantenimientos";
            this.barTaskResendMntos.Id = 72;
            this.barTaskResendMntos.Name = "barTaskResendMntos";
            this.barTaskResendMntos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barTaskResendMntos_ItemClick);
            // 
            // barSubItem4
            // 
            barSubItem4.Caption = "Configuracion Terminal";
            barSubItem4.Id = 68;
            barSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barTerminalConfigView),
            new DevExpress.XtraBars.LinkPersistInfo(this.barTerminalConfigEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.btAccGetConfig)});
            barSubItem4.Name = "barSubItem4";
            // 
            // barTerminalConfigView
            // 
            this.barTerminalConfigView.Caption = "Ver Configuracion Terminal";
            this.barTerminalConfigView.Glyph = global::gcperu.Server.UI.Properties.Resources.TerminalConfig;
            this.barTerminalConfigView.Id = 26;
            this.barTerminalConfigView.Name = "barTerminalConfigView";
            toolTipTitleItem1.Text = "Configuracion Termional";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Permite visualizar la configuracion guarda del Terminal.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.barTerminalConfigView.SuperTip = superToolTip1;
            this.barTerminalConfigView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barTerminalConfigView_ItemClick);
            // 
            // barTerminalConfigEdit
            // 
            this.barTerminalConfigEdit.Caption = "Modificar Configuracion y Enviar";
            this.barTerminalConfigEdit.Glyph = global::gcperu.Server.UI.Properties.Resources.ConfigEdit;
            this.barTerminalConfigEdit.Id = 27;
            this.barTerminalConfigEdit.Name = "barTerminalConfigEdit";
            this.barTerminalConfigEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barTerminalConfigEdit_ItemClick);
            // 
            // btAccGetConfig
            // 
            this.btAccGetConfig.Caption = "Obtener Configuracion Dispositivos";
            this.btAccGetConfig.Id = 23;
            this.btAccGetConfig.Name = "btAccGetConfig";
            this.btAccGetConfig.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btAccGetConfig_ItemClick);
            // 
            // barSubItem5
            // 
            barSubItem5.Caption = "Estado Terminal";
            barSubItem5.Id = 69;
            barSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barAssignEstados),
            new DevExpress.XtraBars.LinkPersistInfo(this.btAccDiscconnect),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSetOFFLine, true)});
            barSubItem5.Name = "barSubItem5";
            // 
            // barAssignEstados
            // 
            this.barAssignEstados.Caption = "Cambiar Estado Terminal (Forzar Nuevo Estado)";
            this.barAssignEstados.Id = 46;
            this.barAssignEstados.ImageIndex = 20;
            this.barAssignEstados.Name = "barAssignEstados";
            toolTipTitleItem2.Text = "Cambio de Estado Manual";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Permite cambiar el estado del Terminal, de una forma manual.\r\nAplicando un estado" +
    " a la fuerza.\r\nUtil cuando los terminales han quedado en un estado no correcto a" +
    "l apagarse o perder la conexion.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.barAssignEstados.SuperTip = superToolTip2;
            this.barAssignEstados.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barAssignEstado_ItemClick);
            // 
            // btAccDiscconnect
            // 
            this.btAccDiscconnect.Caption = "Desconectar Terminal";
            this.btAccDiscconnect.Id = 33;
            this.btAccDiscconnect.Name = "btAccDiscconnect";
            toolTipTitleItem3.Text = "Forzar Desconexión";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Permite desconectar los terminales seleccionados. Es util, para forzarlos a volve" +
    "r a que se contecten sin tener que detener el Servicio.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.btAccDiscconnect.SuperTip = superToolTip3;
            this.btAccDiscconnect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btAccDiscconnect_ItemClick);
            // 
            // barSetOFFLine
            // 
            this.barSetOFFLine.Caption = "Cambiar a OFF-Line";
            this.barSetOFFLine.Glyph = ((System.Drawing.Image)(resources.GetObject("barSetOFFLine.Glyph")));
            this.barSetOFFLine.Id = 49;
            this.barSetOFFLine.Name = "barSetOFFLine";
            toolTipTitleItem4.Text = "Cambiar a OFF-Line";
            toolTipItem4.LeftIndent = 6;
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barSetOFFLine.SuperTip = superToolTip4;
            // 
            // barSubItem6
            // 
            barSubItem6.Caption = "Historial";
            barSubItem6.Id = 70;
            barSubItem6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barTerminalHIncid),
            new DevExpress.XtraBars.LinkPersistInfo(this.barTerminalHConex)});
            barSubItem6.Name = "barSubItem6";
            // 
            // barTerminalHIncid
            // 
            this.barTerminalHIncid.Caption = "Historial Incidencias";
            this.barTerminalHIncid.Glyph = global::gcperu.Server.UI.Properties.Resources.Grafico24;
            this.barTerminalHIncid.Id = 29;
            this.barTerminalHIncid.Name = "barTerminalHIncid";
            toolTipTitleItem5.Text = "Historial de Incidencias.";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Muestra la Incidencias de cualquier tipo que el Terminal ha enviado. (Reinicios, " +
    "Apagados y cierres del programa, etc..)";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.barTerminalHIncid.SuperTip = superToolTip5;
            this.barTerminalHIncid.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barTerminalHIncid_ItemClick);
            // 
            // barTerminalHConex
            // 
            this.barTerminalHConex.Caption = "Historial Conexiones";
            this.barTerminalHConex.Glyph = global::gcperu.Server.UI.Properties.Resources.Grafico24;
            this.barTerminalHConex.Id = 28;
            this.barTerminalHConex.Name = "barTerminalHConex";
            toolTipTitleItem6.Text = "Historial de Conexion";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Muestra un lista de la conexion/desconexiones que ha sufrido un terminal.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.barTerminalHConex.SuperTip = superToolTip6;
            this.barTerminalHConex.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barTerminalHConex_ItemClick);
            // 
            // barSubItem7
            // 
            barSubItem7.Caption = "Utilidades";
            barSubItem7.Id = 71;
            barSubItem7.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barAssignToGrupo),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barAccTelefono, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barAsignarComment),
            new DevExpress.XtraBars.LinkPersistInfo(this.barPositionMap)});
            barSubItem7.Name = "barSubItem7";
            // 
            // barAssignToGrupo
            // 
            this.barAssignToGrupo.Caption = "Asignar a Grupo";
            this.barAssignToGrupo.Description = "Seleccione los Terminales y asigneles un Grupo";
            this.barAssignToGrupo.Id = 31;
            this.barAssignToGrupo.Name = "barAssignToGrupo";
            toolTipTitleItem7.Text = "Asignar a Grupo";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Permite asignar los terminales selecionados al grupo que seleccione en un despleg" +
    "able.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.barAssignToGrupo.SuperTip = superToolTip7;
            this.barAssignToGrupo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barAssignToGrupo_ItemClick);
            // 
            // barAccTelefono
            // 
            this.barAccTelefono.Caption = "Asignar Telefono";
            this.barAccTelefono.Glyph = global::gcperu.Server.UI.Properties.Resources.Telefono24;
            this.barAccTelefono.Id = 36;
            this.barAccTelefono.Name = "barAccTelefono";
            toolTipTitleItem8.Text = "Asignar Telefono al Vehiculo";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Permite asignar un Telefono al Vehiculo.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.barAccTelefono.SuperTip = superToolTip8;
            // 
            // barAsignarComment
            // 
            this.barAsignarComment.Caption = "Comentario";
            this.barAsignarComment.Glyph = ((System.Drawing.Image)(resources.GetObject("barAsignarComment.Glyph")));
            this.barAsignarComment.Id = 34;
            this.barAsignarComment.Name = "barAsignarComment";
            toolTipTitleItem9.Text = "Asignar Comentario";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Permite asignar un Comentario al Terminal.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.barAsignarComment.SuperTip = superToolTip9;
            this.barAsignarComment.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barAsignarComment_ItemClick);
            // 
            // barPositionMap
            // 
            this.barPositionMap.Caption = "Google Maps URL Clipboard";
            this.barPositionMap.Description = "Google Maps URL Clipboard";
            this.barPositionMap.Id = 56;
            this.barPositionMap.ImageIndex = 21;
            this.barPositionMap.Name = "barPositionMap";
            this.barPositionMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barPositionMap_ItemClick);
            // 
            // colEmpID
            // 
            this.colEmpID.FieldName = "EmpresaId";
            this.colEmpID.Name = "colEmpID";
            // 
            // colEmpresa
            // 
            this.colEmpresa.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colEmpresa.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.colEmpresa.AppearanceHeader.Options.UseFont = true;
            this.colEmpresa.AppearanceHeader.Options.UseForeColor = true;
            this.colEmpresa.Caption = "Empresa";
            this.colEmpresa.FieldName = "EmpresaNombre";
            this.colEmpresa.Name = "colEmpresa";
            this.colEmpresa.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colEmpresa.Visible = true;
            this.colEmpresa.Width = 127;
            // 
            // colLogoutFrom
            // 
            this.colLogoutFrom.Caption = "Fin Sesión";
            this.colLogoutFrom.DisplayFormat.FormatString = "dd-MM-yy HH:mm";
            this.colLogoutFrom.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colLogoutFrom.FieldName = "LogoutFrom";
            this.colLogoutFrom.Name = "colLogoutFrom";
            this.colLogoutFrom.ToolTip = "Fecha de FIN de Sesión del conductor en el Terminal";
            this.colLogoutFrom.Visible = true;
            this.colLogoutFrom.Width = 129;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.barUtil});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageCol32;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barControlService,
            this.barControlListen,
            this.barOption,
            this.barFootInfo,
            this.barFootProgress,
            this.barRefreshoOption,
            this.barRefreshData,
            this.barFootInfoCnx,
            this.barFootInfoDCnx,
            this.barFootInfoNum,
            this.barFootRed,
            this.barFootGreen,
            this.barFootYellow,
            this.barFootBlue,
            this.barFootOrange,
            this.barSubItem1,
            this.btAccSendAllLI,
            this.btAccSendConfiguracion,
            this.btAccGetConfig,
            this.barSelectAll,
            this.barUnSelect,
            this.barTerminalConfigView,
            this.barTerminalConfigEdit,
            this.barTerminalHConex,
            this.barTerminalHIncid,
            this.barGrupos,
            this.barAssignToGrupo,
            this.barUnLockAdmin,
            this.btAccDiscconnect,
            this.barAsignarComment,
            this.btAccTelefonos,
            this.barAccTelefono,
            this.barOptionCountFooter,
            this.barOptionCountFooter_ShowAll,
            this.barOptionCountFooter_ShowOnlyActive,
            this.barTerminalAddSelectUpdate,
            this.barSelectiveUpdateLista,
            this.barAssignEstados,
            this.barSubItem2,
            this.barCausaOffline,
            this.barSetOFFLine,
            this.barFilters,
            this.barFiltersAdd,
            this.barFiltersLista,
            this.barAccConnectSOS,
            this.barPositionMap,
            this.barAccConnectSOS2,
            this.barUpdateStateWorkingNow,
            this.barOptionCountFooter_ShowOnlyWorking,
            this.barViewServVeh,
            this.barButtonItem1,
            this.barTemplateConfig,
            this.btAccApplyTemplConfig,
            this.barTiposActividad,
            this.barViewPetVeh,
            barSubItem3,
            this.barStaticItem1,
            barSubItem4,
            barSubItem5,
            barSubItem6,
            barSubItem7,
            this.barTaskResendMntos,
            this.barSubItem8,
            this.barViewSession,
            this.barViewTerminalDataReceived});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 76;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMarqueeProgressBar1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar2.Appearance.Options.UseFont = true;
            this.bar2.BarName = "Main menu";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barControlService, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barControlListen, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFilters),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barRefreshData, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barOption, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barRefreshoOption, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barUnLockAdmin, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barControlService
            // 
            this.barControlService.Caption = "Iniciar Servicio";
            this.barControlService.Id = 1;
            this.barControlService.ImageIndex = 1;
            this.barControlService.Name = "barControlService";
            toolTipTitleItem10.Text = "Manejar Servicio de Windows";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Inicia o Detiene el Servicio.\r\nSi el servicio se detiene, se detienen todos los p" +
    "rocesos asociaos.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.barControlService.SuperTip = superToolTip10;
            this.barControlService.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barControlService_CheckedChanged);
            // 
            // barControlListen
            // 
            this.barControlListen.Caption = "Permitir Conexiones";
            this.barControlListen.Id = 2;
            this.barControlListen.ImageIndex = 4;
            this.barControlListen.Name = "barControlListen";
            toolTipTitleItem11.Text = "Gestión Conexiones";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Permite o Deniega la conexion de los Terminales a este Servidor. \r\nSi se deniega," +
    " ningun terminal podrá conectarse.\r\nSi se activa, los terminales vinculados a es" +
    "te servidor, podrán conectarse.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.barControlListen.SuperTip = superToolTip11;
            this.barControlListen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barControlListen_CheckedChanged);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Ficheros";
            this.barSubItem2.Id = 47;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barGrupos),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCausaOffline),
            new DevExpress.XtraBars.LinkPersistInfo(this.barTemplateConfig),
            new DevExpress.XtraBars.LinkPersistInfo(this.barTiposActividad)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barGrupos
            // 
            this.barGrupos.Caption = "Grupos";
            this.barGrupos.Description = "Abre el Panel para el Mantenimiento de los Grupos";
            this.barGrupos.Id = 30;
            this.barGrupos.Name = "barGrupos";
            toolTipTitleItem12.Text = "Definicion de Grupos";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Permite crear, modificar y borrar los distintos grupos.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.barGrupos.SuperTip = superToolTip12;
            this.barGrupos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barGrupos_ItemClick);
            // 
            // barCausaOffline
            // 
            this.barCausaOffline.Caption = "Causas OFF-Line";
            this.barCausaOffline.Id = 48;
            this.barCausaOffline.Name = "barCausaOffline";
            // 
            // barTemplateConfig
            // 
            this.barTemplateConfig.Caption = "Plantillas de Configuración";
            this.barTemplateConfig.Id = 62;
            this.barTemplateConfig.Name = "barTemplateConfig";
            this.barTemplateConfig.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barTemplateConfig_ItemClick);
            // 
            // barTiposActividad
            // 
            this.barTiposActividad.Caption = "Tipos de Actividad";
            this.barTiposActividad.Id = 64;
            this.barTiposActividad.Name = "barTiposActividad";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Acciones";
            this.barSubItem1.Id = 19;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barViewServVeh),
            new DevExpress.XtraBars.LinkPersistInfo(this.btAccSendAllLI),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.btAccSendConfiguracion, "Enviar Configuracion Masiva", false, true, false, 0),
            new DevExpress.XtraBars.LinkPersistInfo(this.btAccApplyTemplConfig),
            new DevExpress.XtraBars.LinkPersistInfo(this.btAccGetConfig),
            new DevExpress.XtraBars.LinkPersistInfo(this.btAccDiscconnect, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btAccTelefonos),
            new DevExpress.XtraBars.LinkPersistInfo(this.barAccConnectSOS, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barAccConnectSOS2)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barViewServVeh
            // 
            this.barViewServVeh.Caption = "Mostrar Servicios para el Vehiculo";
            this.barViewServVeh.Id = 60;
            this.barViewServVeh.Name = "barViewServVeh";
            // 
            // btAccSendAllLI
            // 
            this.btAccSendAllLI.Caption = "Enviar Lista Incidencias";
            this.btAccSendAllLI.Id = 21;
            this.btAccSendAllLI.Name = "btAccSendAllLI";
            toolTipItem13.Text = "Envia la lista de Incidencias a los Terminales.\r\nDe esta manera, los terminales, " +
    "obtendrán una lista actualizada de las mismas.";
            superToolTip13.Items.Add(toolTipItem13);
            this.btAccSendAllLI.SuperTip = superToolTip13;
            this.btAccSendAllLI.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btAccSendLI_ItemClick);
            // 
            // btAccSendConfiguracion
            // 
            this.btAccSendConfiguracion.Caption = "Enviar Configuracion Plantilla";
            this.btAccSendConfiguracion.Id = 22;
            this.btAccSendConfiguracion.Name = "btAccSendConfiguracion";
            this.btAccSendConfiguracion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btAccSendConfiguracion_ItemClick);
            // 
            // btAccApplyTemplConfig
            // 
            this.btAccApplyTemplConfig.Caption = "Asignar Plantilla de Configuración";
            this.btAccApplyTemplConfig.Id = 63;
            this.btAccApplyTemplConfig.Name = "btAccApplyTemplConfig";
            this.btAccApplyTemplConfig.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btAccApplyTemplConfig_ItemClick);
            // 
            // btAccTelefonos
            // 
            this.btAccTelefonos.Caption = "Actualizar Telefonos";
            this.btAccTelefonos.Id = 35;
            this.btAccTelefonos.Name = "btAccTelefonos";
            this.btAccTelefonos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btAccTelefonos_ItemClick);
            // 
            // barAccConnectSOS
            // 
            this.barAccConnectSOS.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.barAccConnectSOS.Appearance.Options.UseForeColor = true;
            this.barAccConnectSOS.Border = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.barAccConnectSOS.Caption = "Enviar Solicitud Conectar SOS";
            this.barAccConnectSOS.Id = 55;
            this.barAccConnectSOS.Name = "barAccConnectSOS";
            this.barAccConnectSOS.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barAccConnectSOS_ItemClick);
            // 
            // barAccConnectSOS2
            // 
            this.barAccConnectSOS2.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.barAccConnectSOS2.Appearance.Options.UseForeColor = true;
            this.barAccConnectSOS2.Caption = "Enviar Solicitud Conectar SOS 2";
            this.barAccConnectSOS2.Id = 57;
            this.barAccConnectSOS2.Name = "barAccConnectSOS2";
            this.barAccConnectSOS2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barAccConnectSOS_ItemClick);
            // 
            // barFilters
            // 
            this.barFilters.Appearance.Options.UseTextOptions = true;
            this.barFilters.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.barFilters.Caption = "Filtros Definidos";
            this.barFilters.Id = 50;
            this.barFilters.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barFiltersAdd, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFiltersLista)});
            this.barFilters.Name = "barFilters";
            // 
            // barFiltersAdd
            // 
            this.barFiltersAdd.Caption = "Agregar Filtro Aplicado a Rejilla";
            this.barFiltersAdd.Id = 53;
            this.barFiltersAdd.Name = "barFiltersAdd";
            this.barFiltersAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barFiltersAdd_ItemClick);
            // 
            // barFiltersLista
            // 
            this.barFiltersLista.Caption = "Ver Lista";
            this.barFiltersLista.Id = 54;
            this.barFiltersLista.Name = "barFiltersLista";
            this.barFiltersLista.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barFiltersLista_ItemClick);
            // 
            // barRefreshData
            // 
            this.barRefreshData.Caption = "Refrescar";
            this.barRefreshData.Id = 7;
            this.barRefreshData.ImageIndex = 6;
            this.barRefreshData.Name = "barRefreshData";
            this.barRefreshData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barRefreshData_ItemClick);
            // 
            // barOption
            // 
            this.barOption.Caption = "Opciones Servidor";
            this.barOption.Id = 3;
            this.barOption.ImageIndex = 2;
            this.barOption.Name = "barOption";
            this.barOption.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barOption_ItemClick);
            // 
            // barRefreshoOption
            // 
            this.barRefreshoOption.Id = 6;
            this.barRefreshoOption.ImageIndex = 5;
            this.barRefreshoOption.Name = "barRefreshoOption";
            toolTipTitleItem13.Text = "Refrescar configuracion";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Refrescar configuración del Servidor.";
            superToolTip14.Items.Add(toolTipTitleItem13);
            superToolTip14.Items.Add(toolTipItem14);
            this.barRefreshoOption.SuperTip = superToolTip14;
            this.barRefreshoOption.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barRefreshoOption_ItemClick);
            // 
            // barUnLockAdmin
            // 
            this.barUnLockAdmin.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barUnLockAdmin.Appearance.ForeColor = System.Drawing.Color.DarkGreen;
            this.barUnLockAdmin.Appearance.Options.UseFont = true;
            this.barUnLockAdmin.Appearance.Options.UseForeColor = true;
            this.barUnLockAdmin.Caption = "Desbloquear";
            this.barUnLockAdmin.Description = "Desbloquear Funciones de Adminstracion";
            this.barUnLockAdmin.Id = 32;
            this.barUnLockAdmin.ImageIndex = 16;
            this.barUnLockAdmin.Name = "barUnLockAdmin";
            this.barUnLockAdmin.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barUnLockAdmin_CheckedChanged);
            // 
            // bar3
            // 
            this.bar3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar3.Appearance.Options.UseFont = true;
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootInfo),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barOptionCountFooter, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootInfoCnx),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootInfoDCnx),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootInfoNum),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootRed),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootGreen),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootYellow),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootBlue),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootOrange),
            new DevExpress.XtraBars.LinkPersistInfo(this.barFootProgress)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barFootInfo
            // 
            this.barFootInfo.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.barFootInfo.Id = 4;
            this.barFootInfo.ImageIndex = 9;
            this.barFootInfo.Name = "barFootInfo";
            this.barFootInfo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barFootInfo.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barFootInfo.Width = 100;
            // 
            // barOptionCountFooter
            // 
            this.barOptionCountFooter.Caption = "Activos";
            this.barOptionCountFooter.Id = 37;
            this.barOptionCountFooter.ImageIndex = 18;
            this.barOptionCountFooter.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barOptionCountFooter_ShowAll),
            new DevExpress.XtraBars.LinkPersistInfo(this.barOptionCountFooter_ShowOnlyActive),
            new DevExpress.XtraBars.LinkPersistInfo(this.barOptionCountFooter_ShowOnlyWorking)});
            this.barOptionCountFooter.Name = "barOptionCountFooter";
            // 
            // barOptionCountFooter_ShowAll
            // 
            this.barOptionCountFooter_ShowAll.Caption = "Mostrar Todos";
            this.barOptionCountFooter_ShowAll.Id = 40;
            this.barOptionCountFooter_ShowAll.Name = "barOptionCountFooter_ShowAll";
            this.barOptionCountFooter_ShowAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barOptionCountFooter_ShowAll_ItemClick);
            // 
            // barOptionCountFooter_ShowOnlyActive
            // 
            this.barOptionCountFooter_ShowOnlyActive.Caption = "Mostrar Solo Activos (No apagados o en Pruebas)";
            this.barOptionCountFooter_ShowOnlyActive.Id = 41;
            this.barOptionCountFooter_ShowOnlyActive.Name = "barOptionCountFooter_ShowOnlyActive";
            this.barOptionCountFooter_ShowOnlyActive.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barOptionCountFooter_ShowOnlyActive_ItemClick);
            // 
            // barOptionCountFooter_ShowOnlyWorking
            // 
            this.barOptionCountFooter_ShowOnlyWorking.Caption = "Mostrar Trabajando Ahora";
            this.barOptionCountFooter_ShowOnlyWorking.Id = 59;
            this.barOptionCountFooter_ShowOnlyWorking.Name = "barOptionCountFooter_ShowOnlyWorking";
            // 
            // barFootInfoCnx
            // 
            this.barFootInfoCnx.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.barFootInfoCnx.Caption = "  5";
            this.barFootInfoCnx.Id = 8;
            this.barFootInfoCnx.ImageIndex = 7;
            this.barFootInfoCnx.Name = "barFootInfoCnx";
            this.barFootInfoCnx.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barFootInfoCnx.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barFootInfoCnx.Width = 30;
            // 
            // barFootInfoDCnx
            // 
            this.barFootInfoDCnx.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.barFootInfoDCnx.Caption = "   25";
            this.barFootInfoDCnx.Id = 9;
            this.barFootInfoDCnx.ImageIndex = 8;
            this.barFootInfoDCnx.Name = "barFootInfoDCnx";
            this.barFootInfoDCnx.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barFootInfoDCnx.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barFootInfoDCnx.Width = 40;
            // 
            // barFootInfoNum
            // 
            this.barFootInfoNum.Caption = "Nº:   120";
            this.barFootInfoNum.Id = 10;
            this.barFootInfoNum.Name = "barFootInfoNum";
            this.barFootInfoNum.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barFootRed
            // 
            this.barFootRed.Caption = "0";
            this.barFootRed.Id = 13;
            this.barFootRed.ImageIndex = 15;
            this.barFootRed.Name = "barFootRed";
            this.barFootRed.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barFootRed.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barFootGreen
            // 
            this.barFootGreen.Caption = "0";
            this.barFootGreen.Id = 14;
            this.barFootGreen.ImageIndex = 13;
            this.barFootGreen.Name = "barFootGreen";
            this.barFootGreen.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barFootGreen.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barFootYellow
            // 
            this.barFootYellow.Caption = "0";
            this.barFootYellow.Id = 15;
            this.barFootYellow.ImageIndex = 11;
            this.barFootYellow.Name = "barFootYellow";
            this.barFootYellow.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barFootYellow.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barFootBlue
            // 
            this.barFootBlue.Caption = "0";
            this.barFootBlue.Id = 16;
            this.barFootBlue.ImageIndex = 12;
            this.barFootBlue.Name = "barFootBlue";
            this.barFootBlue.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barFootBlue.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barFootOrange
            // 
            this.barFootOrange.Caption = "0";
            this.barFootOrange.Id = 17;
            this.barFootOrange.ImageIndex = 14;
            this.barFootOrange.Name = "barFootOrange";
            this.barFootOrange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barFootOrange.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barFootProgress
            // 
            this.barFootProgress.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barFootProgress.Appearance.Options.UseFont = true;
            this.barFootProgress.Edit = this.repositoryItemMarqueeProgressBar1;
            this.barFootProgress.EditHeight = 20;
            this.barFootProgress.Id = 5;
            this.barFootProgress.Name = "barFootProgress";
            this.barFootProgress.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barFootProgress.Width = 100;
            // 
            // repositoryItemMarqueeProgressBar1
            // 
            this.repositoryItemMarqueeProgressBar1.Name = "repositoryItemMarqueeProgressBar1";
            // 
            // barUtil
            // 
            this.barUtil.BarName = "Utilbar";
            this.barUtil.DockCol = 0;
            this.barUtil.DockRow = 1;
            this.barUtil.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barUtil.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSelectAll, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barUnSelect, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barTerminalConfigView, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barTerminalConfigEdit, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barTerminalHConex, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barTerminalHIncid, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSelectiveUpdateLista),
            new DevExpress.XtraBars.LinkPersistInfo(this.barGrupos),
            new DevExpress.XtraBars.LinkPersistInfo(this.barAssignToGrupo),
            new DevExpress.XtraBars.LinkPersistInfo(this.barAsignarComment, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barAccTelefono),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSetOFFLine, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barAssignEstados),
            new DevExpress.XtraBars.LinkPersistInfo(this.barPositionMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.barUpdateStateWorkingNow)});
            this.barUtil.OptionsBar.AllowQuickCustomization = false;
            this.barUtil.OptionsBar.AllowRename = true;
            this.barUtil.OptionsBar.UseWholeRow = true;
            this.barUtil.Text = "Utilbar";
            // 
            // barSelectAll
            // 
            this.barSelectAll.Caption = "Selecionar Todos";
            this.barSelectAll.Glyph = global::gcperu.Server.UI.Properties.Resources.Select16;
            this.barSelectAll.Id = 24;
            this.barSelectAll.Name = "barSelectAll";
            toolTipTitleItem14.Text = "Seleccion";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Permite seleccionar todos los Vehiculos en la Lista.";
            superToolTip15.Items.Add(toolTipTitleItem14);
            superToolTip15.Items.Add(toolTipItem15);
            this.barSelectAll.SuperTip = superToolTip15;
            this.barSelectAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSelectAll_ItemClick);
            // 
            // barUnSelect
            // 
            this.barUnSelect.Caption = "Quitar Selección";
            this.barUnSelect.Glyph = global::gcperu.Server.UI.Properties.Resources.UnSelect16;
            this.barUnSelect.Id = 25;
            this.barUnSelect.Name = "barUnSelect";
            toolTipTitleItem15.Text = "No selección";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Quita la selección a los registros que estuvieran seleccionados.";
            superToolTip16.Items.Add(toolTipTitleItem15);
            superToolTip16.Items.Add(toolTipItem16);
            this.barUnSelect.SuperTip = superToolTip16;
            this.barUnSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barUnSelect_ItemClick);
            // 
            // barSelectiveUpdateLista
            // 
            this.barSelectiveUpdateLista.Caption = "Ver lista actualizacion selectiva";
            this.barSelectiveUpdateLista.Id = 45;
            this.barSelectiveUpdateLista.ImageIndex = 19;
            this.barSelectiveUpdateLista.Name = "barSelectiveUpdateLista";
            this.barSelectiveUpdateLista.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSelectiveUpdateLista_ItemClick);
            // 
            // barUpdateStateWorkingNow
            // 
            this.barUpdateStateWorkingNow.Caption = "Actualizar Estado Vehiculos Trabajando";
            this.barUpdateStateWorkingNow.Id = 58;
            this.barUpdateStateWorkingNow.ImageIndex = 22;
            this.barUpdateStateWorkingNow.Name = "barUpdateStateWorkingNow";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1212, 84);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 664);
            this.barDockControlBottom.Size = new System.Drawing.Size(1212, 47);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 84);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 580);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1212, 84);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 580);
            // 
            // imageCol32
            // 
            this.imageCol32.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCol32.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCol32.ImageStream")));
            this.imageCol32.Images.SetKeyName(16, "AdminLock.ico");
            this.imageCol32.Images.SetKeyName(17, "AdminUnlock.ico");
            this.imageCol32.Images.SetKeyName(18, "Calculadora.32.ico");
            this.imageCol32.Images.SetKeyName(19, "TerminalUpdate.ico");
            this.imageCol32.Images.SetKeyName(20, "ChangeState32.ico");
            this.imageCol32.Images.SetKeyName(21, "BallonGMao48.ico");
            this.imageCol32.Images.SetKeyName(22, "CarRefreshOrange32.ico");
            this.imageCol32.Images.SetKeyName(23, "Add.png");
            this.imageCol32.Images.SetKeyName(24, "CheckList");
            // 
            // barTerminalAddSelectUpdate
            // 
            this.barTerminalAddSelectUpdate.Caption = "Agregar a Actualización Selectiva";
            this.barTerminalAddSelectUpdate.Id = 44;
            this.barTerminalAddSelectUpdate.ImageIndex = 19;
            this.barTerminalAddSelectUpdate.Name = "barTerminalAddSelectUpdate";
            this.barTerminalAddSelectUpdate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barTerminalAddSelectUpdate_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Envin";
            this.barButtonItem1.Id = 61;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barViewPetVeh
            // 
            this.barViewPetVeh.Caption = "Mostrar Peticiones para el Vehiculo";
            this.barViewPetVeh.Id = 65;
            this.barViewPetVeh.Name = "barViewPetVeh";
            this.barViewPetVeh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barViewPetVeh_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Configuracion Terminal";
            this.barStaticItem1.Id = 67;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barSubItem8
            // 
            this.barSubItem8.Caption = "Localizadores";
            this.barSubItem8.Id = 73;
            this.barSubItem8.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barViewServVeh),
            new DevExpress.XtraBars.LinkPersistInfo(this.barViewPetVeh),
            new DevExpress.XtraBars.LinkPersistInfo(this.barViewSession),
            new DevExpress.XtraBars.LinkPersistInfo(this.barViewTerminalDataReceived)});
            this.barSubItem8.Name = "barSubItem8";
            // 
            // barViewSession
            // 
            this.barViewSession.Caption = "Mostrar Sesiones Conductor";
            this.barViewSession.Id = 74;
            this.barViewSession.Name = "barViewSession";
            // 
            // barViewTerminalDataReceived
            // 
            this.barViewTerminalDataReceived.Caption = "Mostrar Tramas Recibidas ";
            this.barViewTerminalDataReceived.Id = 75;
            this.barViewTerminalDataReceived.Name = "barViewTerminalDataReceived";
            this.barViewTerminalDataReceived.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barViewTerminalDataReceived_ItemClick);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // bsTerm
            // 
            this.bsTerm.DataSource = typeof(Core.DAL.GC.TerminalState);
            // 
            // dbgTerm
            // 
            this.dbgTerm.DataSource = this.bsTerm;
            this.dbgTerm.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.dbgTerm.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.dbgTerm.Location = new System.Drawing.Point(0, 84);
            this.dbgTerm.MainView = this.dbgTermView;
            this.dbgTerm.Name = "dbgTerm";
            this.dbgTerm.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cbrepImgVehEstado,
            this.cbrepImgCnxEstado,
            this.rpCbHasConfig,
            this.rpCbInSelUpdate,
            this.rpCbOnLine,
            this.rpCbWorkingNow});
            this.dbgTerm.Size = new System.Drawing.Size(1212, 580);
            this.dbgTerm.TabIndex = 4;
            this.dbgTerm.ToolTipController = this.ttc;
            this.dbgTerm.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dbgTermView});
            // 
            // dbgTermView
            // 
            this.dbgTermView.Appearance.FocusedRow.BackColor = System.Drawing.Color.DarkGray;
            this.dbgTermView.Appearance.FocusedRow.BorderColor = System.Drawing.Color.Gray;
            this.dbgTermView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.dbgTermView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.dbgTermView.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.dbgTermView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.dbgTermView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.DarkGray;
            this.dbgTermView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.dbgTermView.Appearance.SelectedRow.BackColor = System.Drawing.Color.Gainsboro;
            this.dbgTermView.Appearance.SelectedRow.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dbgTermView.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.dbgTermView.Appearance.SelectedRow.Options.UseBackColor = true;
            this.dbgTermView.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.dbgTermView.Appearance.SelectedRow.Options.UseForeColor = true;
            this.dbgTermView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.dbgTermBd5,
            this.dbgTermBd1,
            this.gridBand1,
            this.dbgTermBd2,
            this.dbgTermBd3});
            this.dbgTermView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colVC,
            this.colVM,
            this.colConductor,
            this.colNS,
            this.colVersion,
            colGFecha,
            colGLatitud,
            this.colGLongitud,
            this.colGFM,
            this.colGNSat,
            this.colGVelocidad,
            this.colOdometro,
            this.colConnectFrom,
            this.colDisconnectFrom,
            colLastSession,
            this.colDisconnectCount,
            this.colLastReception,
            this.colUltInfo,
            this.colPwState,
            this.colPwStateDesde,
            this.colTermEstado,
            this.colFDetenido,
            this.colMHistoricoDesde,
            colEstConexion,
            colLoginFrom,
            this.colLogoutFrom,
            this.GpsFixGeoTx,
            this.colEmpresa,
            colWorkingNow,
            colHasConfiguration,
            this.colTfoVeh,
            this.colInSelectiveUpdate,
            this.colOnLine,
            this.colOffLineCausa,
            this.colCnfgTemplateID,
            this.colEmpID,
            this.colGrupo,
            this.colVCAdm,
            this.colTipoActividad,
            this.colServerID,
            this.colIccid,
            this.colSoftwareTipoEnum});
            this.dbgTermView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colEmpID;
            styleFormatCondition1.Value1 = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dbgTermView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.dbgTermView.GridControl = this.dbgTerm;
            this.dbgTermView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "Grupo", null, "\" Nº {0} \"")});
            this.dbgTermView.Images = this.imageVehEstado;
            this.dbgTermView.Name = "dbgTermView";
            this.dbgTermView.OptionsBehavior.Editable = false;
            this.dbgTermView.OptionsCustomization.AllowChangeBandParent = true;
            this.dbgTermView.OptionsDetail.AllowZoomDetail = false;
            this.dbgTermView.OptionsDetail.EnableMasterViewMode = false;
            this.dbgTermView.OptionsDetail.ShowDetailTabs = false;
            this.dbgTermView.OptionsLayout.LayoutVersion = "3";
            this.dbgTermView.OptionsLayout.StoreAppearance = true;
            this.dbgTermView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.dbgTermView.OptionsSelection.MultiSelect = true;
            this.dbgTermView.OptionsView.AutoCalcPreviewLineCount = true;
            this.dbgTermView.OptionsView.ColumnAutoWidth = false;
            this.dbgTermView.OptionsView.HeaderFilterButtonShowMode = DevExpress.XtraEditors.Controls.FilterButtonShowMode.SmartTag;
            this.dbgTermView.OptionsView.ShowAutoFilterRow = true;
            this.dbgTermView.OptionsView.ShowDetailButtons = false;
            this.dbgTermView.OptionsView.ShowFooter = true;
            this.dbgTermView.OptionsView.ShowPreview = true;
            this.dbgTermView.PreviewFieldName = "Comentario";
            this.dbgTermView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.Default;
            this.dbgTermView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.dbgTermView_CustomDrawCell_1);
            this.dbgTermView.EndGrouping += new System.EventHandler(this.dbgTermView_EndGrouping);
            this.dbgTermView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.dbgTermView_CustomColumnDisplayText);
            this.dbgTermView.LayoutUpgrade += this.dbgTermView_LayoutUpgrade;
            this.dbgTermView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dbgTermView_MouseUp);
            // 
            // dbgTermBd5
            // 
            this.dbgTermBd5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.dbgTermBd5.AppearanceHeader.Options.UseFont = true;
            this.dbgTermBd5.AppearanceHeader.Options.UseTextOptions = true;
            this.dbgTermBd5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dbgTermBd5.Caption = "Estado";
            this.dbgTermBd5.Columns.Add(colEstConexion);
            this.dbgTermBd5.Columns.Add(colWorkingNow);
            this.dbgTermBd5.Columns.Add(this.colTermEstado);
            this.dbgTermBd5.Columns.Add(this.colPwState);
            this.dbgTermBd5.Columns.Add(this.colOnLine);
            this.dbgTermBd5.Columns.Add(this.colOffLineCausa);
            this.dbgTermBd5.Columns.Add(this.colPwStateDesde);
            this.dbgTermBd5.Columns.Add(this.colMHistoricoDesde);
            this.dbgTermBd5.MinWidth = 20;
            this.dbgTermBd5.Name = "dbgTermBd5";
            this.dbgTermBd5.Width = 617;
            // 
            // colTermEstado
            // 
            this.colTermEstado.AppearanceCell.Options.UseTextOptions = true;
            this.colTermEstado.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colTermEstado.Caption = " ";
            this.colTermEstado.ColumnEdit = this.cbrepImgVehEstado;
            this.colTermEstado.FieldName = "TerminalEstado";
            this.colTermEstado.ImageIndex = 0;
            this.colTermEstado.MinWidth = 40;
            this.colTermEstado.Name = "colTermEstado";
            this.colTermEstado.OptionsColumn.AllowSize = false;
            this.colTermEstado.OptionsColumn.FixedWidth = true;
            this.colTermEstado.OptionsColumn.ReadOnly = true;
            this.colTermEstado.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTermEstado.ToolTip = "Estado del Vehiculo";
            this.colTermEstado.Visible = true;
            this.colTermEstado.Width = 40;
            // 
            // cbrepImgVehEstado
            // 
            this.cbrepImgVehEstado.AutoHeight = false;
            this.cbrepImgVehEstado.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbrepImgVehEstado.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cbrepImgVehEstado.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inactivo", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Libre", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Retorno/Espera", 2, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Recogida", 3, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ocupado", 4, 4)});
            this.cbrepImgVehEstado.LargeImages = this.imageVehEstado;
            this.cbrepImgVehEstado.Name = "cbrepImgVehEstado";
            this.cbrepImgVehEstado.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.cbrepImgVehEstado.SmallImages = this.imageVehEstado;
            // 
            // colPwState
            // 
            this.colPwState.Caption = "Estado";
            this.colPwState.FieldName = "OperateStateEnum";
            this.colPwState.Name = "colPwState";
            this.colPwState.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPwState.ToolTip = "Estado del Dispositivo";
            this.colPwState.Visible = true;
            this.colPwState.Width = 80;
            // 
            // colOnLine
            // 
            this.colOnLine.Caption = " ";
            this.colOnLine.ColumnEdit = this.rpCbOnLine;
            this.colOnLine.FieldName = "OnLine";
            this.colOnLine.ImageIndex = 10;
            this.colOnLine.MinWidth = 40;
            this.colOnLine.Name = "colOnLine";
            this.colOnLine.OptionsColumn.AllowSize = false;
            this.colOnLine.OptionsColumn.FixedWidth = true;
            this.colOnLine.Visible = true;
            this.colOnLine.Width = 40;
            // 
            // rpCbOnLine
            // 
            this.rpCbOnLine.AutoHeight = false;
            this.rpCbOnLine.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpCbOnLine.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rpCbOnLine.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("OnLine", true, 10),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("OFF-Line", false, 11)});
            this.rpCbOnLine.Name = "rpCbOnLine";
            this.rpCbOnLine.SmallImages = this.imageVehEstado;
            // 
            // colOffLineCausa
            // 
            this.colOffLineCausa.Caption = "Causa";
            this.colOffLineCausa.FieldName = "OffLineCausa";
            this.colOffLineCausa.Name = "colOffLineCausa";
            this.colOffLineCausa.Visible = true;
            this.colOffLineCausa.Width = 169;
            // 
            // colPwStateDesde
            // 
            this.colPwStateDesde.Caption = "Desde ...";
            this.colPwStateDesde.DisplayFormat.FormatString = "dd-MM-yy HH:mm";
            this.colPwStateDesde.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPwStateDesde.FieldName = "PowerStateDesde";
            this.colPwStateDesde.Name = "colPwStateDesde";
            this.colPwStateDesde.Visible = true;
            this.colPwStateDesde.Width = 99;
            // 
            // colMHistoricoDesde
            // 
            this.colMHistoricoDesde.Caption = "M.Histor ...";
            this.colMHistoricoDesde.DisplayFormat.FormatString = "dd-MM-yy HH:mm:ss";
            this.colMHistoricoDesde.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colMHistoricoDesde.FieldName = "ModoHistoricoDesde";
            this.colMHistoricoDesde.Name = "colMHistoricoDesde";
            this.colMHistoricoDesde.Visible = true;
            this.colMHistoricoDesde.Width = 109;
            // 
            // dbgTermBd1
            // 
            this.dbgTermBd1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.dbgTermBd1.AppearanceHeader.Options.UseFont = true;
            this.dbgTermBd1.AppearanceHeader.Options.UseTextOptions = true;
            this.dbgTermBd1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dbgTermBd1.Caption = "Vehiculo / Terminal";
            this.dbgTermBd1.Columns.Add(this.colServerID);
            this.dbgTermBd1.Columns.Add(this.colInSelectiveUpdate);
            this.dbgTermBd1.Columns.Add(colHasConfiguration);
            this.dbgTermBd1.Columns.Add(this.colCnfgTemplateID);
            this.dbgTermBd1.Columns.Add(this.colEmpresa);
            this.dbgTermBd1.Columns.Add(this.colGrupo);
            this.dbgTermBd1.Columns.Add(this.colVC);
            this.dbgTermBd1.Columns.Add(this.colVCAdm);
            this.dbgTermBd1.Columns.Add(this.colVM);
            this.dbgTermBd1.Columns.Add(this.colTfoVeh);
            this.dbgTermBd1.Columns.Add(colLoginFrom);
            this.dbgTermBd1.Columns.Add(this.colTipoActividad);
            this.dbgTermBd1.Columns.Add(this.colConductor);
            this.dbgTermBd1.Columns.Add(this.colLogoutFrom);
            this.dbgTermBd1.Columns.Add(this.colNS);
            this.dbgTermBd1.Columns.Add(this.colSoftwareTipoEnum);
            this.dbgTermBd1.Columns.Add(this.colIccid);
            this.dbgTermBd1.Columns.Add(this.colVersion);
            this.dbgTermBd1.MinWidth = 20;
            this.dbgTermBd1.Name = "dbgTermBd1";
            this.dbgTermBd1.Width = 1610;
            // 
            // colServerID
            // 
            this.colServerID.Caption = "SRV";
            this.colServerID.FieldName = "ServerID";
            this.colServerID.MinWidth = 40;
            this.colServerID.Name = "colServerID";
            this.colServerID.OptionsColumn.AllowSize = false;
            this.colServerID.ToolTip = "Servidor que gestiona la conexión";
            this.colServerID.Visible = true;
            this.colServerID.Width = 40;
            // 
            // colInSelectiveUpdate
            // 
            this.colInSelectiveUpdate.AppearanceCell.Options.UseTextOptions = true;
            this.colInSelectiveUpdate.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colInSelectiveUpdate.ColumnEdit = this.rpCbInSelUpdate;
            this.colInSelectiveUpdate.FieldName = "InUpdateSelective";
            this.colInSelectiveUpdate.Name = "colInSelectiveUpdate";
            this.colInSelectiveUpdate.OptionsColumn.FixedWidth = true;
            this.colInSelectiveUpdate.OptionsColumn.ShowCaption = false;
            this.colInSelectiveUpdate.ToolTip = "Indica si el Terminal está incluido en la Actualización selectiva";
            this.colInSelectiveUpdate.Visible = true;
            this.colInSelectiveUpdate.Width = 28;
            // 
            // rpCbInSelUpdate
            // 
            this.rpCbInSelUpdate.AutoHeight = false;
            this.rpCbInSelUpdate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpCbInSelUpdate.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rpCbInSelUpdate.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 9)});
            this.rpCbInSelUpdate.Name = "rpCbInSelUpdate";
            this.rpCbInSelUpdate.SmallImages = this.imageVehEstado;
            // 
            // colCnfgTemplateID
            // 
            this.colCnfgTemplateID.Caption = " ";
            this.colCnfgTemplateID.FieldName = "TemplateConfigID";
            this.colCnfgTemplateID.Name = "colCnfgTemplateID";
            this.colCnfgTemplateID.ToolTip = "Identificador de la Plantilla de Configuración Asociada";
            this.colCnfgTemplateID.Visible = true;
            this.colCnfgTemplateID.Width = 31;
            // 
            // colGrupo
            // 
            this.colGrupo.AppearanceHeader.ForeColor = System.Drawing.Color.Blue;
            this.colGrupo.AppearanceHeader.Options.UseForeColor = true;
            this.colGrupo.Caption = "Grupo";
            this.colGrupo.FieldName = "GrupoNombre";
            this.colGrupo.Name = "colGrupo";
            this.colGrupo.Visible = true;
            this.colGrupo.Width = 173;
            // 
            // colVC
            // 
            this.colVC.Caption = "Codigo";
            this.colVC.FieldName = "VehiculoCodigo";
            this.colVC.Name = "colVC";
            this.colVC.Visible = true;
            this.colVC.Width = 77;
            // 
            // colVCAdm
            // 
            this.colVCAdm.Caption = "Cod.Admin";
            this.colVCAdm.FieldName = "VehiculoCodigoADM";
            this.colVCAdm.Name = "colVCAdm";
            this.colVCAdm.Visible = true;
            this.colVCAdm.Width = 84;
            // 
            // colVM
            // 
            this.colVM.Caption = "Matricula";
            this.colVM.FieldName = "VehiculoMatricula";
            this.colVM.Name = "colVM";
            this.colVM.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colVM.SummaryItem.DisplayFormat = "Nº: {0}";
            this.colVM.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.colVM.Visible = true;
            this.colVM.Width = 77;
            // 
            // colTfoVeh
            // 
            this.colTfoVeh.Caption = "Telefono/ext";
            this.colTfoVeh.FieldName = "VehiculoTelefono";
            this.colTfoVeh.Name = "colTfoVeh";
            this.colTfoVeh.Visible = true;
            this.colTfoVeh.Width = 104;
            // 
            // colTipoActividad
            // 
            this.colTipoActividad.Caption = "Actividad";
            this.colTipoActividad.FieldName = "TipoActividadNombre";
            this.colTipoActividad.Name = "colTipoActividad";
            this.colTipoActividad.Visible = true;
            this.colTipoActividad.Width = 84;
            // 
            // colConductor
            // 
            this.colConductor.Caption = "Conductor";
            this.colConductor.FieldName = "ConductorNombre";
            this.colConductor.Name = "colConductor";
            this.colConductor.Visible = true;
            this.colConductor.Width = 133;
            // 
            // colNS
            // 
            this.colNS.Caption = "Nº Serie";
            this.colNS.FieldName = "TerminalNS";
            this.colNS.Name = "colNS";
            this.colNS.Visible = true;
            this.colNS.Width = 99;
            // 
            // colSoftwareTipoEnum
            // 
            this.colSoftwareTipoEnum.AppearanceCell.Options.UseTextOptions = true;
            this.colSoftwareTipoEnum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colSoftwareTipoEnum.Caption = "Progr.Tipo";
            this.colSoftwareTipoEnum.FieldName = "SoftwareTipoEnum";
            this.colSoftwareTipoEnum.Name = "colSoftwareTipoEnum";
            this.colSoftwareTipoEnum.ToolTip = "Tipo de programa que corre sobre el Terminal";
            this.colSoftwareTipoEnum.Visible = true;
            this.colSoftwareTipoEnum.Width = 77;
            // 
            // colIccid
            // 
            this.colIccid.Caption = "Nº Tarjeta";
            this.colIccid.FieldName = "Iccid";
            this.colIccid.Name = "colIccid";
            this.colIccid.Visible = true;
            this.colIccid.Width = 150;
            // 
            // colVersion
            // 
            this.colVersion.Caption = "Version";
            this.colVersion.FieldName = "TerminalVersionSW";
            this.colVersion.Name = "colVersion";
            this.colVersion.Visible = true;
            this.colVersion.Width = 77;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Fechas";
            this.gridBand1.Columns.Add(colGFecha);
            this.gridBand1.Columns.Add(this.colUltInfo);
            this.gridBand1.Columns.Add(this.colLastReception);
            this.gridBand1.Columns.Add(this.colFDetenido);
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 421;
            // 
            // colUltInfo
            // 
            this.colUltInfo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colUltInfo.AppearanceCell.Options.UseFont = true;
            this.colUltInfo.Caption = "Ult.Informacion";
            this.colUltInfo.DisplayFormat.FormatString = "dd-MM-yy HH:mm:ss";
            this.colUltInfo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colUltInfo.FieldName = "FechaTerminalInfoMasActual";
            this.colUltInfo.Name = "colUltInfo";
            this.colUltInfo.ToolTip = "Fecha de la última trama de información recibida";
            this.colUltInfo.Visible = true;
            this.colUltInfo.Width = 105;
            // 
            // colLastReception
            // 
            this.colLastReception.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colLastReception.AppearanceCell.Options.UseFont = true;
            this.colLastReception.Caption = "Ult.Recepcion";
            this.colLastReception.DisplayFormat.FormatString = "dd-MM-yy HH:mm:ss";
            this.colLastReception.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colLastReception.FieldName = "TerminalLastReception";
            this.colLastReception.Name = "colLastReception";
            this.colLastReception.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colLastReception.Visible = true;
            this.colLastReception.Width = 111;
            // 
            // colFDetenido
            // 
            this.colFDetenido.Caption = "Parado ...";
            this.colFDetenido.DisplayFormat.FormatString = "dd-MM-yy HH:mm:ss";
            this.colFDetenido.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colFDetenido.FieldName = "GpsFechaDetenido";
            this.colFDetenido.Name = "colFDetenido";
            this.colFDetenido.Visible = true;
            this.colFDetenido.Width = 99;
            // 
            // dbgTermBd2
            // 
            this.dbgTermBd2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.dbgTermBd2.AppearanceHeader.Options.UseFont = true;
            this.dbgTermBd2.Caption = "Información GPS";
            this.dbgTermBd2.Columns.Add(colGLatitud);
            this.dbgTermBd2.Columns.Add(this.colGLongitud);
            this.dbgTermBd2.Columns.Add(this.GpsFixGeoTx);
            this.dbgTermBd2.Columns.Add(this.colGNSat);
            this.dbgTermBd2.Columns.Add(this.colGFM);
            this.dbgTermBd2.Columns.Add(this.colOdometro);
            this.dbgTermBd2.Columns.Add(this.colGVelocidad);
            this.dbgTermBd2.MinWidth = 20;
            this.dbgTermBd2.Name = "dbgTermBd2";
            this.dbgTermBd2.Width = 596;
            // 
            // colGLongitud
            // 
            this.colGLongitud.Caption = "Longitud";
            this.colGLongitud.FieldName = "GpsLongitud";
            this.colGLongitud.Name = "colGLongitud";
            this.colGLongitud.Visible = true;
            this.colGLongitud.Width = 70;
            // 
            // GpsFixGeoTx
            // 
            this.GpsFixGeoTx.Caption = "Lugar";
            this.GpsFixGeoTx.FieldName = "GpsFixGeoTx";
            this.GpsFixGeoTx.Name = "GpsFixGeoTx";
            this.GpsFixGeoTx.ToolTip = "Texto descriptivo de la ubicación del Vehiculo segun la informacion gps.";
            this.GpsFixGeoTx.Visible = true;
            this.GpsFixGeoTx.Width = 234;
            // 
            // colGNSat
            // 
            this.colGNSat.Caption = "NºSat";
            this.colGNSat.FieldName = "GpsNSAT";
            this.colGNSat.Name = "colGNSat";
            this.colGNSat.Visible = true;
            this.colGNSat.Width = 44;
            // 
            // colGFM
            // 
            this.colGFM.Caption = "Calidad";
            this.colGFM.FieldName = "GpsFixModeEnum";
            this.colGFM.Name = "colGFM";
            this.colGFM.Visible = true;
            this.colGFM.Width = 56;
            // 
            // colOdometro
            // 
            this.colOdometro.Caption = "Odometro";
            this.colOdometro.FieldName = "OdometroTt";
            this.colOdometro.Name = "colOdometro";
            this.colOdometro.Visible = true;
            this.colOdometro.Width = 77;
            // 
            // colGVelocidad
            // 
            this.colGVelocidad.Caption = "Velocidad";
            this.colGVelocidad.FieldName = "GpsVelocidad";
            this.colGVelocidad.Name = "colGVelocidad";
            this.colGVelocidad.Visible = true;
            this.colGVelocidad.Width = 52;
            // 
            // dbgTermBd3
            // 
            this.dbgTermBd3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.dbgTermBd3.AppearanceHeader.Options.UseFont = true;
            this.dbgTermBd3.Caption = "Logistica";
            this.dbgTermBd3.Columns.Add(this.colConnectFrom);
            this.dbgTermBd3.Columns.Add(this.colDisconnectFrom);
            this.dbgTermBd3.Columns.Add(this.colDisconnectCount);
            this.dbgTermBd3.Columns.Add(colLastSession);
            this.dbgTermBd3.MinWidth = 20;
            this.dbgTermBd3.Name = "dbgTermBd3";
            this.dbgTermBd3.Width = 306;
            // 
            // colConnectFrom
            // 
            this.colConnectFrom.Caption = "Conectado ...";
            this.colConnectFrom.DisplayFormat.FormatString = "dd-MM-yy HH:mm:ss";
            this.colConnectFrom.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colConnectFrom.FieldName = "TerminalConnectedFrom";
            this.colConnectFrom.Name = "colConnectFrom";
            this.colConnectFrom.Visible = true;
            this.colConnectFrom.Width = 97;
            // 
            // colDisconnectFrom
            // 
            this.colDisconnectFrom.Caption = "Desconectado ...";
            this.colDisconnectFrom.DisplayFormat.FormatString = "dd-MM-yy HH:mm:ss";
            this.colDisconnectFrom.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDisconnectFrom.FieldName = "TerminalDisconnect";
            this.colDisconnectFrom.Name = "colDisconnectFrom";
            this.colDisconnectFrom.Visible = true;
            this.colDisconnectFrom.Width = 95;
            // 
            // colDisconnectCount
            // 
            this.colDisconnectCount.Caption = "Nº Descon.";
            this.colDisconnectCount.DisplayFormat.FormatString = "d0";
            this.colDisconnectCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDisconnectCount.FieldName = "TerminalDisconnectCount";
            this.colDisconnectCount.Name = "colDisconnectCount";
            this.colDisconnectCount.ToolTip = "Nº de Desconexiones";
            this.colDisconnectCount.Visible = true;
            this.colDisconnectCount.Width = 66;
            // 
            // ttc
            // 
            this.ttc.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.ttc_GetActiveObjectInfo);
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "iMaginary";
            // 
            // imageMini
            // 
            this.imageMini.ImageSize = new System.Drawing.Size(32, 32);
            this.imageMini.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageMini.ImageStream")));
            this.imageMini.Images.SetKeyName(0, "Select.ico");
            this.imageMini.Images.SetKeyName(1, "UnSelect.ico");
            // 
            // popupGridOptions
            // 
            this.popupGridOptions.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem8, true),
            new DevExpress.XtraBars.LinkPersistInfo(barSubItem3),
            new DevExpress.XtraBars.LinkPersistInfo(barSubItem4),
            new DevExpress.XtraBars.LinkPersistInfo(barSubItem6),
            new DevExpress.XtraBars.LinkPersistInfo(barSubItem5),
            new DevExpress.XtraBars.LinkPersistInfo(barSubItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.barAccConnectSOS, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barTerminalAddSelectUpdate)});
            this.popupGridOptions.Manager = this.barManager1;
            this.popupGridOptions.Name = "popupGridOptions";
            // 
            // Mainfm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1212, 711);
            this.ControlBox = false;
            this.Controls.Add(this.dbgTerm);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Mainfm";
            this.Text = "Consola Gestión";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mainfm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.cbrepImgCnxEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageVehEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpCbHasConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpCbWorkingNow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCol32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgTerm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgTermView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbrepImgVehEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpCbOnLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpCbInSelUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageMini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupGridOptions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barControlService;
        private DevExpress.Utils.ImageCollection imageCol32;
        private DevExpress.XtraBars.BarButtonItem barControlListen;
        private DevExpress.XtraBars.BarButtonItem barOption;
        private DevExpress.XtraBars.BarEditItem barFootProgress;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar repositoryItemMarqueeProgressBar1;
        private DevExpress.XtraBars.BarStaticItem barFootInfo;
        private DevExpress.XtraBars.BarButtonItem barRefreshoOption;
        private System.Windows.Forms.BindingSource bsLog;
        private System.Windows.Forms.BindingSource bsTerm;
        private DevExpress.Utils.ImageCollection imageVehEstado;
        private DevExpress.XtraBars.BarButtonItem barRefreshData;
        private DevExpress.XtraGrid.GridControl dbgTerm;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView dbgTermView;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox cbrepImgCnxEstado;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTermEstado;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox cbrepImgVehEstado;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPwState;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPwStateDesde;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMHistoricoDesde;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVC;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colConductor;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNS;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVersion;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUltInfo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLastReception;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFDetenido;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGLongitud;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGNSat;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGFM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOdometro;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGVelocidad;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colConnectFrom;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDisconnectFrom;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDisconnectCount;
        private DevExpress.XtraBars.BarStaticItem barFootInfoCnx;
        private DevExpress.XtraBars.BarStaticItem barFootInfoDCnx;
        private DevExpress.XtraBars.BarStaticItem barFootInfoNum;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarStaticItem barFootRed;
        private DevExpress.XtraBars.BarStaticItem barFootGreen;
        private DevExpress.XtraBars.BarStaticItem barFootYellow;
        private DevExpress.XtraBars.BarStaticItem barFootBlue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.BarStaticItem barFootOrange;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem btAccSendAllLI;
        private DevExpress.XtraBars.BarButtonItem btAccSendConfiguracion;
        private DevExpress.XtraBars.BarButtonItem btAccGetConfig;
        private DevExpress.XtraBars.Bar barUtil;
        private DevExpress.XtraBars.BarButtonItem barSelectAll;
        private DevExpress.XtraBars.BarButtonItem barUnSelect;
        private DevExpress.Utils.ImageCollection imageMini;
        private DevExpress.XtraBars.BarButtonItem barTerminalConfigView;
        private DevExpress.XtraBars.BarButtonItem barTerminalConfigEdit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn GpsFixGeoTx;
        private DevExpress.XtraBars.BarButtonItem barTerminalHConex;
        private DevExpress.XtraBars.BarButtonItem barTerminalHIncid;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLogoutFrom;
        private DevExpress.XtraBars.BarButtonItem barGrupos;
        private DevExpress.XtraBars.BarButtonItem barAssignToGrupo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rpCbHasConfig;
        private DevExpress.XtraBars.BarCheckItem barUnLockAdmin;
        private DevExpress.XtraBars.BarButtonItem btAccDiscconnect;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTfoVeh;
        private DevExpress.XtraBars.BarButtonItem barAsignarComment;
        private DevExpress.XtraBars.BarButtonItem btAccTelefonos;
        private DevExpress.XtraBars.BarButtonItem barAccTelefono;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEmpresa;
        private DevExpress.XtraBars.BarSubItem barOptionCountFooter;
        private DevExpress.XtraBars.BarButtonItem barOptionCountFooter_ShowAll;
        private DevExpress.XtraBars.BarButtonItem barOptionCountFooter_ShowOnlyActive;
        private DevExpress.XtraBars.BarButtonItem barSelectiveUpdateLista;
        private DevExpress.XtraBars.BarButtonItem barTerminalAddSelectUpdate;
        private DevExpress.XtraBars.PopupMenu popupGridOptions;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colInSelectiveUpdate;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rpCbInSelUpdate;
        private DevExpress.XtraBars.BarButtonItem barAssignEstados;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem barCausaOffline;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOnLine;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rpCbOnLine;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOffLineCausa;
        private DevExpress.XtraBars.BarButtonItem barSetOFFLine;
        private DevExpress.XtraBars.BarSubItem barFilters;
        private DevExpress.XtraBars.BarButtonItem barFiltersAdd;
        private DevExpress.XtraBars.BarButtonItem barFiltersLista;
        private DevExpress.XtraBars.BarButtonItem barAccConnectSOS;
        private DevExpress.XtraBars.BarButtonItem barPositionMap;
        private DevExpress.XtraBars.BarButtonItem barAccConnectSOS2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rpCbWorkingNow;
        private DevExpress.XtraBars.BarButtonItem barUpdateStateWorkingNow;
        private DevExpress.Utils.ToolTipController ttc;
        private DevExpress.XtraBars.BarButtonItem barOptionCountFooter_ShowOnlyWorking;
        private DevExpress.XtraBars.BarButtonItem barViewServVeh;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barTemplateConfig;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCnfgTemplateID;
        private DevExpress.XtraBars.BarButtonItem btAccApplyTemplConfig;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEmpID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colGrupo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colVCAdm;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colServerID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTipoActividad;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIccid;
        private DevExpress.XtraBars.BarButtonItem barTiposActividad;
        private DevExpress.XtraBars.BarButtonItem barViewPetVeh;
        private DevExpress.XtraBars.BarButtonItem barTaskResendMntos;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem8;
        private DevExpress.XtraBars.BarButtonItem barViewSession;
        private DevExpress.XtraBars.BarButtonItem barViewTerminalDataReceived;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand dbgTermBd5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand dbgTermBd1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSoftwareTipoEnum;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand dbgTermBd2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand dbgTermBd3;
    }
}