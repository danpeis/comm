using System.IO.IsolatedStorage;
using gcperu.Server.Core.Info;

namespace gcperu.Server.UI
{
    partial class OptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.LabelControl labelControl23;
            DevExpress.XtraEditors.LabelControl labelControl24;
            this.cmdCkExpand = new DevExpress.XtraEditors.CheckButton();
            this.cmdThrowIncid = new DevExpress.XtraEditors.SimpleButton();
            this.txtMaxClientes = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cmdAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.frEMail = new DevExpress.XtraEditors.GroupControl();
            this.txtEmailFrom = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtEMails = new DevExpress.XtraEditors.MemoEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.frmEmailUser = new DevExpress.XtraEditors.PanelControl();
            this.txtEMail_Pwd = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtEMail_User = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.ckEMail_Requiere = new DevExpress.XtraEditors.CheckEdit();
            this.txtEMail_SMTP = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtServerPort = new DevExpress.XtraEditors.TextEdit();
            this.txtUrlEndPoint = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.TrackDiasPosDiario = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.TrackIntervalVehWorkingNow = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.TrackIntervalStoreProcess = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.TrackIntervalReSend = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.TrackIntervalSend = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.LabelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.cbLevelLog = new DevExpress.XtraEditors.ComboBoxEdit();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.txtEmpresaNombre = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.tbAdvancedOptions = new DevExpress.XtraTab.XtraTabControl();
            this.tpSpecialServices = new DevExpress.XtraTab.XtraTabPage();
            this.cbComp = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.cbSolic = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.cbPetc = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.cbUser = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.tpGen = new DevExpress.XtraTab.XtraTabPage();
            this.opUpdateMode = new DevExpress.XtraEditors.RadioGroup();
            this.TrackIntervalPrxServ = new gcperu.Server.UI.Controls.MyTrackBarControl();
            this.ckIntegraSystem = new DevExpress.XtraEditors.CheckEdit();
            this.tpTerminalUpdates = new DevExpress.XtraTab.XtraTabPage();
            this.frUpdateMode = new DevExpress.XtraEditors.GroupControl();
            this.dbgC = new DevExpress.XtraGrid.GridControl();
            this.bsTerminalUpdate = new System.Windows.Forms.BindingSource(this.components);
            this.dgbV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colUpdateMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rptcbUpdateMode = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colSoftwareTipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rptcbSoftwareTipo = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colVersionRequerida = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rptcbState = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtBDVersion = new DevExpress.XtraEditors.TextEdit();
            labelControl23 = new DevExpress.XtraEditors.LabelControl();
            labelControl24 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaxClientes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frEMail)).BeginInit();
            this.frEMail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMails.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmEmailUser)).BeginInit();
            this.frmEmailUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail_Pwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail_User.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEMail_Requiere.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail_SMTP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUrlEndPoint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackDiasPosDiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackDiasPosDiario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalVehWorkingNow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalVehWorkingNow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalStoreProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalStoreProcess.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalReSend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalReSend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalSend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalSend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbLevelLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpresaNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAdvancedOptions)).BeginInit();
            this.tbAdvancedOptions.SuspendLayout();
            this.tpSpecialServices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbComp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSolic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPetc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbUser.Properties)).BeginInit();
            this.tpGen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.opUpdateMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalPrxServ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalPrxServ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckIntegraSystem.Properties)).BeginInit();
            this.tpTerminalUpdates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frUpdateMode)).BeginInit();
            this.frUpdateMode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dbgC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerminalUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgbV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptcbUpdateMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptcbSoftwareTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptcbState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBDVersion.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl23
            // 
            labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl23.Appearance.Options.UseFont = true;
            labelControl23.Location = new System.Drawing.Point(349, 172);
            labelControl23.Name = "labelControl23";
            labelControl23.Size = new System.Drawing.Size(187, 16);
            labelControl23.TabIndex = 33;
            labelControl23.Text = "\'Calcular Proximo Servicio\' cada:";
            // 
            // labelControl24
            // 
            labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelControl24.Appearance.Options.UseFont = true;
            labelControl24.Location = new System.Drawing.Point(19, 368);
            labelControl24.Name = "labelControl24";
            labelControl24.Size = new System.Drawing.Size(233, 16);
            labelControl24.TabIndex = 35;
            labelControl24.Text = "Modo de Actualizaci�n de los Terminales";
            // 
            // cmdCkExpand
            // 
            this.cmdCkExpand.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.cmdCkExpand.Location = new System.Drawing.Point(224, 179);
            this.cmdCkExpand.Name = "cmdCkExpand";
            this.cmdCkExpand.Size = new System.Drawing.Size(143, 23);
            this.cmdCkExpand.TabIndex = 24;
            this.cmdCkExpand.Text = "Opciones Avanzadas";
            this.cmdCkExpand.CheckedChanged += new System.EventHandler(this.cmdCkExpand_CheckedChanged);
            // 
            // cmdThrowIncid
            // 
            this.cmdThrowIncid.Location = new System.Drawing.Point(12, 179);
            this.cmdThrowIncid.Name = "cmdThrowIncid";
            this.cmdThrowIncid.Size = new System.Drawing.Size(175, 23);
            this.cmdThrowIncid.TabIndex = 23;
            this.cmdThrowIncid.Text = "Emitir Incidencias por ...";
            this.cmdThrowIncid.Click += new System.EventHandler(this.cmdThrowIncid_Click);
            // 
            // txtMaxClientes
            // 
            this.txtMaxClientes.EditValue = "";
            this.txtMaxClientes.EnterMoveNextControl = true;
            this.txtMaxClientes.Location = new System.Drawing.Point(251, 60);
            this.txtMaxClientes.Name = "txtMaxClientes";
            this.txtMaxClientes.Size = new System.Drawing.Size(64, 20);
            this.txtMaxClientes.TabIndex = 22;
            // 
            // LabelControl7
            // 
            this.LabelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl7.Appearance.Options.UseFont = true;
            this.LabelControl7.Location = new System.Drawing.Point(34, 61);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(183, 16);
            this.LabelControl7.TabIndex = 21;
            this.LabelControl7.Text = "Maximos Conexiones Permitidas";
            // 
            // cmdAceptar
            // 
            this.cmdAceptar.Location = new System.Drawing.Point(240, 545);
            this.cmdAceptar.Name = "cmdAceptar";
            this.cmdAceptar.Size = new System.Drawing.Size(127, 34);
            this.cmdAceptar.TabIndex = 20;
            this.cmdAceptar.Text = "Aceptar";
            this.cmdAceptar.ToolTip = "Guarda la configuraci�n";
            this.cmdAceptar.Click += new System.EventHandler(this.cmdAceptar_Click);
            // 
            // frEMail
            // 
            this.frEMail.Controls.Add(this.txtEmailFrom);
            this.frEMail.Controls.Add(this.labelControl8);
            this.frEMail.Controls.Add(this.txtEMails);
            this.frEMail.Controls.Add(this.LabelControl3);
            this.frEMail.Controls.Add(this.frmEmailUser);
            this.frEMail.Controls.Add(this.ckEMail_Requiere);
            this.frEMail.Controls.Add(this.txtEMail_SMTP);
            this.frEMail.Controls.Add(this.LabelControl4);
            this.frEMail.Location = new System.Drawing.Point(12, 211);
            this.frEMail.Name = "frEMail";
            this.frEMail.Size = new System.Drawing.Size(355, 328);
            this.frEMail.TabIndex = 19;
            this.frEMail.Text = "Configuraci�n E-mail envio incidencias";
            // 
            // txtEmailFrom
            // 
            this.txtEmailFrom.EditValue = "";
            this.txtEmailFrom.EnterMoveNextControl = true;
            this.txtEmailFrom.Location = new System.Drawing.Point(9, 155);
            this.txtEmailFrom.Name = "txtEmailFrom";
            this.txtEmailFrom.Size = new System.Drawing.Size(209, 20);
            this.txtEmailFrom.TabIndex = 9;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(9, 133);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(107, 16);
            this.labelControl8.TabIndex = 10;
            this.labelControl8.Text = "Email cuenta envio";
            // 
            // txtEMails
            // 
            this.txtEMails.EnterMoveNextControl = true;
            this.txtEMails.Location = new System.Drawing.Point(9, 47);
            this.txtEMails.Name = "txtEMails";
            this.txtEMails.Size = new System.Drawing.Size(329, 85);
            this.txtEMails.TabIndex = 2;
            this.txtEMails.ToolTip = "Introduca los E-mail de cuantos destinatarios desea que reciban el mensaje cuando" +
    " ocurra una incidencia";
            this.txtEMails.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl3.Appearance.Options.UseFont = true;
            this.LabelControl3.Location = new System.Drawing.Point(9, 25);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(165, 16);
            this.LabelControl3.TabIndex = 8;
            this.LabelControl3.Text = "E-mail gesti�n de incidencias";
            // 
            // frmEmailUser
            // 
            this.frmEmailUser.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.frmEmailUser.Controls.Add(this.txtEMail_Pwd);
            this.frmEmailUser.Controls.Add(this.LabelControl6);
            this.frmEmailUser.Controls.Add(this.txtEMail_User);
            this.frmEmailUser.Controls.Add(this.LabelControl5);
            this.frmEmailUser.Location = new System.Drawing.Point(9, 251);
            this.frmEmailUser.Name = "frmEmailUser";
            this.frmEmailUser.Size = new System.Drawing.Size(329, 63);
            this.frmEmailUser.TabIndex = 7;
            this.frmEmailUser.Visible = false;
            // 
            // txtEMail_Pwd
            // 
            this.txtEMail_Pwd.EditValue = "";
            this.txtEMail_Pwd.EnterMoveNextControl = true;
            this.txtEMail_Pwd.Location = new System.Drawing.Point(77, 35);
            this.txtEMail_Pwd.Name = "txtEMail_Pwd";
            this.txtEMail_Pwd.Properties.PasswordChar = '*';
            this.txtEMail_Pwd.Size = new System.Drawing.Size(152, 20);
            this.txtEMail_Pwd.TabIndex = 6;
            // 
            // LabelControl6
            // 
            this.LabelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl6.Appearance.Options.UseFont = true;
            this.LabelControl6.Location = new System.Drawing.Point(7, 38);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(55, 16);
            this.LabelControl6.TabIndex = 3;
            this.LabelControl6.Text = "Password";
            // 
            // txtEMail_User
            // 
            this.txtEMail_User.EditValue = "";
            this.txtEMail_User.EnterMoveNextControl = true;
            this.txtEMail_User.Location = new System.Drawing.Point(77, 11);
            this.txtEMail_User.Name = "txtEMail_User";
            this.txtEMail_User.Size = new System.Drawing.Size(206, 20);
            this.txtEMail_User.TabIndex = 5;
            // 
            // LabelControl5
            // 
            this.LabelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl5.Appearance.Options.UseFont = true;
            this.LabelControl5.Location = new System.Drawing.Point(7, 12);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(43, 16);
            this.LabelControl5.TabIndex = 1;
            this.LabelControl5.Text = "Usuario";
            // 
            // ckEMail_Requiere
            // 
            this.ckEMail_Requiere.Location = new System.Drawing.Point(7, 226);
            this.ckEMail_Requiere.Name = "ckEMail_Requiere";
            this.ckEMail_Requiere.Properties.Caption = "Requiere autentificaci�n";
            this.ckEMail_Requiere.Size = new System.Drawing.Size(211, 19);
            this.ckEMail_Requiere.TabIndex = 4;
            this.ckEMail_Requiere.CheckedChanged += new System.EventHandler(this.ckEMail_Requiere_CheckedChanged);
            // 
            // txtEMail_SMTP
            // 
            this.txtEMail_SMTP.EditValue = "";
            this.txtEMail_SMTP.EnterMoveNextControl = true;
            this.txtEMail_SMTP.Location = new System.Drawing.Point(9, 200);
            this.txtEMail_SMTP.Name = "txtEMail_SMTP";
            this.txtEMail_SMTP.Size = new System.Drawing.Size(209, 20);
            this.txtEMail_SMTP.TabIndex = 3;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl4.Appearance.Options.UseFont = true;
            this.LabelControl4.Location = new System.Drawing.Point(9, 178);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(205, 16);
            this.LabelControl4.TabIndex = 4;
            this.LabelControl4.Text = "Servidor para enviar correo (SMTP)";
            // 
            // txtServerPort
            // 
            this.txtServerPort.EnterMoveNextControl = true;
            this.txtServerPort.Location = new System.Drawing.Point(251, 34);
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.Size = new System.Drawing.Size(64, 20);
            this.txtServerPort.TabIndex = 18;
            // 
            // txtServerIP
            // 
            this.txtUrlEndPoint.EditValue = "";
            this.txtUrlEndPoint.EnterMoveNextControl = true;
            this.txtUrlEndPoint.Location = new System.Drawing.Point(11, 34);
            this.txtUrlEndPoint.Name = "txtUrlEndPoint";
            this.txtUrlEndPoint.Size = new System.Drawing.Size(226, 20);
            this.txtUrlEndPoint.TabIndex = 15;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl2.Appearance.Options.UseFont = true;
            this.LabelControl2.Location = new System.Drawing.Point(251, 12);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(37, 16);
            this.LabelControl2.TabIndex = 16;
            this.LabelControl2.Text = "Puerto";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl1.Appearance.Options.UseFont = true;
            this.LabelControl1.Location = new System.Drawing.Point(11, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(116, 16);
            this.LabelControl1.TabIndex = 17;
            this.LabelControl1.Text = "Interfaz IP Eschucha";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(352, 109);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(218, 16);
            this.labelControl15.TabIndex = 30;
            this.labelControl15.Text = "N� Dias Mantener Posiciones en Diario";
            // 
            // TrackDiasPosDiario
            // 
            this.TrackDiasPosDiario.EditValue = 5;
            this.TrackDiasPosDiario.Location = new System.Drawing.Point(349, 124);
            this.TrackDiasPosDiario.Name = "TrackDiasPosDiario";
            this.TrackDiasPosDiario.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TrackDiasPosDiario.Properties.Appearance.Options.UseForeColor = true;
            this.TrackDiasPosDiario.Properties.Maximum = 15;
            this.TrackDiasPosDiario.Properties.Minimum = 1;
            this.TrackDiasPosDiario.Size = new System.Drawing.Size(231, 45);
            this.TrackDiasPosDiario.TabIndex = 29;
            this.TrackDiasPosDiario.ToolTipTitle = "Intervalo de Tiempo en minutos, para volver a reenviar una petici�n a un Terminal" +
    ", que no ha sido confirmada y por tanto eliminada";
            this.TrackDiasPosDiario.Value = 5;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(22, 302);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(300, 16);
            this.labelControl14.TabIndex = 28;
            this.labelControl14.Text = "Intervalo Obtener Vehiculos ahora Trabajando (min)";
            // 
            // TrackIntervalVehWorkingNow
            // 
            this.TrackIntervalVehWorkingNow.EditValue = 15;
            this.TrackIntervalVehWorkingNow.Location = new System.Drawing.Point(19, 317);
            this.TrackIntervalVehWorkingNow.Name = "TrackIntervalVehWorkingNow";
            this.TrackIntervalVehWorkingNow.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TrackIntervalVehWorkingNow.Properties.Appearance.Options.UseForeColor = true;
            this.TrackIntervalVehWorkingNow.Properties.Maximum = 30;
            this.TrackIntervalVehWorkingNow.Size = new System.Drawing.Size(315, 45);
            this.TrackIntervalVehWorkingNow.TabIndex = 27;
            this.TrackIntervalVehWorkingNow.ToolTipTitle = "Intervalo de Tiempo en minutos, para volver a reenviar una petici�n a un Terminal" +
    ", que no ha sido confirmada y por tanto eliminada";
            this.TrackIntervalVehWorkingNow.Value = 15;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Appearance.Options.UseForeColor = true;
            this.labelControl22.Location = new System.Drawing.Point(340, 285);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(261, 13);
            this.labelControl22.TabIndex = 28;
            this.labelControl22.Text = "Modo Selectiva. Version Requerida Terminales";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(340, 259);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(268, 14);
            this.labelControl13.TabIndex = 25;
            this.labelControl13.Text = "Modo Normal. Version Requerida Terminales";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(22, 235);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(298, 16);
            this.labelControl11.TabIndex = 23;
            this.labelControl11.Text = "Intervalo Procesar Informaci�n Recibida (Segundos)";
            // 
            // TrackIntervalStoreProcess
            // 
            this.TrackIntervalStoreProcess.EditValue = 15;
            this.TrackIntervalStoreProcess.Location = new System.Drawing.Point(19, 250);
            this.TrackIntervalStoreProcess.Name = "TrackIntervalStoreProcess";
            this.TrackIntervalStoreProcess.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TrackIntervalStoreProcess.Properties.Appearance.Options.UseForeColor = true;
            this.TrackIntervalStoreProcess.Properties.Maximum = 30;
            this.TrackIntervalStoreProcess.Properties.Minimum = 10;
            this.TrackIntervalStoreProcess.Size = new System.Drawing.Size(315, 45);
            this.TrackIntervalStoreProcess.TabIndex = 22;
            this.TrackIntervalStoreProcess.ToolTipTitle = "Intervalo de Tiempo en minutos, para volver a reenviar una petici�n a un Terminal" +
    ", que no ha sido confirmada y por tanto eliminada";
            this.TrackIntervalStoreProcess.Value = 15;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(22, 172);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(292, 16);
            this.labelControl10.TabIndex = 21;
            this.labelControl10.Text = "Intervalo ReEnvio Peticiones no Confirmadas (Min.)";
            // 
            // TrackIntervalReSend
            // 
            this.TrackIntervalReSend.EditValue = 5;
            this.TrackIntervalReSend.Location = new System.Drawing.Point(19, 187);
            this.TrackIntervalReSend.Name = "TrackIntervalReSend";
            this.TrackIntervalReSend.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TrackIntervalReSend.Properties.Appearance.Options.UseForeColor = true;
            this.TrackIntervalReSend.Properties.Maximum = 20;
            this.TrackIntervalReSend.Properties.Minimum = 2;
            this.TrackIntervalReSend.Size = new System.Drawing.Size(315, 45);
            this.TrackIntervalReSend.TabIndex = 20;
            this.TrackIntervalReSend.ToolTipTitle = "Intervalo de Tiempo en minutos, para volver a reenviar una petici�n a un Terminal" +
    ", que no ha sido confirmada y por tanto eliminada";
            this.TrackIntervalReSend.Value = 5;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(19, 109);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(292, 16);
            this.labelControl9.TabIndex = 19;
            this.labelControl9.Text = "Intervalor Procesar Envio a Terminales (Segundos)";
            // 
            // TrackIntervalSend
            // 
            this.TrackIntervalSend.EditValue = 15;
            this.TrackIntervalSend.Location = new System.Drawing.Point(16, 124);
            this.TrackIntervalSend.Name = "TrackIntervalSend";
            this.TrackIntervalSend.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TrackIntervalSend.Properties.Appearance.Options.UseForeColor = true;
            this.TrackIntervalSend.Properties.Maximum = 45;
            this.TrackIntervalSend.Properties.Minimum = 10;
            this.TrackIntervalSend.Size = new System.Drawing.Size(315, 45);
            this.TrackIntervalSend.TabIndex = 18;
            this.TrackIntervalSend.Value = 15;
            // 
            // LabelControl12
            // 
            this.LabelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl12.Appearance.Options.UseFont = true;
            this.LabelControl12.Location = new System.Drawing.Point(16, 57);
            this.LabelControl12.Name = "LabelControl12";
            this.LabelControl12.Size = new System.Drawing.Size(69, 16);
            this.LabelControl12.TabIndex = 17;
            this.LabelControl12.Text = "Nivel de Log";
            // 
            // cbLevelLog
            // 
            this.cbLevelLog.EnterMoveNextControl = true;
            this.cbLevelLog.Location = new System.Drawing.Point(13, 79);
            this.cbLevelLog.Name = "cbLevelLog";
            this.cbLevelLog.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cbLevelLog.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbLevelLog.Size = new System.Drawing.Size(315, 20);
            this.cbLevelLog.TabIndex = 16;
            // 
            // txtEmpresaNombre
            // 
            this.txtEmpresaNombre.EditValue = "";
            this.txtEmpresaNombre.EnterMoveNextControl = true;
            this.txtEmpresaNombre.Location = new System.Drawing.Point(11, 144);
            this.txtEmpresaNombre.Name = "txtEmpresaNombre";
            this.txtEmpresaNombre.Size = new System.Drawing.Size(355, 20);
            this.txtEmpresaNombre.TabIndex = 27;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Location = new System.Drawing.Point(11, 122);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(131, 16);
            this.labelControl16.TabIndex = 28;
            this.labelControl16.Text = "Nombre de la Empresa";
            // 
            // tbAdvancedOptions
            // 
            this.tbAdvancedOptions.Location = new System.Drawing.Point(386, 12);
            this.tbAdvancedOptions.Name = "tbAdvancedOptions";
            this.tbAdvancedOptions.SelectedTabPage = this.tpSpecialServices;
            this.tbAdvancedOptions.Size = new System.Drawing.Size(590, 567);
            this.tbAdvancedOptions.TabIndex = 29;
            this.tbAdvancedOptions.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpGen,
            this.tpSpecialServices,
            this.tpTerminalUpdates});
            // 
            // tpSpecialServices
            // 
            this.tpSpecialServices.Controls.Add(this.cbComp);
            this.tpSpecialServices.Controls.Add(this.labelControl17);
            this.tpSpecialServices.Controls.Add(this.cbSolic);
            this.tpSpecialServices.Controls.Add(this.labelControl18);
            this.tpSpecialServices.Controls.Add(this.cbPetc);
            this.tpSpecialServices.Controls.Add(this.labelControl19);
            this.tpSpecialServices.Controls.Add(this.cbUser);
            this.tpSpecialServices.Controls.Add(this.labelControl20);
            this.tpSpecialServices.Name = "tpSpecialServices";
            this.tpSpecialServices.Size = new System.Drawing.Size(583, 538);
            this.tpSpecialServices.Text = "Servicios Especiales";
            // 
            // cbComp
            // 
            this.cbComp.EnterMoveNextControl = true;
            this.cbComp.Location = new System.Drawing.Point(26, 110);
            this.cbComp.Name = "cbComp";
            this.cbComp.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cbComp.Properties.AutoHeight = false;
            this.cbComp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbComp.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("cmp_Nombre", "Nombre", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cbComp.Properties.DisplayMember = "cmp_Nombre";
            this.cbComp.Properties.NullText = "<Seleccione la compa�ia a asignar>";
            this.cbComp.Properties.ValueMember = "cmp_CodPK";
            this.cbComp.Size = new System.Drawing.Size(445, 20);
            this.cbComp.TabIndex = 29;
            this.cbComp.ToolTip = "Conductor que se utilizar� si no se puede localizar al conductor logado en un veh" +
    "iculo";
            this.cbComp.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Location = new System.Drawing.Point(26, 88);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(104, 16);
            this.labelControl17.TabIndex = 28;
            this.labelControl17.Text = "Compa�ia Asignar";
            // 
            // cbSolic
            // 
            this.cbSolic.EnterMoveNextControl = true;
            this.cbSolic.Location = new System.Drawing.Point(26, 176);
            this.cbSolic.Name = "cbSolic";
            this.cbSolic.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cbSolic.Properties.AutoHeight = false;
            this.cbSolic.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSolic.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("sp_Concepto", "Nombre", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cbSolic.Properties.DisplayMember = "sp_Concepto";
            this.cbSolic.Properties.NullText = "<Seleccione el propietario de la Solicitud>";
            this.cbSolic.Properties.ValueMember = "sp_CodPK";
            this.cbSolic.Size = new System.Drawing.Size(445, 20);
            this.cbSolic.TabIndex = 27;
            this.cbSolic.ToolTip = "De cara al GITS, quien solicita los servicios de EPES";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Location = new System.Drawing.Point(26, 154);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(90, 16);
            this.labelControl18.TabIndex = 26;
            this.labelControl18.Text = "Solicitado por...";
            // 
            // cbPetc
            // 
            this.cbPetc.EnterMoveNextControl = true;
            this.cbPetc.Location = new System.Drawing.Point(26, 235);
            this.cbPetc.Name = "cbPetc";
            this.cbPetc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cbPetc.Properties.AutoHeight = false;
            this.cbPetc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPetc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("pet_Descripcion", "Nombre", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cbPetc.Properties.DisplayMember = "pet_Descripcion";
            this.cbPetc.Properties.NullText = "<Seleccione el medio a trav�s del cual se solicito el Servicio>";
            this.cbPetc.Properties.ValueMember = "pet_CodPK";
            this.cbPetc.Size = new System.Drawing.Size(445, 20);
            this.cbPetc.TabIndex = 25;
            this.cbPetc.ToolTip = "Conductor que se utilizar� si no se puede localizar al conductor logado en un veh" +
    "iculo";
            this.cbPetc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Location = new System.Drawing.Point(26, 213);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(199, 16);
            this.labelControl19.TabIndex = 24;
            this.labelControl19.Text = "El Servicio se solicit� a trav�s de ..";
            // 
            // cbUser
            // 
            this.cbUser.EnterMoveNextControl = true;
            this.cbUser.Location = new System.Drawing.Point(26, 47);
            this.cbUser.Name = "cbUser";
            this.cbUser.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cbUser.Properties.AutoHeight = false;
            this.cbUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbUser.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("usu_Nombre", "Nombre", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cbUser.Properties.DisplayMember = "usu_Nombre";
            this.cbUser.Properties.NullText = "<Seleccione el Usuario que aparece como el propietario de los servicios enviados " +
    "por el Sistema EPES>";
            this.cbUser.Properties.ValueMember = "usu_CodPK";
            this.cbUser.Size = new System.Drawing.Size(445, 20);
            this.cbUser.TabIndex = 23;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Location = new System.Drawing.Point(26, 25);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(135, 16);
            this.labelControl20.TabIndex = 22;
            this.labelControl20.Text = "Usuario creo el Servicio";
            // 
            // tpGen
            // 
            this.tpGen.Controls.Add(labelControl24);
            this.tpGen.Controls.Add(this.opUpdateMode);
            this.tpGen.Controls.Add(labelControl23);
            this.tpGen.Controls.Add(this.labelControl22);
            this.tpGen.Controls.Add(this.TrackIntervalPrxServ);
            this.tpGen.Controls.Add(this.ckIntegraSystem);
            this.tpGen.Controls.Add(this.labelControl13);
            this.tpGen.Controls.Add(this.labelControl15);
            this.tpGen.Controls.Add(this.LabelControl12);
            this.tpGen.Controls.Add(this.TrackDiasPosDiario);
            this.tpGen.Controls.Add(this.cbLevelLog);
            this.tpGen.Controls.Add(this.labelControl14);
            this.tpGen.Controls.Add(this.TrackIntervalSend);
            this.tpGen.Controls.Add(this.TrackIntervalVehWorkingNow);
            this.tpGen.Controls.Add(this.labelControl9);
            this.tpGen.Controls.Add(this.TrackIntervalReSend);
            this.tpGen.Controls.Add(this.labelControl11);
            this.tpGen.Controls.Add(this.labelControl10);
            this.tpGen.Controls.Add(this.TrackIntervalStoreProcess);
            this.tpGen.Name = "tpGen";
            this.tpGen.Size = new System.Drawing.Size(583, 538);
            this.tpGen.Text = "General";
            // 
            // opUpdateMode
            // 
            this.opUpdateMode.EditValue = ((byte)(1));
            this.opUpdateMode.Location = new System.Drawing.Point(19, 390);
            this.opUpdateMode.Name = "opUpdateMode";
            this.opUpdateMode.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(1)), "Automatica"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(2)), "Selectiva")});
            this.opUpdateMode.Size = new System.Drawing.Size(309, 30);
            this.opUpdateMode.TabIndex = 34;
            // 
            // TrackIntervalPrxServ
            // 
            this.TrackIntervalPrxServ.EditValue = 1;
            this.TrackIntervalPrxServ.Location = new System.Drawing.Point(346, 187);
            this.TrackIntervalPrxServ.Name = "TrackIntervalPrxServ";
            this.TrackIntervalPrxServ.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TrackIntervalPrxServ.Properties.Appearance.Options.UseForeColor = true;
            this.TrackIntervalPrxServ.Properties.Maximum = 5;
            this.TrackIntervalPrxServ.Size = new System.Drawing.Size(231, 45);
            this.TrackIntervalPrxServ.TabIndex = 32;
            this.TrackIntervalPrxServ.ToolTipTitle = "Intervalo (minutos) calculo del Proximo Servicio de un Vehiculo";
            this.TrackIntervalPrxServ.Value = 1;
            // 
            // ckIntegraSystem
            // 
            this.ckIntegraSystem.Location = new System.Drawing.Point(20, 22);
            this.ckIntegraSystem.Name = "ckIntegraSystem";
            this.ckIntegraSystem.Properties.Appearance.BackColor = System.Drawing.Color.GreenYellow;
            this.ckIntegraSystem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckIntegraSystem.Properties.Appearance.Options.UseBackColor = true;
            this.ckIntegraSystem.Properties.Appearance.Options.UseFont = true;
            this.ckIntegraSystem.Properties.Caption = "Activar Sistema de Integraci�n con otros Servicios/Sistemas";
            this.ckIntegraSystem.Size = new System.Drawing.Size(420, 19);
            this.ckIntegraSystem.TabIndex = 31;
            // 
            // tpTerminalUpdates
            // 
            this.tpTerminalUpdates.Controls.Add(this.frUpdateMode);
            this.tpTerminalUpdates.Name = "tpTerminalUpdates";
            this.tpTerminalUpdates.Size = new System.Drawing.Size(583, 538);
            this.tpTerminalUpdates.Text = "Actualizacion Terminales";
            // 
            // frUpdateMode
            // 
            this.frUpdateMode.Controls.Add(this.dbgC);
            this.frUpdateMode.Dock = System.Windows.Forms.DockStyle.Top;
            this.frUpdateMode.Location = new System.Drawing.Point(0, 0);
            this.frUpdateMode.Name = "frUpdateMode";
            this.frUpdateMode.Size = new System.Drawing.Size(583, 271);
            this.frUpdateMode.TabIndex = 29;
            this.frUpdateMode.Text = "Modo de Actualizacion Software Terminales";
            // 
            // dbgC
            // 
            this.dbgC.DataSource = this.bsTerminalUpdate;
            this.dbgC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbgC.EmbeddedNavigator.Buttons.First.Visible = false;
            this.dbgC.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.dbgC.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.dbgC.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.dbgC.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.dbgC.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.dbgC.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None;
            this.dbgC.Location = new System.Drawing.Point(2, 22);
            this.dbgC.MainView = this.dgbV;
            this.dbgC.Name = "dbgC";
            this.dbgC.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rptcbState,
            this.rptcbUpdateMode,
            this.rptcbSoftwareTipo});
            this.dbgC.Size = new System.Drawing.Size(579, 247);
            this.dbgC.TabIndex = 29;
            this.dbgC.UseEmbeddedNavigator = true;
            this.dbgC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgbV});
            // 
            // bsTerminalUpdate
            // 
            this.bsTerminalUpdate.DataSource = typeof(TerminalUpdateInfo);
            // 
            // dgbV
            // 
            this.dgbV.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gray;
            this.dgbV.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Gray;
            this.dgbV.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgbV.Appearance.FocusedRow.Options.UseBackColor = true;
            this.dgbV.Appearance.FocusedRow.Options.UseFont = true;
            this.dgbV.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgbV.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgbV.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgbV.Appearance.Row.Options.UseFont = true;
            this.dgbV.Appearance.TopNewRow.BackColor = System.Drawing.Color.PapayaWhip;
            this.dgbV.Appearance.TopNewRow.Options.UseBackColor = true;
            this.dgbV.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUpdateMode,
            this.colSoftwareTipo,
            this.colVersionRequerida});
            this.dgbV.GridControl = this.dbgC;
            this.dgbV.Name = "dgbV";
            this.dgbV.NewItemRowText = "Click aqui para agregar un nuevo grupo";
            this.dgbV.OptionsCustomization.AllowRowSizing = true;
            this.dgbV.OptionsDetail.EnableMasterViewMode = false;
            this.dgbV.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.dgbV.OptionsView.RowAutoHeight = true;
            this.dgbV.OptionsView.ShowDetailButtons = false;
            this.dgbV.OptionsView.ShowFooter = true;
            this.dgbV.OptionsView.ShowGroupPanel = false;
            this.dgbV.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSoftwareTipo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colUpdateMode, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colUpdateMode
            // 
            this.colUpdateMode.AppearanceCell.Options.UseTextOptions = true;
            this.colUpdateMode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colUpdateMode.Caption = "Modo";
            this.colUpdateMode.ColumnEdit = this.rptcbUpdateMode;
            this.colUpdateMode.FieldName = "UpdateMode";
            this.colUpdateMode.Name = "colUpdateMode";
            this.colUpdateMode.Visible = true;
            this.colUpdateMode.VisibleIndex = 1;
            this.colUpdateMode.Width = 130;
            // 
            // rptcbUpdateMode
            // 
            this.rptcbUpdateMode.AutoHeight = false;
            this.rptcbUpdateMode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rptcbUpdateMode.Name = "rptcbUpdateMode";
            // 
            // colSoftwareTipo
            // 
            this.colSoftwareTipo.AppearanceCell.Options.UseTextOptions = true;
            this.colSoftwareTipo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colSoftwareTipo.Caption = "Software Tipo";
            this.colSoftwareTipo.ColumnEdit = this.rptcbSoftwareTipo;
            this.colSoftwareTipo.FieldName = "SoftwareTipo";
            this.colSoftwareTipo.Name = "colSoftwareTipo";
            this.colSoftwareTipo.Visible = true;
            this.colSoftwareTipo.VisibleIndex = 0;
            this.colSoftwareTipo.Width = 169;
            // 
            // rptcbSoftwareTipo
            // 
            this.rptcbSoftwareTipo.AutoHeight = false;
            this.rptcbSoftwareTipo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rptcbSoftwareTipo.Name = "rptcbSoftwareTipo";
            // 
            // colVersionRequerida
            // 
            this.colVersionRequerida.Caption = "Versi�n Requerida";
            this.colVersionRequerida.FieldName = "VersionRequerida";
            this.colVersionRequerida.Name = "colVersionRequerida";
            this.colVersionRequerida.Visible = true;
            this.colVersionRequerida.VisibleIndex = 2;
            this.colVersionRequerida.Width = 259;
            // 
            // rptcbState
            // 
            this.rptcbState.AutoHeight = false;
            this.rptcbState.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rptcbState.Name = "rptcbState";
            this.rptcbState.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Location = new System.Drawing.Point(89, 83);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(128, 16);
            this.labelControl21.TabIndex = 30;
            this.labelControl21.Text = "Version de Base Datos";
            // 
            // txtBDVersion
            // 
            this.txtBDVersion.EditValue = "";
            this.txtBDVersion.Enabled = false;
            this.txtBDVersion.EnterMoveNextControl = true;
            this.txtBDVersion.Location = new System.Drawing.Point(251, 82);
            this.txtBDVersion.Name = "txtBDVersion";
            this.txtBDVersion.Size = new System.Drawing.Size(64, 20);
            this.txtBDVersion.TabIndex = 31;
            // 
            // OptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 591);
            this.Controls.Add(this.txtBDVersion);
            this.Controls.Add(this.labelControl21);
            this.Controls.Add(this.tbAdvancedOptions);
            this.Controls.Add(this.txtEmpresaNombre);
            this.Controls.Add(this.labelControl16);
            this.Controls.Add(this.cmdCkExpand);
            this.Controls.Add(this.cmdThrowIncid);
            this.Controls.Add(this.txtMaxClientes);
            this.Controls.Add(this.LabelControl7);
            this.Controls.Add(this.cmdAceptar);
            this.Controls.Add(this.frEMail);
            this.Controls.Add(this.txtServerPort);
            this.Controls.Add(this.txtUrlEndPoint);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "OptionForm";
            this.Text = "Opciones de Configuracion";
            this.Load += new System.EventHandler(this.OptionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtMaxClientes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frEMail)).EndInit();
            this.frEMail.ResumeLayout(false);
            this.frEMail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMails.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmEmailUser)).EndInit();
            this.frmEmailUser.ResumeLayout(false);
            this.frmEmailUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail_Pwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail_User.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckEMail_Requiere.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail_SMTP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUrlEndPoint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackDiasPosDiario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackDiasPosDiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalVehWorkingNow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalVehWorkingNow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalStoreProcess.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalStoreProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalReSend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalReSend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalSend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalSend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbLevelLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpresaNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAdvancedOptions)).EndInit();
            this.tbAdvancedOptions.ResumeLayout(false);
            this.tpSpecialServices.ResumeLayout(false);
            this.tpSpecialServices.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbComp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSolic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPetc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbUser.Properties)).EndInit();
            this.tpGen.ResumeLayout(false);
            this.tpGen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.opUpdateMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalPrxServ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackIntervalPrxServ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckIntegraSystem.Properties)).EndInit();
            this.tpTerminalUpdates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frUpdateMode)).EndInit();
            this.frUpdateMode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dbgC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTerminalUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgbV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptcbUpdateMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptcbSoftwareTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptcbState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBDVersion.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.CheckButton cmdCkExpand;
        internal DevExpress.XtraEditors.SimpleButton cmdThrowIncid;
        internal DevExpress.XtraEditors.TextEdit txtMaxClientes;
        internal DevExpress.XtraEditors.LabelControl LabelControl7;
        internal DevExpress.XtraEditors.SimpleButton cmdAceptar;
        internal DevExpress.XtraEditors.GroupControl frEMail;
        internal DevExpress.XtraEditors.MemoEdit txtEMails;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.PanelControl frmEmailUser;
        internal DevExpress.XtraEditors.TextEdit txtEMail_Pwd;
        internal DevExpress.XtraEditors.LabelControl LabelControl6;
        internal DevExpress.XtraEditors.TextEdit txtEMail_User;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.CheckEdit ckEMail_Requiere;
        internal DevExpress.XtraEditors.TextEdit txtEMail_SMTP;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.TextEdit txtServerPort;
        internal DevExpress.XtraEditors.TextEdit txtUrlEndPoint;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl12;
        internal DevExpress.XtraEditors.ComboBoxEdit cbLevelLog;
        internal DevExpress.XtraEditors.TextEdit txtEmailFrom;
        internal DevExpress.XtraEditors.LabelControl labelControl8;
        private System.Windows.Forms.BindingSource bindingSource1;
        internal DevExpress.XtraEditors.LabelControl labelControl9;
        private gcperu.Server.UI.Controls.MyTrackBarControl TrackIntervalSend;
        internal DevExpress.XtraEditors.LabelControl labelControl10;
        private gcperu.Server.UI.Controls.MyTrackBarControl TrackIntervalReSend;
        internal DevExpress.XtraEditors.LabelControl labelControl11;
        private gcperu.Server.UI.Controls.MyTrackBarControl TrackIntervalStoreProcess;
        internal DevExpress.XtraEditors.LabelControl labelControl13;
        internal DevExpress.XtraEditors.LabelControl labelControl14;
        private gcperu.Server.UI.Controls.MyTrackBarControl TrackIntervalVehWorkingNow;
        internal DevExpress.XtraEditors.LabelControl labelControl15;
        private gcperu.Server.UI.Controls.MyTrackBarControl TrackDiasPosDiario;
        internal DevExpress.XtraEditors.TextEdit txtEmpresaNombre;
        internal DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraTab.XtraTabControl tbAdvancedOptions;
        private DevExpress.XtraTab.XtraTabPage tpSpecialServices;
        internal DevExpress.XtraEditors.LookUpEdit cbComp;
        internal DevExpress.XtraEditors.LabelControl labelControl17;
        internal DevExpress.XtraEditors.LookUpEdit cbSolic;
        internal DevExpress.XtraEditors.LabelControl labelControl18;
        internal DevExpress.XtraEditors.LookUpEdit cbPetc;
        internal DevExpress.XtraEditors.LabelControl labelControl19;
        internal DevExpress.XtraEditors.LookUpEdit cbUser;
        internal DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraTab.XtraTabPage tpGen;
        internal DevExpress.XtraEditors.CheckEdit ckIntegraSystem;
        internal DevExpress.XtraEditors.LabelControl labelControl21;
        internal DevExpress.XtraEditors.TextEdit txtBDVersion;
        internal DevExpress.XtraEditors.LabelControl labelControl22;
        private gcperu.Server.UI.Controls.MyTrackBarControl TrackIntervalPrxServ;
        private DevExpress.XtraTab.XtraTabPage tpTerminalUpdates;
        private DevExpress.XtraEditors.GroupControl frUpdateMode;
        private DevExpress.XtraGrid.GridControl dbgC;
        private DevExpress.XtraGrid.Views.Grid.GridView dgbV;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rptcbState;
        private System.Windows.Forms.BindingSource bsTerminalUpdate;
        private DevExpress.XtraGrid.Columns.GridColumn colSoftwareTipo;
        private DevExpress.XtraGrid.Columns.GridColumn colVersionRequerida;
        private DevExpress.XtraEditors.RadioGroup opUpdateMode;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdateMode;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rptcbUpdateMode;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rptcbSoftwareTipo;
    }
}