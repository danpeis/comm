﻿using System;
using System.Drawing;
using System.Net.Mime;
using System.Reflection;
using System.Security.Principal;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.Utils;
using EntitySpaces.DebuggerVisualizer;
using EntitySpaces.Interfaces;
using EntitySpaces.LoaderMT;
using gcperu.Server.Core;
using gcperu.Server.Core.Initialization;
using gcperu.Server.Core.IoC;
using gitspt.global.Core.Exceptions;
using gitspt.global.Core.Extension;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;
using gitspt.global.Core.Util;
using GCPeru.Server.Core.Settings;
using GCPeru.Server.Core.Settings.Manager;

namespace gcperu.Server.UI
{
    public class RuncaseBase :RuncoreCase,IRunCase
    {
        protected RuncaseBase()
        {
        }

        public override void Run()
        {
            base.Run();

            ModuleConfig.AppConfig.AppVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ModuleConfig.AppConfig.AppTitle = string.Format("{0}    {1}", Core.Constantes.APPID, ModuleConfig.AppConfig.AppVersion);

            esProviderFactory.Factory = new esDataProviderFactory();
            esVisualizer.Initialize();

            ApplyGlobalCompileOptions();
        }

        protected override void LoadIocSettings()
        {
            _IocContainer.Kernel.Bind<ISSTraceLog>().ToConstant(new SSTraceLog());
        }

        protected override void LoadLogConfig()
        {
            base.LoadLogConfig();

            if (_log!=null && _log.SiConfig.Enabled)
                return;

            _log = _IocContainer.Get<ISSTraceLog>();

            _log.LoadSmartInpectFile(Contantes.UI_FILE_LOG_CONFIG, new WindowsPrincipal(WindowsIdentity.GetCurrent()));
            _log.LogInformation("LOG Configuration loaded OK.");

        }

        /// <summary>
        /// Load user config. Llamar a este metodo siempre despues de obtener/indicar el usuario del Sistema.
        /// </summary>
        protected void LoadUserConfig()
        {
            //Configuracion de la Interfaz desde el usuario
            if (string.IsNullOrEmpty(ModuleConfig.UserConfig.InterfazFontDefault))
            {
                AppearanceObject.DefaultFont = new Font("Tahoma", 10);
                ModuleConfig.UserConfig.InterfazFontDefault = AppearanceObject.DefaultFont.ToStringFont();
            }
            else
            {
                AppearanceObject.DefaultFont = ModuleConfig.UserConfig.InterfazFontDefault.ToFont(new Font("Tahoma", 10));
            }
            if (ModuleConfig.UserConfig.InterfazControlActiveColor == 0)
            {
                AppearanceDefault.Control.BackColor = Color.AliceBlue;
                ModuleConfig.UserConfig.InterfazControlActiveColor = Color.AliceBlue.ToArgb();
            }
            else
            {
                AppearanceDefault.Control.BackColor = Color.FromArgb(ModuleConfig.UserConfig.InterfazControlActiveColor);
            }

            _log.LogInformation("LoadUserConfig. Ok");
        }


        protected override void ApplyGlobalCompileOptions()
        {
#if (EnProduccion)
         ModuleConfig.AppConfig.CompileSymbols.EnProduccion = true;
#endif

#if (SecurityEnable)
         ModuleConfig.AppConfig.CompileSymbols.SecuridadActivada = true;
#endif

#if (SaveRestoreLayout)
         ModuleConfig.AppConfig.CompileSymbols.SaveRestoreLayout = true;
#endif
        }

        public void Dispose()
        {
        }
    }

    public class DebugRunCase : RuncaseBase, IDisposable
    {
       
        public override void Run()
        {
            base.Run();

            _log.LogTraceStart("CaseRun: DebugCase... >>>");

            try
            {
                UnhandlerExceptionHandler.SetDomainEvents();
                UnhandlerExceptionHandler.SetUiThreadEvents();

                if (!base.AddAndVerifyDatabaseConnections())
                {
                    MsgBox.Show("NO SE HA PODIDO CONECTAR CON EL SERVIDOR DE DATOS.\nLA APLICACIÓN NO PUEDE CONTINUAR.\n\nDISCULPE LAS MOLESTIAS");
                    return;
                }

                //BonusSkins.Register();
                SkinManager.EnableFormSkins();
                UserLookAndFeel.Default.SetSkinStyle("DevExpress Style");

                LoadUserConfig();
                ConfigurarGestionLayouts();

                Application.Run(new Mainfm());

            }
            catch (System.Exception EX)
            {
                ManejarExceptions.ManejarException("ERROR INICIAR APLICACIÓN. TIPO INICIO: NORMAL", EX, ExceptionErrorLevel.Critical);

                MsgBox.Show("LA APLICACIÓN NO PUEDE CONTINUAR Y SE CERRARÁ.\n\nDISCULPE LAS MOLESTIAS.");
            }
            finally
            {
                _log.LogTraceStop("CaseRun: EnProducionRun. <<<<");
            }
        }

        public void Dispose()
        {
            base.DisposeBase();
        }

        protected override void LoadIocSettings()
        {
            base.LoadIocSettings();

            _IocContainer.Kernel.Bind<IAppConfigManager>().ToConstant(new AppConfigManager());
            _IocContainer.Kernel.Bind<IUserConfigManager>().ToConstant(new UserConfigManager());


        }

        //protected override void ApplyGlobalCompileOptions()
    }


    public abstract class ProducionCase : RuncaseBase, IDisposable
    {
        public override void Run()
        {
            base.Run();

            _log.LogTraceStart("CaseRun: ProducioncoreCase... >>>");

            try
            {
                UnhandlerExceptionHandler.SetDomainEvents();
                UnhandlerExceptionHandler.SetUiThreadEvents();

                base.Run();

                if (!base.AddAndVerifyDatabaseConnections())
                {
                    MsgBox.Show("NO SE HA PODIDO CONECTAR CON EL SERVIDOR DE DATOS.\nLA APLICACIÓN NO PUEDE CONTINUAR.\n\nDISCULPE LAS MOLESTIAS");
                    return;
                }

                //BonusSkins.Register();
                SkinManager.EnableFormSkins();
                UserLookAndFeel.Default.SetSkinStyle("DevExpress Style");

                LoadUserConfig();
                ConfigurarGestionLayouts();

                Application.Run(new Mainfm());

            }
            catch (System.Exception EX)
            {
                ManejarExceptions.ManejarException("ERROR INICIAR APLICACIÓN. TIPO INICIO: NORMAL", EX, ExceptionErrorLevel.Critical);

                MsgBox.Show("LA APLICACIÓN NO PUEDE CONTINUAR Y SE CERRARÁ.\n\nDISCULPE LAS MOLESTIAS.");
            }
            finally
            {
                _log.LogTraceStop("CaseRun: EnProducionRun. <<<<");
            }
        }

        public void Dispose()
        {
            base.DisposeBase();
        }

        protected override void LoadIocSettings()
        {
            base.LoadIocSettings();

            _IocContainer.Kernel.Bind<IAppConfigManager>().ToConstant(new AppConfigManager());
            _IocContainer.Kernel.Bind<IUserConfigManager>().ToConstant(new UserConfigManager());

            
        }

        protected abstract override void ApplyGlobalCompileOptions();
    }

}