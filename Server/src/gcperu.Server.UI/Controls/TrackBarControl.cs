using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.ViewInfo;

namespace gcperu.Server.UI.Controls
{
    public class MyTrackBarControl : TrackBarControl
    {

        public MyTrackBarControl()
        {
            Properties.Appearance.ForeColor = Color.Black;
        }

        protected TrackBarViewInfo RangeViewInfo { get { return ViewInfo as TrackBarViewInfo; } }

        protected int ThumbTextIndent { get { return 7; } }

        protected Point MinTextPos
        {
            get { return new Point(RangeViewInfo.ThumbBounds.Left , RangeViewInfo.ThumbBounds.Bottom + ThumbTextIndent); }
        }

        protected Point MaxTextPos
        {
            get { return new Point(RangeViewInfo.ThumbBounds.Left - 15, RangeViewInfo.ThumbBounds.Bottom + ThumbTextIndent); }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            GraphicsCache cache = new GraphicsCache(e.Graphics);

            var fontbold = new Font(ViewInfo.PaintAppearance.Font.FontFamily,10, FontStyle.Bold);
            var brush = new SolidBrush(System.Drawing.Color.DarkRed);

            if (Value>Properties.Minimum && Value<Properties.Maximum)
                e.Graphics.DrawString(Value.ToString(), fontbold, brush, MinTextPos);

            e.Graphics.DrawString(Properties.Minimum.ToString(), ViewInfo.PaintAppearance.Font, ViewInfo.PaintAppearance.GetForeBrush(cache), new PointF(ViewInfo.PointsRect.Left - 7, ViewInfo.PointsRect.Bottom));
            e.Graphics.DrawString(Properties.Maximum.ToString(), ViewInfo.PaintAppearance.Font, ViewInfo.PaintAppearance.GetForeBrush(cache), new PointF(ViewInfo.PointsRect.Right - 15, ViewInfo.PointsRect.Bottom));
        }
    }
}