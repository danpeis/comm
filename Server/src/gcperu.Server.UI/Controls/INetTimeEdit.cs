using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.ViewInfo;
using gcperu.Server.Core.Extension;
using Gurock.SmartInspect;
//using Gurock.SmartInspect.PostSharp;


namespace gcperu.Server.UI.Controls
{
    [UserRepositoryItem("axTimeEdit")]
    public class RepositoryItemINetTimeEdit : RepositoryItemTimeEdit
    {
        public const string INetTimeEditName = "axTimeEdit";
        private IContainer components;

        #region Field

        #endregion

        #region Events

        /// <summary>
        /// Indica que el control se ha inicializado.
        /// </summary>
        public event EventHandler Initialized;

        public virtual void OnInitialized()
        {
            EventHandler handler = Initialized;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        #endregion

        #region Ctor

        public static void RegisterINetTimeEdit()
        {
            //Image img = null;
            //try
            //{
            //    img = (Bitmap)Bitmap.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("DevExpress.CustomEditors.CustomEdit.bmp"));
            //}
            //catch
            //{
            //}

            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(INetTimeEditName,
                                                                           typeof(axTimeEdit),
                                                                           typeof(RepositoryItemINetTimeEdit),
                                                                           typeof(BaseSpinEditViewInfo),
                                                                           new ButtonEditPainter(), true, null));


            // EditorRegistrationInfo.Default.Editors.Add( new EditorClassInfo( INetTimeEditName, typeof( axTimeEdit ), typeof( RepositoryItemLookUpEdit ), typeof( LookUpEditViewInfo ), new BaseEditPainter( ), true ) );
        }

        static RepositoryItemINetTimeEdit()
        {
            RegisterINetTimeEdit();
        }

        public RepositoryItemINetTimeEdit()
        {
        }

        #endregion

        #region Propertys

        #region Behavior

        private bool _obligatorio;

        [Category("Behavior"), DefaultValue(false)]
        public bool Obligatorio
        {
            get { return _obligatorio; }
            set { _obligatorio = value; }
        }


        private TimeEditMask _TimeEditMask = TimeEditMask.HourMin;

        [Category("Behavior"), DefaultValue(Controls.TimeEditMask.HourMin)]
        public TimeEditMask TimeEditMask
        {
            get { return _TimeEditMask; }
            set
            {
                _TimeEditMask = value;
                switch (value)
                {
                    case TimeEditMask.None:
                        Mask.EditMask = "";
                        break;
                    case TimeEditMask.HourMin:
                        Mask.EditMask = "HH:mm";
                        break;
                    case TimeEditMask.HourMinSec:
                        Mask.EditMask = "HH:mm:ss";
                        break;
                    case TimeEditMask.Hour:
                        Mask.EditMask = "HH";
                        break;
                }
            }
        }

        #endregion

        #region Data

        #endregion

        #endregion

        #region Overrides

        protected override RepositoryItem CreateRepositoryItem()
        {
            return base.CreateRepositoryItem();

        }

        public override void EndInit()
        {
            base.EndInit();
            OnInitialized();
        }

        public override string EditorTypeName
        {
            get
            {
                return INetTimeEditName;
            }
        }

        public override void Assign(RepositoryItem item)
        {
            BeginUpdate();
            try
            {
                base.Assign(item);
                var source = item as RepositoryItemINetTimeEdit;
                if (source == null)
                    return;

                Obligatorio = source.Obligatorio;
                TimeEditMask = source.TimeEditMask;
            }
            finally
            {
                EndUpdate();
            }
        }

        #endregion
    }

    //[SiTrace(SessionPolicy = SessionPolicy.TypeName, IncludeArguments = true, IncludeReturnValue = true, IncludeSpecialNames = false)]
    [ToolboxItem(true), DefaultEvent("ValueChanged"), DefaultProperty("Value")]
    public class axTimeEdit : TimeEdit
    {

        #region Fields

        private bool _loaded;
        private DateTime? _currentValue;
        private bool _editValueChanging;  //Indica que se estan cambiando el valor asignado al control.
        private bool _raisingValueChanged;  //Indica que se est� lanzando el evento ValueChanged y ValueChanging.
        private bool _editorKeyPressed;  //Indica si se ha presionado una tecla sobre el combo.

        #endregion

        #region Cotr

        static axTimeEdit()
        {
            RepositoryItemINetTimeEdit.RegisterINetTimeEdit();
        }



        public axTimeEdit()
        {

#if (!SkipPostSharp)
            SiAuto.Si.Connections = "tcp()";
            SiAuto.Si.Level = Level.Debug;
            SiAuto.Si.DefaultLevel = Level.Debug;
            SiAuto.Si.Enabled = true;      
#endif

            this.LostFocus += new EventHandler(INetTimeEdit_LostFocus);
            this.Enter += new EventHandler(INetTimeEdit_Enter);
            this.KeyDown += new KeyEventHandler(INetTimeEdit_KeyDown);
            this.KeyPress += new KeyPressEventHandler(INetTimeEdit_KeyPress);
            Properties.Initialized += new EventHandler(Repository_Initialized);

            InitializeProperties();
        }

        void INetTimeEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) || e.KeyChar == 8)
                _editorKeyPressed = true;
        }

        void Repository_Initialized(object sender, EventArgs e)
        {
            _loaded = true;
            ActionEnterLeave(false, true);
            Properties.AppearanceDisabled.ForeColor = Color.Black;
        }

        void INetTimeEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                e.Handled = true;
                base.EditValue = null;
            }
            else if (e.KeyCode == Keys.Enter && e.Shift)
            {
                SendKeys.Send("+{TAB}");
            }
        }

        #endregion

        #region eventos

        public event EventHandler<ChangedEventArgs> ValueChanged;

        public virtual void OnValueChanged(ChangedEventArgs changedEventArgs)
        {
            EventHandler<ChangedEventArgs> handler = ValueChanged;
            if (handler != null) handler(this, changedEventArgs);
        }

        public event EventHandler<Controls.ChangingEventArgs> ValueChanging;

        public virtual void OnValueChanging(ChangingEventArgs e)
        {
            EventHandler<ChangingEventArgs> handler = ValueChanging;
            if (handler != null) handler(this, e);
        }

        #endregion

        #region Properties

        [Category("Behavior"), DefaultValue(false)]
        public bool Obligatorio
        {
            get { return Properties.Obligatorio; }
            set { Properties.Obligatorio = value; ActionEnterLeave(false); }
        }

        [Category("Behavior"), DefaultValue(TimeEditMask.HourMin)]
        public TimeEditMask TimeEditMask
        {
            get { return Properties.TimeEditMask; }
            set { Properties.TimeEditMask = value; }
        }

        //[Category("Behavior"), DefaultValue("")]
        //public string EditMask
        //{
        //    get { return Properties.Mask.EditMask; }
        //    set
        //    {
        //        TimeEditMask=TimeEditMask.None;
        //        Properties.Mask.EditMask = value;
        //    }
        //}

        #endregion
        
        #region EventHandler

        protected override void OnEditValueChanged()
        {
            base.OnEditValueChanged();
            if (Properties.IsDesignMode || _editValueChanging || _raisingValueChanged) return;
            if (_editorKeyPressed) return;

            try
            {
                //Evento se lanza ante cualquier cambio en el Combo, sin necesidad de perder el foco.
                bool cancelar = false;
                RaiseEventValueChanging(EditValue, ref cancelar);
                if (cancelar)
                {
                    _editorKeyPressed = false;
                    return;
                }

                DateTime? _currenValueOld = _currentValue;
                _currentValue = EditValue;
                //Ha cambiado el valor, se lanza desde aqui.
                OnValueChanged(new ChangedEventArgs(_currenValueOld, _currentValue));

            }
            catch (System.Exception e)
            {
                Console.WriteLine(new System.Exception("axTimeEdit.OnEditValueChanged. Lanzando evento ValueChanged", e).ToString());
            }
            finally
            {
                _raisingValueChanged = false;
            }


        }

        void INetTimeEdit_Enter(object sender, EventArgs e)
        {
            if (!Properties.IsDesignMode)
            {
                ActionEnterLeave(true);

                if (Properties.Mask.MaskType == MaskType.Numeric)
                    SelectAll();
                else
                    SelectionStart = base.Text.Length + 1;
            }
        }

        void INetTimeEdit_LostFocus(object sender, EventArgs e)
        {
            if (!Properties.IsDesignMode)
            {
                if (_raisingValueChanged) return;

                ActionEnterLeave(false);
                if (!_loaded) return;

                if (ValorHaCambiado())
                {
                    try
                    {
                        //Evento se lanza ante cualquier cambio en el Combo, sin necesidad de perder el foco.
                        bool cancelar = false;
                        RaiseEventValueChanging(EditValue, ref cancelar);
                        if (cancelar)
                        {
                            _editorKeyPressed = false;
                            return;
                        }

                        DateTime? _currenValueOld = _currentValue;
                        _currentValue = EditValue;
                        //Ha cambiado el valor, se lanza desde aqui.
                        OnValueChanged(new ChangedEventArgs(_currenValueOld, _currentValue));
                    }
                    catch (System.Exception ex)
                    {
                        Console.WriteLine(new System.Exception("axTimeEdit.LostFocus. Lanzando evento ValueChanged", ex).ToString());
                    }
                    finally
                    {
                        _raisingValueChanged = false;
                    }
                }
            }
        }

        private bool ValorHaCambiado()
        {
            return _currentValue != EditValue;
        }

        #endregion

        #region Overrides

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Properties.Initialized -= Repository_Initialized;
            }
            base.Dispose(disposing);
        }

        protected override void InitializeDefaultProperties()
        {
            base.InitializeDefaultProperties();
            InitializeProperties();
        }

        public override string EditorTypeName
        {
            get
            {
                return RepositoryItemINetTimeEdit.INetTimeEditName;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RepositoryItemINetTimeEdit Properties
        {
            get
            {
                return base.Properties as RepositoryItemINetTimeEdit;
            }
        }

        #endregion

        #region Privadas

        private void RaiseEventValueChanging(DateTime? value, ref bool cancel)
        {
            #region ValueChanging

            cancel = false;
            //Hay cambios, y hay que establecerlo en el control Lookup.
            if (ValueChanging != null)
            {
                try
                {
                    //Hay algo que controla este evento, por tanto lo lanzamos.
                    _raisingValueChanged = true;
                    var eargs = new ChangingEventArgs(_currentValue, EditValue);

                    OnValueChanging(eargs);
                    if (eargs.Cancel)
                    {
                        _editValueChanging = true;
                        base.EditValue = _currentValue;
                        _editValueChanging = false;

                        cancel = true;
                        return;
                    }
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(new System.Exception("ERROR INetTextEdit.RaiseEventValueChanging.", e).ToString());
                }
                finally
                {
                    _raisingValueChanged = false;
                }
            }

            #endregion
        }

        private void InitRuntime()
        {
            if (!Properties.IsDesignMode)
            {
            }
        }

        private void InitializeProperties()
        {
            EnterMoveNextControl = true;
            Properties.Appearance.TextOptions.HAlignment = HorzAlignment.Far;
            base.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnValidation;

            //Properties.Mask.UseMaskAsDisplayFormat = true;
            Properties.Mask.UseMaskAsDisplayFormat = true;
            Properties.Mask.MaskType = MaskType.DateTimeAdvancingCaret;
            Properties.TimeEditMask = TimeEditMask.HourMin;

        }

        private void ActionEnterLeave(bool focused, bool initialized = false)
        {
        }

        #endregion

        #region Publicas

        [Bindable(true), DefaultValue(typeof(DateTime?)), Category("Data"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime? Value
        {
            get { return EditValue; }
            set { EditValue = value; }
        }

        [DefaultValue(typeof(DateTime?), "null"), Category("Data"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new DateTime? EditValue
        {
            get
            {
                if (base.EditValue == null) return null;

                return base.EditValue.ObjectToDateTime();
            }

            set
            {
                if (!IsDesignMode && !_loaded)
                    InitRuntime();

                _editValueChanging = true;
                base.EditValue = value;
                _currentValue = value;
                _editValueChanging = false;
            }
        }

        public override string Text
        {
            get
            {
                if (EditValue.HasValue)
                        return EditValue.Value.TimeOfDay.ToString();
                else
                    return "";
            }
        }


        #endregion

    }
}