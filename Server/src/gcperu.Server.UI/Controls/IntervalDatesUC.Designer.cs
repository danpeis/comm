namespace gcperu.Server.UI.Controls
{
    partial class IntervalDatesUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            this.btFeclas = new DevExpress.XtraEditors.ButtonEdit();
            this.btPeriodo = new DevExpress.XtraEditors.ButtonEdit();
            this.dtH = new axDateEdit();
            this.dtD = new axDateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.btFeclas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPeriodo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtH.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtD.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtD.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btFeclas
            // 
            this.btFeclas.Location = new System.Drawing.Point(72, 1);
            this.btFeclas.Margin = new System.Windows.Forms.Padding(0);
            this.btFeclas.Name = "btFeclas";
            this.btFeclas.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject1.Options.UseFont = true;
            this.btFeclas.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinLeft, "Left", 15, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinRight, "Right", 15, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btFeclas.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btFeclas.Size = new System.Drawing.Size(36, 20);
            this.btFeclas.TabIndex = 4;
            this.btFeclas.TabStop = false;
            this.btFeclas.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btFeclas_ButtonClick);
            // 
            // btPeriodo
            // 
            this.btPeriodo.EnterMoveNextControl = true;
            this.btPeriodo.Location = new System.Drawing.Point(181, 1);
            this.btPeriodo.Margin = new System.Windows.Forms.Padding(4);
            this.btPeriodo.Name = "btPeriodo";
            this.btPeriodo.Properties.Appearance.Options.UseTextOptions = true;
            this.btPeriodo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btPeriodo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btPeriodo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Todas", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Trimestre", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Mes", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Semana", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Dia", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "", null, null, true)});
            this.btPeriodo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btPeriodo.Size = new System.Drawing.Size(212, 20);
            this.btPeriodo.TabIndex = 5;
            this.btPeriodo.TabStop = false;
            this.btPeriodo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btPeriodo_ButtonClick);
            // 
            // dtH
            // 
            this.dtH.DateEditPartShow = DateEditPartShow.Both;
            this.dtH.EditValue = new System.DateTime(2011, 2, 17, 12, 45, 0, 0);
            this.dtH.EnterMoveNextControl = true;
            this.dtH.Location = new System.Drawing.Point(109, 1);
            this.dtH.Margin = new System.Windows.Forms.Padding(4);
            this.dtH.Name = "dtH";
            this.dtH.Properties.Appearance.BackColor = System.Drawing.Color.LightCyan;
            this.dtH.Properties.Appearance.Options.UseBackColor = true;
            this.dtH.Properties.Appearance.Options.UseTextOptions = true;
            this.dtH.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.dtH.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.dtH.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.dtH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtH.Properties.DateEditPartShow = DateEditPartShow.Both;
            this.dtH.Properties.Mask.EditMask = "dd/MM/yy";
            this.dtH.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dtH.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtH.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtH.Size = new System.Drawing.Size(71, 20);
            this.dtH.TabIndex = 1;
            this.dtH.ValueChanged += new System.EventHandler<ChangedEventArgs>(this.Fecha_ValueChanged);
            // 
            // dtD
            // 
            this.dtD.DateEditPartShow = DateEditPartShow.Both;
            this.dtD.EditValue = new System.DateTime(2011, 2, 17, 12, 45, 21, 600);
            this.dtD.EnterMoveNextControl = true;
            this.dtD.Location = new System.Drawing.Point(0, 1);
            this.dtD.Margin = new System.Windows.Forms.Padding(4);
            this.dtD.Name = "dtD";
            this.dtD.Properties.Appearance.BackColor = System.Drawing.Color.LightCyan;
            this.dtD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtD.Properties.Appearance.Options.UseBackColor = true;
            this.dtD.Properties.Appearance.Options.UseFont = true;
            this.dtD.Properties.Appearance.Options.UseTextOptions = true;
            this.dtD.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.dtD.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.dtD.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.dtD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtD.Properties.DateEditPartShow = DateEditPartShow.Both;
            this.dtD.Properties.Mask.EditMask = "dd/MM/yy";
            this.dtD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dtD.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtD.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtD.Size = new System.Drawing.Size(72, 20);
            this.dtD.TabIndex = 0;
            this.dtD.ValueChanged += new System.EventHandler<ChangedEventArgs>(this.Fecha_ValueChanged);
            // 
            // IntervalDatesUC
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.dtD);
            this.Controls.Add(this.btPeriodo);
            this.Controls.Add(this.btFeclas);
            this.Controls.Add(this.dtH);
            this.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.Name = "IntervalDatesUC";
            this.Size = new System.Drawing.Size(397, 25);
            this.Load += new System.EventHandler(this.IntervalDatesUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btFeclas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPeriodo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtH.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtD.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtD.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private axDateEdit dtD;
        private axDateEdit dtH;
        private DevExpress.XtraEditors.ButtonEdit btFeclas;
        private DevExpress.XtraEditors.ButtonEdit btPeriodo;
    }
}
