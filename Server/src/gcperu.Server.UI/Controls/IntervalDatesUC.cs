using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using gcperu.Server.Core.Extension;
using Microsoft.VisualBasic;

namespace gcperu.Server.UI.Controls
{
    public partial class IntervalDatesUC : DevExpress.XtraEditors.XtraUserControl
    {
        public enum PeriodosFecha
        {
            Todas=0,
            Dia=1,
            Semana=2,
            Mes=3,
            Trimestre=4
        }

        private bool _buttonDiaVisible = true,
                     _buttonMesVisible = true,
                     _buttonTrimestreVisible = true,
                     _buttonCampa�aVisible = true,
                     _buttonTodasVisible = true,
                     _buttonSemanaVisible = false;

        private Dictionary<string,EditorButton> _dbuttons=new Dictionary<string, EditorButton>();

        private PeriodosFecha _buttonsShowed = PeriodosFecha.Dia & PeriodosFecha.Mes & PeriodosFecha.Trimestre;
        private DateTime? _desdeOldValue=null, _hastaOldValue=null;
        private PeriodosFecha _periodoCurrent, _periodoDefault;
        private PeriodosFecha _periodoMaximo=PeriodosFecha.Todas;

        public IntervalDatesUC()
        {
            InitializeComponent();
            this.Height = dtD.Height;
            
            //Cargamos los Botones de Periodos.
            foreach (EditorButton eb in btPeriodo.Properties.Buttons)
            {
                _dbuttons.Add(eb.Caption, eb);
            }
        }

        #region EventosDefinicion

        public event EventHandler<IntervalDatesChangedEventArgs> ValueChanged;

        public virtual void OnValueChanged(IntervalDatesChangedEventArgs datesChangedEventArgs)
        {
            EventHandler<IntervalDatesChangedEventArgs> handler = ValueChanged;
            if (handler != null) handler(this, datesChangedEventArgs);
        }

        public virtual void OnValueChanged()
        {
            EventHandler<IntervalDatesChangedEventArgs> handler = ValueChanged;
            if (handler != null) handler(this, GenerateIntervalChanged());
        }

        #endregion

        #region Properties

        [Description("Indica el periodo a utilizar  de forma predeterminada")]
        public PeriodosFecha PeriodoDefault
        {
            get { return _periodoDefault; }
            set
            {
                _periodoDefault = value;
                _periodoCurrent = value;
                SetFechasByPeriodo(_periodoDefault);
            }
        }

        [Description("Indica el M�ximo Periodo que el Usuario puede seleccionar. Valido para acotar el N� m�ximo de tiempo que se puede filtrar"),DefaultValue(PeriodosFecha.Todas)]
        public PeriodosFecha PeriodoMaximo
        {
            get { return _periodoMaximo; }
            set
            {
                _periodoMaximo = value;
                VerifyPeriodoMaximo(_periodoMaximo);
            }
        }

        [Description("Indica el periodo actualmente utilizado en el rango de fechas"),Browsable(false)]
        public PeriodosFecha PeriodoCurrent
        {
            get { return _periodoCurrent; }
        }

        [Category("Data"), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime? Desde
        {
            get { return dtD.EditValue; }
            set
            {
                _desdeOldValue = dtD.EditValue;
                dtD.EditValue = value;
                VerifyDesdeChanged(_desdeOldValue);
                if (!DesignMode) OnValueChanged();
            }
        }

        [Category("Data"), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime? Hasta
        {
            get { return dtH.EditValue; }
            set
            {
                _hastaOldValue= dtH.EditValue;
                dtH.EditValue = value;
                VerifyHastaChanged(_hastaOldValue);
                if (!DesignMode) OnValueChanged();
            }
        }

        [DefaultValue(true)]
        public bool ButtonDiaVisible
        {
            get { return _buttonDiaVisible; }
            set 
            {
                _buttonDiaVisible =value;
                SetVisibilidadButtons(PeriodosFecha.Dia,value);
            }
        }

        [DefaultValue(true)]
        public bool ButtonMesVisible
        {
            get { return _buttonMesVisible; }
            set
            {
                _buttonMesVisible = value;
                SetVisibilidadButtons(PeriodosFecha.Mes, value);
            }
        }

        [DefaultValue(true)]
        public bool ButtonTrimestreVisible
        {
            get { return _buttonTrimestreVisible; }
            set
            {
                _buttonTrimestreVisible = value;
                SetVisibilidadButtons(PeriodosFecha.Trimestre, value);
            }
        }

        [DefaultValue(true)]
        public bool ButtonTodasVisible
        {
            get { return _buttonTodasVisible; }
            set
            {
                _buttonTodasVisible = value;
                SetVisibilidadButtons(PeriodosFecha.Todas, value);
            }
        }

        [DefaultValue(true)]
        public bool ButtonSemanaVisible
        {
            get { return _buttonSemanaVisible; }
            set
            {
                _buttonSemanaVisible = value;
                SetVisibilidadButtons(PeriodosFecha.Semana, value);
            }
        }

        public override Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
                dtD.Font = value;
                dtH.Font = value;
                btFeclas.Font = value;
                btPeriodo.Font = value;

                foreach (EditorButton btn in btPeriodo.Properties.Buttons)
                {
                    btn.Appearance.Font = value;
                }

                foreach (EditorButton btn in btFeclas.Properties.Buttons)
                {
                    btn.Appearance.Font = value;
                }

                ApplyBestSize(dtD,dtD.CalcBestSize());
                ApplyBestSize(dtH, dtH.CalcBestSize());
                ApplyBestSize(btPeriodo, btPeriodo.CalcBestSize());

                btFeclas.Left = dtD.Left + dtD.Width+2;
                dtH.Left = btFeclas.Left + btFeclas.Width+2;
                btPeriodo.Left = dtH.Left + dtH.Width + 2;

                this.Height = dtD.Height + 2;
                this.Width = btPeriodo.Left + btPeriodo.Width + 2;
            }
        }

        #endregion

        #region Metodos

        private void ApplyBestSize(Control ctrl,Size size)
        {
            ctrl.Width = size.Width;
            ctrl.Height = size.Height;
        }

        private void SetVisibilidadButtons(PeriodosFecha value,bool visible)
        {
            string captionbtn = value.ToString();

            foreach (EditorButton btn in btPeriodo.Properties.Buttons)
            {
                //Tengo que obtener el boton.
               if (btn.Caption==captionbtn)
               {
                   btn.Visible = visible;
                   break;
               }
            }
        }

        private void VerifyPeriodoMaximo(PeriodosFecha periodoMaximo)
        {
            if ((byte)_periodoCurrent>(byte)_periodoMaximo)
                SetFechasByPeriodo(periodoMaximo);

            switch (periodoMaximo)
            {
                 case PeriodosFecha.Todas:
                    if (_buttonDiaVisible) _dbuttons["Dia"].Visible = true;
                    if (_buttonSemanaVisible) _dbuttons["Semana"].Visible = true;
                    if (_buttonMesVisible) _dbuttons["Mes"].Visible = true;
                    if (_buttonTrimestreVisible) _dbuttons["Trimestre"].Visible = true;
                    if (_buttonCampa�aVisible) _dbuttons["Campa�a"].Visible = true;
                    if (_buttonTodasVisible) _dbuttons["Todas"].Visible = true;
                    break;

                case PeriodosFecha.Trimestre:
                    if (_buttonDiaVisible) _dbuttons["Dia"].Visible = true;
                    if (_buttonSemanaVisible) _dbuttons["Semana"].Visible = true;
                    if (_buttonMesVisible) _dbuttons["Mes"].Visible = true;
                    if (_buttonTrimestreVisible) _dbuttons["Trimestre"].Visible = true;
                    _dbuttons["Campa�a"].Visible = false;
                    _dbuttons["Todas"].Visible = false;
                    break;

                case PeriodosFecha.Mes:
                    if (_buttonDiaVisible) _dbuttons["Dia"].Visible = true;
                    if (_buttonSemanaVisible) _dbuttons["Semana"].Visible = true;
                    if (_buttonMesVisible) _dbuttons["Mes"].Visible = true;
                    _dbuttons["Trimestre"].Visible = false;
                    _dbuttons["Campa�a"].Visible = false;
                    _dbuttons["Todas"].Visible = false;
                    break;

                case PeriodosFecha.Semana:
                    if (_buttonDiaVisible) _dbuttons["Dia"].Visible = true;
                    if (_buttonSemanaVisible) _dbuttons["Semana"].Visible = true;
                    _dbuttons["Mes"].Visible = true;
                    _dbuttons["Trimestre"].Visible = false;
                    _dbuttons["Campa�a"].Visible = false;
                    _dbuttons["Todas"].Visible = false;
                    break;

                case PeriodosFecha.Dia:
                    if (_buttonDiaVisible) _dbuttons["Dia"].Visible = true;
                    _dbuttons["Semana"].Visible = false;
                    _dbuttons["Mes"].Visible = false;
                    _dbuttons["Trimestre"].Visible = false;
                    _dbuttons["Campa�a"].Visible = false;
                    _dbuttons["Todas"].Visible = false;
                    break;
            }

        }

        private void SetFechasByPeriodo(PeriodosFecha periodo)
        {

            _desdeOldValue = dtD.EditValue;
            _hastaOldValue = dtH.EditValue;

            switch (periodo)
            {
                case PeriodosFecha.Dia:
                    dtD.EditValue = DateTime.Now.Date;
                    dtH.EditValue = dtD.EditValue.Value.AddMinutes(1439);
                    break;

                case PeriodosFecha.Mes:
                    dtD.EditValue = string.Format("{0}/{1}/{2}", "01", DateTime.Now.Month, DateTime.Now.Year).ObjectToDateTime();
                    dtH.EditValue =
                        DateAndTime.DateAdd(DateInterval.Day, -1,
                                            DateAndTime.DateAdd(DateInterval.Month, 1, string.Format("{0}/{1}/{2}", "01", DateTime.Now.Month, DateTime.Now.Year).ObjectToDateTime().Value)).AddMinutes(1439);
                    break;

                case PeriodosFecha.Semana:
                     dtD.EditValue = DateTime.Now.AddDays(-(int)(DateTime.Now.DayOfWeek-1));
                     dtH.EditValue = dtD.EditValue.Value.AddDays(6).AddMinutes(1439);
                     break;

                case PeriodosFecha.Trimestre:
                    switch (DateTime.Now.Month)
                    {
                        case 1:
                        case 2:
                        case 3:
                            dtD.EditValue = string.Format("{0}/{1}/{2}", "01", "01", DateTime.Now.Year).ObjectToDateTime();
                            dtH.EditValue = string.Format("{0}/{1}/{2}", "31", "03", DateTime.Now.Year).ObjectToDateTime().Value.AddMinutes(1439);
                            break;

                        case 4:
                        case 5:
                        case 6:
                            dtD.EditValue = string.Format("{0}/{1}/{2}", "01", "04", DateTime.Now.Year).ObjectToDateTime();
                            dtH.EditValue = string.Format("{0}/{1}/{2}", "30", "06", DateTime.Now.Year).ObjectToDateTime().Value.AddMinutes(1439);
                            break;

                        case 7:
                        case 8:
                        case 9:
                            dtD.EditValue = string.Format("{0}/{1}/{2}", "01", "07", DateTime.Now.Year).ObjectToDateTime();
                            dtH.EditValue = string.Format("{0}/{1}/{2}", "30", "09", DateTime.Now.Year).ObjectToDateTime().Value.AddMinutes(1439);
                            break;

                        case 10:
                        case 11:
                        case 12:
                            dtD.EditValue = string.Format("{0}/{1}/{2}", "01", "10", DateTime.Now.Year).ObjectToDateTime();
                            dtH.EditValue = string.Format("{0}/{1}/{2}", "31", "12", DateTime.Now.Year).ObjectToDateTime().Value.AddMinutes(1439);
                            break;
                            
                    }
                    break;

                case PeriodosFecha.Todas:
                    dtD.EditValue = null;
                    dtH.EditValue = null;
                    break;
            }
        }

        //private void VerifyRangoPeriodoMaximo()
        //{
        //    if (!(dtD.EditValue.HasValue && dtH.EditValue.HasValue)) return;

        //    switch (_periodoMaximo)
        //    {
        //        case PeriodosFecha.Campa�a:
        //            if (dtD.EditValue.Value.AddDays(365)>dtH.EditValue)
        //                btPeriodo_ButtonClick(null,new ButtonPressedEventArgs(_dbuttons["Campa�a"]));
        //            break;

        //        case PeriodosFecha.Trimestre:
        //            if (dtD.EditValue.Value.AddDays(92)> dtH.EditValue)
        //                btPeriodo_ButtonClick(null, new ButtonPressedEventArgs(_dbuttons["Trimestre"]));
        //            break;

        //        case PeriodosFecha.Mes:
        //            if (dtD.EditValue.Value.AddDays(31) > dtH.EditValue)
        //                btPeriodo_ButtonClick(null, new ButtonPressedEventArgs(_dbuttons["Mes"]));
        //            break;

        //        case PeriodosFecha.Semana:
        //            if (dtD.EditValue.Value.AddDays(7) > dtH.EditValue)
        //                btPeriodo_ButtonClick(null, new ButtonPressedEventArgs(_dbuttons["Semana"]));
        //            break;

        //        case PeriodosFecha.Dia:
        //            if (dtD.EditValue.Value.AddDays(1) > dtH.EditValue)
        //                btPeriodo_ButtonClick(null, new ButtonPressedEventArgs(_dbuttons["Dia"]));
        //            break;
        //    }
        //}

         private bool VerifyRangoPeriodoMaximo(PeriodosFecha periodoVerify)
        {
            if (!(dtD.EditValue.HasValue && dtH.EditValue.HasValue)) 
                return false;

            switch (periodoVerify)
            {
                case PeriodosFecha.Trimestre:
                    if (dtD.EditValue.Value.AddDays(92)> dtH.EditValue)
                    {
                        btPeriodo_ButtonClick(null, new ButtonPressedEventArgs(_dbuttons["Trimestre"]));
                        return true;
                    }
                    break;

                case PeriodosFecha.Mes:
                    if (dtD.EditValue.Value.AddDays(31) > dtH.EditValue)
                    {
                        btPeriodo_ButtonClick(null, new ButtonPressedEventArgs(_dbuttons["Mes"]));
                        return true;
                    }
                    break;

                case PeriodosFecha.Semana:
                    if (dtD.EditValue.Value.AddDays(7) > dtH.EditValue)
                    {
                        btPeriodo_ButtonClick(null, new ButtonPressedEventArgs(_dbuttons["Semana"]));
                        return true;
                    }
                    break;

                case PeriodosFecha.Dia:
                    if (dtD.EditValue.Value.AddDays(1) > dtH.EditValue)
                    {
                        btPeriodo_ButtonClick(null, new ButtonPressedEventArgs(_dbuttons["Dia"]));
                        return true;
                    }
                    break;
            }

            return false;
        }

        public bool IsValido()
        {
            if (dtD.Value == null && dtH.Value == null)
                return true;

            if (dtD.Value == null && dtH.Value != null || dtD.Value != null && dtH.Value == null)
                return false;

            if (dtD.Value.Value > dtH.Value.Value || dtH.Value.Value < dtD.Value.Value)
                return false;

            return true;

        }

        /// <summary>
        /// Agrega un incremento de uno al periodo seleccionado actualmente.
        /// </summary>
        public void ModifyPeriodo(int increment,DateTime? fromDate=null,DateTime? toDate=null)
        {
            _desdeOldValue = dtD.EditValue;
            _hastaOldValue = dtH.EditValue;

            if (fromDate==null && toDate==null)
            {
                switch (PeriodoCurrent)
                {
                    case PeriodosFecha.Dia:
                        dtD.EditValue = DateAndTime.DateAdd(DateInterval.Day, increment, dtD.EditValue.Value);
                        dtH.EditValue = dtD.EditValue.Value.AddMinutes(1439);
                        break;

                    case PeriodosFecha.Semana:
                        dtD.EditValue = dtD.DateTime.AddDays(7 * increment);
                        dtH.EditValue = dtD.DateTime.AddDays(6).AddMinutes(1439);
                        break;

                    case  PeriodosFecha.Mes:
                        dtD.EditValue = DateAndTime.DateAdd(DateInterval.Month, increment, dtD.EditValue.Value);
                        dtH.EditValue = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtD.EditValue.Value)).AddMinutes(1439);
                        break;

                    case PeriodosFecha.Trimestre:
                        dtD.EditValue = DateAndTime.DateAdd(DateInterval.Month, increment*3, dtD.EditValue.Value);
                        dtH.EditValue = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1 * 3, dtD.EditValue.Value)).AddMinutes(1439);
                        break;

                }
            }
            else if (fromDate!=null)
            {
                switch (PeriodoCurrent)
                {
                    case PeriodosFecha.Dia:
                        dtH.EditValue = dtD.EditValue.Value.AddMinutes(1439);
                        break;

                    case PeriodosFecha.Semana:
                        dtH.EditValue = fromDate.Value.AddDays(6).AddMinutes(1439);
                        break;

                    case  PeriodosFecha.Mes:
                        dtH.EditValue = string.Format("{0}/{1}/{2}", "01", fromDate.Value.Month, fromDate.Value.Year).ObjectToDateTime();
                        dtH.EditValue =  DateAndTime.DateAdd(DateInterval.Day, -1,DateAndTime.DateAdd(DateInterval.Month, 1, dtH.EditValue.Value)).AddMinutes(1439);
                        break;

                    case PeriodosFecha.Trimestre:
                        dtH.EditValue = string.Format("{0}/{1}/{2}", "01", fromDate.Value.Month, fromDate.Value.Year).ObjectToDateTime();
                        dtH.EditValue = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 3, dtH.EditValue.Value)).AddMinutes(1439);
                        break;

                }
                
            }
            else if (toDate!=null)
            {
                switch (PeriodoCurrent)
                {
                    case PeriodosFecha.Dia:
                        dtD.EditValue = dtD.EditValue.Value;
                        break;

                    case PeriodosFecha.Semana:
                        dtD.EditValue = toDate.Value.AddDays(-6).AddMinutes(1439);
                        break;

                    case PeriodosFecha.Mes:
                        dtD.EditValue = string.Format("{0}/{1}/{2}", "01", toDate.Value.Month, toDate.Value.Year).ObjectToDateTime().Value.AddMinutes(1439);
                        break;

                    case PeriodosFecha.Trimestre:
                        dtD.EditValue = string.Format("{0}/{1}/{2}", "01", toDate.Value.Month, toDate.Value.Year).ObjectToDateTime().Value.AddMinutes(1439);
                        dtD.EditValue = DateAndTime.DateAdd(DateInterval.Month, -2, dtH.EditValue.Value);
                        break;

                }
            }
        }

        private IntervalDatesChangedEventArgs GenerateIntervalChanged()
        {
            return new IntervalDatesChangedEventArgs(new IntervalDatesInfo(_desdeOldValue, _hastaOldValue), new IntervalDatesInfo(dtD.EditValue, dtH.EditValue));
        }

        #endregion

        #region EventHandlers

        private void btPeriodo_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            btFeclas.Enabled = true;
            dtD.Enabled = true;
            dtH.Enabled = true;
            switch (e.Button.Caption)
            {
                case "Dia":
                    _periodoCurrent = PeriodosFecha.Dia;
                    SetFechasByPeriodo(PeriodosFecha.Dia);
                    break;

                case "Mes":
                    _periodoCurrent=PeriodosFecha.Mes;
                    SetFechasByPeriodo(PeriodosFecha.Mes);
                    break;

                case "Semana":
                    _periodoCurrent = PeriodosFecha.Semana;
                    SetFechasByPeriodo(PeriodosFecha.Semana);
                    break;

                case "Trimestre":
                    _periodoCurrent=PeriodosFecha.Trimestre;
                    SetFechasByPeriodo(PeriodosFecha.Trimestre);
                    break;

                case "Todas":
                    _periodoCurrent = PeriodosFecha.Todas;
                    SetFechasByPeriodo(PeriodosFecha.Todas);
                    btFeclas.Enabled = false;
                    dtD.Enabled = false;
                    dtH.Enabled = false;
                    break;
            }

            OnValueChanged();
        }

        private void Fecha_ValueChanged(object sender, Controls.ChangedEventArgs e)
        {   
            if ((sender as axDateEdit).Name=="dtD")
            {
                VerifyDesdeChanged((DateTime)e.OldValue);
            }
            else if ((sender as axDateEdit).Name=="dtH")
            {
                VerifyHastaChanged((DateTime)e.OldValue);
            }
        }

        private void VerifyHastaChanged(DateTime? oldValue)
        {
            if (dtH.EditValue == null)
            {
                dtH.EditValue = oldValue;
            }
            else if (dtH.EditValue < dtD.EditValue)
            {
                //Hay que poner la fecha hasta, mayor a la desde, y coincidiendo con el final del periodo actual.
                dtD.EditValue = dtH.EditValue.Value.Date;
            }
            OnValueChanged();
        }

        private void VerifyDesdeChanged(DateTime? oldValue)
        {
            if (dtD.EditValue == null)
            {
                dtD.EditValue = oldValue;
                ModifyPeriodo(1);
            }
            else if (dtD.EditValue > dtH.EditValue)
            {
                //Hay que poner la fecha hasta, mayor a la desde, y coincidiendo con el final del periodo actual.
                ModifyPeriodo(0, dtD.EditValue);
            }
            else
            {
                ModifyPeriodo(0);
            }

            //VerifyRangoPeriodoMaximo(_periodoCurrent);
            OnValueChanged();
        }

        private void btFeclas_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Caption)
            {
                case "Left":
                    ModifyPeriodo(-1);
                    OnValueChanged();
                    break;

                case "Right":
                     ModifyPeriodo(1);
                    OnValueChanged();
                    break;
            }
        }

        #endregion

        private void IntervalDatesUC_Load(object sender, EventArgs e)
        {
            this.BackColor=Color.Transparent;
            btPeriodo.BackColor = Color.Transparent;

            btFeclas.BorderStyle = BorderStyles.NoBorder;
            btPeriodo.BorderStyle=BorderStyles.NoBorder;
        }
    }

    public class IntervalDatesInfo
    {
        public DateTime? Desde { get; private set; }
        public DateTime? Hasta   { get; private set; }

        public IntervalDatesInfo(DateTime? desde, DateTime? hasta)
        {
            Desde = desde;
            Hasta = hasta;
        }

        public override string ToString()
        {
            if (Desde != null && Hasta != null)
                return string.Format("{0} -> {1}", Desde.Value.ToString("dd/MM/yy"),Hasta.Value.ToString("dd/MM/yy"));

            return  "no valida => no valida";
        }
    }

    public class IntervalDatesChangedEventArgs : EventArgs
    {
        public IntervalDatesChangedEventArgs(IntervalDatesInfo oldInterval, IntervalDatesInfo newInterval)
        {
            this.OldInterval = oldInterval;
            this.NewInterval = newInterval;
        }

        // Properties
        public IntervalDatesInfo NewInterval { get; set; }
        public IntervalDatesInfo OldInterval { get; protected set; }

        public override string ToString()
        {
            return string.Format("Old:{0} => New:{1}", OldInterval, NewInterval);
        }
    }

}
