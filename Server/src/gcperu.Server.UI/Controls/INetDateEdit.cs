using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.ViewInfo;
using gcperu.Server.Core.Extension;
using Gurock.SmartInspect;

namespace gcperu.Server.UI.Controls
{
    [UserRepositoryItem("axDateEdit")]
    public class RepositoryItemINetDateEdit : RepositoryItemDateEdit
    {
        public const string INetDateEditName = "axDateEdit";
        private IContainer components;

        #region Field

        #endregion

        #region Events

        /// <summary>
        /// Indica que el control se ha inicializado.
        /// </summary>
        public event EventHandler Initialized;

        public virtual void OnInitialized()
        {
            EventHandler handler = Initialized;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        #endregion

        #region Ctor

        public static void RegisterINetDateEdit()
        {
            //Image img = null;
            //try
            //{
            //    img = (Bitmap)Bitmap.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("DevExpress.CustomEditors.CustomEdit.bmp"));
            //}
            //catch
            //{
            //}

            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(INetDateEditName,
                                                                           typeof(axDateEdit),
                                                                           typeof(RepositoryItemINetDateEdit),
                                                                           typeof(DateEditViewInfo),
                                                                           new ButtonEditPainter(), true, null));


            // EditorRegistrationInfo.Default.Editors.Add( new EditorClassInfo( INetDateEditName, typeof( axDateEdit ), typeof( RepositoryItemLookUpEdit ), typeof( LookUpEditViewInfo ), new BaseEditPainter( ), true ) );
        }

        static RepositoryItemINetDateEdit()
        {
            RegisterINetDateEdit();
        }

        public RepositoryItemINetDateEdit()
        {
        }

        #endregion

        #region Propertys

        #region Behavior

        private bool _obligatorio;

        [Category("Behavior"), DefaultValue(false)]
        public bool Obligatorio
        {
            get { return _obligatorio; }
            set { _obligatorio = value; }
        }


        private DateEditMask _dateEditMask= Controls.DateEditMask.ShortDate;

        [Category("Behavior"), DefaultValue(Controls.DateEditMask.ShortDate)]
        public DateEditMask DateEditMask
        {
            get { return _dateEditMask; }
            set
            {
                _dateEditMask = value;
                switch (value)
                {
                    case DateEditMask.None:
                        Mask.EditMask = "";
                        break;
                    case DateEditMask.ShortDate:
                        Mask.EditMask = "dd/MM/yy";
                        _dateEditPartShow=DateEditPartShow.Date;
                        break;
                    case DateEditMask.LongDate:
                        Mask.EditMask = "D";
                        _dateEditPartShow = DateEditPartShow.Date;
                        break;
                    case DateEditMask.ShortTime:
                        Mask.EditMask = "t";
                        _dateEditPartShow = DateEditPartShow.Time;
                        break;
                    case DateEditMask.LongTime:
                        Mask.EditMask = "T";
                        _dateEditPartShow = DateEditPartShow.Time;
                        break;
                    case DateEditMask.ShortDateTime:
                        Mask.EditMask = "dd/MM/yy HH:mm";
                        _dateEditPartShow = DateEditPartShow.Both;
                        break;
                    case DateEditMask.FullDateTime:
                        Mask.EditMask = "F";
                        _dateEditPartShow = DateEditPartShow.Both;
                        break;
                }
            }
        }

        private Controls.DateEditPartShow _dateEditPartShow=DateEditPartShow.Date;

        [Category("Behavior"), DefaultValue(Controls.DateEditPartShow.Date)]
        public Controls.DateEditPartShow DateEditPartShow
        {
            get { return _dateEditPartShow; }
            set { _dateEditPartShow = value; }
        }

        #endregion

        #region Data

        #endregion

        #endregion

        #region Overrides

        protected override RepositoryItem CreateRepositoryItem()
        {
            return base.CreateRepositoryItem();

        }

        public override void EndInit()
        {
            base.EndInit();
            OnInitialized();
        }

        public override string EditorTypeName
        {
            get
            {
                return INetDateEditName;
            }
        }

        public override void Assign(RepositoryItem item)
        {
            BeginUpdate();
            try
            {
                base.Assign(item);
                var source = item as RepositoryItemINetDateEdit;
                if (source == null)
                    return;

                Obligatorio = source.Obligatorio;
                DateEditMask = source.DateEditMask;
                DateEditPartShow = source.DateEditPartShow;
            }
            finally
            {
                EndUpdate();
            }
        }

        #endregion
    }

    //[SiTrace(SessionPolicy = SessionPolicy.TypeName, IncludeArguments = true, IncludeReturnValue = true, IncludeSpecialNames = false)]
    [ToolboxItem(true), DefaultEvent("ValueChanged"), DefaultProperty("Value")]
    public class axDateEdit : DateEdit
    {

        #region Fields

        private bool _loaded;
        private DateTime? _currentValue;
        private bool _editValueChanging;  //Indica que se estan cambiando el valor asignado al control.
        private bool _raisingValueChanged;  //Indica que se est� lanzando el evento ValueChanged y ValueChanging.
        private bool _comboKeyPressed;  //Indica si se ha presionado una tecla sobre el combo.

        #endregion

        #region Cotr
        
        static axDateEdit()
        {
            RepositoryItemINetDateEdit.RegisterINetDateEdit();
        }



        public axDateEdit()
        {

#if (!SkipPostSharp)
            SiAuto.Si.Connections = "tcp()";
            SiAuto.Si.Level = Level.Debug;
            SiAuto.Si.DefaultLevel = Level.Debug;
            SiAuto.Si.Enabled = true;      
#endif

            this.LostFocus += new EventHandler(INetDateEdit_LostFocus);
            this.Enter += new EventHandler(INetDateEdit_Enter);
            this.KeyDown += new KeyEventHandler(INetDateEdit_KeyDown);
            this.KeyPress += new KeyPressEventHandler(INetDateEdit_KeyPress);
            //Properties.Initialized += new EventHandler(Repository_Initialized);

            //InitializeProperties();
        }

        void INetDateEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) || e.KeyChar == 8)
                _comboKeyPressed = true;
        }

        void Repository_Initialized(object sender, EventArgs e)
        {
            _loaded = true;
            ActionEnterLeave(false, true);
            Properties.AppearanceDisabled.ForeColor = Color.Black;
        }

        void INetDateEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Delete)
            {
                e.Handled = true;
                SendKeys.Send("^0");
            }
            else if (e.KeyCode==Keys.Down && e.Control)
            {
                base.ShowPopup();
            }
            else if (e.KeyCode==Keys.Enter && e.Shift)
            {
               SendKeys.Send("+{TAB}");
            }
        }

        #endregion

        #region eventos

        public event EventHandler<ChangedEventArgs> ValueChanged;

        public virtual void OnValueChanged(ChangedEventArgs changedEventArgs)
        {
            EventHandler<ChangedEventArgs> handler = ValueChanged;
            if (handler != null) handler(this, changedEventArgs);
        }

        public event EventHandler<Controls.ChangingEventArgs> ValueChanging;

        public virtual void OnValueChanging(ChangingEventArgs e)
        {
            EventHandler<ChangingEventArgs> handler = ValueChanging;
            if (handler != null) handler(this, e);
        }

        #endregion

        #region Properties

        [Category("Behavior"), DefaultValue(false)]
        public bool Obligatorio
        {
            get { return Properties.Obligatorio; }
            set { Properties.Obligatorio = value; SetObligatorioStyle(value); }
        }

        [Category("Behavior"), DefaultValue(DateEditMask.ShortDate)]
        public DateEditMask DateEditMask
        {
            get { return Properties.DateEditMask; }
            set { Properties.DateEditMask = value;}
        }

        [Category("Behavior"), DefaultValue(DateEditPartShow.Date)]
        public DateEditPartShow DateEditPartShow
        {
            get { return Properties.DateEditPartShow; }
            set { Properties.DateEditPartShow = value; }
        }

        //[Category("Behavior"), DefaultValue("")]
        //public string EditMask
        //{
        //    get { return Properties.Mask.EditMask; }
        //    set
        //    {
        //        DateEditMask=DateEditMask.None;
        //        Properties.Mask.EditMask = value;
        //    }
        //}

        #endregion


        #region EventHandler

        protected override void OnEditValueChanged()
        {
            base.OnEditValueChanged();
             if (Properties.IsDesignMode || _editValueChanging || _raisingValueChanged) return;
             if (_comboKeyPressed) return;

             try
             {
                 //Evento se lanza ante cualquier cambio en el Combo, sin necesidad de perder el foco.
                 bool cancelar = false;
                 RaiseEventValueChanging(EditValue, ref cancelar);
                 if (cancelar)
                 {
                     _comboKeyPressed = false;
                     return;
                 }

                 DateTime? _currenValueOld = _currentValue;
                 _currentValue = EditValue;
                 //Ha cambiado el valor, se lanza desde aqui.
                 OnValueChanged(new ChangedEventArgs(_currenValueOld, _currentValue));

             }
             catch (System.Exception e)
             {
                 Console.WriteLine(new System.Exception("axDateEdit.OnEditValueChanged. Lanzando evento ValueChanged", e).ToString());
             }
             finally
             {
                 _raisingValueChanged = false;
             }


        }

        void INetDateEdit_Enter(object sender, EventArgs e)
        {
            if (!Properties.IsDesignMode)
            {
                ActionEnterLeave(true);

                if (Properties.Mask.MaskType == MaskType.Numeric)
                    SelectAll();
                else
                    SelectionStart = base.Text.Length + 1;
            }
        }

        void INetDateEdit_LostFocus(object sender, EventArgs e)
        {
            if (!Properties.IsDesignMode)
            {
                if (_raisingValueChanged) return;

                ActionEnterLeave(false);
                if (!_loaded) return;

                if (ValorHaCambiado())
                {
                    try
                    {
                        //Evento se lanza ante cualquier cambio en el Combo, sin necesidad de perder el foco.
                        bool cancelar = false;
                        RaiseEventValueChanging(EditValue, ref cancelar);
                        if (cancelar)
                        {
                            _comboKeyPressed = false;
                            return;
                        }

                        DateTime? _currenValueOld = _currentValue;
                        _currentValue = EditValue;
                        //Ha cambiado el valor, se lanza desde aqui.
                        OnValueChanged(new ChangedEventArgs(_currenValueOld, _currentValue));
                    }
                    catch (System.Exception ex)
                    {
                        Console.WriteLine(new System.Exception("axDateEdit.LostFocus. Lanzando evento ValueChanged", ex).ToString());
                    }
                    finally
                    {
                        _raisingValueChanged = false;
                    }
                }
            }
        }

        private bool ValorHaCambiado()
        {
            return _currentValue != EditValue;
        }

        #endregion

        #region Overrides

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Properties.Initialized -= Repository_Initialized;
            }
            base.Dispose(disposing);
        }

        protected override void InitializeDefaultProperties()
        {
            base.InitializeDefaultProperties();
            InitializeProperties();
        }

        public override string EditorTypeName
        {
            get
            {
                return RepositoryItemINetDateEdit.INetDateEditName;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RepositoryItemINetDateEdit Properties
        {
            get
            {
                return base.Properties as RepositoryItemINetDateEdit;
            }
        }

        #endregion

        #region Privadas

        private void RaiseEventValueChanging(DateTime? value, ref bool cancel)
        {
            cancel = false;
            //Hay cambios, y hay que establecerlo en el control Lookup.
            if (ValueChanging != null)
            {
                try
                {
                    //Hay algo que controla este evento, por tanto lo lanzamos.
                    _raisingValueChanged = true;
                    var eargs = new ChangingEventArgs(_currentValue, EditValue);

                    OnValueChanging(eargs);
                    if (eargs.Cancel)
                    {
                        _editValueChanging = true;
                        base.EditValue = _currentValue;
                        _editValueChanging = false;

                        cancel = true;
                        return;
                    }
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(new System.Exception("ERROR INetTextEdit.RaiseEventValueChanging.",e).ToString());
                }
                finally
                {
                    _raisingValueChanged = false;
                }
            }
        }

        private void InitRuntime()
        {
            if (Properties.IsDesignMode)
                return;

            _loaded = true;
        }

        private void InitializeProperties()
        {
            EnterMoveNextControl = true;
            base.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnValidation;

            if (Properties != null)
            {
                Properties.Appearance.TextOptions.HAlignment = HorzAlignment.Far;
                Properties.Mask.UseMaskAsDisplayFormat = true;
                Properties.Mask.MaskType = MaskType.DateTimeAdvancingCaret;
                Properties.DateEditMask = DateEditMask.ShortDate;
            }
            
        }

        private void SetObligatorioStyle(bool value)
        {
            if (value)
            {
                BorderStyle = BorderStyles.Simple;
                Properties.Appearance.BorderColor = Color.Red;
            }
            else
            {
                BorderStyle = BorderStyles.Default;
                Properties.Appearance.BorderColor = Color.Empty;
            }
        }

        private void ActionEnterLeave(bool focused, bool initialized = false)
        {
            if (focused)
            {
                base.BackColor = AppearanceDefault.Control.BackColor;
            }
            else
            {
                base.BackColor = Color.Empty;
            }
        }

        #endregion

        #region Publicas

        [Bindable(true), DefaultValue(typeof(DateTime?)), Category("Data"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime? Value
        {
            get { return EditValue; }
            set { EditValue = value; }
        }

        [DefaultValue(typeof(DateTime?),"null"), Bindable(true), Category("Data"),DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new DateTime? EditValue
        {
            get
            {
                if (base.EditValue == null) return null;

                System.DateTime? valor = base.EditValue.ObjectToDateTime();
                switch (Properties.DateEditPartShow)
                {
                    case DateEditPartShow.Date:
                        return valor.HasValue ? valor.Value.Date : (DateTime?) null;

                    case DateEditPartShow.Time:
                        return valor.HasValue ? valor.Value.TimeOfDay.ObjectToDateTime() : (DateTime?)null;

                    default:
                    case DateEditPartShow.Both:
                        return valor;
                }
            }

            set
            {
                if (!IsDesignMode && !_loaded)
                    InitRuntime();

                _editValueChanging = true;
                base.EditValue = value;
                _currentValue = value;
                _editValueChanging = false;
            }
        }

        public override string Text
        {
            get { return EditValue.ToString(); }
        }


        #endregion

     }
}