﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Registrator;
using System.Linq;

namespace gcperu.Server.UI.Controls
{

     [Description("Combines controls into a group and allows a caption to be displayed along any group edge."), DefaultEvent("CheckChanged")]
    [DefaultBindingProperty("Checked")]
    public class DxGroupControlCheck : GroupControl
    {
        private DevExpress.XtraEditors.CheckEdit _ck;
        private Rectangle _captionNewBounds;
        private string _caption;

        private bool _raiseevent;
        private List<Control> _listControlsNoActive;

        public DxGroupControlCheck()
            : base()
        {
            //_panel=new PanelControl();
            _ck = new CheckEdit();

            this.Controls.Add(_ck);
            //this.Controls.Add(_panel);

            _ck.AutoSize = true;
            _ck.Text = "";
            _ck.Checked = true; //Valor por defecto
            _raiseevent = true;

            _listControlsNoActive= new List<Control>();

            _ck.CheckedChanged += new EventHandler(CheckedChanged);
            this.ControlAdded += new ControlEventHandler(INetGroupControlCheck_ControlAdded);
            this.CustomDrawCaption+=new GroupCaptionCustomDrawEventHandler(INetGroupControlCheck_CustomDrawCaption);
        }

        void INetGroupControlCheck_ControlAdded(object sender, ControlEventArgs e)
        {
            if (e.Control is System.Windows.Forms.Label)
                return;

            if (_ck.Equals(e.Control))
                return;

            if (e.Control.Enabled == false)
                _listControlsNoActive.Add(e.Control);
        }

        protected override void InitElements()
        {
            base.InitElements();
        }

        public event EventHandler<EventArgs> CheckChanged;

        public void OnCheckChanged(EventArgs e)
        {
            EventHandler<EventArgs> handler = CheckChanged;
            if (handler != null) handler(this, e);
        }

        [Browsable(true),Bindable(true),DefaultValue(true)]
        public bool Checked
        {
            get { return _ck.Checked; }
            set
            {
                bool? valuebefore=false;
                if (!DesignMode)
                    valuebefore = _ck.Checked;

                _raiseevent = false;
                _ck.Checked = value;
                if (!DesignMode && valuebefore != null && value != valuebefore)
                    CheckedChanged(null, null);

                _raiseevent = true;

            }
        }

        [Browsable(true), Bindable(true), DefaultValue(true)]
        public bool CheckVisible
        {
            get { return _ck.Visible; }
            set { _ck.Visible = value; }
        }

        [Browsable(true), Bindable(true), DefaultValue(true)]
        public bool CheckEnabled
        {
            get { return _ck.Enabled; }
            set { _ck.Enabled = value; }
        }

        void INetGroupControlCheck_CustomDrawCaption(object sender, GroupCaptionCustomDrawEventArgs e)
        {
            CalculeCheckLocation();
            e.Graphics.DrawString(e.Info.Caption, e.Info.AppearanceCaption.Font, e.Info.AppearanceCaption.GetForeBrush(e.Cache), new Rectangle(_ck.Left + _ck.PreferredSize.Width, this.ViewInfo.TextBounds.Top, this.ViewInfo.CaptionBounds.Width, this.ViewInfo.CaptionBounds.Height));
            e.Handled = true;
        }

        protected override void Dispose(bool disposing)
        {
            _ck.CheckedChanged += new EventHandler(CheckedChanged);
            base.Dispose(disposing);
        }

        private void CalculeCheckLocation()
        {
            _ck.Location = new Point(1, ((this.ViewInfo.CaptionBounds.Height/2) - (_ck.Height/2)));
        }

        void CheckedChanged(object sender, EventArgs e)
        {

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is System.Windows.Forms.Label)
                    continue;

                if (!_ck.Equals(ctrl))
                {
                    if (_ck.Checked)
                    {
                        if (!JumpThisControl(ctrl))
                            ctrl.Enabled = _ck.Checked;
                    }
                    else
                    {
                        if (!JumpThisControl(ctrl))
                            ctrl.Enabled = _ck.Checked;
                    }
                }
            }

            if (_raiseevent)
                OnCheckChanged(EventArgs.Empty);
        }

        private bool JumpThisControl(Control ctrl)
        {
            return _listControlsNoActive != null && _listControlsNoActive.Contains(ctrl);
        }

    }

}
