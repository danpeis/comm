using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace gcperu.Server.UI.Controls
{
    public class PropertyBag : Dictionary<string, object>
    {

    }

    public class ChangingEventArgs : CancelEventArgs
    {
        // Methods
        public ChangingEventArgs(object oldValue, object newValue)
            : this(oldValue, newValue, false)
        {
        }

        public ChangingEventArgs(object oldValue, object newValue, bool cancel)
            : base(cancel)
        {
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }

        // Properties
        public object NewValue { get; set; }
        public object OldValue { get; protected set; }

        public override string ToString()
        {
            return string.Format("Old:{0} => New:{1}", OldValue, NewValue);
        }
    }

    public class NewValueEventArgs : EventArgs
    {
        private PropertyBag _valueBag;

        public NewValueEventArgs()
        {
        }

        public NewValueEventArgs(PropertyBag valueBag)
        {
            _valueBag = valueBag;
        }

        public PropertyBag ValueBag
        {
            get { return _valueBag ?? (_valueBag = new PropertyBag()); }
            private set { _valueBag = value; }
        }

    }

    public class ChangedEventArgs : EventArgs
    {
        public ChangedEventArgs(object oldValue, object newValue)
        {
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }

        // Properties
        public object NewValue { get; set; }
        public object OldValue { get; protected set; }

        public override string ToString()
        {
            return string.Format("Old:{0} => New:{1}", OldValue, NewValue);
        }
    }
}