﻿namespace gcperu.Server.UI.Controls
{
    public enum DateEditMask
    {
        None,
        LongDate,
        ShortDate,
        LongTime,
        ShortTime,
        ShortDateTime,
        FullDateTime
    }

    public enum TimeEditMask
    {
        None,
        HourMinSec,
        HourMin,
        Hour
    }

    public enum DateEditPartShow
    {
        Date,
        Time,
        Both
    }
}