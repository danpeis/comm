using System.Linq;
using System.Threading;
using System.Windows.Forms;
using gcperu.Server.UI;
using gitspt.global.Core.Exceptions;
using gitspt.global.Core.Log;
using Microsoft.SqlServer.MessageBox;

namespace gcperu.Server.UI.Exception
{

	public class ThreadExceptionHandler
	{

		public void ApplicationUnhandledException(object sender, ThreadExceptionEventArgs e)
		{

			try
			{

				DialogResult result = ShowThreadExceptionDialog(e.Exception);

				if (result == DialogResult.Abort)
				{
					//modInicio.InitProcesosFinalizacion();
					Application.Exit();
				}

			}
			catch (System.Exception ex)
			{

				try
				{
					MessageBox.Show(string.Format("ERROR FATAL\\n {0}", ex.Message), "Error fatal", MessageBoxButtons.OK, MessageBoxIcon.Stop);

				}
				finally
				{
					//modInicio.InitProcesosFinalizacion();
					Application.Exit();
				}

			}

		}

		private DialogResult ShowThreadExceptionDialog(System.Exception ex)
		{

			string errMsg = "Error no controlado:" + System.Environment.NewLine + System.Environment.NewLine + ex.Message + System.Environment.NewLine + System.Environment.NewLine + ex.GetType().ToString() + System.Environment.NewLine + System.Environment.NewLine + "Stack Trace:" + System.Environment.NewLine + ex.StackTrace;

			ManejarExceptions.ManejarException(ex, ExceptionErrorLevel.Critical, false, false); //Logamos la excepcion pero no mostramos el mensaje
			return MessageBox.Show(errMsg, "Error de Aplicación", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);

		}

	}

	
	public static class ManejarExceptions
	{

	    #region ManejarException Overloads

	    public static void ManejarException(System.Exception ex,ExceptionErrorLevel Severity, bool ReThrow)
	    {
	        ManejarException(ex, Severity, ReThrow, true);
	    }

	    public static void ManejarException(System.Exception ex, ExceptionErrorLevel Severity)
	    {
	        ManejarException(ex, Severity, false, true);
	    }

	    public static void ManejarException(System.Exception ex)
	    {
	        ManejarException(ex, ExceptionErrorLevel.Critical, false, true);
	    }

	    public static void ManejarException(System.Exception ex, ExceptionErrorLevel Severity, bool ReThrow, bool ShowMensaje)
	    {

	        if (ShowMensaje)
	        {
	            ShowMensajeBox(ex, Severity);
	        }

	        TraceLog.LogException(ex);

	    }


	    public static void ManejarException(System.Exception ex, string Policy, ExceptionErrorLevel Severity, bool ReThrow)
	    {
	        ManejarException(ex, Policy, Severity, ReThrow, true);
	    }

	    public static void ManejarException(System.Exception ex, string Policy, ExceptionErrorLevel Severity)
	    {
	        ManejarException(ex, Policy, Severity, false, true);
	    }

	    public static void ManejarException(System.Exception ex, string Policy)
	    {
	        ManejarException(ex, Policy, ExceptionErrorLevel.Critical, false, true);
	    }

	    public static void ManejarException(System.Exception ex, string Policy, ExceptionErrorLevel Severity, bool ReThrow, bool ShowMensaje)
	    {
	        if (ShowMensaje)
	        {
	            ShowMensajeBox(ex, Severity);
	        }

	        TraceLog.LogException(ex);

	    }

	    #endregion

        private static bool ShowMensajeBox(System.Exception ex, ExceptionErrorLevel Severity)
		{
			ExceptionMessageBoxSymbol _symbolIco = ExceptionMessageBoxSymbol.Information;


            if (Severity == ExceptionErrorLevel.Information)
			{
				_symbolIco = ExceptionMessageBoxSymbol.Information;
			}
            else if (Severity == ExceptionErrorLevel.Fatal)
			{
				_symbolIco = ExceptionMessageBoxSymbol.Error;
			}
            else if (Severity == ExceptionErrorLevel.Critical)
			{
				_symbolIco = ExceptionMessageBoxSymbol.Stop;
			}

			var box = new ExceptionMessageBox(ex, ExceptionMessageBoxButtons.OK, _symbolIco);
			box.Caption = "GITS. Comunica Server";
			try
			{
				box.Show(GetMainForm());
			}
			catch (System.Exception ex1)
			{
			}

			return false;
		}

		private static Form GetMainForm()
		{
			try
			{
			    return (from Form f in Application.OpenForms.OfType<Mainfm>() select f).Single();

			}
			catch (System.Exception ex)
			{
				return null;
			}

		}

	}





}
