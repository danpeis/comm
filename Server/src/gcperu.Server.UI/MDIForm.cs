﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using gcperu.Server.UI;

namespace gcperu.Server.UI
{
    
    public partial class MDIForm : XtraForm
    {
        private static MDIForm _instance;

        private MDIForm()
        {
            InitializeComponent();

            _instance = this;
        }

        private void MDIForm_Load(object sender, EventArgs e)
        {

            this.Text = string.Format("{0} - [{1}]", this.Text, System.Windows.Forms.Application.ProductVersion.ToString());

            //Mostrarmos el formulario principal, y que no se podrá cerrar.
            var fmain = new Mainfm();
            fmain.MdiParent = this;
            fmain.Show();

        }

        public static MDIForm Create()
        {
            if (_instance==null)
            {
                _instance = new MDIForm();
            }

            return _instance;
        }

        public static MDIForm Instance
        {
            get { return _instance; }
        }
    }
}
