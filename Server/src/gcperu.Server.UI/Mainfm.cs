﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reactive.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Localization.Internal;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using gcperu.Contract;
using gcperu.Server.Core;
using gcperu.Server.Core.Agents;
using gcperu.Server.Core.DAL.GC;
using gcperu.Server.Core.DAL.GT;
using gcperu.Server.Core.Exceptions;
using gcperu.Server.Core.Extension;
using gcperu.Server.Core.InterComm;
using gcperu.Server.Core.InterComm.GComunica.Core.IPC;
using gcperu.Server.UI.Paneles.Forms.Internal;
using gcperu.Server.UI.Xtra;
using gcperu.Server.UI;
using gcperu.Server.UI.Helper;
using gcperu.Server.UI.Paneles.Forms;
using gcperu.Server.UI.Properties;
using gitspt.global.Core.Extension;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;
using GCPeru.Server.Core.Settings;
using Microsoft.VisualBasic;
using ServiceStack.Text;
using ExceptionErrorLevel = gitspt.global.Core.Exceptions.ExceptionErrorLevel;
using ManejarExceptions = gcperu.Server.UI.Exception.ManejarExceptions;
using Timer = System.Windows.Forms.Timer;

namespace gcperu.Server.UI
{

    public partial class Mainfm : Form
    {
    
        private const string layoutversion = "0.01";
        private ISSTraceLog _log;

        private enum FiltroStatusBarType
        {
            Todos,
            Activos,
        }

        #region Fields

        /// <summary>
        /// Indica si está instancia se ejecuta a traves del Servicio.
        /// En modo servicio, este es el encargado de gestionar todas las conexiones.
        /// </summary>
        private bool _modoServicio = true;  

        /// <summary>
        /// Canal de comunicacion con el Centinela. Utiliza comuniacion interproces a traves de WCF.
        /// </summary>
        private IDisposable _ipcChannelListenerSubscription;
        private IDisposableObserver<MessageIPCinfo> _ipcChannelSender;

        /// <summary>
        /// Controlador del ServicioWindows.
        /// </summary>
        private WServiceManager _wsmanager;

        /// <summary>
        /// Coleccion de TErminales. Utilizada para mostrarlos en el Grid.
        /// </summary>
        private TerminalStateCol _listaTerm;
        /// <summary>
        /// Timer, para auto-actualizar el Grid.
        /// </summary>
        private Timer _tmrRefreshData;

        private XLayoutCollection _xListStoreLayout;

        private bool _procesarCheck;
        private FiltroStatusBarType _filtroStatusBar =FiltroStatusBarType.Activos;

        private TerminalStateQ _tsQ;

        #endregion

        #region Ctor

        public Mainfm()
        {
            //Gestion.GlobalEventsDomain.SetUIThreadEvents();

            InitializeComponent();

            _log = Ioc.Container.Get<ISSTraceLog>();

            OpenIntercommChannel();

            //controlador del Servicio.
#if (!ExecuteServer)

            //Configuramos las conexiones.
            //InitializationProcess.InitializeExternos();

            _log.LogInformation("Mainform. Ctor. Ejecución en Modo solo INTERFAZ. SERVICIO SE ENCARGA DE TODO.");

            _wsmanager = new WServiceManager(UI.Contantes.WSERVICE_GCSERVER1);

            _procesarCheck = false;
            _modoServicio = true;

            _log.LogInformation(string.Format("Mainform. Ctor. ESTADO DEL SERVICIO={0}",_wsmanager.Status.ToString()));

            if (_wsmanager.Status == 0)
            {
                barControlService.Enabled = false;
                barControlService.Caption = "Servicio no encontrado";
                barControlListen.Enabled = false;
                barControlListen.Caption = "Servicio no encontrado";
                _log.LogInformation(string.Format("Mainform. Ctor. SERVICIO {0} NO EXISTE.",UI.Contantes.WSERVICE_GCSERVER1));

                SetStatusService(false);
                SetStatusListen(false);
            }
            else if (_wsmanager.Status == ServiceControllerStatus.Running)
            {
                barControlService.ImageIndex = 0;
                barControlService.Caption = "Detener Servicio";
                barControlListen.Caption = "Detener Conexiones";
                
                RequestWSWorkerStatus();
            }
            else if (_wsmanager.Status == ServiceControllerStatus.Stopped)
            {
                barControlService.ImageIndex = 1;
                barControlService.Caption = "Iniciar Servicio";
                barControlListen.Caption = "Permitir Conexiones";

                SetStatusListen(false);
            }

#else
            ExecuteServer();
#endif

            //Obtenemos la lista de Terminal State.
            _listaTerm= new TerminalStateCol();
#if (Load20)
            ObtenerStatusTerminales(20);
#else
            ObtenerStatusTerminales();
#endif

            _log.LogInformation(string.Format("Mainform. Ctor. Terminales obtenidos. Nº={0}", _listaTerm.Count));

            try
            {
                //_listaTerm.UpdateTerminalStateVehiculoInfoFromGITS();
                _log.LogInformation(string.Format("Mainform. Ctor. Terminales Actualizados Información del Vehiculo desde el GITS."));
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(ex,ExceptionErrorLevel.Error);
            }

            bsTerm.DataSource = _listaTerm;

            ConfigurarInterfaz();

            //Layouts.
            _xListStoreLayout = new XLayoutCollection();
            _xListStoreLayout.Add(new XLayoutGrid(dbgTerm, layoutversion));
            _xListStoreLayout.RestoreLayout();
            _log.LogInformation("Mainform. Ctor. Layuout grid restaurado.");

            //Arrancar el Timer
            StartTimer();

            FillInfoStatusBar();

            dbgTermView.BestFitColumns();

            this.WindowState = FormWindowState.Maximized;
            _procesarCheck = true;

            this.Text = "Consola de Gestión";
        }

        #endregion



        #region Privadas

        #region Interprocess Communications

        private void OpenIntercommChannel()
        {
            //Se suscribe y escucha el canal
            _ipcChannelListenerSubscription = InterProcessAgent.WSWorkerMessageListenerObservable.Subscribe(
                next =>
                {
                    if (next.Sender == IpcChannelSenderTypes.ConsoleUI)
                        return;

                    _log.LogDebug(MessageIPCinfo.Logcolor, string.Format("IPC: Message Receive:\n\n{0}", next.Dump()));
                },
                ex =>
                {
                    _log.LogException(MessageIPCinfo.Logcolor, ex);
                });

            _ipcChannelSender = InterProcessAgent.WSWorkerBroadcastObserver;



            //TODO: INTERPROCESS. CORREGIR E IMPLEMENTAR LA RECEPCIÓN DE MENSAJES.
            //SetStatusListen(GetStatusListen());
        }

        private void IntercommProcessMessage(MessageIPCinfo msg)
        {
            switch (msg.Tipo)
            {
               case MessagesIPCTypesEnum.SocketStatus:

                    if (msg.Data == null || !(msg.Data is SocketstatusMessageIpcData))
                        return;
                    SetStatusListen(((SocketstatusMessageIpcData) msg.Data).Listening);

                    break;
            }
        }

        private void RequestWSWorkerStatus()
        {
            var msg = new MessageIPCinfo(IpcChannelSenderTypes.ConsoleUI, MessagesIPCTypesEnum.SocketStatus, null);
            _ipcChannelSender.Send(msg, () => _log.LogDebug(MessageIPCinfo.Logcolor, string.Format("IPC:Message Send.\n{0}", msg.Dump())));
        }


        private void CloseIntercommChannel()
        {
            if (_ipcChannelListenerSubscription != null)
                _ipcChannelListenerSubscription.Dispose();

            if (_ipcChannelSender != null)
                _ipcChannelSender.Dispose();

            SetStatusListen(false);
        }

        #endregion


        private bool StartService()
        {
            if (_wsmanager.Start())
            {
                //Conectamos con Wcfhost.
                Thread.Sleep(2000);
                OpenIntercommChannel();
                return true;
            }
            return false;

        }

        private bool StopService()
        {
            if (_wsmanager.Stop())
            {
                CloseIntercommChannel();
                return true;
            }
            return false;
        }

        private void SetStatusService(bool started)
        {
            if (started)
            {
                barControlService.ImageIndex = 0;
                barControlService.Caption = "Detener Servicio";
                barControlListen.Enabled = true;

                RequestWSWorkerStatus();
            }
            else
            {
                barControlService.ImageIndex = 1;
                barControlService.Caption = "Iniciar Servicio";

                RequestWSWorkerStatus();
                barControlListen.Enabled = false;
            }
        }

        private void SetStatusListen(bool Listening)
        {
            _procesarCheck = false;
            if (Listening)
            {
                barFootInfo.ImageIndex = 9;
                barControlListen.ImageIndex = 3;
                barControlListen.Caption = "Detener Conexiones";
                barControlListen.Enabled = true;
            }
            else
            {
                barFootInfo.ImageIndex = 10;
                barControlListen.ImageIndex = 4;
                barControlListen.Caption = "Permitir Conexiones";
                barControlListen.Enabled = true;
            }
            _procesarCheck = true;
        }

        private bool RefreshData()
        {
            try
            {
                barFootInfo.Caption = "Refrescando datos ...";
                Application.DoEvents();

                ObtenerStatusTerminales();

                bsTerm.DataSource = _listaTerm;

                //dbgTerm.DataSource = _listaTerm;
                //dbgTerm.RefreshDataSource();
                return true;
            }
            catch (System.Exception _exception)
            {
                MessageBox.Show(_exception.ToString());
                return false;
            }
            finally
            {
                barFootInfo.Caption = "";
                FillInfoStatusBar();
                Application.DoEvents();
            }
        }

        private void ObtenerStatusTerminales(int top=0)
        {
            try
            {
                if (_tsQ==null)
                {
                    var perQ = new PersonalQ("per");
                    var grpQ = new GrupoTerminalQ("grt");
                    var empQ = new EmpresasQ(("emp"));
                    var tcnfQ = new TerminalConfigurationQ("tconf");
                    var tupQ = new TerminalSelectiveSoftwareUpdateQ("tsupd");

                    _tsQ=new TerminalStateQ("ts");

                    if (top > 0)
                        _tsQ.es.Top = top;

                    _tsQ.SelectAll().Select(perQ.PerNombreCompleto.As("ConductorNombre"), empQ.EmpNombre.As("EmpresaNombre"),grpQ.EnActivo.As("GrupoActivo"), grpQ.Nombre.As("GrupoNombre"),
                        
                        _tsQ.TerminalConnectedFrom.Case()
                        .When(_tsQ.TerminalConnectedFrom.IsNull()).Then(0)
                        .Else(1)
                        .End().As("EstadoConexion"),

                        tcnfQ.TerminalID.Case()
                        .When(tcnfQ.TerminalID.IsNull()).Then(0)
                        .Else(1)
                        .End().As("HasConfiguration"),

                        tupQ.TerminalID.Case()
                        .When(tupQ.TerminalID.IsNull()).Then(0)
                        .Else(1)
                        .End().As("InUpdateSelective")
                        );

                    //,"<IIF(tconf.TerminalID is null,0,1) as HasConfiguration>","<IIF(tsupd.TerminalID is null,0,1) as InUpdateSelective>"

                    _tsQ.LeftJoin(perQ).On(_tsQ.PersonalID == perQ.PerCodPK);
                    _tsQ.LeftJoin(grpQ).On(_tsQ.GrupoID == grpQ.Id);
                    _tsQ.LeftJoin(empQ).On(_tsQ.EmpresaID == empQ.EmpCodPK);
                    _tsQ.LeftJoin(tcnfQ).On(_tsQ.TerminalID == tcnfQ.TerminalID);
                    _tsQ.LeftJoin(tupQ).On(_tsQ.TerminalID == tupQ.TerminalID);
                }

                _listaTerm.Load(_tsQ);
            }
            catch (System.Exception ex)
            {
                _log.LogException(string.Format("ERROR REFRESCAR INFO TERMINALSTATE.\nQuery:{0}", _tsQ.es.LastQuery),ex);
            }
        }

        private System.Drawing.Color GetColorByFecha(DateTime? Fecha,Color actualcolor)
        {
            if (!Fecha.HasValue) return actualcolor;
            TimeSpan interval = DateTime.Now.Subtract(Fecha.Value);

            if (interval.TotalMinutes <= 15)
                return Color.DarkGreen;
            if (interval.TotalMinutes > 15 && interval.TotalMinutes <= 30)
                return Color.LimeGreen;
            if (interval.TotalMinutes > 30 && interval.TotalMinutes <= 60)
                return Color.Goldenrod;
            if (interval.TotalMinutes > 60 && interval.TotalMinutes <= 180)
                return Color.Tomato;
            if (interval.TotalDays==1)
                return Color.Red;

            return Color.Red;
        }

        private string GetDateString(DateTime? Fecha)
        {
            if (!Fecha.HasValue) return "";
            TimeSpan interval = DateTime.Now.Subtract(Fecha.Value);

            if (interval.TotalHours <15)
                return "";
            if (interval.TotalHours >= 15 && interval.TotalHours < 24)
                return string.Format("+ de {0} Horas",Math.Round(interval.TotalHours,0));
            else if (interval.Days == 1 && interval.Days<=30)
                return string.Format("{0} Dia {1} horas", interval.Days,interval.Hours);
            else if (interval.Days > 1 && interval.Days<=30)
                return string.Format("{0} Dias {1} horas", interval.Days,interval.Hours);
            else
                return string.Format("+ {0} Mes", Microsoft.VisualBasic.DateAndTime.DateDiff(DateInterval.Month, Fecha.Value, DateTime.Now,FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1));
        }

        private void CreateFormatConditions()
        {
            var _empresasCol = new EmpresasCol();
            _empresasCol.Query.Where(_empresasCol.Query.EmpEstado == 0);
            _empresasCol.LoadAll();

            dbgTermView.FormatConditions.Clear();

            foreach (var emp in _empresasCol)
            {
                var fc = new StyleFormatCondition(FormatConditionEnum.Equal, colEmpresa, null, emp.EmpNombre);
                fc.Appearance.ForeColor = System.Drawing.Color.FromArgb((int)emp.EmpColor.Value);
                fc.ApplyToRow = true;
                fc.Appearance.Options.UseForeColor = true;
                dbgTermView.FormatConditions.Add(fc);
            }
        }

#if (ExecuteServer)

        private void AddToolbarbuttonCrearServicios()
        {
            //var newButton = new BarButtonItem { Caption = "Test Funcionalidades", Id = 1000, ImageIndex = 24, Name = "", AccessibleDescription = "Test Funcionalidades" };
            //newButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FuncionalidadTestForm_ItemClick);

            //this.barManager1.Items.Add(newButton);
            //this.barUtil.LinksPersistInfo.Add(new LinkPersistInfo(newButton));

        }

#endif


        #endregion

        #region TimerRefresh

        private void StartTimer()
        {

#if (StartTimerRefreshGrid)
            if (Settings.Default.AutomaticRefreshInterval.ToInt()>0)
            {
                _tmrRefreshData = new Timer {Interval = (int) TimeSpan.FromSeconds(Settings.Default.AutomaticRefreshInterval).TotalMilliseconds};
                _tmrRefreshData.Tick += new EventHandler(_tmrRefreshData_Tick);

                _tmrRefreshData.Start();

                barRefreshData.Caption = string.Format("Refrescar (Automatico {0} seg)", Settings.Default.AutomaticRefreshInterval.ToInt());
                _log.LogDebug("TIMER REFRESH DATA. INICIALIZADO...");
            }
            else
            {
                barRefreshData.Caption = "Refrescar (Deshabilitado)";
                _log.LogDebug("TIMER REFRESH DATA 'DESHABILITADO'");
            }
            
#endif
        }

        void _tmrRefreshData_Tick(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void StopTimer()
        {
            if (_tmrRefreshData != null)
            {
                _tmrRefreshData.Tick -= _tmrRefreshData_Tick;
                _tmrRefreshData.Stop();
            }
        }

        #endregion

        #region Events

        private void Mainfm_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason==CloseReason.MdiFormClosing)
            {
                if (MessageBox.Show("¿ Desea cerrar la consola de Visualización ?","Pregunta",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2)==DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }

            CloseIntercommChannel();

            UnsetEvents();
            _xListStoreLayout.SaveLayout();
            StopTimer();

#if ExecuteServer
            StopServer();
#endif



        }


        private void UnsetEvents()
        {
            this.dbgTermView.CustomDrawCell -= this.dbgTermView_CustomDrawCell_1;
            this.dbgTermView.EndGrouping -= this.dbgTermView_EndGrouping;
            this.dbgTermView.CustomColumnDisplayText -= this.dbgTermView_CustomColumnDisplayText;
        }

        private void FillInfoStatusBar()
        {

            try
            {
                IEnumerable<TerminalState> l;

                /*TODO: HAY QUE INCLUIR VARIOS CAMPOS ESTADOS EN BD MAS, QUE ABARQUEN TODAS LAS POSIBILIDADES QUE NE NECESITEN. (DeviceState= [* SHUTDOWN, RUNNING, APP_RESTARTING, SYSTEM_RESTARTING, ERROR] Y DeviceStateTx=Indique la causa del Error).
                CADA UNO QUE INDIQUE UN CONCEPTO, NO MEZCLAR TODOS JUNTOS COMO ESTABA ANTES */
                //
                switch (_filtroStatusBar)
                {
                    case FiltroStatusBarType.Activos:
                        l =_listaTerm.Where(
                                ts => (ts.GrupoID==null || (ts.GrupoID!=null && ts.GetColumn<bool>("GrupoActivo")==true)) &&
                                ts.OperationStatusEnum==OperationalDeviceStatus.Running && ts.OnLine);
                        break;

                    default:
                    case FiltroStatusBarType.Todos:
                        l = _listaTerm;
                        break;

                }


                switch (_filtroStatusBar)
                {
                    case FiltroStatusBarType.Activos:
                    case FiltroStatusBarType.Todos:
                        barFootInfoCnx.Caption = string.Format(" {0}", l.Count(t => t.EstadoConexion));
                        barFootInfoDCnx.Caption = string.Format(" {0}", l.Count(t => !t.EstadoConexion));
                        barFootInfoNum.Caption = string.Format("Nº: {0}", l.Count());
                        break;
                }

                barFootRed.Caption = string.Format("{0}", l.Count(t => t.TerminalEstadoEnum==VehiculoEstado.Rojo));
                barFootGreen.Caption = string.Format("{0}", l.Count(t => t.TerminalEstadoEnum == VehiculoEstado.Verde));
                barFootYellow.Caption = string.Format("{0}", l.Count(t => t.TerminalEstadoEnum == VehiculoEstado.Amarillo));
                barFootBlue.Caption = string.Format("{0}", l.Count(t => t.TerminalEstadoEnum == VehiculoEstado.Azul));
                barFootOrange.Caption = string.Format("{0}", l.Count(t => t.TerminalEstadoEnum == VehiculoEstado.Naranja));
            }
            catch (System.Exception e)
            {
                 ManejarExceptions.ManejarException(new System.Exception("ERROR CALCULAR TOTALES PIE"));
            }
        }

        private void barControlService_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!_procesarCheck) return;

            if (!ValidateAdministrador.Validate()) return;

            if (barControlService.Caption=="Iniciar Servicio")
            {
                if (this.Visible) 
                    if (!StartService())
                    {
                        SetStatusService(_wsmanager.Status == ServiceControllerStatus.Running);
                        return;
                    }

                SetStatusService(true);
            }
            else
            {
                if (MessageBox.Show("Desea detener la ejecución del Servicio, esto hará que los terminales sean desconectados. ¿ Continuar ?","Preguna",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2)==DialogResult.No)
                {
                    return;
                }

                if (this.Visible)
                    if (!StopService())
                    {
                        SetStatusService(_wsmanager.Status == ServiceControllerStatus.Stopped);
                        //El canal no se cierra de nuestro lado, aunque se apague el servicio. Es el otro extremo quien tiene que volver a abrirlo.
                        return;
                    }

                //El canal no se cierra de nuestro lado, aunque se apague el servicio. Es el otro extremo quien tiene que volver a abrirlo.
                SetStatusService(false);
            }
        }

        private void barControlListen_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!_procesarCheck) return;
            if (!ValidateAdministrador.Validate()) return;

            if (barControlListen.Caption == "Permitir Conexiones")
            {
                barControlListen.ImageIndex = 3;
                barControlListen.Caption = "Detener Conexiones";
                
                //Enviamos mensaje al ServiceHost.
                if (this.Visible) 
                {
                    var msg = new MessageIPCinfo(IpcChannelSenderTypes.ConsoleUI, MessagesIPCTypesEnum.StartListen, null);
                    _ipcChannelSender.Send(msg, () => _log.LogDebug(MessageIPCinfo.Logcolor, string.Format("IPC:Message Send.\n{0}", msg.Dump())));
                }

                Thread.Sleep(500);
                SetStatusListen(true);
            }
            else
            {
                if (MessageBox.Show("Esta acción cerrara las conexiones existentes. Si continua ningún terminal podrá conectarse. ¿ Continuar ?", "Preguna", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    return;
                }

                barControlListen.ImageIndex = 4;
                barControlListen.Caption = "Permitir Conexiones";
                //Enviamos mensaje al ServiceHost.
                if (this.Visible )
                {
                     var msg = new MessageIPCinfo(IpcChannelSenderTypes.ConsoleUI, MessagesIPCTypesEnum.StopListen, null);
                    _ipcChannelSender.Send(msg,  () => _log.LogDebug(MessageIPCinfo.Logcolor,string.Format("IPC:Message Send.\n{0}", msg.Dump())));
                }

                Thread.Sleep(500);
                SetStatusListen(false);
            }
        }

        private void barOption_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var foption = new OptionForm();
            if (foption.ShowDialog() == DialogResult.OK)
            {
                var msg = new MessageIPCinfo(IpcChannelSenderTypes.ConsoleUI, MessagesIPCTypesEnum.RebootListen, null);
               _ipcChannelSender.Send(msg,  () => _log.LogDebug(MessageIPCinfo.Logcolor,string.Format("IPC:Message Send.\n{0}", msg.Dump())));
            }
        }

        private void dbgTermView_CustomDrawCell_1(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            try
            {
                var currentView = sender as GridView;
                if (e.RowHandle == currentView.FocusedRowHandle) return;

                DateTime? fecha;

                switch (e.Column.FieldName)
                {
                    case "GpsFechaDetenido":
                        if ((DateTime?)currentView.GetRowCellValue(e.RowHandle,e.Column)!=null)
                        {
                            e.Appearance.BackColor = Color.Moccasin;
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font(e.Appearance.Font,FontStyle.Bold);
                        }
                        break;

                    case "TerminalDisconnect":
                        if ((DateTime?)currentView.GetRowCellValue(e.RowHandle, e.Column) != null)
                        {
                            e.Appearance.BackColor = Color.Moccasin;
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                        }
                        break;

                    case "ModoHistoricoDesde":
                        if ((DateTime?)currentView.GetRowCellValue(e.RowHandle, e.Column) != null)
                        {
                            e.Appearance.BackColor = Color.Moccasin;
                            e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                        }
                        break;

                    case "TerminalConnectedFrom":
                        if ((DateTime?)currentView.GetRowCellValue(e.RowHandle, e.Column) != null)
                        {
                            e.Appearance.BackColor = Color.LightGreen;
                            //e.Appearance.ForeColor = Color.Red;
                            e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                        }
                        break;

                    case "GpsFecha":
                        e.Appearance.ForeColor = GetColorByFecha((DateTime?)currentView.GetRowCellValue(e.RowHandle, e.Column),e.Appearance.BackColor);
                        break;

                    case "FechaTerminalInfoMasActual":
                        e.Appearance.ForeColor = GetColorByFecha((DateTime?)currentView.GetRowCellValue(e.RowHandle, e.Column), e.Appearance.BackColor);
                        //System.Diagnostics.Debug.WriteLine(currentView.GetRowCellValue(e.RowHandle, e.Column).ToString());
                        break;

                    case "TerminalLastReception":
                        e.Appearance.ForeColor = GetColorByFecha((DateTime?)currentView.GetRowCellValue(e.RowHandle, e.Column), e.Appearance.BackColor);
                        //currentView.SetRowCellValue(e.RowHandle, e.Column, GetDateString(fecha));
                        break;
                    
                }
            }
            catch (System.Exception ex)
            {
            }
        }

        private void dbgTermView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            try
            {
                DateTime? fecha;
                string formato = "";

                switch (e.Column.FieldName)
                {
                    case "GpsFechaDetenido":
                        formato = GetDateString((DateTime?)e.Value);
                        if (formato != "")
                            e.DisplayText = formato;
                        break;

                    case "TerminalDisconnect":
                        formato = GetDateString((DateTime?)e.Value);
                        if (formato != "")
                            e.DisplayText = formato;
                        break;

                    case "ModoHistoricoDesde":
                        formato = GetDateString((DateTime?)e.Value);
                        if (formato != "")
                            e.DisplayText = formato;
                        break;

                    case "TerminalConnectedFrom":
                        formato = GetDateString((DateTime?)e.Value);
                        if (formato != "")
                            e.DisplayText = formato;
                        break;

                    case "GpsFecha":
                        formato = GetDateString((DateTime?)e.Value);
                        if (formato != "")
                            e.DisplayText = formato;
                        break;

                    case "FechaTerminalInfoMasActual":
                        formato = GetDateString((DateTime?)e.Value);
                        if (formato != "")
                            e.DisplayText = formato;
                        break;

                    case "TerminalLastReception":

                        formato = GetDateString((DateTime?)e.Value);
                        if (formato != "")
                            e.DisplayText = formato;
                        break;

                        //case "LoginFrom":
                    case "LogoutFrom":

                        formato = GetDateString((DateTime?)e.Value);
                        if (formato != "")
                            e.DisplayText = formato;
                        break;

                }
            }
            catch (System.Exception ex)
            {
                
            }
        }

        private void barRefreshData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        private void barTerminalConfigEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!ValidateAdministrador.Validate()) return;

            ShowTerminalConfiguration(true);
        }

        private void barTerminalConfigView_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowTerminalConfiguration(false);
        }

        private void barTerminalHIncid_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Obtener la configuracion del Terminal y mostrala por pantalla
                if (dbgTermView.SelectedRowsCount != 1)
                    MessageBox.Show("Seleccione el Vehiculo/Terminal del cual desea mostrar el historial de incidencias.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(dbgTermView.FocusedRowHandle)] as TerminalState;
                if (ts == null)
                {
                    MessageBox.Show("NO SE HA PODIDO OBTENER EL VEHICULO/TERMINAL ASOCIADO AL REGISTRO VISUALIZADO", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //MOSTRAMOS LAS INCIDENCIAS.

                var fvisor = new VisorIncidenciasTerminalForm(ts);
                fvisor.ShowDialog();
            }
            catch (System.Exception ex)
            {
                _log.LogException(new GCException(string.Format("ERROR VISUALIZAR INCIDENCIAS DEL TERMINAL"), ex));
            }
        }

        private void barTerminalHConex_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Obtener la configuracion del Terminal y mostrala por pantalla
                if (dbgTermView.SelectedRowsCount != 1)
                    MessageBox.Show("Seleccione el Vehiculo/Terminal del cual desea mostrar el historial de conexiones.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(dbgTermView.FocusedRowHandle)] as TerminalState;
                if (ts == null)
                {
                    MessageBox.Show("NO SE HA PODIDO OBTENER EL VEHICULO/TERMINAL ASOCIADO AL REGISTRO VISUALIZADO", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //MOSTRAMOS LAS INCIDENCIAS.

                var fvisor = new VisoConexionesTerminalForm(ts);
                fvisor.ShowDialog();
            }
            catch (System.Exception ex)
            {
                _log.LogException(new GCException(string.Format("ERROR VISUALIZAR CONEXIONES DEL TERMINAL"), ex));
            }
        }

        private void barGrupos_ItemClick(object sender, ItemClickEventArgs e)
        {
            var fgrupos = new GrupoTerminalForm();
            fgrupos.ShowDialog();
        }

        private void barAssignToGrupo_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
                var _listaIDs = new List<decimal>();

                foreach (int rowhandle in dbgTermView.GetSelectedRows())
                {
                    var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)] as TerminalState;
                    if (ts == null) continue;

                    _listaIDs.Add(ts.TerminalID);
                }

                var fassign = new AssignGrupoTerminalForm(_listaIDs);
                fassign.ShowDialog();

                if (fassign.Result)
                    RefreshData();
            }
            catch (System.Exception ex)
            {
                _log.LogException(new GCException(string.Format("ERROR SELECCIONE DE TERMINALES."), ex));
            }
        }

        private void barAsignarComment_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Asignar un comentario.
            var fcomment = new ComentarioForm();

            if (dbgTermView.GetSelectedRows().Count()==1)
            {
                var ts = bsTerm[dbgTermView.FocusedRowHandle] as TerminalState;
                if (ts != null)
                    fcomment.Comentario = ts.Comentario;
            }
            
            fcomment.ShowDialog(this);

            if (fcomment.DialogResult==DialogResult.OK)
            {
                try
                {
                    foreach (int rowhandle in dbgTermView.GetSelectedRows())
                    {
                        var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)] as TerminalState;
                        if (ts == null) continue;

                        var tsbd = TerminalState.Get(ts.TerminalID);
                        if (tsbd!=null)
                        {
                            tsbd.Comentario = fcomment.Comentario;
                            tsbd.Save();
                        }
                    }
                    RefreshData();
                }
                catch (System.Exception ex)
                {
                    var enew = new System.Exception(string.Format("ERROR AL ASIGNAR COMENTARIO A TERMINALES."), ex);
                    ManejarExceptions.ManejarException(enew);
                }
            }
        }

        private void barSelectiveUpdateLista_ItemClick(object sender, ItemClickEventArgs e)
        {
            bsTerm.DataSource = _listaTerm;
            dbgTerm.DataSource = _listaTerm;
            var fshow = new SelectiveUpdateListForm();
            fshow.ShowDialog();
        }

        private void StopServer()
        {
            try
            {
                //TODO: UI-MAINFORM. DEBUG-MODE. INIT SERVER LISTENING CLIENTS.
                //ServerSetting.SocketHandle.StopListening();
                //ServerSetting.Componentes.DisposeComponentes();
            }
            catch (System.Exception e)
            {
                _log.LogException(new System.Exception("ERROR al STOP SERVER",e));
            }
        }

        #endregion

        #region ACCIONES

        private void btAccSendLI_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!ValidateAdministrador.Validate()) return;

            if (MessageBox.Show("Enviar Lista de Incidencias a todos los Terminales conectados ¿ Continuar ?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
            foreach (int rowhandle in dbgTermView.GetSelectedRows())
            {
                var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)] as TerminalState;
                if (ts == null) continue;

                //Creamos un registro de peticion, para obtener la configuracion
                TerminalDataSend.CreatePeticionSend(ts.VehiculoID, PeticionCodeSend.IncidenciasSend, 1,"","",_log);
            }
        }

        private void btAccGetConfig_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (MessageBox.Show("¿ ENVIAR UNA PETICION A CADA VEHICULO SELECCIONADO PARA OBTENER SU CONFIGURACIÓN?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                return;

            //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
            foreach (int rowhandle in dbgTermView.GetSelectedRows())
            {
                var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)] as TerminalState;
                if (ts == null) continue;
                
                //Creamos un registro de peticion, para obtener la configuracion
                TerminalDataSend.CreatePeticionSend(ts.VehiculoID,  PeticionCodeSend.ConfiguracionGet, 1,"","",_log);
            }

            MessageBox.Show("Peticiones creadas correctamente.", "Aviso", MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        private void btAccDiscconnect_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (MessageBox.Show("¿ DESEA FORZAR LA DESCONEXIÓN DE LOS TERMINALES SELECCIONADOS?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                return;

            byte errores = 0;
            List<decimal> _listVehiculos=new List<decimal>();

            //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
            foreach (int rowhandle in dbgTermView.GetSelectedRows())
            {
                var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)] as TerminalState;
                if (ts == null) continue;
                //Hay que obtener el, TerminalState registro fuera de la coleccion.


                if (!_modoServicio)
                {
                    try
                    {
                        //TODO:MAINFORM | ServerSetting.SocketHandle.DiscconectTerminal(ts.VehiculoID))
                        //if (!ServerSetting.SocketHandle.DiscconectTerminal(ts.VehiculoID))
                        //    errores += 1;
                    }
                    catch (System.Exception ex)
                    {
                        ManejarExceptions.ManejarException(ex, ExceptionErrorLevel.Error);
                        errores += 1;
                    }
                }
                else
                {
                    _listVehiculos.Add(ts.VehiculoID);
                }
            }

            //Enviamos al Servidor, la lista de Terminales a desconnectar.

            if (_modoServicio)
            {
                //Enviamos mensaje al WSWorker.
                 var msg = new MessageIPCinfo(IpcChannelSenderTypes.ConsoleUI, MessagesIPCTypesEnum.DisconnectTerminal, new DisconnectMessageIpcData(_listVehiculos));
                 _ipcChannelSender.Send(msg,  () => _log.LogDebug(MessageIPCinfo.Logcolor,string.Format("IPC:Message Send.\n{0}", msg.Dump())));

                MessageBox.Show("Petición enviada al Servicio. ...", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.DoEvents();
            }
            else
            {
                if (errores == 0)
                    MessageBox.Show("Terminales desconectados correctatemente.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    MessageBox.Show(string.Format("Se produjeron errores al forzar la desconexión. Errores {0}", errores), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            RefreshData();

           
        }

        private void btAccTelefonos_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (MessageBox.Show("¿REFRESCAR LA INFORMACION DE TELEFONOS DE LOS TERMINALES SELECCIONADOS?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                return;

            byte errores = 0;
            byte oks = 0;
            //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
            foreach (int rowhandle in dbgTermView.GetSelectedRows())
            {
                var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)] as TerminalState;
                if (ts == null) continue;

                //Hay que obtener el, TerminalState registro fuera de la coleccion.
                try
                {
                    var tsbd = TerminalState.Get(ts.TerminalID);
                    if (tsbd == null) continue;

                    //Obtener el Telefono del Vehiculo, en el GTIS.
                    var veh = new Vehiculo();
                    if (veh.LoadByPrimaryKey(ts.VehiculoID))
                    {
                        if (veh.VehTelefonoExt!="")
                            tsbd.VehiculoTelefono = veh.VehTelefonoExt;

                        tsbd.Save();
                        oks += 1;
                    }
                }
                catch (System.Exception ex)
                {
                    ManejarExceptions.ManejarException(new System.Exception("ERROR ACTUALIZAR TELEFONO.", ex));
                    errores += 1;
                }
            }

            RefreshData();

            if (errores == 0)
                MessageBox.Show("Telefonos actualizados correctamente.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                MessageBox.Show(string.Format("Se produjeron errores al actualizar los telefonos. Errores {0}", errores), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void barSelectAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            dbgTermView.SelectAll();
        }

        private void barUnSelect_ItemClick(object sender, ItemClickEventArgs e)
        {
            dbgTermView.ClearSelection();
        }

        private void btAccApplyTemplConfig_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!ValidateAdministrador.Validate()) return;
            //TODO:MAINFORM | Aplicar TemplateConfiguration
            //try
            //{
            //    //Obtener la configuracion de uno de los terminales seleccionados, que servirá de plantilla el envio.
            //    if (dbgTermView.SelectedRowsCount == 0)
            //    {
            //        MessageBox.Show("Seleccione el Vehiculo/Terminal del cual desea Aplicar una Plantilla de Configuración", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }

            //    TerminalState ts = null;
            //    TerminalConfiguration tc = null;
            //    HashTableTrama hashtemplate = null;
            //    var listavehiculos = new List<decimal>();

            //    foreach (int rowhandle in dbgTermView.GetSelectedRows())
            //    {
            //        ts = (TerminalState)bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)];
            //        listavehiculos.Add(ts.VehiculoID);
            //    }
                
            //    //MOSTRAMOS LA CONFIGURACION COMO PLANTILLA DE CAMBIO PARA LOS TERMINALES SELECCIONADOS.
            //    var ftemplApply = new TemplateConfigurationAssignForm(listavehiculos);
            //    ftemplApply.ShowDialog();

            //    if (ftemplApply.DialogResult == DialogResult.OK)
            //        RefreshData();


            //}
            //catch (System.Exception ex)
            //{
            //    ManejarExceptions.ManejarException(ex, "", ExceptionErrorLevel.Error, false, true);
            //    _log.LogException(new GCException(string.Format("ERROR VISUALIZAR CONFIGURACION DE VEHICULO"), ex));
            //}

        }

        #endregion

        #region Metodos

#if (ExecuteServer)

        private void ExecuteServer()
        {
            //try
            //{
            //    //TODO:Mainfrm | ExecuteServer revisar
            //    _modoServicio = false;

            //    //Load de Log configuration
            //    _log.LoadSmartInpectFile(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "");

            //    barControlService.Visibility = BarItemVisibility.Never;
            //    barControlListen.Visibility = BarItemVisibility.Never;

            //    //Iniciamos el Proceso de Arranque
            //    if (!InitializationProcess.Initialize())
            //    {
            //        var e = new GCException("ERROR OCURRIDO EN EL PROCESO DE INICIALIZACIÓN. NO SE PUEDE CONTINUAR CON LA EJECUCIÓN DEL SERVICIO");
            //        _log.LogException(e); //HACE QUE EL SERVICIO, SE DETENGA.
            //    }

            //    InitializationProcess.InitSQLProfiling();

            //    AddToolbarbuttonCrearServicios();

            //    ServerSetting.Componentes.RunComponentes();
            //    ServerSetting.SocketHandle.StartListening();
            //}
            //catch (Exception e)
            //{
            //    _log.LogException(new GCException("ERROR EN INICIALIZACION. DETALLE DEL ERROR.", e));
            //}
        }
#endif



        private void ShowTerminalConfiguration(bool editable)
        {
            //TODO: MAINFORM. ShowTerminalConfiguration
            //try
            //{
            //    //Obtener la configuracion del Terminal y mostrala por pantalla
            //    if (dbgTermView.SelectedRowsCount != 1)
            //        MessageBox.Show("Seleccione el Vehiculo/Terminal del cual desea mostrar la configuración", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //    var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(dbgTermView.FocusedRowHandle)] as TerminalState;
            //    if (ts == null)
            //    {
            //        MessageBox.Show("NO SE HA PODIDO OBTENER EL VEHICULO/TERMINAL ASOCIADO AL REGISTRO VISUALIZADO", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        return;
            //    }

            //    if (editable && ExistePeticionEnvioPendiente(ts.VehiculoID.Value,PeticionCodeSend.ConfiguracionSet))
            //        return;

            //    var tc = TerminalConfiguration.GetConfigurationByVehiculoID(ts.VehiculoID.Value);
            //    if (tc == null || tc.ConfigData == "")
            //    {
            //        if (MessageBox.Show("ESTE VEHICULO/TERMINAL NO TIENE UNA CONFIGURACION ASOCIADA ACTUALMENTE ¿Desea crear una petición para obtenerla?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            //        {
            //            //Creamos un registro de peticion, para obtener la configuracion
            //            TerminalDataSend.CreatePeticionSend(ts.VehiculoID.Value, "", PeticionCodeSend.ConfiguracionGet, 1);
            //        }
            //        return;
            //    }

            //    //MOSTRAMOS LA CONFIGURACION.
            //    var ht = HashTableTrama.Deserialize(tc.ConfigData);
            //    var foption = new TerminalOptionForm(ts,tc, ht, editable);
            //    foption.ShowDialog();
            //}
            //catch (System.Exception ex)
            //{
            //    _log.LogException(new GCException(string.Format("ERROR VISUALIZAR CONFIGURACION DE VEHICULO"), ex));
            //    ManejarExceptions.ManejarException(ex,"",ExceptionErrorLevel.Error,false,true);
            //}
        }

        private void ShowChangeGlobalConfiguration()
        {
            //TODO: MAINFORM. ShowChangeGlobalConfiguration
            //try
            //{
            //    //Obtener la configuracion de uno de los terminales seleccionados, que servirá de plantilla el envio.
            //    if (dbgTermView.SelectedRowsCount==0)
            //    {
            //        MessageBox.Show("Seleccione el Vehiculo/Terminal del cual desea mostrar la configuración", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }

            //    TerminalState ts = null;
            //    TerminalConfiguration tc = null;
            //    HashTableTrama hashtemplate = null;
            //    var listavehiculos = new List<decimal>();

            //    foreach (int rowhandle in dbgTermView.GetSelectedRows())
            //    {
            //        ts = (TerminalState) bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)];
            //        if (hashtemplate==null && ts.HasConfiguration)
            //        {
            //            //Buscamos algun terminal con la configuración obtenida.
            //            tc = TerminalConfiguration.GetConfigurationByVehiculoID(ts.VehiculoID.Value);
            //            if (tc != null && tc.ConfigData != "")
            //            {
            //                hashtemplate = HashTableTrama.Deserialize(tc.ConfigData);
            //            }
            //        }

            //        listavehiculos.Add(ts.VehiculoID.Value);
            //    }

            //    if (hashtemplate==null)
            //    {
            //        MessageBox.Show("NO HAY NINGÚN TERMINAL QUE POSEE UNA CONFIGURACIÓN.\nOBTENGA LA CONFIGURACIÓN PARA ALGUN TERMINAL, Y DESPUES VUELVA A INTENTARLO.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        return;
            //    }

            //    //MOSTRAMOS LA CONFIGURACION COMO PLANTILLA DE CAMBIO PARA LOS TERMINALES SELECCIONADOS.
            //    var foption = new ChangeGlobalOptionForm(listavehiculos, hashtemplate);
            //    foption.ShowDialog();
            //}
            //catch (System.Exception ex)
            //{
            //    ManejarExceptions.ManejarException(ex,"",ExceptionErrorLevel.Error,false,true);
            //    _log.LogException(new GCException(string.Format("ERROR VISUALIZAR CONFIGURACION DE VEHICULO"), ex));
            //}
        }

        private bool ExistePeticionEnvioPendiente(decimal vehiculoID,PeticionCodeSend peticionTipo)
        {
            try
            {
                var tdsq = new TerminalDataSendQ();
                tdsq.Where(tdsq.PeticionCode == peticionTipo, tdsq.VehiculoID == vehiculoID);

                var td = new TerminalDataSend();
                if (td.Load(tdsq))
                {
                    if (MessageBox.Show("ATENCIÓN!. EXISTE UN ENVIO PENDIENTE DE ESTE TIPO PARA EL TERMINAL, SI CONTINUA SOBREESCRIBIRÁ ESTE ENVIO CON EL NUEVO. ¿ Continuar?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return true;
                }

                return false;
            }
            catch (System.Exception e)
            {
                ManejarExceptions.ManejarException(new GCException("ERROR BUSCAR OTRA PETICION IGUAL PENDIENTE DE ENVIO.\r",e));
                return false;
            }
        }

        #endregion

        private void barUnLockAdmin_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (!barUnLockAdmin.Checked)
            {
                barUnLockAdmin.Caption = "Desbloquear";
                barUnLockAdmin.ImageIndex = 16;
                barUnLockAdmin.Appearance.ForeColor = Color.Red;

               ModuleConfig.Global.SessionAdminActive = false;
            }
            else
            {
                if (!ValidateAdministrador.Validate())
                {
                    barUnLockAdmin.Checked = !barUnLockAdmin.Checked;
                    return;
                }

                barUnLockAdmin.Caption = "Bloquear";
                barUnLockAdmin.ImageIndex = 17;
                barUnLockAdmin.Appearance.ForeColor = Color.DarkGreen;

                ModuleConfig.Global.SessionAdminActive = true;

            }
        }

		private void dbgTermView_LayoutUpgrade(object sender, LayoutUpgradeEventArgs e)
        {
            //if (e.)
        }

        private void barOptionCountFooter_ShowAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            _filtroStatusBar =FiltroStatusBarType.Todos;
            barOptionCountFooter.Caption = "Todos";
            FillInfoStatusBar();
        }

        private void barOptionCountFooter_ShowOnlyActive_ItemClick(object sender, ItemClickEventArgs e)
        {
            _filtroStatusBar=FiltroStatusBarType.Activos ;
            barOptionCountFooter.Caption = "Activos";
            FillInfoStatusBar();
        }

        private void dbgTermView_EndGrouping(object sender, EventArgs e)
        {
            if (dbgTermView.GroupCount > 0)
                dbgTermView.ExpandAllGroups();
        }

        private void dbgTermView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;

            GridView view = (GridView) sender;
            Point pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);

            if (!info.InRow) return;

            if (dbgTermView.SelectedRowsCount > 1)
            {
                popupGridOptions.ItemLinks[0].Visible  = false;
                popupGridOptions.ItemLinks[1].Visible = false;
            }
            else
            {
                popupGridOptions.ItemLinks[0].Visible = true;
                popupGridOptions.ItemLinks[1].Visible = true;
            }
            popupGridOptions.ShowPopup(dbgTerm.PointToScreen(e.Location));
        }

        private void barTerminalAddSelectUpdate_ItemClick(object sender, ItemClickEventArgs e)
        {

            if (MessageBox.Show("¿AGREGAR LOS TERMINALES SELECCIONADOS A LA LISTA DE ACTUALIZACIÓN DE SOFTWARE SELECTIVA?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                return;

            var listaUpdateC = new TerminalSelectiveSoftwareUpdateCol();
            listaUpdateC.LoadAll();

            //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
            foreach (int rowhandle in dbgTermView.GetSelectedRows())
            {
                var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)] as TerminalState;
                if (ts == null) continue;

                if (listaUpdateC.FindByPrimaryKey(ts.TerminalID)==null)
                {
                    //Hay que obtener el, TerminalState registro fuera de la coleccion.
                    var newt = listaUpdateC.AddNew();
                    newt.TerminalID = ts.TerminalID;
                    newt.VehiculoID = ts.VehiculoID;
                    newt.VehiculoMatricula = ts.VehiculoMatricula;
                }
            }

            try
            {
                listaUpdateC.Save();
            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new System.Exception("ERROR AGREGAR TERMINALES A LA LISTA.",ex),ExceptionErrorLevel.Error);
            }

        }

        private void ConfigurarInterfaz()
        {
            //Configuracion Interfaz
            if (ModuleConfig.AppConfig.Server.TerminalUpdateMode == SoftwareUpateMode.Selectivo)
            {
                colInSelectiveUpdate.Visible = true;
            }
            else
            {
                colInSelectiveUpdate.Visible = false;
            }

            //Load filters
            FillFilterMenu();

            CreateFormatConditions();

            //dbgTermView.BestFitColumns();
        }


        private void barRefreshoOption_ItemClick(object sender, ItemClickEventArgs e)
        {
            //TODO: MAINFORM barRefreshoOption_ItemClick
            //ConfigurarInterfaz();
            //if (IsPosibleSendMensaje())
            //    _pipeServer.SendMessage(new MessageServerInfo(MessagesServerTypesEnum.RefreshConfiguracion));
            //else
            //    _log.LogWarning("RefreshoOption. No se puede enviar mensaje [RefreshConfiguracion] porque (Canal está cerrado)"); 
        }

        private void barAssignEstado_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
                var _listaIDs = new List<decimal>();

                foreach (int rowhandle in dbgTermView.GetSelectedRows())
                {
                    var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)] as TerminalState;
                    if (ts == null) continue;

                    _listaIDs.Add(ts.TerminalID);
                }

                var fassign = new AssignEstadoForm(_listaIDs);
                fassign.ShowDialog();

                if (fassign.Result)
                    RefreshData();
            }
            catch (System.Exception ex)
            {
                _log.LogException(new GCException(string.Format("ERROR. ASIGNAR NUEVO ESTADO A TERMINALES."), ex));
            }
        }

        private void barFiltersAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            var fadd = new AddFilterForm(dbgTermView.ActiveFilterString);
            fadd.ShowDialog();

            if (fadd.DialogResult == DialogResult.OK)
                FillFilterMenu();
        }

        private void barFiltersLista_ItemClick(object sender, ItemClickEventArgs e)
        {
            var flista = new FilterListaForm();
            flista.ShowDialog();

            if (flista.DialogResult == DialogResult.OK)
                FillFilterMenu();
        }

        private void FillFilterMenu()
        {

            //BORRAMOS SI HAY MENUS YA CREADOS
            var _listRemove = new List<BarItemLink>();

            for (int i = 0; i < barFilters.ItemLinks.Count; i++)
            {
                var _link = barFilters.ItemLinks[i];
                if (_link.Item.Name.Contains("barFiltersOption"))
                        _listRemove.Add(_link);
            }
            if (_listRemove.Count>0)
            {
                foreach (var _link in _listRemove)
                {
                    barFilters.ItemLinks.Remove(_link);
                }
            }


            bool beginGroup = false;

            foreach (var _filter in ModuleConfig.UserConfig.ConsoleFiltersNamed)
            {
                var newbar = new BarButtonItem();

                newbar.Tag = _filter.Filter;
                newbar.Caption = _filter.Name;
                newbar.Id = _filter.Id;
                newbar.Name = "barFiltersOption" + _filter.Id;
                newbar.SuperTip = new SuperToolTip();
                newbar.SuperTip.Items.Add(_filter.Description);

                newbar.ItemClick += new ItemClickEventHandler(filterSaved_ItemClick);
                 
                var link = barFilters.AddItem(newbar);
                if (!beginGroup)
                {
                    link.BeginGroup = true;
                    beginGroup = true;
                }

            }

        }

        void filterSaved_ItemClick(object sender, ItemClickEventArgs e)
        {
            dbgTermView.ActiveFilterString = e.Item.Tag.ToString();
        }

        private void barAccConnectSOS_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (e.Item.Name == barAccConnectSOS.Name)
                if (!ValidateAdministrador.Validate()) return;

            if (MessageBox.Show("Enviar solicitud para que se Auto-Conecte la Ayuda-SOS ¿ Continuar ?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            string name = e.Item.Name;

            //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
            foreach (int rowhandle in dbgTermView.GetSelectedRows())
            {
                var ts = bsTerm[dbgTermView.GetDataSourceRowIndex(rowhandle)] as TerminalState;
                if (ts == null) continue;

                //Creamos un registro de peticion, para obtener la configuracion
                TerminalDataSend.CreatePeticionSend(ts.VehiculoID, PeticionCodeSend.AutoConnectSOS, 1, name.Substring(name.Length-1, 1), "",_log);
            }
        }

        private void barPositionMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
            if (dbgTermView.GetSelectedRows().Count() != 1) return;

            var ts = bsTerm.Current as TerminalState;

            Clipboard.SetText(string.Format(string.Format("http://maps.google.es/maps?q={0},{1}&z=13", ts.GpsLatitud.ToString().Replace(",", "."), ts.GpsLongitud.ToString().Replace(",", "."))));
        }

        private void ttc_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (!(e.SelectedControl == dbgTerm)) return;

            ToolTipControlInfo info = null;

            var view = (BandedGridView)dbgTerm.GetViewAt(e.ControlMousePosition);
            if (view == null) return;

            var hi =(GridHitInfo) view.CalcHitInfo(e.ControlMousePosition);
            if (hi.HitTest==GridHitTest.RowCell)
            {
                if (hi.Column.ColumnEdit!=null)
                {
                     e.Info = new ToolTipControlInfo("", view.GetRowCellDisplayText(hi.RowHandle, hi.Column));
                }
            }
        }

        private void barViewPetVeh_ItemClick(object sender, ItemClickEventArgs e)
        {
            //VER LAS PETICIONES HISTORICO O EN CURSO
            if (dbgTermView.GetSelectedRows().Count() != 1) return;

            var f = new ListaPeticionesEnviarTerminalForm((bsTerm.Current as TerminalState).VehiculoID, (bsTerm.Current as TerminalState).VehiculoMatricula);
            f.MdiParent = MDIForm.Instance;
            f.Show();
        }

        private void barViewTerminalDataReceived_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Recorrer todos los terminales, seleccionados y crear una peticion de obtencion de la configuracion
            if (dbgTermView.GetSelectedRows().Count() != 1) return;

            var f = new ListaTerminalDataReceivedForm((bsTerm.Current as TerminalState).VehiculoID, (bsTerm.Current as TerminalState).VehiculoMatricula);
            f.MdiParent = MDIForm.Instance;
            f.Show();
        }


        private void btAccSendConfiguracion_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!ValidateAdministrador.Validate()) return;
            ShowChangeGlobalConfiguration();
        }

        private void barTemplateConfig_ItemClick(object sender, ItemClickEventArgs e)
        {
            var flista = new gcperu.Server.UI.Paneles.Forms.TemplateConfigurationListaForm();
            flista.ShowDialog();
        }

        private void barTaskResendMntos_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!ValidateAdministrador.Validate())
                return;

            if (MessageBox.Show("¿ Actualizar los ficheros de Mantenimientos en el Terminal ?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            var ts = (bsTerm.Current as TerminalState);
            if (ts != null)
            {
                //CREAR LAS PETICIONES DE ENVIO DE LOS MANTENIMIENTOS
                TerminalState.CreatePeticionesSyncroMantenimientos(ts,_log);
                ts.FechaLastSyncroMtnos = DateTime.Now.Date;

                ts.Save();
            }
            else
            {
                ManejarExceptions.ManejarException(new System.Exception("No se ha podido obtener el Registro TerminalState."));
            }
        }

    }
}
