using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using gcperu.Server.UI.Helper;
using gcperu.Server.UI.Properties;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;
using GCPeru.Server.Core.Settings;
using Gurock.SmartInspect;

namespace gcperu.Server.UI.Xtra
{
    public interface IXLayout
    {
        bool SaveLayout();
        bool RestoreLayout();
    }

    #region XLayout

    public class XLayoutGrid : IXLayout
    {

        private ISSTraceLog _log = Ioc.Container.Get<ISSTraceLog>();

        //Private Shared defaultInstanceOptions As New XLayoutOptions         'Utilizada para Acceder a las opciones desde todo el Proyecto. Unica instancia   
        private static XLayoutGridOptions _layoutSharedOptions = new XLayoutGridOptions(); //Opciones que son las base si no se especifican, otras

        private GridControl _grid;
        private string _version;
        private XLayoutGridOptions _layoutOptions = new XLayoutGridOptions();

        #region Contructores

        private XLayoutGrid()
        {
        }

        public XLayoutGrid(DevExpress.XtraGrid.GridControl Grid,string version)
        {

            if (Grid == null || Grid.Views == null || Grid.Views.Count == 0)
            {
                throw new ArgumentException("Control Grid especificado no posee ninguna vista o es nothing");
            }

            _grid = Grid;
            _version = version;

            //Todas las vistas disponibles, la Asignamos para guardar el formato
            foreach (var view in _grid.Views)
            {
                _layoutOptions.Views.Add((BaseView)view);
            }

            if (_layoutSharedOptions == null)
            {
                _layoutOptions.Options = (OptionsLayoutGrid) _layoutOptions.Views[0].OptionsLayout; //Utilizamos las opciones por defecto de la Vista
            }
            else
            {
                _layoutOptions.Options = _layoutSharedOptions.Options;
                _layoutOptions.Tarjet.Type = _layoutSharedOptions.Tarjet.Type;
                _layoutOptions.Tarjet.PathString = _layoutSharedOptions.Tarjet.PathString;
                _layoutOptions.Tarjet.PathStream = _layoutSharedOptions.Tarjet.PathStream;
            }

        }

        public XLayoutGrid(DevExpress.XtraGrid.GridControl Grid, OptionsLayoutGrid Options)
        {

            if (Options == null)
            {
                throw new ArgumentException("Debe especificar las opciones");
            }

            _grid = Grid;

            //Todas las vistas disponibles, la Asignamos para guardar el formato
            foreach (var view in _grid.Views)
            {
                _layoutOptions.Views.Add((BaseView) view);
            }

            _layoutOptions.Options = Options; //Utilizamos las opciones por defecto de la Vista

        }

        #endregion

        #region Propiedades
        public DevExpress.XtraGrid.GridControl Grid
        {
            get
            {
                return _grid;
            }
        }

        public XLayoutGridOptions Opciones
        {
            get
            {
                return _layoutOptions;
            }
        }

        #endregion

        #region Propiedades Shared

        public static XLayoutGridOptions Options
        {
            get
            {
                return _layoutSharedOptions;
            }
            set
            {
                _layoutSharedOptions = value;
            }
        }

        //Public Shared ReadOnly Property [Default]() As XLayoutGrid
        //    Get
        //        Return defaultInstance
        //    End Get
        //End Property

        #endregion

        #region Funciones Publicas

        public bool RestoreLayout()
        {

            if (ModuleConfig.UserConfig.HandleLayouts == false)
            {
                return false;
            }
            bool tempRestoreLayout = false;
            switch (_layoutOptions.Tarjet.Type)
            {
                case XLayoutOptionsBase.TargetEnum.ToXml:
                    tempRestoreLayout = this.RestoreFromXml();
                    break;
                case XLayoutOptionsBase.TargetEnum.ToRegsitry:
                    break;
                case XLayoutOptionsBase.TargetEnum.ToStream:
                    break;
            }
            return tempRestoreLayout;
        }

        public bool SaveLayout()
        {
            if (ModuleConfig.UserConfig.HandleLayouts == false)
            {
                return false;
            }

            bool tempSaveLayout = false;
            switch (_layoutOptions.Tarjet.Type)
            {
                case XLayoutOptionsBase.TargetEnum.ToXml:
                    tempSaveLayout = this.SaveToXml();
                    break;
                case XLayoutOptionsBase.TargetEnum.ToRegsitry:
                    break;
                case XLayoutOptionsBase.TargetEnum.ToStream:
                    break;
            }
            return tempSaveLayout;
        }

        #endregion

        #region Funciones Privadas: Save/Load Layout

        private bool SaveToXml()
        {

            string _FileNameSave = _layoutOptions.Tarjet.PathString;
            if (string.IsNullOrEmpty(_FileNameSave)) return false;
            try
            {
                foreach (var view in _layoutOptions.Views)
                {
                    _FileNameSave = Path.Combine(_layoutOptions.Tarjet.PathString, (string)this.GetFileNameLayoutView(view));
                     view.SaveLayoutToXml(_FileNameSave, _layoutOptions.Options);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                ExceptionHandler.ShowMensajeBox(ex,Level.Error);
            }

            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return false;
        }

        private bool RestoreFromXml()
        {
            string _FileNameSave = _layoutOptions.Tarjet.PathString;

            try
            {
                foreach (var view in _layoutOptions.Views)
                {
                    _FileNameSave = Path.Combine(_layoutOptions.Tarjet.PathString, (string)this.GetFileNameLayoutView(view));

                    if (File.Exists(_FileNameSave))
                    {
                        view.RestoreLayoutFromXml(_FileNameSave, _layoutOptions.Options);
                        if (_layoutOptions.DeleteFilter)
                        {
                            if (view is DevExpress.XtraGrid.Views.Grid.GridView)
                            {
                                ((DevExpress.XtraGrid.Views.Grid.GridView)view).ActiveFilterString = "";
                            }
                        }
                    }
                }
                return true;
            }
            catch (System.Exception ex)
            {
                ExceptionHandler.ShowMensajeBox(ex, Level.Error);
            }
            
            return false;
        }

        #endregion

        #region Funciones Shared

        public static void LoadInitialOptionsLayouts()
        {
            XLayoutGrid.Options.Tarjet.Type = XLayoutOptionsBase.TargetEnum.ToXml;
            XLayoutGrid.Options.Tarjet.PathString = Path.Combine(ModuleConfig.UserConfig.Manager.DirectoryBase,"LayoutsGrid");

            if (Directory.Exists(XLayoutGrid.Options.Tarjet.PathString) == false)
            {
                Directory.CreateDirectory(XLayoutGrid.Options.Tarjet.PathString);
            }

            TraceLog.LogDebug(string.Format("LoadInitialOptionsLayouts. Type={0} Path={1}", XLayoutGrid.Options.Tarjet.Type.ToString(), XLayoutGrid.Options.Tarjet.PathString));
        }

        #endregion

        #region Funciones Privadas

        private object GetFileNameLayoutView(DevExpress.XtraGrid.Views.Base.BaseView view)
        {
            //Para formar el Nombre del Fichero, utilizamos el Formulario en el cual esta situado
            string _fileName = null;

            //If TypeOf (_grid.TopLevelControl) Is System.Windows.Forms.Form Then
            _fileName = string.Format("{0}_{1}_{2}_{3}.xml",GetFormContainer().Name, _grid.Name,view.Name,_version);

            return _fileName;

        }

        private Form GetFormContainer()
        {

            try
            {
                return _grid.FindForm();
                
            }
            catch (System.Exception ex)
            {
                return _grid.TopLevelControl.FindForm();
            }
        }

        #endregion

    }

    public class XLayoutCollection : List<IXLayout>, IXLayout
    {

        public bool RestoreLayout()
        {
            try
            {
                //Operaciones a realizar cuando se realiza la Carga de la Formulario Base
                if (this.Count > 0)
                {
                    foreach (var xLayout in this)
                    {
                        xLayout.RestoreLayout();
                    }
                }
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }

        public bool SaveLayout()
        {
            try
            {
                if (this.Count > 0)
                {
                    foreach (var xLayout in this)
                    {
                        xLayout.SaveLayout();
                    }
                }
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }
    }

    #endregion

    #region XTraLayoutOptions

    public class XLayoutOptionsBase
    {

        public enum TargetEnum : byte
        {
            ToXml = 1,
            ToStream = 2,
            ToRegsitry = 3
        }

        protected XLayoutTarjetOptions _tarjet = new XLayoutTarjetOptions();

        #region Contructores
        public XLayoutOptionsBase()
        {
            _tarjet.Type = XLayoutOptionsBase.TargetEnum.ToXml;
        }

        public XLayoutOptionsBase(XLayoutTarjetOptions Tarjet)
        {
            _tarjet = Tarjet;
        }
        #endregion

        public XLayoutTarjetOptions Tarjet
        {
            get
            {
                return _tarjet;
            }
            set
            {
                _tarjet = value;
            }
        }

    }

    public class XLayoutGridOptions : XLayoutOptionsBase
    {

        private OptionsLayoutGrid _layoutOptions = new OptionsLayoutGrid(); //Opciones que son las base si no se especifican, otras
        private bool _DeleteFilter = true;
        private List<DevExpress.XtraGrid.Views.Base.BaseView> _views = new List<DevExpress.XtraGrid.Views.Base.BaseView>();

        #region Contructores
        public XLayoutGridOptions()
            : base()
        {

            _layoutOptions.Columns.AddNewColumns = true;
            _layoutOptions.Columns.RemoveOldColumns = true;
            _layoutOptions.Columns.StoreAllOptions = true;
            _layoutOptions.Columns.StoreLayout = true;
            _layoutOptions.StoreDataSettings = true;
            _layoutOptions.StoreVisualOptions = true;

        }


        public XLayoutGridOptions(OptionsLayoutGrid LayoutOptions, XLayoutTarjetOptions Tarjet)
            : this(LayoutOptions, Tarjet, true)
        {
        }

        public XLayoutGridOptions(OptionsLayoutGrid LayoutOptions, XLayoutTarjetOptions Tarjet, bool DeleteFilter)
            : base(Tarjet)
        {
            _layoutOptions = LayoutOptions;
            _DeleteFilter = DeleteFilter;
        }


        public XLayoutGridOptions(OptionsLayoutGrid LayoutOptions, XLayoutTarjetOptions Tarjet, DevExpress.XtraGrid.Views.Base.BaseView View)
            : this(LayoutOptions, Tarjet, View, true)
        {
        }

        public XLayoutGridOptions(OptionsLayoutGrid LayoutOptions, XLayoutTarjetOptions Tarjet, DevExpress.XtraGrid.Views.Base.BaseView View, bool DeleteFilter)
            : base(Tarjet)
        {
            _DeleteFilter = DeleteFilter;
            _layoutOptions = LayoutOptions;
            _views.Add(View);
        }
        #endregion

        #region Propiedades
        public List<DevExpress.XtraGrid.Views.Base.BaseView> Views
        {
            get
            {
                return _views;
            }
        }

        public OptionsLayoutGrid Options
        {
            get
            {
                return _layoutOptions;
            }
            set
            {
                _layoutOptions = value;
            }
        }

        public bool DeleteFilter
        {
            get
            {
                return _DeleteFilter;
            }
            set
            {
                _DeleteFilter = value;
            }
        }

        #endregion

    }

    public class XLayoutTreeOptions : XLayoutOptionsBase
    {

        #region Contructores
        public XLayoutTreeOptions()
            : base()
        {
        }

        public XLayoutTreeOptions(XLayoutTarjetOptions Tarjet)
            : base(Tarjet)
        {
        }
        #endregion

    }


    public class XLayoutTarjetOptions
    {

        private XLayoutOptionsBase.TargetEnum _tarjetType = XLayoutOptionsBase.TargetEnum.ToXml;
        private string _tarjetPath;
        private System.IO.Stream _tarjetStream;

        public XLayoutTarjetOptions()
        {
        }

        public XLayoutTarjetOptions(Xtra.XLayoutOptionsBase.TargetEnum Type, object Path)
        {

            if ((Path) is string)
            {
                _tarjetPath = (string)Path;
            }
            else if ((Path) is System.IO.Stream)
            {
                _tarjetStream = (System.IO.Stream)Path;
            }

        }

        public XLayoutOptionsBase.TargetEnum Type
        {
            get
            {
                return _tarjetType;
            }
            set
            {
                _tarjetType = value;
            }
        }


        public string PathString
        {
            get
            {
                return _tarjetPath;
            }
            set
            {
                _tarjetPath = value;
            }
        }

        public System.IO.Stream PathStream
        {
            get
            {
                return _tarjetStream;
            }
            set
            {
                _tarjetStream = value;
            }
        }

    }

    #endregion



}