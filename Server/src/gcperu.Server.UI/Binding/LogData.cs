using System;
using System.Collections.Generic;

namespace gcperu.Server.UI.Binding
{
    public class LogData
    {
        public double ID { get; set; }
        public DateTime Fecha { get; set; }
        public string TermID { get; set; }
        public string data { get; set; }

        public LogData(double id, DateTime fecha, string termId, string data)
        {
            ID = id;
            Fecha = fecha;
            TermID = termId;
            this.data = data;
        }
    }

    public class LogDataCol : List<LogData>
    {
    }

}