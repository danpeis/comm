using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using EntitySpaces.Core;
using EntitySpaces.DynamicQuery;
using gcperu.Contract;
using gcperu.Server.Core;
using gcperu.Server.Core.Info;
using gcperu.Server.UI.Properties;
using gitspt.global.Core.Exceptions;
using gitspt.global.Core.Extension;
using gitspt.global.Core.Log;
using gitspt.global.Core.Util.Ficheros;
using GCPeru.Server.Core.Settings;
using ManejarExceptions = gcperu.Server.UI.Exception.ManejarExceptions;
using gcperu.Server.Core.Extension;
using gitspt.global.Core.IoC;
using LevelLog = gitspt.global.Core.Log.LevelLog;

namespace gcperu.Server.UI
{
    public partial class OptionForm : DevExpress.XtraEditors.XtraForm
    {

        private const int FORM_WIDTH_NORMAL=391;
        private const int FORM_WIDTH_EXTENDIDO=1000;

        private ISSTraceLog _log;
        private OperacionResult result = OperacionResult.None;
        private ListaTerminalUpdateInfo _terminalUpdateInfo;


        public OperacionResult Result
        {
            get { return result; }
        }

        public OptionForm()
        {
            InitializeComponent();

            _log = Ioc.Container.Get<ISSTraceLog>();
        }

        private void OptionForm_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
            this.Width = FORM_WIDTH_NORMAL;

            FillLogLevels();
            cmdCkExpand.Image = Resources.Fecha_Izquierda_Doble.ToBitmap();

            txtUrlEndPoint.Text = ModuleConfig.AppConfig.Server.UrlEndPoint;
            txtEmpresaNombre.Text = ModuleConfig.AppConfig.Server.Empresa;

            TrackIntervalSend.Value = ModuleConfig.AppConfig.Server.ComponentWatchSendInterval;
            TrackIntervalReSend.Value = ModuleConfig.AppConfig.Server.ComponentWatchReSendInterval;
            TrackDiasPosDiario.Value = ModuleConfig.AppConfig.Server.DiarioPosicionesMantenerDias;

            //Rellenar Datos de los Combos para ServiciosEspeciales
            FillCombosServiciosEspeciales();
            FillCombosEnum();


            //Email
            txtEmailFrom.Text = ModuleConfig.AppConfig.Server.EMail.EmailFrom;
            txtEMails.Text = ModuleConfig.AppConfig.Server.EMail.EmailListaTo;

            ckEMail_Requiere.Checked = (ModuleConfig.AppConfig.Server.EMail.EmailSecurityUser != "");
            if (ckEMail_Requiere.Checked)
                frmEmailUser.Visible = true;

            txtEMail_SMTP.Text = ModuleConfig.AppConfig.Server.EMail.EmailServerSmtp;
            txtEMail_User.Text = ModuleConfig.AppConfig.Server.EMail.EmailSecurityUser;
            txtEMail_Pwd.Text = ModuleConfig.AppConfig.Server.EMail.EmailSecurityPwd;

            cbLevelLog.EditValue = Enum.GetName(typeof(Contract.LevelLog),ModuleConfig.AppConfig.Server.LevelLog);

            if (!ModuleConfig.Global.SessionAdminActive)
            {
                cmdCkExpand.Enabled = false;
                txtUrlEndPoint.Enabled = false;
                txtServerPort.Enabled = false;
                txtMaxClientes.Enabled = false;
                cmdThrowIncid.Enabled = false;
            }

            //TERMINAL UPDATE INFO
            opUpdateMode.EditValue = (byte)ModuleConfig.AppConfig.Server.TerminalUpdateMode;

            _terminalUpdateInfo = (ListaTerminalUpdateInfo)ModuleConfig.AppConfig.Server.TerminalUpdate.Clone();
            bsTerminalUpdate.DataSource = _terminalUpdateInfo;

            txtBDVersion.Text = ModuleConfig.Global.DatabaseVersion.ToString();
        }

        private void FillCombosServiciosEspeciales()
        {
            var dbut = new esUtility();
            dbut.es.Connection.Name="GT";

            cbPetc.Properties.DataSource = dbut.FillDataTable(esQueryType.Text, "Select * from Peticiones Where pet_Eliminado=0");
            cbUser.Properties.DataSource = dbut.FillDataTable(esQueryType.Text, "Select * from Usuarios Where usu_Eliminado=0");
            cbComp.Properties.DataSource = dbut.FillDataTable(esQueryType.Text, "Select * from Companias Where cmp_Eliminada=0");
            cbSolic.Properties.DataSource = dbut.FillDataTable(esQueryType.Text, "Select * from SolicitadoPor Where sp_Eliminado=0");
        }

        private void FillCombosEnum()
        {

            rptcbUpdateMode.Properties.TextEditStyle=TextEditStyles.DisableTextEditor;
            rptcbUpdateMode.Properties.Items.Clear();
            rptcbUpdateMode.Properties.Items.AddRange(Enum.GetNames(typeof(SoftwareUpateMode)));

            rptcbSoftwareTipo.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            rptcbSoftwareTipo.Properties.Items.Clear();
            rptcbSoftwareTipo.Properties.Items.AddRange(Enum.GetNames(typeof(TerminalSoftwareTipo)));

        }

        private void FillLogLevels()
        {
            string[] names = Enum.GetNames(typeof (Contract.LevelLog));
            int i = 0;

            for (i = 0; i < names.Length; i++)
            {
                cbLevelLog.Properties.Items.Add(names[i]);
            }
        }

        private Contract.LevelLog NivelLogComboValue()
        {

            //Retrieve value of the selected item. 
            Type enumType = typeof(Contract.LevelLog);
            string selection = (string)cbLevelLog.EditValue;
            var value = (Contract.LevelLog)(Enum.Parse(enumType, selection));

            return value;

        }

        private bool Validar()
        {

            //Puerto
            Int32 SockerPort = Int32.Parse(txtServerPort.Text);
            if (SockerPort < 1 | SockerPort > 65535)
            {
                MessageBox.Show("Aviso. Numero de puerto no v�lido. Debe estar entre 1 y 65535", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtServerPort.Focus();
                return false;
            }

            if (ckEMail_Requiere.Checked)
            {
                if (txtEMail_User.Text == "")
                {
                    MessageBox.Show("Aviso. Nombre de Usuario no v�lido, para enviar el correo.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtEMail_User.Focus();
                    return false;
                }
            }

            if (cbLevelLog.EditValue == "")
            {
                MessageBox.Show("Aviso. Seleccione un nivel de Log.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                if (cmdCkExpand.Checked == false)
                {
                    cmdCkExpand.PerformClick();
                }
                cbLevelLog.Focus();
            }

            //TERMINAL UPDATE INFO
            foreach (var item in _terminalUpdateInfo)
            {
                if (!new FileVersion(item.VersionRequerida).MenorIgualQue(new FileVersion("99999.9999.9999.9999")));
                {
                    MessageBox.Show("Actualizaci�n Terminales. La versi�n indicada no est� bien definida.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }

            foreach (byte value1 in Enum.GetValues(typeof(TerminalSoftwareTipo)))
            {
                foreach (int value2 in Enum.GetValues(typeof (SoftwareUpateMode)))
                {
                    var count = (_terminalUpdateInfo.Count(it => it.UpdateMode == value2.NumToEnum(SoftwareUpateMode.Automatico) && it.SoftwareTipo == value1.NumToEnum(TerminalSoftwareTipo.GNavWin8Dsk)));
                    if (count>1)
                    {
                        MessageBox.Show(string.Format("Actualizaci�n Terminales. Modo '{0}' / Software '{1}'.\n\n Hay m�s de 1 especificaci�n indicada para este modo y Sotftware", value2.NumToEnum(SoftwareUpateMode.Automatico), value1.NumToEnum(TerminalSoftwareTipo.GNavWin8Dsk)), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }

                    if (count==0)
                    {
                        MessageBox.Show(string.Format("Actualizaci�n Terminales. Modo '{0}' / Software '{1}'.\n\n No hay especificaci�n indicada para este modo y Tipo de software", value2.NumToEnum(SoftwareUpateMode.Automatico), value1.NumToEnum(TerminalSoftwareTipo.GNavWin8Dsk)), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
            }

            return true;
        }

        private void cmdCkExpand_CheckedChanged(object sender, EventArgs e)
        {
            if (this.Width == FORM_WIDTH_EXTENDIDO)
            {
                this.Width = FORM_WIDTH_NORMAL;
            }
            else
            {
                this.Width = FORM_WIDTH_EXTENDIDO;
                tbAdvancedOptions.SelectedTabPage=tpGen;
            }

            if (cmdCkExpand.Checked)
            {
                cmdCkExpand.Image = Resources.Fecha_Izquierda_Doble.ToBitmap();
            }
            else
            {
                cmdCkExpand.Image = Resources.Fecha_Derecha_Doble.ToBitmap();
            }
        }

        private void cmdAceptar_Click(object sender, EventArgs e)
        {
            //Validar las opciones
            if (Validar() == false)
            {
                return;
            }

            // ***** TEMP ****************************
            txtUrlEndPoint.Text = ModuleConfig.AppConfig.Server.UrlEndPoint;
            txtEmpresaNombre.Text = ModuleConfig.AppConfig.Server.Empresa;

            TrackIntervalSend.Value = ModuleConfig.AppConfig.Server.ComponentWatchSendInterval;
            TrackIntervalReSend.Value = ModuleConfig.AppConfig.Server.ComponentWatchReSendInterval;
            TrackDiasPosDiario.Value = ModuleConfig.AppConfig.Server.DiarioPosicionesMantenerDias;

            //Rellenar Datos de los Combos para ServiciosEspeciales
            FillCombosServiciosEspeciales();
            FillCombosEnum();


            //Email
            txtEmailFrom.Text = ModuleConfig.AppConfig.Server.EMail.EmailFrom;
            txtEMails.Text = ModuleConfig.AppConfig.Server.EMail.EmailListaTo;

            ckEMail_Requiere.Checked = (ModuleConfig.AppConfig.Server.EMail.EmailSecurityUser != "");
            if (ckEMail_Requiere.Checked)
                frmEmailUser.Visible = true;

            txtEMail_SMTP.Text = ModuleConfig.AppConfig.Server.EMail.EmailServerSmtp;
            txtEMail_User.Text = ModuleConfig.AppConfig.Server.EMail.EmailSecurityUser;
            txtEMail_Pwd.Text = ModuleConfig.AppConfig.Server.EMail.EmailSecurityPwd;

            cbLevelLog.EditValue = Enum.GetName(typeof(Contract.LevelLog), ModuleConfig.AppConfig.Server.LevelLog);

            if (!ModuleConfig.Global.SessionAdminActive)
            {
                cmdCkExpand.Enabled = false;
                txtUrlEndPoint.Enabled = false;
                txtServerPort.Enabled = false;
                txtMaxClientes.Enabled = false;
                cmdThrowIncid.Enabled = false;
            }

            //TERMINAL UPDATE INFO
            opUpdateMode.EditValue = (byte)ModuleConfig.AppConfig.Server.TerminalUpdateMode;

            _terminalUpdateInfo = (ListaTerminalUpdateInfo)ModuleConfig.AppConfig.Server.TerminalUpdate.Clone();
            bsTerminalUpdate.DataSource = _terminalUpdateInfo;
            // ***** TEMP ****************************



            //Asignamos de Nuevo las opciones, a los datos

            ModuleConfig.AppConfig.Server.UrlEndPoint = txtUrlEndPoint.Text;
            ModuleConfig.AppConfig.Server.Empresa = txtEmpresaNombre.Text;

            ModuleConfig.AppConfig.Server.ComponentWatchSendInterval = TrackIntervalSend.Value;
            ModuleConfig.AppConfig.Server.ComponentWatchReSendInterval = TrackIntervalReSend.Value;
            ModuleConfig.AppConfig.Server.DiarioPosicionesMantenerDias = TrackDiasPosDiario.Value;

            //Email
            ModuleConfig.AppConfig.Server.EMail.EmailFrom = txtEmailFrom.Text;
            ModuleConfig.AppConfig.Server.EMail.EmailListaTo = txtEMails.Text;

            ModuleConfig.AppConfig.Server.EMail.EmailServerSmtp = txtEMail_SMTP.Text;
            ModuleConfig.AppConfig.Server.EMail.EmailSecurityUser = txtEMail_User.Text;
            ModuleConfig.AppConfig.Server.EMail.EmailSecurityPwd = txtEMail_Pwd.Text;


            //Opciones Avanzadas
            ModuleConfig.AppConfig.Server.LevelLog = NivelLogComboValue();

            //TERMINAL UPDATE INFO
            ModuleConfig.AppConfig.Server.TerminalUpdateMode = ((byte)opUpdateMode.EditValue).NumToEnum(SoftwareUpateMode.Automatico);
            
            try
            {
                ModuleConfig.Save();

                _log.SetDefaultLogLevel((LevelLog) ModuleConfig.AppConfig.Server.LevelLog); //Actualizamos el nivel de log.
                _log.SetLogLevel((LevelLog) ModuleConfig.AppConfig.Server.LevelLog); //Actualizamos el nivel de log.

                MessageBox.Show("Algunas opciones no surtir�n efecto hasta que no reinicie el Servicio.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (System.Exception ex)
            {
                ManejarExceptions.ManejarException(new System.Exception("Error. Guardar datos de la configuraci�n.",ex),ExceptionErrorLevel.Error, false, true);
                return;
            }

            result =OperacionResult.OK;
            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void ckEMail_Requiere_CheckedChanged(object sender, EventArgs e)
        {
            frmEmailUser.Visible = ckEMail_Requiere.Checked;
        }

        private void cmdThrowIncid_Click(object sender, EventArgs e)
        {
            var f = new gcperu.Server.UI.Paneles.Forms.IncidenciasAlertForm();
            f.ShowDialog();
        }

       
    }
}