using System;
using System.Linq;
using System.Windows.Forms;
using Gurock.SmartInspect;
using Microsoft.SqlServer.MessageBox;

namespace gcperu.Server.UI.Helper
{
    public static class ExceptionHandler
    {

        public static bool ShowMensajeBox(System.Exception ex, Gurock.SmartInspect.Level Severity)
        {
            ExceptionMessageBoxSymbol _symbolIco = ExceptionMessageBoxSymbol.Information;

            if (Severity ==Level.Message)
            {
                _symbolIco = ExceptionMessageBoxSymbol.Information;
            }
            //ORIGINAL LINE: Case SeverityLevelEnum.Error
            else if (Severity ==Level.Error)
            {
                _symbolIco = ExceptionMessageBoxSymbol.Error;
            }
            //ORIGINAL LINE: Case SeverityLevelEnum.Critical
            else if (Severity ==Level.Fatal)
            {
                _symbolIco = ExceptionMessageBoxSymbol.Stop;
            }

            var box = new ExceptionMessageBox(ex, ExceptionMessageBoxButtons.OK, _symbolIco);
            box.Caption = "GITS Comunica. Gestor de Comunicaciones con Terminales Embarcados";
            try
            {
                box.Show(GetMainForm());
            }
            catch (System.Exception ex1)
            {
            }


            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return false;
        }

        private static Form GetMainForm()
        {
            try
            {
                return Application.OpenForms.OfType<Mainfm>().Single();
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }

    }
}