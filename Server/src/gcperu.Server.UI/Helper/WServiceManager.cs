using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using gcperu.Server.Core.Exceptions;
using gitspt.global.Core.IoC;
using gitspt.global.Core.Log;
using Gurock.SmartInspect;

namespace gcperu.Server.UI.Helper
{
    public class WServiceManager:IDisposable
    {
        private System.ServiceProcess.ServiceController _controller;
        private string _serviceName;
        private ISSTraceLog _log;

        public WServiceManager(string serviceName)
        {
            _serviceName = serviceName;
            _controller = new ServiceController(serviceName);
            _log = Ioc.Container.Get<ISSTraceLog>();
        }

        public ServiceControllerStatus Status
        {
            get
            {
                try
                {
                    _controller.Refresh();
                    return _controller.Status;
                }
                catch (System.Exception e)
                {
                    return 0;
                }
            }
        }

        public bool Start()
        {
            try
            {
                _controller.Start();
                byte intentos = 0;
                while (_controller.Status != ServiceControllerStatus.Running && intentos<=20)
                {
                    Thread.Sleep(500);
                    intentos += 1;
                }
                return (_controller.Status == ServiceControllerStatus.Running);
            }
            catch (System.Exception e)
            {
                var ex = new GCException(string.Format("ERROR INICIAR EL SERVICIO {0}", _serviceName),e);
                _log.LogException(ex);

                Helper.ExceptionHandler.ShowMensajeBox(ex, Level.Error);

                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                _controller.Stop();
                byte intentos = 0;
                while (_controller.Status!=ServiceControllerStatus.Stopped && intentos<=20)
                {
                    Thread.Sleep(500);
                    intentos += 1;
                }

                return (_controller.Status != ServiceControllerStatus.Stopped);
            }
            catch (System.Exception e)
            {
                var ex = new GCException(string.Format("ERROR DETENER EL SERVICIO {0}", _serviceName), e);
                _log.LogException(ex);

                Helper.ExceptionHandler.ShowMensajeBox(ex, Level.Error);

                return false;
            }
        }

        public void Dispose()
        {
            _controller = null;
        }
    }
}