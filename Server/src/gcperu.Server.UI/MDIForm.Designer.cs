﻿namespace gcperu.Server.UI
{
    partial class MDIForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mdiTabbed = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mdiTabbed)).BeginInit();
            this.SuspendLayout();
            // 
            // mdiTabbed
            // 
            this.mdiTabbed.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.mdiTabbed.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.Always;
            this.mdiTabbed.MdiParent = this;
            // 
            // MDIForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1241, 720);
            this.IsMdiContainer = true;
            this.Name = "MDIForm";
            this.Text = "GITS Comunica Server";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MDIForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mdiTabbed)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager mdiTabbed;

    }
}



