﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using GComunica.Core.Exceptions;
using GComunica.Core.Log;
using GComunica.CoreServer;
using GComunica.CoreServer.Config;

namespace GComunica.Server.WS
{
    public partial class StartForm : Form
    {

        #region Declaraciones

        //public ServicioEpesNoThread ServEpes = null;

        #endregion

        public StartForm()
        {
            InitializeComponent();

            try
            {
                //Iniciamos el Proceso de Arranque
                if (!InitializationProcess.Initialize())
                {
                    var e = new GCException("ERROR OCURRIDO EN EL PROCESO DE INICIALIZACIÓN. NO SE PUEDE CONTINUAR CON LA EJECUCIÓN DEL SERVICIO");
                    TraceLog.LogException(e);   //HACE QUE EL SERVICIO, SE DETENGA.
                }

                ServerSetting.Componentes.RunComponentes();
                ServerSetting.SocketHandle.StartListening();
            }
            catch (Exception e)
            {
                TraceLog.LogException(new GCException("ERROR EN INICIALIZACION. DETALLE DEL ERROR.", e));
            }
            
        }

        private void StartForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            TraceLog.LogDebug("StartForm.Closed. Dispose Componentes y StopListening");
        }
    }
}
