using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using GComunica.Core.IPC;
using GComunica.Core.Log;
using GComunica.CoreServer.Config;
using Gurock.SmartInspect;

namespace GComunica.Server.WS
{
    public class MessageServerIPC : IMessageServer
    {
        private Thread thupdate = null;

        public bool SendMessage(MessagesServerTypesEnum MensajeTipo)
        {
            TraceLog.LogDebug(string.Format("IPC.SERVER: MessageRecibido:{0}", MensajeTipo.ToString()));

            switch (MensajeTipo)
            {
                case MessagesServerTypesEnum.StartListen:

                    if (!ServerSetting.SocketHandle.IsListening)
                    {
                        ServerSetting.SocketHandle.StartListening();
                        return true;
                    }
                    else
                    {
                        TraceLog.LogWarning("EL SOCKET EST� ESCUCHANDO. IMPOSIBLE INICIAR DE NUEVO.");
                    }

                    return false;

                case MessagesServerTypesEnum.StopListen:

                    
                    if (ServerSetting.SocketHandle.IsListening)
                    {
                        ServerSetting.SocketHandle.StopListening();
                        return true;
                    }
                    else
                    {
                        TraceLog.LogWarning("EL SOCKET NO EST� ESCUCHANDO. IMPOSIBLE DETENER.");
                    }

                    return false;

                case MessagesServerTypesEnum.RebootListen:

                    ServerConfig.Load();
                    if (ServerSetting.SocketHandle.IsListening)
                    {
                        ServerSetting.SocketHandle.StopListening();
                    }
                    ServerSetting.SocketHandle.StartListening();
                    return true;

                case MessagesServerTypesEnum.RefreshConfiguracion:
                    CoreServer.Helper.ListaTiposActividadAmbulancia.ListaFechaLoaded = null;
                    CoreServer.Helper.ListaMobileNetworkProfile.ListaTerminal = null;
                    CoreServer.Helper.ListaIncidencia.ListaIncidParaTerminal = null;
                    CoreServer.Helper.ListaServicioEspecialTipo.ListaFechaLoaded = null;

                    ServerConfig.Load();
                    return true;

                case MessagesServerTypesEnum.SocketStatus:
                    return ServerSetting.SocketHandle.IsListening;

                default:
                    return false;
            }
        }

        public bool SendMessage(MessageServerInfo mensaje)
        {
            TraceLog.LogDebug(string.Format("IPC.SERVER: MessageRecibido:{0}", mensaje.Tipo.ToString()));

            switch (mensaje.Tipo)
            {
               
                case MessagesServerTypesEnum.DisconnectTerminal:
                    return DisconnectTerminales(mensaje.ListaDisconnect);

                default:
                    return SendMessage(mensaje.Tipo);
            }
            
        }

        public bool VerifyComunica()
        {
            return true;
        }

        #region Privadas

        private bool DisconnectTerminales(List<decimal> listaDisconnect)
        {
            //Recorrer todos los terminales a desconectar
            byte errores = 0;

            TraceLog.LogTraceStar("IPC: DesconectarTerminales. START ...");
            foreach (decimal vehid in listaDisconnect)
            {
                try
                {
                    if (!ServerSetting.SocketHandle.DiscconectTerminal(vehid))
                    {
                        errores += 1;
                    }
                }
                catch (Exception ex)
                {
                    errores += 1;
                }
            }

            TraceLog.LogTraceStop(string.Format("IPC: DesconectarTerminales. END. Errores:{0}", errores));

            if (errores==0)
            {
                TraceLog.Main.LogColored(Level.Message, Color.Azure, "IPC:Desconectar. TODOS LOS TERMINALES HAN SIDO DESCONECTADOS.");
                return true;
            }

            return false;
        }

        #endregion

    }


 }