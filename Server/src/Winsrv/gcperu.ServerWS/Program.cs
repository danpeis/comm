﻿using System.ServiceProcess;

namespace GComunica.Server.WS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] { new ServerWS1() };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
