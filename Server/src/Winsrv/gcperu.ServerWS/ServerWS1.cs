﻿using System;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading;
using GComunica.Core.Exceptions;
using GComunica.Core.IPC;
using GComunica.Core.Log;

namespace GComunica.Server.WS
{
    public partial class ServerWS1 : ServiceBase
    {
        private Thread thserver1 = null;
        private Server1 classstart = new Server1();
        /// <summary>
        /// Utilizado para hospedar, el servicio wcf, para escuchar las peticiones de los clientes.
        /// </summary>
        private ServiceHost _hostServerListen;

        public ServerWS1()
        {
            InitializeComponent();
        }

        public void TestOnStart()
        {
            OnStart(new string[0]);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                TraceLog.LogTraceStart("ServerWS1.OnStart... START");

                thserver1 = new Thread(new ThreadStart(classstart.Initialize));
                thserver1.Start();

                InitializePipeChannelServer();

                TraceLog.LogTraceStop("ServerWS1.OnStart. END");
            }
            catch (Exception e)
            {
                TraceLog.LogInformation("Asdfasfasdf");
            }
        }

        private void InitializePipeChannelServer()
        {
            TraceLog.LogDebug(string.Format("ServerWS1. Configuramos el WCF Service[GitsNavegaSentry] en direcion [net.pipe://localhost/{0}]", GComunica.CoreServer.Constantes.SERVER1_PIPE_ENDPOINT_ADDRESS));
            _hostServerListen = new ServiceHost(typeof(MessageServerIPC), new Uri[] { new Uri("net.pipe://localhost") });
            _hostServerListen.AddServiceEndpoint(typeof(IMessageServer), new NetNamedPipeBinding(), GComunica.CoreServer.Constantes.SERVER1_PIPE_ENDPOINT_ADDRESS);

            _hostServerListen.Open();
            TraceLog.LogDebug("ServerWS1. WCF Service[GCServer1] abierto OK. Servicio en Escucha.");
        }

        protected override void OnStop()
        {
            TraceLog.LogTraceStart("ServerWS1.OnStop");

            classstart.RequestStop();
            Thread.Sleep(1000);

            byte intentos = 1;
            while (!classstart.Finalized && intentos <= 20)
            {
                thserver1.Join(500);
                intentos += 1;
            }

            if (thserver1.IsAlive)
            {
                thserver1.Abort();
                TraceLog.LogWarning("Thread Server1.Abortado por superar tiempo maximo de espera.");
            }
            else
                TraceLog.LogInformation("Thread Server1.Detenido de forma correcta.");

            TraceLog.LogInformation("Cerrando el WCF Service[GCServer1]...");
            if (_hostServerListen != null)
            {
                try
                {
                    TraceLog.LogInformation("Cerrando el WCF Service[GCServer1]...");
                    _hostServerListen.Close();
                    _hostServerListen = null;
                    TraceLog.LogInformation("Cerrando WCF Service[GCServer1] OK");
                }
                catch (Exception e)
                {
                    TraceLog.LogException(new GCException("ERROR CERRAR EL WCF SERVICE[GCServer1]", e));
                }
            }

            TraceLog.LogTraceStop("ServerWS1.OnStop");
        }

        private void UnhandledException()
        {
            System.AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            TraceLog.LogInformation("UnhandledException");
            TraceLog.LogException((Exception)e.ExceptionObject);
        }
    }
}
