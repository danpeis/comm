﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using GeoFramework.Licensing;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("GCServerWS")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("AMCOEX")]
[assembly: AssemblyProduct("GCServerWS")]
[assembly: AssemblyCopyright("Copyright © AMCOEX 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("cc410409-0ba7-44df-a590-6450fb9db5f3")]

#region GeoFrameworks License Keys

[assembly: GeoFrameworksLicense(
    //GIS
  "14XF8A-X2B25J-AAB2Y1-X3XA1X-FGUXX3-3NYUW6",
    //GPS 2.4
  "14XFGG-X2B25J-GAB2Y1-XAXG1X-FGUXB3-3JYUWN")]

#endregion

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.5.0514")]
[assembly: AssemblyFileVersion("1.5.0514")]
