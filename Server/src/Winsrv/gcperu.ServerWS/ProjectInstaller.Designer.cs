﻿namespace GCServerWS
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.GC1ProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.GC1ServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // GC1ProcessInstaller
            // 
            this.GC1ProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.GC1ProcessInstaller.Password = null;
            this.GC1ProcessInstaller.Username = null;
            // 
            // GC1ServiceInstaller
            // 
            this.GC1ServiceInstaller.DisplayName = "GITS Comunica. GestorComunicaciones";
            this.GC1ServiceInstaller.ServiceName = "GitsComunicaServer1";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.GC1ProcessInstaller,
            this.GC1ServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller GC1ProcessInstaller;
        private System.ServiceProcess.ServiceInstaller GC1ServiceInstaller;
    }
}