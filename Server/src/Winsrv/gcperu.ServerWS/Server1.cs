using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using GComunica.Core.Exceptions;
using GComunica.Core.Log;
using GComunica.CoreServer.Config;

namespace GComunica.Server.WS
{
    public class Server1
    {
        private StartForm fstart = null;
        private string _fileLog;
        private Process _prcConsole;
        private bool _finalized;

        public bool Finalized
        {
            get { return _finalized; }
        }

        public Server1()
        {
        }

        private void Application_ThreadExit(object sender, EventArgs e)
        {
            TraceLog.LogTraceStart("ClassStart.Application_ThreadExit " + System.Threading.Thread.CurrentThread.ManagedThreadId);
            //fstart.ServEpes.Stop();
            TraceLog.LogTraceStop("ClassStart.Application_ThreadExit " + System.Threading.Thread.CurrentThread.ManagedThreadId);
        }

        public void Initialize()
        {
            /************** CONFIGURACION DEL LOG ******************************/
            try
            {
                TraceLog.LoadSmartInpectFile(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Constantes.FILE_CONFIG_SMARTINSPECT)); //Cargamos el fichero de Configuracion
                TraceLog.LogInformation("MAIN. GCSERVER. INICIA ...");

                _fileLog = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                //Load de Log configuration
                //TraceLog.LoadSmartInpectFile(_fileLog, "");
            }
            catch (Exception e)
            {
                throw new GCException("ERROR CONFIGURACION DEL LOG.", e);
            }

            /* NO FUNCIONA, PORQUE SE QUEDA EN SEGUNDO PLANO.
            //Arrancar el SmartInspect.
            if (!ProcesoIsExecuting("SmartInspectConsole"))
            {
                ExecSmartInspectConsole(TraceLog.GetTcpPortListen());
            }
            else
            {

            }
             * */

            TraceLog.LogDebug("Initialize Log");

            /************** FIN CONFIG LOG ******************************/
            
            // Instantiate a new instance of Form1.
            fstart = new StartForm();
            fstart.Text = "GCServer invisible running form";
            //Lanzado, antes de que la aplicacion salga
            Application.ThreadExit += new EventHandler(Application_ThreadExit);

            //Inicia el loop de eventos. No llama a ningun formulario
            Application.Run();

        }

        public void RequestStop()
        {
            _finalized = false;
            TraceLog.LogTraceStar("Server1. RequestStop. ...");
            TraceLog.LogInformation("ClassStart.RequestStop");
            ServerSetting.SocketHandle.StopListening();
            ServerSetting.Componentes.DisposeComponentes();
            fstart.Close();
            Application.Exit();
            _finalized = true;
            TraceLog.LogTraceStop("Server1. RequestStop. OK");
        }

        #region Private

        private bool ProcesoIsExecuting(string ProcessName)
        {
            var listaprocesos = Process.GetProcesses();

            foreach (Process _proceso in listaprocesos)
            {
                if (_proceso.ProcessName == ProcessName)
                    return true;
            }

            return false;
        }

        private void ExecSmartInspectConsole(int Port)
        {
            //Gurock Software\SmartInspect Professional\SmartInspectConsole.exe
            //Lanzar la ejecución del Programa.

            var cmdargs = "";
            if (Port>0)
            {
                cmdargs = string.Format("/tcpport:{0}", Port);
            }

            var infostart = new ProcessStartInfo()
                                {
                                    FileName =
                                        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
                                                     @"Gurock Software\SmartInspect Professional\SmartInspectConsole.exe"),
                                    Arguments = cmdargs,
                                    ErrorDialog = false,
                                    UseShellExecute = true,
                                    WindowStyle = ProcessWindowStyle.Maximized
            };

            try
            {
                _prcConsole = Process.Start(infostart);
                Thread.Sleep(3000);
            }
            catch (Exception e)
            {
                TraceLog.LogException(new GCException("ERROR INICIAR CONSOLA SmartInspect.",e));
            }
        }

        #endregion
    }
}