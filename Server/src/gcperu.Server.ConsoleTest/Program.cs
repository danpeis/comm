﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Amcoex.Communications.SignalR.Packaging;
using gcperu.Server.ConsoleTest;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;

namespace gcperu.Server.ConsoleTest
{
    class Program
    {
        public readonly static SignalRPackageServer PackageServer = new SignalRPackageServer();

        static void Main(string[] args)
        {
            var p = new GCPeru.Server.Core.Managers.PackageManager();

            var url = "http://192.168.50.113:5678";
            using (WebApp.Start<Startup>(url))
            {
                //var context = GlobalHost.ConnectionManager.GetHubContext<PackageHub, IPackageHandler>();
                Console.WriteLine("Server running on {0}", url);

                PackageServer.DataPackagesReceived.Subscribe(
                    tupla => Debug.WriteLine(string.Format("ConnectionID: '{0}' | PackageData: '{1}'",tupla.Item1,tupla.Item2.Id)));

                while (Console.ReadLine() != "quit")
                {
                }
            }
        }

        private static string ScanForDefaultConfig(Assembly assembly)
		{
			if(assembly == null)
				assembly = System.Reflection.Assembly.GetEntryAssembly ();

			string[] res;
			try {
				// this might fail for the 'Anonymously Hosted DynamicMethods Assembly' created by an Reflect.Emit()
				res = assembly.GetManifestResourceNames ();
			} catch {
				// for those assemblies, we don't provide a config
				return null;
			}
			var dconf_resource = res.Where (r =>
					r.EndsWith ("default.conf", StringComparison.OrdinalIgnoreCase) ||
					r.EndsWith ("default.json", StringComparison.OrdinalIgnoreCase) ||
					r.EndsWith ("default.conf.json", StringComparison.OrdinalIgnoreCase))
				.FirstOrDefault ();
			
			if(string.IsNullOrEmpty (dconf_resource))
				return null;
		
			var stream = assembly.GetManifestResourceStream (dconf_resource);
			string default_json = new StreamReader(stream).ReadToEnd ();
			return default_json;
		}
	}

    
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            //GlobalHost.DependencyResolver.Register(
            //    typeof(PackageHub),
            //    () => new PackageHub(Program.PackageServer));

            GlobalHost.DependencyResolver.Register(typeof(SignalRPackageServer), () => new SignalRPackageServer());

            app.MapSignalR(new HubConfiguration { EnableDetailedErrors = true });
        }
    }
}
