﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using Amcoex.Communications;
using Newtonsoft.Json;

namespace gcperu.Contract.Frames
{
    [DataContract]
    public class FrameJson : IFrame
    {
        [IgnoreDataMember]
        private IModel _model = null;

        public FrameJson()
        {
        }

        public FrameJson(IModel model)
        {
            if (model==null)
                throw new ArgumentNullException("model");

            if (!FrameStatic.ModelsDatatypes.Contains(model.GetType().FullName))
                throw new ArgumentException("Model type is not valid");

            DataType = model.GetType().FullName;
            Model = model;
        }

        [DataMember]
        public string DataType { get; private set; }

        [IgnoreDataMember]
        public IModel Model {
            get
            {
                return _model ?? (_model = (IModel) JsonConvert.DeserializeObject(Encoding.UTF8.GetString(ModelData), Assembly.GetExecutingAssembly().GetType(DataType)));   
            }
            private set
            {
                _model = value;
                ModelData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(value));
            }
        }

        [DataMember]
        public byte[] ModelData { get; private set; }

        private static class FrameStatic
        {
            public static readonly IEnumerable<string> ModelsDatatypes;

            static FrameStatic()
            {
                ModelsDatatypes = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.Namespace != null && t.Namespace.StartsWith("gcperu.Contract.Models")).Select(t => t.FullName).ToList();
            }
        }
    }
}