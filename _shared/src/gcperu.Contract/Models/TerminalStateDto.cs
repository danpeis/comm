﻿using System.Runtime.Serialization;

namespace gcperu.Contract.Models
{
    [DataContract]
    public class TerminalStateDto : ModelDtoBase
    {
        private GpsPosition _fixPosition;

        public TerminalStateDto()
        {
        }

        public TerminalStateDto(string id) : base(id)
        {
        }

        [DataMember]
        public double Odometro { get; private set; }

        [DataMember]
        public double Speed { get; private set; }

        [DataMember]
        public double SpeedMax { get; private set; }

        /// <summary>
        /// Indica si esta en modo de Parada
        /// </summary>
        [DataMember]
        public bool StopMode { get; private set; }

        [DataMember]
        public GpsPosition FixPosition
        {
            get { return _fixPosition ?? (_fixPosition = new GpsPosition()); }
        }

        /// <summary>
        /// Indica el estado de Funcionamiento a nivel del Dispositivo y la aplicación. 
        /// </summary>
        [DataMember]
        public OperationalDeviceStatus OperationalDeviceStatus { get; private set; }

        /// <summary>
        /// Indica el estado del dispositivo a nivel funcional de la aplicacion. Ej: Funcionamiento correo o algun error.
        /// </summary>
        [DataMember]
        public FunctionalDeviceStatus FunctionalDeviceStatus { get; private set; }


    }

    [DataContract]
    public class GpsPosition
    {
        /// <summary>
        /// Indica la calidad de la posicion. En caso de que no exista una posicion valida, el tipo sera "FixInvalid" y si no se puede saber o algun problema "Unknown"
        /// </summary>
        [DataMember]
        public GpsFixMode FixMode { get; set; }

        [DataMember]
        public double Latitud { get; set; }
        
        [DataMember]
        public double Longitud { get; set; }
        
        /// <summary>
        /// Altitud en metros
        /// </summary>
        [DataMember]
        public double Altitud { get; set; }

        /// <summary>
        /// Heading o rumbo en grados decimales. (0-360º)
        /// </summary>
        [DataMember]
        public double Rumbo { get; set; }
    }

    //public class 
}