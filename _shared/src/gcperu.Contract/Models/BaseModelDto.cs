﻿using System;
using System.Runtime.Serialization;
using Amcoex.Communications;

namespace gcperu.Contract.Models
{
    [DataContract]
    [KnownType(typeof(ConfigurationDto))]
    [KnownType(typeof(IdentityDto))]
    [KnownType(typeof(IncidenceDto))]
    [KnownType(typeof(RequestToTerminalDto))]
    [KnownType(typeof(TerminalStateDto))]
    public abstract class ModelDtoBase : IModel
    {
        protected ModelDtoBase()
        {
        }

        protected ModelDtoBase(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException("id");
            Id = id;
            TimeStamp = DateTime.Now;
        }

        /// <summary>
        /// Identificación del registro. Nº de secuencia o cualquier dato que identifique univocamente un registro.
        /// </summary>
        [DataMember]
        public string Id { get; private set; }

        /// <summary>
        /// Marca de tiempo
        /// </summary>
        [DataMember]
        public DateTime TimeStamp { get; private set; }

        
    }
}