﻿using System.Runtime.Serialization;

namespace gcperu.Contract.Models
{
    [DataContract]
    public class ConfigurationDto : ModelDtoBase
    {
        public ConfigurationDto()
        {
        }

        public ConfigurationDto(string id, string DataJson)
            : base(id)
        {
        }

        public string DataJson { get; private set; } 
    }
}