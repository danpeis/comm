﻿using System;
using System.Runtime.Serialization;

namespace gcperu.Contract.Models
{
    [DataContract]
    public class IdentityDto : ModelDtoBase
    {
        public IdentityDto()
        {
        }

        public IdentityDto(string id, string numSerie, string iMEI, string numSerieSim, string versionSoftware, byte softwareType) : base(id)
        {
            NumSerie = numSerie;
            IMEI = iMEI;
            NumSerieSim = numSerieSim;
            VersionSoftware = versionSoftware;
            SoftwareType = softwareType;
        }

        [DataMember]
        public string NumSerie { get; private set; }

        [DataMember]
        public string IMEI { get; private set; }

        [DataMember]
        public string VersionSoftware { get; private set; }

        [DataMember]
        public string NumSerieSim { get; private set; }

        [DataMember]
        public string MobileNetworkProfileId { get; private set; }

        [DataMember]
        public byte SoftwareType { get; private set; }
    }
}