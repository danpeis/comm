﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace gcperu.Contract.Models
{
    [DataContract]
    public class IdentityResponseDto : ModelDtoBase
    {
        private List<SoftwareRequiredInfo> _softwareVersionsRequerired;

        public IdentityResponseDto()
        {
        }

        public IdentityResponseDto(string id, bool registered, DateTime datetimeServer, string vehiclePairedIdentity)
            : base(id)
        {
            Registered = registered;
            DatetimeServer = datetimeServer;
            VehiclePairedIdentity = vehiclePairedIdentity;
        }

        [DataMember]
        public bool Registered { get; private set; }
        
        /// <summary>
        /// Maxima Numero de Secuencia de Trama recibida en el Servidor, enviada por el Terminal.
        /// </summary>
        [DataMember]
        public string MaxNumSecuencia { get; set; }

        /// <summary>
        /// Marca de Tiempo,Fecha y Hora del Servidor. Utilizada para sincronización.
        /// </summary>
        [DataMember]
        public DateTime DatetimeServer { get; private set; }

        /// <summary>
        /// Identificación del Vehiculo al que esta asociado este Terminal. Normalmente sera la 'Matricula'
        /// </summary>
        [DataMember]
        public string VehiclePairedIdentity { get; private set; }

        /// <summary>
        /// Lista de las versiones requeridas de los distintos modulos que conforman el software
        /// </summary>
        [DataMember]
        public List<SoftwareRequiredInfo> SoftwareVersionsRequerired
        {
            get { return _softwareVersionsRequerired ?? (_softwareVersionsRequerired=new List<SoftwareRequiredInfo>()); }
        }
    }


    [DataContract]
    public class SoftwareRequiredInfo
    {
        [DataMember]
        public TerminalSoftwareModule Module { get; set; }

        [DataMember]
        public string Version { get; set; }
    }
}
