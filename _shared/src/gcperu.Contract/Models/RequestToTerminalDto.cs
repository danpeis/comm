﻿using System.Runtime.Serialization;
using gcperu.Contract.Info;

namespace gcperu.Contract.Models
{
    [DataContract]
    public class RequestToTerminalDto : ModelDtoBase
    {
        public RequestToTerminalDto()
        {
        }

        public RequestToTerminalDto(string id, TerminalRequestDataInfo data)
            : base(id)
        {
            Data = data;
        }

        /// <summary>
        /// Nº de la Peticion del terminal. Necesario para identificar las peticiones enviadas en un Paquete.
        /// </summary>
        public int PetitionId { get; set; }

        public TerminalRequestDataInfo Data { get; private set; }
    }
}