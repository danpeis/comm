﻿using System.Runtime.Serialization;

namespace gcperu.Contract.Models
{
    [DataContract]
    public class IncidenceDto : ModelDtoBase
    {
        public IncidenceDto()
        {
        }

        public IncidenceDto(string id, string code, string description) : base(id)
        {
            Code = code;
            Description = description;
        }

        public string Code { get; private set; }

        public string Description { get; private set; }
    }
}
