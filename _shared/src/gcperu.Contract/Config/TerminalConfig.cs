﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Amcoex.Communications;
using gcperu.Contract.Models;

namespace gcperu.Contract.Config
{

    //public interface ITerminalConfigValidator
    //{
    //    bool Validate();
    //}

    public class TerminalConfig 
    {

        private GeneralConfig _general;
        private Behaviors _behaviors;
        private List<EndPoint> _endpointsCol;
        private LogShippingConfig _logShipping;
        private PatchConfig _patchConfig;

        #region Properties

        /// <summary>
        /// Configuraciones sobre comportamientos
        /// </summary>
        public GeneralConfig General
        {
            get { return _general ?? (_general = new GeneralConfig()); }
        }

        /// <summary>
        /// Configuraciones sobre comportamientos
        /// </summary>
        public Behaviors Behavior
        {
            get { return _behaviors ?? (_behaviors = new Behaviors()); }
        }

        /// <summary>
        /// Lista de Endpoints. Para la configuración del comportamiento <see cref="TerminalConfig.Behaviors.BehaviorEndpointConfig"/>
        /// </summary>
        public List<EndPoint> EndPoins
        {
            get { return _endpointsCol ?? (_endpointsCol = new List<EndPoint>()); }
        }

        /// <summary>
        /// Configuracion del Envio de ficheros LOG generados por la aplicación a un Servidor FTP.
        /// <para>El comportamiento de la creación de log se configura en "Behavior.Logging"</para>
        /// </summary>
        public LogShippingConfig LogShipping
        {
            get { return _logShipping ?? (_logShipping = new LogShippingConfig()); }
        }

        /// <summary>
        /// Configuracion de los distintos programas y su configuración de aplicar Parches y origenes de los ficheros de parcheo.
        /// </summary>
        public PatchConfig PatchSoftware
        {
            get { return _patchConfig ?? (_patchConfig = new PatchConfig()); }
        }


        #endregion

        
        #region Nested Classes

        public class GeneralConfig
        {

            /// <summary>
            /// Url de la pagina web que se debe abrir en el Marco web de la aplicación
            /// </summary>
            public string UrlWebFrame { get; set; }

        }

        public class PatchConfig
        {
            private PatchSoftwareConfig _patchUI;
            private PatchSoftwareConfig _patchWorkerWS;
            private PatchSoftwareConfig _patchSentryWS;

            public PatchConfig()
            {
                //UI-WSWorker
                UIandWSWorker.Server.ServerAddress = "ftp.amcoex.es";
                UIandWSWorker.Server.DefaultRemotePath = "/httpdocs/downloads/amcoex/gitsnavega-peru/patchs/ui-wsworker";
                UIandWSWorker.Server.AuthUser = "soft2867@amcoex.es";
                UIandWSWorker.Server.AuthPass = "3141516";

                UIandWSWorker.TempFolder = "gcperu-patchs-ui";
                UIandWSWorker.TimeoutApply = 60;
                UIandWSWorker.FilewatchToApplyPatch = "gcperu-ui.exe";
                UIandWSWorker.PatternPatchFile = "gcperu-ui_patch_*.exe";

                //WS-Sentry
                //En principio no se contempla la actualización de este componente. Se incluye su configuración por si hiciera falta en un futuro.
            }

            /// <summary>
            /// UI and Windows Service Worker. Parche conjunto para los 2 modulos
            /// </summary>
            public PatchSoftwareConfig UIandWSWorker
            {
                get { return _patchUI ?? (_patchUI = new PatchSoftwareConfig()); }
            }

            /// <summary>
            /// Windows Service Centinela
            /// </summary>
            public PatchSoftwareConfig SentryWS
            {
                get { return _patchSentryWS ?? (_patchSentryWS = new PatchSoftwareConfig()); }
            }

        }

        public class PatchSoftwareConfig
        {
            private FTPServer _ftpserver;

            public FTPServer Server
            {
                get { return _ftpserver ?? (_ftpserver = new FTPServer()); }
            }

            /// <summary>
            /// (Optional) Patron o Expression Regular del Nombre de los Ficheros que comforman los parches de este Software
            /// <para>Ej: GNPERU-Terminal.</para>
            /// </summary>
            public string PatternPatchFile { get; set; }

            /// <summary>
            /// Nombre del Fichero (incluida extension) de referencia para el parcheo. Fichero del cual obtener la version del software.
            /// </summary>
            public string FilewatchToApplyPatch { get; set; }

            /// <summary>
            /// Timeout para aplicar parche (Tiempo|seg). Si este tiempo se supera, se considera que la aplicación del Parche no ha tenido exito.
            /// </summary>
            public int TimeoutApply { get; set; }

            /// <summary>
            /// Directorio temporal donde guardar los ficheros de parche descargados
            /// </summary>
            public string TempFolder { get; set; }
        }


        public class EndPoint
        {
            /// <summary>
            /// Identificador del destino de Conexion
            /// </summary>
            public int Id { get; set; }

            /// <summary>
            /// Url destino para la Conexion
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// Nombre amigable o descriptivo del destino de conexion
            /// </summary>
            public string Name { get; set; }

        }

        public class FTPServer
        {
            public FTPServer()
            {
                Port = 21;
                Protocol =FTPProtocol.FTP;
                Encryption=FTPEncryption.Plain;
                AuthenticationMode=FTPAuthenticationMode.Normal;
                TransferMode=FTPTransferMode.Active;
            }

            /// <summary>
            /// Direccion del Servidor. Ej: ftp.amcoex.es
            /// </summary>
            public string ServerAddress { get; set; }
            /// <summary>
            /// Nº del Puerto. ( 0 - para utilizar el puerto por defecto que para el protocolo FTP es el 21)
            /// </summary>
            public int Port { get; set; }

            public FTPProtocol Protocol { get; set; }

            /// <summary>
            /// Indica el nivel de seguridad de la conexion
            /// </summary>
            public FTPEncryption Encryption { get; set; }

            /// <summary>
            /// Indica el modo de autentificación contra el Servidor
            /// </summary>
            public FTPAuthenticationMode AuthenticationMode { get; set; }

            /// <summary>
            /// User Login
            /// </summary>
            public string AuthUser { get; set; }

            /// <summary>
            /// User Password
            /// </summary>
            public string AuthPass { get; set; }

            /// <summary>
            /// Indica el modo de autentificación contra el Servidor
            /// </summary>
            public FTPTransferMode TransferMode { get; set; }

            /// <summary>
            /// Directorio Remoto por Defecto
            /// </summary>
            public string DefaultRemotePath { get; set; }


        }


        //public class PatchSource

        /// <summary>
        /// Configuracion del Envio de ficheros LOG generados por la aplicación a un Servidor FTP
        /// </summary>
        public class LogShippingConfig
        {
            private FTPServer _ftpserver;

            public LogShippingConfig()
            {
                //UI-WSWorker
                FtpServer.ServerAddress = "ftp.amcoex.es";
                FtpServer.DefaultRemotePath = "/httpdocs/downloads/amcoex/gitsnavega-peru/logs/";
                FtpServer.AuthUser = "soft2867@amcoex.es";
                FtpServer.AuthPass = "3141516";

                ModeShipping=LogModeShipping.None;
            }

            public FTPServer FtpServer
            {
                get { return _ftpserver ?? (_ftpserver = new FTPServer()); }
            }

            /// <summary>
            /// Periodicidad de Envio
            /// </summary>
            public LogModeShipping ModeShipping { get; set; }
        }


        #region Behaviors Classes

        public class Behaviors
        {

            private BehaviorGeneralConfig _generalBehaviorGeneral;
            private BehaviorFrameConfig _frameBehavior;
            private BehaviorCommConfig _commBehavior;
            private BehaviorOdometerConfig _odometerBehavior;
            private BehaviorEndpointConfig _endpointConfig;
            private BehaviorLogConfig _logConfig;


            /// <summary>
            /// Configuraciones generales
            /// </summary>
            public BehaviorGeneralConfig General
            {
                get { return _generalBehaviorGeneral ?? (_generalBehaviorGeneral = new BehaviorGeneralConfig()); }
            }

            /// <summary>
            /// Configuraciones sobre Frames y elementos relacionados
            /// </summary>
            public BehaviorFrameConfig Frames
            {
                get { return _frameBehavior ?? (_frameBehavior = new BehaviorFrameConfig()); }
            }

            /// <summary>
            /// Configuraciones sobre Comunicaciones y elementos relacionados
            /// </summary>
            public BehaviorCommConfig Comms
            {
                get { return _commBehavior ?? (_commBehavior = new BehaviorCommConfig()); }
            }

            /// <summary>
            /// Configuraciones sobre Cuentakilometros y calculo de distancias
            /// </summary>
            public BehaviorOdometerConfig Odometer
            {
                get { return _odometerBehavior ?? (_odometerBehavior = new BehaviorOdometerConfig()); }
            }

            /// <summary>
            /// Configuraciones sobre Cuentakilometros y calculo de distancias
            /// </summary>
            public BehaviorEndpointConfig Endpoint
            {
                get { return _endpointConfig ?? (_endpointConfig = new BehaviorEndpointConfig()); }
            }

            /// <summary>
            /// Configuraciones sobre creación de Ficheros de Logging
            /// </summary>
            public BehaviorLogConfig Logging
            {
                get { return _logConfig ?? (_logConfig = new BehaviorLogConfig()); }
            }

            #region Nested Classes

            public class BehaviorGeneralConfig
            {
                /// <summary>
                /// Umbral de Parada (Tiempo|minutos): Intervalo de Tiempo a transcurrir estando debajo del Umbral de velocidad, para Activar: Modo de Parada
                /// </summary>
                [Range(3, 10), Description("Umbral de Parada: Tiempo (min)")]
                public int ThresholdStopModeTime { get; set; }

                /// <summary>
                /// Umbral de Parada (Velocidad|Km/h): Limite de Velocidad que estando por debajo se activa el contador de tiempo para Activar: Modo de Parada
                /// </summary>
                [Range(5, 20), Description("Umbral de Parada: Velocidad (km/h)")] 
                public int ThresholdStopModeSpeed { get; set; }

                /// <summary>
                /// Protector de Pantalla (Tiempo|segundos): Intervalo de Tiempo a transcurrir para activar el Protector
                /// </summary>
                [Range(1, 15), Description("Apagar pantalla transcurridos")] 
                public int ScreenSaverTime { get; set; }

                /// <summary>
                /// Maximo Tiempo permitido sin conexion a internet. En este caso se considera que existe algun problema en el sensor o dispositivo interno.
                /// </summary>
                [Range(0, 60), Description("Máximo Tiempo sin Conexion a Internet")] 
                public int NoConnectionInternetMaxTime { get; set; }

                /// <summary>
                /// No conexion a internet. Reintentos de reparar el Modem sin reiniciar el Sistema. A veces supondra reinicar la aplicación.
                /// </summary>
                [Range(0, 15), Description("Nº Intentos reset modem sin reinicio")] 
                public int NoConnectionInternetRetryWithoutRestarSystem { get; set; }

                /// <summary>
                /// No conexion a Servidor Central. Maximo Tiempo permitido sin conexion con el Servidor Central (EndPoint).
                /// </summary>
                [Range(0, 360), Description("Máximo Tiempo sin conexion con el Servidor Central")] 
                public int NoConnectionEndpointMaxTime { get; set; }

                /// <summary>
                /// GPS. Maximo Tiempo permitido sin recepción GPS. (Solo valido para revisar en estados que implican movimento).
                /// </summary>
                [Range(0, 360), Description("Máximo Tiempo sin Recepción GPS")] 
                public int NoGpsReceptionMaxTime { get; set; }

                /// <summary>
                /// GPS. Registar posiciones gps en en log de la aplicación.
                /// </summary>
                public bool LogToGpsPositions { get; set; }

                /// <summary>
                /// Reboot max days. Nº Maximo de dias sin reinciar/apagar el Terminal
                /// </summary>
                [Range(4, 10), Description("Maximo número de dias sin Apagar")] 
                public bool ForceRebootSystemDays { get; set; }
            }


            public class BehaviorFrameConfig
            {
                public BehaviorFrameConfig()
                {
                    FrecuencyShippingTime = 30;
                    FrecuencyBuildStateFrameModeMotionTime = 30;
                    FrecuencyBuildStateFrameModeStopTime =15 ;
                }

                /// <summary>
                /// Frecuencia (Tiempo|min) Generar frames (tramas) <see cref="TerminalStateDto"/> en Modo Parada
                /// </summary>
                [Range(10, 20),DefaultValue(15), Description("Frecuencia Envio Tramas en Parada (min.)")] 
                public int FrecuencyBuildStateFrameModeStopTime { get; set; }

                /// <summary>
                /// Frecuencia (Tiempo|seg) Generar frames (tramas) <see cref="TerminalStateDto"/> en Modo Movimiento
                /// </summary>
                [Range(10, 120), DefaultValue(30),Description("Frecuencia Envio Tramas en Movimiento (seg.)")] 
                public int FrecuencyBuildStateFrameModeMotionTime { get; set; }

                /// <summary>
                /// Frecuencia (Tiempo|segundos) Envio de Frames <see cref="IModel"/>. Solo si hay frames pendientes de envio.
                /// <para>Este parametro es importante para que no se colapse el sistema, y dar tiempo a que los procesos de verificación de Envio tengan efecto.</para>
                /// </summary>
                [Range(10, 60),DefaultValue(30), Description("Frecuencia Envio de Paquetes (seg.)")] 
                public int FrecuencyShippingTime { get; set; }
            }

            /// <summary>
            /// Configuraciones sobre Comunicaciones
            /// </summary>
            public class BehaviorCommConfig
            {
                public BehaviorCommConfig()
                {
                    TimeoutMaxAck = 40;
                    TimeoutMinAck = 8;
                    MaxFramesByPackage = 20;
                }

                /// <summary>
                /// Maximo Timeout (Tiempo|segundos) para recibir respuesta de envio recibido.<para>El timeout va aumentado o disminuyendo en funcion de la rapidez de las comunicaciones. A mas lentitud => Mayor Timeout.</para>
                /// </summary>
                [Range(15, 90), DefaultValue(40), Description("Máximo Tiempo Tiempo Máximo (seg.)")] 
                public int TimeoutMaxAck { get; set; }

                /// <summary>
                /// Minimo Timeout (Tiempo|segundos) para recibir respuesta de envio recibido.<para>El timeout va aumentado o disminuyendo en funcion de la rapidez de las comunicaciones. A mas lentitud => Mayor Timeout.</para>
                /// </summary>
                [Range(5, 15), DefaultValue(8), Description("Espera Respuestas Tiempo Minimo (seg.)")] 
                public int TimeoutMinAck { get; set; }

                /// <summary>
                /// Frames por Paquete. Maximo numero de frames por paquete a enviar. Utilizado para disminuir el tamaño de los paquetes, para dispositivos con problemas en paquetes grandes.
                /// </summary>
                [Range(5, 30), DefaultValue(20), Description("Maximo Nº de Tramas por Paquete")] 
                public int MaxFramesByPackage { get; set; }
            }

            public class BehaviorOdometerConfig
            {
                public BehaviorOdometerConfig()
                {
                    DistanceMaxBeetweenConsecutivePoints = 10;
                }

                /// <summary>
                /// Maxima distancia permitida como valida, entre 2 puntos consecutivos (Distancia|Km)
                /// </summary>
                [Range(5, 30), DefaultValue(10), Description("Distancia Máxima entre puntos consecutivos (km)")] 
                public int DistanceMaxBeetweenConsecutivePoints { get; set; }
            }

            public class BehaviorEndpointConfig
            {
                public BehaviorEndpointConfig()
                {
                    Timeout = 8;
                    RetryNumberByEndpoint = 3;
                    FirstEndpoint = 1;
                }

                /// <summary>
                /// Timeout connection.
                /// </summary>
                [Range(5, 30), DefaultValue(8), Description("Timeout connection")] 
                public int Timeout { get; set; }

                /// <summary>
                /// Numero de reintentos de conexion por <see cref="EndPoint"/>
                /// </summary>
                [Range(1, 5), DefaultValue(3), Description("Nº Reintentos por destino de conexión")] 
                public int RetryNumberByEndpoint { get; set; }

                /// <summary>
                /// Primer <see cref="EndPoint"/> al cual conectar el primera instanacia.
                /// <para>El ciclo de balanceo o reintentos de conexiones sera: First (F) -> F+1 -> F+2 .. -> F+N -> 1 -> 2 .. -> F (continua el ciclo) </para>
                /// </summary>
                [Range(1, 1000), Description("Nº del Primer servidor a conectar")] 
                public int FirstEndpoint { get; set; }

            }

            public class BehaviorLogConfig
            {
                public BehaviorLogConfig()
                {
                    DefaultLevelLog=LevelLog.Debug;
                    FileRotationMode=LogFileRotateMode.Daily;
                    LogFile = "gnperu.sil";
                }

                /// <summary>
                /// Nivel predeterminado para la generación del Log.
                /// </summary>
                public LevelLog DefaultLevelLog { get; set; }

                /// <summary>
                /// Indica el modo por el cual se generaran automaticamente los ficheros de Log.
                /// </summary>
                public LogFileRotateMode FileRotationMode { get; set; }

                /// <summary>
                /// Nombre del fichero de log generado. A este nombre automaticamente se le agregaran un suffijo del <see cref="LogFileRotateMode"/>
                /// </summary>
                public string LogFile { get; set; }

            }

            #endregion


        }

        #endregion


        #endregion


    }

    
}