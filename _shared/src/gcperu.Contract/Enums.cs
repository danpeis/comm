﻿namespace gcperu.Contract
{
    public enum VehiculoEstado : byte
    {
        Rojo = 0,
        Verde = 1,
        Naranja = 2,
        Amarillo = 3,
        Azul = 4
    }

    #region FTP Enums

    public enum FTPProtocol : byte
    {
        FTP,
        SFTP
    }

    public enum FTPEncryption : byte
    {

        /// <summary>
        /// Solo usar FTP plano, sin cifrado e inseguro. Opcion por defecto
        /// </summary>
        Plain = 0,

        /// <summary>
        /// Requerir FTP implicito sobre TLS
        /// </summary>
        RequiredExplicitOnTLS,

        /// <summary>
        /// Requerir FTP explicito sobre TLS
        /// </summary>
        RequiredImplicitOnTLS,

        /// <summary>
        /// Usar FTP explicito sobre TLS si disponible
        /// </summary>
        UserExplicitAvailableOnTLS
    }
    public enum FTPAuthenticationMode : byte
    {

        /// <summary>
        /// Acceso a traves de Usuario y contraseña
        /// </summary>
        Normal = 0,

        /// <summary>
        /// Acceso anonimo.
        /// </summary>
        Anonymnous = 99,
    }
    public enum FTPTransferMode : byte
    {

        /// <summary>
        /// Modo de transferencia Activo. Opcion por defecto
        /// </summary>
        Active = 0,

        /// <summary>
        /// Modo de transferencia Passiva
        /// </summary>
        Passive = 99,
    }

    #endregion

    #region Logging Enums

    public enum LevelLog
    {
        Debug = 0,
        Verbose = 1,
        Message = 2,
        Warning = 3,
        Error = 4,
        Fatal = 5
    }

    /// <summary>
    /// Tipos de rotación de los Ficheros de log creados.
    /// </summary>
    public enum LogFileRotateMode
    {
        None,
        Hourly,
        Daily
    }

    public enum LogModeShipping : byte
    {
        None = 0,
        Daily = 1,
        Weekly = 2
    }

    #endregion

    #region GPS enums

    public enum GpsFixMode : byte
    {
        /// <summary>
        /// Gps no tiene una posiciones valida
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Gps no tiene una posiciones valida
        /// </summary>
        FixInvalid = 1,
        /// <summary>
        /// Gps dando posiciones en 2 Dimensiones
        /// </summary>
        Fix2D = 2,
        /// <summary>
        /// Gps dando posiciones en 3 Dimensiones
        /// </summary>
        Fix3D = 3
    }

    #endregion

    /// <summary>
    /// Indica el estado de Funcionamiento a nivel del Dispositivo y la aplicación. 
    /// </summary>
    public enum OperationalDeviceStatus : byte
    {
        Shutdown,
        Running,
        AppRestarting,
        SystemRestarting,
        AppClosed,
    }
    /// <summary>
    /// Indica el estado del dispositovo a nivel funcional de la aplicacion. Ej: Funcionamiento correo o algun error.
    /// </summary>
    public enum FunctionalDeviceStatus : byte
    {
        Ready = 1,
        Error
    }

    public enum TerminalSoftwareTipo : byte
    {
        GNavWin8Dsk = 1,
    }
    public enum TerminalSoftwareModule : byte
    {
        WSSentry = 1,
        WSWorker = 2,
        UI = 3,
    }

}