﻿using System.Runtime.Serialization;

namespace gcperu.Contract.Info
{
    /// <summary>
    /// Clase utilizada para solicitar algo al Terminal u informacion que Report/Informa sobre datos puntuales del Terminal
    /// </summary>
    [DataContract(Name = "RDI")]
    //[KnownType(typeof(RequestDataConfigTemplateInfo))]
    public class TerminalRequestDataInfo
    {

        public enum TipoData
        {
            Undefined = 0,
            Sos=1,
            Sos2=2,
        }

        public TerminalRequestDataInfo()
        {
            Tipo = TipoData.Undefined;
        }

        public TerminalRequestDataInfo(TipoData tipo)
        {
            Tipo = tipo;
        }

        /// <summary>
        /// Tipo de peticion solicitada
        /// </summary>
        [DataMember(Name = "tp", Order = 1, IsRequired = true)]
        public TipoData Tipo { get; set; }

    }
}