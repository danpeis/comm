﻿using System.Runtime.Serialization;

namespace gcperu.Contract.Info
{
    /// <summary>
    /// Clase utilizada para solicitar algo al Servidor u obtener algun tipo de información del Mismo
    /// </summary>
    [DataContract]
    //[KnownType(typeof(RequestDataConfigTemplateInfo))]
    public class ServerRequestDataInfo
    {
        public enum TipoData
        {
            Undefined = 0,
            DefaultTemplateConfig = 1
        }

        public ServerRequestDataInfo(TipoData tipo)
        {
            Tipo = tipo;
        }

        public ServerRequestDataInfo()
        {
            Tipo = TipoData.Undefined;
        }

        /// <summary>
        /// Tipo de informacion/dato que almacena
        /// </summary>
        [DataMember(Name = "tp", Order = 1, IsRequired = true)]
        public TipoData Tipo { get; set; }

    }
}